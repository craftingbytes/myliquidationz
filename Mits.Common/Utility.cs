﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Security.Cryptography;

namespace Mits.Common
{
    /// <summary>
    /// class for keeping common utility methods
    /// </summary>
    public class Utility
    {

        #region Security Helper Methods

        
        /// <summary>
        /// Returns an md5 hash for a given string without seed
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string CreateHash(string value)
        {
            return CreateHash(value, false);
        }

        /// <summary>
        /// Returns an md5 hash for a given string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="addSeed"></param>
        /// <returns></returns>
        public static string CreateHash(string value, bool addSeed)
        {
            string hash = "";
            string seed = DateTime.Now.Millisecond.ToString();
            if (!addSeed) seed = "";
            string hashSource = seed + "2344ilku&&^%&#fg" + value + "sdiuvhiu&%^^ebrk80hsd&&^%&";
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(hashSource));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            hash = sBuilder.ToString();

            return hash;
        }
        #endregion

        #region Mail Html Body Helper Methods
        public static string ConvertToHtmlTable(Dictionary<object,object> _dictionary)
        {
            StringBuilder message = new StringBuilder();

           
            message.Append("<br/><br/><table style='font-family:Calibri; font-size:11pt;' border='0' width='750px' cellpadding='3' cellspacing='1's id='table1'>"  );

            if (_dictionary != null)
            {
                foreach (KeyValuePair<object,object> de in _dictionary)
                {
                    message.Append("<tr>");
                    message.Append("<td style='width:20%;'><b>" + de.Key + ":</b></td>");
                    message.Append("<td style='width:80%;'>" + de.Value + "</td>");
                    message.Append("</tr>");
                }
            }

            message.Append("</table><br/><br/>");

            return message.ToString();

        }
        public static string BodyStart()
        {
            StringBuilder message = new StringBuilder();
            message.Append("<html><body style='font-family:Calibri; font-size:11pt;'>");
            return message.ToString();
        }
        public static string BodyEnd()
        {
            StringBuilder message = new StringBuilder();
            message.Append("</body></html>");
            return message.ToString();
        }
        public static string AddTextLine(string line)
        {
            StringBuilder message = new StringBuilder();
            message.Append(line);
            return message.ToString();
        }
        public static string AddTextLine(string line, bool isBold)
        {
            StringBuilder message = new StringBuilder();
            if (isBold == true)
            {
                message.Append("<b>" + line + "</b>");
            }
            else
            {
                message.Append(line);
            }
            return message.ToString();
        }
        public static string AddBreak(int count=1)
        {
            StringBuilder message = new StringBuilder();
            for (int i = 0; i < count; i++)
            {
                message.Append("<br/>");
            }
            return message.ToString();
        }
        public static string UnOrderListStart()
        {
            StringBuilder message = new StringBuilder();
            message.Append("<ul>");
            return message.ToString();
        }
        public static string UnOrderListEnd()
        {
            StringBuilder message = new StringBuilder();
            message.Append("</ul>");
            return message.ToString();
        }
        public static string ListItemStart()
        {
            StringBuilder message = new StringBuilder();
            message.Append("<li>");
            return message.ToString();
        }
        public static string ListItemEnd()
        {
            StringBuilder message = new StringBuilder();
            message.Append("</li>");
            return message.ToString();
        }
        #endregion 
    }
}
