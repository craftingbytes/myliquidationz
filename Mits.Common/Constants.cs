﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mits.Common
{
    public class Constants
    {
        public const string AffiliateSessionParameters = "AffiliateSessionParameters";

        public class SessionParameters
        {
            // Const keys for cookie dictionary
            public const string AffiliateId = "AffiliateId";
            public const string AffiliateContactId = "AffiliateContactId";
            public const string AffiliateRoleId = "AffiliateRoleId";
            public const string AffiliateRoleName = "AffiliateRoleName";
            public const string AffiliateTypeId = "AffiliateTypeId";
            public const string AffiliateType = "AffiliateType";
            public const string AffiliateContactName = "AffiliateContactName";
            public const string IpAddress = "IpAddress";
            public const string SelectedAffiliateId = "SelectedAffiliateId";
            public const string MasterAffiliateContactId = "MasterAffiliateContactId";
            public const string MailBackDevices = "MailBackDevices";
            public const string ConsumerProductTypes = "ConsumerProductTypes";
            public const string ConsumerProductManufacturerID = "ConsumerProductManufacturerID";
            public const string ConsumerDetails = "ConsumerDetails";
            public const string CurrentPaymentID = "CurrentPaymentID";
            public const string CurrentAffiliateID = "CurrentAffiliateID";
            public const string InvoiceItems = "InvoiceItems";
            public const string CurrentInvoiceID = "CurrentInvoiceID";
            public const string CurrentProcessingDataID = "CurrentProcessingDataID";
        }

        public class RoleNames
        {
            public const string Processor_Recycler = "Processor/Recycler";
            public const string Processor = "Processor";
        }

        public enum ActionType : int
        {
            Insert = 1,
            Update = 2,
            Delete = 3,
            Read = 4,
            Login = 5,
            InvoiceVerified = 6,
            InvoiceUnverified = 7,
            InvoiceApproved = 8,
            InvoiceUnApproved = 9
        }

        public class Messages
        {
            public const string InvalidLoginCredentials = "Invalid Login Credentials, Please try with correct credentials.";
            public const string AffiliateNameExists = "Entity Name already exists. Please try with different Entity Name.";
            public const string RecordSaved = "Record has been saved successfully.";
            public const string RecordNotSaved = "Record could not be saved.";
            public const string UserNameExists = "User Name already exists. Please try with different User Name.";
            public const string EmailExists = "Email already exists. Please try with different Email.";
            public const string InformationExists = "An email was sent to your account with instructions on how to reset your password.";
            public const string ServerError = "Server error occured.";
            public const string TryLater = "Your Request could not be processed now, please try later.";
            public const string RecordExist = "This record already exist";
            public const string UserInactive = "The current user is not active in our record";
            //public static string AccountLocked = 

            public static string AccountLocked
            {
                get {  return ""+(ConfigHelper.LoginTryCount+1)+" unsuccesful attempts to login have caused this address or account to be locked. The account will remain locked for " + ConfigHelper.LoginTryMinute.ToString() + "  minutes"; }
            }
            public static string LoginTry(int? count)
            {
               string msg="You have made "+(count)+" unsuccessful attempt to login. "+(ConfigHelper.LoginTryCount+1)+" unsuccessful attempts will cause the address or account to be locked for " + ConfigHelper.LoginTryMinute.ToString() + "  minutes";
               return msg;
            }
        }

        public enum AffiliateType : int
        {
            Public = 0,
            OEM = 1,
            Processor = 2,
            Collector = 3,
            State = 4,
            Eworld = 5,
            Auditor = 7
        }

        public enum AffiliateContactRole : int
        {
            Administrator = 1,
            Finance = 2,
            OEM = 3,
            Processor = 4,
            Collector = 5,
            OEMPotential = 6,
            Manager = 7,
            ExecutiveAssistant = 8,
            Auditor = 9
        }

        public enum EventTypeEnum
        {
            Single = 6,
            Days = 7,
            Monthly = 8,
            Quaterly = 9,
            Yearly = 10
        }

        public enum AreaName
        {
            EntitySearch,
            Entity,
            EntityContact,
            ContractRate,
            Target,
            Accreditation,
            EntityAccreditation,
            StatePolicy,
            OEMStatePolicy,
            Policy,
            ProductType,
            ServiceTypeGroup,
            ServiceType,
            DocumentLib,
            ProcessingReportSearch,
            ProcessingReportForm,
            Event,
            Invoice,
            Payable,
            Receivable,
            Reconciliation,
            EntityState,
            Map,
            AccountsReport,
            GeneralSummaryReport,
            ElectronicProductsRecyclingReport,
            OEMTargetDetailReport,
            TimeSpanComparisonReport,
            CertificateOfAssuredRecyclingReport,
            ProcessRecyclerReport,
            InvoiceReport,
            ProcessingReport,
            Authorization,
            Contracts,
            GeneralSummaryOEM,
            AdminYearlyAccountComparisonReport,
            AdminObligationReport,
            OEMTargetReportAccountDetail_StateView,
            OEMTargetReportAccountDetail_OEMView,
            OEMTargetReportAccountDetailSummary,
            OEMTargetReportProcessedStatusSummary,
            OEMTargetReportObligationStatus_StateView,
            OEMTargetReportObligationStatus_StateView_new,
            OEMTargetReportObligationStatus_OEMView,
            AdminWeightProcessedReport,
            AdminUnAssignedWeightReport,
            AssignedWeightDetail,
            ProcessingReportDetail,
            ProcessingReportAll,
            StateGuidelines,
            RecyclerProductWeight,
            RecyclerMonthReport,
            NewsFeed,
            ClientAddress,
            AuditReport,
            CollectionEvent,
            OEMInvoice,
            ProcessorInvoice,
            OEMReconciliationDetail,
            OEMWeightProcessedReport,
            StateProductType
        }
    }
}
