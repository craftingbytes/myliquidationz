﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Mits.Common
{
    public class ConfigHelper
    {
        #region Login
        public static string ComputePwdHash
        {
            get { return ConfigurationManager.AppSettings["computePwdHash"].ToLower(); }
        }
        public static int LoginTryCount
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["loginTryCount"]); }
        }
        public static int LoginTryMinute
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["loginTryMinute"]); }
        }
        #endregion

        #region SMTP and Email

        public static string SmptServer
        {
            get { return ConfigurationManager.AppSettings["smtp"]; }
            //get { return EmailSettingsHelper.SmptServer; }
        }

        public static int SmtpPort
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]); }
        }

        public static bool SmtpAuth
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["smtpAuth"]); }
        }

        public static string SmtpUserName
        {
            get { return ConfigurationManager.AppSettings["username"]; }
        }

        public static string SmtpPassword
        {
            get { return ConfigurationManager.AppSettings["password"]; }
        }

        public static string FromEmail
        {
            get { return ConfigurationManager.AppSettings["fromEmail"]; }
        }

        public static string AdminEmail
        {
            get { return ConfigurationManager.AppSettings["adminEmail"]; }
        }
        public static string AdminCCEmail
        {
            get { return ConfigurationManager.AppSettings["adminCCEmail"]; }
        }
        public static string InvoiceToEmail
        {
            get { return ConfigurationManager.AppSettings["invoiceToEmail"]; }
        }
        public static string MailBackToEmail
        {
            get { return ConfigurationManager.AppSettings["mailBackToEmail"]; }
        }
        public static string MailBackCCEmail
        {
            get { return ConfigurationManager.AppSettings["mailBackCCEmail"]; }
        }
        public static string SendUserCredentials
        {
            get { return ConfigurationManager.AppSettings["sendUserCredentials"].ToLower(); }
        }
     
        #endregion

       
    }
}
