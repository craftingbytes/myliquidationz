﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mits.Common
{
    public class SessionCookieValues : ISessionCookieValues
    {
        private Dictionary<string, string> _values = null;

        public SessionCookieValues()
        {
        }

        public Dictionary<string, string> GetSessionValues()
        {
            if (_values == null)
            {
                var cookie = HttpContext.Current.Request.Cookies[Constants.AffiliateSessionParameters];
                if (cookie != null)
                {
                    var dictStr = cookie.Value;
                    if (!string.IsNullOrWhiteSpace(dictStr))
                    {
                        _values = dictStr.Split('&')
                            .Select(s => s.Split('='))
                            .ToDictionary(
                                p => Uri.UnescapeDataString(p[0].Trim()),
                                p => Uri.UnescapeDataString(p[1].Trim())
                            );
                    }
                }
            }

            return _values;
        }

        public void SetSessionValues(Dictionary<string, string> values)
        {
            HttpCookie cookie = new HttpCookie(Constants.AffiliateSessionParameters);
            cookie.Value = string.Join("&", values.Select(v => $"{Uri.EscapeDataString(v.Key)}={Uri.EscapeDataString(v.Value)}"));
            HttpContext.Current.Response.Cookies.Set(cookie);
        }

        public void ClearCookie()
        {
            HttpContext.Current.Response.Cookies[Constants.AffiliateSessionParameters].Expires = DateTime.Now.AddSeconds(-1);
            _values = null;
        }

        public string GetSessionValue(string key)
        {
            Dictionary<string, string> dict = GetSessionValues();
            if (dict != null)
            {
                string value;
                if (dict.TryGetValue(key, out value))
                {
                    return value;
                }
            }

            return "-1";
        }

        public int GetSessionIntValue(string key)
        {
            string value = GetSessionValue(key);
            if (!string.IsNullOrWhiteSpace(value))
            {
                try
                {
                    return int.Parse(value);
                }
                catch (Exception ex)
                {
                }
            }

            return -1;
        }

        public void SetSessionValue<T>(string key, T value)
        {
            Dictionary<string, string> dict = GetSessionValues();
            if (dict != null)
            {
                dict[key] = value.ToString();

                SetSessionValues(dict);
            }
        }
    }
}
