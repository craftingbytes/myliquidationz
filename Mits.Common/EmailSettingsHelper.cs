﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
namespace Mits.Common
{
    public class EmailSettingsHelper
    {
        public static string SmptServer;
        public static int SmtpPort;
        public static bool SmtpAuth;
        public static string SmtpUserName;
        public static string SmtpPassword;
        public static string FromEmail;
        public static string AdminEmail;
        public static string AdminCCEmail;
        public static string InvoiceToEmail;
        public static string MailBackToEmail;
        public static string MailBackCCEmail;
        private string xmlDocumentPath;

        public EmailSettingsHelper()
        {
            xmlDocumentPath = System.Web.HttpContext.Current.Server.MapPath("~/Helpers/EmailSettings.xml");
            XmlDocument _xmlDocumnet = new XmlDocument();
            _xmlDocumnet.Load(xmlDocumentPath);
            XmlElement _rootElement = _xmlDocumnet.DocumentElement;
            
            SmptServer = _rootElement.Attributes["smtp"].Value;
            SmtpPort=Convert.ToInt32(_rootElement.Attributes["smtpPort"].Value);
            SmtpAuth = Convert.ToBoolean(_rootElement.Attributes["smtpAuth"].Value);
            SmtpUserName = _rootElement.Attributes["username"].Value;
            SmtpPassword = _rootElement.Attributes["password"].Value;
            FromEmail = _rootElement.Attributes["fromEmail"].Value;
            AdminEmail = _rootElement.Attributes["adminEmail"].Value;
            AdminCCEmail = _rootElement.Attributes["adminCCEmail"].Value;
            InvoiceToEmail = _rootElement.Attributes["invoiceToEmail"].Value;
            MailBackToEmail = _rootElement.Attributes["mailBackToEmail"].Value;
            MailBackCCEmail = _rootElement.Attributes["mailBackCCEmail"].Value;


        }
    }
}
