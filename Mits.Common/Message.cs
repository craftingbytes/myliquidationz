﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mits.Common
{
    public class Message
    {
        private string type;
        private string text;

        public Message()
        {
        }

        public Message(string error, string message)
        {
            this.type = error;
            this.text = message;
        }

        public string Type
        {
            get { return type; }            
        }

        public string Text
        {
            get { return text; }            
        }
    }

    public enum MessageType
    {
        Success = 1,
        Failure = 2,
        BusinessError = 3,
        Exception = 4
    }

    public class MessageException : Exception
    {
        public Message Msg { get; private set; }

        public MessageType Type { get; private set; }

        public MessageException(MessageType type, string msg)
            :base(msg)
        {
            Type = type;
            Msg = new Message(type.ToString(), msg);
        }
    }
}
