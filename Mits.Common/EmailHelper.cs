﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using System.ComponentModel;

namespace Mits.Common
{
    public class EmailHelper
    {

        public static void SmtpClient_OnCompleted(object sender, AsyncCompletedEventArgs e)
        {
            ////Get the Original MailMessage object
            //MailMessage mail = (MailMessage)e.UserState;

            ////write out the subject
            //string subject = mail.Subject;

            //if (e.Cancelled)
            //{
            //    //Console.WriteLine("Send canceled for mail with subject [{0}].", subject);
            //}
            //if (e.Error != null)
            //{
            //    //Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString());
            //}
            //else
            //{
            //    //Console.WriteLine("Message [{0}] sent.", subject);
            //}
        }

        public static void SendMailAsync(string subject, string markup, string[] toAddress,
            string[] ccAddress, string[] bccAddress, List<Attachment> attachments = null)
        {
           try
            {
                SmtpClient smtpClient = new SmtpClient();
                MailMessage message = CreateMailMessage(subject, markup, toAddress, ccAddress, bccAddress, attachments);

                // Send SMTP mail
                smtpClient.SendCompleted += new SendCompletedEventHandler(SmtpClient_OnCompleted);
                smtpClient.SendAsync(message,"UserState");
            }
            catch (Exception ex)
            {
                //_success = false;
                throw ex.InnerException;

            }


        }

        public static bool SendMail(string subject, string markup, string[] toAddress,
            string[] ccAddress, string[] bccAddress, List<Attachment> attachments = null)
        {
            bool _success = false;
            try
            {

                SmtpClient smtpClient = new SmtpClient();
                MailMessage message = CreateMailMessage(subject, markup, toAddress, ccAddress, bccAddress, attachments);

                // Send SMTP mail
                smtpClient.Send(message);

                _success = true;
                return _success;
            }
            catch (Exception ex)
            {
                _success = false;
                throw ex.InnerException;
                //return _success;
                
            }
        }

        private static MailMessage CreateMailMessage(string subject, string markup, string[] toAddress, string[] ccAddress, string[] bccAddress, List<Attachment> attachments)
        {
            MailMessage message = new MailMessage();

            string _fromEmail = ConfigurationManager.AppSettings["fromEmail"];

            MailAddress fromAddress = new MailAddress(ConfigHelper.FromEmail);

            message.From = fromAddress;

            message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");

            // To address collection of MailAddress
            if (toAddress != null)
            {
                foreach (string toAdd in toAddress)
                {
                    message.To.Add(toAdd);
                }
            }
            if (ccAddress != null)
            {
                foreach (string toAdd in ccAddress)
                {
                    message.CC.Add(toAdd);
                }
            }
            if (bccAddress != null)
            {
                foreach (string toAdd in bccAddress)
                {
                    message.Bcc.Add(toAdd);
                }
            }

            if (attachments != null)
            {
                foreach (var file in attachments)
                {

                    message.Attachments.Add(file);
                }
            }

            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = markup;
            return message;
        }
    }
}
