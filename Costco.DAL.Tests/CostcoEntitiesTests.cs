﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Costco.DAL.EntityModels;
using Costco.DAL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Costco.DAL.Tests
{
    [TestClass]
    public class CostcoEntitiesTests
    {
        private const int stateIdNewYork = 37;
        private const int affiliateIdCostCo1015 = 561;
        private const int affiliateIdCostCo1030 = 574;

        private AuditReport getAuditReport(CostcoEntities ctx)
        {
            AuditReport auditReport = new AuditReport
            {
                AuditDate = new DateTime(2020, 08, 30, 11, 17, 42),
                FacilityName = "PK Metals",
                PlanYear = 2020,
                StateId = stateIdNewYork,
            };
            var auditReportFromDb = ctx.AuditReports.SingleOrDefault(ar =>
                ar.AuditDate == auditReport.AuditDate
                && ar.StateId == auditReport.StateId);
            if (auditReportFromDb == null)
            {
                ctx.AuditReports.Add(auditReport);
                ctx.SaveChanges();
            }
            else
            {
                auditReport = auditReportFromDb;
            }

            return auditReport;
        }

        [TestMethod]
        public void TestAddAuditReportState()
        {
            using (var ctx = new CostcoEntities())
            {
                AuditReport auditReport = getAuditReport(ctx);

                ctx.p_AddAuditReportState(auditReport.Id, stateIdNewYork);

                var resultFromDb = ctx.AuditReports.SingleOrDefault(ar =>
                    ar.StateId == stateIdNewYork
                    && ar.Id == auditReport.Id);
                Assert.IsNotNull(resultFromDb);
            }
        }

        [TestMethod]
        public void TestAddDocumentToAuditReport()
        {
            using (var ctx = new CostcoEntities())
            {
                // TODO: As per the application, this is suppoed to be 7, but throws as noted below.
                int tableId = 1; // ctx.ForeignTables.First<ForeignTable>(i => i.ForeignTableName == "AuditReport").Id;

                AuditReport auditReport = getAuditReport(ctx);

                Document document = new Document
                {
                    DocumentTypeId = 15,
                    AffiliateContactId = 603,
                    ServerFileName = "C:\\inetpub\\wwwroot\\CostcoMVC5\\Uploads\\1128891c-4831-4fd6-9cc0-9311bd658128",
                    UploadFileName = "cor34917.pdf",
                    UploadDate = new DateTime(2020, 08, 30, 11, 17, 42),
                    DeleteDate = null
                };
                var documentFromDb = ctx.Documents.SingleOrDefault(doc =>
                    doc.ServerFileName == document.ServerFileName
                    && doc.UploadFileName == document.UploadFileName);
                if (documentFromDb == null)
                {
                    ctx.Documents.Add(document);
                    ctx.SaveChanges();
                }
                else
                {
                    document = documentFromDb;
                }

                var documentRelationFromDb = ctx.DocumentRelations.SingleOrDefault(dr =>
                    dr.DocumentID == document.Id
                    && dr.ForeignID == auditReport.Id
                    && dr.ForeignTableID == tableId);
                if (documentRelationFromDb != null)
                {
                    ctx.DocumentRelations.Remove(documentRelationFromDb);
                    ctx.SaveChanges();
                }

                DocumentRelation docRelation = new DocumentRelation
                {
                    DocumentID = document.Id,
                    Document = document,
                    ForeignID = auditReport.Id,
                    ForeignTableID = tableId, // TODO: SaveChanges throws if "AuditReport" is set to 7.
                };
                ctx.DocumentRelations.Add(docRelation);
                ctx.SaveChanges();

                ctx.p_AddDocumentToAuditReport(docRelation.Id);
                ctx.SaveChanges();

                // TODO: Add DB queries to test the sproc. Just making sure it doesn't throw for now.
            }
        }

        [TestMethod]
        public void TestInsertUserReport()
        {
            using (var ctx = new CostcoEntities())
            {
                int affiliateContactId = affiliateIdCostCo1015;
                int groupId = 1;

                AuditReport auditReport = getAuditReport(ctx);
                ctx.p_InsertUserReport(affiliateContactId, auditReport.Id, groupId);
                // TODO: Add DB queries to test the sproc. Just making sure it doesn't throw for now.
            }
        }

        [TestMethod]
        public void TestChangeDisplayOrder()
        {
            using (var ctx = new CostcoEntities())
            {
                long userReportListId = 12; // Picked an ID from the DB

                AuditReport auditReport = getAuditReport(ctx);
                ctx.p_ChangeDisplayOrder(userReportListId, 0); // up
                ctx.p_ChangeDisplayOrder(userReportListId, 1); // down
                // TODO: Add DB queries to test the sproc. Just making sure it doesn't throw for now.
            }
        }

        [TestMethod]
        public void TestUpdateSalesOrderSoldAmount()
        {
            using (var ctx = new CostcoEntities())
            {
                SalesOrder order = new SalesOrder
                {
                    AffiliateId = affiliateIdCostCo1015,
                    Created = new DateTime(2020, 08, 30, 11, 17, 42),
                    SoldAmount = 2567.23M,
                    SoldToName = "Herman Munster",
                    SoldToAddress = "1313 Mockingbird Lane",
                    SoldToZip = "90028"
                };
                var orderFromDb = ctx.SalesOrders.SingleOrDefault(o =>
                    o.SoldToName == order.SoldToName);
                if (orderFromDb == null)
                {
                    ctx.SalesOrders.Add(order);
                    ctx.SaveChanges();
                }
                else
                {
                    order = orderFromDb;
                }

                //******************

                SalvageSalesOrder SSOrder = new SalvageSalesOrder
                {
                    AffiliateId = affiliateIdCostCo1015,
                    DateCreated = new DateTime(2020, 08, 30, 11, 17, 42),
                    RetailAmount = 2567.23M,
                    SoldToName = "Herman Munster",
                    SoldToAddress = "1313 Mockingbird Lane",
                    SoldToZip = "90028"
                };
                var SSOrderFromDb = ctx.SalvageSalesOrders.SingleOrDefault(o =>
                    o.SoldToName == SSOrder.SoldToName);
                if (SSOrderFromDb == null)
                {
                    ctx.SalvageSalesOrders.Add(SSOrder);
                    ctx.SaveChanges();
                }
                else
                {
                    SSOrder = SSOrderFromDb;
                }

                SalvageSalesOrderItem SSOItem = new SalvageSalesOrderItem
                {
                    SalvageSalesOrderId = SSOrder.Id,
                    ItemId = "widget-22576",
                    Description = "Something too wonderful for words!",
                    RetailValue = 99.95M,
                    SellingPrice = 73.45M,
                    Quantity = 10
                };
                var SSOItemFromDb = ctx.SalvageSalesOrderItems.SingleOrDefault(i =>
                    i.ItemId == SSOItem.ItemId);
                if (SSOItemFromDb == null)
                {
                    ctx.SalvageSalesOrderItems.Add(SSOItem);
                    ctx.SaveChanges();
                }
                else
                {
                    SSOItem = SSOItemFromDb;
                }

                SalesOrderSSOItem ssoitem = new SalesOrderSSOItem
                {
                    SalesOrderId = order.Id,
                    SSOId = SSOrder.Id,
                    SSOItemId = SSOItem.Id,
                    Description = "SSO Widget-42",
                    Price = 10.00M,
                    SellingPrice = 7.25M,
                    Quantity = 10
                };

                var ssoItemFromDb = ctx.SalesOrderSSOItems.SingleOrDefault(i =>
                    i.SalesOrderId == ssoitem.SalesOrderId &&
                    i.Description == ssoitem.Description);
                if (ssoItemFromDb == null)
                {
                    ctx.SalesOrderSSOItems.Add(ssoitem);
                    ctx.SaveChanges();
                }
                else
                {
                    ssoitem = ssoItemFromDb;
                }

                //******************

                SalesOrderItem orderItem = new SalesOrderItem
                {
                    SalesOrderId = order.Id,
                    ShipmentId = 1045,      // Random ID from DB
                    ShipmentItemId = 1145,  // Random ID from DB
                    Description = "Standard Widget-42",
                    Price = 20.00M,
                    SellingPrice = 10.00M,
                    Quantity = 10
                };

                var orderItemFromDb = ctx.SalesOrderItems.SingleOrDefault(i =>
                    i.SalesOrderId == orderItem.SalesOrderId &&
                    i.Description == orderItem.Description);
                if (orderItemFromDb == null)
                {
                    ctx.SalesOrderItems.Add(orderItem);
                    ctx.SaveChanges();
                }
                else
                {
                    orderItem = orderItemFromDb;
                }

                //******************

                SalesOrderBulkItem bulkItem = new SalesOrderBulkItem
                {
                    SalesOrderId = order.Id,
                    BulkShipmentId = 1007,      // Random ID from DB
                    BulkShipmentItemId = 1007,  // Random ID from DB
                    Description = "Bulk Widget-42",
                    Price = 5.00M,
                    SellingPrice = 3.00M,
                    Quantity = 10
                };

                var bulkItemFromDb = ctx.SalesOrderBulkItems.SingleOrDefault(i =>
                    i.SalesOrderId == bulkItem.SalesOrderId &&
                    i.Description == bulkItem.Description);
                if (bulkItemFromDb == null)
                {
                    ctx.SalesOrderBulkItems.Add(bulkItem);
                    ctx.SaveChanges();
                }
                else
                {
                    bulkItem = bulkItemFromDb;
                }

                decimal? expectedAmount = ssoitem.SellingPrice * ssoitem.Quantity +
                    bulkItem.SellingPrice * bulkItem.Quantity +
                    orderItem.SellingPrice * orderItem.Quantity;
                decimal ? soldAmount = null;
                soldAmount = ctx.p_UpdateSalesOrderSoldAmount(order.Id).First(); // TODO: This will always be a single number.
                Assert.IsNotNull(soldAmount);
                Assert.AreEqual(expectedAmount, soldAmount);
            }
        }

        [TestMethod]
        public void TestUpdateSalvageSalesOrderSoldAmount()
        {
            using (var ctx = new CostcoEntities())
            {
                SalvageSalesOrder order = new SalvageSalesOrder
                {
                    AffiliateId = affiliateIdCostCo1015,
                    DateCreated = new DateTime(2020, 08, 30, 11, 17, 42),
                    RetailAmount = 2567.23M,
                    SoldToName = "Herman Munster",
                    SoldToAddress = "1313 Mockingbird Lane",
                    SoldToZip = "90028"
                };
                var orderFromDb = ctx.SalvageSalesOrders.SingleOrDefault(o =>
                    o.SoldToName == order.SoldToName);
                if (orderFromDb == null)
                {
                    ctx.SalvageSalesOrders.Add(order);
                    ctx.SaveChanges();
                }
                else
                {
                    order = orderFromDb;
                }

                SalvageSalesOrderItem item = new SalvageSalesOrderItem
                {
                    SalvageSalesOrderId = order.Id,
                    ItemId = "widget-22576",
                    Description = "Something too wonderful for words!",
                    RetailValue = 99.95M,
                    SellingPrice = 73.45M,
                    Quantity = 10
                };
                var itemFromDb = ctx.SalvageSalesOrderItems.SingleOrDefault(i =>
                    i.ItemId == item.ItemId);
                if (itemFromDb == null)
                {
                    ctx.SalvageSalesOrderItems.Add(item);
                    ctx.SaveChanges();
                }
                else
                {
                    item = itemFromDb;
                }

                decimal soldAmount;
                decimal retailAmount;
                ctx.p_UpdateSalvageSalesOrderSoldAmount(item.SalvageSalesOrderId, out soldAmount, out retailAmount);

                Assert.AreEqual(item.SellingPrice * item.Quantity, soldAmount);
                Assert.AreEqual(item.RetailValue * item.Quantity, retailAmount);
            }
        }

        [TestMethod]
        public void TestGetUserReportList()
        {
            using (var ctx = new CostcoEntities())
            {
                int affiliateContactId = affiliateIdCostCo1015;
                List<UserReport> reports = ctx.p_GetUserReportList(affiliateContactId);
                Assert.IsNotNull(reports);
                Assert.AreEqual(28, reports.Count);
            }
        }
    }
}
