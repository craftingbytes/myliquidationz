﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="MVC.Upload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title></title>
<%--    <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/cupertino/jquery-ui-1.8.2.custom.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/jquery.multiSelect.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/ui.jqgrid.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Scripts/highlighter/styles/shCore.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Scripts/highlighter/styles/shThemeDefault.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/uploadify.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/default.css")%>" />

    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/uploadify.css")%>" />
    

    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery-1.4.2.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery-ui-1.8.4.custom.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.multiSelect.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/grid.locale-en.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.jqGrid.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/grid.subgrid.js")%>' type="text/javascript"></script>
    <script src="<%=ResolveClientUrl("~/Scripts/js/swfobject.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveClientUrl("~/Scripts/js/jquery.uploadify.v2.1.0.min.js") %>" type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/highlighter/scripts/shCore.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/highlighter/scripts/shBrushJScript.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/highlighter/scripts/shBrushCss.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.tree.js")%>' type="text/javascript"></script>--%>

    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/general.css")%>"  />
    <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/cupertino/jquery-ui-1.10.2.custom.css")%>" />
    <%--<link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/uploadify.css")%>" />--%>
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/uploadifive.css")%>" />

    <script src='<%=ResolveClientUrl("~/Scripts/jquery-1.10.2.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/jquery-ui-1.10.2.custom.min.js")%>' type="text/javascript"></script>

    <script src="<%=ResolveClientUrl("~/Scripts/UploadiFive/jquery.uploadifive.js") %>" type="text/javascript"></script>

    <%--<script src="<%=ResolveClientUrl("~/Scripts/js/swfobject.js") %>" type="text/javascript"></script>--%>
    <%--<script src="<%=ResolveClientUrl("~/Scripts/js/jquery.uploadifive.min.js") %>" type="text/javascript"></script>--%>

<script type="text/javascript">

    $(function () {


        var processDataId = getParameterByName('processDataId');
        var attachFile = getParameterByName('attachfile');

        //coming from reminder attach files button
        var fromReminder = getParameterByName('fromReminder');

        //coming from invoice attach files button
        var fromInvoice = getParameterByName('fromInvoice');

        //coming from invoice attach files button
        var fromAffiliate = getParameterByName('fromAffiliate');

        var auditReportId = getParameterByName('auditReportId');

        //$('#fileInput').uploadify({
        $('#fileInput').uploadifive({
            //'uploader': 'Scripts/js/uploadify.swf',
            'cancelImg': '/Content/images/cancel.png',
            'buttonImg': '/Content/images/browse.jpg',
            'auto': false,
            'multi': false,
            'simUploadLimit': 1,
            'sizeLimit': 10737418240,
            'checkScript': '/FileUpload/FileExists',
            'onCheck': function (file, exists) {
                if (exists) {
                    alert("File with the same name already exists. \nPlease, rename your file.");
                    //document.getElementById(jQuery(event.target).attr('id') + "Uploader").uploadifive('clearQueue');
                    $('#fileInput').uploadifive('clearQueue');
                    //return false;
                } 
            },
            'uploadScript': '/Helpers/Upload.ashx?attachfile=' + getParameterByName('attachfile'),
            'folder': 'Uploads',
            //onComplete: function (e, q, f, d) {
            onUploadComplete : function(file, data) {
                fileName = file.name;

                if (fromInvoice == 'true')
                    window.opener.FileUploaded(fileName);
                else if (fromAffiliate == 'true') {
                    window.opener.FileUploaded(fileName);
                }
                else if (fromReminder == 'true')
                    window.opener.postFileData(fileName);

                else if (attachFile == 'true')
                    window.opener.SaveAttachment(fileName, processDataId); //processDataId will be empty when user ad it on reprt form, and will have value when come from listing
                else if (auditReportId && auditReportId != "") {
                    window.opener.addDocument(fileName, auditReportId);
                    //window.opener.stuff();
                }
                else
                    window.opener.LoadData(fileName);

                window.close();
                return true;
            },
            //onAllComplete: function (event, data) {
            onQueueComplete : function(uploads) {
                if (uploads.count > 0) {
                    window.close();
                }
            }

        });

        $('#btnClear').button();
        $('input[id$=btnClear]').click(function () {
            // $('#fileInput').uploadifyCancel();
            $('#fileInput').uploadifive('clearQueue');
        });


        $("input[id$=btnUpload]").button().click(function () {
            $('#fileInput').uploadifive('upload');
        });
    });

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

</script>

<link href="Content/css/default.css" rel="stylesheet" type="text/css" />
<%--<link href="Content/css/uploadify.css" rel="stylesheet" type="text/css" />--%>

</head>
<body>

<%--<img src="Content/images/browse.jpg" alt="browse not loaded" />--%>
    <div id="dialog"   style="margin-left:10px; margin-top:10px;">
        <div>
            <input id="fileInput" name="fileInput" type="file" />
            <br />
            <input type="button" id="btnUpload" name="btnUpload" value="Upload" />
            <input type="button" id="btnClear" name="btnClear" value="Clear" />
        </div>
    </div>
    </body>
</html>
