﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using Mits.BLL.Authorization;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class Accounts : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public Accounts()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        private AccessRights AccessRightsAccounts
        {
            get
            {
                //Authorization.Authorization authorization = new Authorization.Authorization();
                return BLL.Authorization.Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.EntitySearch);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();

                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new MITSService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                string affiliateContactId = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId);
                if (affiliateContactId == "-1")
                {
                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                List<ReportParameter> reportParams = new List<ReportParameter>();
                ReportParameter rptParam1 = new ReportParameter("ContactId", affiliateContactId);
                reportParams.Add(rptParam1);

                //ReportParameter rptParam2 = new ReportParameter("State", string.IsNullOrEmpty(Request["StateId"]) ? aff.StateId.ToString() : Request["StateId"]);
                //ReportParameter rptParam3 = new ReportParameter("PlanYear", Request["PlanYear"] == "" ? null : Request["PlanYear"]);
                //reportParams.Add(rptParam1);
                //reportParams.Add(rptParam2);
                //reportParams.Add(rptParam3);

                
                //if (SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.Auditor
                //    && SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.Administrator
                //    && SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.Finance
                //    && SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant
                //    && SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.Manager
                //    )
                //{
                //    ReportParameter rptParam1 = new ReportParameter("SingleAffiliateId", SessionParameters.AffiliateId.ToString());
                //    reportParams.Add(rptParam1);
                //}
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Admin Account Receivable/Payable";

                ServerReportHelper.GetLoginInfo("/AccountingReport", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }

        }
    }
}
