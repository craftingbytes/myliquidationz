﻿using System;
using System.Collections.Generic;

using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using Mits.Common;

namespace Mits.Web.Reports
{
    public partial class RecyclerMonthReport : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public RecyclerMonthReport()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Recycler Month Report";

                ServerReportHelper.GetLoginInfo("/RecyclerMonthReport", rptviewer.ServerReport);

                int affiliatedRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                if (affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.Auditor
                    && affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.Administrator
                    && affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.Finance
                    && affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant
                    && affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.Manager
                    )
                {
                    ReportParameter rptParam1;
                    ReportParameter rptParam2;
                    List<ReportParameter> reportParams = new List<ReportParameter>();

                    rptParam1 = new ReportParameter("SingleAffiliateId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                    rptParam2 = new ReportParameter("RecyclerId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));

                    reportParams.Add(rptParam1);
                    reportParams.Add(rptParam2);
                    rptviewer.ServerReport.SetParameters(reportParams);
                }

                rptviewer.ServerReport.Refresh();

            }
        }
    }
}