﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using Mits.BLL.Authorization;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class GenericReport : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public GenericReport()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();

                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new MITSService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                ////ReportParameter rptParam1 = new ReportParameter("AffiliateId", string.IsNullOrEmpty(Request["AffiliateId"]) ? aff.Id.ToString() : Request["AffiliateId"]);
                //ReportParameter rptParam2 = new ReportParameter("State", string.IsNullOrEmpty(Request["StateId"]) ? aff.StateId.ToString() : Request["StateId"]);
                ////ReportParameter rptParam3 = new ReportParameter("PlanYear", Request["PlanYear"] == "" ? null : Request["PlanYear"]);


                //List<ReportParameter> reportParams = new List<ReportParameter>();
                ////reportParams.Add(rptParam1);
                //reportParams.Add(rptParam2);
                ////reportParams.Add(rptParam3);

                //if (SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.Auditor
                //    && SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.Administrator
                //    && SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.Finance
                //    && SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant
                //    && SessionParameters.AffiliateRoleId != (int)Common.Constants.AffiliateContactRole.Manager
                //    )
                //{
                //    ReportParameter rptParam1 = new ReportParameter("SingleAffiliateId", SessionParameters.AffiliateId.ToString());
                //    reportParams.Add(rptParam1);
                //}

                //rptviewer.ProcessingMode = ProcessingMode.Remote;
                //rptviewer.ServerReport.DisplayName = "Admin OEM Obligation Detail Report";
                //if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReportServerAuth"]) == true)
                //{
                //    string userName = ConfigurationManager.AppSettings["ReportUserName"].ToString();
                //    string pass = ConfigurationManager.AppSettings["ReportPassword"].ToString();
                //    string domain = ConfigurationManager.AppSettings["ReportDomain"].ToString();
                //    rptviewer.ServerReport.ReportServerCredentials = (IReportServerCredentials)new CustomReportCredentials(userName, pass, domain);
                    
                //    //rptviewer.ServerReport.ReportServerCredentials = (IReportServerCredentials)new CustomReportCredentials("xavor-zaighum", "Yazdan9492945900", "ewor01vm001");
                //}

                //rptviewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportRoot"]);
                //rptviewer.ServerReport.ReportPath = ConfigurationManager.AppSettings["SSRS.FolderPath"] + "/OEMTargetDetailReport";
                //rptviewer.ServerReport.SetParameters(reportParams);
               
                //rptviewer.ServerReport.Refresh();


                String reportName = Request["ReportName"];
                String displayName = Request["DisplayName"];


                rptviewer.ShowPrintButton = false;
                rptviewer.Reset();
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.SizeToReportContent = true;
                if (displayName == null)
                    rptviewer.ServerReport.DisplayName = reportName;
                else
                    rptviewer.ServerReport.DisplayName = displayName;

                ServerReportHelper.GetLoginInfo("/" + reportName, rptviewer.ServerReport);

                List<ReportParameter> newReportParams = new List<ReportParameter>();

                ReportParameterInfoCollection reportParams = rptviewer.ServerReport.GetParameters();
                foreach (ReportParameterInfo reportParam in reportParams)
                {

                    //do something
                    string parametername = reportParam.Name;
                    if (reportParam.State == ParameterState.MissingValidValue)
                    {
                        string paramName = reportParam.Name;
                        if (paramName == "AffiliateId")
                        {
                            ReportParameter newParam = new ReportParameter("AffiliateId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                            newReportParams.Add(newParam);
                        }
                        else
                        {
                            string requestParam = Request[paramName];
                            if (!string.IsNullOrEmpty(requestParam))
                            {
                                ReportParameter newParam = new ReportParameter(paramName, requestParam);
                                newReportParams.Add(newParam);
                            }
                        }
                    }
                }

                rptviewer.ServerReport.SetParameters(newReportParams);
                rptviewer.ServerReport.Refresh();
            }
        }
    }
}