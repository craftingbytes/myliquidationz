﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using Mits.BLL.Authorization;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class OEMTargetReportObligationStatus_OEMView : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public OEMTargetReportObligationStatus_OEMView()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();
                rptviewer.SizeToReportContent = true;
                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new MITSService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                //ReportParameter rptParam1 = new ReportParameter("AffiliateId", _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId).ToString());
                ReportParameter rptParam2 = new ReportParameter("State", string.IsNullOrEmpty(Request["StateId"]) ? aff.StateId.ToString() : Request["StateId"]);
                //ReportParameter rptParam3 = new ReportParameter("PlanYear", Request["PlanYear"] == "" ? null : Request["PlanYear"]);

                List<ReportParameter> reportParams = new List<ReportParameter>();
                //reportParams.Add(rptParam1);
                reportParams.Add(rptParam2);
                //reportParams.Add(rptParam3);

                int affiliatedRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                if (affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.Auditor
                    && affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.Administrator
                    && affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.Finance
                    && affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant
                    && affiliatedRoleId != (int)Common.Constants.AffiliateContactRole.Manager
                    )
                {
                    ReportParameter rptParam1 = new ReportParameter("SingleAffiliateId", _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId).ToString());
                    reportParams.Add(rptParam1);
                }


                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Detailed Report";

                ServerReportHelper.GetLoginInfo("/OEMTargetReportObligationStatus_OEMView", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}