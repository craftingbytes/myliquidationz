﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;

namespace Mits.Web.Reports
{
    public partial class RecyclerInvoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReportParameter rptParam = new ReportParameter("InvoiceID", Request["InvoiceID"]);
                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(rptParam);
                rptviewer.Reset();
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Recycler Invoice Report";

                ServerReportHelper.GetLoginInfo("/RecyclerInvoicesReport", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);
                rptviewer.ServerReport.Refresh();
            }
        }
    }
}