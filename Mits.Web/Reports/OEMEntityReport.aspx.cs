﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class OEMEntityReport : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public OEMEntityReport()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();
                rptviewer.SizeToReportContent = true;
                ReportParameter rptParamEntityID = new ReportParameter("EntityID", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                ReportParameter rptParamEntity = new ReportParameter("Entity", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateTypeId));
                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(rptParamEntityID);
                reportParams.Add(rptParamEntity);
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "OEM Account Summary ";

                ServerReportHelper.GetLoginInfo("/OEMEntityReport", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}