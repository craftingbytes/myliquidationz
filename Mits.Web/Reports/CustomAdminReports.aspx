﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomAdminReports.aspx.cs" Inherits="Mits.Web.Reports.CustomAdminReports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" style="height: 100%">
    <title></title>
</head>
<body style="height:100%">
    <form id="form1" runat="server" style="height: 100%">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <asp:DropDownList ID="lstReport" runat="server" AutoPostBack="True" 
        onselectedindexchanged="lstReport_SelectedIndexChanged" Width="213px">
        <asp:ListItem Value="0">Select</asp:ListItem>
        <asp:ListItem Value="1">Report1</asp:ListItem>
        <asp:ListItem Value="2">Report2</asp:ListItem>
    </asp:DropDownList>

    <rsweb:ReportViewer ID="rptviewer" runat="server" Width="100%"  height="1930px" 
        style="margin-top: 7px" >
    </rsweb:ReportViewer>

       
    </form>
</body>
</html>
