﻿using System;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using Mits.BLL.Authorization;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class ProcessingReportDetailByState : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public ProcessingReportDetailByState()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        private AccessRights AccessRightsAccounts
        {
            get
            {
                //Authorization.Authorization authorization = new Authorization.Authorization();
                return BLL.Authorization.Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.ProcessingReportDetail);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();
                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new MITSService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                ReportParameter rptParam2 = new ReportParameter("StateId", string.IsNullOrEmpty(Request["StateId"]) ? aff.StateId.ToString() : Request["StateId"]);
                ReportParameter rptParam3 = new ReportParameter("StartDateTime", string.IsNullOrEmpty(Request["StartDateTime"]) ? "2009-01-01" : Request["StartDateTime"]);
                ReportParameter rptParam4 = new ReportParameter("EndDateTime", string.IsNullOrEmpty(Request["EndDateTime"]) ? DateTime.Now.ToString("yyyy-MM-dd") : Request["EndDateTime"]);

                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(rptParam2);
                reportParams.Add(rptParam3);
                reportParams.Add(rptParam4);
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Processing Report - Detail";

                ServerReportHelper.GetLoginInfo("/ProcessingReportDetailByState", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);
                rptviewer.ServerReport.Refresh();

            }
        }
    }
}