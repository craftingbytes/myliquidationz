﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class EntitySubReport : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public EntitySubReport()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReportParameter rptParam = new ReportParameter("Entity", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateTypeId));
                ReportParameter rptParam2 = new ReportParameter("EntityID", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(rptParam);
                reportParams.Add(rptParam2);
                rptviewer.Reset();
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Processor Plan Year Comparsion Report";

                ServerReportHelper.GetLoginInfo("/RecEntityTimeComparisonReport", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);
                rptviewer.ServerReport.Refresh();
            }
        }
    }
}