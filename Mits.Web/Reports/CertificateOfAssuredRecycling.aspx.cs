﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;
namespace Mits.Web.Reports
{
    public partial class CertificateOfAssuredRecycling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                rptviewer.Reset();
                //ReportParameter rptParam = new ReportParameter("OEM",SessionParameters.AffiliateId.ToString());

                //List<ReportParameter> reportParams = new List<ReportParameter>();
                //reportParams.Add(rptParam);
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Entity Certificate";

                ServerReportHelper.GetLoginInfo("/CertificateOfAssuredRecyclingOrProcessAdminReport", rptviewer.ServerReport);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}