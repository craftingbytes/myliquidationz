﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;

namespace Mits.Web.Reports
{
    public partial class AdminUnAssignedWeightReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Total unassigned Weight per State per Recycler ";

                ServerReportHelper.GetLoginInfo("/AdminUnAssignedWeightReport", rptviewer.ServerReport);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}