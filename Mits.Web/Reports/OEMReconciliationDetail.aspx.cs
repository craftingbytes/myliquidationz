﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using Mits.BLL.Authorization;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class OEMReconciliationDetail : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public OEMReconciliationDetail()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();

                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new MITSService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                string affiliateContactId = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId);
                if (affiliateContactId == "-1")
                {
                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                List<ReportParameter> reportParams = new List<ReportParameter>();
                ReportParameter rptParam1 = new ReportParameter("ContactId", affiliateContactId);
                reportParams.Add(rptParam1);
                
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.SizeToReportContent = true;
                rptviewer.ServerReport.DisplayName = "OEM Reconciliation Detail Report";

                ServerReportHelper.GetLoginInfo("/OEMReconcileDetailReport", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}