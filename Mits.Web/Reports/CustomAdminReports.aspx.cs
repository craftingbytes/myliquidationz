﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;

namespace Mits.Web.Reports
{
    public partial class CustomAdminReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ReportParameter rptParam = new ReportParameter("InvoiceID", Request["InvoiceID"]);
                //List<ReportParameter> reportParams = new List<ReportParameter>();
                //reportParams.Add(rptParam);
                //rptviewer.Reset();
                //rptviewer.ProcessingMode = ProcessingMode.Remote;
                //rptviewer.ServerReport.DisplayName = "OEM Invoice Report";
                //if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReportServerAuth"]) == true)
                //{
                //    string userName = ConfigurationManager.AppSettings["ReportUserName"].ToString();
                //    string pass = ConfigurationManager.AppSettings["ReportPassword"].ToString();
                //    string domain = ConfigurationManager.AppSettings["ReportDomain"].ToString();
                //    rptviewer.ServerReport.ReportServerCredentials = (IReportServerCredentials)new CustomReportCredentials(userName, pass, domain);
                    
                //    //rptviewer.ServerReport.ReportServerCredentials = (IReportServerCredentials)new CustomReportCredentials("xavor-zaighum", "Yazdan9492945900", "ewor01vm001");
                //}

                //rptviewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportRoot"]);
                //rptviewer.ServerReport.ReportPath = ConfigurationManager.AppSettings["SSRS.FolderPath"] + "/OEMInvoiceReport";
                //rptviewer.ServerReport.SetParameters(reportParams);
                //rptviewer.ServerReport.Refresh();
            }
        }

        protected void lstReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            rptviewer.Reset();
            rptviewer.SizeToReportContent = true;
            rptviewer.ProcessingMode = ProcessingMode.Remote;
            rptviewer.ServerReport.DisplayName = "OEM Reconciliation Detail Report";

            ServerReportHelper.GetLoginInfo("/CertificateOfAssuredRecyclingOrProcessAdminReport", rptviewer.ServerReport);

            rptviewer.ServerReport.Refresh();
        }

 
    }
}