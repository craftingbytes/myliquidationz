﻿using System;
using System.Configuration;
using Mits.Common;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
//using RdlcRptGen; //TODO: commented out to get it to compile


namespace Mits.Web.Reports
{
    public partial class GenericReport2 : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public GenericReport2()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReportViewer1.Reset();

                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new MITSService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }


                //String reportName = Request["ReportName"];
                //String displayName = Request["DisplayName"];


                //rptviewer.ShowPrintButton = false;
                //rptviewer.Reset();
                //rptviewer.ProcessingMode = ProcessingMode.Remote;

                //if (displayName == null)
                //    rptviewer.ServerReport.DisplayName = reportName;
                //else
                //    rptviewer.ServerReport.DisplayName = displayName;


                //if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReportServerAuth"]) == true)
                //{
                //    string userName = ConfigurationManager.AppSettings["ReportUserName"].ToString();
                //    string pass = ConfigurationManager.AppSettings["ReportPassword"].ToString();
                //    string domain = ConfigurationManager.AppSettings["ReportDomain"].ToString();
                //    rptviewer.ServerReport.ReportServerCredentials = (IReportServerCredentials)new CustomReportCredentials(userName, pass, domain);

                //}
                //rptviewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportRoot"]);
                //rptviewer.ServerReport.ReportPath = ConfigurationManager.AppSettings["SSRS.FolderPath"] + "/" + reportName;


                //List<ReportParameter> newReportParams = new List<ReportParameter>();

                //ReportParameterInfoCollection reportParams = rptviewer.ServerReport.GetParameters();
                //foreach (ReportParameterInfo reportParam in reportParams)
                //{

                //    //do something
                //    string parametername = reportParam.Name;
                //    if (reportParam.State == ParameterState.MissingValidValue)
                //    {
                //        string paramName = reportParam.Name;
                //        if (paramName == "AffiliateId")
                //        {
                //            ReportParameter newParam = new ReportParameter("AffiliateId", SessionParameters.AffiliateId.ToString());
                //            newReportParams.Add(newParam);
                //        }
                //        else
                //        {
                //            string requestParam = Request[paramName];
                //            ReportParameter newParam = new ReportParameter(paramName, requestParam);
                //            newReportParams.Add(newParam);
                //        }
                //    }
                //}

                //rptviewer.ServerReport.SetParameters(newReportParams);
                //rptviewer.ServerReport.Refresh();
            }
        }

        public DataSet GetDataSet(string ConnectionString, string SQL)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = SQL;
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();

            conn.Open();
            da.Fill(ds);
            conn.Close();

            return ds;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string conn = ConfigurationManager.ConnectionStrings["MITSReportReader"].ConnectionString;
            //Label1.Text = txtboxSql.Text;

            try
            {
                var ReportDS = GetDataSet(conn, txtboxSql.Text);
                //TODO: commented out to get it to compile
                //var CrtGen = new RdlcGenClass(ReportDS.Tables[0]);
                //CrtGen.AddReportHeader("");
                //CrtGen.ShowReport(ReportViewer1);
            }
            catch (Exception ex)
            {
                Label1.Text = ex.Message;
            }
        }
    }
}