﻿#define LOCAL_DEBUG
//#undef LOCAL_DEBUG

using Microsoft.Reporting.WebForms;
using System;
using System.Configuration;

namespace Mits.Web.Reports
{
    public class ServerReportHelper
    {
        public static void GetLoginInfo(in string folder, ServerReport report)
        {
            report.ReportServerCredentials = null;

            string userName = null;
            string pass = null;
            string domain = null;

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReportServerAuth"]) == true)
            {
#if (DEBUG && LOCAL_DEBUG)
                string base64Encoded = ConfigurationManager.AppSettings["ReportDevCreds"].ToString();
                byte[] data = System.Convert.FromBase64String(base64Encoded);
                string base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);
                char[] seperator = { ':' };
                string[] creds = base64Decoded.Split(seperator);
                if (creds.Length == 2 && !string.IsNullOrEmpty(creds[0]) && !string.IsNullOrEmpty(creds[1]))
                {
                    userName = creds[0];
                    pass = creds[1];
                    domain = ConfigurationManager.AppSettings["ReportDevDomain"].ToString();
                }
#else
                userName = ConfigurationManager.AppSettings["ReportUserName"].ToString();
                pass = ConfigurationManager.AppSettings["ReportPassword"].ToString();
                domain = ConfigurationManager.AppSettings["ReportDomain"].ToString();
#endif
                report.ReportServerCredentials = (IReportServerCredentials)new CustomReportCredentials(userName, pass, domain);
            }
#if (DEBUG && LOCAL_DEBUG)
            report.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportDevRoot"]);
            report.ReportPath = ConfigurationManager.AppSettings["SSRS.DevFolderPath"] + folder;
#else
            report.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportRoot"]);
            report.ReportPath = ConfigurationManager.AppSettings["SSRS.FolderPath"] +  folder;
#endif
        }
    }
}