﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class DetailedProcessingReport : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public DetailedProcessingReport()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();
                ReportParameter rptParam1 = new ReportParameter("AffiliateId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                ReportParameter rptParam2 = new ReportParameter("StateId", Request["StateId"] == "" ? null : Request["StateId"]);
                ReportParameter rptParam3 = new ReportParameter("PlanYear", Request["PlanYear"] == "" ? null : Request["PlanYear"]);
                ReportParameter rptParam4 = new ReportParameter("StartDate", Request["StartDate"] == "" ? null : Request["StartDate"]);
                ReportParameter rptParam5 = new ReportParameter("EndDate", Request["EndDate"] == "" ? null : Request["EndDate"]);

                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(rptParam1);
                reportParams.Add(rptParam2);
                reportParams.Add(rptParam3);
                reportParams.Add(rptParam4);
                reportParams.Add(rptParam5);
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Detailed Report";

                ServerReportHelper.GetLoginInfo("/DetailedProcessingReport", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}