﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using Mits.BLL.Authorization;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class GenericReportNoToolbar : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public GenericReportNoToolbar()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptviewer.Reset();

                //Affiliate aff = new MITSService<Affiliate>().GetSingle(a => a.Id == SessionParameters.AffiliateId);

                //if (aff == null)
                //{

                //    Response.RedirectToRoute("AccessDenied", "Home");
                //    return;
                //}



                String reportName = Request["ReportName"];
                String displayName = Request["DisplayName"];


                rptviewer.ShowPrintButton = false;
                rptviewer.Reset();
                rptviewer.SizeToReportContent = true;
                rptviewer.ProcessingMode = ProcessingMode.Remote;

                if (displayName == null)
                    rptviewer.ServerReport.DisplayName = reportName;
                else
                    rptviewer.ServerReport.DisplayName = displayName;

                ServerReportHelper.GetLoginInfo("/" + reportName, rptviewer.ServerReport);

                List<ReportParameter> newReportParams = new List<ReportParameter>();

                ReportParameterInfoCollection reportParams = rptviewer.ServerReport.GetParameters();
                foreach (ReportParameterInfo reportParam in reportParams)
                {

                    //do something
                    string parametername = reportParam.Name;
                    if (reportParam.State == ParameterState.MissingValidValue)
                    {
                        string paramName = reportParam.Name;
                        if (paramName == "AffiliateId")
                        {
                            ReportParameter newParam = new ReportParameter("AffiliateId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                            newReportParams.Add(newParam);
                        }
                        else
                        {
                            string requestParam = Request[paramName];
                            ReportParameter newParam = new ReportParameter(paramName, requestParam);
                            newReportParams.Add(newParam);
                        }
                    }
                }

                rptviewer.ServerReport.SetParameters(newReportParams);
                rptviewer.ServerReport.Refresh();
            }
        }
    }
}