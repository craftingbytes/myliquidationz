﻿using System;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;
using Mits.Common;
using System.Web.Mvc;

namespace Mits.Web.Reports
{
    public partial class ProcessRecyclerReport : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;

        public ProcessRecyclerReport()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                rptviewer.Reset();
                rptviewer.SizeToReportContent = true;
                ReportParameter rptParam = new ReportParameter("RecyclerName", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));

                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(rptParam);

                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Recycler Certificate";

                ServerReportHelper.GetLoginInfo("/CertificateOfAssuredRecyclingOrProcessRecyclerReport", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}