﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using System.Configuration;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class StatePolicyController : MITSApplicationController
    {

      private StatePolicyBO _statePolicyBO;

      private AccessRights AccessRightsStatePolicy
      {
          get
          {
              //Authorization authorization = new Authorization();
              return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.StatePolicy);
          }
      }
        public StatePolicyController(ISessionCookieValues sessionValue)
            : this(sessionValue, new StatePolicyBO())
        {
        }
      
        public StatePolicyController(ISessionCookieValues sessionValue, StatePolicyBO statePolicyBO)
            : base(sessionValue)
        {
            _statePolicyBO = statePolicyBO;                   
        }
        
        
        //
        // GET: /StatePolicy/
      
        public ActionResult Index()
        {
            if (AccessRightsStatePolicy != null && AccessRightsStatePolicy.Read.HasValue == true && AccessRightsStatePolicy.Read == true)
            {
                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                    State _stateObj = new State();
                    return View(new StatePolicyViewModel(_stateObj));
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");

                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");

            }

        }

        //Getting all policies against a particular state and sending response back to the view
        
        public ActionResult GetPolicies(string state)
        {
            int _stateId = int.Parse(state);
            var data = new MITSService<StatePolicy>().GetAll(x => x.StateId == _stateId & x.Active == true);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.PolicyId,


                    }).ToArray()
            };
            return Json(jsonData);

        }

        //Invokes 'Add' function of StatePolicyBO and send response back to the view

        public ActionResult Save(string state, string policies)
        {

            if (AccessRightsStatePolicy != null && AccessRightsStatePolicy.Add.HasValue == true && AccessRightsStatePolicy.Add == true || (AccessRightsStatePolicy.Update.HasValue == true && AccessRightsStatePolicy.Update == true))
            {


            Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();

            try
            {
                if (int.Parse(state)== 0)
                {
                    return Json(new
                {
                    success = false,
                    message = ConfigurationManager.AppSettings["RecordNotSaved"],
                });
                }
                else
                {


                    _returnDictionaryValue = _statePolicyBO.Add(state, policies);
                }


            }
            catch (Exception ex)
            {
                string log = ex.Message;
                return Json(new
                {
                    success = false,
                    message = ConfigurationManager.AppSettings["RecordNotSaved"],
                });
            }
            if (_returnDictionaryValue["duplicate"] == 0)
            {
                var data = _statePolicyBO.GetAll();
                var jsonData = new
                {
                    success = true,
                    message = ConfigurationManager.AppSettings["RecordSaved"],
                    selected = _returnDictionaryValue["selectedId"].ToString(),
                    data = (
                        from m in data
                        select new
                        {
                            Value = m.Id,
                            Text = m.Name,

                        }).ToArray()
                };
                return Json(jsonData);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                });
            }
         }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });

            }

        }
  


    }
}
