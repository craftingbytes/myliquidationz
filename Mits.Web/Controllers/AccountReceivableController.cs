﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class AccountReceivableController : MITSApplicationController
    {
        private AccessRights AccessRightsPayment
        {
            get
            {
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Receivable);
            }
        }

        private AccountReceivableBO accountReceivableBO;

        private Int32? CurrentPaymentID
        {
            get
            {
                int id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.CurrentPaymentID);
                return (id > 0) ? (Int32?)id : null;
            }
            set
            {
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.CurrentPaymentID, (value.HasValue && value.Value > 0) ? value.Value : -1);
            }
        }

        private Int32? CurrentAffiliateID
        {
            get
            {
                int id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.CurrentAffiliateID);
                return (id > 0) ? (Int32?)id : null;
            }
            set
            {
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.CurrentAffiliateID, (value.HasValue && value.Value > 0) ? value.Value : -1);
            }
        }

        private Payment CurrentPayment
        {
            get;
            set;
        }

        public AccountReceivableController(ISessionCookieValues sessionValues)
            : this(sessionValues, new AccountReceivableBO())
        {
            accountReceivableBO = new AccountReceivableBO();
        }

        public AccountReceivableController(ISessionCookieValues sessionValues, AccountReceivableBO accountReceivableBO)
            : base(sessionValues)
        {
            this.accountReceivableBO = accountReceivableBO;
        }

        public ActionResult Index()
        {
            if (AccessRightsPayment != null && AccessRightsPayment.Read.HasValue == true && AccessRightsPayment.Read == true)
            {
                ViewData["receivables"] = accountReceivableBO.GetReceivables();
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }

        }

        [HttpGet]
        public ActionResult GetReceivables(int page, int rows, string searchReceivable, string searchDateTo, string searchDateFrom)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = accountReceivableBO.GetReceivablesPaged(pageIndex, pageSize, searchReceivable, searchDateTo, searchDateFrom, out totalRecords).AsEnumerable<Payment>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        cell = new object[] { 
                            m.Id,
                             (m.Affiliate.Name==null)?"":m.Affiliate.Name,
                             String.Format("{0:MM/dd/yyyy}",m.Date),
                           (m.PaymentType.Name==null)?"":m.PaymentType.Name,
                            m.ReferenceNumber,
                            String.Format("{0:#,0.0000}",m.Amount),
                            
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(Int32? id, bool? isSaved) 
        {
            CurrentPaymentID = null;
            if (AccessRightsPayment != null && AccessRightsPayment.Read.HasValue == true && AccessRightsPayment.Read == true)
            {
                ViewData["AccessRights"] = AccessRightsPayment;
                if (id.HasValue)
                {
                    CurrentPaymentID = id.Value;
                    CurrentPayment = accountReceivableBO.GetPaymentByID(id.Value);
                }

                if (CurrentPayment == null)
                    CurrentPayment = accountReceivableBO.CreateNewPayment(0);


                if (isSaved.HasValue && isSaved.Value)
                    return View(new AccountReceivableViewModel(CurrentPayment) { Message = "Account receivable payment has been saved successfully." });
                else
                    return View(new AccountReceivableViewModel(CurrentPayment) { Message = null });
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }
        
        [HttpPost]
        public ActionResult Edit(Payment payment)
        {
            if (AccessRightsPayment != null && AccessRightsPayment.Read.HasValue == true && AccessRightsPayment.Read == true)
            {
                if (CurrentPaymentID.HasValue)
                    CurrentPayment = accountReceivableBO.GetPaymentByID(CurrentPaymentID.Value);
                else
                    CurrentPayment = accountReceivableBO.CreateNewPayment(CurrentAffiliateID.Value);

                if (CurrentPayment.Id == 0)
                    CurrentPayment.AffiliateId = payment.AffiliateId;
                CurrentPayment.Amount = payment.Amount;
                CurrentPayment.Date = payment.Date;
                CurrentPayment.Note = payment.Note;
                CurrentPayment.PaymentTypeID = payment.PaymentTypeID;
                CurrentPayment.ReferenceNumber = payment.ReferenceNumber;


                string strPaymentDetails = Request.Form["txtPaymentDetails"];

                Int32 paymentDetailID = 0, invoiceID = 0;
                decimal paymentAmount = 0;


                foreach (string strPaymentDetail in strPaymentDetails.Split(';'))
                {
                    if (strPaymentDetail != "")
                    {
                        paymentDetailID = Convert.ToInt32(strPaymentDetail.Split(':')[0]);
                        invoiceID = Convert.ToInt32(strPaymentDetail.Split(':')[1]);
                        paymentAmount = Convert.ToDecimal(strPaymentDetail.Split(':')[2]);

                        PaymentDetail _paymentDetail = CurrentPayment.PaymentDetails.Where(pd => pd.Id == paymentDetailID && pd.InvoiceId == invoiceID).FirstOrDefault();
                        if (_paymentDetail != null)
                            _paymentDetail.PaidAmount = paymentAmount;
                    }
                }

                bool _statusFlag = false;
                if (CurrentPayment.Id > 0)
                    _statusFlag = accountReceivableBO.UpdatePayment(CurrentPayment);
                else
                    _statusFlag = accountReceivableBO.AddPayment(CurrentPayment);

                if (_statusFlag)
                {
                    return RedirectToAction("Edit", new { id = CurrentPayment.Id.ToString(), isSaved = true });
                }
                else
                {
                    return View("Edit", new AccountReceivableViewModel(CurrentPayment) { Message = "Account recieveable payment has not been saved successfully." });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult GetAllUnPaidInvoices(int? id, int page, int rows, string sord, string sidx)
        {
            CurrentAffiliateID = id;
            if (CurrentPaymentID.HasValue)
                CurrentPayment = accountReceivableBO.GetPaymentByID(CurrentPaymentID.Value);
            else
                CurrentPayment = accountReceivableBO.CreateNewPayment(CurrentAffiliateID.Value);
            return GetGridJsonData(page, rows, sord, sidx);
        }

        public JsonResult GetGridJsonData(int page, int rows, string sord, string sidx)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = CurrentPayment.PaymentDetails.Count;
            int counter = pageIndex * pageSize;

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,

                rows = (
                   from m in CurrentPayment.PaymentDetails

                   select new
                   {
                       id = counter++,
                       cell = new object[] { 
                           m.Id,
                           m.InvoiceId,
                           (m.Invoice.InvoiceDueDate.HasValue)?Convert.ToDateTime(m.Invoice.InvoiceDueDate.Value).ToString("MM/dd/yyyy"):string.Empty,
                           accountReceivableBO.GetInvoiceAmount(m.InvoiceId.Value),
                            accountReceivableBO.GetInvoicePaidAmount(m.InvoiceId.Value),
                           accountReceivableBO.GetInvoiceAmount(m.InvoiceId.Value)-accountReceivableBO.GetInvoicePaidAmount(m.InvoiceId.Value),
                           m.PaidAmount.HasValue?m.PaidAmount.Value:0,
                           m.PaidAmount.HasValue?m.PaidAmount.Value:0
                       }
                   }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public bool CheckPaymentIsValid(Invoice invoice, decimal? paidAmount, decimal payment)
        {
            if (!paidAmount.HasValue)
                paidAmount = 0;
            return accountReceivableBO.GetInvoiceAmount(invoice.Id) >= (accountReceivableBO.GetInvoicePaidAmount(invoice.Id) + payment);
        }

        public ActionResult GetUserAccessRights()
        {
            var jsonData = new
            {
                showEdit = AccessRightsPayment.Update.HasValue && AccessRightsPayment.Update.Value,
                showDelete = AccessRightsPayment.Delete.HasValue && AccessRightsPayment.Delete.Value
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePayment(Int32 id)
        {
            string msg = accountReceivableBO.DeletePayment(id);
            //string msg = "Fail";
            //string msg = "Success";
            return Content(msg);
        }

       
            
    }
}
