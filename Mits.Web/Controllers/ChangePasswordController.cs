﻿using Mits.BLL;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.DAL.EntityModels;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace Mits.Web.Controllers
{
    public class ChangePasswordController : MITSApplicationController
    {


        private ChangePasswordBO _changePasswordBO;

      
        
         public ChangePasswordController(ISessionCookieValues sessionValue)
            : this(sessionValue, new ChangePasswordBO(sessionValue))
        {
        }

         public ChangePasswordController(ISessionCookieValues sessionValue, ChangePasswordBO changePasswordBO)
            : base(sessionValue)
        {
            _changePasswordBO = changePasswordBO;                   
        }

        //
        // GET: /ChangePassword/

        public ActionResult Index()
        {
            string affiliateType = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
            if (affiliateType != string.Empty && affiliateType != "-1" )
            {
                int affiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                 AffiliateContact _aff = new MITSService<AffiliateContact>().GetSingle(a=> a.Id == affiliateContactId);
                 ViewData["Question"] = new SelectList(_changePasswordBO.GetAllQuestions(), "id", "Question", _aff.SecurityQuestionId);
                 ViewData["Answer"] = _aff.Answer;
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult SaveCriteria1(string oldPassword, string newPassword, string confirmPassword, string questionId, string answer)
        {
            string affiliateType = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
            if (affiliateType != string.Empty && affiliateType != "-1")
            {

                string _retrunValue;

                try
                {
                    _retrunValue = _changePasswordBO.AddCriteria3(oldPassword, newPassword, confirmPassword, questionId, answer);
                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }
                return Json(new
                     {
                         success = true,
                         message = _retrunValue,
                     });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult SaveCriteria2(string _answer, string _questionId)
        {
            string affiliateType = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
            if (affiliateType != string.Empty && affiliateType != "-1")
            {

                string _retrunValue;

                try
                {
                    _retrunValue = _changePasswordBO.AddCriteria2(_answer, _questionId);
                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }
                return Json(new
                {
                    success = true,
                    message = _retrunValue,


                });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
    }
}
