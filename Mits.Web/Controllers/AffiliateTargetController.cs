﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.BusinessObjects;
using Mits.DAL.EntityModels;
using Mits.BLL;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class AffiliateTargetController : MITSApplicationController
    {
       //
        // GET: /AffiliateTarget/
        private AffiliateTargetBO affiliateTargetBO;
        AffiliateTarget affiliateTarget = null;
       
        public AffiliateTargetController(ISessionCookieValues sessionValues)
            : this(sessionValues, new AffiliateTargetBO())
        {
            affiliateTargetBO = new AffiliateTargetBO();
        }

        public AffiliateTargetController(ISessionCookieValues sessionValues, AffiliateTargetBO affiliateTargetBO)
            : base(sessionValues)
        {
            this.affiliateTargetBO = affiliateTargetBO;
        }
        private AccessRights AccessRights
        {
            get
            {
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Target);
            }
        }

        private Affiliate SelectedAffiliate
        {
            get 
            {
                return new AffiliateBO().GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId)); 
            }
        }

        public ActionResult Index(int? id)
        {
            if (AccessRights != null && AccessRights.Read.HasValue == true && AccessRights.Read == true)
            {
                ViewData["AccessRights"] = AccessRights;
                string affiliateType = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
                if (affiliateType == Constants.AffiliateType.Eworld.ToString() && id.HasValue)
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, id.Value);
                else if (affiliateType != Constants.AffiliateType.Eworld.ToString())
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));

                ViewData["SelectedAffiliate"] = SelectedAffiliate;
                ViewData["Data"] = Request["data"];
                return View();
            }
            else
            {
                

                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public string EditAffiliateTarget(FormCollection col)
        {
            string _statusMessage = "";
            bool _status = true;
            if (col.AllKeys.Contains("oper"))
            {
                if (col["oper"] == "add")
                {
                    affiliateTarget = new AffiliateTarget();
                    affiliateTarget.StateId = Convert.ToInt32(col["State"]);
                    affiliateTarget.PolicyId = Convert.ToInt32(col["Policy"]);
                    affiliateTarget.ServiceTypeId = Convert.ToInt32(col["ServiceType"]);
                    affiliateTarget.QuantityTypeId = Convert.ToInt32(col["QuantityType"]);
                    affiliateTarget.Pace = Convert.ToInt32(col["Pace"]);
                    affiliateTarget.Weight = Convert.ToDecimal(col["Weight"]);
                    affiliateTarget.PlanYear = Convert.ToInt32(col["Planyear"]);
                    PlanYear py = new MITSService<PlanYear>().GetAll(x => x.StateId == affiliateTarget.StateId && x.Year == affiliateTarget.PlanYear).First();
                    //affiliateTarget.StartDate = Convert.ToDateTime(col["StartDate"]);
                    //affiliateTarget.EndDate = Convert.ToDateTime(col["EndDate"]);
                    affiliateTarget.StartDate = Convert.ToDateTime(py.StartDate.Value);
                    affiliateTarget.EndDate = Convert.ToDateTime(py.EndDate.Value);
                    affiliateTarget.IsPermanentDropOffLocation = Convert.ToBoolean(col["Permanent"]);
                    affiliateTarget.AffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                    if (!affiliateTargetBO.HasAffiliateContrateRate(affiliateTarget))
                    {
                        _statusMessage = "No Affiliate Contract Rate";
                        _status = false;
                    }
                    else if (affiliateTargetBO.IsAlreadyExist(affiliateTarget))
                    {
                        _statusMessage = "The Date criteria is overlapping with another target.Please change criteria or date range";
                        _status = false;
                    }
                    else if (affiliateTargetBO.AddAffiliateTarget(affiliateTarget))
                    {
                        _statusMessage = "Target has been added successfully.";
                        _status = true;
                    }
                    else
                    {
                        _statusMessage = "Unable to add Target.";
                        _status = true;
                    }
                }
                else if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    affiliateTarget = affiliateTargetBO.GetAffiliateTargetByID(Convert.ToInt32(col["id"]));
                    if (affiliateTarget != null)
                    {
                        affiliateTarget.StateId = Convert.ToInt32(col["State"]);
                        affiliateTarget.PolicyId = Convert.ToInt32(col["Policy"]);
                        affiliateTarget.ServiceTypeId = Convert.ToInt32(col["ServiceType"]);
                        affiliateTarget.QuantityTypeId = Convert.ToInt32(col["QuantityType"]);
                        affiliateTarget.Pace = Convert.ToInt32(col["Pace"]);
                        affiliateTarget.Weight = Convert.ToDecimal(col["Weight"]);
                        affiliateTarget.PlanYear = Convert.ToInt32(col["Planyear"]);
                        PlanYear py = new MITSService<PlanYear>().GetAll(x => x.StateId == affiliateTarget.StateId && x.Year == affiliateTarget.PlanYear).First();
                        //affiliateTarget.StartDate = Convert.ToDateTime(col["StartDate"]);
                        //affiliateTarget.EndDate = Convert.ToDateTime(col["EndDate"]);
                        affiliateTarget.StartDate = Convert.ToDateTime(py.StartDate.Value);
                        affiliateTarget.EndDate = Convert.ToDateTime(py.EndDate.Value);
                        affiliateTarget.IsPermanentDropOffLocation = Convert.ToBoolean(col["Permanent"]);
                        if (affiliateTargetBO.IsAlreadyExist(affiliateTarget))
                        {
                            _statusMessage = "The Date criteria is overlapping with another target.Please change criteria or date range";
                            _status = false;
                        }
                        else if (affiliateTargetBO.UpdateAffiliateTarget(affiliateTarget))
                        {
                            _statusMessage = "Target has been updated successfully.";
                            _status = true;
                        }
                        else
                        {
                            _statusMessage = "Unable to update Target.";
                            _status = true;
                        }
                    }
                }
                else if (col["oper"] == "del" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    if (affiliateTargetBO.DeleteAffiliateTarget(Convert.ToInt32(col["id"])))
                        {
                            _statusMessage = "Target has been deleted successfully.";
                            _status = true;
                        }
                        else
                        {
                            _statusMessage = "Unable to delete Target.";
                            _status = true;
                        }
                }
            }
            return _status.ToString() + ":" + _statusMessage;
        }

        public ActionResult GetAllAffiliateTargets(int page, int rows, string sord, string sidx)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = 0;
            var data = affiliateTargetBO.GetAllAffiliateTarget(pageIndex, pageSize, out totalRecords, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,

                rows = (
                    from m in data

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                           m.Id,
                            m.State.Name,
                            m.Policy.Name,
                            m.ServiceType.Name,
                            m.QuantityType.Name, 
                            m.Pace == null ? 100:m.Pace,
                            m.Weight,
                            (m.StartDate.HasValue)?Convert.ToDateTime(m.StartDate.Value).ToString("MM/dd/yyyy"):string.Empty,
                            (m.EndDate.HasValue)?Convert.ToDateTime(m.EndDate.Value).ToString("MM/dd/yyyy"):string.Empty,
                            m.PlanYear.HasValue?m.PlanYear.Value:0,
                            m.IsPermanentDropOffLocation.HasValue? m.IsPermanentDropOffLocation.Value : false,
                            "<a onclick='ShowProductTypesDialog("+m.Id +",\""+GetAffiliateTargetProductsIds(m)+"\")' style='cursor:hand;'>View</a>"
                        }
                    }).ToArray()
            };

            

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public string GetAffiliateTargetProductsIds(AffiliateTarget affiliateTarget) 
        {
            string ids = "";
            foreach (Int32 id in affiliateTarget.AffiliateTargetProducts.Select(p => p.ProductTypeId))
            {
                ids += id.ToString() + ",";
            }
            return ids.TrimEnd(',');
        }

        public ActionResult GetDropDownsData()
        {
            string strStates = ":Select;", strPlanyears = ":Select;", strPolicies = ":Select;", strServiceTypes = ":Select;", strQuantityTypes = ":Select;";

            foreach (State s in affiliateTargetBO.GetStates())
            {
                strStates += s.Id + ":" + s.Name + ";";
            }
            foreach (Policy p in affiliateTargetBO.GetPolicies())
            {
                strPolicies += p.Id + ":" + p.Name + ";";
            }
            foreach (ServiceType s in affiliateTargetBO.GetServiceTypes())
            {
                strServiceTypes += s.Id + ":" + s.Name + ";";
            }
            foreach (QuantityType q in affiliateTargetBO.GetQuantityTypes())
            {
                strQuantityTypes += q.Id + ":" + q.Name + ";";
            }

            strPlanyears += "2010" + ":" + "2010" + ";";
            strPlanyears += "2011" + ":" + "2011" + ";";

            var jsonData = new
            {
                states = strStates.TrimEnd(';'),
                policies = strPolicies.TrimEnd(';'),
                serviceTypes = strServiceTypes.TrimEnd(';'),
                quantityTypes = strQuantityTypes.TrimEnd(';'),
                planYears = strPlanyears.TrimEnd(';'),

                canEdit=AccessRights.Update.HasValue && AccessRights.Update.Value
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPlanyears(int stateId)
        {
            var planYears = new MITSService<PlanYear>().GetAll(x => x.StateId == stateId).Distinct().OrderBy(x => x.Year).ToList();
            //PlanYear _item = new PlanYear() { Id = 0, Year = "Select" };
            //cities.Insert(0, _city);
            //string strPlanyears = ":Select;";
            //foreach (PlanYear py in planYears)
            //{ 
            //    strPlanyears += py.Year.ToString();
            //}
            IList<SelectItem> years = new List<SelectItem>();
            SelectItem _Item = new SelectItem();
            _Item.value = "0";
            _Item.text = "Select";
            years.Add(_Item);
            foreach (PlanYear py in planYears)
            {
                SelectItem _item = new SelectItem();
                _item.value = py.Year.ToString();
                _item.text = py.Year.ToString();
                if (!years.Contains(_item))
                {
                    years.Add(_item);
                }
            }

            var jsonData = new
            {
                data = (
                 from m in years
                 select new
                 {
                     Value = m.value,
                     Text = m.text
                 }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetProductTypesByTargetId(int targetId)
        {
            string strProductTypes = "";

            foreach (ProductType p in affiliateTargetBO.GetProductTypesByTargetId(targetId))
            {
                strProductTypes += p.Id + ":" + p.Name + ";";
            }

            var jsonData = new
            {
                productTypes = strProductTypes.TrimEnd(';'),
                canEdit = AccessRights.Update.HasValue && AccessRights.Update.Value
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult  SaveAffliateProducts(Int32 affiliateTargetID,string  productIDs) 
        {
            List<Int32> pIDs = new List<Int32>();
            foreach (string pID in productIDs.Split(','))
            {
                if (pID != "")
                    pIDs.Add(Convert.ToInt32(pID));
            }

            affiliateTarget = affiliateTargetBO.GetAffiliateTargetByID(affiliateTargetID);
            var result = affiliateTargetBO.AssignProducts(affiliateTarget, pIDs);

            var jsonData = new { IsSubmited = result };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUserAccessRights()
        {
            var jsonData = new
            {
                showEdit = AccessRights.Update.HasValue && AccessRights.Update.Value,
                showDelete = AccessRights.Delete.HasValue && AccessRights.Delete.Value
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }

    public class SelectItem
    {
        public string value;
        public string text;
    }
}
