﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.BusinessObjects;
using Mits.DAL.EntityModels;
using System.Collections;
using System.IO;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class ContractsController : MITSApplicationController
    {
        private AccessRights AccessRightsContracts
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Contracts);
            }
        }
        
        //
        // GET: /Contracts/
        private ContractsBO _contractsBO;
        public ContractsController(ISessionCookieValues sessionValues)
            : this(sessionValues, new ContractsBO(sessionValues))
        {
        }

        public ContractsController(ISessionCookieValues sessionValues, ContractsBO contractsBO)
            : base(sessionValues)
        {
            _contractsBO = contractsBO;
        
        }

        public ActionResult Index()
        {
            if (AccessRightsContracts != null && AccessRightsContracts.Read.HasValue == true && AccessRightsContracts.Read == true)
            {
                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate _affObj = new MITSService<Affiliate>().GetSingle(x => x.Id== affiliateId);
                ViewData["Name"] = _affObj.Name;
                return View();
            }
            else
            {
               return RedirectToAction("AccessDenied", "Home");
            }
        }
         public ActionResult GetDocument(string sidx, string sord, int page, int rows)
       {
       
           int pageIndex = Convert.ToInt32(page) - 1;
           int pageSize = rows;
           int totalRecords;
           var data = _contractsBO.GetPaged(pageIndex, pageSize,out totalRecords).AsEnumerable<Document>();
           int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
           var jsonData = new
           {
               total = totalPages,
               page = page,
               records = totalRecords,
               rows = (
                   from m in data

                   select new
                   {
                       id = m.Id,
                       cell = new object[] { 
                           m.Id,
                           m.UploadFileName,
                            (m.UploadDate!=null)?Convert.ToDateTime(m.UploadDate).ToString("MM/dd/yyyy"):string.Empty,
                           (!string.IsNullOrEmpty(m.UploadFileName)) ? m.UploadFileName : "-"


                        }
                   }).ToArray()
           };
           return Json(jsonData, JsonRequestBehavior.AllowGet);

       }

         public DownloadResult Download()
         {
             string fileName = Convert.ToString(Request.QueryString["FileName"]);
             // string fileName = cellvalue;
             if (fileName != "-")
             {
                 fileName = fileName.Replace("<~>", "#");
                 return new DownloadResult { VirtualPath = "~/Uploads/" + fileName, FileDownloadName = fileName };
             }
             else
             {
                 return null;
             }
         }


    }

}
