﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;

namespace Mits.Web.Controllers
{
    public class PlanYearController : MITSApplicationController
    {
        //
        // GET: /PlanYear/

        public PlanYearController(ISessionCookieValues sessionValue)
            : this(sessionValue, new PlanYearBO())
        {

            _planYearBO = new PlanYearBO();
        }

        public PlanYearController(ISessionCookieValues sessionValue, PlanYearBO pDataBO)
            : base(sessionValue)
        {
            _planYearBO = pDataBO;
        }

        private PlanYearBO _planYearBO;

        public ActionResult Index()
        {
            string affiliateRoleName = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName);
            if (affiliateRoleName == "Administrator" || affiliateRoleName == "Executive Assistant")
            {
                IList<State> currentStates = _planYearBO.GetStates();
                State tmpState = new State();
                tmpState.Id = 0;
                tmpState.Name = "All State";
                currentStates.Insert(0, tmpState);

                ViewData["States"] = new SelectList(currentStates, "Id", "Name");

                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }


        [HttpGet]
        public ActionResult GetPlanYears(string state, int page, int rows)
        {
            string affiliateRoleName = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName);
            if (affiliateRoleName == "Administrator" || affiliateRoleName == "Executive Assistant")
            {
                int stateid;
                if (state == null)
                    stateid = 0;
                else
                    stateid = int.Parse(state);

                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;
                int totalRecords;

                var data = _planYearBO.GetPlanYears(pageIndex, pageSize, stateid, out totalRecords).AsEnumerable<PlanYear>();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = (
                        from m in data

                        select new
                        {
                            id = m.Id,
                            cell = new object[] { 
                        m.State.Name,
                        m.Year,
                        m.StartDate== null ? "": ((DateTime)m.StartDate).ToString("MM/dd/yyyy"),
                        m.EndDate == null ? "":((DateTime)m.EndDate).ToString("MM/dd/yyyy")
                    }
                        }).ToArray()
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }


        public ActionResult Delete()
        {
            string affiliateRoleName = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName);
            if (affiliateRoleName == "Administrator" || affiliateRoleName == "Executive Assistant")
            {
                try
                {
                    int id;

                    id = int.Parse(Request.Form["id"]);
                    var deleteResult = _planYearBO.DeletePlanYear(id);

                    return RedirectToAction("index", "PlanYear");
                }
                catch (Exception ex)
                {

                }
                return View("PlanYear/index");
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult PerformAction(PlanYear obj)
        {
            try
            {
                System.Int32 id;
                switch (Request.Form["oper"])
                {

                    case "edit":
                        obj.Id = System.Int32.Parse(Request.Form["id"]);
                        obj.StateId = System.Int32.Parse(Request.Form["State"]);
                        obj.Year = int.Parse(Request.Form["PlanYear"].ToString());
                        obj.StartDate = DateTime.Parse(Request.Form["StartDate"].ToString());
                        obj.EndDate = DateTime.Parse(Request.Form["EndDate"].ToString());

                        var editResult = _planYearBO.EditPlanYear(obj);
                        break;
                    case "add":
                        obj.Id = 0;
                        obj.StateId = System.Int32.Parse(Request.Form["State"]);
                        obj.Year = int.Parse(Request.Form["PlanYear"].ToString());
                        obj.StartDate = DateTime.Parse(Request.Form["StartDate"].ToString());
                        obj.EndDate = DateTime.Parse(Request.Form["EndDate"].ToString());

                        _planYearBO.EditPlanYear(obj);
                        break;
                    default:
                        break;
                }
                return RedirectToAction("index", "PlanYear");
            }
            catch (Exception ex)
            {
            }
            return View("PlanYear/index");
        }

        public ActionResult GetDropDownsData()
        {
            string strStates = ":Select;";
            foreach (State s in _planYearBO.GetStates())
            {
                strStates += s.Id + ":" + s.Name + ";";
            }

            var jsonData = new
            {
                states = strStates.TrimEnd(';'),
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

    }
}
