﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.BusinessObjects;
using Mits.DAL.EntityModels;
using Mits.BLL.Authorization;
using Mits.Common;

namespace Mits.Web.Controllers
{
    public class SpecificStateRateController : MITSApplicationController
    {
        //
        // GET: /SpecificStateRate/


        public SpecificStateRateController(ISessionCookieValues sessionValue)
            : this(sessionValue, new SpecificStateRateBO())
        {

            specificStateRateBO = new SpecificStateRateBO();
        }

        public SpecificStateRateController(ISessionCookieValues sessionValue, SpecificStateRateBO pDataBO)
            : base(sessionValue)
        { 
            specificStateRateBO = pDataBO;
        
        }

        private SpecificStateRateBO specificStateRateBO;

        public ActionResult Index()
        {
            string affiliateRoleName = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName);
            if (affiliateRoleName == "Administrator" || affiliateRoleName == "Executive Assistant")
            {
                IList<State> currentStates = specificStateRateBO.GetCurrentSpecificState();
                ViewData["States"] = new SelectList(currentStates, "Id", "Name");

                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetSpecificRates(string state)
        {
            string affiliateRoleName = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName);
            if (affiliateRoleName == "Administrator" || affiliateRoleName == "Executive Assistant")
            {
                int stateid;
                if (state == null)
                    stateid = 0;
                else
                    stateid = int.Parse(state);
                var data = specificStateRateBO.GetSpecificRates(stateid).AsEnumerable<StateSpecificRate>();

                var jsonData = new
                {
                    rows = (
                        from m in data

                        select new
                        {
                            id = m.Id,
                            cell = new object[] { 
                            m.State.Name,
                            m.Urban,
                            m.Rural,
                            m.Metro,
                            m.NonMetro,
                        }
                        }).ToArray()
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult Delete()
        {
            string affiliateRoleName = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName);
            if (affiliateRoleName == "Administrator" || affiliateRoleName == "Executive Assistant")
            {
                try
                {
                    int id;

                    id = int.Parse(Request.Form["id"]);
                    var deleteResult = specificStateRateBO.DeleteSpecificRate(id);

                    return RedirectToAction("index", "SpecificStateRate");
                }
                catch (Exception ex)
                {

                }
                return View("SpecificStateRate/index");
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult PerformAction(StateSpecificRate obj)
        {
            try
            {
                System.Int32 id;
                switch (Request.Form["oper"])
                {
                    case "edit":
                        obj.Id = System.Int32.Parse(Request.Form["id"]);
                        obj.StateId = System.Int32.Parse(Request.Form["State"]);
                        obj.Urban = double.Parse(Request.Form["Urban"].ToString());
                        obj.Rural = double.Parse(Request.Form["Rural"].ToString());
                        obj.Metro = double.Parse(Request.Form["Metro"].ToString());
                        obj.NonMetro = double.Parse(Request.Form["NonMetro"].ToString());
                        var editResult = specificStateRateBO.EditSpecificRate(obj);

                        break;

                    case "add":
                        obj.StateId = System.Int32.Parse(Request.Form["State"]);
                        obj.Urban = double.Parse(Request.Form["Urban"].ToString());
                        obj.Rural = double.Parse(Request.Form["Rural"].ToString());
                        obj.Metro = double.Parse(Request.Form["Metro"].ToString());
                        obj.NonMetro = double.Parse(Request.Form["NonMetro"].ToString());
                        var addResult = specificStateRateBO.AddSpecificRate(obj);

                        break;

                    default:
                        break;
                }
                return RedirectToAction("index", "SpecificStateRate");
            }
            catch (Exception ex)
            {
            }
            return View("SpecificStateRate/index");
        }

        public ActionResult GetDropDownsData()
        {
            string strStates = ":Select;";
            foreach (State s in specificStateRateBO.GetStates())
            {
                strStates += s.Id + ":" + s.Name + ";";
            }

            var jsonData = new
            {
                states = strStates.TrimEnd(';'),
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


    }
}
