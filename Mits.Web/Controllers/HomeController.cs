﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    
    [HandleError]
    public class HomeController : MITSApplicationController
    {
        public HomeController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
        }

        public ActionResult Index()
        {
            //new website home
            int affiliteTypeId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            if ((int)Constants.AffiliateType.Eworld == affiliteTypeId)
            {
                if ((int)Constants.AffiliateContactRole.Finance == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId))
                    return RedirectToAction("Index", "Invoice"); //homeMenu.URL = "Invoice/Index";
                else
                    return RedirectToAction("Index", "Affiliate");  //homeMenu.URL = "Affiliate/Index";
            }
            else if ((int)Constants.AffiliateType.OEM == affiliteTypeId)
            {
                return RedirectToAction("Map", "Affiliate");  //homeMenu.URL = "Affiliate/Map";
            }
            else if ((int)Constants.AffiliateType.Processor == affiliteTypeId)
            {
                return RedirectToAction("List", "ProcessingData");  //"ProcessingData/Index";
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            //new website home
            //prior to new www website change
            //NewsFeedBO _NewsFeedBO = new NewsFeedBO();
            //var Feeds = _NewsFeedBO.GetAllNewsFeed();
            //var sortedList = new List<NewsFeed>();            
            //for (int i = 0; i < Feeds.Count - 3; i++)
            //{
            //    sortedList.Add(Feeds[i]);
            //}
            //int last = Math.Max(0,Feeds.Count - 3);
            //for (int i = Feeds.Count - 1; i >= last; i--)
            //{
            //    sortedList.Add(Feeds[i]);
            //}
            //ViewData["NewsFeeds"] = sortedList;
            //return View();
        }

        public ActionResult About()
        {
            return View();
        }
        public ActionResult FAQ()
        {
            return View();
        }
        public ActionResult Clients()
        {
            return View();
        }
        public ActionResult StateGuideLines()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            return View();
        }
        public ActionResult ReadMore()
        {
            return View();
        }
        public ActionResult MoreAbout()
        {
            return View();
        }
        public ActionResult Error()
        {
            return View();
        }
        public ActionResult SiteDown()
        {
            return View();
        }

        public ActionResult EnviroNMental()
        {
            return View();
        }

        public ActionResult OemRetailCreditProgram()
        {
            return View();
        }

        public ActionResult SharedRes()
        {
            return View();
        }

        public ActionResult Consumer()
        {
            return View();
        }
        public ActionResult Manufacturer()
        {
            return View();
        }

        public ActionResult Recycler()
        {
            return View();
        }

        public ActionResult RRP()
        {
            return View();
        }

        public ActionResult DropOffLocationsEarth911()
        {
            return View();
        }
        public ActionResult AccessDenied()
        {
            if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId) == -1)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }

        public ActionResult SonyForm()
        {
            return View();
        }

        public JsonResult SendSonyEmail(string PoNumber,string Requester,string Email,string ContactPhone,string ItemName,string Cost, string PurchaseFrom,string DateRequired,string Description)
        {
            ContactUsBO contactBo = new ContactUsBO();

            SonyMailContent mailContent = new SonyMailContent();
            mailContent.PoNumber = PoNumber;
            mailContent.Requester = Requester;
            mailContent.Email = Email;
            mailContent.ContactPhone = ContactPhone;
            mailContent.ItemName = ItemName;
            mailContent.Cost = Cost;
            mailContent.PurchaseFrom = PurchaseFrom;
            mailContent.DateRequired = DateRequired;
            mailContent.Description = Description;

            bool result = contactBo.SendEmail(mailContent);
            string msg;
            if (result)
                msg = "Message successfully sent! ";
            else
                msg = "Message sent failed.";

            var json = new { 
             showResult = result,
             showMsg = msg
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }
    }
}
