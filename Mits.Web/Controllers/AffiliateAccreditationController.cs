﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using System.Configuration;
using Mits.BLL.Authorization;
namespace Mits.Web.Controllers
{
    public class AffiliateAccreditationController : MITSApplicationController
    {

        private AffiliateAccreditationBO _affiliateAccreditationBO;

        private AccessRights AccessRightsAffiliateAccreditation
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.EntityAccreditation);
            }
        }
      
        public AffiliateAccreditationController(ISessionCookieValues sessionValues)
            : this(sessionValues, new AffiliateAccreditationBO())
        {
        }
     
        public AffiliateAccreditationController(ISessionCookieValues sessionValues, AffiliateAccreditationBO affiliateAccreditationBO)
            : base(sessionValues)
        {
            _affiliateAccreditationBO = affiliateAccreditationBO;                   
        }

       //Getting a list of affiliates

        private IList<Affiliate> GetAll()
        {
            IList<Affiliate> _affiliateList ;
            IList<Affiliate> unSortedList = new MITSService<Affiliate>().GetAll();
            IEnumerable<Affiliate> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _affiliateList = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _affiliateList = unSortedList;
            }

            Affiliate _selectAffiliate = new Affiliate();
            _selectAffiliate.Id = 0;
            _selectAffiliate.Name = "Select";
            _affiliateList.Insert(0, _selectAffiliate);
            return _affiliateList;
        }

        //Getting a list of all accreditations

        private IList<AccreditationType> GetAllAccreditation()
        {
            IList<AccreditationType> _accreditationList;
            IList<AccreditationType> unSortedList = new MITSService<AccreditationType>().GetAll();
            IEnumerable<AccreditationType> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _accreditationList = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _accreditationList = unSortedList;
            }
            return _accreditationList;
        }
        
        
        //
        // GET: /AffiliateAccreditation/

        public ActionResult Index()
        {
            if (AccessRightsAffiliateAccreditation != null && AccessRightsAffiliateAccreditation.Read.HasValue == true && AccessRightsAffiliateAccreditation.Read == true)
            {
                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                    return View(new AffiliateAccreditationViewModel());
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");

                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");

            }
        }


        //Getting accreditation against a particular affiliate

        public ActionResult GetAccreditation(string affiliate)
        {
            int _affiliateId = int.Parse(affiliate);
            var data = new MITSService<AffiliateAccreditation>().GetAll(x => x.AffiliateId == _affiliateId & x.Active == true);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.AccreditationTypeId,


                    }).ToArray()
            };
            return Json(jsonData);
        }

        //Invokes 'Add' function of AffiliateAccreditationBO and send the response back to view

        public ActionResult Save(string affiliate, string accreditations)
        {
            if (AccessRightsAffiliateAccreditation != null && AccessRightsAffiliateAccreditation.Add.HasValue == true && AccessRightsAffiliateAccreditation.Add == true || (AccessRightsAffiliateAccreditation.Update.HasValue == true && AccessRightsAffiliateAccreditation.Update == true))
            {


                Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();

                try
                {
                    if (int.Parse(affiliate) == 0)
                    {
                        return Json(new
                        {
                            success = false,
                            message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                        });
                    }
                    else
                    {
                        _returnDictionaryValue = _affiliateAccreditationBO.Add(affiliate, accreditations);
                    }

                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }

                var data = this.GetAll();
                if (_returnDictionaryValue["duplicate"] == 0)
                {
                    var jsonData = new
                    {
                        success = true,
                        message = ConfigurationManager.AppSettings["RecordSaved"],
                        selected = _returnDictionaryValue["selectedId"].ToString(),
                        data = (
                            from m in data
                            select new
                            {
                                Value = m.Id,
                                Text = m.Name,

                            }).ToArray()
                    };
                    return Json(jsonData);
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                    });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });
            }
        }
       
        
    }
}
