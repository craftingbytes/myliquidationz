﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.Common;
using Mits.BLL.BusinessObjects;
using Mits.BLL.Authorization;
using System.IO;

namespace Mits.Web.Controllers
{
    public class FileUploadController : MITSApplicationController
    {
        //
        // GET: /FileUpload/
        public FileUploadController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult FileExists(FormCollection forms)
        {
            Dictionary<string, string> fileArray = new Dictionary<string, string>();
            foreach (string key in forms.AllKeys)
            {
                if (key != "folder")
                {
                    string targetDirectory = Path.Combine(Request.PhysicalApplicationPath, @"uploads\");
                    string targetFilePath = Path.Combine(targetDirectory, forms[key]);
                    if (System.IO.File.Exists(targetFilePath))
                        fileArray.Add(key, forms[key]);
                }
            }

            if (fileArray.Count > 0)
                return Json(1);
            else
                return Json(0);
        }
    }

}
