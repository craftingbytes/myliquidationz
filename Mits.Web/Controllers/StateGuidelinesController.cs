﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Mits.BLL.ViewModels;
using Mits.BLL;
using Mits.BLL.BusinessObjects;
using Mits.BLL.Authorization;
using Mits.Common;
using Mits.DAL.EntityModels;

namespace Mits.Web.Controllers
{
    public class StateGuidelinesController : MITSApplicationController
    {
        private StateGuidelineBO _stateGuidelineBO = null;

        private AccessRights AccessRightsAccreditation
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.StateGuidelines);
            }
        }
        

        public StateGuidelinesController(ISessionCookieValues sessionValue)
            : this(sessionValue, new StateGuidelineBO())
        {
        }

        public StateGuidelinesController(ISessionCookieValues sessionValue, StateGuidelineBO stateGuidelineBO)
            : base(sessionValue)
        {
            _stateGuidelineBO = stateGuidelineBO;
        }

        //
        // GET: /StateGuidelines/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult State(string id)
        {
            var state = new MITSService<State>().GetSingle(s => s.Abbreviation == id);
            if (state == null)
            {
                // return the Index view
                return RedirectToAction("Index");
            }
            var model = new StateGuidelinesViewModel(state);
            return View(model);
        }

        public ActionResult Edit()
        {
            if (AccessRightsAccreditation == null)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            else if ((AccessRightsAccreditation.Update.HasValue == true && AccessRightsAccreditation.Update == true) || (AccessRightsAccreditation.Add.HasValue == true && AccessRightsAccreditation.Add == true))
            {
                var unSortedList = new MITSService<State>().GetAll();
                var sortedList = unSortedList.OrderBy(f => f.Name).ToList();
                var states = new SelectList(sortedList, "Id", "Name");
                ViewData["States"] = states;
                var lawStateItems = new List<SelectListItem>();
                lawStateItems.Add(new SelectListItem { Value = "0", Text = "Select" });
                lawStateItems.Add(new SelectListItem { Value = "10", Text = "Law Introduced/Not Passed" });
                lawStateItems.Add(new SelectListItem { Value = "20", Text = "No Law Introduced" });
                lawStateItems.Add(new SelectListItem { Value = "30", Text = "Law Passed/Effective Soon" });
                lawStateItems.Add(new SelectListItem { Value = "40", Text = "Law in Effect" });
                lawStateItems.Add(new SelectListItem { Value = "50", Text = "Voluntary/Program Running" });
                ViewData["LawStateItems"] = new SelectList(lawStateItems, "Value", "Text", "0");
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
           
        }

        public JsonResult GetStateGuideline(string state)
        {
            int stateId = int.Parse(state);
            var guideline = _stateGuidelineBO.GetStateGuidelineByStateId(stateId);
            if (guideline != null)
            {
                var jsonData = new 
                {
                    GoverningBody = string.IsNullOrEmpty(guideline.GoverningBody) ? string.Empty : guideline.GoverningBody,
                    Website = string.IsNullOrEmpty(guideline.Website) ? string.Empty : guideline.Website,
                    CoveredDevices = string.IsNullOrEmpty(guideline.CoveredDevices) ? string.Empty : guideline.CoveredDevices,
                    OEMObligation = string.IsNullOrEmpty(guideline.OEMObligation) ? string.Empty : guideline.OEMObligation,
                    Updates = guideline.Updates,
                    LawState = guideline.LawState == null ? 0 : guideline.LawState,
                    RegulationType = string.IsNullOrEmpty(guideline.RegulationType) ? string.Empty : guideline.RegulationType,
                    RegulationChange = string.IsNullOrEmpty(guideline.RegulationChange) ? string.Empty : guideline.RegulationChange
                };
                return Json(jsonData);
            }
            return Json(new
            {
                GoverningBody = string.Empty,
                Website = string.Empty,
                CoveredDevices = string.Empty,
                OEMObligation = string.Empty,
                Updates = string.Empty,
                LawState = "0",
                RegulationType = string.Empty,
                RegulationChange = string.Empty
            });
        }

        public ActionResult SaveStateGuideline(StateGuideline guideline)
        {
            if (true)
            {
                if (_stateGuidelineBO.SaveStateGuideline(guideline))
                {
                    return Json(new
                    {
                        success = true,
                        message = ConfigurationManager.AppSettings["RecordSaved"]
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"]
                    });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });
            }
            
        }

        [HttpGet]
        public ActionResult GetContacts(int? state)
        {
            if (!state.HasValue)
            {
                return Content("{rows:[]}");
            }
            var contacts = _stateGuidelineBO.GetContactsByStateId(state.Value);
            var jsonData = new
            {
                total = 1,
                page = 1,
                records = contacts.Count,
                rows = (
                    from m in contacts

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            string.IsNullOrEmpty(m.Name) ? string.Empty : m.Name,
                            string.IsNullOrEmpty(m.Phone) ? string.Empty : m.Phone,
                            string.IsNullOrEmpty(m.EMail) ? string.Empty : m.EMail
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        // Edit/Add contact
        public ActionResult SaveContact(int id, int state, string name, string phone, string email)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Content("failture");
            }
            StateGuideline_Contact contact = new StateGuideline_Contact();
            contact.Id = id;
            contact.StateId = state;
            contact.Name = name;
            contact.Phone = phone;
            contact.EMail = email;

            if (_stateGuidelineBO.SaveContact(contact))
            {
                return Content(contact.Id.ToString());
            }
            return Content("failture");
        }

        // Delete contact by contact id
        public ActionResult DeleteContact(int id)
        {
            if (_stateGuidelineBO.DeleteContact(id))
            {
                return Content("success");
            }
            else
            {
                return Content("failture");
            }
        }

        [HttpGet]
        public ActionResult GetDeadlineDates(int? state)
        {
            if (!state.HasValue)
            {
                return Content("{rows:[]}");
            }
            var deadlineDates = _stateGuidelineBO.GetDeadlineDatesByStateId(state.Value);
            var jsonData = new
            {
                total = 1,
                page = 1,
                records = deadlineDates.Count,
                rows = (
                    from m in deadlineDates

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            string.IsNullOrEmpty(m.Name) ? string.Empty : m.Name,
                            string.IsNullOrEmpty(m.Date) ? string.Empty : m.Date
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        // Edit/Add deadline date
        public ActionResult SaveDeadlineDate(int id, int state, string name, string date)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Content("failture");
            }
            StateGuideline_DeadlineDate deadlineDate = new StateGuideline_DeadlineDate();
            deadlineDate.Id = id;
            deadlineDate.StateId = state;
            deadlineDate.Name = name;
            deadlineDate.Date = date;

            if (_stateGuidelineBO.SaveDeadlineDate(deadlineDate))
            {
                return Content(deadlineDate.Id.ToString());
            }
            return Content("failture");
        }

        // Delete deadline date by deadline date id
        public ActionResult DeleteDeadlineDate(int id)
        {
            if (_stateGuidelineBO.DeleteDeadlineDate(id))
            {
                return Content("success");
            }
            else
            {
                return Content("failture");
            }
        }

        [HttpGet]
        public ActionResult GetLinks(int? state)
        {
            if (!state.HasValue)
            {
                return Content("{rows:[]}");
            }
            var links = _stateGuidelineBO.GetLinksByStateId(state.Value);
            var jsonData = new
            {
                total = 1,
                page = 1,
                records = links.Count,
                rows = (
                    from m in links

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            string.IsNullOrEmpty(m.Name) ? string.Empty : m.Name,
                            string.IsNullOrEmpty(m.Link) ? string.Empty : m.Link
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        // Edit/Add link
        public ActionResult SaveLink(int id, int state, string name, string link)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Content("failture");
            }
            StateGuideline_Link _link = new StateGuideline_Link();
            _link.Id = id;
            _link.StateId = state;
            _link.Name = name;
            _link.Link = link;

            if (_stateGuidelineBO.SaveLink(_link))
            {
                return Content(_link.Id.ToString());
            }
            return Content("failture");
        }

        // Delete deadline date by link id
        public ActionResult DeleteLink(int id)
        {
            if (_stateGuidelineBO.DeleteLink(id))
            {
                return Content("success");
            }
            else
            {
                return Content("failture");
            }
        }

    }
}
