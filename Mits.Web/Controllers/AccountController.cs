﻿#define OLD_VALUES

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.ViewModels;
using Mits.BLL.BusinessObjects;
using Mits.BLL;
using Mits.Common;
using Mits.DAL.EntityModels;
using System.Configuration;
using System.Security.Claims;
using MvcApplication2.Models;
using System.Web.Routing;
using System.Web.Security;

namespace Mits.Web.Controllers
{
    [AllowAnonymous]
    public class AccountController : MITSApplicationController
    {
        private LoginBO _loginBO;
        private AffiliateContactBO _affiliateContactBO;
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        public AccountController(ISessionCookieValues sessionValues)
            : this(sessionValues, new LoginBO())
        {
        }

        public AccountController(ISessionCookieValues sessionValues, LoginBO loginBO)
            : base(sessionValues)
        {
            _loginBO = loginBO;
            _affiliateContactBO = new AffiliateContactBO(sessionValues);

            
        }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        //
        // GET: /Account/Login

        public ActionResult Login()
        {
            return View(new LoginViewModel());            
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            List<Message> messages = new List<Message>();
            AffiliateContactViewModel vm   = _loginBO.LoginAffiliateContact(model.UserName, model.Password);
            if (vm == null)
            {
                LoginViewModel newModel = new LoginViewModel();
                messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.InvalidLoginCredentials));
                newModel.Messages = messages;
                return View(newModel);
            }
            else if (vm.Messages.Count > 0)
            {
                LoginViewModel newModel = new LoginViewModel();
                newModel.Messages = vm.Messages;
                return View(newModel);
            }
            else
            {
                var contact = vm.AffiliateContact;
                var nameParts = new[] { contact.FirstName, contact.MiddleInitials, contact.LastName }.Where(p => p != null);
                var claimsIdentity = new ClaimsIdentity(
                    new Claim[] {
                        new Claim("email", contact.Email),
                        new Claim("role", contact.Role.RoleName),
                        new Claim("name", string.Join(" ", nameParts)),
                    }, Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie, "name", "role");
                HttpContext.GetOwinContext().Authentication.SignIn(claimsIdentity);

                AffiliateContact assocContact;
                if (vm.AffiliateContact.RoleId == 12)
                {
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.MasterAffiliateContactId, vm.AffiliateContact.Id);
                    assocContact = _affiliateContactBO.GetDefaultAssociateContact(vm.AffiliateContact.Id);
                }
                else
                {
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.MasterAffiliateContactId, -1);
                    assocContact = vm.AffiliateContact;
                    
                }

                SetSessionParameters(assocContact);
                UpdateViewData();

                int affiliateTypeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                if ((int)Constants.AffiliateType.Eworld == affiliateTypeid)
                {
                    if ((int)Constants.AffiliateContactRole.Finance == affiliateRoleId)
                        return RedirectToAction("Index", "Invoice");
                    else
                        return RedirectToAction("Index", "Affiliate");
                }
                else if ((int)Constants.AffiliateType.OEM == affiliateTypeid)
                {
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
                    MapXMLHandler.CreateMapXMLFile(assocContact.AffiliateId.Value, assocContact.Affiliate.AffiliateTargets.ToList());
                    return RedirectToAction("Map", "Affiliate", new { id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) });
                    //return RedirectToAction("Index", "AffiliateContact", new { id = SessionParameters.AffiliateId });
                }
                else if ((int)Constants.AffiliateType.Processor == affiliateTypeid)
                {
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
                    return RedirectToAction("List", "ProcessingData");
                }
                //else if ((int)Constants.AffiliateType.Collector == SessionParameters.AffiliateTypeId)
                //{
                //}
                //else if ((int)Constants.AffiliateType.State == SessionParameters.AffiliateTypeId)
                //{
                //}
                else if ((int)Constants.AffiliateType.Auditor == affiliateTypeid)
                {
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
                    return RedirectToAction("Index", "ChangePassword");
                }


            }

            return View(new LoginViewModel());
        }

        private void SetSessionParameters(AffiliateContact ac)
        {
            string affiliateType = ac.Affiliate.AffiliateType.Type;
            if (_sessionValues != null)
            {
                Dictionary<string, string> values = _sessionValues.GetSessionValues();

                if (values == null)
                {
                    values = new Dictionary<string, string>()
                    {
                        { Constants.SessionParameters.AffiliateId, ac.Affiliate.Id.ToString() },
                        { Constants.SessionParameters.AffiliateContactId, ac.Id.ToString() },
                        { Constants.SessionParameters.AffiliateRoleId, ((int)ac.RoleId).ToString() },
                        { Constants.SessionParameters.AffiliateRoleName, ac.Role.RoleName == Constants.RoleNames.Processor_Recycler ? Constants.RoleNames.Processor : ac.Role.RoleName },
                        { Constants.SessionParameters.AffiliateTypeId, ((int)ac.Affiliate.AffiliateTypeId).ToString() },
                        { Constants.SessionParameters.AffiliateType, affiliateType == Constants.RoleNames.Processor_Recycler ? Constants.RoleNames.Processor : affiliateType },
                        { Constants.SessionParameters.AffiliateContactName, ac.FirstName + " " + ac.LastName },
                        { Constants.SessionParameters.IpAddress, HttpContext.Request.UserHostAddress.ToString() }
                    };
                }
                else
                {
                    values[Constants.SessionParameters.AffiliateId] = ac.Affiliate.Id.ToString();
                    values[Constants.SessionParameters.AffiliateContactId] = ac.Id.ToString();
                    values[Constants.SessionParameters.AffiliateRoleId] = ((int)ac.RoleId).ToString();
                    values[Constants.SessionParameters.AffiliateRoleName] = ac.Role.RoleName == Constants.RoleNames.Processor_Recycler ? Constants.RoleNames.Processor : ac.Role.RoleName;
                    values[Constants.SessionParameters.AffiliateTypeId] = ((int)ac.Affiliate.AffiliateTypeId).ToString();
                    values[Constants.SessionParameters.AffiliateType] = affiliateType == Constants.RoleNames.Processor_Recycler ? Constants.RoleNames.Processor : affiliateType;
                    values[Constants.SessionParameters.AffiliateContactName] = ac.FirstName + " " + ac.LastName;
                    values[Constants.SessionParameters.IpAddress] = HttpContext.Request.UserHostAddress.ToString();
                }

                _sessionValues.SetSessionValues(values);
            }

            AuditEvent auditEvent = new AuditEvent();
            auditEvent.AffiliateContactId = ac.Id;
            auditEvent.Entity = "Login";
            auditEvent.AuditActionId = Convert.ToInt32(Mits.Common.Constants.ActionType.Login);
            auditEvent.IpAddress = HttpContext.Request.UserHostAddress.ToString();
            auditEvent.TimeStamp = DateTime.Now;
            auditEvent.Note = HttpContext.Request.Browser.Browser + " " + HttpContext.Request.Browser.Version;
            _loginBO.AddAuditEvent(auditEvent);
        }


        [HttpGet]
        public ActionResult Logout() 
        {
            _sessionValues.ClearCookie();
            return RedirectToAction("Login");
        }

        public ActionResult Costco()
        { 
            string url = ConfigurationManager.AppSettings["LoginUrlCostco"].ToString();
            return Redirect(url);
        }

        public ActionResult Glass()
        {
            string url = ConfigurationManager.AppSettings["LoginUrlGlass"].ToString();
            return Redirect(url);
        }

        public ActionResult WDTS()
        {
            string url = ConfigurationManager.AppSettings["LoginUrlWDTS"].ToString();
            return Redirect(url);
        }

        // TODO CB: Not referenced anywhere.
        public ActionResult SwitchUser(int associatedContactId)
        {
            int masterContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.MasterAffiliateContactId);
            int acId = associatedContactId;
            AffiliateContact assocContact;
            _sessionValues.ClearCookie();
            if (masterContactId > 0)
            {
                //SessionParameters.MasterAffiliateContactId = masterContactId;
                AffiliateMasterContact mc = new MITSService<AffiliateMasterContact>().GetSingle(x => x.MasterAffiliateContactId == masterContactId && x.AssociateContactId == acId);
                assocContact = mc.AffiliateContact1;

                if (assocContact != null)
                {
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.MasterAffiliateContactId, masterContactId);
                    SetSessionParameters(assocContact);
                    UpdateViewData();
                    int affiliateTypeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                    int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                    if ((int)Constants.AffiliateType.Eworld == affiliateTypeid)
                    {
                        if ((int)Constants.AffiliateContactRole.Finance == affiliateRoleId)
                            return RedirectToAction("Index", "Invoice");
                        else
                            return RedirectToAction("Index", "Affiliate");
                    }
                    else if ((int)Constants.AffiliateType.OEM == affiliateTypeid)
                    {
                        _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
                        MapXMLHandler.CreateMapXMLFile(assocContact.AffiliateId.Value, assocContact.Affiliate.AffiliateTargets.ToList());
                        return RedirectToAction("Map", "Affiliate", new { id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) });
                        //return RedirectToAction("Index", "AffiliateContact", new { id = SessionParameters.AffiliateId });
                    }
                    else if ((int)Constants.AffiliateType.Processor == affiliateTypeid)
                    {
                        _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
                        return RedirectToAction("List", "ProcessingData");
                    }
                    //else if ((int)Constants.AffiliateType.Collector == affiliateTypeid)
                    //{
                    //}
                    //else if ((int)Constants.AffiliateType.State == affiliateTypeid)
                    //{
                    //}
                    else if ((int)Constants.AffiliateType.Auditor == affiliateTypeid)
                    {
                        _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
                        return RedirectToAction("Index", "ChangePassword");
                    }

                }
                
            }

            return RedirectToAction("Index");
            
        }


        // **************************************
        // URL: /Account/Register
        // **************************************

        // TODO CB: Not referenced in original code as sent to CB.
        // Link commented-out on login page.
        public ActionResult Register()
        {
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus = MembershipService.CreateUser(model.UserName, model.Password, model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsService.SignIn(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************

        // TODO CB: works, but email notification is commented-out.
        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

    }
}
