﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class AccreditationController : Mits.Web.Controllers.MITSApplicationController
    {
        
        private AccreditationBO _accreditationBO;

        private AccessRights AccessRightsAccreditation
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Accreditation);
            }
        }
        
        public AccreditationController(ISessionCookieValues sessionValues)
            : this(sessionValues, new AccreditationBO())
        {
        }

        public AccreditationController(ISessionCookieValues sessionValues, AccreditationBO accreditationBO)
            : base(sessionValues)
        {
            _accreditationBO = accreditationBO;                   
        }
        
        //
        // GET: /Accreditation/

        public ActionResult Index()
        {
            if (AccessRightsAccreditation != null && AccessRightsAccreditation.Read.HasValue == true && AccessRightsAccreditation.Read == true)
            {
                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                    ViewData["Accreditations"] = _accreditationBO.GetAll();
                    return View();
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");
                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
                //Response.Redirect("~/Helpers/Error.aspx");
                //return null;
            }            
        }

       
        //Getting name and description against a particular Accreditation and retruning the response to View

        public ActionResult GetDetail(string accreditation)
        {
            int _id = int.Parse(accreditation);
            AccreditationType data = new MITSService<AccreditationType>().GetSingle(x => x.Id == _id);

            if (data != null)
            {
                return Json(new
                {
                    Id = data.Id,
                    Name = data.Name,
                    Desc = data.Description
                });
            }

            return Json(new
            {
                Id = 0,
                Name = string.Empty,
                Desc = string.Empty
            });
        }

        //Invokes Add function of AccreditationBO and return the response to View

        public ActionResult Save(string accreditation, string name, string desc)
        {

            if (AccessRightsAccreditation != null && 
                (AccessRightsAccreditation.Add.HasValue == true && AccessRightsAccreditation.Add == true ) ||
                (AccessRightsAccreditation.Update.HasValue == true && AccessRightsAccreditation.Update==true))
            {

                Dictionary<string, int> _retrunDictionaryValue = new Dictionary<string, int>();

                try
                {
                    _retrunDictionaryValue = _accreditationBO.Add(accreditation, name, desc);
                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }
                if (_retrunDictionaryValue["duplicate"] == 0)
                {
                    var data = _accreditationBO.GetAll();
                    var jsonData = new
                    {
                        success = true,
                        message = ConfigurationManager.AppSettings["RecordSaved"],
                        selected = _retrunDictionaryValue["selectedId"].ToString(),
                        data = (
                            from m in data
                            select new
                            {
                                Value = m.Id,
                                Text = m.Name

                            }).ToArray()
                    };
                    return Json(jsonData);
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                    });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied="AccessDenied",
                    
                });
            }

        }
    }
}
