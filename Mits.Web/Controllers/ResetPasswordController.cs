﻿using Mits.BLL;
using Mits.BLL.BusinessObjects;
using Mits.BLL.ViewModels;
using Mits.Common;
using Mits.DAL.EntityModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Mits.Web.Controllers
{
    [AllowAnonymous]
    public class ResetPasswordController :MITSApplicationController
    {
        private ResetPasswordBO _resetPasswordBO;

        public ResetPasswordController(ISessionCookieValues sessionValues)
            : this(sessionValues, new ResetPasswordBO())
        {
            
        }

        public ResetPasswordController(ISessionCookieValues sessionValues, ResetPasswordBO resetPasswordBO)
            : base(sessionValues)
        {
            _resetPasswordBO = resetPasswordBO;                   
        }

        // GET: /ResetPassword/

        public ActionResult Index()
        {
            AffiliateContact _affContact = new AffiliateContact();
           
            return View(new ResetPasswordViewModel(_affContact));
        }

        [HttpPost]
        public ActionResult Index(AffiliateContact affiliateContact)
        {
            AffiliateContact _affContact = new AffiliateContact();
            ResetPasswordViewModel _passwodViewModel = new ResetPasswordViewModel(_affContact);
            try
            {
                string answer = Request.Form["txtAnswer"];
                string email = Request.Form["txtEmail"];
                int questionId = int.Parse(Request.Form["ddlQuestion"]);

                // Generate an encrypted reset password request token for the account associated with the given email.
                string code = UserTokenProvider.GenerateSecurityToken(UserTokenProvider.ResetPasswordPurpose, email);

                // If we got this far, the account is valid.

                // Send a notification message to the given email address with the encrypted token as a URL parameter.
                string callbackUrl = Url.Action("AuthorizePasswordReset", "ResetPassword", new { code = code }, protocol: Request.Url.Scheme);
                _resetPasswordBO.SendPasswordResetEmail(email, answer, questionId, callbackUrl);

                _passwodViewModel.Messages = new List<Message>() { new Message(MessageType.Success.ToString(), Constants.Messages.InformationExists) };
            }
            catch (MessageException ex)
            {
                _passwodViewModel.Messages = new List<Message>() { ex.Msg };
            }
            catch (Exception ex)
            {
#if DEBUG
                // The DEBUG build gives correct error messages. 
                _passwodViewModel.Messages = new List<Message>() { new Message(MessageType.Exception.ToString(), "DEBUG: " + ex.Message) };
#else
                // RELEASE builds respond with the email-sent message regardless of whether the given email had an account or not.
                _passwodViewModel.Messages = new List<Message>() { new Message(MessageType.Exception.ToString(), Constants.Messages.InformationExists) };
#endif
            }

            return View("Index", _passwodViewModel);
        }

        [HttpGet]
        public ActionResult AuthorizePasswordReset()
        {
            try
            {
                // Decrypt and validate the token for the password reset request.
                string code = Request["code"];
                AffiliateContact contact = UserTokenProvider.ValidateSecurityToken(UserTokenProvider.ResetPasswordPurpose, code, TimeSpan.FromDays(1));

                // The account exists and the token is valid if we made it this far.

                // Set the account password to a random string and clear the security stamp.
                string password = RandomPassword.Generate(6);
                string savedPassword = password;
                if (ConfigHelper.ComputePwdHash == "true")
                {
                    savedPassword = Utility.CreateHash(savedPassword, false);
                }
                contact.Password = savedPassword;
                contact.SecurityStamp = null;
                new MITSService<AffiliateContact>().Save(contact);

                // Display the successful password reset page.
                ViewData["password"] = password;
                return View();
            }
            catch (Exception ex)
            {
            }

            // The token was either invalid or some error ocurred. Redirect to the login page.
            return RedirectToAction("Login", "Account");
        }

    }
}
