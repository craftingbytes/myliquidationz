﻿using Mits.BLL;
using Mits.DAL.EntityModels;
using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Linq;

namespace Mits.Web.Controllers
{
    class UserTokenProvider
    {
        public const string ChangePasswordPurpose = "change-password";
        public const string ResetPasswordPurpose = "reset-password";
        public const string ConfirmEmailPurpose = "confirm-email";

        private const string _unknownUserError = "Unknown or invalid user account email.";
        private const string _tokenGenerationError = "Error creating user account token.";
        private const string _tokenValidationError = "The token was not valid.";

        public static byte[] EncryptBytes(byte[] unprotectedData, MemoryProtectionScope scope, byte filler = 0)
        {
            if (unprotectedData == null || unprotectedData.Length == 0)
            {
                throw new ArgumentNullException();
            }
            // Size of encryption buffer must be a multiple of 16.
            int nBytes = ((unprotectedData.Length % 16) > 0) ? (unprotectedData.Length / 16 + 1) * 16 : unprotectedData.Length;
            byte[] protectedData = Enumerable.Repeat(filler, nBytes).ToArray();
            unprotectedData.CopyTo(protectedData, 0);
            ProtectedMemory.Protect(protectedData, scope);

            return protectedData;
        }

        public static byte[] DecryptBytes(byte[] protectedData, MemoryProtectionScope scope)
        {
            if (protectedData == null || protectedData.Length == 0)
            {
                throw new ArgumentNullException();
            }
            byte[] unprotectedData = new byte[protectedData.Length];
            protectedData.CopyTo(unprotectedData, 0);
            ProtectedMemory.Unprotect(unprotectedData, scope);
            return unprotectedData;
        }

        public static string GenerateSecurityToken(string purpose, string email)
        {
            if (string.IsNullOrWhiteSpace(purpose))
            {
                throw new ArgumentNullException("purpose");
            }

            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentNullException("email");
            }

            // Get the account info from the email.
            MITSService<AffiliateContact> service = new MITSService<AffiliateContact>();
            AffiliateContact contact = service.GetSingle(t => t.Email == email);
            if (contact == null)
            {
                throw new Exception(_unknownUserError);
            }

            // Make a GUID security stamp for this account if needed.
            if (string.IsNullOrWhiteSpace(contact.SecurityStamp))
            {
                var guid = Guid.NewGuid();
                contact.SecurityStamp = String.Concat(Array.ConvertAll(guid.ToByteArray(), b => b.ToString("X2")));
                service.Save(contact);
            }

            byte[] bytes = null;

            using (MemoryStream ms = new MemoryStream())
            {
                using (var writer = StreamExtensions.CreateWriter(ms))
                {
                    // Creation time for tokem expiration.
                    writer.Write(DateTimeOffset.UtcNow);

                    // User's account ID number.
                    writer.Write(Convert.ToString(contact.Id, CultureInfo.InvariantCulture));

                    // Purpose for the token.
                    writer.Write(purpose ?? "");

                    // Security stamp from the user's account.
                    writer.Write(contact.SecurityStamp.Trim());
                    writer.Flush();
                }

                bytes = ms.ToArray();
            }

            if (bytes == null || bytes.Length == 0)
            {
                throw new Exception(_tokenGenerationError);
            }

            return Uri.EscapeDataString(Convert.ToBase64String(UserTokenProvider.EncryptBytes(bytes, MemoryProtectionScope.SameLogon)));
        }

        public static AffiliateContact ValidateSecurityToken(string purpose, string token, TimeSpan ts)
        {
            if (string.IsNullOrWhiteSpace(purpose))
            {
                throw new ArgumentNullException("purpose");
            }

            if (string.IsNullOrWhiteSpace(token))
            {
                throw new ArgumentNullException("token");
            }

            if (ts == null)
            {
                throw new ArgumentNullException("ts");
            }

            int userId = 0;

            DateTimeOffset creationTime = DateTime.Now;
            string purp = null;
            string stamp = null;

            // Decrypt and read the info from the token.
            byte[] unprotectedData = UserTokenProvider.DecryptBytes(Convert.FromBase64String(Uri.UnescapeDataString(token)), MemoryProtectionScope.SameLogon);

            using (var ms = new MemoryStream(unprotectedData))
            using (var reader = StreamExtensions.CreateReader(ms))
            {
                // Read fields in order.
                creationTime = reader.ReadDateTimeOffset();
                string tempstr = reader.ReadString();
                userId = int.Parse(tempstr);
                purp = reader.ReadString();
                stamp = reader.ReadString();
            }

            // userId should be set, now try to get the contact. 
            MITSService<AffiliateContact> service = new MITSService<AffiliateContact>();
            AffiliateContact contact = service.GetSingle(t => t.Id == userId);
            if (contact == null || string.IsNullOrWhiteSpace(contact.SecurityStamp))
            {
                throw new Exception(_tokenValidationError);
            }

            // We have an id and a security stamp if we made it this far.

            // Test the validity of the info from the token against the account info fromthe DB.
            DateTimeOffset expirationTime = creationTime + ts;
            string expectedStamp = contact.SecurityStamp.Trim();
            if (expirationTime < DateTimeOffset.UtcNow ||   // Has the token expired?
                !string.Equals(purp, purpose) ||            // Does the purpose of the token match?
                !string.Equals(stamp, expectedStamp))       // Does it match the current security stamp?
            {
                // Throw if any of the tests failed.
                throw new Exception(_tokenValidationError);
            }

            return contact;
       }
    }

    // Based on Levi's authentication sample
    internal static class StreamExtensions
    {
        internal static readonly Encoding DefaultEncoding = new UTF8Encoding(false, true);

        public static BinaryReader CreateReader(this Stream stream)
        {
            return new BinaryReader(stream, DefaultEncoding, true);
        }

        public static BinaryWriter CreateWriter(this Stream stream)
        {
            return new BinaryWriter(stream, DefaultEncoding, true);
        }

        public static DateTimeOffset ReadDateTimeOffset(this BinaryReader reader)
        {
            return new DateTimeOffset(reader.ReadInt64(), TimeSpan.Zero);
        }

        public static void Write(this BinaryWriter writer, DateTimeOffset value)
        {
            writer.Write(value.UtcTicks);
        }
    }
}
