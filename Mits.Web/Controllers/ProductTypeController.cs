﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using System.Configuration;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class ProductTypeController : MITSApplicationController
    {
        
         private ProductTypeBO _productTyeBO;

         private AccessRights AccessRightsProductType
         {
             get
             {
                 //Authorization authorization = new Authorization();
                 return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.ProductType);
             }
         }
      
        public ProductTypeController(ISessionCookieValues sessionValue)
             : this(sessionValue, new ProductTypeBO())
        {
        }

        public ProductTypeController(ISessionCookieValues sessionValue, ProductTypeBO productTyeBO)
            : base(sessionValue)
        {
            _productTyeBO = productTyeBO;                   
        }
      
        
        
        //
        // GET: /ProductType/

        public ActionResult Index()
        {
            if (AccessRightsProductType != null && AccessRightsProductType.Read.HasValue == true && AccessRightsProductType.Read == true)
            {
                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                    return View(new ProductTypeViewModel());
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");

                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");

            }
        }

        //Getting detail against a particular product

        public ActionResult GetDetail(string productType)
        {
            int _id = int.Parse(productType);
            ProductType data = new MITSService<ProductType>().GetSingle(x => x.Id == _id);
            try
            {
                if (data.SiteImageID == null)
                    data.SiteImageID = 0;
                if (data.WeightQuantityTypeID == null)
                    data.WeightQuantityTypeID = 0;
                if (data.ContainerQuantityTypeId == null)
                    data.ContainerQuantityTypeId = 0;
            }
            catch (Exception ex)
            {
                data.SiteImageID = 0;
                data.WeightQuantityTypeID = 0;
                data.ContainerQuantityTypeId = 0;
            }
            if (data != null)
            {

                var jsonData = new
                {

                    id = data.Id,
                    Name = data.Name,
                    Desc = data.Description,
                    Notes=data.Notes,
                    Image = data.SiteImageID,
                    Weight = data.EstimatedWeight,
                    WeightQuantity = data.WeightQuantityTypeID,
                    Container = data.EstimatedItemsPerContainer,
                    ContainerType = data.ContainerQuantityTypeId
                };
                    return Json(jsonData);
               
                
            }

            return Json(new
            {
                Id = 0,
                Name = string.Empty,
                Desc = string.Empty,
                Value = string.Empty
            });
        }

        //Invokes 'Add' function of ProductTypeBO and send response back to the view

        public ActionResult Save(string productType, string name, string desc, string notes, string imageId, string weight, string weightQuantity, string container, string containerType)
        {
            if (AccessRightsProductType != null && AccessRightsProductType.Add.HasValue == true && AccessRightsProductType.Add == true || (AccessRightsProductType.Update.HasValue == true && AccessRightsProductType.Update == true))
            {
                Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();

                try
                {
                    _returnDictionaryValue = _productTyeBO.Add(productType, name, desc, notes, imageId, weight, weightQuantity, container, containerType);
                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }
                if (_returnDictionaryValue["duplicate"] == 0)
                {
                    var data = _productTyeBO.GetAll();
                    var jsonData = new
                    {
                        success = true,
                        message = ConfigurationManager.AppSettings["RecordSaved"],
                        selected = _returnDictionaryValue["selectedId"].ToString(),
                        data = (
                            from m in data
                            select new
                            {
                                Value = m.Id,
                                Text = m.Name

                            }).ToArray()
                    };
                    return Json(jsonData);
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                    });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });
            }
        }



    }
}
