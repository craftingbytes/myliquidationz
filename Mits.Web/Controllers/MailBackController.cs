﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.BusinessObjects;
using Mits.BLL;
using Mits.DAL.EntityModels;
using Mits.Common;
using System.Data;

namespace Mits.Web.Controllers
{
    public class MailBackController : MITSApplicationController
    {
        MailBackBO mailBackBO;
        public MailBackController(ISessionCookieValues sessionValues)
            : this(sessionValues, new MailBackBO(sessionValues))
        {
        }

        public MailBackController(ISessionCookieValues sessionValues, MailBackBO _mailBackBO)
            : base(sessionValues)
        {
            mailBackBO = _mailBackBO;

        }

        public ActionResult Index()
        {
            ViewData["editOnLoad"] = true;
            SelectList statesList = null;
           if (Request.UrlReferrer != null && Request.UrlReferrer.AbsolutePath.ToLower().Contains("/mailback/shippingaddress"))
            {
                ViewData["editOnLoad"] = false;
            }

            Affiliate _affiliate = new AffiliateBO().GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));

            ViewData["Zip"] = "";
            ViewData["City"] = "";
            ViewData["State"] = "";

            int selectedState = 0;
            if (_affiliate != null && _affiliate.StateId.HasValue)
            {
                selectedState = _affiliate.StateId.Value;

                ViewData["Zip"] = _affiliate.Zip;
                ViewData["City"] = _affiliate.City.Name;
                ViewData["State"] = _affiliate.State.Name;
            }
            statesList = new SelectList(mailBackBO.GetStates(), "Id", "Name", selectedState);
            ViewData["States"] = statesList;

       

            return View();
        }

        [HttpGet]
        public ActionResult GetManufacturerList()
        {
            var manufacturers = mailBackBO.GetAllOEMs();

            string htmlSelect = "";
            foreach (Affiliate oem in manufacturers)
            {
                htmlSelect += oem.Id + ":" + oem.Name + ";";
            }

            ContentResult result = Content(htmlSelect.TrimEnd(';'));

            return result;
        }
       
        [HttpGet]
        public ActionResult ShippingAddress(string addressInfo)
        {
            if (!string.IsNullOrEmpty(addressInfo))
            {
                string[] address = addressInfo.Split(new char[] { ' ' });

                ViewData["City"] = string.IsNullOrEmpty(address[0]) ? "" : address[0];
                ViewData["State"] = string.IsNullOrEmpty(address[1]) ? "" : address[1];
                ViewData["Zip"] = string.IsNullOrEmpty(address[2]) ? "" : address[2];
            }
            
            if (mailBackBO.ConsumerDetails != null)
            {
                ViewData["FirstName"] = mailBackBO.ConsumerDetails.FirstName;
                ViewData["LastName"] = mailBackBO.ConsumerDetails.LastName;
                ViewData["Address"] = mailBackBO.ConsumerDetails.Address;
                ViewData["Phone"] = mailBackBO.ConsumerDetails.Phone;
                ViewData["City"] = mailBackBO.ConsumerDetails.City;
                ViewData["State"] = mailBackBO.ConsumerDetails.State;
                ViewData["Zip"] = mailBackBO.ConsumerDetails.Zip;
                ViewData["Email"] = mailBackBO.ConsumerDetails.Email;
            }

      
            
            return View();
        }
        [HttpPost]
        public ActionResult ShippingAddress(FormCollection collection)
        {
             Consumer _consumer = new Consumer();
            _consumer.FirstName = Request["txtFirstName"];
            _consumer.LastName = Request["txtLastName"];
            _consumer.Address = Request["txtAddress"];
            _consumer.City = Request["txtCity"];
            _consumer.State = Request["txtState"];
            _consumer.Zip = Request["txtPostalCode"];
            _consumer.Email = Request["txtEmail"];
            _consumer.Phone = Request["txtPhone"];

            mailBackBO.ConsumerDetails = _consumer;

            return RedirectToAction("ShippingValidate");
        }

        [HttpGet]
        public ActionResult ShippingValidate()
        {
            Consumer _consumer = mailBackBO.ConsumerDetails;

            ViewData["Name"] = _consumer.FirstName + " " + _consumer.LastName;
            ViewData["Address"] = _consumer.Address + " " + _consumer.City + "," + _consumer.State + " " + _consumer.Zip;
            ViewData["Email"] = _consumer.Email;

            return View();
        }

        [HttpGet]
        public ActionResult ShippingComplete()
        {
            Consumer consumer = new Consumer();
            DataTable datatable = mailBackBO.MailBackDevices;
            string state = string.Empty;
            if (datatable != null && datatable.Rows.Count > 0)
            {
                  ViewData["MailbackSuccess"] = mailBackBO.SendMailBack();

                  ViewData["AdminEmail"] = ConfigHelper.AdminEmail;
            }

            return View();
        }

        [HttpGet]
        public ActionResult GetBoxSizes()
        {

            string htmlSelect = "";

            htmlSelect += "1:Small 2x3x3;2:Medium 7x5x5;3:Large 10x15x15;";

            return Content(htmlSelect.TrimEnd(';'));
        }
        [HttpGet]
        public ActionResult GetDevicesList()
        {
            Affiliate _affiliate = new AffiliateBO().GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));

            
            IList<ProductType> _devices = mailBackBO.GetStateDevices(_affiliate.State == null ? "" : _affiliate.State.Abbreviation);

            string htmlSelect = "";
            foreach (ProductType device in _devices)
            {

                htmlSelect += device.Id + ":" + device.Name + ";";
            }

            return Content(htmlSelect.TrimEnd(';'));

        }
        //[HttpGet]
        //public ActionResult GetDeviceImage()
        //{

        //    UserInfo userinfo = new UserInfo();
        //    Ewo.sqlDataStore.Consumer sql = new Ewo.sqlDataStore.Consumer();
        //    string sCity = userinfo.City;
        //    string sState = userinfo.StateProvince;
        //    string sPostalCode = userinfo.PostalCode;
        //    DataSet dataSet = sql.ConsumerProductType(sCity, sState, sPostalCode);
        //    DataTable dataTable = dataSet.Tables[0];
        //    int productTypeId = Convert.ToInt32(Request["ProductTypeId"]);

        //    var results = from myRow in dataTable.AsEnumerable()
        //                  where myRow.Field<int>("ProductTypeId") == productTypeId
        //                  select myRow;
        //    string imagePath = "";
        //    foreach (DataRow row in results)
        //    {
        //        imagePath = Convert.ToString(row["SiteImagePath"]);
        //    }


        //    //string htmlSelect = "";
        //    //foreach (DataRow m in dataTable.Rows)
        //    //{

        //    //    htmlSelect += m["ProductTypeID"] + ":" + m["ProductTypeName"] + ";";
        //    //}

        //    return Content(imagePath);
        //}

        [HttpPost]
        public ActionResult GetDevicesList(string city, string state, string zip)
        {

            ViewData["City"] = city;
            ViewData["State"] = state;
            ViewData["Zip"] = zip;


            IList<ProductType> _devices = mailBackBO.GetStateDevices(string.IsNullOrEmpty(state) ? "" :
                state);


            string htmlSelect = "";
            foreach (ProductType device in _devices)
            {

                htmlSelect += device.Id + ":" + device.Name + ";";
            }

            return Content(htmlSelect.TrimEnd(';'));

        }


        [HttpGet]
        public JsonResult GetSelectedDevices()
        {
            string[] devices =mailBackBO.ConsumerProductTypes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries); ;
            string mfgId = mailBackBO.ConsumerProductManufacturerID;

            DataTable datatable = mailBackBO.MailBackDevices;
            
            DataRow row;

            if (datatable == null)
            {
                datatable = CreateMailBackDeviceDataTable();
                foreach (string item in devices)
                {
                    row = datatable.NewRow();
                    row["Manufacturer"] = mfgId.ToString();
                    row["Type"] = item.Trim();
                    row["Model"] = "";
                    row["Size"] = "";
                    row["Quantity"] = "";
                    row["NeedBox"] = "No";
                    row["BoxSize"] = "";
                    row["BoxesQuantity"] = "";
                    row["Weight"] = "";
                    row["Dimension"] = "";
                    datatable.Rows.Add(row);
                }
                datatable.AcceptChanges();
                mailBackBO.MailBackDevices = datatable;
            }

            

            int indexer = 0;
            var jsonData = new
            {
                total = 1,
                page = 1,
                records = 5,
                rows = (
                    from m in datatable.AsEnumerable()
                    select new
                    {
                        id = indexer++,
                        cell = new object[] { 
                            m["Manufacturer"],
                            m["Type"],
                            m["Model"],
                            m["Size"],
                            m["Quantity"],
                            m["Weight"],
                            m["Dimension"],
                            m["NeedBox"],
                            m["BoxSize"],
                            m["BoxesQuantity"]
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
      

        [HttpPost]
        public JsonResult Save(FormCollection form)
        {
            
            DataTable datatable = mailBackBO.MailBackDevices;
            DataRow row = null;

            int id = -1;
            if (datatable == null)
            {
                datatable = CreateMailBackDeviceDataTable();
            }
            if (form["oper"] == "edit")
            {
                id = Convert.ToInt32(Request["id"]);

                row = datatable.Rows[id];
                row = FillEditRow(row);

            }
            else if (form["oper"] == "add")
            {
                row = datatable.NewRow();
                FillMailBackItemRow(ref row);

                datatable.Rows.Add(row);
                id = datatable.Rows.IndexOf(row);
            }
            else if (form["oper"] == "del")
            {
                id = Convert.ToInt32(Request["id"]);
                if (datatable.Rows.Count >= id)
                {
                    datatable.Rows.RemoveAt(id);
                }

            }
            if (row != null && row["Type"] != null)
            {
                row["TypeName"] = mailBackBO.GetProductByTypeId(Convert.ToInt32(row["Type"])).Name;
                var manufacturer = mailBackBO.GetAllOEMs();
                
                var affObject = manufacturer.FirstOrDefault<Affiliate>(i => i.Id == Convert.ToInt32(row["Manufacturer"]));

                if (affObject != null)
                {
                    row["ManufacturerName"] = affObject.Name;
                }
            }

            datatable.AcceptChanges();

            mailBackBO.MailBackDevices = datatable;

            if (form["oper"] != "del")
            {
                var device = new { id = id };


                return Json(device, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ClearDevices()
        {
            DataTable datatable = mailBackBO.MailBackDevices;
            mailBackBO.MailBackDevices = CreateMailBackDeviceDataTable();

            return Content(string.Empty);
        }
        private void FillMailBackItemRow(ref DataRow row)
        {
            row["BoxSize"] = Request["BoxSize"];
            row["BoxesQuantity"] = Request["BoxesQuantity"];
            row["Manufacturer"] = Request["Manufacturer"];
            row["Model"] = Request["Model"];
            row["NeedBox"] = Request["NeedBox"];
            row["Quantity"] = Request["Quantity"];
            row["Size"] = Request["Size"];
            row["Type"] = Request["Type"];
            row["Weight"] = Request["Weight"];
            row["Dimension"] = Request["Dimension"];
            //row["oper"] = Request["oper"];
        }
        private DataTable CreateMailBackDeviceDataTable()
        {
            DataTable datatable = new DataTable("Devices");
            datatable.Columns.Add("BoxSize");
            datatable.Columns.Add("BoxesQuantity");
            datatable.Columns.Add("Manufacturer");
            datatable.Columns.Add("ManufacturerName");
            datatable.Columns.Add("Model");
            datatable.Columns.Add("NeedBox");
            datatable.Columns.Add("Quantity");
            datatable.Columns.Add("Size");
            datatable.Columns.Add("Type");
            datatable.Columns.Add("TypeName");
            datatable.Columns.Add("oper");
            datatable.Columns.Add("Weight");
            datatable.Columns.Add("Dimension");
            return datatable;
        }
        private DataRow FillEditRow(DataRow row)
        {

            foreach (string item in Request.Params.AllKeys)
            {
                if (item.Equals("BoxSize")) row[item] = Request[item];
                if (item.Equals("BoxesQuantity")) row[item] = Request[item];
                if (item.Equals("Manufacturer")) row[item] = Request[item];
                if (item.Equals("Model")) row[item] = Request[item];
                if (item.Equals("NeedBox")) row[item] = Request[item];
                if (item.Equals("Quantity")) row[item] = Request[item]; ;
                if (item.Equals("Size")) row[item] = Request[item];
                if (item.Equals("Type")) row[item] = Request[item];
                if (item.Equals("Weight")) row[item] = Request[item];
                if (item.Equals("Dimension")) row[item] = Request[item];
            }
            return row;
        }
    }
}
