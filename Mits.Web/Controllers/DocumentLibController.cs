﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.BusinessObjects;
using Mits.DAL.EntityModels;
using System.Collections;
using System.IO;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class DocumentLibController : MITSApplicationController
    {
        private AccessRights AccessRightsDocumentLib
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.DocumentLib);
            }
        }


        private DocumentLibBO _documentLibBO;
        public DocumentLibController(ISessionCookieValues sessionValues)
            : this(sessionValues, new DocumentLibBO())
        {
        }

        public DocumentLibController(ISessionCookieValues sessionValues, DocumentLibBO documentLibBO)
            : base(sessionValues)
        {
            _documentLibBO = documentLibBO;
        
        }


        [HttpGet]
        public ActionResult Add()
        {
            if (AccessRightsDocumentLib != null && AccessRightsDocumentLib.Add.HasValue && AccessRightsDocumentLib.Add.Value)
            {
                var statesList = new SelectList(new MITSService<State>().GetAll(), "Id", "Name");
                IList<State> unSortedList = new MITSService<State>().GetAll();
                IEnumerable<State> enumSorted = unSortedList.OrderBy(f => f.Name);
                try
                {
                    statesList = new SelectList(enumSorted.ToList(), "Id", "Name");
                }
                catch (Exception ex)
                {
                    statesList = new SelectList(unSortedList, "Id", "Name");
                }
                ViewData["States"] = statesList;
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }

        }
       [HttpPost]
        public ActionResult Add(FormCollection formCollection)
        {

            string title = Request.Form["Title"];
            string tags = Request.Form["Tags"];
            string description = Request.Form["Desc"];
            string uploadDate = Request.Form["UpDate"];
            string fileName = Request.Form["filePath"];
            string State = Request.Form["State"];
            string PlanYear = Request.Form["PlanYear"];


            bool isSuccess = _documentLibBO.AddDocumentLib(title, tags, description, uploadDate, fileName, State, PlanYear);
            //return view
            if (isSuccess)
            {
                return RedirectToAction("List", "DocumentLib");

            }
            else
            {
                return RedirectToAction("Add", "DocumentLib");
            }
        }

       [HttpGet]
       public ActionResult List()
       {
           if (AccessRightsDocumentLib != null && AccessRightsDocumentLib.Read.HasValue && AccessRightsDocumentLib.Read.Value)
           {
               ViewData["States"] = new SelectList(_documentLibBO.GetStates(), "Id", "Name");
               return View();
           }
           else
           {
               return RedirectToAction("AccessDenied", "Home");
           }

       }
       [HttpGet]
       public ActionResult GetDocumentLibList(string sidx, string sord, int page, int rows, string State, bool _search, string searchString, string PlanYear)
       {

           int pageIndex = Convert.ToInt32(page) - 1;
           int planyear = 0;
           int.TryParse(PlanYear, out planyear);
           int pageSize = rows;
           int totalRecords;
           var data = _documentLibBO.GetPaged(sidx, sord, pageIndex, pageSize, searchString, State,planyear, out totalRecords).AsEnumerable<DocumentLib>();
           int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
           var jsonData = new
           {
               total = totalPages,
               page = page,
               records = totalRecords,
               rows = (
                   from m in data

                   select new
                   {
                       id = m.Id,
                       cell = new object[] { 
                           m.Id,
                           (m.State==null) ?"":m.State.Name,
                           m.Title,
                           m.Description,
                           m.Tags,
                           //(m.UploadDate!=null)?Convert.ToDateTime(m.UploadDate).ToString("MM/dd/yyyy"):string.Empty,
                           m.PlanYear,
                           (!string.IsNullOrEmpty(m.AttachmentPath)) ? m.AttachmentPath : "-"

                        }
                   }).ToArray()
           };
           return Json(jsonData, JsonRequestBehavior.AllowGet);
       }

       /// <summary>
       /// Generic function to perform operations from Admin like Add,Edit,Delete based on Certain criteria         
       /// </summary>        
       /// <Array of custom params>Array of cutom params passed to stored proc</param>        
       public ActionResult PerformAction(DocumentLib obj)
       {
           try
           {

               System.Int32 id;


               switch (Request.Form["oper"])
               {


                   case "edit":
                       obj.Id = System.Int32.Parse(Request.Form["id"]);
                       obj.StateId = System.Int32.Parse(Request.Form["State"]);
                       obj.PlanYear = System.Int32.Parse(Request.Form["Year"]);
                       var editResult = _documentLibBO.EditDocumentLib(obj);

                       break;

                   case "del":
                       if (AccessRightsDocumentLib != null)
                       {
                           if (AccessRightsDocumentLib.Delete.HasValue
                               && AccessRightsDocumentLib.Delete.Value)
                           {

                               id = System.Int32.Parse(Request.Form["id"]);
                               var delResult = _documentLibBO.DeleteDocumentLib(id);
                           }
                           else
                           {
                               return RedirectToAction("AccessDenied", "Home");
                           }
                       }
                       else
                       {
                           return RedirectToAction("AccessDenied", "Home");
                       }

                       break;

                   default:
                       break;
               }


               return RedirectToAction("List", "DocumentLib");


           }
           catch (Exception ex)
           {

           }

           return View("DocumentLib/List");
       }
       public DownloadResult Download()
       {
           string fileName = Convert.ToString(Request.QueryString["FileName"]); 
          // string fileName = cellvalue;
           if (fileName != "-")
           {
               fileName = fileName.Replace("<~>", "#");
               return new DownloadResult { VirtualPath = "~/Uploads/" + fileName, FileDownloadName = fileName };
           }
           else
           {
               return null;
           }
       }

       public ActionResult GetDropDownsData()
       {
           string strStates = ":Select;";
           foreach (State s in _documentLibBO.GetStates())
           {
               strStates += s.Id + ":" + s.Name + ";";
           }
           
           var jsonData = new
           {
               states = strStates.TrimEnd(';'),
           };
           return Json(jsonData, JsonRequestBehavior.AllowGet);
       }

       public ActionResult GetUserAccessRights()
       {
           var jsonData = new
           {
               showEdit = AccessRightsDocumentLib.Update.HasValue && AccessRightsDocumentLib.Update.Value,
               showDelete = AccessRightsDocumentLib.Delete.HasValue && AccessRightsDocumentLib.Delete.Value
           };
           return Json(jsonData, JsonRequestBehavior.AllowGet);
       }


    }

    public class DownloadResult : ActionResult
    {

        public DownloadResult()
        {
        }

        public DownloadResult(string virtualPath)
        {
            this.VirtualPath = virtualPath;
        }
        public string VirtualPath
        {
            get;
            set;
        }

        public string FileDownloadName
        {
            get;
            set;
        }

        public override void ExecuteResult(ControllerContext context)
        {

            if (!String.IsNullOrEmpty(FileDownloadName))
            {

                string ext = Path.GetExtension(FileDownloadName);
                string type = "";

                switch (ext.ToLower())
                {

                    case ".htm":
                    case ".html":
                    case ".log":
                        type = "text/HTML";
                        break;
                    case ".txt":
                        type = "text/plain";
                        break;
                    case ".doc":
                        type = "application/ms-word";
                        break;
                    case ".tiff":

                    case ".tif":
                        type = "image/tiff";
                        break;
                    case ".asf":
                        type = "video/x-ms-asf";
                        break;
                    case ".avi":
                        type = "video/avi";
                        break;
                    case ".zip":
                        type = "application/zip";
                        break;
                    case ".xls":
                    case ".csv":
                        type = "application/vnd.ms-excel";
                        break;
                    case ".gif":
                        type = "image/gif";
                        break;
                    case ".jpg":
                    case "jpeg":
                        type = "image/jpeg";
                        break;
                    case ".bmp":
                        type = "image/bmp";
                        break;
                    case ".wav":
                        type = "audio/wav";
                        break;
                    case ".mp3":
                        type = "audio/mpeg3";
                        break;
                    case ".mpg":
                    case "mpeg":
                        type = "video/mpeg";
                        break;
                    case ".rtf":
                        type = "application/rtf";
                        break;
                    case ".asp":
                        type = "text/asp";
                        break;
                    case ".pdf":
                        type = "application/pdf";
                        break;
                    case ".fdf":
                        type = "application/vnd.fdf";
                        break;
                    case ".ppt":
                        type = "application/mspowerpoint";
                        break;
                    case ".dwg":
                        type = "image/vnd.dwg";
                        break;
                    case ".msg":
                        type = "application/msoutlook";
                        break;
                    case ".xml":
                    case ".sdxl":
                        type = "application/xml";
                        break;
                    case ".xdp":
                        type = "application/vnd.adobe.xdp+xml";
                        break;
                    default:
                        type = "application/octet-stream";
                        break;
                }
                context.HttpContext.Response.AppendHeader("content-disposition", "attachment; filename=" + HttpContext.Current.Server.UrlEncode(FileDownloadName));
                context.HttpContext.Response.ContentType = type;
                string serverPath = context.HttpContext.Server.MapPath(this.VirtualPath);
                //if (File.Exists(serverPath))
                {
                    context.HttpContext.Response.WriteFile(serverPath);
                }
               
            }


        }
    }
}
