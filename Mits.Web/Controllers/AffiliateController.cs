﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.BusinessObjects;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.Common;
using Mits.BLL.Authorization;
using System.Configuration;
using Mits.Web.Controllers;
using System.IO;

namespace Mits.BLL.BusinessObjects
{
    public class AffiliateController : Mits.Web.Controllers.MITSApplicationController
    {
        private AffiliateBO _affiliateBO;
      
        private AccessRights AccessRightsAffiliateSearch 
        {
            get 
            {
                //Authorization.Authorization authorization = new Authorization.Authorization();
                return Authorization.Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.EntitySearch);     
            }
        }

        private AccessRights AccessRightsAffiliate
        {
            get
            {
                //Authorization.Authorization authorization = new Authorization.Authorization();
                return Authorization.Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Entity);
            }
        }
        private AccessRights AccessRightsMap
        {
            get
            {
                //Authorization.Authorization authorization = new Authorization.Authorization();
                return Authorization.Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Map);
            }
        }
        private Affiliate SelectedAffiliate
        {
            get
            {
                return new AffiliateBO().GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId));
            }
        }

        private Int32 CurrentAffiliateID
        {
            set 
            {
                _sessionValues.SetSessionValue<int>(_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId) + Constants.SessionParameters.CurrentAffiliateID, value);
            }
            get 
            {
                return _sessionValues.GetSessionIntValue(_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId) + Constants.SessionParameters.CurrentAffiliateID);
            }
        }

        public AffiliateController(ISessionCookieValues sessionValues)
            : this(sessionValues, new AffiliateBO())
        {
            
        }

        public AffiliateController(ISessionCookieValues sessionValues, AffiliateBO affiliateBO)
            : base(sessionValues)
        {
            ViewData["AccessRights"] = AccessRightsAffiliate;
            _affiliateBO = affiliateBO;
        
        }
        public ActionResult Index()
        {
            if (AccessRightsAffiliateSearch != null && AccessRightsAffiliateSearch.Read == true)
            {
                ViewData["EntityType"] = _affiliateBO.GetAffiliateType();   
                ViewData["Data"] = "{" + Request["data"] + "}";
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetAffiliates(int page, int rows, string searchString , string searchEntity)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = _affiliateBO.GetAffiliatesPaged(pageIndex, pageSize, searchString, searchEntity, out totalRecords).AsEnumerable<Affiliate>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                           m.Id,
                            m.Name,
                            m.AffiliateType!=null ?m.AffiliateType.Type:"",
                            m.Address1,
                            m.State!=null ?m.State.Name:"",
                            m.Phone,
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }
        
        public ActionResult GetCities(string state)
        {
            int stateId = int.Parse(state);
            IList<City> data;
            var data1 = new MITSService<City>().GetAll(x => x.StateId == stateId);
            IEnumerable<City> enumList = data1.OrderBy(f => f.Name);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = data1;
            }

            City _city = new City();
            _city.Id = 0;
            _city.Name = "Select";
            data.Insert(0, _city);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name

                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetAffiliateRoles(int affiliateTypeId)
        {
            var service = new MITSService<Role>();
            List<Role> roles = service.GetAll(x => x.AffiliateTypeId == affiliateTypeId).ToList();
            int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
            roles.AddRange(service.GetAll(x => x.CreateByAffiliateId == affiliateId).ToList());
            roles.Insert(0, new Role() { Id = 0, RoleName = "Select" });
            var jsonData = new
            {
                data = (
                    from m in roles
                    select new
                    {
                        Value = m.Id,
                        Text = m.RoleName

                    }).ToArray()
            };
            return Json(jsonData);
        }

        // GET: /Affiliate/Create

        public ActionResult Create(int? saved)
        {
            if (AccessRightsAffiliate != null && AccessRightsAffiliate.Add == true)
            {
                
                Affiliate _aff = new Affiliate();
                if (saved.HasValue)
                {
                    ViewData["saved"] = "true";
                }
                return View(new AffiliateViewModel(_sessionValues, _aff));
               
                
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }  
        }

        //
        // POST: /Affiliate/Create

        [HttpPost]
        public ActionResult Create(Affiliate affiliate)
        {
            try
            {                
                AffiliateViewModel model = new AffiliateViewModel(_sessionValues, affiliate);                
                string services = Request["selectedObjects"];
                int roleId = Convert.ToInt32(Request["affiliateRoleId"]);
                List<Message> messages = _affiliateBO.Add(affiliate, services, roleId);
                model.Messages = messages;
                foreach (Message message in messages)
                {
                    if (message.Type == MessageType.BusinessError.ToString())
                    {
                        return View("Create", model);
                    }
                }
                Affiliate _aff = new Affiliate();
                AffiliateViewModel newModel = new AffiliateViewModel(_sessionValues, _aff);
                newModel.Messages = messages;
                foreach (Message msg in messages)
                {
                    if (msg.Type == MessageType.Success.ToString())
                    {
                        return RedirectToAction("Create", new { saved = 1 });
                    }
                }
                return View("Create", newModel);                
            }
            catch(Exception ex)
            {
                return RedirectToAction("Create");                
            }
        }

        public ActionResult Edit(int id)
        {
            CurrentAffiliateID = id;
            Affiliate affiliate = _affiliateBO.GetAffiliate(id);
            if (affiliate == null)
            {
                return RedirectToAction("Error", "Home");
            }
            AffiliateViewModel model = new AffiliateViewModel(_sessionValues, affiliate);
            ViewData["Name"] = affiliate.Name;
            ViewData["Data"] = Request["data"];
            return View("Edit", model);
        }

        //
        // POST: /Affiliate/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection form)
        {            
            try
            {
                CurrentAffiliateID = id;
                Affiliate affiliateModel = _affiliateBO.GetAffiliate(id);
                AffiliateViewModel model = new AffiliateViewModel(_sessionValues, affiliateModel);
                TryUpdateModel(affiliateModel);                                
                string services = Request["selectedObjects"];
                int roleId = Convert.ToInt32(Request["affiliateRoleId"]);
                List<Message> messages = _affiliateBO.Update(affiliateModel, services, roleId);

                SaveAttachment(Request.Form["txtUploadedFileName"], id);


                AffiliateViewModel updatedModel = new AffiliateViewModel(_sessionValues, affiliateModel);
                updatedModel.Messages = messages;
                ViewData["Name"] = affiliateModel.Name;
                ViewData["Data"] = Request["data"];
                return View("Edit", updatedModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Edit", id);
            }
        }
        /// <summary>
        /// Generic function to perform operations from Admin like Add,Edit,Delete based on Certain criteria         
        /// </summary>        
        /// <Array of custom params>Array of cutom params passed to stored proc</param>        
        public ActionResult PerformAction(Affiliate obj)
        {
            try
            {

                System.Int32 id;


                switch (Request.Form["oper"])
                {


                    //case "edit":
                    //    obj.Id = System.Int32.Parse(Request.Form["id"]);
                    //    obj.StateId = System.Int32.Parse(Request.Form["State"]);
                    //    var editResult = _documentLibBO.EditDocumentLib(obj);

                    //    break;

                    case "del":
                        if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId) == (int)Common.Constants.AffiliateContactRole.Administrator)
                        {
                            id = System.Int32.Parse(Request.Form["id"]);
                            var delResult = _affiliateBO.DeleteAffiliate(id);
                        }
                        else
                        {
                            return RedirectToAction("AccessDenied", "Home");
                        }

                        break;

                    default:
                        break;
                }


                return RedirectToAction("Index", "Affiliate");


            }
            catch (Exception ex)
            {

            }

            return View("/Affiliate");
        }

        public bool SaveAttachment(string fileName, Int32 invoiceId)
        {
            if (!String.IsNullOrEmpty(fileName) && invoiceId > 0)
            {
                string path = Server.MapPath("~/Uploads/" + fileName);
                String saveLocation = Server.MapPath("~/Uploads/") + Guid.NewGuid();
                Document doc = new Document();

                doc.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                doc.ServerFileName = saveLocation;
                doc.UploadFileName = fileName;
                doc.UploadDate = System.DateTime.Now;

                int savedFileId = _affiliateBO.SaveAttachment(doc);

                DocumentRelation docRel = new DocumentRelation();
                docRel.DocumentID = savedFileId;
                docRel.ForeignID = invoiceId;
                return _affiliateBO.SaveAttachmentRelation(docRel);
            }
            return false;
        }

        public JsonResult IsFileExist(string fileName)
        {
            if (fileName != "-")
            {
                fileName = fileName.Replace("<~>", "#");
                string virtualPath = "~/Uploads/" + fileName.Replace(" ", " ");
                string serverPath = ControllerContext.HttpContext.Server.MapPath(virtualPath);
                if (System.IO.File.Exists(serverPath))
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }
            }
            else
            {
                return null;
            }
        }

        public DownloadResult Download(string fileName)
        {
            //string fileName = Convert.ToString(Request.QueryString["FileName"]);

            if (fileName != "-")
            {
                fileName = fileName.Replace("<~>", "#");
                string virtualPath = "~/Uploads/" + fileName.Replace(" ", " ");
                string serverPath = ControllerContext.HttpContext.Server.MapPath(virtualPath);
                return new DownloadResult { VirtualPath = virtualPath, FileDownloadName = fileName };
            }
            else
            {
                return null;
            }
        }
        [HttpGet]
        public ActionResult Map(int ? id) 
        {
            if (AccessRightsMap != null && AccessRightsMap.Read.HasValue == true && AccessRightsMap.Read == true)
            {
                int affilitateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                if (affilitateId > 0)
                {
                    if (id.HasValue)
                    {
                        _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, (int)id);
                    }
                    else
                    {
                        _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, affilitateId);
                    }
                    MapXMLHandler.CreateMapXMLFile(affilitateId, new MITSService<AffiliateTarget>().GetAll(X => X.AffiliateId == affilitateId).ToList());
                    ViewData["MapOwnerName"] = SelectedAffiliate.Name;
                    ViewData["Data"] = Request["data"];
                    ViewData["AffiliateTypeId"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                    ViewData["SelectedAffiliateId"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId);
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Home");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public JsonResult GetAttachments()
        {
            List<Document> docs = new AffiliateBO().GetDocsByPDataId(CurrentAffiliateID);

            string html = "No attachment found.";
            if (docs.Count > 0)
            {
                html = "<table width='100%'>";
                foreach (Document doc in docs)
                {
                    html += "<tr><td width='80%'>"
                          + "<a href=javascript:Download('" + doc.UploadFileName + "'); style='color: #FF9900; text-decoration: none; _text-decoration: none;'>"
                          + "<img src='../../Content/images/d4.PNG' alt='Open selected file' title='Open selected file' id='btnDel' border='0' />"
                          + ": <font style='font-weight: bold; vertical-align: top;'>" + doc.UploadFileName + "</font> </a>"
                          + "</td><td width='20%'>"
                          + "<img src='../../Content/images/icon/delete.png' alt='Remove attachment' style='cursor:hand;' title='Remove attachment' id='btnDel' border='0' onclick='javascript:DeleteAttachment(" + doc.Id + ");' />"
                          + "</td></tr>";
                }
                html += "</table>";
            }
            return Json(html);
        }
        public JsonResult DeleteAttachment(int? docID)
        {
            bool result = new AffiliateBO().DeleteDocument(docID.Value);

           return Json(result);
        }
    }
}
