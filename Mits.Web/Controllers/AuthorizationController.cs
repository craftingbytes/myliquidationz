﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.Authorization;
using Mits.Common;
using Mits.DAL.EntityModels;
using Mits.BLL.ViewModels;
using Mits.BLL;

namespace Mits.Web.Controllers
{
    public class AuthorizationController : MITSApplicationController
    {
        public AuthorizationController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
        }

        private AccessRights AccessRightsGroupSecurity
        {
            get
            {
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Authorization);
            }
        }
      
        //
        // GET: /Authorization/

        public ActionResult Index()
        {
            if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId) == -1)
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            ViewData["CanAdd"] = "";
            ViewData["CanUpdate"] = "";
            if (IsAdmin())
            {
                return View(new SelectList(GetRoleList(), "Id", "RoleName"));
            }
            else
            {
                ViewData["CanAdd"] = CanAdd() ? "" : "disabled";
                ViewData["CanUpdate"] = CanUpdate() ? "" : "disabled";
                if (CanRead())
                {
                    return View(new SelectList(GetRoleList(), "Id", "RoleName"));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Home");
                }
            }
        }

        public ActionResult GetPermissions(Int32? roleId)
        {
            var data = Authorization.GetPermissions(_sessionValues, roleId);
            var jsonData = new
            {
                page = 1,
                totla = 1,
                records = data.Count,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Area.Id,
                        cell = new object[] { 
                            m.Area.Id,
                            m.Area.Caption,
                            GetPermissionValue(m, "Read"),
                            GetPermissionValue(m, "Add"),
                            GetPermissionValue(m, "Update"),
                            GetPermissionValue(m, "Delete"),
                            m.Area.IsArea.HasValue && m.Area.IsArea.Value, //actions
                            "" + m.Level, //tree level
                            m.Area.ParentId.HasValue ? "" + m.Area.ParentId : "", //tree parent id
                            m.Leaf ? "true" : "false", // is leaf
                            "false" //expand
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateRole(string roleName, string permissions)
        {
            string message = "";
            if (IsAdmin() || CanAdd())
            {
                List<Permission> permissionList = PopulatePermissions(permissions);
                int roleId = Authorization.CreateRole(_sessionValues, roleName, permissionList);
                if (roleId != -1)
                {
                    var jsonData = new
                    {
                        message = "Role: " + roleName + " has been created.",
                        roleId = roleId,
                        roleList = (
                         from m in GetRoleList()
                         select new
                         {
                             Value = m.Id,
                             Text = m.RoleName
                         }).ToArray()
                    };
                    return Json(jsonData);
                }
                else
                {
                    message = "Unable to create role: " + roleName + ".";
                }
            }
            else
            {
                message = "AccessDenied";
            }
            var data = new
            {
                message = message
            };
            return Json(data);
        }

        public ActionResult UpdateRole(int roleId, string roleName, string permissions)
        {
            string message = "";
            if (IsAdmin() || CanUpdate())
            {
                List<Permission> permissionList = PopulatePermissions(permissions);
                int updateId = Authorization.UpdateRole(_sessionValues, roleId, roleName, permissionList);
                if (updateId != -1)
                {
                    message = "Role: " + roleName + " has been updated.";
                }
                else
                {
                    message = "Unable to update role: " + roleName + ".";
                }
            }
            else
            {
                message = "AccessDenied";
            }
            var data = new
            {
                message = message
            };
            return Json(data);
        }

        private bool CanRead()
        {
            return AccessRightsGroupSecurity != null && AccessRightsGroupSecurity.Read.HasValue && AccessRightsGroupSecurity.Read.Value;
        }

        private bool CanAdd()
        {
            return AccessRightsGroupSecurity != null && AccessRightsGroupSecurity.Add.HasValue && AccessRightsGroupSecurity.Add.Value;
        }

        private bool CanUpdate()
        {
            return AccessRightsGroupSecurity != null && AccessRightsGroupSecurity.Update.HasValue && AccessRightsGroupSecurity.Update.Value;
        }

        private string GetPermissionValue(PermissionNode permission, string type)
        {
            Area area = permission.Area;
            if (area.IsArea.HasValue && !area.IsArea.Value)
            {
                return "-";
            }
            Permission affiliatePermission = permission.AffiliatePermission;
            Permission rolePermission = permission.RolePermission;
            if (type == "Read")
            {
                if (affiliatePermission == null || !affiliatePermission.Read.HasValue || !affiliatePermission.Read.Value)
                {
                    return "-";
                }
                if (rolePermission != null && rolePermission.Read.HasValue && rolePermission.Read.Value)
                {
                    return "1";
                }
                return "0";
            }
            if (type == "Update")
            {
                if (affiliatePermission == null || !affiliatePermission.Update.HasValue || !affiliatePermission.Update.Value)
                {
                    return "-";
                }
                if (rolePermission != null && rolePermission.Update.HasValue && rolePermission.Update.Value)
                {
                    return "1";
                }
                return "0";
            }
            if (type == "Add")
            {
                if (affiliatePermission == null || !affiliatePermission.Add.HasValue || !affiliatePermission.Add.Value)
                {
                    return "-";
                }
                if (rolePermission != null && rolePermission.Add.HasValue && rolePermission.Add.Value)
                {
                    return "1";
                }
                return "0";
            }
            if (type == "Delete")
            {
                if (affiliatePermission == null || !affiliatePermission.Delete.HasValue || !affiliatePermission.Delete.Value)
                {
                    return "-";
                }
                if (rolePermission != null && rolePermission.Delete.HasValue && rolePermission.Delete.Value)
                {
                    return "1";
                }
                return "0";
            }
            return string.Empty;
        }

        private List<Permission> PopulatePermissions(string sPermissions)
        {
            List<Permission> permissionList = new List<Permission>();
            string[] permissionArray = sPermissions.Split(',');
            foreach (string permission in permissionArray)
            {
                string[] values = permission.Split(':');
                if (values.Length != 2 || values[1].Length != 4 || values[1].Replace('0', '-') == "----")
                {
                    continue;
                }
                string sAreaId = values[0];
                string sPermission = values[1];
                Permission p = new Permission();
                p.AreaId = Convert.ToInt32(sAreaId);
                p.Read = sPermission[0] == '1' ? true : false;
                p.Add = sPermission[1] == '1' ? true : false;
                p.Update = sPermission[2] == '1' ? true : false;
                p.Delete = sPermission[3] == '1' ? true : false;
                permissionList.Add(p);
            }
            return permissionList;
        }

        private bool IsAdmin()
        {
            return _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId) == (int)Constants.AffiliateContactRole.Administrator;
        }

        private List<Role> GetRoleList()
        {
            MITSService<Role> roleService = new MITSService<Role>();
            List<Role> roles = roleService.GetAll().ToList();
            if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) != (int)Constants.AffiliateType.Eworld)
            {
                roles = (from m in roles
                         where !m.AffiliateTypeId.HasValue && m.CreateByAffiliateId == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId)
                         select m).ToList();
            }
            Role dummy = new Role() { Id = 0, RoleName = "Select" };
            roles.Insert(0, dummy);
            return roles;
        }
    }
}
