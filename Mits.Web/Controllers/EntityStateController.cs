﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using System.Configuration;
using Mits.BLL.Authorization;
namespace Mits.Web.Controllers
{
    public class EntityStateController : MITSApplicationController
    {
        private EntityStateBO _entityStateBO;

        private AccessRights AccessRightsEntityState
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.EntityState); //change
            }
        }

        public EntityStateController(ISessionCookieValues sessionValues)
            : this(sessionValues, new EntityStateBO()) //change
        {
        }

        public EntityStateController(ISessionCookieValues sessionValues, EntityStateBO entityBO) //change
            : base(sessionValues)
        {
            _entityStateBO = entityBO;
        }
        //
        // GET: /EntityState/
        [HttpGet]
        public ActionResult Index(int? id)
        {
            if (AccessRightsEntityState != null && AccessRightsEntityState.Read.HasValue == true && AccessRightsEntityState.Read == true)
            {
                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                AffiliateBO _affilaiteBo = new AffiliateBO();
                ViewData["Data"] = Request["data"];
                if (id.HasValue)
                {
                    Affiliate _aff = _affilaiteBo.GetAffiliate((int)id);
                    return View(new EntityStateViewModel(_aff));
                }
                else
                {
                    Affiliate _aff = new Affiliate();
                    return View(new EntityStateViewModel(_aff));
                }
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");

                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");

            }
        }
        //Getting all states against a particular entity and sending response back to the view

        public ActionResult GetStates(int entity)
        {
            int _entityId = entity;
            var data = new MITSService<AffiliateState>().GetAll(x => x.AffiliateId == _entityId & x.Active == true);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.StateId,

                    }).ToArray()
            };
            return Json(jsonData);

        }


        public ActionResult Edit(int? id)
        {

            return RedirectToAction("Index", new { id = id });
        }
        //Invokes 'Add' function of EntityStateBO and send response back to the view

        public ActionResult Save(string entity, string states)
        {

            if (AccessRightsEntityState != null && AccessRightsEntityState.Add.HasValue == true && AccessRightsEntityState.Add == true || (AccessRightsEntityState.Update.HasValue == true && AccessRightsEntityState.Update == true))
            {
                List<Message> messages = new List<Message>();
                try
                {
                    if (int.Parse(entity) == 0)
                    {
                        return Json(new
                        {
                            success = false,
                            message = ConfigurationManager.AppSettings["RecordNotSaved"],
                        });
                    }
                    else
                    {
                        messages = _entityStateBO.Add(entity, states);
                    }


                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }
                string mesg = null;
                foreach (var msg in messages)
                {
                    mesg = msg.Text;
                }
                var data = _entityStateBO.GetAllEnity();
                var jsonData = new
                {
                    success = true,
                    message = mesg,
                    data = (
                        from m in data
                        select new
                        {
                            Value = m.Id,
                            Text = m.Name,


                        }).ToArray()
                };
                return Json(jsonData);

            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });

            }

        }

    }
}
