﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Web.UI.WebControls;
using Mits.Common;
using Mits.BLL.BusinessObjects;
using Mits.BLL.Authorization;
using System.IO;

namespace Mits.Web.Controllers
{
    public class AuditReportController : MITSApplicationController
    {
        private AuditReportBO auditReportBO;
        private AccessRights accessRight;

        public AuditReportController(ISessionCookieValues sessionValues)
            : this(sessionValues, new AuditReportBO())
        {
        }

        public AuditReportController(ISessionCookieValues sessionValues, AuditReportBO auditReportBO)
            : base(sessionValues)
        {
            this.accessRight = Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.AuditReport);
            this.auditReportBO = auditReportBO;
        }

        //
        // GET: /AuditReport/

        public ActionResult Index()
        {
            if (CanRead())
            {
                ViewData["States"] = GetStateSelectList();
                //ViewData["Years"] = GetPlanYearSelectList();
                ViewData["Facilitys"] = GetFacilitySelectList();
                ViewData["CanAdd"] = CanAdd() ? "" : "disabled";
                ViewData["CanUpdate"] = CanUpdate() ? "" : "disabled";
                ViewData["CanDelete"] = CanDelete() ? "" : "disabled";
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetAuditReports(int? stateId, string facilityName, string planyear, int page, int rows)
        {
            if (!CanRead())
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }
            int? affiliateId = null;
            if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) == (int)Common.Constants.AffiliateType.OEM)
            {
                affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int totalRecords = 0;

            if (stateId.HasValue && stateId.Value == 0)
            {
                stateId = null;
            }
            var datas = auditReportBO.GetAuditReports(stateId, facilityName, planyear, pageIndex, rows, out totalRecords);
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in datas
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            m.Id,
                            m.PlanYear,
                            m.State.Name,
                            m.FacilityName,
                            m.AuditDate.HasValue ? m.AuditDate.Value.ToString("MM/dd/yyyy") : string.Empty
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Add()
        {
            if (CanAdd())
            {
                ViewData["States"] = GetStateSelectList();
                //ViewData["Years"] = GetPlanYearSelectList();
                ViewData["Facilitys"] = GetFacilitySelectList();
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpPost]
        public ActionResult Add(FormCollection formCollection)
        {
            if (CanAdd())
            {
                string fileNames = formCollection["FileNames"];
                string[] fileNameArray = fileNames.Split(',');
                List<Document> documentList = new List<Document>();
                foreach (string fileName in fileNameArray)
                {
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        Document doc = new Document();
                        doc.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                        doc.ServerFileName = Server.MapPath("~/Uploads/") + Guid.NewGuid();
                        doc.UploadFileName = fileName;
                        doc.UploadDate = System.DateTime.Now;
                        documentList.Add(doc);
                    }
                }
                string statestring = formCollection["State"];
                int[] statelist = Array.ConvertAll( statestring.Split(','), int.Parse); 

                AuditReport auditReport = new AuditReport();
                auditReport.StateId = statelist[0]; // Convert.ToInt32(formCollection["State"]);
                auditReport.FacilityName = formCollection["FacilityName"];
                if (!string.IsNullOrEmpty(formCollection["Auditdate"]))
                {
                    auditReport.AuditDate = DateTime.Parse(formCollection["Auditdate"]);
                }
                if (!string.IsNullOrEmpty(formCollection["PlanYear"]))
                {
                    auditReport.PlanYear = Int32.Parse(formCollection["PlanYear"]);
                }
                //Message message = auditReportBO.AddAuditReport(auditReport, documentList);
                int NewId = auditReportBO.AddAuditReport(auditReport, documentList);

                //var data = new
                //{
                //    type = message.Type,
                //    message = message.Text
                //};

                if (NewId == 0)
                {
                    return Json(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                }
                else if (statelist.Count() > 1) {
                    for (int i = 1; i < statelist.Count(); i++)
                    {
                        MITSEntities entities = new MITSEntities();
                        entities.p_AddAuditReportState(NewId, statelist[i]);
                    }
                }

                //return Json(type=MessageType.Success.ToString(),  Constants.Messages.RecordSaved));
                return Json(new { type = MessageType.Success.ToString(), message = Constants.Messages.RecordSaved.ToString() });
            }
            else
            {
                var data = new
                {
                    type = MessageType.Failure.ToString(),
                    message = "AccessDenied"
                };
                return Json(data);
            }
            
        }

        public string EditAuditReport(FormCollection values)
        {
            string message = "";
            bool status = true;
            if (values["oper"] == "edit")
            {
                if (CanUpdate())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        var auditReport = auditReportBO.GetAuditReportById(id);
                        if (auditReport == null)
                        {
                            message = Constants.Messages.RecordNotSaved;
                            status = false;
                        }
                        else
                        {
                            auditReport.StateId = Convert.ToInt32(values["State"]);
                            auditReport.FacilityName = values["FacilityName"];
                            if (!string.IsNullOrEmpty(values["AuditDate"]))
                            {
                                auditReport.AuditDate = DateTime.Parse(values["AuditDate"]);
                            }
                            else
                            {
                                auditReport.AuditDate = null;
                            }
                            if (auditReportBO.UpdateAuditReport(auditReport))
                            {
                                message = Constants.Messages.RecordSaved;
                                status = true;
                            }
                            else
                            {
                                message = Constants.Messages.RecordNotSaved;
                                status = false;
                            }
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "del")
            {
                if (CanDelete())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = "Record could not be deleted.";
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        if (auditReportBO.DeleteAuditReport(id))
                        {
                            message = "Record has been deleted successfully.";
                            status = true;
                        }
                        else
                        {
                            message = "Record could not be deleted.";
                            status = false;
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else
            {
                message = "Invalid edit operation.";
                status = false;
            }
            return status.ToString() + ":" + message;
        }

        public ActionResult GetDocuments(int auditReportId)
        {
            if (!CanRead())
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }
            var documents = auditReportBO.GetDocuments(auditReportId);
            List<Document> _documents = new List<Document>();
            string uploadPath = Server.MapPath("~/Uploads/");
            foreach (var doc in documents)
            {
                //if (System.IO.File.Exists(uploadPath + doc.UploadFileName))
                //{
                    _documents.Add(doc);
                //}
            }

            var jsonData = new
            {
                rows = (
                    from d in _documents
                    select new
                    {
                        DocumentID = d.Id,
                        UploadFileName = d.UploadFileName
                    }).ToList()

            };

            return Json(jsonData);
        }

        public ActionResult DeleteDocument(int documentId)
        {
            if (CanDelete())
            {
                bool result = new AffiliateBO().DeleteDocument(documentId);
                return Json(result);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }
        }

        public ActionResult AddDocument(int auditReportId, string fileName)
        {
            if (CanAdd())
            {
                Document doc = new Document();
                doc.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                doc.ServerFileName = Server.MapPath("~/Uploads/") + Guid.NewGuid();
                doc.UploadFileName = fileName;
                doc.UploadDate = System.DateTime.Now;

                Message message = auditReportBO.AddDocument(auditReportId, doc);
                var data = new
                {
                    type = message.Type,
                    message = message.Text
                };
                return Json(data);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }
        }

        public ActionResult GetDropDownsData()
        {
            MITSEntities entitities = new MITSEntities();
            //var unSortedStates = new MITSService<State>().GetAll();

            var unSortedStates = (from s in entitities.States
                                 join p in entitities.StateGuidelines on s.Id equals p.StateId
                                 where p.LawState != 20 && p.LawState != 0
                                 select s).Distinct();


            var sortedStates = unSortedStates.OrderBy(f => f.Name).ToList();
            string strStates = ":Select;";
            foreach (State s in sortedStates)
            {
                strStates += s.Id + ":" + s.Name + ";";
            }

            var jsonData = new
            {
                states = strStates.TrimEnd(';'),
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private SelectList GetStateSelectList()
        {
            //var unSortedStates = new MITSService<State>().GetAll();
            MITSEntities entitities = new MITSEntities();
            var unSortedStates = (from s in entitities.States
                                  join p in entitities.StateGuidelines on s.Id equals p.StateId
                                  where p.LawState != 20 && p.LawState != 0
                                  select s).Distinct();

            var sortedStates = unSortedStates.OrderBy(f => f.Name).ToList();
            sortedStates.Insert(0, new State() { Id = 0, Name = "Select" });
            var states = new SelectList(sortedStates, "Id", "Name");
            return states;
        }

        private SelectList GetPlanYearSelectList()
        {
            //var unSortedStates = new MITSService<State>().GetAll();
            MITSEntities entitities = new MITSEntities();
            var unSortedYears = (from y in entitities.PlanYears
                                 select new { y.Year }).Distinct();

            var sortedYears = unSortedYears.OrderByDescending(x => x.Year).ToList();
            sortedYears.Insert(0, new { Year = 0});
            var years = new SelectList(sortedYears, "Years", "Years");
            return years;
        }

        private SelectList GetFacilitySelectList()
        {
            MITSEntities mitsEntities = new MITSEntities();
            var names = (from m in mitsEntities.AuditReports
                         group m by new { m.FacilityName } into g
                         orderby new { g.Key.FacilityName }
                         select new { g.Key.FacilityName }).ToList();
            names.Insert(0, new { FacilityName = "" });
            return new SelectList(names, "FacilityName", "FacilityName");
        }

        private bool CanRead()
        {
            return (accessRight != null && accessRight.Read.HasValue && accessRight.Read.Value);
        }

        private bool CanAdd()
        {
            return (accessRight != null && accessRight.Add.HasValue && accessRight.Add.Value);
        }

        private bool CanUpdate()
        {
            return (accessRight != null && accessRight.Update.HasValue && accessRight.Update.Value);
        }

        private bool CanDelete()
        {
            return (accessRight != null && accessRight.Delete.HasValue && accessRight.Delete.Value);
        }

        public ActionResult FileExists(FormCollection forms) { 
            Dictionary<string, string> fileArray = new Dictionary<string, string>(); 
            foreach (string key in forms.AllKeys) 
            { 
                if (key != "folder") 
                { 
                    string targetDirectory = Path.Combine(Request.PhysicalApplicationPath, @"uploads\"); 
                    string targetFilePath = Path.Combine(targetDirectory, forms[key]); 
                    if (System.IO.File.Exists(targetFilePath)) 
                        fileArray.Add(key, forms[key]); 
                } 
            } 
                    
            return Json(fileArray); 
        }
    }
}
