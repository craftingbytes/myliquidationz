﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.BusinessObjects;
using Mits.DAL.EntityModels;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class NewsFeedController : MITSApplicationController
    {

        private NewsFeedBO _NewsFeedBO;

        private AccessRights AccessRightsAccreditation
        {
            get
            {
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.NewsFeed);
            }
        }

        public NewsFeedController(ISessionCookieValues sessionValues)
            : this(sessionValues, new NewsFeedBO())
        {
        }

        public NewsFeedController(ISessionCookieValues sessionValues, NewsFeedBO newsFeedBO)
            : base(sessionValues)
        {
            this._NewsFeedBO = newsFeedBO;
            ViewData["AccessRights"] = AccessRightsAccreditation;
        }
        //
        // GET: /NewsFeed/

        public ActionResult Index()
        {
            if (AccessRightsAccreditation != null && AccessRightsAccreditation.Read.HasValue == true && AccessRightsAccreditation.Read == true)
            {
                var NewsFeedList = new List<SelectListItem>();
                for (int i = 1; i <= 10; i++)
                {
                    NewsFeedList.Add(new SelectListItem { Value = "" + i, Text = "News Feed " + i });
                }                
                ViewData["NewsFeed"] = new SelectList(NewsFeedList, "Value", "Text", "1");
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult GetNewsFeed(int Id)
        {
            try
            {
                var feed = _NewsFeedBO.GetNewsFeed(Id);
                if (feed != null)
                {
                    var jsonData = new
                    {
                        News = string.IsNullOrEmpty(feed.News) ? string.Empty : feed.News,
                        FeedLink = string.IsNullOrEmpty(feed.FeedLink) ? string.Empty : feed.FeedLink,
                        Date = feed.Date == null ? string.Empty : ((DateTime)feed.Date).ToString("MM/dd/yyyy")
                    };
                    return Json(jsonData);
                }
                else
                {
                    var jsonData = new
                    {
                        News = string.Empty,
                        FeedLink = string.Empty,
                        Date = string.Empty
                    };
                    return Json(jsonData);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = Constants.Messages.ServerError,
                });
            }
        }

        public ActionResult SaveNewsFeed(NewsFeed Feed)
        {
            if (AccessRightsAccreditation != null && AccessRightsAccreditation.Add.HasValue == true && AccessRightsAccreditation.Add == true || (AccessRightsAccreditation.Update.HasValue == true && AccessRightsAccreditation.Update == true))
            {
                Message msg = _NewsFeedBO.SaveNewsFeed(Feed);
                if (msg.Type == MessageType.BusinessError.ToString() || msg.Type == MessageType.Exception.ToString())
                {
                    return Json(new
                    {
                        success = false,
                        message = msg.Text,
                    });
                }
                return Json(new
                {
                    success = true,
                    message = msg.Text,
                });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }
        }
    }
}
