﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL.Authorization;


namespace Mits.Web.Controllers
{
    public class AffiliateTargetAssignedWeight
    {
        public bool Active;
        public int OEMId;
        public string OEMName;
        public decimal TargetWeight;
        public decimal AssignedWeight;
        //public IList<int> InvoiceId;
        public decimal RemainingTargetWeight;
        public decimal ReconciliationWeight;
        public bool IsPermanentDropOffLocation;
    }

    public class ProcessedInvoiceWeight
    {
        public bool Active;
        public int InvoiceId;
        public string ProcessingDate;
        public string CollectionMethod;
        public decimal Weight;
    }

    public class ReconciliationController : MITSApplicationController
    {
        private ReconciliationBO _reconciliationBO;
        private AccessRights AccessRightsReconciliation
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Reconciliation);
            }
        }
        
        //
        // GET: /Reconciliation/

        public ReconciliationController(ISessionCookieValues sessionValue)
            : this(sessionValue, new ReconciliationBO())
        {

        }

        public ReconciliationController(ISessionCookieValues sessionValue, ReconciliationBO reconciliationBO)
            : base(sessionValue)
        {
            //ViewData["AccessRights"] = AccessRightsAffiliate;
            _reconciliationBO = reconciliationBO;

        }

        public ActionResult Index1()
        {
            if (AccessRightsReconciliation != null && AccessRightsReconciliation.Read.HasValue == true && AccessRightsReconciliation.Read == true)
            {

                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                    //ViewData["Entities"] = _reconciliationBO.GetEntities();
                    //ViewData["AvailableWeight"] = _reconciliationBO.GetAvailableWeight();
                    return View();
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");

                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");

            }
        }

        public ActionResult Index()
        {
            if (AccessRightsReconciliation != null && AccessRightsReconciliation.Read.HasValue == true && AccessRightsReconciliation.Read == true)
            {
                IList<Affiliate> entities = _reconciliationBO.GetEntities();
                SelectList _entities = new SelectList(entities, "Id", "Name");
                ViewData["Entity"] = entities;
                return View(new ReconciliationViewModel());
               
               
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult Revert()
        {
            if (AccessRightsReconciliation != null && AccessRightsReconciliation.Read.HasValue == true && AccessRightsReconciliation.Read == true)
            {
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult GetReconciliationHistory(int oemId)
        {
            IList<ReconciledItem> recs =  _reconciliationBO.GetReconcileHistory(oemId);

            var jsonData = new
            {
                rows = (
                    from m in recs

                    select new
                    {
                        id = m.OemInvoiceid,
                        cell = new object[] { 
                            m.OemInvoiceid,
                            m.ReconcileDate,
                            m.ReconcileTotalQuantity
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEntities()
        {
            IList<Affiliate> data;
            IList<Affiliate> entities = _reconciliationBO.GetEntities();

            var jsonData = new
            {
                data = (
                    from m in entities
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name

                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetAllOemAffiliates()
        {
            IList<Affiliate> data;
            IList<Affiliate> targets = new MITSService<Affiliate>().GetAll(x => x.AffiliateTypeId == 1);

      


            IEnumerable<Affiliate> enumList = targets.OrderBy(f => f.Name);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = targets;
            }

            Affiliate _selectAffiliate = new Affiliate();
            _selectAffiliate.Id = 0;
            _selectAffiliate.Name = "All";
            data.Insert(0, _selectAffiliate);

            var jsonData = new
            {
                data = (from m in data
                        select new
                        {
                            Value = m.Id,
                            Text = m.Name

                        }).ToArray()
            };
            return Json(jsonData);

        }

        public ActionResult GetAllStates()
        {
            IList<State> data;
            List<State> states = new List<State>();
            IList<AffiliateTarget> targets = new MITSService<AffiliateTarget>().GetAll(x => x.ServiceTypeId == 10);
            
            foreach (AffiliateTarget target in targets)
            {
                if (states.Contains(target.State) == false)
                {
                    states.Add(target.State);
                }
            }
            IEnumerable<State> enumList = states.OrderBy(f => f.Name);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = states;
            }
            State _selectStates = new State();
            _selectStates.Id = 0;
            _selectStates.Name = "Select";
            data.Insert(0, _selectStates);

            var jsonData = new 
            {
                data = (from m in data
                        select new
                        {
                            Value = m.Id,
                            Text = m.Name
                             
                        }).ToArray()
            };
            return Json(jsonData);
        }
    
        public ActionResult GetTargetStates(string entity)
        {
            int entityId = int.Parse(entity);
            IList<State> data;
            List<State> states = new List<State>();
            IList<AffiliateTarget> targets = new MITSService<AffiliateTarget>().GetAll(x => x.AffiliateId == entityId && x.ServiceTypeId == 10);            
            foreach (AffiliateTarget target in targets)
            {
                if (states.Contains(target.State) == false)
                {
                    states.Add(target.State);
                }
            }
            IEnumerable<State> enumList = states.OrderBy(f => f.Name);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = states;
            }

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name

                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetStatesTargetPeriods(string entity, string state)
        {
            int entityId = int.Parse(entity);
            int stateId = int.Parse(state);
            IList<AffiliateTarget> data;
            IList<AffiliateTarget> targets = new MITSService<AffiliateTarget>().GetAll(x => x.AffiliateId == entityId && x.StateId == stateId && x.ServiceTypeId == 10);
            IEnumerable<AffiliateTarget> enumList = targets.OrderBy(f => f.StartDate);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = targets;
            }            
            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.Id,
                        Text = ((DateTime)m.StartDate).ToShortDateString() + " - " + ((DateTime)m.EndDate).ToShortDateString()

                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetOEMTotalTargetWeight(int oemid, int stateId, int planYear)
        {
            //take care of this
            IList<AffiliateTarget> AffiliateTargets = _reconciliationBO.GetEntityTargets(0, stateId, planYear);
            decimal totalTargetWeight = 0;
            foreach (AffiliateTarget target in AffiliateTargets)
            {
                totalTargetWeight = totalTargetWeight + target.Weight.Value;
            }
            var jsonData = new { totalTargetWeight = totalTargetWeight};
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOEMTargetAssignedWeight(int oemid, int stateId,int planYear)
        {

            MITSEntities db = new MITSEntities();
            var results = db.p_getOEMTargetAssignedWeight(oemid, stateId, planYear).ToList();

            var jsonData = new
            {
                rows = (
                    from m in results

                    select new
                    {
                        id = m.OEMId,
                        cell = new object[] { 
                            m.Active,
                            m.OEMId,
                            m.OEMName,
                            String.Format("{0:N}",m.TargetWeight),
                            String.Format("{0:N}",m.PaceWeight),
                            String.Format("{0:N}",m.AssignedWeight),
                            String.Format("{0:N}",m.RemainingWeight),
                            String.Format("{0:N}",m.RecommendedWeight),
                            m.IsPermanentDropOffLocation
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

            //IList<AffiliateTarget> AffiliateTargets = _reconciliationBO.GetEntityTargets(oemid, stateId, planYear);
            //IList<AffiliateTargetAssignedWeight> targetassigneds = new List<AffiliateTargetAssignedWeight>();
            //decimal totalAvailableWeight;
            //this.GetAvailableTotalWeight(stateId, planYear, out totalAvailableWeight);
            //decimal totalTargetWeight = 0;
            //foreach (AffiliateTarget target in AffiliateTargets)
            //{
            //    totalTargetWeight = totalTargetWeight + target.Weight.Value;
            //}
            //foreach (AffiliateTarget target in AffiliateTargets)
            //{
            //    AffiliateTargetAssignedWeight affTargetWeight = new AffiliateTargetAssignedWeight();
            //    affTargetWeight.OEMId = target.Affiliate.Id;
            //    affTargetWeight.OEMName = target.Affiliate.Name;
            //    affTargetWeight.TargetWeight = target.Weight.HasValue ? target.Weight.Value : 0;
            //    affTargetWeight.AssignedWeight = _reconciliationBO.GetOEMAssignedWeight(affTargetWeight.OEMId, planYear, stateId);

            //    if (affTargetWeight.AssignedWeight < affTargetWeight.TargetWeight)
            //        affTargetWeight.Active = true;
            //    else
            //        affTargetWeight.Active = false;
            //    affTargetWeight.IsPermanentDropOffLocation = target.IsPermanentDropOffLocation.HasValue ? target.IsPermanentDropOffLocation.Value : false;

            //    //affTargetWeight.InvoiceId = _reconciliationBO.GetOEMInvoiceId(affTargetWeight.OEMId, stateId, planYear);

            //    affTargetWeight.RemainingTargetWeight = affTargetWeight.TargetWeight - affTargetWeight.AssignedWeight;
            //    decimal recWeight = Math.Round((affTargetWeight.TargetWeight / totalTargetWeight) * totalAvailableWeight);
            //    if (affTargetWeight.RemainingTargetWeight > 0)
            //        affTargetWeight.ReconciliationWeight = recWeight > affTargetWeight.RemainingTargetWeight ? affTargetWeight.RemainingTargetWeight : recWeight;
            //    else
            //        affTargetWeight.ReconciliationWeight = 0;
            //    targetassigneds.Add(affTargetWeight);
            //}

            //var jsonData = new
            //{
            //    rows = (
            //        from m in targetassigneds

            //        select new
            //        {
            //            id = m.OEMId,
            //            cell = new object[] { 
            //                m.Active,
            //                m.OEMId,
            //                m.OEMName,
            //                String.Format("{0:N}",m.TargetWeight),
            //                String.Format("{0:N}",m.AssignedWeight),
            //                String.Format("{0:N}",m.RemainingTargetWeight),
            //                String.Format("{0:N}",m.ReconciliationWeight),
            //                m.IsPermanentDropOffLocation
            //            }
            //        }).ToArray()
            //};
            //return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        private IList<ProcessedInvoiceWeight> GetAvailableTotalWeight(int stateId, int planyear, out decimal totalWeight)
        {
            IList<ProcessedInvoiceWeight> processedInvoiceWeights = new List<ProcessedInvoiceWeight>();
            IList<Invoice> Invoices = _reconciliationBO.GetAvailableInvoices(stateId, planyear);
            totalWeight = 0;
            foreach (Invoice inv in Invoices)
            {
                ProcessedInvoiceWeight invoice = new ProcessedInvoiceWeight();
                invoice.InvoiceId = inv.Id;
                //ProcessingData processingData = inv.ProcessingDatas.First<ProcessingData>();
                if (inv.ProcessingDatas.Count > 0)
                {
                    invoice.ProcessingDate = inv.ProcessingDatas.FirstOrDefault<ProcessingData>().ProcessingDate.Value.ToString("MM/dd/yyyy");
                    invoice.CollectionMethod = inv.ProcessingDatas.First<ProcessingData>().CollectionMethod.Method;
                }
                else
                {
                    invoice.ProcessingDate = "";
                    invoice.CollectionMethod = "";
                }
               
                invoice.Weight = _reconciliationBO.GetAvailableProcessedWeight(invoice.InvoiceId);
                if (invoice.Weight <= 0)
                    invoice.Active = false;
                else
                    invoice.Active = true;
                totalWeight = totalWeight + invoice.Weight;
                processedInvoiceWeights.Add(invoice);
            }
            return processedInvoiceWeights;
        }

        public ActionResult GetAvailableProcessedWeight(int stateId, int planyear, bool displayReconciled)
        {

            var totalWeight = new System.Data.Objects.ObjectParameter("TotalAvailableWeight", typeof(decimal));

            MITSEntities db = new MITSEntities();
            var processedInvoiceWeights = db.p_getAvailableProcessedWeight(stateId, planyear, displayReconciled, totalWeight );

            if (totalWeight.Value == null)
            {
                totalWeight.Value = new Decimal(0.0);
            }

            var jsonData = new
            {
                rows = (
                from m in processedInvoiceWeights
                select new
                {
                    id = m.InvoiceId,
                    cell = new object[]{
                      m.Active,
                      m.InvoiceId,
                      0,
                      "",
                      m.ProcessingDate,
                      m.CollectionMethod,
                     String.Format("{0:N}",m.Weight) 
                  }
                }).ToArray(),
                TotalAvailableWeight =  (decimal)totalWeight.Value 
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


            //decimal totalWeight;
            //IList<ProcessedInvoiceWeight> processedInvoiceWeights = this.GetAvailableTotalWeight(stateId, planyear,out totalWeight);
            //var jsonData = new { 
            //  rows = (
            //  from m in processedInvoiceWeights
            //  select new
            //  {
            //      id = m.InvoiceId,
            //      cell = new object[]{
            //          m.Active,
            //          m.InvoiceId,
            //          0,
            //          "",
            //          m.ProcessingDate,
            //          m.CollectionMethod,
            //         String.Format("{0:N}",m.Weight) 
            //      }
            //  }).ToArray(),
            //  TotalAvailableWeight = totalWeight
            //};
            //return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTargetWeight(string target)
        {
            int targetId = int.Parse(target);
            double targetWeight = Convert.ToDouble(_reconciliationBO.GetTargetWeight(targetId));
            double weightNotCollected = 0;
            double weightCollected = 0;
            weightNotCollected = _reconciliationBO.GetWeightNotCollected(targetId, ref weightCollected);
            //weightNotCollected = targetWeight - weightCollected;
            var jsonData = new { TargetWeight = targetWeight, WeightNotCollected = weightNotCollected, WeightCollected = weightCollected };
           // var jsonData = new { WeightNotCollected = weightNotCollected };
            
            return Json(jsonData);
        }

        public ActionResult GetAvailableWeight(string state)
        {
            int stateId = int.Parse(state);
            string availableProcessorsWeight = _reconciliationBO.GetAvailableWeight(stateId);
            var jsonData = new { availableProcessorsWeight = availableProcessorsWeight };
            return Json(jsonData);
        }

        //public ActionResult GetAvailableWeight(string state,int targetId)
        //{
        //    int stateId = int.Parse(state);
        //    string availableProcessorsWeight = _reconciliationBO.GetAvailableWeight(stateId, targetId);
        //    var jsonData = new { availableProcessorsWeight = availableProcessorsWeight };
        //    return Json(jsonData);
        //}

        public ActionResult RevertRecocilation(int stateId, int planYear, int OemId)
        {
            try
            {
                _reconciliationBO.DeleteRecocilation(stateId, planYear, OemId);
                return Json(new
                {
                    success = true
                });
            }
            catch
            {
                return Json(new
                {
                    success = true
                });
            }
        }

        public ActionResult RevertReconcilationByInvoiceId(int invoiceId)
        {
            try
            {
                _reconciliationBO.DeleteRecocilation(invoiceId);
                return Json(new
                {
                    success = true
                });
            }
            catch
            {
                return Json(new
                {
                    success = true
                });
            }
        }

        public ActionResult Reconcile(string entity, string state, string target, string availableWeight, string weightNotCollected, string weightCollected, string weightToReconcile)
        {
            if (AccessRightsReconciliation != null && AccessRightsReconciliation.Add.HasValue == true && AccessRightsReconciliation.Add == true
                || (AccessRightsReconciliation.Update.HasValue == true && AccessRightsReconciliation.Update == true))
            {
                decimal updatedAvailableWeight = decimal.Parse(availableWeight);
                if (decimal.Parse(availableWeight) < decimal.Parse(weightToReconcile))
                {
                    weightToReconcile = availableWeight;
                }
                int entityId = int.Parse(entity);
                int stateId = int.Parse(state);
                int targetId = int.Parse(target);
                decimal updatedWeightNotCollected = _reconciliationBO.Reconcile(entityId, stateId, targetId, weightNotCollected, weightToReconcile);
                decimal updatedWeightCollected = decimal.Parse(weightCollected);
                updatedWeightCollected = updatedWeightCollected + decimal.Parse(weightToReconcile);
                updatedAvailableWeight = updatedAvailableWeight - decimal.Parse(weightToReconcile);
                var jsonData = new { updatedWeightNotCollected = updatedWeightNotCollected, updatedAvailableWeight, updatedWeightCollected };
                return Json(jsonData);
            }
            else
            {

                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });
            }
        }

        class AssignInvoice
        {
            public string InvoiceId;
            public string AssignQual;
            public string OemId;
        }

        //public ActionResult ExeReconcile(string OemId,string StateId, string PlanYear, string RecQual, string AssignQual)
        public ActionResult ExeReconcile(string StateId, string PlanYear,string AssignQual)
        {
            if (AccessRightsReconciliation == null)
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });
            }
            else if (AccessRightsReconciliation.Add.HasValue == true && AccessRightsReconciliation.Add == true
                || (AccessRightsReconciliation.Update.HasValue == true && AccessRightsReconciliation.Update == true))
            {
                try
                {
                    //decimal _RecQual = Convert.ToDecimal(RecQual);
                    AssignQual = AssignQual.Remove(AssignQual.Length - 1, 1);
                    string[] AssigningInvoices = AssignQual.Split(';');
                    Hashtable OEMs = new Hashtable();
                    IList<AssignInvoice> assignInvoices = new List<AssignInvoice>();
                    foreach (string strInvoiceQual in AssigningInvoices)
                    {
                        string[] InvoiceQual = strInvoiceQual.Split('#');
                        string _processInvoiceId = InvoiceQual[0];
                        string _qual = InvoiceQual[1];
                        string _oemid = InvoiceQual[2];
                        string _oemqual = InvoiceQual[3];
                        if (!OEMs.Contains(_oemid))
                        {
                            OEMs.Add(_oemid, _oemqual);
                        }
                        AssignInvoice assignInvoice = new AssignInvoice();

                        assignInvoice.InvoiceId = _processInvoiceId;
                        assignInvoice.AssignQual = _qual;
                        assignInvoice.OemId = _oemid;
                        assignInvoices.Add(assignInvoice);
                    }
                    DateTime reconcileDate = DateTime.Now;
                    foreach (string oemid in OEMs.Keys)
                    {
                        MITSEntities db = new MITSEntities();
                        int oemId = Convert.ToInt32(oemid);
                        int stateId = Convert.ToInt32(StateId);
                        int planYear = Convert.ToInt32(PlanYear);
                        Invoice newOemInvoice = new Invoice();
                        newOemInvoice.AffiliateId = oemId;
                        newOemInvoice.InvoiceDate = DateTime.Now;
                        newOemInvoice.InvoiceDueDate = DateTime.Now.AddDays(GetTerms(oemId));
                        newOemInvoice.PlanYear = planYear;
                        newOemInvoice.StateId = stateId;
                        newOemInvoice.IsPaid = false;
                        db.Invoices.Add(newOemInvoice);
                        db.SaveChanges();
                        int _OemInvoiceId = newOemInvoice.Id;

                        decimal oemRecQual = Convert.ToDecimal(OEMs[oemid]);
                        var result = from n in assignInvoices where n.OemId == oemid select n;
                        
                        foreach (AssignInvoice a in result)
                        {
                            string _invoiceId = a.InvoiceId;
                            decimal _qual = Convert.ToDecimal(a.AssignQual);
                            int _oemid = Convert.ToInt32(a.OemId);
                            if (_qual <= 0) continue;
                            int invoiceId;

                            if (_invoiceId.IndexOf("_") > 0)
                            {
                                string[] invoice = _invoiceId.Split('_');
                                invoiceId = Convert.ToInt32(invoice[0]);

                                InvoiceUnpackId UnpackId = new InvoiceUnpackId();
                                UnpackId.OEMInvoiceId = _OemInvoiceId;
                                UnpackId.ProcesserInvoiceId = invoiceId;
                                UnpackId.NewProcesserInvoiceId = _invoiceId;
                                db.InvoiceUnpackIds.Add(UnpackId);
                            }
                            else
                            {
                                invoiceId = Convert.ToInt32(_invoiceId);
                            }

                            //IList<InvoiceItem> items = new MITSService<InvoiceItem>().GetAll(x => x.InvoiceId == invoiceId);
                            IList<InvoiceItem> items = db.InvoiceItems.Where(x => x.InvoiceId == invoiceId).ToList() ;
                            
                            foreach (InvoiceItem item in items)
                            {
                                #region
                                decimal itemQuantity = item.Quantity.HasValue ? item.Quantity.Value : 0;
                                if (_qual <= 0) continue;
                                decimal assignedQuantity = 0;
                                IList<InvoiceReconciliation> recs = new MITSService<InvoiceReconciliation>().GetAll(x => x.ProcesserInvoiceItemId == item.Id);
                                foreach (InvoiceReconciliation rec in recs)
                                {
                                    assignedQuantity = assignedQuantity + rec.Quantity.Value;
                                }
                                itemQuantity = itemQuantity - assignedQuantity;
                                if (itemQuantity <= 0) continue;

                                InvoiceReconciliation newItemRec = new InvoiceReconciliation();
                                newItemRec.OEMInvoiceId = _OemInvoiceId;
                                newItemRec.ProcesserInvoiceItemId = item.Id;
                                newItemRec.ReconcileDate = reconcileDate;
                                newItemRec.Rate = GetContractRate((int)newOemInvoice.AffiliateId, (int)newOemInvoice.StateId, (int)newOemInvoice.PlanYear, (int)item.QuantityTypeId, (int)item.ProductTypeId);

                                if (itemQuantity <= oemRecQual && itemQuantity <= _qual)
                                {
                                    newItemRec.Quantity = itemQuantity;
                                    oemRecQual = oemRecQual - itemQuantity;
                                    _qual = _qual - itemQuantity;
                                }
                                else if (itemQuantity <= oemRecQual && itemQuantity >= _qual)
                                {
                                    newItemRec.Quantity = _qual;
                                    oemRecQual = oemRecQual - _qual;
                                    _qual = 0;
                                }
                                else if (itemQuantity >= oemRecQual && itemQuantity <= _qual)
                                {
                                    newItemRec.Quantity = oemRecQual;
                                    _qual = _qual - oemRecQual;
                                    oemRecQual = 0;
                                }
                                else if (itemQuantity >= oemRecQual && itemQuantity >= _qual)
                                {
                                    if (oemRecQual > _qual)
                                    {
                                        newItemRec.Quantity = _qual;
                                        oemRecQual = oemRecQual - _qual;
                                        _qual = 0;
                                    }
                                    else
                                    {
                                        newItemRec.Quantity = oemRecQual;
                                        _qual = _qual - oemRecQual;
                                        oemRecQual = 0;
                                    }
                                }
                                db.InvoiceReconciliations.Add(newItemRec);

                                #endregion
                            }
                            db.SaveChanges();
                        }
                    }

                    #region
                    /*
                    foreach (string strInvoiceQual in AssigningInvoices)
                    {
                        string[] InvoiceQual = strInvoiceQual.Split('#');
                        string _processInvoiceId = InvoiceQual[0];
                        decimal _qual = Convert.ToDecimal(InvoiceQual[1]);
                        int _oemid = Convert.ToInt32(InvoiceQual[2]);

                        if (_qual <= 0)
                        {
                            continue;
                        }

                        IList<InvoiceItem> items = new MITSService<InvoiceItem>().GetAll(x => x.InvoiceId == _processInvoiceId);
                        bool ifNeedUnpack = false;
                        foreach (InvoiceItem item in items)
                        {
                            #region
                            if (_RecQual <= 0 || _qual <= 0)
                                continue;

                            decimal assignedQuantity = 0;
                            IList<InvoiceReconciliation> recs = new MITSService<InvoiceReconciliation>().GetAll(x => x.ProcesserInvoiceItemId == item.Id);
                            foreach (InvoiceReconciliation rec in recs)
                            {
                                assignedQuantity = assignedQuantity + rec.Quantity.Value;
                            }
                            item.Quantity = item.Quantity - assignedQuantity;
                            if (item.Quantity <= 0)
                                continue;
                            InvoiceReconciliation newItemRec = new InvoiceReconciliation();
                            newItemRec.OEMInvoiceId = _OemInvoiceId;
                            newItemRec.ProcesserInvoiceItemId = item.Id;

                            if (item.Quantity <= _RecQual && item.Quantity <= _qual)
                            {
                                newItemRec.Quantity = item.Quantity;
                                _RecQual = _RecQual - item.Quantity.Value;
                                _qual = _qual - item.Quantity.Value;
                            }
                            else if (item.Quantity <= _RecQual && item.Quantity >= _qual)
                            {
                                newItemRec.Quantity = _qual;
                                _RecQual = _RecQual - _qual;
                                _qual = 0;
                                ifNeedUnpack = true;
                            }
                            else if (item.Quantity >= _RecQual && item.Quantity <= _qual)
                            {
                                newItemRec.Quantity = _RecQual;
                                _qual = _qual - _RecQual;
                                _RecQual = 0;
                                ifNeedUnpack = true;
                            }
                            else if (item.Quantity >= _RecQual && item.Quantity >= _qual)
                            {
                                ifNeedUnpack = true;
                                if (_RecQual > _qual)
                                {
                                    newItemRec.Quantity = _qual;
                                    _RecQual = _RecQual - _qual;
                                    _qual = 0;
                                }
                                else
                                {
                                    newItemRec.Quantity = _RecQual;
                                    _qual = _qual - _RecQual;
                                    _RecQual = 0;
                                }
                            }
                            db.InvoiceReconciliations.Add(newItemRec);
                            #endregion
                        }

                        if (ifNeedUnpack)
                        {
                            #region
                            IList<InvoiceUnpackId> listInvoicUnpackIds = new MITSService<InvoiceUnpackId>().GetAll(x => x.OEMInvoiceId == _OemInvoiceId && x.ProcesserInvoiceId == _processInvoiceId);
                            IList<InvoiceUnpackId> listInvoicUnpackIds2 = new MITSService<InvoiceUnpackId>().GetAll(x => x.ProcesserInvoiceId == _processInvoiceId);
                            if (!(listInvoicUnpackIds.Count > 0))
                            {
                                InvoiceUnpackId UnpackId = new InvoiceUnpackId();
                                UnpackId.OEMInvoiceId = _OemInvoiceId;
                                UnpackId.ProcesserInvoiceId = _processInvoiceId;
                                if (!(listInvoicUnpackIds2.Count > 0))
                                {
                                    UnpackId.NewProcesserInvoiceId = _processInvoiceId.ToString() + "_" + "1";
                                }
                                else
                                {
                                    UnpackId.NewProcesserInvoiceId = _processInvoiceId.ToString() + "_" + (listInvoicUnpackIds2.Count + 1).ToString();
                                }
                                db.InvoiceUnpackIds.Add(UnpackId);
                            }
                            #endregion
                        }

                    }
                    db.SaveChanges();
                     */
                    #endregion

                    //var jsonData = new { AssignedQual = RecQual };
                    var jsonData = new { AssignedQual = 1 };
                    return Json(jsonData);
                }
                catch
                {
                    return Json(new
                    {
                        success = false
                    });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });
            }
        }

        private decimal GetContractRate(int affiliateId, int stateId, int planyear, int quantityTypeId, int productTypeId)
        {
            decimal rate = 0;
            MITSEntities db = new MITSEntities();
            IList<AffiliateContractRate> rates = db.AffiliateContractRates.Where(x => x.AffiliateId == affiliateId
                                                                                && x.StateId == stateId
                                                                                && x.PlanYear == planyear
                                                                                && x.ServiceTypeId == 10
                                                                                && x.ProductTypeId == productTypeId
                                                                                && x.QuantityTypeId == quantityTypeId).ToList();
            if (rates.Count() > 0)
                rate = rates[0].Rate.Value;

            return rate;
        }

        private int GetTerms(int affiliateId)
        {
            int days = 30;
            MITSEntities db = new MITSEntities();
            var termdays =  from a in db.Affiliates
                            join t in db.PaymentTerms on a.PaymentTermId equals t.Id
                            where a.Id == affiliateId
                            select t.Days;

            foreach (int d in termdays)
            {
                days = d;
            }

            return days;

        }

    }
}
