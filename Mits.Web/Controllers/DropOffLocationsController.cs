﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.BusinessObjects;
using Mits.BLL;
using Mits.DAL.EntityModels;
using Mits.Common;
using System.Data;
using Mits.BLL.ViewModels;
using System.Xml;

namespace Mits.Web.Controllers
{
    public class DropOffLocationsController : MITSApplicationController
    {
        DropOffLocationsBO dropOffLocsBO;

        public DropOffLocationsController(ISessionCookieValues sessionValues)
            : this(sessionValues, new DropOffLocationsBO())
        {
        }

        public DropOffLocationsController(ISessionCookieValues sessionValues, DropOffLocationsBO _dropOffLocsBO)
            : base(sessionValues)
        {
            dropOffLocsBO = _dropOffLocsBO;

        }
        public ActionResult Index()
        {
            IList<State> _stateList;
            IList<State> unSortedList = new MITSService<State>().GetAll();
            IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _stateList = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _stateList = unSortedList;
            }


            State selectState = new State();
            selectState.Id = 0;
            selectState.Name = "Select";
            _stateList.Insert(0, selectState);

            SelectList States = new SelectList(_stateList, "Id", "Name", 0);
            ViewData["States"] = States;
            return View();

        }
        public ActionResult Michigan()
        {
            return View();

        }
        public ActionResult Sony()
        {
            return View();

        }
        public JsonResult GetLocations(string lat, string lgt, string city, string state, string prods, string mfgr)
        {

            var _dropOffLocations = dropOffLocsBO.GetNearestLocations(Convert.ToDecimal(lat),
                Convert.ToDecimal(lgt), city, state, prods, mfgr);


            var jsonData = Json(_dropOffLocations, JsonRequestBehavior.AllowGet);

            return jsonData;
        }

        public JsonResult GetMichiganLocations(string lat, string lgt, string city, string state, string prods, string mfgr)
        {

            var _dropOffLocations = dropOffLocsBO.GetNearestMichiganLocations(Convert.ToDecimal(lat),
                Convert.ToDecimal(lgt), city, state, prods, mfgr);


            var jsonData = Json(_dropOffLocations, JsonRequestBehavior.AllowGet);

            return jsonData;
        }

        public JsonResult GetLocationsForAcer(string lat, string lgt, string city, string state, string prods, string mfgr)
        {
            var _dropOffLocationsForAcer = dropOffLocsBO.GetNearestLocationsForAcer(Convert.ToDecimal(lat),
                Convert.ToDecimal(lgt), city, state, prods, mfgr);

            var jsonData = Json(_dropOffLocationsForAcer, JsonRequestBehavior.AllowGet);
            return jsonData;
        }

        public JsonResult GetLocationsForSony(string lat, string lgt, string city, string state, string prods, string mfgr)
        {
            var _dropOffLocationsForAcer = dropOffLocsBO.GetNearestLocationsForSony(Convert.ToDecimal(lat),
                Convert.ToDecimal(lgt), city, state, prods, mfgr);

            var jsonData = Json(_dropOffLocationsForAcer, JsonRequestBehavior.AllowGet);
            return jsonData;
        }

        public void SaveLaLo(string lat, string lgt, string address)
        {
            dropOffLocsBO.SaveDropOffLocation(Convert.ToDecimal(lat), Convert.ToDecimal(lgt), address);
        }

        public ActionResult LocationMap(string lng, string lat)
        {
            return View();

        }
        public ActionResult Qualcomm()
        {
            return View();

        }

        public ActionResult Acer()
        {
            return View();
        }

        public ActionResult Getlalo()
        {
            IList<DropOff_Location> locs = dropOffLocsBO.GetAllLocationsForTemp();
            ViewData["DropOff"] = new SelectList(locs, "ZipCode", "Address1");
            return View();
        }

        public ActionResult List()
        {
            int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Administrator ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Manager)
            {
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult GetDropOffLocations(int page, int rows, string searchString)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = dropOffLocsBO.GetDropOffLocations(pageIndex, pageSize, searchString, out totalRecords).AsEnumerable<DropOffLocation>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            m.Name==null?"":m.Name.Trim().ToUpper() == "NULL"?"":m.Name,
                            m.Address1==null?"":m.Address1.Trim().ToUpper() == "NULL"?"":m.Address1,
                            m.Phone,
                            m.Fax,
                            m.WebSite,
                            m.Longitude,
                            m.Latitude,
                            m.State.Name,
                            m.Zip,
                            m.Email
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Administrator ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Manager)
            {
                DropOffLocation dropoffloc = dropOffLocsBO.GetDropOffLocation(id);
                if (dropoffloc == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                dropoffloc.Name = dropoffloc.Name == null ? "" : dropoffloc.Name.Trim().ToUpper() == "NULL" ? "" : dropoffloc.Name;
                dropoffloc.Address1 = dropoffloc.Address1 == null ? "" : dropoffloc.Address1.Trim().ToUpper() == "NULL" ? "" : dropoffloc.Address1;
                DropOffLocationViewModel model = new DropOffLocationViewModel(dropoffloc);
                ViewData["Name"] = dropoffloc.Name;
                ViewData["Data"] = Request["data"];

                return View("Edit", model);
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection form)
        {
            int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Administrator ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Manager)
            {

                try
                {
                    DropOffLocation dropOffModel = dropOffLocsBO.GetDropOffLocation(id);
                    DropOffLocationViewModel model = new DropOffLocationViewModel(dropOffModel);
                    TryUpdateModel(dropOffModel);
                    string services = Request["selectedObjects"];
                    string locationtypes = Request["selectedTypes"];

                    List<Message> messages = dropOffLocsBO.Update(dropOffModel, services, locationtypes);


                    DropOffLocationViewModel updatedModel = new DropOffLocationViewModel(dropOffModel);
                    updatedModel.Messages = messages;
                    ViewData["Name"] = dropOffModel.Name;
                    ViewData["Data"] = Request["data"];
                    return View("Edit", updatedModel);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Edit", id);
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }


        // GET: /Affiliate/Create

        public ActionResult Create(int? saved)
        {
            int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Administrator ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Manager)
            {

                DropOffLocation _dff = new DropOffLocation();
                if (saved.HasValue)
                {
                    ViewData["saved"] = "true";
                }
                return View("Edit", new DropOffLocationViewModel(_dff));
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        //
        // POST: /Affiliate/Create

        [HttpPost]
        public ActionResult Create(DropOffLocation dropoff, FormCollection form)
        {
            int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Administrator ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Manager)
            {
                try
                {
                    DropOffLocationViewModel model = new DropOffLocationViewModel(dropoff);
                    string devices = Request["selectedObjects"];
                    string locationtypes = Request["selectedTypes"];

                    List<Message> messages = dropOffLocsBO.Add(dropoff, devices, locationtypes);
                    model.Messages = messages;
                    foreach (Message message in messages)
                    {
                        if (message.Type == MessageType.BusinessError.ToString())
                        {
                            return View("Create", model);
                        }
                    }
                    DropOffLocation _dff = new DropOffLocation();
                    DropOffLocationViewModel newModel = new DropOffLocationViewModel(_dff);
                    newModel.Messages = messages;
                    foreach (Message msg in messages)
                    {
                        if (msg.Type == MessageType.Success.ToString())
                        {
                            //return RedirectToAction("Create", new { saved = 1 });
                            return View("Edit", newModel);
                        }
                    }
                    return View("Create", newModel);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Create");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }


        /// <summary>
        /// Generic function to perform operations from Admin like Add,Edit,Delete based on Certain criteria         
        /// </summary>        
        /// <Array of custom params>Array of cutom params passed to stored proc</param>        
        public ActionResult PerformActions(DropOffLocation obj)
        {
            try
            {
                System.Int32 id;
                switch (Request.Form["oper"])
                {
                    case "del":
                        int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                        if (affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Administrator ||
                            affiliateRoleId == (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant ||
                            affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Manager)
                        {
                            id = System.Int32.Parse(Request.Form["id"]);
                            var delResult = dropOffLocsBO.DeleteDropOffLocation(id);
                        }
                        else
                        {
                            return RedirectToAction("AccessDenied", "Home");
                        }

                        break;

                    default:
                        break;
                }
                return RedirectToAction("List", "DropOffLocations");
            }
            catch (Exception ex)
            {

            }

            return View("/DropOffLocations/List");
        }

        public ActionResult Delete()
        {
            int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Administrator ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.ExecutiveAssistant ||
                affiliateRoleId == (int)Common.Constants.AffiliateContactRole.Manager)
            {
                try
                {
                    int id;

                    id = int.Parse(Request.Form["id"]);
                    var deleteResult = dropOffLocsBO.DeleteDropOffLocation(id);

                    return RedirectToAction("list", "DropOffLocations");
                }
                catch (Exception ex)
                {

                }
                return View("DropOffLocations/List");
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public string Data(int zip)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", null));
            var docRoot = doc.CreateElement("DropoffLocations");
            doc.AppendChild(docRoot);
            try
            {
                string mapAPIUrl = @"http://maps.googleapis.com/maps/api/geocode/xml?address=" + zip + "&sensor=false";
                System.Net.WebRequest request = System.Net.HttpWebRequest.Create(mapAPIUrl);
                System.Net.WebResponse response = request.GetResponse();
                System.IO.Stream stream = response.GetResponseStream();
                System.IO.StreamReader streamReader = new System.IO.StreamReader(stream);
                string content = streamReader.ReadToEnd();
                XmlDocument xmlDocumnet = new XmlDocument();
                xmlDocumnet.LoadXml(content);
                XmlElement rootNode = xmlDocumnet.DocumentElement;
                XmlNode statusNode = rootNode.SelectSingleNode("/GeocodeResponse/status");
                string status = statusNode.InnerText;

                var stsNode = doc.CreateElement("status");
                stsNode.InnerText = status;
                docRoot.AppendChild(stsNode);

                if (status == "OK")
                {
                    string state = string.Empty;
                    string city = string.Empty;
                    string lat = string.Empty;
                    string lng = string.Empty;
                    XmlNodeList addressTypeNodes = rootNode.SelectNodes(@"//address_component/type");
                    foreach (XmlNode node in addressTypeNodes)
                    {
                        if (node.InnerText == "administrative_area_level_1")
                        {
                            state = node.PreviousSibling.InnerText;
                        }
                        else if (node.InnerText == "locality")
                        {
                            city = node.PreviousSibling.InnerText;
                        }
                    }
                    XmlNode latNode = rootNode.SelectSingleNode(@"/GeocodeResponse/result/geometry/location/lat");
                    lat = latNode.InnerText;
                    XmlNode lngNode = rootNode.SelectSingleNode(@"/GeocodeResponse/result/geometry/location/lng");
                    lng = lngNode.InnerText;
                    //Get dropofflocations
                    var _dropOffLocations = dropOffLocsBO.GetNearestAffiliates(Convert.ToDecimal(lat), Convert.ToDecimal(lng), city, state, null, null);

                    var resultNode = doc.CreateElement("result");
                    docRoot.AppendChild(resultNode);
                    foreach (NearAffiliate loc in _dropOffLocations)
                    {
                        var locationNode = doc.CreateElement("DropoffLocation");
                        var nameNode = doc.CreateElement("name");
                        nameNode.InnerText = loc.Name;
                        var addressNode = doc.CreateElement("address");
                        addressNode.InnerText = loc.Address;
                        var cityNode = doc.CreateElement("city");
                        cityNode.InnerText = loc.City;
                        var stateNode = doc.CreateElement("state");
                        stateNode.InnerText = loc.State;
                        var zipNode = doc.CreateElement("zip");
                        zipNode.InnerText = loc.Zip;
                        var websiteNode = doc.CreateElement("website");
                        websiteNode.InnerText = loc.Website;
                        var phoneNode = doc.CreateElement("phone");
                        phoneNode.InnerText = loc.Phone;
                        var businessHoursNode = doc.CreateElement("businessHours");
                        businessHoursNode.InnerText = loc.BusinessHours;
                        var distanceMilesNode = doc.CreateElement("distanceMiles");
                        distanceMilesNode.InnerText = "" + loc.DistanceMiles;
                        var latitudeNode = doc.CreateElement("lat");
                        latitudeNode.InnerText = "" + loc.Latitude;
                        var longitudeNode = doc.CreateElement("lng");
                        longitudeNode.InnerText = "" + loc.Longitude;

                        locationNode.AppendChild(nameNode);
                        locationNode.AppendChild(addressNode);
                        locationNode.AppendChild(cityNode);
                        locationNode.AppendChild(stateNode);
                        locationNode.AppendChild(zipNode);
                        locationNode.AppendChild(websiteNode);
                        locationNode.AppendChild(phoneNode);
                        locationNode.AppendChild(businessHoursNode);
                        locationNode.AppendChild(distanceMilesNode);
                        locationNode.AppendChild(latitudeNode);
                        locationNode.AppendChild(longitudeNode);

                        resultNode.AppendChild(locationNode);
                    }
                }
            }
            catch (Exception e)
            {
                var stsNode = doc.CreateElement("status");
                stsNode.InnerText = "Inner Error";
                docRoot.AppendChild(stsNode);
            }
            return doc.OuterXml;
        }
    }
}
