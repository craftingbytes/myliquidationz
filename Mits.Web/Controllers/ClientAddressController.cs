﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Text;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class ClientAddressController : MITSApplicationController
    {

        private ClientAddressBO clientAddressBO;

        private AccessRights AccessRight
        {
            get
            {
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.ClientAddress);
            }
        }

        public ClientAddressController(ISessionCookieValues sessionValues)
            : this(sessionValues, new ClientAddressBO(sessionValues))
        {
        }

        public ClientAddressController(ISessionCookieValues sessionValues, ClientAddressBO clientAddressBO)
            : base(sessionValues)
        {
            this.clientAddressBO = clientAddressBO;
        }

        //
        // GET: /ClientAddress/

        public ActionResult Index()
        {
            if (CanRead())
            {
                string affiliateName = "";
                bool viewAll = false;
                int roleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                if (roleId == (int)Constants.AffiliateContactRole.Administrator ||
                            roleId == (int)Constants.AffiliateContactRole.Finance ||
                            roleId == (int)Constants.AffiliateContactRole.Manager ||
                            roleId == (int)Constants.AffiliateContactRole.ExecutiveAssistant)
                {
                    affiliateName = "Processor";
                    viewAll = true;
                    int processorType = (int)Constants.AffiliateType.Processor;
                    var Affiliates = new MITSService<Affiliate>().GetAll(x => x.AffiliateTypeId == processorType).ToList();
                    Affiliates.Insert(0, new Affiliate() { Id = 0, Name = "All"});
                    ViewData["Processors"] = new SelectList(Affiliates, "Id", "Name");
                }
                else if (roleId == (int)Constants.AffiliateContactRole.Processor)
                {
                    affiliateName = new AffiliateBO().GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId)).Name;
                }
                ViewData["AffiliateName"] = affiliateName;
                ViewData["ViewAll"] = viewAll;
                ViewData["AffiliateId"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                ViewData["CanAdd"] = CanAdd() ? "" : "disabled";
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetClientAddresses(int page, int rows, int? affiliateId)
        {
            if (!CanRead())
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = 0;
            if (affiliateId == 0)
            {
                affiliateId = null;
            }
            var data = clientAddressBO.GetClientAddresses(pageIndex, pageSize, affiliateId, out totalRecords);
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            m.AffiliateId,
                            m.Affiliate.Name,
                            m.StateId,
                            m.CityId,
                            string.IsNullOrEmpty(m.CompanyName) ? string.Empty : m.CompanyName,
                            string.IsNullOrEmpty(m.Address) ? string.Empty : m.Address,
                            m.StateId.HasValue ? m.State.Name : string.Empty,
                            m.CityId.HasValue ? m.City.Name : string.Empty,
                            string.IsNullOrEmpty(m.Zip) ? string.Empty : m.Zip,
                            string.IsNullOrEmpty(m.Phone) ? string.Empty : m.Phone
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public string EditClientAddress(FormCollection values)
        {
            string message = "";
            bool status = true;
            if (values["oper"] == "add")
            {
                if (CanAdd())
                {
                    ClientAddress client = new ClientAddress();
                    client.AffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                    int roleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                    if (roleId == (int)Constants.AffiliateContactRole.Administrator ||
                                roleId == (int)Constants.AffiliateContactRole.Finance ||
                                roleId == (int)Constants.AffiliateContactRole.Manager ||
                                roleId == (int)Constants.AffiliateContactRole.ExecutiveAssistant)
                    {
                        client.AffiliateId = Convert.ToInt32(values["Affiliate"]);
                    }
                    client.CompanyName = values["Company"];
                    client.Address = values["Address"];
                    client.StateId = Convert.ToInt32(values["State"]);
                    client.CityId = Convert.ToInt32(values["City"]);
                    client.Zip = values["Zip"];
                    client.Phone = values["Phone"];
                    if (clientAddressBO.AddClientAddress(client))
                    {
                        message = Constants.Messages.RecordSaved;
                        status = true;
                    }
                    else
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "edit")
            {
                if (CanUpdate())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        var client = clientAddressBO.GetClientAddressById(id);
                        if (client == null)
                        {
                            message = Constants.Messages.RecordNotSaved;
                            status = false;
                        }
                        else
                        {
                            client.CompanyName = values["Company"];
                            client.Address = values["Address"];
                            client.StateId = Convert.ToInt32(values["State"]);
                            client.CityId = Convert.ToInt32(values["City"]);
                            client.Zip = values["Zip"];
                            client.Phone = values["Phone"];
                            if (clientAddressBO.UpdateClientAddress(client))
                            {
                                message = Constants.Messages.RecordSaved;
                                status = true;
                            }
                            else
                            {
                                message = Constants.Messages.RecordNotSaved;
                                status = false;
                            }
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "del")
            {
                if (CanDelete())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = "Record could not be deleted.";
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        if (clientAddressBO.DeleteClientAddress(id))
                        {
                            message = "Record has been deleted successfully.";
                            status = true;
                        }
                        else
                        {
                            message = "Record could not be deleted.";
                            status = false;
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else
            {
                message = "Invalid edit operation.";
                status = false;
            }
            return status.ToString() + ":" + message;
        }

        [HttpGet]
        public ActionResult GetPickUpAddresses(int page, int rows, int? clientId)
        {
            if (!CanRead())
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }
            if (!clientId.HasValue || clientId.Value < 1)
            {
                var emptyData = new
                {
                    total = 0,
                    page = page,
                    records = 0
                };
                return Json(emptyData, JsonRequestBehavior.AllowGet);
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = 0;
            var data = clientAddressBO.GetPickUpAddresses(clientId.Value, pageIndex, pageSize, out totalRecords);
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            m.ClientAddressId,
                            m.StateId,
                            m.CityId,
                            string.IsNullOrEmpty(m.CompanyName) ? string.Empty : m.CompanyName,
                            string.IsNullOrEmpty(m.Address) ? string.Empty : m.Address,
                            m.StateId.HasValue ? m.State.Name : string.Empty,
                            m.CityId.HasValue ? m.City.Name : string.Empty,
                            string.IsNullOrEmpty(m.Zip) ? string.Empty : m.Zip,
                            string.IsNullOrEmpty(m.Phone) ? string.Empty : m.Phone,
                            string.IsNullOrEmpty(m.Fax) ? string.Empty : m.Fax,
                            string.IsNullOrEmpty(m.Email) ? string.Empty : m.Email,
                            m.CollectionMethodId.HasValue ? m.CollectionMethod.Method : string.Empty
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public string EditPickUpAddress(FormCollection values, int? clientId)
        {
            string message = "";
            bool status = true;
            if (values["oper"] == "add")
            {
                if (CanAdd())
                {
                    if (!clientId.HasValue)
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                    else
                    {
                        PickUpAddress pickUp = new PickUpAddress();
                        pickUp.ClientAddressId = clientId.Value;
                        pickUp.CompanyName = values["PickUpCompany"];
                        pickUp.Address = values["PickUpAddress"];
                        pickUp.StateId = Convert.ToInt32(values["PickUpState"]);
                        pickUp.CityId = Convert.ToInt32(values["PickUpCity"]);
                        pickUp.Zip = values["PickUpZip"];
                        pickUp.Phone = values["PickUpPhone"];
                        pickUp.Fax = values["Fax"];
                        pickUp.Email = values["Email"];
                        pickUp.CollectionMethodId = Convert.ToInt32(values["Collection"]);
                        if (clientAddressBO.AddPickUpAddress(pickUp))
                        {
                            message = Constants.Messages.RecordSaved;
                            status = true;
                        }
                        else
                        {
                            message = Constants.Messages.RecordNotSaved;
                            status = false;
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "edit")
            {
                if (CanUpdate())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        var pickUp = clientAddressBO.GetPickUpAddressById(id);
                        if (pickUp == null)
                        {
                            message = Constants.Messages.RecordNotSaved;
                            status = false;
                        }
                        else
                        {
                            pickUp.CompanyName = values["PickUpCompany"];
                            pickUp.Address = values["PickUpAddress"];
                            pickUp.StateId = Convert.ToInt32(values["PickUpState"]);
                            pickUp.CityId = Convert.ToInt32(values["PickUpCity"]);
                            pickUp.Zip = values["PickUpZip"];
                            pickUp.Phone = values["PickUpPhone"];
                            pickUp.Fax = values["Fax"];
                            pickUp.Email = values["Email"];
                            pickUp.CollectionMethodId = Convert.ToInt32(values["Collection"]);
                            if (clientAddressBO.UpdatePickUpAddress(pickUp))
                            {
                                message = Constants.Messages.RecordSaved;
                                status = true;
                            }
                            else
                            {
                                message = Constants.Messages.RecordNotSaved;
                                status = false;
                            }
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "del")
            {
                if (CanDelete())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = "Record could not be deleted.";
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        if (clientAddressBO.DeletePickUpAddress(id))
                        {
                            message = "Record has been deleted successfully.";
                            status = true;
                        }
                        else
                        {
                            message = "Record could not be deleted.";
                            status = false;
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else
            {
                message = "Invalid edit operation.";
                status = false;
            }
            return status.ToString() + ":" + message;
        }

        public ActionResult GetAffiliates()
        {
            List<Affiliate> Affiliates = new List<Affiliate>();

            int roleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (roleId == (int)Constants.AffiliateContactRole.Administrator ||
                        roleId == (int)Constants.AffiliateContactRole.Finance ||
                        roleId == (int)Constants.AffiliateContactRole.Manager ||
                        roleId == (int)Constants.AffiliateContactRole.ExecutiveAssistant)
            {
                int processorType = (int)Constants.AffiliateType.Processor;
                Affiliates = new MITSService<Affiliate>().GetAll(x => x.AffiliateTypeId == processorType).ToList();
            }
            else if (roleId == (int)Constants.AffiliateContactRole.Processor)
            {
                Affiliate affiliate = new AffiliateBO().GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
                Affiliates.Add(affiliate);
            }
            Affiliate _default = new Affiliate() { Id = 0, Name = "Select" };
            Affiliates.Insert(0, _default);
            StringBuilder sb = new StringBuilder();
            foreach (Affiliate aff in Affiliates)
            {
                sb.Append(aff.Id + ":" + aff.Name + ";");
            }
            var jsonData = new
            {
                data = sb.ToString().TrimEnd(';')
            };
            return Json(jsonData);
        }

        public ActionResult GetSupportedStates(int affiliateId)
        {
            var supportedStates = clientAddressBO.GetSupportedStates(affiliateId);
            State _state = new State() { Id = 0, Name = "Select" };
            supportedStates.Insert(0, _state);
            var jsonData = new
            {
                data = (
                    from m in supportedStates
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name
                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetCities(int stateId)
        {
            var cities = new MITSService<City>().GetAll(x => x.StateId == stateId).OrderBy(x => x.Name).ToList();
            City _city = new City() { Id = 0, Name = "Select" };
            cities.Insert(0, _city);

            var jsonData = new
            {
                data = (
                 from m in cities
                 select new
                 {
                     Value = m.Id,
                     Text = m.Name
                 }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetCollectionMethods()
        {
            List<CollectionMethod> methods = new List<CollectionMethod>();
            methods = new MITSService<CollectionMethod>().GetAll().ToList();
            CollectionMethod _default = new CollectionMethod() { Id = 0, Method = "Select" };
            methods.Insert(0, _default);
            StringBuilder sb = new StringBuilder();
            foreach (CollectionMethod m in methods)
            {
                sb.Append(m.Id + ":" + m.Method + ";");
            }
            var jsonData = new
            {
                data = sb.ToString().TrimEnd(';')
            };
            return Json(jsonData);
        }

        private bool CanRead()
        {
            if (AccessRight == null)
            {
                return false;
            }
            return (AccessRight.Read.HasValue && AccessRight.Read.Value);
        }

        private bool CanAdd()
        {
            if (AccessRight == null)
            {
                return false;
            }
            return (AccessRight.Add.HasValue && AccessRight.Add.Value);
        }

        private bool CanUpdate()
        {
            if (AccessRight == null)
            {
                return false;
            }
            return (AccessRight.Update.HasValue && AccessRight.Update.Value);
        }

        private bool CanDelete()
        {
            if (AccessRight == null)
            {
                return false;
            }
            return (AccessRight.Delete.HasValue && AccessRight.Delete.Value);
        }
    }
}
