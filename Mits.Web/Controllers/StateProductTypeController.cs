﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.Authorization;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Text;

namespace Mits.Web.Controllers
{
    public class StateProductTypeController : MITSApplicationController
    {
        private StateProductTypeBO stateProductTypeBO;
        private AccessRights accessRight;

        public StateProductTypeController(ISessionCookieValues sessionValue)
            : this(sessionValue, new StateProductTypeBO())
        {
        }

        public StateProductTypeController(ISessionCookieValues sessionValue, StateProductTypeBO stateProductTypeBO)
            : base(sessionValue)
        {
            this.accessRight = Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.StateProductType);
            this.stateProductTypeBO = stateProductTypeBO;
        }

        //
        // GET: /StateProductType/

        public ActionResult Index()
        {
            if (CanRead())
            {
                ViewData["State"] = GetStateSelectList();
                ViewData["CanAdd"] = CanAdd() ? "" : "disabled";
                ViewData["CanUpdate"] = CanUpdate() ? "" : "disabled";
                ViewData["CanDelete"] = CanDelete() ? "" : "disabled";
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetStateProductTypes(int page, int rows, int? stateId)
        {
            if (!CanRead())
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = 0;
            if (stateId.HasValue && stateId.Value == 0)
            {
                stateId = null;
            }
            var data = stateProductTypeBO.GetStateProductTypes(pageIndex, pageSize, stateId, out totalRecords);
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            m.State.Name,
                            m.ProductType.Name
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public string EditStateProductType(FormCollection values)
        {
            string message = "";
            bool status = true;
            if (values["oper"] == "add")
            {
                if (CanAdd())
                {
                    StateProductType stateProductType = new StateProductType();
                    stateProductType.StateId = Convert.ToInt32(values["State"]);
                    stateProductType.ProductTypeId = Convert.ToInt32(values["ProductType"]);
                    if (stateProductTypeBO.IsExistsStateProductType(stateProductType))
                    {
                        message = Constants.Messages.RecordExist;
                        status = false;
                    }
                    else if (stateProductTypeBO.AddStateProductType(stateProductType))
                    {
                        message = Constants.Messages.RecordSaved;
                        status = true;
                    }
                    else
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "edit")
            {
                if (CanUpdate())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        var stateProductType = stateProductTypeBO.GetStateProductTypeById(id);
                        if (stateProductType == null)
                        {
                            message = Constants.Messages.RecordNotSaved;
                            status = false;
                        }
                        else
                        {
                            stateProductType.StateId = Convert.ToInt32(values["State"]);
                            stateProductType.ProductTypeId = Convert.ToInt32(values["ProductType"]);
                            if (stateProductTypeBO.IsExistsStateProductType(stateProductType))
                            {
                                message = Constants.Messages.RecordExist;
                                status = false;
                            }
                            else if (stateProductTypeBO.UpdateStateProductType(stateProductType))
                            {
                                message = Constants.Messages.RecordSaved;
                                status = true;
                            }
                            else
                            {
                                message = Constants.Messages.RecordNotSaved;
                                status = false;
                            }
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "del")
            {
                if (CanDelete())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = "Record could not be deleted.";
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        if (stateProductTypeBO.DeleteStateProductType(id))
                        {
                            message = "Record has been deleted successfully.";
                            status = true;
                        }
                        else
                        {
                            message = "Record could not be deleted.";
                            status = false;
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else
            {
                message = "Invalid edit operation.";
                status = false;
            }
            return status.ToString() + ":" + message;
        }

        public ActionResult GetStates()
        {
            var states = new MITSService<State>().GetAll().OrderBy(f => f.Name).ToList();
            State _default = new State() { Id = 0, Name = "Select" };
            states.Insert(0, _default);
            StringBuilder sb = new StringBuilder();
            foreach (State s in states)
            {
                sb.Append(s.Id + ":" + s.Name + ";");
            }
            var jsonData = new
            {
                data = sb.ToString().TrimEnd(';')
            };
            return Json(jsonData);
        }

        public ActionResult GetProductTypes()
        {
            var types = new MITSService<ProductType>().GetAll().OrderBy(f => f.Name).ToList();
            ProductType _default = new ProductType() { Id = 0, Name = "Select" };
            types.Insert(0, _default);
            StringBuilder sb = new StringBuilder();
            foreach (ProductType s in types)
            {
                sb.Append(s.Id + ":" + s.Name + ";");
            }
            var jsonData = new
            {
                data = sb.ToString().TrimEnd(';')
            };
            return Json(jsonData);
        }

        private SelectList GetStateSelectList()
        {
            var unSortedStates = new MITSService<State>().GetAll();
            var sortedStates = unSortedStates.OrderBy(f => f.Name).ToList();
            sortedStates.Insert(0, new State() { Id = 0, Name = "All" });
            var states = new SelectList(sortedStates, "Id", "Name");
            return states;
        }

        private bool CanRead()
        {
            return (accessRight != null && accessRight.Read.HasValue && accessRight.Read.Value);
        }

        private bool CanAdd()
        {
            return (accessRight != null && accessRight.Add.HasValue && accessRight.Add.Value);
        }

        private bool CanUpdate()
        {
            return (accessRight != null && accessRight.Update.HasValue && accessRight.Update.Value);
        }

        private bool CanDelete()
        {
            return (accessRight != null && accessRight.Delete.HasValue && accessRight.Delete.Value);
        }

    }
}
