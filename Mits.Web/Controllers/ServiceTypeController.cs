﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using System.Configuration;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class ServiceTypeController : MITSApplicationController
    {
        
        private ServiceTypeBO _serviceTyeBO;


        private AccessRights AccessRightsServiceType
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.ServiceType);
            }
        }
        public ServiceTypeController(ISessionCookieValues sessionValue)
            : this(sessionValue, new ServiceTypeBO())
        {
        }
      
        public ServiceTypeController(ISessionCookieValues sessionValue, ServiceTypeBO serviceTyeBO)
            : base(sessionValue)
        {
            _serviceTyeBO = serviceTyeBO;                   
        }
        
        
        //
        // GET: /ServiceType/

      public ActionResult Index()
      {
          if (AccessRightsServiceType != null && AccessRightsServiceType.Read.HasValue == true && AccessRightsServiceType.Read == true)
          {

              //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
              //{
                  return View(new ServiceTypeViewModel());
              //}
              //else
              //{
              //    return RedirectToAction("AccessDenied", "Home");

              //}
          }
          else
          {
              return RedirectToAction("AccessDenied", "Home");

          }
      }


//Gets Name,Description and asscociated groups, if any, against the selected service type

      public ActionResult GetDetail(string serviceType)
      {
          int _id = int.Parse(serviceType);
          ServiceType data = new MITSService<ServiceType>().GetSingle(x => x.Id == _id);
          var newData = new MITSService<ServiceTypeGroupRelation>().GetAll(x => x.ServiceTypeId == _id & x.Active == true);

          if (data != null)
          {
              if (newData != null)
              {
                  var jsonData = new
                  {
                     
                      id = data.Id,
                      Name = data.Name,
                      Desc = data.Description,
                      newData = (
                         from m in newData
                         select new
                         {
                             Value = m.ServiceTypeGroupId,


                         }).ToArray(),
                  };
                  return Json(jsonData);
              }
              else
              {
                  var jsonData = new
                  {
                     
                      id = data.Id,
                      Name = data.Name,
                      Desc = data.Description,
                      Value = string.Empty
                  };
                  return Json(jsonData);
              }
          }

          return Json(new
          {
              Id = 0,
              Name = string.Empty,
              Desc = string.Empty,
              Value=string.Empty
          });
      }


//Invokes ServiceType Add function and the return appropriate response to the view

      public ActionResult Save(string servicetype, string associatedGroups, string name, string desc)
      {
          if (AccessRightsServiceType != null && AccessRightsServiceType.Add.HasValue == true && AccessRightsServiceType.Add == true || (AccessRightsServiceType.Update.HasValue == true && AccessRightsServiceType.Update == true))
          {
              Dictionary<string, int> returnDictionaryValue = new Dictionary<string, int>();

              try
              {
                  returnDictionaryValue = _serviceTyeBO.Add(servicetype, associatedGroups, name, desc);
              }
              catch (Exception ex)
              {
                  string log = ex.Message;
                  return Json(new
                  {
                      success = false,
                      message = ConfigurationManager.AppSettings["RecordNotSaved"],
                  });
              }
              if (returnDictionaryValue["duplicate"] == 0)
              {
                  var data = _serviceTyeBO.GetAll();
                  var jsonData = new
                  {
                      success = true,
                      message = ConfigurationManager.AppSettings["RecordSaved"],
                      selected = returnDictionaryValue["selectedId"].ToString(),
                      data = (
                          from m in data
                          select new
                          {
                              Value = m.Id,
                              Text = m.Name

                          }).ToArray()
                  };
                  return Json(jsonData);
              }
              else
              {
                  return Json(new
                  {
                      success = false,
                      message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                  });
              }
          }
          else
          {

              return Json(new
              {
                  success = false,
                  AccessDenied = "AccessDenied",

              });
          }
      }
     

    }
}
