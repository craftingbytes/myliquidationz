﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.BusinessObjects;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.Common;
using Mits.BLL.Authorization;
using System.Configuration;

namespace Mits.Web.Controllers
{
    public class AffiliateContactController : Mits.Web.Controllers.MITSApplicationController
    {
        private AffiliateContactBO _affiliateContactBO;
        private AccessRights AccessRightsAffiliateContact
        {
            get
            {
               // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.EntityContact);
            }
        }
        public AffiliateContactController(ISessionCookieValues sessionValues)
            : this(sessionValues, new AffiliateContactBO(sessionValues))
        {
            
        }

        public AffiliateContactController(ISessionCookieValues sessionValues, AffiliateContactBO affiliateContactBO)
            : base(sessionValues)
        {
            ViewData["AccessRights"] = AccessRightsAffiliateContact;
            _affiliateContactBO = affiliateContactBO;
        }        

        [HttpGet]
        public ActionResult GetAffiliateContacts(string sidx, string sord, int page, int rows)
        {
            int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId);

            if (affiliateId == -1)
                affiliateId = 0;

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = _affiliateContactBO.GetAffiliateContactsPaged(pageIndex, pageSize, out totalRecords, affiliateId).AsEnumerable<AffiliateContact>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                           m.Id,
                            m.FirstName,
                            m.LastName,
                            m.Address1,
                            m.State!=null ?m.State.Name:"",
                            m.Phone,    
                            m.RoleId
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }

        public ActionResult Clear()
        {
            return RedirectToAction("Index", new { id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId), data = Request["data"] });
        }
        //
        // GET: /AffiliateContact/

        public ActionResult Index(int? id)
        {
            int affiliateId = -1;
            if (id.HasValue)
                affiliateId = (int)id;
            if (AccessRightsAffiliateContact != null && AccessRightsAffiliateContact.Read.HasValue == true && AccessRightsAffiliateContact.Read == true)
            {
                /*if (SessionParameters.AffiliateId == SessionParameters.SelectedAffiliateId && affiliateId != -1  && affiliateId != SessionParameters.SelectedAffiliateId)
                {
                    return RedirectToAction("AccessDenied", "Home");
                }*/
                if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) != (int)Constants.AffiliateType.Eworld && _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) != affiliateId)
                {
                    return RedirectToAction("AccessDenied", "Home");
                }
                AffiliateBO affiliateBO = new AffiliateBO();
                if (affiliateId == -1)
                    affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId);
                else
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, affiliateId);

                Affiliate affiliate = affiliateBO.GetAffiliate(affiliateId);
                if (affiliate == null)
                {
                    return RedirectToAction("Error", "Home");
                }                

                AffiliateContact _affContact = new AffiliateContact();
                _affContact.Affiliate = affiliate;
                ViewData["Name"] = affiliate.Name;
                ViewData["AffiliateType"] = affiliate.AffiliateTypeId;
                if (_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName) == Constants.AffiliateContactRole.Administrator.ToString())
                    ViewData["LoginUser"] = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
                else
                    ViewData["LoginUser"] = string.Empty;
                ViewData["Data"] = Request["data"];
                return View(new AffiliateContactViewModel(_affContact));
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }
        

        [HttpPost]
        public ActionResult Index(AffiliateContact affiliateContact)
        {
            try
            {
                if (AccessRightsAffiliateContact != null && AccessRightsAffiliateContact.Add.HasValue == true && AccessRightsAffiliateContact.Add == true)
                {
                    AffiliateBO affiliateBO = new AffiliateBO();
                    int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId);
                    if (affiliateId == -1)
                        affiliateId = 0;

                    Affiliate affiliate = affiliateBO.GetAffiliate(affiliateId);
                    affiliateContact.Affiliate = affiliate;
                    AffiliateContactViewModel model = new AffiliateContactViewModel(affiliateContact);
                    string services = Request["selectedObjects"];
                    List<Message> messages = _affiliateContactBO.Add(affiliateContact, services);
                    ViewData["Name"] = affiliate.Name;
                    if (_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName) == Constants.AffiliateContactRole.Administrator.ToString())
                        ViewData["LoginUser"] = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
                    else
                        ViewData["LoginUser"] = string.Empty;
                    
                    AffiliateContact _affContact = new AffiliateContact();
                    _affContact = _affiliateContactBO.GetAffiliateContact(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId));
                    AffiliateContactViewModel updatedModel = new AffiliateContactViewModel(_affContact);
                    updatedModel.Messages = messages;
                    ViewData["Data"] = Request["data"];
                    return View("Index", updatedModel);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Home");
                }

            }
            catch (Exception ex)
            {
                return View();
            }
        }
        
        //
        // GET: /AffiliateContact/Edit/5

        public ActionResult Edit(int id)
        {
            AffiliateContact _affContact = _affiliateContactBO.GetAffiliateContact(id);
            if (_affContact == null)
            {
                return RedirectToAction("Error", "Home");
            }
            AffiliateBO _affiliateBO = new AffiliateBO();
            Affiliate _affiliate = _affiliateBO.GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId));
            if (_affiliate != null)
            {
                ViewData["Name"] = _affiliate.Name;
                if (_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName) == Constants.AffiliateContactRole.Administrator.ToString())
                    ViewData["LoginUser"] = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
                else
                    ViewData["LoginUser"] = string.Empty;
            }
            ViewData["Data"] = Request["data"];
            return View("Index", new AffiliateContactViewModel(_affContact));

        }
        //
        // POST: /AffiliateContact/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {            
            AffiliateContact affiliateContactModel = _affiliateContactBO.GetAffiliateContact(id);
            try
            {
                bool passwordIsChanged = true;
                AffiliateContactViewModel model = new AffiliateContactViewModel(affiliateContactModel);                
                //ExcludeProperty password, because the password need to check to old one, when the password don't change
                //the password text box is the hash password so we need check it. if use the TryUpdateModel function the 
                //password will be change to the new password.
                string OldPassword;
                OldPassword = affiliateContactModel.Password;
                TryUpdateModel(affiliateContactModel);
                if (affiliateContactModel.Password.Equals(OldPassword))
                {
                    passwordIsChanged = false;
                }
                               
                string services = Request["selectedObjects"];

                List<Message> messages = _affiliateContactBO.Update(affiliateContactModel, services, passwordIsChanged);                

                AffiliateContactViewModel updatedModel = new AffiliateContactViewModel(affiliateContactModel);
                updatedModel.Messages = messages;
                AffiliateBO _affiliateBO = new AffiliateBO();
                Affiliate _affiliate = _affiliateBO.GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId));
                if (_affiliate != null)
                {
                    ViewData["Name"] = _affiliate.Name;
                    if (_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName) == Constants.AffiliateContactRole.Administrator.ToString())
                        ViewData["LoginUser"] = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
                    else
                        ViewData["LoginUser"] = string.Empty;
                }
                ViewData["Data"] = Request["data"];
                return View("Index", updatedModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Edit", new { id = affiliateContactModel.Id, success = false, data = Request["data"] });
            }
        }

        [HttpGet]
        public ActionResult GetContactMasters(string sidx, string sord, int page, int rows, int contactId = 0)
        {
            //int affiliateId = 0;

            //if (SessionParameters.SelectedAffiliateId != -1)
            //    affiliateId = SessionParameters.SelectedAffiliateId;

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = _affiliateContactBO.GetAffiliateMasterContacts(pageIndex, pageSize, out totalRecords, contactId).AsEnumerable<AffiliateMasterContact>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                           m.Id,
                            m.AffiliateContact.UserName,
                            m.AffiliateContact1.Affiliate.Name,
                            m.AffiliateContact1.UserName,
                            m.DefaultUser                     
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }

        public ActionResult GetAffiliatesByType( int affiliateTypeId)
        {
            IList<Affiliate> data;
            IList<Affiliate> targets = new MITSService<Affiliate>().GetAll(x => x.AffiliateTypeId == affiliateTypeId);




            IEnumerable<Affiliate> enumList = targets.OrderBy(f => f.Name);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = targets;
            }

            Affiliate _selectAffiliate = new Affiliate();
            _selectAffiliate.Id = 0;
            _selectAffiliate.Name = "Select";
            data.Insert(0, _selectAffiliate);

            var jsonData = new
            {
                data = (from m in data
                        select new
                        {
                            Value = m.Id,
                            Text = m.Name

                        }).ToArray()
            };
            return Json(jsonData);

        }

        public ActionResult GetAssociatedContactsByAffiliateId(int affiliateId)
        {
            IList<AffiliateContact> data;
            IList<AffiliateContact> targets = new MITSService<AffiliateContact>().GetAll(x => x.AffiliateId == affiliateId);




            IEnumerable<AffiliateContact> enumList = targets.OrderBy(f => f.UserName);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = targets;
            }

            AffiliateContact _selectAffiliate = new AffiliateContact();
            _selectAffiliate.Id = 0;
            _selectAffiliate.UserName = "Select";
            data.Insert(0, _selectAffiliate);

            var jsonData = new
            {
                data = (from m in data
                        select new
                        {
                            Value = m.Id,
                            Text = m.UserName

                        }).ToArray()
            };
            return Json(jsonData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMasterContact(Int32 masterContactId,  Int32 associateId)
        {
            String msg = "";
            bool status = false;
            AffiliateMasterContact masterContact = new AffiliateMasterContact();
            masterContact.MasterAffiliateContactId = masterContactId;
            masterContact.AssociateContactId = associateId;

            if (_affiliateContactBO.AddMasterContact(masterContact))
                status = true;
            else
                msg = "An error occured adding associate contact.";

            return Json(new { status = status, message = msg });
            
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SetMasterContactDefault(Int32 masterContactId, Int32 id)
        {
            String msg = "";
            bool status = false;
            

            if (_affiliateContactBO.SetMasterContactDefault( masterContactId, id ))
                status = true;
            else
                msg = "An error occured setting default.";

            return Json(new { status = status, message = msg });

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteMasterContact(Int32 id)
        {
            String msg = "";
            bool status = false;


            if (_affiliateContactBO.DeleteAffiliateMaster(id))
                status = true;
            else
                msg = "An error occured deleting record.";

            return Json(new { status = status, message = msg });

        }


    }
}
