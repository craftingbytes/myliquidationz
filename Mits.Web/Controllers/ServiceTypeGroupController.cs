﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using System.Configuration;
using Mits.BLL.Authorization;


namespace Mits.Web.Controllers
{
    public class ServiceTypeGroupController : MITSApplicationController
    {
        private ServiceTypeGroupBO _serviceTypeGroupBO;

        private AccessRights AccessRightsServiceTypeGroup
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.ServiceTypeGroup);
            }
        }
        public ServiceTypeGroupController(ISessionCookieValues sessionValue)
            : this(sessionValue, new ServiceTypeGroupBO())
        {
        }

        public ServiceTypeGroupController(ISessionCookieValues sessionValue, ServiceTypeGroupBO serviceTypeGroupBO)
            : base(sessionValue)
        {
            _serviceTypeGroupBO = serviceTypeGroupBO;                   
        }
        
        //
        // GET: /ServiceTypeGroup/

        public ActionResult Index()
        {
            if (AccessRightsServiceTypeGroup != null && AccessRightsServiceTypeGroup.Read.HasValue == true && AccessRightsServiceTypeGroup.Read == true)
            {
                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                    ViewData["ServiceTypeGroup"] = this.GetAll();
                    return View();
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");

                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        //Getting a list of all service type groups

        private IList<ServiceTypeGroup> GetAll()
        {
            IList<ServiceTypeGroup> unSortedList = new MITSService<ServiceTypeGroup>().GetAll();
            IEnumerable<ServiceTypeGroup> sortedEnum = unSortedList.OrderBy(f => f.Name);
            IList<ServiceTypeGroup> _serviceTypeGroup = sortedEnum.ToList();

            ServiceTypeGroup selectGroup = new ServiceTypeGroup();
            selectGroup.Id = 0;
            selectGroup.Name = "Select";
            _serviceTypeGroup.Insert(0, selectGroup);
            return _serviceTypeGroup;
        }

        //Getting detail of ServiceTypeGroup's name and Desc and sending response back to view

        public ActionResult GetDetail(string servicetypegroup)
        {
            int _id = int.Parse(servicetypegroup);
            ServiceTypeGroup data = new MITSService<ServiceTypeGroup>().GetSingle(x => x.Id == _id);

            if (data != null)
            {
                return Json(new
                {
                    Id = data.Id,
                    Name = data.Name,
                    Desc = data.Description
                });
            }

            return Json(new
            {
                Id = 0,
                Name = string.Empty,
                Desc = string.Empty
            });
        }




        //Invokes 'Add' function of ServiceTypeGroupBO and sends response back to view

        public ActionResult Save(string servicetypegroup, string name, string desc)
        {


            if (AccessRightsServiceTypeGroup != null && AccessRightsServiceTypeGroup.Add.HasValue == true && AccessRightsServiceTypeGroup.Add == true || (AccessRightsServiceTypeGroup.Update.HasValue == true && AccessRightsServiceTypeGroup.Update == true))
            {

                Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();

                try
                {
                    _returnDictionaryValue = _serviceTypeGroupBO.Add(servicetypegroup, name, desc);
                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }
                if (_returnDictionaryValue["duplicate"] == 0)
                {
                    var data = this.GetAll();
                    var jsonData = new
                     {
                         success = true,
                         message = ConfigurationManager.AppSettings["RecordSaved"],
                         selected = _returnDictionaryValue["selectedId"].ToString(),
                         data = (
                             from m in data
                             select new
                             {
                                 Value = m.Id,
                                 Text = m.Name

                             }).ToArray()
                     };
                    return Json(jsonData);
                }
                else
                {
                    return Json(new
                            {
                                success = false,
                                message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                            });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });
            }
        }
    }
}
