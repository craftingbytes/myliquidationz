﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.BusinessObjects;
using Mits.DAL.EntityModels;
using Mits.BLL.Authorization;
using Mits.Common;

namespace Mits.Web.Controllers
{
    public class StateWeightCategoryController : MITSApplicationController
    {
        //
        // GET: /SpecificStateRate/


        public StateWeightCategoryController(ISessionCookieValues sessionValue)
            : this(sessionValue, new StateWeightCategoryBO())
        {

            stateWeightCategoryBO = new StateWeightCategoryBO();
        }

        public StateWeightCategoryController(ISessionCookieValues sessionValue, StateWeightCategoryBO pDataBO)
            : base(sessionValue)
        {
            stateWeightCategoryBO = pDataBO;

        }

        private StateWeightCategoryBO stateWeightCategoryBO;

        public ActionResult Index()
        {
            IList<State> currentStates = stateWeightCategoryBO.GetAvailableStates();
            ViewData["States"] = new SelectList(currentStates, "Id", "Name");
            return View();
        }

    }
}
