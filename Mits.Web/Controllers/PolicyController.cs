﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class PolicyController : Mits.Web.Controllers.MITSApplicationController
    {
        private PolicyBO _policyBO;

        private AccessRights AccessRightsPolicy
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Policy);
            }
        }
        
        public PolicyController(ISessionCookieValues sessionValue)
            : this(sessionValue, new PolicyBO())
        {
        }

        public PolicyController(ISessionCookieValues sessionValue, PolicyBO policyBO)
            : base(sessionValue)
        {
            _policyBO = policyBO;                   
        }
        
        //
        // GET: /Policy/

        public ActionResult Index()
        {
            if (AccessRightsPolicy != null && AccessRightsPolicy.Read.HasValue == true && AccessRightsPolicy.Read == true)
            {
                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                    ViewData["Policy"] = _policyBO.GetAll();
                    return View();
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");
                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
                //Response.Redirect("~/Helpers/Error.aspx");
                //return null;
            }            
        }

       
        //Getting name and description against a particular Policy and retruning the response to View

        public ActionResult GetDetail(string policy)
        {
            int _id = int.Parse(policy);
            Policy data = new MITSService<Policy>().GetSingle(x => x.Id == _id);

            if (data != null)
            {
                return Json(new
                {
                    Id = data.Id,
                    Name = data.Name,
                    Desc = data.Description
                });
            }

            return Json(new
            {
                Id = 0,
                Name = string.Empty,
                Desc = string.Empty
            });
        }

        //Invokes Add function of PolicyBO and return the response to View

        public ActionResult Save(string policy, string name, string desc)
        {

            if (AccessRightsPolicy != null &&
                (AccessRightsPolicy.Add.HasValue == true && AccessRightsPolicy.Add == true) ||
                (AccessRightsPolicy.Update.HasValue == true && AccessRightsPolicy.Update == true))
            {

                Dictionary<string, int> _retrunDictionaryValue = new Dictionary<string, int>();

                try
                {
                    _retrunDictionaryValue = _policyBO.Add(policy, name, desc);
                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }
                if (_retrunDictionaryValue["duplicate"] == 0)
                {
                    var data = _policyBO.GetAll();
                    var jsonData = new
                    {
                        success = true,
                        message = ConfigurationManager.AppSettings["RecordSaved"],
                        selected = _retrunDictionaryValue["selectedId"].ToString(),
                        data = (
                            from m in data
                            select new
                            {
                                Value = m.Id,
                                Text = m.Name

                            }).ToArray()
                    };
                    return Json(jsonData);
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                    });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied="AccessDenied",
                    
                });
            }

        }

    }
}
