﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL.Authorization;
using Mits.Common;
using Mits.BLL.BusinessObjects;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Text;

namespace Mits.Web.Controllers
{
    public class EventController : MITSApplicationController
    {

        private AccessRights AccessRightsEvent
        {
            get
            {
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.Event);
            }
        }
        
        private EventBO eventBO;
        private AffiliateBO _affiliateBO;

        public EventController(ISessionCookieValues sessionValues)
            : this(sessionValues, new EventBO())
        {
            _affiliateBO = new AffiliateBO();
        }

        public EventController(ISessionCookieValues sessionValues, EventBO eventBObj)
            : base(sessionValues)
        {
            eventBO = eventBObj;

        }

        public ActionResult Index()
        {

            return View();
        }
        public ActionResult Create()
        {

            ViewData["data"] = "data is here...";

            return View();
        }
        public ActionResult ViewEvents()
        {
            if (AccessRightsEvent != null && AccessRightsEvent.Read.HasValue && AccessRightsEvent.Read.Value)
            {
                IList<State> statestList = eventBO.GetStates();
                SelectList statesSelectList = new SelectList(statestList, "Abbreviation", "Name");
                ViewData["statesList"] = statesSelectList;
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }


        [HttpGet]
        public ActionResult IsEventTemplateDuplicate(string name)
        {

            var isDuplicate = eventBO.IsEventTemplateDuplicate(name);


            return Json(isDuplicate, JsonRequestBehavior.AllowGet); ;


        }


        [HttpGet]
        public ActionResult LoadReminderStatusList()
        {




            var list = eventBO.GetReminderStatus();

            StringBuilder statuses = new StringBuilder();
            foreach (var item in list)
            {
                statuses.Append(item.Id + ":" + item.Status + ";");
            }

            ContentResult result = Content(statuses.ToString().TrimEnd(';'));

            return result;

        }


        [HttpGet]
        public ActionResult LoadAffiliateesEmailList()
        {




            var list = eventBO.GetReminderStatus();

            StringBuilder statuses = new StringBuilder();
            foreach (var item in list)
            {
                statuses.Append(item.Id + ":" + item.Status + ";");
            }

            ContentResult result = Content(statuses.ToString().TrimEnd(';'));

            return result;

        }



        [HttpGet]
        public ActionResult LoadPeriodList()
        {




            var list = eventBO.GetReminderPeriod();

            StringBuilder periodList = new StringBuilder();
            foreach (var item in list)
            {
                if (item.Period1 == "Days")
                    periodList.Append(item.Id + ":" + item.Period1 + ";");
            }

            ContentResult result = Content(periodList.ToString().TrimEnd(';'));

            return result;

        }


        [HttpGet]
        public ActionResult LoadOEM()
        {
            var list = eventBO.GetAffiliates(Convert.ToInt32(Mits.Common.Constants.AffiliateType.OEM));


            var jsonList = (
                from m in list
                select new
                {
                    AffiliateId = m.Id.ToString(),
                    Name = m.Name

                }).ToArray();

            return Json(jsonList, JsonRequestBehavior.AllowGet); ;

        }

        [HttpGet]
        public ActionResult LoadProcessor()
        {
            var list = eventBO.GetAffiliates(Convert.ToInt32(Mits.Common.Constants.AffiliateType.Processor));


            var jsonList = (
                from m in list
                select new
                {
                    AffiliateId = m.Id,
                    Name = m.Name

                }).ToArray();

            return Json(jsonList, JsonRequestBehavior.AllowGet); ;

        }

        [HttpGet]
        public ActionResult LoadCollector()
        {
            var list = eventBO.GetAffiliates(Convert.ToInt32(Mits.Common.Constants.AffiliateType.Collector));


            var jsonList = (
                from m in list
                select new
                {
                    AffiliateId = m.Id,
                    Name = m.Name

                }).ToArray();

            return Json(jsonList, JsonRequestBehavior.AllowGet); ;

        }



        [HttpGet]
        public ActionResult LoadStates()
        {
            var list = eventBO.GetAffiliates(Convert.ToInt32(Mits.Common.Constants.AffiliateType.State));


            var jsonList = (
                from m in list
                select new
                {
                    AffiliateId = m.Id,
                    Name = m.Name

                }).ToArray();

            return Json(jsonList, JsonRequestBehavior.AllowGet); ;

        }

        [HttpGet]
        public ActionResult LoadAdmins()
        {
            var list = eventBO.GetAffiliates(Convert.ToInt32(Mits.Common.Constants.AffiliateType.Eworld));


            var jsonList = (
                from m in list
                select new
                {
                    AffiliateId = m.Id,
                    Name = m.Name

                }).ToArray();

            return Json(jsonList, JsonRequestBehavior.AllowGet); ;

        }

        [HttpGet]
        public ActionResult LoadEmails(int groupId)
        {
            var list = eventBO.GetAffiliateEmails(groupId);

            return Json(list, JsonRequestBehavior.AllowGet); ;

        }

        [HttpGet]
        public ActionResult GetEvent(int eventId)
        {
            var eve = eventBO.GetEvent(eventId);

            if (eve == null)
                return RedirectToAction("CreateView", "Event");


            var jsonResult = new
            {
                Title = eve.Title,
                Description = eve.Description,
                Date = eve.DateBegin.Value.ToShortDateString(),
                StateId = eve.StateId,
                EventTypeId = eve.EventTypeId,
                RecSingle = (eve.SingleRecurrenceValue != null) ? eve.SingleRecurrenceValue.Value.ToShortDateString() : "",
                RecDay = eve.RecurrenceIntervalForDay,
                RecMonth = eve.RecurrenceIntervalForMonth,
                RecQuater = eve.RecurrenceIntervalQuaterly,
                EndsOn = (eve.DateEnd != null) ? eve.DateEnd.Value.ToShortDateString() : "",
                RecYearly = (eve.RecurrenceIntervalYearly != null) ? eve.RecurrenceIntervalYearly.Value.ToShortDateString() : "",
                TemplateName = eve.TemplateName,
                HasReminders = (eve.ReminderGroups.Count > 0) ? true : false
            };


            return Json(jsonResult, JsonRequestBehavior.AllowGet); ;

        }

        [HttpGet]
        public ActionResult LoadAffiliates()
        {
            var list = eventBO.GetAffiliates();

            var jsonList = (
                from m in list
                select new
                {
                    AffiliateId = m.Id,
                    Name = m.Name

                }).ToArray();

            return Json(jsonList, JsonRequestBehavior.AllowGet); ;
            

        }
        [HttpGet]
        public ActionResult GetReminderByGroup(string groupId)
        {
            var process = eventBO;
            var list = process.GetReminderByGroup(Convert.ToInt32(groupId));
            //  var emails = process.GetAffiliateEmails();

            var jsonData = new
            {
                total = 5,
                page = 1,
                records = 20,
                rows = (
                    from m in list
                    select new
                    {

                        id = m.Id,
                        cell = new object[] {                       
                            m.Id,
                            m.ReminderGroup.Id,
                            m.Number,
                            m.Period.Id,
                            m.Title,
                            m.ReminderStatu.Id,
                            m.ShowOnCalendar,
                            m.Emails,
                             "<a href='#' style='curor:hand;color:#1E78A5;font-size:11px;font-weight:bold;' onclick=openDialog("+ m.Id +") >"+m.ReminderAttachments.Count +" Attachments</a>",
                             ""}
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }

        [HttpGet]
        public ActionResult GetGroups(string eventId, string sidx, string sord, int page, int rows, bool _search, string searchString)
        {
            if (string.IsNullOrEmpty(eventId) || eventId == "undefined" || eventId == "")
                return null;

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;


            var list = eventBO.GetReminderGroups(Convert.ToInt32(eventId), pageIndex, pageSize, searchString, out totalRecords);
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in list

                    select new
                    {
                        id = m.ReminderGroupId,
                        cell = new object[] { 
                            m.ReminderGroupId,m.AffiliateNames,m.AffiliateIds}
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }

        [HttpGet]
        public ActionResult LoadFiles(int reminderId)
        {
            var list = eventBO.GetReminderAttatchments(reminderId);

            return Json(list, JsonRequestBehavior.AllowGet); ;

        }

        [HttpGet]
        public ActionResult GetReminderGroups(string eventId)
        {
            // var list = eventBO.GetReminderGroups(Convert.ToInt32(eventId));

            return null;// Json(list, JsonRequestBehavior.AllowGet); ;

        }
        [HttpPost]
        public ActionResult UploadFile(FormCollection formCollection)
        {


            eventBO.SaveFileData(Convert.ToInt32(formCollection["RemiderId"]), formCollection["FilePath"], formCollection["FileName"]);

            return Json("", JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult DeleteEvent(int? eventId)
        {

            if (eventId.HasValue)
            {
                eventBO.DeleteEvent(eventId.Value);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteGroup(int? id)
        {
            if (id.HasValue)
            {
                eventBO.DeleteReminderGroup(id.Value);
                return Content("Success");
            }
            else
            {

                return Content("Error Occurred");

            }

            
        }

        [HttpPost]
        public ActionResult PerformAction(int? groupId, FormCollection form)
        {
            int id;

            switch (Request.Form["oper"])
            {
                case "add":


                    break;


                case "del":

                    id = System.Int32.Parse(Request.Form["id"]);
                    eventBO.DeleteReminderGroup(id);

                    break;


                default:
                    break;
            }






            return Json("", JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SaveReminderEmails(FormCollection formCollection)
        {

        
            int remId = Convert.ToInt32(formCollection["RemiderId"]);
            string emails = Convert.ToString(formCollection["Emails"]).TrimEnd(';');
            Reminder rem = eventBO.GetReminder(remId);
            rem.Emails = emails;
            eventBO.UpdateRemider(rem);


            return Json(rem.Id, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEvent(string EventId, FormCollection formCollection)
        {
            EventType type = null;

            RecurrenceType recType = null;
            State state = null;

            Event evnt = new Event();

         //   evnt.AffiliateId = userinfo.UserCompanyAffiliateID;

            
            //if (!String.IsNullOrEmpty(Convert.ToString(formCollection["EventId"])) && !Convert.ToBoolean(formCollection["IsTemplate"]))
            if (!String.IsNullOrEmpty(Convert.ToString(formCollection["EventId"])))
            {
                // evnt.EventId = Convert.ToInt32(formCollection["EventId"]);

                evnt = eventBO.GetEvent(Convert.ToInt32(formCollection["EventId"]));
            }




            if (Convert.ToString(formCollection["Date"]) != "")
            {
                evnt.ActionDate = Convert.ToDateTime(formCollection["Date"]);
                evnt.DateBegin = Convert.ToDateTime(formCollection["Date"]);
            }



            if (Convert.ToString(formCollection["RecType"]) == "Every")
            {
                if (!String.IsNullOrEmpty(formCollection["EveryDays"]))
                {
                    evnt.RecurrenceIntervalForDay = Convert.ToInt32(formCollection["EveryDays"]);
                    recType = eventBO.GetRecurrenceType().FirstOrDefault(r => r.Id == Convert.ToInt32(Mits.Common.Constants.EventTypeEnum.Days));
                    evnt.RecurrenceType = recType;

                }
            }
            else if (Convert.ToString(formCollection["RecType"]) == "Monthly On")
            {
                if (!String.IsNullOrEmpty(formCollection["Monthly"]))
                {
                    evnt.RecurrenceIntervalForMonth = Convert.ToInt32(formCollection["Monthly"]);
                    recType = eventBO.GetRecurrenceType().FirstOrDefault(r => r.Id == Convert.ToInt32(Mits.Common.Constants.EventTypeEnum.Monthly));
                    evnt.RecurrenceType = recType;

                }
            }
            else if (Convert.ToString(formCollection["RecType"]) == "Quarterly On")
            {
                if (!String.IsNullOrEmpty(formCollection["Quaterly"]))
                {
                    evnt.RecurrenceIntervalQuaterly = Convert.ToInt32(formCollection["Quaterly"]);
                    recType = eventBO.GetRecurrenceType().FirstOrDefault(r => r.Id == Convert.ToInt32(Mits.Common.Constants.EventTypeEnum.Quaterly));
                    evnt.RecurrenceType = recType;

                }
            }
            else if (Convert.ToString(formCollection["RecType"]) == "Yearly On")
            {
                if (!String.IsNullOrEmpty(formCollection["Yearly"]))
                {
                    evnt.RecurrenceIntervalYearly = Convert.ToDateTime(formCollection["Yearly"]);
                    recType = eventBO.GetRecurrenceType().FirstOrDefault(r => r.Id == Convert.ToInt32(Mits.Common.Constants.EventTypeEnum.Yearly));
                    evnt.RecurrenceType = recType;

                }
            }
            else
            {
                if (!String.IsNullOrEmpty(formCollection["SingleRec"]))
                {
                    evnt.SingleRecurrenceValue = Convert.ToDateTime(formCollection["SingleRec"]);
                    recType = eventBO.GetRecurrenceType().FirstOrDefault(r => r.Id == Convert.ToInt32(Mits.Common.Constants.EventTypeEnum.Single));
                    evnt.RecurrenceType = recType;

                }
            }

            if (!String.IsNullOrEmpty(formCollection["EndsOnValue"]))
            {

                evnt.DateEnd = Convert.ToDateTime(formCollection["EndsOnValue"]);

            }


            if (!String.IsNullOrEmpty(formCollection["Desc"]))
                evnt.Description = formCollection["Desc"];
            else
                evnt.Description = "";

            if (!String.IsNullOrEmpty(formCollection["Title"]))
                evnt.Title = formCollection["Title"];
            else
                evnt.Title = "";



            if (formCollection["EventType"] != null)
            {
                evnt.EventType = type = eventBO.GetEventType().FirstOrDefault(t => t.Id == Convert.ToInt32(formCollection["EventType"]));

            }
            if (formCollection["State"] != null)
            {
                evnt.State = state = eventBO.GetStateList().FirstOrDefault(t => t.Id == Convert.ToInt32(formCollection["State"]));
            }

            evnt.IsTemplate = Convert.ToBoolean(formCollection["IsTemplate"]);

            evnt.TemplateName = formCollection["TemplateName"];



         
            int eventId;

            if (!String.IsNullOrEmpty(Convert.ToString(formCollection["EventId"])))
            {

                //if template save then save as template and clone remiders
                if (Convert.ToBoolean(formCollection["IsTemplate"]))
                {
                    //evnt.EventId = 0;
                    Event ev = new Event();
                    ev.ActionDate = evnt.ActionDate;
                    ev.DateBegin = evnt.DateBegin;
                    ev.DateEnd = evnt.DateEnd;
                    ev.Description = evnt.Description;
                    ev.Title = evnt.Title;
                    ev.EventType = type;
                    ev.IsTemplate = true;
                    ev.RecurrenceIntervalForDay = evnt.RecurrenceIntervalForDay;
                    ev.RecurrenceIntervalForMonth = evnt.RecurrenceIntervalForMonth;
                    ev.RecurrenceIntervalQuaterly = evnt.RecurrenceIntervalQuaterly;
                    ev.RecurrenceIntervalYearly = evnt.RecurrenceIntervalYearly;
                    ev.RecurrenceType = recType;
                    ev.SingleRecurrenceValue = evnt.SingleRecurrenceValue;
                    ev.TemplateName = evnt.TemplateName;
                    ev.State = state;

                    eventId = eventBO.SaveEvent(ev);


                }
                else
                {
                    eventId = eventBO.UpdateChanges(evnt);
                }
            }
            else
            {
                //if template save then save as template and clone remiders
                if (Convert.ToBoolean(formCollection["IsTemplate"]))
                {
                    eventId = eventBO.SaveEvent(evnt);
                }
                else
                {
                    eventId = eventBO.SaveEvent(evnt);
                }


                var affiliates = Convert.ToString(formCollection["Affiliates"]);
                if (!string.IsNullOrEmpty(affiliates))
                {
                    var list = affiliates.Split(',');

                    eventBO.SaveGroupReminder(eventId, list);
                }


            }

            

            return Json(eventId, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SaveGroupReminder(ReminderGroup group, int? eventId, FormCollection formCollection)
        {


            var affiliates = Convert.ToString(formCollection["Affiliates"]);

            var list = affiliates.Split(',');

            var grpId = eventBO.SaveGroupReminder(eventId.Value, list);

            return Json(grpId, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SaveReminder(FormCollection formCollection)
        {

            var eventId = Convert.ToString(formCollection["eventId"]);
            if (!string.IsNullOrEmpty(eventId))
            {
                var number = Convert.ToString(formCollection["Number"]);
                var period = Convert.ToString(formCollection["Period"]);
                var title = Convert.ToString(formCollection["Title"]);
                var stat = Convert.ToString(formCollection["Status"]);
                var showOnCal = Convert.ToString(formCollection["ShowonCal"]);
                bool show = false;
                if (showOnCal == "on")
                {
                    show = true;
                }

                var reminderId = eventBO.SaveReminder(Convert.ToInt32(eventId), number, Convert.ToInt32(period), title, Convert.ToInt32(stat), show);

                return Json(reminderId, JsonRequestBehavior.AllowGet);
            }

            return null;

        }

        [HttpGet]
        public JsonResult SearchEvents(string searchText, string states, string start, string end)
        {

            if (AccessRightsEvent != null)
            {
                if (AccessRightsEvent.Read.HasValue
                    && AccessRightsEvent.Read.Value)
                {


                    DateTime startDate = ConvertFromUnixTimestamp(double.Parse(start));
                    DateTime endDate = ConvertFromUnixTimestamp(double.Parse(end));


                    List<Event> searchResult = eventBO.SearchEvents(searchText, states, startDate, endDate);
                    //Temp Fix to avoid long events would have ripple for multiday events
                    //select new { id = e.EventId, title = e.EventTitle, start = e.DateBegin.ToString("yyyy-MM-dd"), end = (e.DateEnd != null ? DateTime.Parse(e.DateEnd.ToString()).ToString("yyyy-MM-dd") : ""), url = "/Event/CreateEvent?EventId="+ e.EventId.ToString(),description=e.EventDescription,eventType=e.EventType };
                    //var eventList = (from e in searchResult
                    //                 select new { id = e.Id, title = e.Title, start = (e.DateBegin == null ? "" : e.DateBegin.Value.ToString("yyyy-MM-dd")), end = (e.DateEnd == null ? "" : e.DateEnd.Value.ToString("yyyy-MM-dd")), url = "/Event/CreateEvent?EventId=" + e.Id.ToString(), description = e.Description, eventType = (e.EventType == null ? "" : e.EventType.Type), stateName = (e.State == null ? "" : e.State.Name) }
                    //                ).ToArray();
                    var eventList = (from e in searchResult
                                     select new { id = e.Id, title = e.Title, start = (e.DateBegin == null ? "" : e.DateBegin.Value.ToString("yyyy-MM-dd")), end = "", url = "/Event/CreateEvent?EventId=" + e.Id.ToString(), description = e.Description, eventType = (e.EventType == null ? "" : e.EventType.Type), stateName = (e.State == null ? "" : e.State.Name) }
                                    ).ToArray();


                    return Json(eventList, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //return RedirectToAction("AccessDenied", "Home");
                }
            }
            else
            {
                //return RedirectToAction("AccessDenied", "Home");
            }

            return null;
        }

        DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }


        double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalSeconds);
        }
        [HttpGet]
        public ActionResult CreateEvent(int? EventId)
        {

            if (AccessRightsEvent != null && AccessRightsEvent.Add.HasValue && AccessRightsEvent.Add.Value)
            {


                var templates = eventBO.GetTemplateEvents();
                var states = eventBO.GetStateList();
                var eventsTypes = eventBO.GetEventType();

                var statesList = new SelectList(states, "Id", "Name");
                var templateList = new SelectList(templates, "Id", "TemplateName");
                var eventTypeList = new SelectList(eventsTypes, "Id", "Type");


                ViewData["States"] = statesList;
                ViewData["EventTypes"] = eventTypeList;
                ViewData["Templates"] = templateList;
                if (EventId.HasValue)
                {
                    ViewData["EventId"] = EventId.Value;
                }
                else
                {
                    ViewData["EventId"] = "";
                }

                ViewData["EntityType"] = _affiliateBO.GetAffiliateType(); 
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");

            }

        }
        public DownloadResult Download()
        {
            string fileName = Request["FileName"];



            if (fileName != "")
            {
                fileName = fileName.Replace("<~>", "#");
                return new DownloadResult { VirtualPath = "~/uploads/" + fileName, FileDownloadName = fileName };
            }
            else
            {
                return null;
            }
        }

        [HttpGet]
        public ActionResult GetReminderByEvent(string eventId, string sidx, string sord, int page, int rows, bool _search, string searchString)
        {
            if (string.IsNullOrEmpty(eventId) || eventId == "undefined" || eventId == "")
                return null;
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;


            var process = eventBO;
            var list = process.GetReminderByEvent(Convert.ToInt32(eventId), pageIndex, pageSize, searchString, out totalRecords); 
            //  var emails = process.GetAffiliateEmails();

            if (totalRecords <= 0)
                return null;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in list
                    select new
                    {

                        id = m.Id,
                        cell = new object[] {                       
                            m.Id,
                            m.ReminderGroup.Id,
                            m.Number,
                            m.Period.Id,
                            m.Title,
                            m.ReminderStatu.Id,
                            m.ShowOnCalendar,
                            m.Emails,
                            "<a href='#' style='curor:hand;color:#1E78A5;font-size:11px;font-weight:bold;' onclick=openDialog("+ m.Id +") >"+m.ReminderAttachments.Count +" Attachments</a>",
                            m.Id,
                            ""}
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }

        public ActionResult GetAffiliatesByType(int affiliateTypeId)
        {
            IList<Affiliate> data;
            IList<Affiliate> targets = new MITSService<Affiliate>().GetAll(x => x.AffiliateTypeId == affiliateTypeId);
            IEnumerable<Affiliate> enumList = targets.OrderBy(f => f.Name);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = targets;
            }

            Affiliate _selectAffiliate = new Affiliate();
            _selectAffiliate.Id = 0;
            _selectAffiliate.Name = "Select";
            data.Insert(0, _selectAffiliate);

            var jsonData = new
            {
                data = (from m in data
                        select new
                        {
                            Value = m.Id,
                            Text = m.Name

                        }).ToArray()
            };
            return Json(jsonData);

        }

        public ActionResult GetAffiliateEmails(int affiliateId)
        {
            IList<AffiliateContact> data;
            IList<AffiliateContact> targets = new MITSService<AffiliateContact>().GetAll(x => x.AffiliateId == affiliateId);
            IEnumerable<AffiliateContact> enumList = targets.OrderBy(f => f.Email);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception ex)
            {
                data = targets;
            }

            AffiliateContact _selectAffiliateEmail = new AffiliateContact();
            _selectAffiliateEmail.Id = 0;
            _selectAffiliateEmail.Email = "Select";
            data.Insert(0, _selectAffiliateEmail);

            var jsonData = new
            {
                data = (from m in data
                        select new
                        {
                            Value = m.Id,
                            Text = m.Email

                        }).ToArray()
            };
            return Json(jsonData);

        }

    }
}
