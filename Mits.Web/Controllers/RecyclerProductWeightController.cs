﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.Common;
using Mits.BLL.BusinessObjects;
using Mits.DAL.EntityModels;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class RecyclerProductWeightController : MITSApplicationController
    {

        private RecyclerProductWeightBO _productWeightBO;

        private AccessRights AccessRightsAccreditation
        {
            get
            {
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.RecyclerProductWeight);
            }
        }

        private Affiliate Affiliate
        {
            get
            {
                return _productWeightBO.GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
            }
        }

        public RecyclerProductWeightController(ISessionCookieValues sessionValue)
            : this(sessionValue, new RecyclerProductWeightBO())
        {
        }

        public RecyclerProductWeightController(ISessionCookieValues sessionValue, RecyclerProductWeightBO productWeightBO)
            : base(sessionValue)
        {
            this._productWeightBO = productWeightBO;
            ViewData["Affiliate"] = Affiliate;
            ViewData["AccessRights"] = AccessRightsAccreditation;
        }

        //
        // GET: /RecyclerProductWeight/

        public ActionResult Index()
        {
            if (AccessRightsAccreditation == null)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            if (AccessRightsAccreditation != null && AccessRightsAccreditation.Read.HasValue == true && AccessRightsAccreditation.Read == true)
            {
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetProductWeights(int page, int rows, string searchFrom, string searchTo)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = _productWeightBO.GetProductWeightPaged(pageIndex, pageSize, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), searchFrom, searchTo, out totalRecords);
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                from m in data
                select new
                {
                    id = m.Id,
                    cell = new object[] {
                        m.Month + "/" + m.Year,
                        m.TotalWeightReceived,
                        m.OnHand,
                        m.CRTGlass,
                        m.CircuitBoards,
                        m.Batteries,
                        m.OtherMaterials
                    }
                }
                ).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            if (AccessRightsAccreditation != null && AccessRightsAccreditation.Add.HasValue == true && AccessRightsAccreditation.Add == true)
            {
                var weight = new RecyclerProductWeight();
                return View(weight);
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }


        [HttpPost]
        public ActionResult Create(string txtMonth, RecyclerProductWeight ProductWeight)
        {
            try
            {
                string sDate = txtMonth;
                ViewData["Month"] = sDate;
                DateTime date = DateTime.Parse(sDate);
                string sYear = date.ToString("yyyy");
                string sMonth = date.ToString("MM");
                ProductWeight.Year = sYear;
                ProductWeight.Month = sMonth;
                ProductWeight.RecyclerId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

                List<Message> messages = _productWeightBO.Add(ProductWeight);
                foreach (Message message in messages)
                {
                    if (message.Type == MessageType.Success.ToString())
                    {
                        ViewData["saved"] = "true";
                        break;
                    }
                }
                ViewData["Messages"] = messages;
                return View(ProductWeight);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Create");
            }
        }

        public ActionResult Edit(int id)
        {
            var ProductWeight = _productWeightBO.GetProductWeight(id);

            if (ProductWeight == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewData["Month"] = ProductWeight.Month + "/" + ProductWeight.Year;
            return View(ProductWeight);
        }

        [HttpPost]
        public ActionResult Edit(RecyclerProductWeight ProductWeight)
        {
            List<Message> messages = _productWeightBO.Save(ProductWeight);
            foreach (Message message in messages)
            {
                if (message.Type == MessageType.Success.ToString())
                {
                    ViewData["saved"] = "true";
                    break;
                }
            }
            ViewData["Messages"] = messages;
            return View(ProductWeight);
        }
    }
}