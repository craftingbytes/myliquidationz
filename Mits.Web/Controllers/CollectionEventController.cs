﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.DAL.EntityModels;
using System.Text;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class CollectionEventController : MITSApplicationController
    {
        private CollectionEventBO collectionEventBO;
        private AccessRights accessRight;

        public CollectionEventController(ISessionCookieValues sessionValues)
            : this(sessionValues, new CollectionEventBO())
        {
        }

        public CollectionEventController(ISessionCookieValues sessionValues, CollectionEventBO collectionEventBO)
            : base(sessionValues)
        {
            this.accessRight = Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.CollectionEvent);
            this.collectionEventBO = collectionEventBO;
        }

        //
        // GET: /CollectionEvent/

        public ActionResult Index()
        {
            if (CanRead())
            {
                ViewData["State"] = GetStateSelectList();
                ViewData["CanAdd"] = CanAdd() ? "" : "disabled";
                ViewData["CanUpdate"] = CanUpdate() ? "" : "disabled";
                ViewData["CanDelete"] = CanDelete() ? "" : "disabled";
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult Map()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetCollectionEvents(int page, int rows, int? stateId)
        {
            if (!CanRead())
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied"
                });
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = 0;
            if (stateId.HasValue && stateId.Value == 0)
            {
                stateId = null;
            }
            var data = collectionEventBO.GetCollectionEvents(pageIndex, pageSize, stateId, out totalRecords);
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            m.State.Name,
                            m.CityId,
                            m.City.Name,
                            m.Address,
                            m.Location,
                            m.ZipCode,
                            m.Hours,
                            m.Note
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public string EditCollectionEvent(FormCollection values)
        {
            string message = "";
            bool status = true;
            if (values["oper"] == "add")
            {
                if (CanAdd())
                {
                    CollectionEvent collectionEvent = new CollectionEvent();
                    collectionEvent.StateId = Convert.ToInt32(values["State"]);
                    collectionEvent.CityId = Convert.ToInt32(values["City"]);
                    collectionEvent.ZipCode = values["Zip"];
                    collectionEvent.Address = values["Address"];
                    collectionEvent.Hours = values["Hours"];
                    collectionEvent.Location = values["Location"];
                    collectionEvent.Note = values["Note"];
                    if (collectionEventBO.AddCollectionEvent(collectionEvent))
                    {
                        message = Constants.Messages.RecordSaved;
                        status = true;
                    }
                    else
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "edit")
            {
                if (CanUpdate())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = Constants.Messages.RecordNotSaved;
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        var collectionEvent = collectionEventBO.GetCollectionEventById(id);
                        if (collectionEvent == null)
                        {
                            message = Constants.Messages.RecordNotSaved;
                            status = false;
                        }
                        else
                        {
                            collectionEvent.StateId = Convert.ToInt32(values["State"]);
                            collectionEvent.CityId = Convert.ToInt32(values["City"]);
                            collectionEvent.ZipCode = values["Zip"];
                            collectionEvent.Address = values["Address"];
                            collectionEvent.Hours = values["Hours"];
                            collectionEvent.Location = values["Location"];
                            collectionEvent.Note = values["Note"];
                            if (collectionEventBO.UpdateCollectionEvent(collectionEvent))
                            {
                                message = Constants.Messages.RecordSaved;
                                status = true;
                            }
                            else
                            {
                                message = Constants.Messages.RecordNotSaved;
                                status = false;
                            }
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else if (values["oper"] == "del")
            {
                if (CanDelete())
                {
                    if (string.IsNullOrEmpty(values["id"]))
                    {
                        message = "Record could not be deleted.";
                        status = false;
                    }
                    else
                    {
                        int id = Convert.ToInt32(values["id"]);
                        if (collectionEventBO.DeleteCollectionEvent(id))
                        {
                            message = "Record has been deleted successfully.";
                            status = true;
                        }
                        else
                        {
                            message = "Record could not be deleted.";
                            status = false;
                        }
                    }
                }
                else
                {
                    message = "AccessDenied";
                    status = false;
                }
            }
            else
            {
                message = "Invalid edit operation.";
                status = false;
            }
            MapXMLHandler.CreateCollectionEventMapXmlFile();
            return status.ToString() + ":" + message;
        }

        public ActionResult Detail(string id)
        {
            State _state = new MITSService<State>().GetSingle(x => x.Abbreviation == id);
            ViewData["StateName"] = _state == null ? string.Empty : _state.Name;
            ViewData["StateId"] = _state == null ? 0 : _state.Id;
            return View();
        }

        [HttpGet]
        public ActionResult GetCollectionEvent(int stateId)
        {
            var data = new MITSService<CollectionEvent>().GetAll(x => x.StateId == stateId).ToList();
            var jsonData = new
            {
                total = 1,
                page = 1,
                records = data.Count,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            m.Address,
                            m.City.Name,
                            m.Location,
                            m.ZipCode,
                            m.Hours,
                            m.Note
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStates()
        {
            var states = new MITSService<State>().GetAll().OrderBy(f => f.Name).ToList();
            State _default = new State() { Id = 0, Name = "Select" };
            states.Insert(0, _default);
            StringBuilder sb = new StringBuilder();
            foreach (State s in states)
            {
                sb.Append(s.Id + ":" + s.Name + ";");
            }
            var jsonData = new
            {
                data = sb.ToString().TrimEnd(';')
            };
            return Json(jsonData);
        }

        private SelectList GetStateSelectList()
        {
            var unSortedStates = new MITSService<State>().GetAll();
            var sortedStates = unSortedStates.OrderBy(f => f.Name).ToList();
            sortedStates.Insert(0, new State() { Id = 0, Name = "All" });
            var states = new SelectList(sortedStates, "Id", "Name");
            return states;
        }

        private bool CanRead()
        {
            return (accessRight != null && accessRight.Read.HasValue && accessRight.Read.Value);
        }

        private bool CanAdd()
        {
            return (accessRight != null && accessRight.Add.HasValue && accessRight.Add.Value);
        }

        private bool CanUpdate()
        {
            return (accessRight != null && accessRight.Update.HasValue && accessRight.Update.Value);
        }

        private bool CanDelete()
        {
            return (accessRight != null && accessRight.Delete.HasValue && accessRight.Delete.Value);
        }
    }
}
