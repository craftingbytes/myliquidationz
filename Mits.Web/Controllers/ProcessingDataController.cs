﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.OleDb;
using Mits.Web.Controllers;
using Mits.Common;
using Mits.BLL.Authorization;
using System.Text;

namespace Mits.Controllers
{
    public class ProcessingDataController : MITSApplicationController
    {
       // Logger mitsLogger = null;
       
        private AccessRights AccessRightsProcessingReport
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.ProcessingReportForm);

            }
        }

        private ProcessingDataBO processingDataBO;

        private Int32 CurrentInvoiceID
        {
            set
            {
                _sessionValues.SetSessionValue<int>(_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId) + Constants.SessionParameters.CurrentInvoiceID, value);
            }
            get
            {
                return _sessionValues.GetSessionIntValue(_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId) + Constants.SessionParameters.CurrentInvoiceID);
            }
        }


        private Int32 CurrentProcessingDataID
        {
            set
            {
                _sessionValues.SetSessionValue<int>(_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId) + Constants.SessionParameters.CurrentProcessingDataID, value);
            }
            get
            {
                return _sessionValues.GetSessionIntValue(_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId) + Constants.SessionParameters.CurrentProcessingDataID);
            }
        }
     
        public ProcessingDataController(ISessionCookieValues sessionValue) : this(sessionValue, new ProcessingDataBO(sessionValue))
        {
           
            processingDataBO = new ProcessingDataBO(sessionValue);
        }

        public ProcessingDataController(ISessionCookieValues sessionValue, ProcessingDataBO pDataBO)
            : base(sessionValue)
        {
            processingDataBO = pDataBO;
        
        }
     
        public class ProcessingLiteItem
        {
            public int TruckItem { get; set; }
            public int TotalWeight { get; set; }
            public int Tare { get; set; }
            public int Qty { get; set; }
            public bool RNonMetro { get; set; }
            public int Description { get; set; }
            public int PalletUnitNumber { get; set; }
            public string Service { get; set; }
            public int Product { get; set; }
       }
      
        [HttpGet]
        public ActionResult Index()
        {
           
            //if (SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString()
            //    || SessionParameters.AffiliateType == Constants.AffiliateType.Processor.ToString())
            if (AccessRightsProcessingReport != null)
            {
                CurrentProcessingDataID = 0;
                if (AccessRightsProcessingReport.Add.HasValue
                    && AccessRightsProcessingReport.Add.Value)
                {
                    //if (TempData.ContainsKey("InvoiceId"))
                    //{
                    //    ViewData["InvoiceId"] = TempData["InvoiceId"];
                    //    CurrentProcessingDataID = Convert.ToInt32(TempData["InvoiceId"]);
                    //}
                    //else if (TempData.ContainsKey("excp"))
                    //{

                    //    ViewData["excp"] = TempData["InvoiceId"];
                    //    ViewData.ContainsKey("excp");

                    //}

                    IList<State> states = processingDataBO.GetSupportedStates(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));

                    ViewData["States"] = new SelectList(states, "Id", "Name");
                    int selectedStateId = 0;
                    if (states.Count > 0)
                    {
                        selectedStateId = states[0] != null ? states[0].Id : 0;
                    }
                    ViewData["VendorCities"] = new SelectList(processingDataBO.GetCities(selectedStateId), "Id", "Name");
                    ViewData["PickUpCities"] = new SelectList(processingDataBO.GetCities(selectedStateId), "Id", "Name");

                    //ViewData["WeightCategories"] = new SelectList(processingDataBO.GetStateWeightCategories(selectedStateId), "Id", "Name");
                    ViewData["CollectionMethods"] = new SelectList(processingDataBO.GetCollectioneMethods(), "Id", "Method");

                    FillUserData(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));

                    IList<ClientAddress> clientAddresses = processingDataBO.GetClientAddresses(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
                    clientAddresses.Insert(0, new ClientAddress() { Id = 0, CompanyName = "Select" });
                    ViewData["ClientAddresses"] = new SelectList(clientAddresses, "Id", "CompanyName");
                    
                    IList<PickUpAddress> pickUpAddresses = new List<PickUpAddress>();
                    pickUpAddresses.Add(new PickUpAddress() { Id = 0, CompanyName = "Select" });
                    ViewData["PickUpAddresses"] = new SelectList(pickUpAddresses, "Id", "CompanyName");

                    IList<StateWeightCategory> weightCategories = new List<StateWeightCategory>();
                    weightCategories.Add(new StateWeightCategory() { Id = 0, Name = "Default" });
                    ViewData["WeightCategories"] = new SelectList(weightCategories, "Id", "Name");

                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Home");
                }


            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            
        }


        [HttpGet]
        public ActionResult Edit(Int32 Id)
        {
            if (AccessRightsProcessingReport == null || AccessRightsProcessingReport.Update.HasValue == false || AccessRightsProcessingReport.Update.Value == false)
                return RedirectToAction("AccessDenied", "Home");

            CurrentProcessingDataID = Id;
            ProcessingData currentPd = processingDataBO.GetProcessingDataByID(Id);
            
            if (currentPd == null)
                return RedirectToAction("Error", "Home");
            if (currentPd.Invoice.Approved == true)
                return RedirectToAction("Error", "Home");

            CurrentInvoiceID = currentPd.Invoice.Id;

            ProcessingDataViewModel model = new ProcessingDataViewModel(currentPd);
            FillUserData(Convert.ToInt32(currentPd.Invoice.AffiliateId));

            IList<State> states = processingDataBO.GetSupportedStates(Convert.ToInt32(currentPd.Invoice.AffiliateId));
            ViewData["VendorStates"] = new SelectList(states, "Id", "Name", currentPd.VendorStateId);
            ViewData["PickupStates"] = new SelectList(states, "Id", "Name", currentPd.PickupStateId);

            ViewData["VendorCities"] = new SelectList(processingDataBO.GetCities(Convert.ToInt32(currentPd.VendorStateId)), "Id", "Name", currentPd.VendorCityId);
            ViewData["PickUpCities"] = new SelectList(processingDataBO.GetCities(Convert.ToInt32(currentPd.PickupStateId)), "Id", "Name", currentPd.PickupCityId);

            ViewData["AffiliateTypeId"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId); ;

            IList<StateWeightCategory> weightCategories = new List<StateWeightCategory>();
            if (currentPd.StateWeightCategoryId == null)
            {
                ViewData["WeightCategories"] = new SelectList(processingDataBO.GetStateWeightCategories(Convert.ToInt32(currentPd.PickupStateId)), "Id", "Name", 0);
            }
            else
            {
                ViewData["WeightCategories"] = new SelectList(processingDataBO.GetStateWeightCategories(Convert.ToInt32(currentPd.PickupStateId)), "Id", "Name", currentPd.StateWeightCategoryId);
            }

            
            
            ViewData["CollectionMethods"] = new SelectList(processingDataBO.GetCollectioneMethods(), "Id", "Method", currentPd.CollectionMethodId);

            return View(model);

        }

        [HttpPost]
        public ActionResult Edit(ProcessingData pd, FormCollection form)
        {
            if (AccessRightsProcessingReport == null || AccessRightsProcessingReport.Update.HasValue == false || AccessRightsProcessingReport.Update.Value == false)
                return RedirectToAction("AccessDenied", "Home");
            ProcessingData newPd = pd;
            ProcessingData currentPd = processingDataBO.GetProcessingDataByID(pd.Id);

            if (currentPd.Invoice.Approved == true)
                return RedirectToAction("Error", "Home");

            if (currentPd.StateWeightCategoryId == 0)
                currentPd.StateWeightCategoryId = null;

            currentPd.VendorCompanyName = newPd.VendorCompanyName;
            currentPd.VendorAddress = newPd.VendorAddress;
            currentPd.VendorCityId = newPd.VendorCityId;
            currentPd.VendorStateId = newPd.VendorStateId;
            currentPd.VendorZip = newPd.VendorZip;
            currentPd.VendorPhone = newPd.VendorPhone;
            currentPd.ProcessingDate = newPd.ProcessingDate;
            currentPd.PickUpCompanyName = newPd.PickUpCompanyName;
            currentPd.PickupAddress = newPd.PickupAddress;
            currentPd.PickupCityId = newPd.PickupCityId;
            //currentPd.PickupStateId = newPd.PickupStateId;
            currentPd.PickupEmail = newPd.PickupEmail;
            currentPd.PickupPhone = newPd.PickupPhone;
            currentPd.PickupZip = newPd.PickupZip;
            currentPd.ShipmentNo = newPd.ShipmentNo;
            currentPd.RecievedBy = newPd.RecievedBy;
            currentPd.Client = newPd.Client;
            currentPd.Comments = newPd.Comments;
            currentPd.CollectionMethodId = newPd.CollectionMethodId;
            if (newPd.StateWeightCategoryId == 0)
                currentPd.StateWeightCategoryId = null;
            else
                currentPd.StateWeightCategoryId = newPd.StateWeightCategoryId;

            int planyear = Convert.ToInt32(form["ddlPlanYear"]);
            currentPd.Invoice.PlanYear = planyear;
            currentPd.Invoice.Notes = currentPd.Comments;




            bool success = processingDataBO.UpdateProcessingData(currentPd);
            if (success)
            {
                AuditEvent auditEvent = new AuditEvent();
                auditEvent.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                auditEvent.Entity = "ProcessingData";
                auditEvent.AuditActionId = Convert.ToInt32(Mits.Common.Constants.ActionType.Update);
                auditEvent.EntityId = pd.InvoiceId;
                auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
                auditEvent.TimeStamp = DateTime.Now;
                //auditEvent.Note = SessionParameters.Browser;
                processingDataBO.AddAuditEvent(auditEvent);

                if (_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType) == Constants.AffiliateType.Eworld.ToString())
                    return RedirectToAction("ProcessorIndex","Invoice");
                else               
                    return RedirectToAction("List");
                
            }
            else
                return RedirectToAction("Edit", new { Id = pd.Id });
        }


        public ActionResult DetailedReport()
        {
            if (AccessRightsProcessingReport != null)
            {
                if (AccessRightsProcessingReport.Read.HasValue
                    && AccessRightsProcessingReport.Read.Value)
                {
                    IList<State> statestList;
                    IList<State> unSortedList = new MITSService<State>().GetAll().ToList();
                    IEnumerable<State> enumSorted = unSortedList.OrderBy(f => f.Name);
                    try
                    {
                        statestList = enumSorted.ToList();
                    }
                    catch (Exception ex)
                    {
                        statestList = unSortedList;
                    }
                    State _state = new State();
                    _state.Name = "All";
                    _state.Id = 0;
                    statestList.Insert(0, _state);
                    SelectList statesSelectList = new SelectList(statestList, "Id", "Name");
                    ViewData["statesList"] = statesSelectList;
                    string affiliateType = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
                    ViewData["IsOEM"] = affiliateType == Constants.AffiliateType.OEM.ToString();

                    ViewData["IsAdmin"] = affiliateType == Constants.AffiliateType.Eworld.ToString();

                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Home");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        
        [HttpPost]
        public ActionResult GetDevicesList(string city, string state, string zip, string planyear)
        {

            if (planyear == null || planyear == "null" || planyear == "")
                planyear = "0";

            if (state == null || state == "null" || state == "")
                state = "0";

            ProcessingData currentPd = processingDataBO.GetProcessingDataByID(CurrentProcessingDataID);

            IEnumerable<ProductType> _prodcutTypes =
                processingDataBO.GetSupportedProducts(Convert.ToInt32(currentPd.Invoice.AffiliateId), Convert.ToInt32(state), Convert.ToInt32(planyear)).Distinct();

            string jqGridSelectFormat = "";
            foreach (ProductType m in _prodcutTypes)
                {
                    jqGridSelectFormat += m.Id + ":" + m.Name + ";";
                }
          
            return Content(jqGridSelectFormat.TrimEnd(';'));
        }



        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            int savedDataProcessId = 0;
            int savedInvoiceId = 0;
            try
            {
               // userInfo.UserInvoiceAffiliateID = userInfo.UserCompanyAffiliateID;

                string stateZip = string.IsNullOrEmpty(form["ddlPickupState"]) ? form["txtPickupZip"] : form["ddlPickupState"];
                //tRegion pickupRegionInfo = pdProcess.GetRegion(stateZip);
                State pickupRegionInfo = processingDataBO.GetRegion(Convert.ToInt32(form["ddlPickupState"])); //ddlPickupState represents regions from tRegionTable

                ProcessingData processData = FillProcessingData(form, pickupRegionInfo);
                Invoice invoice = GetInvoice(form, pickupRegionInfo);
                //List<InvoiceItem> invoiceItems = FillInvoiceItems(form, pickupRegionInfo.Id);


                processData.State = invoice.RegionName;
                if (processingDataBO.Save(invoice, processData))
                {
                    TempData["InvoiceId"] = invoice.Id;
                }

                savedDataProcessId = processData.Id;
                savedInvoiceId = invoice.Id;
                //processingDataBO.SendInvoiceMail(processData.InvoiceId.ToString());
                //throw new Exception();

                


            }
            catch (Exception e)
            {
                TempData["excp"] = e.Message;

            }
            if (savedDataProcessId == 0)
                return RedirectToAction("Error", "Home");
            else
            {
                AuditEvent auditEvent = new AuditEvent();
                auditEvent.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                auditEvent.Entity = "ProcessingData";
                auditEvent.AuditActionId = Convert.ToInt32(Mits.Common.Constants.ActionType.Insert);
                auditEvent.EntityId = savedInvoiceId;
                auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
                auditEvent.TimeStamp = DateTime.Now;
                //auditEvent.Note = SessionParameters.Browser;
                processingDataBO.AddAuditEvent(auditEvent);
                return RedirectToAction("Edit", new { Id = savedDataProcessId });
            }
                
        }

        
        public ActionResult Delete(int ProcessId)
        {
            bool result;
            string msg;
            string affiliateRoleName = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName);
            if (affiliateRoleName == "Administrator" || affiliateRoleName == "Executive Assistant")
            {
                try
                {
                    result = processingDataBO.DeleteProcessData(ProcessId, out msg);

                }
                catch (Exception ex)
                {
                    TempData["excp"] = ex.Message;
                    result = false;
                    msg = "Deleting failed.";
                }
            }
            else
            {
                result = false;
                msg = "You do not have authority to delete processing data.";
            }

            var jsonData = new
            {
                showResult = result,
                showMsg = msg
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Update()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request["Id"])))
                ViewData["Id"] = Request["Id"];
            if (!string.IsNullOrEmpty(Convert.ToString(Request["ProcessingDate"])))
                ViewData["ProcessingDate"] = Request["ProcessingDate"];
            if (!string.IsNullOrEmpty(Convert.ToString(Request["Admin"])))
                ViewData["Admin"] = Request["Admin"];
            if (!string.IsNullOrEmpty(Convert.ToString(Request["InvoiceId"])))
                ViewData["InvoiceId"] = Request["InvoiceId"];
            if (!string.IsNullOrEmpty(Convert.ToString(Request["ShipmentNo"])))
                ViewData["ShipmentNo"] = Request["ShipmentNo"];

            if (AccessRightsProcessingReport != null)
            {
                if (AccessRightsProcessingReport.Update.HasValue
                    && AccessRightsProcessingReport.Update.Value)
                {

                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Home");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult PerformUpdate()
        {

            try
            {
                int processingDataID = Convert.ToInt32(Request["Id"]);
                string shipmentNumber = Request["txtShipment"];
                string comments = Request["txtComments"];
                bool flag = processingDataBO.EditProcessingData(processingDataID, shipmentNumber, comments);

                return View(@"~/Views/ProcessingData/Update.aspx");

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home");
            }

        }

        //TODO: pending 
        [HttpPost]
        public ActionResult Update(FormCollection form)
        {
            StringBuilder message = new StringBuilder();
          //  message.Append("<html><body style='font-family:Calibri; font-size:11pt;'>Dear Admin,<br/><br/>");
            message.Append(Utility.BodyStart());
            message.Append(Utility.AddTextLine("Dear Admin,"));
            message.Append(Utility.AddBreak(2));
          
            //message.Append("Its requested to please update the processing report information.<br/><br/>");
            message.Append(Utility.AddTextLine("Its requested to please update the processing report information."));
            message.Append(Utility.AddBreak(2));
          
            //message.Append("Reason to update the below processing data is : " + form["txtReason"] + "<br/><br/>");
            message.Append(Utility.AddTextLine("Reason to update the below processing data is : " + form["txtReason"] +""));
            message.Append(Utility.AddBreak(2));
            
            //message.Append("Fields to be updated:<br/>");
            message.Append(Utility.AddTextLine("Fields to be updated:"));
            message.Append(Utility.AddBreak());

            Dictionary<object, object> _dictionary = new Dictionary<object, object>();
            _dictionary.Add("Invoice #", Request["InvoiceId"]);
            _dictionary.Add("Shipment #", form["txtShipment"]);
            _dictionary.Add("Comments", form["txtComments"]);
            message.Append(Utility.ConvertToHtmlTable(_dictionary));
//            message.Append(string.Format(@"<br/><br/><table style='font-family:Calibri; font-size:11pt;' border='0' width='750px' cellpadding='3' cellspacing='1's id='table1'>  
//             
//                 <tr>  
//                    <td style='width:20%;'>  
//                        <b>Invoice #:</b>  
//                    </td>  
//                    <td style='width:80%px'>  
//                          {0}
//                    </td>  
//                </tr>  
//                <tr>  
//                    <td style='width:20%;'>  
//                        <b>Shipment #:</b>  
//                    </td>  
//                    <td style='width:80%px'>  
//                        {1}
//                    </td>  
//                </tr>  
//                <tr>  
//                    <td style='width:20%;'>  
//                        <b>Comments:</b>  
//                    </td>  
//                    <td style='width:80%px'>  
//                         {2}
//                    </td>  
//                </tr>  
//                </table>", Request["InvoiceId"], form["txtShipment"], form["txtComments"]));

            //message.Append("<br/><br/><br/>");
            message.Append(Utility.AddBreak(3));
           // message.Append("Click the following link to update: " + this.AppVirtualPath + "/ProcessingData/Index?Id=" + Request["Id"] + "&ProcessingDate=" + Request["ProcessingDate"] + "&InvoiceId=" + Request["InvoiceId"] + "\r\n\r\n");
            message.Append(Utility.AddTextLine("Click the following link to update: " + this.AppVirtualPath + "/ProcessingData/Index?Id=" + Request["Id"] + "&ProcessingDate=" + Request["ProcessingDate"] + "&InvoiceId=" + Request["InvoiceId"] + "\r\n\r\n"));
            AffiliateContactBO _affContactBO = new AffiliateContactBO(_sessionValues);
            AffiliateContact _affContact= _affContactBO.GetAffiliateContact(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId));

            //message.Append("<br/><br/><br/>Thanks, <br/>" + _affContact.LastName + "," + _affContact.LastName );
            message.Append(Utility.AddBreak(3));
            message.Append(Utility.AddTextLine("Thanks,"));
            message.Append(Utility.AddBreak());
            message.Append(Utility.AddTextLine(_affContact.LastName + "," + _affContact.LastName));
          //  message.Append("<br/>" + new AffiliateBO().GetAffiliate(SessionParameters.AffiliateId).Name + "</body></html>");
            message.Append(Utility.AddBreak());
            message.Append(Utility.AddTextLine(new AffiliateBO().GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId)).Name));
            message.Append(Utility.BodyEnd());
      
            EmailHelper.SendMail("Update Processing Data Request", message.ToString(), new string[] { ConfigHelper.AdminEmail }, null, null);


            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult List()
        {

            if (AccessRightsProcessingReport != null)
            {
                if (AccessRightsProcessingReport.Read.HasValue
                    && AccessRightsProcessingReport.Read.Value)
                {
                    IList<State> statestList;
                    IList<State> unSortedList = new MITSService<State>().GetAll().ToList();
                    IEnumerable<State> enumSorted = unSortedList.OrderBy(f => f.Name);
                    try
                    {
                        statestList = enumSorted.ToList();
                    }
                    catch (Exception ex)
                    {
                        statestList = unSortedList;
                    }
                    State _state = new State();
                    _state.Name = "All";
                    _state.Id = -1;
                    statestList.Insert(0, _state);
                    SelectList statesSelectList = new SelectList(statestList, "Id", "Name");
                    ViewData["statesList"] = statesSelectList;
                    string affiliateType = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
                    ViewData["IsOEM"] = affiliateType == Constants.AffiliateType.OEM.ToString();

                    ViewData["IsAdmin"] = affiliateType == Constants.AffiliateType.Eworld.ToString();

                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Home");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetProcessingDataList(string sidx, string sord, int page, int rows, bool _search,
            string searchString, string State, string BeginDate, string EndDate)
        {
             int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = processingDataBO.GetPaged(pageIndex, pageSize, searchString, State, BeginDate,
                EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), out totalRecords).AsEnumerable<ProcessingData>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                          
                            String.Format("{0:MM/dd/yyyy}",m.ProcessingDate),
                            m.Client,
                            m.RecievedBy,
                            m.State,
                            m.Invoice.Id,
                            m.ShipmentNo,
                            m.Id,
                            m.Id,
                            m.Id,
                            m.Invoice.Id,
                            GetDocsListHtml(m.Id),
                            GetDialogueHtml(m.Id),
                            m.Invoice.Approved == null ? false : m.Invoice.Approved,
                            m.Invoice.Verified
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
       
        private string GetDocsListHtml(int pDataId)
        {
            List<Document> _docs = processingDataBO.GetDocsByPDataId(pDataId);

            string _markup = string.Empty;

            if(_docs.Count>0)
            {
                _markup = "<div onclick='GetAttachments(" + pDataId + ")'><img src='../Content/images/icon/sm/paperclip.gif' title='View attachments'></img></div>";
            }

            return _markup;
        }
        private string GetDialogueHtml(int pDataId)
        {

            return "<div onclick='javascript:openFileDialog(" + pDataId + ")'><img src='../Content/images/icon/sm/attach_file.gif' title='Attach File' /></div>";
        }

        private DocumentType GetDocTypeId(String fileName)
        {
            DocumentType documentType = new DocumentType();
            String extension = System.IO.Path.GetExtension(fileName);

            List<DocumentType> docTypeList = new MITSService<DocumentType>().GetAll().ToList();

           foreach (DocumentType docType in docTypeList)
            {
                if (docType.FileExtension == extension)
                {
                    return docType;

                }
            }
           return null;
        }


        public ActionResult GetAttachments(string processDataId)
        {
            List<Document> _docs =processingDataBO.GetDocsByPDataId(Convert.ToInt32(processDataId));

            var jsonData = new
            {
                rows = (
                    from d in _docs
                    select new
                    {
                       DocumentID = d.Id,
                       UploadFileName = d.UploadFileName.Replace(" ", "&nbsp;")
                    }).ToList()

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveAttachment(string fileName, string processDataId)
        {
            ActionResult result = null;

            try
            {
               
                string path = Server.MapPath("~/Uploads/" + fileName);
                String saveLocation = Server.MapPath("~/Uploads/") + Guid.NewGuid();
                Document doc = new Document();

                doc.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                doc.ServerFileName = saveLocation;
                doc.UploadFileName = fileName;
                doc.UploadDate = System.DateTime.Now;

                int savedFileId = processingDataBO.SaveAttachment(doc);

                if (!string.IsNullOrEmpty(processDataId))
                {
                    DocumentRelation docRel = new DocumentRelation();
                    docRel.DocumentID = savedFileId;
                    docRel.ForeignID = Convert.ToInt32(processDataId);
                    processingDataBO.SaveAttachmentRelation(docRel);
                }

                result = Json("True", JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return RedirectToAction("Error", "Home");
            }

            return result;
        }

        public JsonResult GetAttachmentsHTML()
        {
            List<Document> docs = processingDataBO.GetDocsByPDataId(CurrentProcessingDataID);

            string html = "No attachment found.";
            if (docs.Count > 0)
            {
                html = "<table width='100%'>";
                foreach (Document doc in docs)
                {
                    html += "<tr><td width='80%'>"
                          + "<a href=javascript:Download('" + doc.UploadFileName + "'); style='color: #FF9900; text-decoration: none; _text-decoration: none;'>"
                          + "<img src='../../Content/images/d4.PNG' alt='Open selected file' title='Open selected file' id='btnDel' border='0' />"
                          + ": <font style='font-weight: bold; vertical-align: top;'>" + doc.UploadFileName + "</font> </a>"
                          + "</td><td width='20%'>"
                          + "<img src='../../Content/images/icon/delete.png' alt='Remove attachment' style='cursor:hand;' title='Remove attachment' id='btnDel' border='0' onclick='javascript:DeleteAttachment(" + doc.Id + ");' />"
                          + "</td></tr>";
                }
                html += "</table>";
            }
            return Json(html);
        }
        public JsonResult DeleteAttachment(int? docID)
        {
            bool result = new AffiliateBO().DeleteDocument(docID.Value);

            return Json(result);
        }

        [HttpGet]
        public ActionResult ExcelImport(string sidx, string sord, int page, int rows, bool _search, string searchString, bool import, string fileName)
        {
            //mitsLogger = new Logger();
            List<ProcessingLiteItem> items = null;
            if (!import)
            {
                //var emptyjsonData = new
                //{
                //    rows = (
                //  from m in items
                //  select new
                //  {
                //      id = m.TruckItem,
                //      cell = new object[] { 
                //            m.TruckItem,m.Product,m.Service,m.PalletUnitNumber,m.Description,m.RNonMetro,m.Qty,m.Tare,m.TotalWeigtht}
                //  }).ToArray()
                //};

                return Content("{rows:[]}");
            }


            string path = Server.MapPath("~/Uploads/" + fileName);
            string extension = fileName.Substring(fileName.LastIndexOf(".") + 1);
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0;";
            if (extension.ToUpper() == "XLSX")
            {
                connString = "Provider=Microsoft.Ace.OleDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";
            }
           // mitsLogger.Info("Processing data File path: " + path + "");
            // Create the connection object
            OleDbConnection oledbConn = new OleDbConnection(connString);

            // Open connection
            oledbConn.Open();

            // Create OleDbCommand object and select data from worksheet Sheet1
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", oledbConn);

            // Create new OleDbDataAdapter
            OleDbDataAdapter oleda = new OleDbDataAdapter();

            oleda.SelectCommand = cmd;

            // Create a DataSet which will hold the data extracted from the worksheet.
            DataSet ds = new DataSet();

            // Fill the DataSet from the data extracted from the worksheet.
            oleda.Fill(ds);
            oledbConn.Close();

            items = new List<ProcessingLiteItem>();

            ProcessingLiteItem lineItem;
            int i = 0;
            int j = 0;
            DataRow _row = ds.Tables[0].Rows[0];
            if (_row != null)
            {
                if (Convert.ToString(_row[2]).ToLower() == "true" || Convert.ToString(_row[2]).ToLower() == "false")
                    j = 0;
                else if (Convert.ToString(_row[3]).ToLower() == "true" || Convert.ToString(_row[3]).ToLower() == "false")
                    j = 1;
            }
           // mitsLogger.Info("File rows count on Processing data: " + ds.Tables[0].Rows.Count + "");
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                
                lineItem = new ProcessingLiteItem();

                //if (row["Description"] != DBNull.Value)
                //    lineItem.Description = Convert.ToInt32(row["Description"]);
                if (row[j+1] != DBNull.Value)
                    lineItem.Description = Convert.ToInt32(row[j+1]);

                //if(row["Pallet/Unit Numer"]!=DBNull.Value)
                //    lineItem.PalletUnitNumber=Convert.ToInt32(row["Pallet/Unit Numer"]);

                if (row[j] != DBNull.Value)
                    lineItem.PalletUnitNumber = Convert.ToInt32(row[j]);

                if (lineItem.PalletUnitNumber <= 0)
                {
                    break;
                }
                lineItem.Product = 1;

                //if(row["QTY/Weight(lbs)"] !=DBNull.Value)
                //    lineItem.Qty=Convert.ToInt32(row["QTY/Weight(lbs)"]);

                if (row[j+3] != DBNull.Value)
                    lineItem.Qty = Convert.ToInt32(row[j+3]);

                //lineItem.RNonMetro = Convert.ToString(row["Metro"]).ToLower() == "true" ? true : false;
                lineItem.RNonMetro = Convert.ToString(row[j+2]).ToLower() == "true" ? true : false;

                lineItem.Service = "Processing";
                //if (row["Tare(lbs)"] != DBNull.Value)
                //    lineItem.Tare = Convert.ToInt32(row["Tare(lbs)"]);

                if (row[j+4] != DBNull.Value)
                    lineItem.Tare = Convert.ToInt32(row[j+4]);

                //if (row["Total Weight"] != DBNull.Value)
                //    lineItem.TotalWeight = Convert.ToInt32(row["Total Weight"]);
                if (row[j+5] != DBNull.Value)
                    lineItem.TotalWeight = Convert.ToInt32(row[j+5]);

                lineItem.TruckItem = ++i;
                items.Add(lineItem);
            }

            var jsonData = new
            {
                total = 500,
                page = 1,
                records = items.Count,

                rows = (
                    from m in items
                    select new
                    {
                        id = m.TruckItem,
                        cell = new object[] { m.PalletUnitNumber, m.Description, m.RNonMetro, m.Qty, m.Tare }//,m.TotalWeight," "}
                    }).ToArray()


            };


            return Json(jsonData, JsonRequestBehavior.AllowGet);



            // Close connection




        }

        public ActionResult GetClientAddress(int id)
        {
            ClientAddress client = new MITSService<ClientAddress>().GetSingle(x => x.Id == id);
            List<PickUpAddress> pickUps = new MITSService<PickUpAddress>().GetAll(x => x.ClientAddressId == id).OrderBy(x => x.CompanyName).ToList();
            pickUps.Insert(0, new PickUpAddress() { Id = 0, CompanyName = "Select" });

            if (client == null)
            {
                var jsonData = new
                {
                    address = string.Empty,
                    state = 0,
                    city = 0,
                    zip = string.Empty,
                    phone = string.Empty,
                    pickUpAddresses = (
                        from m in pickUps
                        select new
                        {
                            Value = m.Id,
                            Text = m.CompanyName
                        }).ToArray()
                };
                return Json(jsonData);
            }
            else
            {
                var jsonData = new
                {
                    address = client.Address,
                    state = client.StateId,
                    city = client.CityId,
                    zip = string.IsNullOrEmpty(client.Zip) ? string.Empty : client.Zip,
                    phone = string.IsNullOrEmpty(client.Phone) ? string.Empty : client.Phone,
                    pickUpAddresses = (
                        from m in pickUps
                        select new
                        {
                            Value = m.Id,
                            Text = m.CompanyName
                        }).ToArray()
                };
                return Json(jsonData);
            }
        }

        public ActionResult GetPickUpAddress(int id)
        {
            PickUpAddress pickUp = new MITSService<PickUpAddress>().GetSingle(x => x.Id == id);
            if (pickUp == null)
            {
                var jsonData = new
                {
                    address = string.Empty,
                    state = 0,
                    city = 0,
                    zip = string.Empty,
                    phone = string.Empty,
                    fax = string.Empty,
                    mail = string.Empty,
                    method = 0
                };
                return Json(jsonData);
            }
            else
            {
                var jsonData = new
                {
                    address = pickUp.Address,
                    state = pickUp.StateId,
                    city = pickUp.CityId,
                    zip = pickUp.Zip,
                    phone = pickUp.Phone,
                    fax = pickUp.Fax,
                    mail = pickUp.Email,
                    method = pickUp.CollectionMethodId
                };
                return Json(jsonData);
            }
        }

        #region Private Methods
        private ProcessingData FillProcessingData(FormCollection form, State pickupRegionInfo)
        {
            ProcessingData data = new ProcessingData();
            
            string stateZip = string.IsNullOrEmpty(form["ddlVendorState"]) ? form["txtVendorZip"] : form["ddlVendorState"];
            //tRegion vendorRegionInfo = pdProcess.GetRegion(stateZip);
            State vendorState = processingDataBO.GetRegion(Convert.ToInt32(form["ddlVendorState"]));
            City vendorCity = processingDataBO.GetCity( Convert.ToInt32( form["ddlVendorCity"]));
            City pickupCity = processingDataBO.GetCity(Convert.ToInt32(form["ddlPickupCity"]));

            data.ProcessingDate = Convert.ToDateTime(form["txtDate"]);
            
            data.Comments = form["txtComment"];
            data.VendorCompanyName = form["txtCompanyName"];

            data.VendorAddress = form["txtVendorAddress"];
            data.VendorStateId = vendorState.Id;
         
            if (vendorCity != null)
            {
                data.VendorCityId = vendorCity.Id;
            }

            data.VendorZip = form["txtVendorZip"];
            data.VendorPhone = form["txtVendorPhone"];

            data.PickUpCompanyName = form["txtPickUpCompanyName"];
            data.PickupAddress = form["txtPickupAddress"];
            data.PickupStateId = pickupRegionInfo.Id;

            if (pickupCity != null)
            {
                data.PickupCityId = pickupCity.Id;
            }

            data.PickupZip = form["txtPickupZip"];
            data.PickupPhone = form["txtPickupPhone"];
            data.PickupFax = form["txtPickupFax"];
            data.PickupEmail = form["txtPickupEmail"];

            data.Client = form["txtClient"];
            data.RecievedBy = form["txtRecievedBy"];
            data.ShipmentNo = form["txtShipmentNo"];
            data.CollectionMethod = null;
            if (!string.IsNullOrEmpty(form["ddlCollectionMethod"]))
            {
                data.CollectionMethodId = Convert.ToInt32(form["ddlCollectionMethod"]);
            }

            data.StateWeightCategoryId = null;
            if (!string.IsNullOrEmpty(form["ddlWeightCategories"]) && Convert.ToInt32(form["ddlWeightCategories"]) != 0)
            {
                data.StateWeightCategoryId = Convert.ToInt32(form["ddlWeightCategories"]);
            }

            return data;
        }
        private Invoice GetInvoice(FormCollection form, State regionInfo)
        {
            Invoice invoice = new Invoice();


            invoice.AffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
            invoice.InvoiceDate = DateTime.Now;
            invoice.PlanYear = Convert.ToInt32(form["ddlPlanYear"]);
            invoice.Receivable = false;
            invoice.Id = regionInfo.Id;
            invoice.RegionName = regionInfo.Name;
            invoice.State = regionInfo;
            invoice.ShowNotes = false;
            invoice.Notes = form["txtComment"];
           
            
            return invoice;
        }
        //private void InitializeNewInvoice(int regionId, ref DataTable regions, ref DataTable productTypes)
        //{
        //   // Ewo.sqlDataStore.Affiliate sql = new Ewo.sqlDataStore.Affiliate();
        //    DataSet dataset = new DataSet();
        //    dataset = sql.NewInvoice(userInfo.UserImpersonationAffiliateID,
        //                regionId,
        //                Convert.ToInt32("0"));
        //    //selRegion.Attributes["onchange"] = "document.forms[0].submit();";
        //    if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0] != null && dataset.Tables[0].Rows.Count > 0)
        //    {
        //        Int32 invoiceToAffiliateID = 0;
        //        Int32 invoiceFromAffiliateID = 0;
        //        userInfo.UserInvoiceAffiliateID = userInfo.UserCompanyAffiliateID;//default this to the from company
        //        invoiceFromAffiliateID = Convert.ToInt32(dataset.Tables[0].Rows[0]["AffiliateID"]);

        //        Boolean editInvoiceToVisible = ((userInfo.IsUserSuper) && (dataset.Tables.Count >= 5));
        //        if (editInvoiceToVisible)//if they're admin and they're themselves
        //        {
        //            userInfo.UserInvoiceAffiliateID = userInfo.UserCompanyAffiliateID;//if it's admin, then it's from us to them and the to company needs to provide the information

        //        }

        //        invoiceToAffiliateID = Convert.ToInt32(dataset.Tables[1].Rows[0]["AffiliateID"]);

        //        userInfo.UserInvoiceAffiliateID = invoiceFromAffiliateID;//default is we need the information of the affiliate making the invoice
        //        if (editInvoiceToVisible) userInfo.UserInvoiceAffiliateID = invoiceToAffiliateID;//if user is admin, we need the information of the affiliate receiving the invoice
        //        regions = dataset.Tables[2]; // States regions
        //        productTypes = dataset.Tables[3];  // Products
        //    }
        //    else
        //    {
        //        //statusMessage.Text = "An error has occured attempting to load a new invoice";
        //    }

        //}
        private string FillUserData(int affiliateId)
        {
            Affiliate affiliateData = processingDataBO.GetAffiliateDetails(affiliateId);
            string state = "";
            if (affiliateData != null)
            {
                ViewData["CompName"] = affiliateData.Name;

                ViewData["StreetAddress"] = affiliateData.Address1;
                ViewData["StreetAddress"] += "<br />" + affiliateData.Address2;
                ViewData["CityState"] = affiliateData.City != null ? affiliateData.City.Name : "";
                if (affiliateData.State != null)
                {
                    ViewData["CityState"] += ", " + affiliateData.State.Name;
                    state = affiliateData.State.Name;
                }

                ViewData["CityState"] += " " + affiliateData.Zip;
                ViewData["Phone"] = affiliateData.Phone;
                ViewData["Email"] = affiliateData.Email;

               
            }
            return state;
        }
        private List<InvoiceItem> FillInvoiceItems(FormCollection form, int regionID)
        {
            List<InvoiceItem> invoiceItems = new List<InvoiceItem>();
            InvoiceItem item = null;
            JavaScriptSerializer JSS = new JavaScriptSerializer();
            
            string key = string.Empty;
            string value = string.Empty;
            string state = processingDataBO.GetRegion(Convert.ToInt32(form["ddlPickupState"])).Abbreviation; // ddl PickupState represents regions

            StateSpecificRate rate = processingDataBO.GetStateSpecificRate(state);
            for (int i = 1; i <= form.AllKeys.Length; i++)
            {
                key = "Item_" + i.ToString();
                if (form[key] != null)
                {
                    value = form[key];
                    string[] jsonCollection = value.Split(new char[] { ',' });
                    item = new InvoiceItem();
                    item.ServiceTypeId = 3;// Service Type is Processing
                    //TODO:
                    //item.ServiceTypeName = "Processing";

                    foreach (var jsonItem in jsonCollection)
                    {
                        string[] clientitem = jsonItem.Split(new char[] { ':' });
                        switch (clientitem[0])
                        {
                            case "PalletUnitNumber":
                                item.PalletNumber = Convert.ToInt32(clientitem[1]);
                                break;
                            case "Description":
                                item.ProductTypeId = Convert.ToInt32(clientitem[1]);
                                break;
                            case "RNonMetro":
                                item.IsMetro = Convert.ToBoolean(clientitem[1]);
                                break;
                            case "Qty":
                                item.Quantity = Convert.ToInt32(clientitem[1]);
                                break;
                            case "Tare":
                                item.Tare = Convert.ToInt32(clientitem[1]);
                                break;
                        }
                    }
                    
                    item.Quantity = item.Quantity - Convert.ToDecimal(item.Tare);

                    if ((state.Equals("57") || state.Equals("MN")) && item.IsMetro == true)
                    {
                        switch (state)
                        {
                            case "WI":
                                item.QuantityAdjustPercent = Convert.ToDecimal(rate.Rural);
                                break;

                            case "MN":
                                item.QuantityAdjustPercent = Convert.ToDecimal(rate.Metro);
                                break;
                        }

                    }



                    item.QuantityType = new MITSService<QuantityType>().GetSingle(x => x.Name == "Pound");
                    item.ServiceType = new MITSService<ServiceType>().GetSingle(x => x.Name == "Processing");
                    
                    invoiceItems.Add(item);

                }
            }
            return invoiceItems;
        }
       /* private void SetDefaultRate(int userInvoiceAffiliateID, int regionID, InvoiceItem invoiceItem)
        {
            Lists sql = new Lists();
            DataSet dataset = sql.InvoiceQtyTypeList(userInvoiceAffiliateID, regionID, invoiceItem.ProductTypeID, invoiceItem.ServiceTypeID);
            if (dataset != null && dataset.Tables[0].Rows.Count > 0)
            {
                DataRow dRow = dataset.Tables[0].Rows[0];
                invoiceItem.QuantityTypeID = Convert.ToInt32(dRow["QuantityTypeID"]);
                invoiceItem.QuantityTypeName = Convert.ToString(dRow["QuantityTypeName"]);
                invoiceItem.Rate = Convert.ToDecimal(dRow["Rate"]);
                invoiceItem.QuantityAdjustPercent = Convert.ToDecimal(1.00);
            }

        }*/
        private SelectList FillRegionsList(int affiliateId)
        {
            List<SelectListItem> regionList = new List<SelectListItem>();
            SelectListItem selectItem = null;


            foreach (State item in processingDataBO.GetSupportedStates(affiliateId))
                {
                    selectItem = new SelectListItem();
                    selectItem.Text = item.Name;
                    selectItem.Value = item.Id.ToString();
                    regionList.Add(selectItem);
                }
           
            SelectList selList = new SelectList(regionList, "Value", "Text");
            return selList;
        }
        #endregion

        [HttpPost]
        public ActionResult GetStateWeightCategories(string state)
        {

            if (state == null || state == "null" || state == "")
                state = "0";

            int stateId = int.Parse(state);
            //processingDataBO.GetStateWeightCategories(Convert.ToInt32(state)).Distinct();

            IList<StateWeightCategory> data;
            var data1 = new MITSService<StateWeightCategory>().GetAll(x => x.StateId == stateId);
            IEnumerable<StateWeightCategory> enumList = data1.OrderBy(f => f.Name);

            StateWeightCategory _category = new StateWeightCategory();
            _category.Id = 0;
            _category.Name = "Default";

            try
            {
                data = enumList.ToList();
            }
            catch (Exception)
            {
                data = data1;
            }

            data.Insert(0, _category);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name
                    }).ToArray()
            };
            return Json(jsonData);

        }

        [HttpPost]
        public JsonResult ChangeWeightCategory(int stateWeightCategoryId)
        {
            MITSEntities db = new MITSEntities();
            p_changeProcessingDataWeightCategory_Result changeResult = db.p_changeProcessingDataWeightCategory(CurrentProcessingDataID, stateWeightCategoryId);



            //decimal newRatio = 1;
            //if (stateWeightCategoryId != 0)
            //{
            //    MITSEntities db = new MITSEntities();

            //    StateWeightCategory stateWeightCategory = db.StateWeightCategories.FirstOrDefault(x => x.Id == stateWeightCategoryId);
            //    newRatio = stateWeightCategory == null ? 1 : stateWeightCategory.Ratio;
            //}



            //ProcessingData currentPd = processingDataBO.GetProcessingDataByID(CurrentProcessingDataID);
            //int oldStateWeightCategoryId = currentPd.StateWeightCategoryId == null ? 0 : Convert.ToInt32(currentPd.StateWeightCategoryId);
            //currentPd.StateWeightCategoryId = stateWeightCategoryId;

            //bool success = processingDataBO.UpdateProcessingData(currentPd);


            if (Convert.ToBoolean(changeResult.Success))
                return Json(new { success = true, message = "Updated" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "An error occured trying to update State Weight Category, you may have exceeded your target weight.", stateWeightCategoryId = changeResult.CategoryId }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult ChangePickupState(int pickupStateId)
        {
            MITSEntities db = new MITSEntities();
            p_changePickupState_Result changeResult = db.p_changePickupState(CurrentProcessingDataID, pickupStateId);

            if (Convert.ToBoolean(changeResult.Success))
                return Json(new { success = true, message = "Updated" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = changeResult.Message, pickupStateId = changeResult.PickupStateId }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ChangePlanYear(int planYear)
        {
            MITSEntities db = new MITSEntities();
            p_changePlanYear_Result changeResult = db.p_changePlanYear(CurrentProcessingDataID, planYear);

            if (Convert.ToBoolean(changeResult.Success))
                return Json(new { success = true, message = "Updated" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = changeResult.Message, planYear = changeResult.OldPlanYear }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetInvoiceItems(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;


            //: ['Pallet Unit #*', 'Product*', 'Metro', 'Total Qty/Weight*', 'Tare*', 'Net Qty/Weight', 'Actions'],

            var data = processingDataBO.GetInvoiceItems(pageIndex, pageSize, CurrentInvoiceID, out totalRecords).AsEnumerable<InvoiceItem>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.PalletNumber,
                            //m.ProductType.Name,
                            m.ProductTypeId,
                            m.OriginalQuantity,
                            m.Tare,
                            m.Quantity,
                            m.Rate
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult EditInvoiceItem(FormCollection col)
        {
            if (AccessRightsProcessingReport == null || AccessRightsProcessingReport.Update.HasValue == false || AccessRightsProcessingReport.Update.Value == false)
                return Json(new { success = false, message = "Cannot update.  Insufficient Privileges.", invoiceItemId = 0 }, JsonRequestBehavior.AllowGet);

            Int32 itemId = 0;
            bool saved = false;
            decimal _qty = Convert.ToDecimal(col["Qty"]);
            decimal _totalWeight = 0;
            decimal _rate = 0;


            
           
            try
            {
                if (col.AllKeys.Contains("oper"))
                {
                    ProcessingData currentPd = processingDataBO.GetProcessingDataByID(CurrentProcessingDataID);
                    decimal _ratio = currentPd.StateWeightCategoryId == null ? 1 : Convert.ToDecimal(currentPd.StateWeightCategory.Ratio);
                    if (currentPd.Invoice.Approved == true)
                        return Json(new { success = false, message = "Cannot update.  Invoice already approved.", invoiceItemId = 0 }, JsonRequestBehavior.AllowGet);

                    if (col["oper"] == "add")
                    {
                        InvoiceItem item = new InvoiceItem();
                        item = new InvoiceItem();
                        item.InvoiceId = CurrentInvoiceID;
                        item.ServiceTypeId = 3;
                        item.QuantityTypeId = 2;
                        item.PalletNumber = Convert.ToInt32(col["PalletUnitNumber"]);
                        item.ProductTypeId = Convert.ToInt32(col["Description"]);
                        item.OriginalQuantity = _qty;
                        item.Tare = Convert.ToInt32(col["Tare"]);
                        _totalWeight = (_qty - Convert.ToDecimal(item.Tare)) * _ratio;
                        item.Quantity = _totalWeight;

                        if (_totalWeight <=0)
                            return Json(new { success = false, message = "Net weight must be greater than 0", invoiceItemId = 0 }, JsonRequestBehavior.AllowGet);

                        if (processingDataBO.IsBelowTarget(CurrentInvoiceID, _totalWeight))
                            itemId = processingDataBO.AddInvoiceItem(item);
                        else
                            return Json(new { success = false, message = "Cannot add because target has been exceeded.", invoiceItemId = 0 }, JsonRequestBehavior.AllowGet);
                        InvoiceItem newInvoiceItem = processingDataBO.GetInvoiceItemById(itemId);
                        _rate = Convert.ToDecimal(newInvoiceItem.Rate);
                        
                    }
                    else if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                    {
                        //itemId = Convert.ToInt32(col["id"]);
                        int currentItemID = Convert.ToInt32(col["id"]);
                        InvoiceItem item = processingDataBO.GetInvoiceItemById(currentItemID);
                        if (item != null)
                        {
                            item.ServiceTypeId = 3;
                            item.QuantityTypeId = 2;
                            item.PalletNumber = Convert.ToInt32(col["PalletUnitNumber"]);
                            item.ProductTypeId = Convert.ToInt32(col["Description"]);
                            item.OriginalQuantity = _qty;
                            item.Tare = Convert.ToInt32(col["Tare"]);
                            decimal originalTotalQuantity = Convert.ToDecimal(item.Quantity);

                            _totalWeight = (_qty - Convert.ToDecimal(item.Tare)) * _ratio;
                            item.Quantity = _totalWeight;

                            if (_totalWeight <= 0)
                                return Json(new { success = false, message = "Net weight must be greater than 0", invoiceItemId = 0 }, JsonRequestBehavior.AllowGet);

                            if (processingDataBO.IsBelowTarget(CurrentInvoiceID, _totalWeight - originalTotalQuantity))
                                saved = processingDataBO.UpdateInvoiceItem(item);
                            else
                                return Json(new { success = false, message = "Cannot add because target has been exceeded.", invoiceItemId = 0 }, JsonRequestBehavior.AllowGet);
                            InvoiceItem newInvoiceItem = processingDataBO.GetInvoiceItemById(currentItemID);
                            _rate = Convert.ToDecimal(newInvoiceItem.Rate);
                            
                        }
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", invoiceItemId = 0 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, message = "Success", invoiceItemId = itemId, totalWeight = _totalWeight, rate=_rate }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteInvoiceItem(int invoiceItemId)
        {
            if (AccessRightsProcessingReport == null || AccessRightsProcessingReport.Update.HasValue == false || AccessRightsProcessingReport.Update.Value == false)
                return Json(new { success = false, message = "Cannot update.  Insufficient Privileges.", invoiceItemId = 0 }, JsonRequestBehavior.AllowGet);

            try
            {
                if (processingDataBO.DeleteInvoiceItem(invoiceItemId))
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                else
                    throw new Exception("Error");

            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to delete item." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExcelImport2(string fileName)
        {
            //Boolean success = false;
            

            string path = Server.MapPath("~/Uploads/" + fileName);
            string extension = fileName.Substring(fileName.LastIndexOf(".") + 1);
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0;";
            if (extension.ToUpper() == "XLSX")
            {
                connString = "Provider=Microsoft.Ace.OleDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";
            }
            OleDbConnection oledbConn = new OleDbConnection(connString);
            oledbConn.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", oledbConn);
            OleDbDataAdapter oleda = new OleDbDataAdapter();
            oleda.SelectCommand = cmd;
            DataSet ds = new DataSet();
            oleda.Fill(ds);
            oledbConn.Close();

            List<ProcessingLiteItem> items  = new List<ProcessingLiteItem>();
            
            int i = 0;
            int j = 0;
            DataRow _row = ds.Tables[0].Rows[0];
            if (_row != null)
            {
                if (Convert.ToString(_row[2]).ToLower() == "true" || Convert.ToString(_row[2]).ToLower() == "false")
                    j = 0;
                else if (Convert.ToString(_row[3]).ToLower() == "true" || Convert.ToString(_row[3]).ToLower() == "false")
                    j = 1;
            }

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                ProcessingLiteItem lineItem = new ProcessingLiteItem();

                if (row[j] != DBNull.Value)
                    lineItem.PalletUnitNumber = Convert.ToInt32(row[j]);
                if (row[j + 1] != DBNull.Value)
                    lineItem.Description = Convert.ToInt32(row[j + 1]);

                if (row[j + 3] != DBNull.Value)
                    lineItem.Qty = Convert.ToInt32(row[j + 3]);

                if (row[j + 4] != DBNull.Value)
                    lineItem.Tare = Convert.ToInt32(row[j + 4]);

                lineItem.TruckItem = ++i;
                items.Add(lineItem);
            }

            var products = (from productIds in items
                            select productIds.Description).Distinct();
            ProcessingData currentPd = processingDataBO.GetProcessingDataByID(CurrentProcessingDataID);
            decimal _ratio = currentPd.StateWeightCategoryId == null ? 1 : Convert.ToDecimal(currentPd.StateWeightCategory.Ratio);

            foreach (int productId in products)
            {
                MITSEntities db = new MITSEntities();
                AffiliateContractRate rate = db.AffiliateContractRates.Where(x =>
                    x.AffiliateId == currentPd.Invoice.AffiliateId &&
                    x.PlanYear == currentPd.Invoice.PlanYear &&
                    x.ServiceTypeId == 3 &&
                    x.QuantityTypeId == 2 &&
                    x.ProductTypeId == productId).FirstOrDefault<AffiliateContractRate>();

                if (rate == null)
                    return Json(new { success = false, message = "Missin  rate for productId=" + productId }, JsonRequestBehavior.AllowGet);
               
            }

            decimal sumtotalquantity = 0;
            foreach (ProcessingLiteItem pitem in items)
            {
                sumtotalquantity += (pitem.Qty - pitem.Tare) * _ratio;
            }

            if (!processingDataBO.IsBelowTarget(Convert.ToInt32(currentPd.InvoiceId), sumtotalquantity))
                return Json(new { success = false, message = "Total Weight Exceeds Target" }, JsonRequestBehavior.AllowGet);
            foreach (ProcessingLiteItem pitem in items)
            {
                InvoiceItem item = new InvoiceItem();
                item = new InvoiceItem();
                item.InvoiceId = Convert.ToInt32(currentPd.InvoiceId);
                item.ServiceTypeId = 3;
                item.QuantityTypeId = 2;
                item.PalletNumber = pitem.PalletUnitNumber;
                item.ProductTypeId = pitem.Description;
                item.OriginalQuantity = pitem.Qty;
                item.Tare = pitem.Tare;
                decimal _totalWeight = (pitem.Qty - pitem.Tare) * _ratio;
                item.Quantity = _totalWeight;
                int itemId = processingDataBO.AddInvoiceItem(item);
            }
            return Json(new { success = true, message = "Updated" }, JsonRequestBehavior.AllowGet);
            
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateVerify(Int32 id)
        {
            string msg = String.Empty;
            if (AccessRightsProcessingReport != null && AccessRightsProcessingReport.Update.HasValue == true && AccessRightsProcessingReport.Update == true)
            {
                msg = processingDataBO.UpdateVerifyByInvoiceId(id);
            }
            else
            {
                msg = "Access Denied";
            }

            if (msg == "Success")
            {
                ProcessingData pd = processingDataBO.GetProcessingDataByInvoiceID(id);

                return Json(new { success = true, message = msg, verified = pd.Invoice.Verified, pdId = pd.Id });
            }
            else
                return Json(new { success = false, message = msg });

        }

    }
}
