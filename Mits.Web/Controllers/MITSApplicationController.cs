﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.Common;
using Mits.BLL.Authorization;
using Mits.DAL.EntityModels;

namespace Mits.Web.Controllers
{
    public class MITSApplicationController : Controller
    {
        protected ISessionCookieValues _sessionValues = null;

        public string RootPath
        {
            get
            {
                if (System.Web.HttpContext.Current.Request.Url.IsDefaultPort)
                    return System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Host + "/";
                else
                    return System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Host + ":" + System.Web.HttpContext.Current.Request.Url.Port.ToString() + "/";
            }
        }

        public MITSApplicationController(ISessionCookieValues sessionValues)
        {
            _sessionValues = sessionValues;
            ViewData["MenuHTML"] = GetMenusHtmlNew();
            UpdateViewData();
        }

        protected void UpdateViewData()
        {
            int masterContact = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
            if (masterContact > 0)
            {
                MITSEntities db = new MITSEntities();
                var ac = from c in db.AffiliateContacts
                         join m in db.AffiliateMasterContacts on c.Id equals m.AssociateContactId
                         join a in db.Affiliates on c.AffiliateId equals a.Id
                         where m.MasterAffiliateContactId == masterContact
                         select new { Id = c.Id, Name = a.Name + " | " + c.UserName };

                ViewData["AssociatedContacts"] = new SelectList(ac, "Id", "Name", _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId));
                //IList<State> states = processingDataBO.GetSupportedStates(SessionParameters.AffiliateId);
                //ViewData["States"] = new SelectList(states, "Id", "Name");

                int affilateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate affiliate = new MITSService<Affiliate>().GetSingle(x => x.Id == affilateId);
                ViewData["AffiliateName"] = affiliate.Name;
                ViewData["MasterAffiliateContactId"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.MasterAffiliateContactId);
                ViewData["AffiliateContactName"] = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactName);
                ViewData["AffiliateContactId"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
            }
        }

        private string GetMenusHtmlNew()
        {
            string _menuHtml = "<div id=\"menu\">";
            _menuHtml += "<ul class=\"sf-menu\">";
            List<Menu> _menus = null;
            if (String.IsNullOrEmpty(_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName)) || _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType) == "-1")
                _menus = new Menu().GetMenus(Mits.Common.Constants.AffiliateType.Public.ToString());
            else
            {
                _menus = Authorization.GetAuthorizationMenu(_sessionValues).MenuItems;
                Menu homeMenu = new Menu();
                homeMenu.Name = "Home";
                homeMenu.Caption = "Home";
                homeMenu.Show = true;
                int affiliateTypeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                if ((int)Constants.AffiliateType.Eworld == affiliateTypeid)
                {
                    if ((int)Constants.AffiliateContactRole.Finance == affiliateRoleId)
                        homeMenu.URL = "Invoice/Index";
                    else
                        homeMenu.URL = "Affiliate/Index";
                }
                else if ((int)Constants.AffiliateType.OEM == affiliateTypeid)
                {
                    homeMenu.URL = "Affiliate/Map";
                }
                else if ((int)Constants.AffiliateType.Processor == affiliateTypeid)
                {
                    homeMenu.URL = "ProcessingData/List";
                }
                else 
                {
                    homeMenu.URL = "ChangePassword/Index";
                }
                _menus.Insert(0, homeMenu);
            }

            int counter = 1;
            foreach (Menu _menu in _menus)
            {
                string cssClass = "line";
                if (counter == 1)
                    cssClass = "first line";
                else if (counter == _menus.Count)
                    cssClass = "last";
                else
                    cssClass = "line";

                if (counter == _menus.Count && (String.IsNullOrEmpty(_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateRoleName)) || _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType) == "-1"))
                    cssClass = "currentLast";

                if (_menu.OnClick != null && _menu.OnClick != "")
                    _menuHtml += "<li name='parentMenu' class=\"" + cssClass + "\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href='#' onclick=\"" + _menu.OnClick + "\">" + _menu.Caption + "</a>";

                else if (_menu.URL == null || _menu.URL == "" || _menu.URL == "#")
                    _menuHtml += "<li name='parentMenu' class=\"" + cssClass + "\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=#>" + _menu.Caption + "</a>";

                else if (_menu.Name == "LogIn")
                    _menuHtml += "<li name='parentMenu' class=\"" + cssClass + "\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=\"" + RootPath + _menu.URL + "\" target=\"_blank\">" + _menu.Caption + "</a>";
                else
                    _menuHtml += "<li name='parentMenu' class=\"" + cssClass + "\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=\"" + RootPath + _menu.URL + "\">" + _menu.Caption + "</a>";


                _menuHtml += GetChildMenusHtmlNew(_menu);
                if (_menu.MenuItems.Count == 0)
                    _menuHtml += "</div></li>";
                else
                    _menuHtml += "</div><div style='float:left;padding:11px 5px;'><img src='" + AppVirtualPath + "Content/Images/Menus/arrow.png'></div></li>";
                counter++;
            }
            return _menuHtml += "</ul></div>";
        }

        private string GetChildMenusHtmlNew(Menu _parent)
        {
            string _menuHTML = "";
            if (_parent.MenuItems.Count > 0)
                _menuHTML += "<ul>";
            foreach (Menu _menu in _parent.MenuItems)
            {
                if (_menu.OnClick != null && _menu.OnClick != "")
                    _menuHTML += "<li name='parentMenu' class=\"current\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href='#' onclick=\"" + _menu.OnClick + "\">" + _menu.Caption + "</a>";

                else if (_menu.URL == null || _menu.URL == "" || _menu.URL == "#")
                    _menuHTML += "<li class=\"current\"><div style='float:left;padding:10px 0px 0px 0px;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=#>" + _menu.Caption + "</a>";
                else
                    _menuHTML += "<li class=\"current\"><div style='float:left;padding:10px 0px 0px 0px;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=\"" + RootPath + _menu.URL + "\">" + _menu.Caption + "</a>";
                _menuHTML += GetChildMenusHtmlNew(_menu);
                if (_menu.MenuItems.Count == 0)
                    _menuHTML += "</div></li>";
                else
                    _menuHTML += "</div><div style='float:left;padding:13px 5px;'><img src='" + AppVirtualPath + "Content/Images/Menus/arrow.png'></div></li>";
            }
            if (_parent.MenuItems.Count > 0)
                _menuHTML += "</ul>";
            return _menuHTML;
        }

        public string AppVirtualPath
        {
            get
            {
                return System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.HttpContext.Current.Request.ApplicationPath;
            }
        }

        public ActionResult GetReportsRights(string reportName)
        {
            // string reportName = "";
            AccessRights _accessRights = null;
            int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (Mits.Common.Constants.AreaName.AccountsReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.AccountsReport);
            else if (Mits.Common.Constants.AreaName.CertificateOfAssuredRecyclingReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.CertificateOfAssuredRecyclingReport);
            else if (Mits.Common.Constants.AreaName.ElectronicProductsRecyclingReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.ElectronicProductsRecyclingReport);
            else if (Mits.Common.Constants.AreaName.GeneralSummaryReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.GeneralSummaryReport);
            else if (Mits.Common.Constants.AreaName.OEMTargetDetailReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMTargetDetailReport);
            else if (Mits.Common.Constants.AreaName.ProcessRecyclerReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.ProcessRecyclerReport);
            else if (Mits.Common.Constants.AreaName.TimeSpanComparisonReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.TimeSpanComparisonReport);
            else if (Mits.Common.Constants.AreaName.InvoiceReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.InvoiceReport);
            else if (Mits.Common.Constants.AreaName.ProcessingReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.ProcessingReport);
            else if (Mits.Common.Constants.AreaName.GeneralSummaryOEM.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.GeneralSummaryOEM);
            else if (Mits.Common.Constants.AreaName.AdminYearlyAccountComparisonReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.AdminYearlyAccountComparisonReport);
            else if (Mits.Common.Constants.AreaName.AdminObligationReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.AdminObligationReport);
            else if (Mits.Common.Constants.AreaName.OEMTargetReportAccountDetail_StateView.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMTargetReportAccountDetail_StateView);
            else if (Mits.Common.Constants.AreaName.OEMTargetReportAccountDetail_OEMView.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMTargetReportAccountDetail_OEMView);
            else if (Mits.Common.Constants.AreaName.OEMTargetReportAccountDetailSummary.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMTargetReportAccountDetailSummary);
            else if (Mits.Common.Constants.AreaName.OEMTargetReportProcessedStatusSummary.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMTargetReportProcessedStatusSummary);
            else if (Mits.Common.Constants.AreaName.OEMTargetReportObligationStatus_StateView.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMTargetReportObligationStatus_StateView);
            else if (Mits.Common.Constants.AreaName.OEMTargetReportObligationStatus_StateView_new.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMTargetReportObligationStatus_StateView_new);
            else if (Mits.Common.Constants.AreaName.OEMTargetReportObligationStatus_OEMView.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMTargetReportObligationStatus_OEMView);
            else if (Mits.Common.Constants.AreaName.AdminWeightProcessedReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.AdminWeightProcessedReport);
            else if (Mits.Common.Constants.AreaName.AdminUnAssignedWeightReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.AdminUnAssignedWeightReport);
            else if (Mits.Common.Constants.AreaName.AssignedWeightDetail.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.AssignedWeightDetail);
            else if (Mits.Common.Constants.AreaName.ProcessingReportDetail.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.ProcessingReportDetail);
            else if (Mits.Common.Constants.AreaName.ProcessingReportAll.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.ProcessingReportAll);
            else if (Mits.Common.Constants.AreaName.RecyclerMonthReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.RecyclerMonthReport);
            else if (Mits.Common.Constants.AreaName.OEMReconciliationDetail.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMReconciliationDetail);
            else if (Mits.Common.Constants.AreaName.OEMWeightProcessedReport.ToString() == reportName)
                _accessRights = Authorization.GetRights(affiliateRoleId, Mits.Common.Constants.AreaName.OEMWeightProcessedReport);
            return Json((_accessRights != null && _accessRights.Read.HasValue && _accessRights.Read.Value));
        }

        public ActionResult GetReportsRights2(string reportName)
        {
            // string reportName = "";
            AccessRights _accessRights = null;
            _accessRights = Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), reportName);
            return Json((_accessRights != null && _accessRights.Read.HasValue && _accessRights.Read.Value));
        }
    }
}
