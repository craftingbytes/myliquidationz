﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL.Authorization;
using System.Collections;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace Mits.Web.Controllers
{
    public class InvoiceController : Mits.Web.Controllers.MITSApplicationController
    {

        private InvoiceBO invoiceBO;
        private InvoiceItem invoiceItem = null;
        private AccessRights AccessRights
        {
            get
            {
                int affiliateTypeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                int affiliateRoleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                if (affiliateTypeid == (int)Constants.AffiliateType.OEM)
                {
                    return Authorization.GetRights(affiliateRoleId, Constants.AreaName.OEMInvoice);
                }
                else if (affiliateTypeid == (int)Constants.AffiliateType.Processor)
                {
                    return Authorization.GetRights(affiliateRoleId, Constants.AreaName.ProcessorInvoice);
                }
                else
                {
                    AccessRights rights = Authorization.GetRights(affiliateRoleId, Constants.AreaName.OEMInvoice);
                    AccessRights processorRights = Authorization.GetRights(affiliateRoleId, Constants.AreaName.ProcessorInvoice);
                    if (rights == null && processorRights != null)
                    {
                        rights = processorRights;
                    }
                    else if (rights != null && processorRights != null)
                    {
                        if (rights.Read.Value || processorRights.Read.Value)
                        {
                            rights.Read = true;
                        }
                        if (rights.Add.Value || processorRights.Add.Value)
                        {
                            rights.Add = true;
                        }
                        if (rights.Update.Value || processorRights.Update.Value)
                        {
                            rights.Update = true;
                        }
                        if (rights.Delete.Value || processorRights.Delete.Value)
                        {
                            rights.Delete = true;
                        }
                    }
                    return rights;
                }
            }
        }

        private List<InvoiceItem> InvoiceItems
        {
            get
            {
                List<InvoiceItem> obj = null;
                string json = _sessionValues.GetSessionValue(Constants.SessionParameters.InvoiceItems);
                if (json != "-1")
                {
                    obj = (List<InvoiceItem>)JsonConvert.DeserializeObject(Uri.UnescapeDataString(json));
                }
                return obj;
            }
            set
            {
                string json = JsonConvert.SerializeObject(value);
                if (!string.IsNullOrWhiteSpace(json))
                {
                    _sessionValues.SetSessionValue(Constants.SessionParameters.InvoiceItems, Uri.EscapeDataString(json));
                }
            }
        }
        
        //
        // GET: /Invoice/
        private Int32? CurrentInvoiceID
        {
            get
            {
                int id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId + Constants.SessionParameters.CurrentInvoiceID);
                return (id > 0) ? (Int32?)id : null;
            }
            set
            {
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.AffiliateContactId + Constants.SessionParameters.CurrentInvoiceID, (value.HasValue && value.Value > 0) ? value.Value : -1);
            }
        }
        private Invoice CurrentInvoice
        {
            get;
            set;
        }

         public InvoiceController(ISessionCookieValues sessionValues)
             : this(sessionValues, new InvoiceBO())
         {

         }
         
        public InvoiceController(ISessionCookieValues sessionValues, InvoiceBO invoiceBO)
            : base(sessionValues)
        {
            this.invoiceBO = invoiceBO;
        }

        public ActionResult Index()
        {
            CurrentInvoiceID = null;
            if (AccessRights != null && AccessRights.Read.HasValue == true && AccessRights.Read == true)
            {
                ViewData["States"] = invoiceBO.GetStates();
                ViewData["affiliates"] = invoiceBO.GetAffiliate(true);
                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate affiliate = new MITSService<Affiliate>().GetSingle(x => x.Id == affiliateId);
                ViewData["AffiliateName"] = affiliate.Name;
                ViewData["IsOEM"] = false;
                if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) == (int)Constants.AffiliateType.OEM)
                {
                    ViewData["IsOEM"] = true;    
                }
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
          
        }

        public ActionResult ProcessorIndex()
        {
            CurrentInvoiceID = null;
            if (AccessRights != null && AccessRights.Read.HasValue == true && AccessRights.Read == true)
            {
                ViewData["States"] = invoiceBO.GetStates();
                ViewData["affiliates"] = invoiceBO.GetAffiliate(false);
                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate affiliate = new MITSService<Affiliate>().GetSingle(x => x.Id == affiliateId);
                ViewData["AffiliateName"] = affiliate.Name;
                ViewData["IsProcessor"] = false;
                if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) == (int)Constants.AffiliateType.Processor)
                {
                    ViewData["IsProcessor"] = true;
                }
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }

        }

        public ActionResult GetEntityStates(string affiliate)
        {
            int _aff = int.Parse(affiliate);
            var _invoiceList = new MITSService<Invoice>().GetAll(x => x.AffiliateId == _aff && x.StateId != null);
            var data = new MITSService<Invoice>().GetAll(x => x.AffiliateId == _aff && x.StateId != null);
            IEnumerable<Invoice> enumList = data.OrderBy(f => f.State.Name).Distinct();
            try
            {
                _invoiceList.Clear();
                _invoiceList = enumList.ToList();
            }
            catch (Exception ex)
            {
                _invoiceList = null;
            }

            int index = 0;
            while (index < _invoiceList.Count - 1)
            {
                if (_invoiceList[index].StateId == _invoiceList[index + 1].StateId)
                {
                    _invoiceList.RemoveAt(index);
                }
                else
                    index++;
            }
            State _state = new State();
            _state.Id = 0;
            _state.Name = "Select";
            Invoice _invoice = new Invoice();
            _invoice.State = _state;
            _invoiceList.Insert(0, _invoice);

           var jsonData = new
            {
                _invoiceList = (
                    from m in _invoiceList
                    select new
                    {
                        Value = m.StateId,
                        Text = m.State.Name

                    }).ToArray()
            };
            return Json(jsonData);
        }

        [HttpGet]
        public ActionResult GetInvoices(int page, int rows, string searchEntity, string searchInvoice, string searchReceivable, string searchState,string searchDateTo, string searchDateFrom)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) == (int)Constants.AffiliateType.OEM)
            {
                searchEntity = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId);
            }
            var data = invoiceBO.GetInvoicesPaged(pageIndex, pageSize, searchEntity, searchInvoice, searchReceivable,searchState,searchDateTo,searchDateFrom, out totalRecords,1).AsEnumerable<Invoice>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        cell = new object[] { 
                           m.Id,
                             (m.Affiliate.Name==null)?"":m.Affiliate.Name,
                             String.Format("{0:MM/dd/yyyy}",m.InvoiceDate),
                            (m.State==null)?"":m.State.Name,
                             String.Format("{0:#,0.0000}",invoiceBO.GetInvoiceAmount(m.Id,m.Affiliate.Id,m.InvoiceDate.Value)),
                            m.Receivable,
                            m.Affiliate.AffiliateType.Type
                            
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetProcessorInvoices(int page, int rows, string searchEntity, string searchInvoice, string searchReceivable, string searchState, string searchDateTo, string searchDateFrom)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) == (int)Constants.AffiliateType.Processor)
            {
                searchEntity = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId);
            }
            var data = invoiceBO.GetInvoicesPaged(pageIndex, pageSize, searchEntity, searchInvoice, searchReceivable, searchState, searchDateTo, searchDateFrom, out totalRecords, 2).AsEnumerable<Invoice>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                           m.Id,
                             (m.Affiliate.Name==null)?"":m.Affiliate.Name,
                             String.Format("{0:MM/dd/yyyy}",m.InvoiceDate),
                            (m.State==null)?"":m.State.Name,
                             String.Format("{0:#,0.0000}",invoiceBO.GetInvoiceAmount(m.Id,m.Affiliate.Id,m.InvoiceDate.Value)),

                            m.Receivable,
                            m.Affiliate.AffiliateType.Type,
                            m.Approved,
                            GetDocsListHtml((m.ProcessingDatas.Where(x => x.InvoiceId == m.Id).Select(x => x.Id).FirstOrDefault())),
                            GetDialogueHtml((m.ProcessingDatas.Where(x => x.InvoiceId == m.Id).Select(x => x.Id).FirstOrDefault())),
                            m.Verified
                            //m.ProcessingDatas.Where(x => x.InvoiceId == m.Id).Select(x => x.Id).FirstOrDefault()
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(Int32? id,bool? isSaved) 
        {
            if (AccessRights != null && AccessRights.Read.HasValue == true && AccessRights.Read == true)
            {
                ViewData["AccessRights"] = AccessRights;
                if (id.HasValue)
                {
                    CurrentInvoiceID = id.Value;
                    CurrentInvoice = invoiceBO.GetInvoiceByID(id.Value);
                }
                else
                    CurrentInvoiceID = null;

                if (CurrentInvoice == null)
                {
                    return RedirectToAction("Error", "Home"); 
                }
                else
                {
                    ProcessingData pd = invoiceBO.GetProcessingDataByInvoiceId(CurrentInvoice.Id);
                    if (pd == null)
                        return RedirectToAction("Error", "Home");
                    else
                        return RedirectToAction("Edit", "ProcessingData", new { Id = pd.Id } );

                }

                //Get ProcessingData information
                //IList<State> states = invoiceBO.GetSupportedStates(SessionParameters.AffiliateId);
                //IList<State> states = invoiceBO.GetSupportedStates(CurrentInvoice.AffiliateId.HasValue ? CurrentInvoice.AffiliateId.Value : 0);
                //int selectedStateId = CurrentInvoice.StateId.HasValue ? CurrentInvoice.StateId.Value : 0 ;
                ////if (states.Count > 0)
                ////{
                ////    selectedStateId = states[0] != null ? states[0].Id : 0; 
                ////}

                //ViewData["States"] = states;

                //ViewData["VendorState"] = states;
                //ViewData["VendorCities"] = new MITSService<City>().GetAll().Where(c => c.StateId == selectedStateId).OrderBy(a => a.Name).ToList<City>();
                //ViewData["PickUpState"] = states;
                //ViewData["PickUpCities"] = new MITSService<City>().GetAll().Where(c => c.StateId == selectedStateId).OrderBy(a => a.Name).ToList<City>();
                //ViewData["CollectionMethods"] = new MITSService<CollectionMethod>().GetAll();

                //GetAllInvoiceItems(CurrentInvoice);

                //if (isSaved.HasValue && isSaved.Value)
                //    return View(new InvoiceViewModel(CurrentInvoice) { Message = "Invoice has been saved successfully." });
                //else
                //    return View(new InvoiceViewModel(CurrentInvoice) { Message = null });

            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
           

        }

        [HttpPost]
        public ActionResult Edit(Invoice invoice,ProcessingData processingData)
        {
            if (AccessRights != null && AccessRights.Read.HasValue == true && AccessRights.Read == true)
            {
                if (CurrentInvoiceID.HasValue)
                    CurrentInvoice = invoiceBO.GetInvoiceByID(CurrentInvoiceID.Value);

                if (CurrentInvoice == null)
                {
                    CurrentInvoice = new Invoice();
                    CurrentInvoice.Receivable = true;
                }
               

                if (CurrentInvoice.Id == 0)
                {
                    CurrentInvoice.AffiliateId = invoice.AffiliateId;

                    if (GetAffiliateType(CurrentInvoice.AffiliateId.Value) == Constants.AffiliateType.OEM)
                        CurrentInvoice.Receivable = true;
                    else if (GetAffiliateType(CurrentInvoice.AffiliateId.Value) == Constants.AffiliateType.Processor)
                        CurrentInvoice.Receivable = false;
                }
                CurrentInvoice.InvoiceDate = invoice.InvoiceDate;
                CurrentInvoice.Notes = invoice.Notes;
                CurrentInvoice.ShowNotes = invoice.ShowNotes;
                CurrentInvoice.InvoicePeriodFrom = invoice.InvoicePeriodFrom;
                CurrentInvoice.InvoicePeriodTo = invoice.InvoicePeriodTo;
                CurrentInvoice.InvoiceDueDate = invoice.InvoiceDueDate;
                CurrentInvoice.StateId = invoice.StateId;
                CurrentInvoice.PlanYear = invoice.PlanYear;
                

                UpdateInvoiceItems(CurrentInvoice);

                processingData.Comments = CurrentInvoice.Notes;
                UpdateProcessingData(CurrentInvoice.Id, processingData);
                bool _statusFlag = false;
                if (CurrentInvoice.Id > 0)
                    _statusFlag = invoiceBO.UpdateInvoice(CurrentInvoice);
                else
                    _statusFlag = invoiceBO.AddInvoice(CurrentInvoice);

                SaveAttachment(Request.Form["txtUploadedFileName"], CurrentInvoice.Id);

                if (_statusFlag)
                {
                    return RedirectToAction("Edit", new { id = CurrentInvoice.Id.ToString(), isSaved = true });
                }
                else
                {
                    return View("Edit", new InvoiceViewModel(_sessionValues, CurrentInvoice) { Message = "Invoice has not been saved successfully."});
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        private Constants.AffiliateType GetAffiliateType(Int32 affiliateId)
        {
            Affiliate _affiliate = new AffiliateBO().GetAffiliate(affiliateId);
            if (_affiliate != null)
            {
                if (_affiliate.AffiliateTypeId == (int)Common.Constants.AffiliateType.Eworld)
                    return Constants.AffiliateType.Eworld;
                if (_affiliate.AffiliateTypeId == (int)Common.Constants.AffiliateType.Collector)
                    return Constants.AffiliateType.Collector;
                if (_affiliate.AffiliateTypeId == (int)Common.Constants.AffiliateType.OEM)
                    return Constants.AffiliateType.OEM;
                if (_affiliate.AffiliateTypeId == (int)Common.Constants.AffiliateType.Processor)
                    return Constants.AffiliateType.Processor;
                if (_affiliate.AffiliateTypeId == (int)Common.Constants.AffiliateType.Public)
                    return Constants.AffiliateType.Public;
                if (_affiliate.AffiliateTypeId == (int)Common.Constants.AffiliateType.State)
                    return Constants.AffiliateType.State;
            }
            return Constants.AffiliateType.Public;
            
        }

        [HttpPost]
        public JsonResult EditInvoiceItem(FormCollection col)
        {
            if (col.AllKeys.Contains("oper"))
            {
                if (col["oper"] == "add")
                {
                    invoiceItem = new InvoiceItem();
                    invoiceItem.ProductTypeId = Convert.ToInt32(col["ProductType"]);
                    invoiceItem.ServiceTypeId = Convert.ToInt32(col["ServiceType"]);
                    invoiceItem.QuantityTypeId = Convert.ToInt32(col["QuantityType"]);
                    invoiceItem.ServiceDate = Convert.ToDateTime(col["Date"]);
                    invoiceItem.Quantity = Convert.ToDecimal(col["Quantity"]);
                    invoiceItem.Rate = Convert.ToDecimal(col["Rate"]);
                    invoiceItem.QuantityAdjustPercent = Convert.ToDecimal(col["QuantityAdjustPercent"]) / 100;

                    InvoiceItems.Add(invoiceItem);
                }
                else if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    invoiceItem = InvoiceItems.ToList()[Convert.ToInt32(col["id"])];
                    if (invoiceItem != null)
                    {
                        invoiceItem.ProductTypeId = Convert.ToInt32(col["ProductType"]);
                        invoiceItem.ServiceTypeId = Convert.ToInt32(col["ServiceType"]);
                        invoiceItem.QuantityTypeId = Convert.ToInt32(col["QuantityType"]);
                        invoiceItem.ServiceDate = Convert.ToDateTime(col["Date"]);
                        invoiceItem.Quantity = Convert.ToDecimal(col["Quantity"]);
                        invoiceItem.Rate = Convert.ToDecimal(col["Rate"]);
                        invoiceItem.QuantityAdjustPercent = Convert.ToDecimal(col["QuantityAdjustPercent"]) / 100;
                        
                    }
                }
                else if (col["oper"] == "del" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    invoiceItem = InvoiceItems.ToList()[Convert.ToInt32(col["id"])];
                    if (invoiceItem != null)
                    {
                        InvoiceItems.Remove(invoiceItem);
                    }
                }
            }
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetInvoiceItems(int page, int rows, string sord, string sidx)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int counter = pageIndex * pageSize;
            int totalRecords = InvoiceItems.ToList().Count;
            var data = InvoiceItems.ToList().Skip(pageIndex * pageSize).Take(pageSize).ToList();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,

                rows = (
                    from m in data
                    select new
                    {
                        id = counter++,
                        cell = new object[] { 
                           m.Id,
                            invoiceBO.GetServiceTypes().Where(pID => pID.Id == m.ServiceTypeId).FirstOrDefault() !=null ? invoiceBO.GetServiceTypes().Where(pID => pID.Id == m.ServiceTypeId).FirstOrDefault().Name: "",
                            invoiceBO.GetQuantityTypes().Where(pID => pID.Id == m.QuantityTypeId).FirstOrDefault() !=null ? invoiceBO.GetQuantityTypes().Where(pID => pID.Id == m.QuantityTypeId).FirstOrDefault().Name: "",
                            invoiceBO.GetProductTypes().Where(pID => pID.Id == m.ProductTypeId).FirstOrDefault() !=null ? invoiceBO.GetProductTypes().Where(pID => pID.Id == m.ProductTypeId).FirstOrDefault().Name: "",
                            m.ServiceDate.HasValue ? m.ServiceDate.Value.ToString("M/d/yyyy"):"",
                            Convert.ToInt32(m.Quantity.Value),
                            m.Rate,
                            m.QuantityAdjustPercent*100
                        }
                    }).ToArray()
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckRecievable(Int32 affiliateID)
        {
            bool _flag = GetAffiliateType(affiliateID) == Constants.AffiliateType.OEM;

            return Json(new { result = _flag }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetDropDownsData()
        {
            string strServiceTypes = ":Select;", strQuantityTypes = ":Select;", strProductTypes = ":Select;";
            foreach (ServiceType s in invoiceBO.GetServiceTypes())
            {
                strServiceTypes += s.Id + ":" + s.Name + ";";
            }
            foreach (QuantityType q in invoiceBO.GetQuantityTypes())
            {
                strQuantityTypes += q.Id + ":" + q.Name + ";";
            }

            foreach (ProductType p in invoiceBO.GetProductTypes())
            {
                strProductTypes += p.Id + ":" + p.Name + ";";
            }

            var jsonData = new
            {
                serviceTypes = strServiceTypes.TrimEnd(';'),
                quantityTypes = strQuantityTypes.TrimEnd(';'),
                productTypes = strProductTypes.TrimEnd(';'),
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUserAccessRights()
        {
            var jsonData = new
            {
                showEdit = AccessRights.Update.HasValue && AccessRights.Update.Value,
                showDelete = AccessRights.Delete.HasValue && AccessRights.Delete.Value
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private void GetAllInvoiceItems(Invoice invoice)
        {
            InvoiceItems = new List<InvoiceItem>();

            foreach (InvoiceItem item in invoice.InvoiceItems)
            {
                InvoiceItem _invoiceItem = new InvoiceItem();

                _invoiceItem.Id = item.Id;
                _invoiceItem.ServiceTypeId = item.ServiceTypeId;
                _invoiceItem.QuantityTypeId = item.QuantityTypeId;
                _invoiceItem.ProductTypeId = item.ProductTypeId;
                _invoiceItem.ServiceDate = item.ServiceDate;
                _invoiceItem.Quantity = item.Quantity;
                _invoiceItem.Rate = item.Rate;
                _invoiceItem.QuantityAdjustPercent = item.QuantityAdjustPercent;

                InvoiceItems.Add(_invoiceItem);
            }
        }

        private void UpdateInvoiceItems(Invoice invoice) 
        {
            foreach (InvoiceItem updatedItem in InvoiceItems)
            {
                InvoiceItem _invoiceItem = invoice.InvoiceItems.Where(it => it.Id == updatedItem.Id).FirstOrDefault();

                if (_invoiceItem == null || _invoiceItem.Id==0)
                {
                    _invoiceItem = new InvoiceItem();
                    _invoiceItem.Id = updatedItem.Id;
                    _invoiceItem.ServiceTypeId = updatedItem.ServiceTypeId;
                    _invoiceItem.QuantityTypeId = updatedItem.QuantityTypeId;
                    _invoiceItem.ProductTypeId = updatedItem.ProductTypeId;
                    _invoiceItem.ServiceDate = updatedItem.ServiceDate;
                    _invoiceItem.Quantity = updatedItem.Quantity;
                    _invoiceItem.Rate = updatedItem.Rate;
                    _invoiceItem.QuantityAdjustPercent = updatedItem.QuantityAdjustPercent;
                    invoice.InvoiceItems.Add(_invoiceItem);
                }
                else
                {
                    _invoiceItem.Id = updatedItem.Id;
                    _invoiceItem.ServiceTypeId = updatedItem.ServiceTypeId;
                    _invoiceItem.QuantityTypeId = updatedItem.QuantityTypeId;
                    _invoiceItem.ProductTypeId = updatedItem.ProductTypeId;
                    _invoiceItem.ServiceDate = updatedItem.ServiceDate;
                    _invoiceItem.Quantity = updatedItem.Quantity;
                    _invoiceItem.Rate = updatedItem.Rate;
                    _invoiceItem.QuantityAdjustPercent = updatedItem.QuantityAdjustPercent;
                }
            }

            foreach (int deletedItemID in invoice.InvoiceItems.Where(it => !InvoiceItems.Select(i => i.Id).Contains(it.Id)).Select(it=>it.Id).ToList())
            {
                invoiceBO.DeleteInvoiceItem(deletedItemID);
            }

        }

        public void UpdateProcessingData( Int32 invoiceId,ProcessingData processingData)
        {
            ProcessingData currentProcessigData;
            if (invoiceId == 0)
                //when the invoiceId is 0 it mean this is a new invoice so it hasn't processingData
                return;
            else
                currentProcessigData = invoiceBO.GetProcessingDataByInvoiceId(invoiceId);

            //This invoice hasn't processData to mapping
            if (currentProcessigData == null)
                return;

            currentProcessigData.VendorAddress = processingData.VendorAddress;
            currentProcessigData.VendorCityId = processingData.VendorCityId;
            currentProcessigData.VendorCompanyName = processingData.VendorCompanyName;
            currentProcessigData.VendorPhone = processingData.VendorPhone;
            currentProcessigData.VendorStateId = processingData.VendorStateId;
            currentProcessigData.VendorZip = processingData.VendorZip;

            currentProcessigData.PickupAddress = processingData.PickupAddress;
            currentProcessigData.PickupCityId = processingData.PickupCityId;
            currentProcessigData.PickUpCompanyName = processingData.PickUpCompanyName;
            currentProcessigData.PickupEmail = processingData.PickupEmail;
            currentProcessigData.PickupFax = processingData.PickupFax;
            currentProcessigData.PickupPhone = processingData.PickupPhone;
            currentProcessigData.PickupStateId = processingData.PickupStateId;
            currentProcessigData.PickupZip = processingData.PickupZip;
            State pickupState = invoiceBO.GetRegion(processingData.PickupStateId.HasValue?processingData.PickupStateId.Value:0);
            currentProcessigData.State = pickupState.Name;

            currentProcessigData.Client = processingData.Client;
            currentProcessigData.RecievedBy = processingData.RecievedBy;
            currentProcessigData.ShipmentNo = processingData.ShipmentNo;

            if(currentProcessigData.Id == 0)
                invoiceBO.AddProcessingData(currentProcessigData);
            else
                invoiceBO.UpdateProcessingData(currentProcessigData);
        }

        public bool SaveAttachment(string fileName, Int32 invoiceId)
        {
            if (!String.IsNullOrEmpty(fileName) && invoiceId > 0)
            {
                string path = Server.MapPath("~/Uploads/" + fileName);
                String saveLocation = Server.MapPath("~/Uploads/") + Guid.NewGuid();
                Document doc = new Document();

                doc.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                doc.ServerFileName = saveLocation;
                doc.UploadFileName = fileName;
                doc.UploadDate = System.DateTime.Now;

                int savedFileId = invoiceBO.SaveAttachment(doc);

                DocumentRelation docRel = new DocumentRelation();
                docRel.DocumentID = savedFileId;
                docRel.ForeignID = invoiceId;
                return invoiceBO.SaveAttachmentRelation(docRel);
            }
            return false;
        }

        public DownloadResult Download(string fileName)
        {
            //string fileName = Convert.ToString(Request.QueryString["FileName"]);

            if (fileName != "-")
            {
                fileName = fileName.Replace("<~>", "#");
                return new DownloadResult { VirtualPath = "~/Uploads/" + fileName, FileDownloadName = fileName };
            }
            else
            {
                return null;
            }
        }

        private ProcessingData FillProcessingData(FormCollection form)
        {
            ProcessingData data = new ProcessingData();

            State pickupRegionInfo = invoiceBO.GetRegion(Convert.ToInt32(form["ProcessingData.PickupStateId"]));

            string stateZip = string.IsNullOrEmpty(form["ddlVendorState"]) ? form["txtVendorZip"] : form["ddlVendorState"];
            //tRegion vendorRegionInfo = pdProcess.GetRegion(stateZip);
            State vendorState = invoiceBO.GetRegion(Convert.ToInt32(form["ddlVendorState"]));
            City vendorCity = invoiceBO.GetCity(Convert.ToInt32(form["ddlVendorCity"]));
            City pickupCity = invoiceBO.GetCity(Convert.ToInt32(form["ddlPickupCity"]));

            data.ProcessingDate = Convert.ToDateTime(form["txtDate"]);

            data.Comments = form["txtComment"];
            data.VendorCompanyName = form["txtCompanyName"];

            data.VendorAddress = form["txtVendorAddress"];
            data.VendorStateId = vendorState.Id;

            if (vendorCity != null)
            {
                data.VendorCityId = vendorCity.Id;
            }

            data.VendorZip = form["txtVendorZip"];
            data.VendorPhone = form["txtVendorPhone"];

            data.PickUpCompanyName = form["txtPickUpCompanyName"];
            data.PickupAddress = form["txtPickupAddress"];
            data.PickupStateId = pickupRegionInfo.Id;
            data.State = pickupRegionInfo.Name;
            if (pickupCity != null)
            {
                data.PickupCityId = pickupCity.Id;
            }
            
            data.PickupZip = form["txtPickupZip"];
            data.PickupPhone = form["txtPickupPhone"];
            data.PickupFax = form["txtPickupFax"];
            data.PickupEmail = form["txtPickupEmail"];

            data.Client = form["txtClient"];
            data.RecievedBy = form["txtRecievedBy"];
            data.ShipmentNo = form["txtShipmentNo"];
            data.CollectionMethod = null;
            if (!string.IsNullOrEmpty(form["ddlCollectionMethod"]))
            {
                data.CollectionMethodId = Convert.ToInt32(form["ddlCollectionMethod"]);
            }
            return data;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteInvoice(Int32 id)
        {
            string msg = String.Empty;
            if (AccessRights != null && AccessRights.Delete.HasValue == true && AccessRights.Delete == true)
            {
                msg = invoiceBO.DeleteOEMInvoiceByInvoiceId(id);
            }
            else
            {
                msg = "Access Denied";
            }

            return Content(msg);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateApproval(Int32 id)
        {
            string msg = String.Empty;
            if (AccessRights != null && AccessRights.Delete.HasValue == true && AccessRights.Delete == true)
            {
                msg = invoiceBO.UpdateApprovalByInvoiceId(id);
            }
            else
            {
                msg = "Access Denied";
            }

            if (msg == "Success")
            {
                Invoice inv = invoiceBO.GetInvoiceByID(id);

                return Json(new { success = true, message = msg, approved = inv.Approved, invoicedate = String.Format("{0:MM/dd/yyyy}", inv.InvoiceDate) });
            }
            else
                return Json(new { success = false, message = msg });

        }

        [HttpPost]
        public JsonResult AddAdustment(FormCollection col)
        {

            string message = "";
            bool status = true;
            int invoiceId = Convert.ToInt32(col["InvoiceId"]);
            string adjDescription = col["Description"].ToString();
            decimal adjAmount = Convert.ToDecimal(col["Amount"]);



            string msg = String.Empty;
            if (AccessRights != null && AccessRights.Delete.HasValue == true && AccessRights.Add == true)
            {
                message = invoiceBO.AddInvoiceAdjustment(invoiceId, adjDescription, adjAmount);
                if (message == "Success")
                    status = true;
            }
            else
            {
                message = "Access Denied";
                status = false;
            }



            var jsonData = new
            {
                message = message,
                status = status.ToString()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateDate(FormCollection col)
        {
            //if (AccessRightsProduct == null || AccessRightsProduct.Update.HasValue == false
            //        || AccessRightsProduct.Update.Value == false)
            //{
            //    return Json(new { success = false, message = "Access denied" });
            //}

            string msg = String.Empty;

            Invoice invoice = invoiceBO.GetInvoiceByID(Convert.ToInt32(col["id"]));
            invoice.InvoiceDate = Convert.ToDateTime(col["InvoiceDate"]);
            
            bool Success = invoiceBO.UpdateInvoice(invoice);



            if (Success)
                return Json(new { success = true, message = msg });
            else
            {
                msg = "Error changing date";
                return Json(new { success = false, message = msg });
            }
                

        }

        private string GetDocsListHtml(int pdId)
        {
            int _docs = invoiceBO.GetDocCountByPDataId(pdId);

            string _markup = string.Empty;

            if (_docs > 0)
            {
                _markup = "<div onclick='GetAttachments(" + pdId + ")'><img src='../Content/images/icon/sm/paperclip.gif' title='View attachments'></img></div>";
            }

            return _markup;
        }
        private string GetDialogueHtml(int pdId)
        {

            return "<div onclick='javascript:openFileDialog(" + pdId + ")'><img src='../Content/images/icon/sm/attach_file.gif' title='Attach File' /></div>";
        }
 
    }
}
