﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using Mits.BLL.Authorization;
using Mits.BLL;
namespace Mits.Web.Controllers
{
    public class AffiliateContractRateController : MITSApplicationController
    {
        //
        // GET: /AffiliateContractRate/
        private AffiliateContractRateBO affiliateContractRateBO;

        private AccessRights AccessRights
        {
            get
            {
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.ContractRate);
            }
        }

        public AffiliateContractRateController(ISessionCookieValues sessionValues)
            : this(sessionValues, new AffiliateContractRateBO())
        {
            affiliateContractRateBO = new AffiliateContractRateBO();
        }

        public AffiliateContractRateController(ISessionCookieValues sessionValues, AffiliateContractRateBO affiliateContractRateBO)
            : base(sessionValues)
        {
            this.affiliateContractRateBO = affiliateContractRateBO;
        }

        private Affiliate SelectedAffiliate
        {
            get
            {
                return new AffiliateBO().GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId));
            }
        }

        public ActionResult Index(int? id)
        {
            if (CanRead())
            {
                ViewData["AccessRights"] = AccessRights;
                if (id.HasValue)
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, id.Value);
                else if (_sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType) != Constants.AffiliateType.Eworld.ToString())
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));

                ViewData["SelectedAffiliate"] = SelectedAffiliate;
                ViewData["Data"] = Request["data"];

                var stateList = new MITSService<State>().GetAll().OrderBy(f => f.Name).ToList();
                stateList.Insert(0, new State() { Id = 0, Name = "Select" });
                ViewData["States"] = new SelectList(stateList, "Id", "Name");

                var serviceTypeList = new MITSService<ServiceType>().GetAll().OrderBy(x => x.Name).ToList();
                serviceTypeList.Insert(0, new ServiceType() { Id = 0, Name = "Select" });
                ViewData["ServiceTypes"] = new SelectList(serviceTypeList, "Id", "Name");

                ViewData["CanAdd"] = CanAdd() ? "" : "disabled";
                ViewData["CanUpdate"] = CanUpdate() ? "" : "disabled";
                ViewData["CanDelete"] = CanDelete() ? "" : "disabled";

                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        public ActionResult EditContractRates(FormCollection col)
        {
            string message = "";
            bool status = true;
            if (col.AllKeys.Contains("oper"))
            {
                if (col["oper"] == "new")
                {
                    if (CanAdd())
                    {
                        int stateId = Convert.ToInt32(col["stateId"]);
                        DateTime startDate = Convert.ToDateTime(col["startDate"]);
                        DateTime endDate = Convert.ToDateTime(col["endDate"]);
                        int serviceTypeId = Convert.ToInt32(col["serviceTypeId"]);
                        int planYear = Convert.ToInt32(col["planYear"]);
                        if (affiliateContractRateBO.IsExistContractRate(0, _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId), stateId, startDate, endDate, serviceTypeId))
                        {
                            message = "The Date criteria is overlapping with another contract rate.Please change criteria or date range";
                            status = false;
                        }
                        else
                        {
                            var rates = PopulateContractRates(stateId, startDate, endDate, serviceTypeId, col["rates"], planYear);
                            if (affiliateContractRateBO.AddContractRates(rates))
                            {
                                message = Constants.Messages.RecordSaved;
                                status = true;
                            }
                            else
                            {
                                message = Constants.Messages.RecordNotSaved;
                                status = false;
                            }
                        }
                    }
                    else
                    {
                        message = "AccessDenied";
                        status = false;
                    }
                }
                else if (col["oper"] == "edit")
                {
                    if (CanUpdate())
                    {
                        int stateId = Convert.ToInt32(col["stateId"]);
                        DateTime startDate = Convert.ToDateTime(col["startDate"]);
                        DateTime endDate = Convert.ToDateTime(col["endDate"]);
                        int serviceTypeId = Convert.ToInt32(col["serviceTypeId"]);
                        int planYear = Convert.ToInt32(col["planYear"]);
                        var rates = PopulateContractRates(stateId, startDate, endDate, serviceTypeId, col["rates"],planYear);
                        if (affiliateContractRateBO.IsExistContractRate(rates[0].Id, _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId), stateId, startDate, endDate, serviceTypeId))
                        {
                            message = "The Date criteria is overlapping with another contract rate.Please change criteria or date range";
                            status = false;
                        }
                        else if (affiliateContractRateBO.UpdateContractRates(rates))
                        {
                            message = Constants.Messages.RecordSaved;
                            status = true;
                        }
                        else
                        {
                            message = Constants.Messages.RecordNotSaved;
                            status = false;
                        }
                    }
                    else
                    {
                        message = "AccessDenied";
                        status = false;
                    }
                }
                else if (col["oper"] == "del")
                {
                    if (CanDelete())
                    {
                        int stateId = Convert.ToInt32(col["stateId"]);
                        DateTime startDate = Convert.ToDateTime(col["startDate"]);
                        DateTime endDate = Convert.ToDateTime(col["endDate"]);
                        int serviceTypeId = Convert.ToInt32(col["serviceTypeId"]);
                        if (affiliateContractRateBO.DeleteContractRates(_sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId), stateId, startDate, endDate, serviceTypeId))
                        {
                            message = "Record has been deleted successfully.";
                            status = true;
                        }
                        else
                        {
                            message = "Record could not be deleted.";
                            status = false;
                        }
                    }
                    else
                    {
                        message = "AccessDenied";
                        status = false;
                    }
                }
            }
            var jsonData = new
            {
                message = message,
                status = status.ToString()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetContractRates(int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = 0;
            var data = affiliateContractRateBO.GetContractRates(pageIndex, pageSize, out totalRecords, _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId));
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,

                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                           m.Id,
                           m.State.Id,
                           m.State.Name,
                           (m.RateStartDate.HasValue)? Convert.ToDateTime(m.RateStartDate.Value).ToString("MM/dd/yyyy"): string.Empty,
                           (m.RateEndDate.HasValue)?Convert.ToDateTime(m.RateEndDate.Value).ToString("MM/dd/yyyy"):string.Empty,
                           m.PlanYear,
                           m.ServiceType.Id,
                           m.ServiceType.Name,
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetContractRateDetails(int page, int rows, string oper, int? stateId, string startDate, string endDate, int? serviceTypeId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = 0;
            List<AffiliateContractRate> data = new List<AffiliateContractRate>();

            if (!string.IsNullOrEmpty(oper) && stateId.HasValue)
            {
                if (oper == "new" || oper == "edit" && !string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) && serviceTypeId.HasValue)
                {
                    if (oper == "new")
                    {
                        serviceTypeId = 0;
                    }
                    data = affiliateContractRateBO.GetContractRateDetails(pageIndex, pageSize, out totalRecords, _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId), oper, stateId.Value, startDate, endDate, serviceTypeId.Value);
                }
            }

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,

                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                            m.ProductType.Id,
                            (m.ProductType != null) ? m.ProductType.Name : string.Empty,
                            (m.QuantityType != null) ? m.QuantityType.Name : string.Empty, 
                            m.Rate.HasValue ? m.Rate.Value.ToString() : string.Empty
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDropDownsData()
        {
            string strQuantityTypes = ":Select;";

            foreach (QuantityType q in affiliateContractRateBO.GetQuantityTypes())
            {
                strQuantityTypes += q.Id + ":" + q.Name + ";";
            }

            var jsonData = new
            {
                quantityTypes = strQuantityTypes.TrimEnd(';')
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private List<AffiliateContractRate> PopulateContractRates(int stateId, DateTime startDate, DateTime endDate, int serviceTypeId, string sRates, int planYear)
        {
            var rates = sRates.Split(';');
            var rateList = new List<AffiliateContractRate>();
            foreach (var sRate in rates)
            {
                if (string.IsNullOrEmpty(sRate))
                {
                    continue;
                }
                var value = sRate.Split(':');
                AffiliateContractRate rate = new AffiliateContractRate();
                rate.AffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId);
                rate.Id = Convert.ToInt32(value[0]);
                rate.StateId = stateId;
                rate.RateStartDate = startDate;
                rate.RateEndDate = endDate;
                rate.ServiceTypeId = serviceTypeId;
                rate.ProductTypeId = Convert.ToInt32(value[1]);
                rate.QuantityTypeId = Convert.ToInt32(value[2]);
                rate.Rate = Convert.ToDecimal(value[3]);
                rate.PlanYear = planYear;
                rateList.Add(rate);
            }
            return rateList;
        }

        private bool CanRead()
        {
            return (AccessRights != null && AccessRights.Read.HasValue && AccessRights.Read.Value);
        }

        private bool CanAdd()
        {
            return (AccessRights != null && AccessRights.Add.HasValue && AccessRights.Add.Value);
        }

        private bool CanUpdate()
        {
            return (AccessRights != null && AccessRights.Update.HasValue && AccessRights.Update.Value);
        }

        private bool CanDelete()
        {
            return (AccessRights != null && AccessRights.Delete.HasValue && AccessRights.Delete.Value);
        }

        public ActionResult GetPlanYear(int planYear, int stateId) {


                PlanYear py = new MITSService<PlanYear>().GetAll(x => x.StateId == stateId && x.Year == planYear).FirstOrDefault() ;


                var jsonData = new
                {
                    startDate = (py != null) ? Convert.ToDateTime(py.StartDate.Value).ToString("MM/dd/yyyy") : String.Empty,
                    endDate = (py != null) ? Convert.ToDateTime(py.EndDate.Value).ToString("MM/dd/yyyy") : String.Empty
                };

            return Json(jsonData, JsonRequestBehavior.AllowGet); 
        
        }
    }
}
