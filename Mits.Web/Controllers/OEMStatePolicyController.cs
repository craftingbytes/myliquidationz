﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mits.BLL;
using Mits.BLL.Interfaces;
using Mits.BLL.ViewModels;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
using System.Configuration;
using Mits.BLL.Authorization;

namespace Mits.Web.Controllers
{
    public class OEMStatePolicyController : MITSApplicationController
    {
        private OEMStatePolicyBO _oemStatePolicyBO;


        private AccessRights AccessRightsOEMStatePolicy
        {
            get
            {
                //Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), Constants.AreaName.OEMStatePolicy);
            }
        }
       
        public OEMStatePolicyController(ISessionCookieValues sessionValue)
            : this(sessionValue, new OEMStatePolicyBO())
        {
        }
      
        public OEMStatePolicyController(ISessionCookieValues sessionValue, OEMStatePolicyBO oemStatePolicyBO)
            : base(sessionValue)
        {
            _oemStatePolicyBO = oemStatePolicyBO;                   
        }

        //Getting a list of all states

        private IList<State> GetAll()
        {
            IList<State> _stateList;
            IList<State> unSortedList1 = new MITSService<State>().GetAll();
            IEnumerable<State> sortedEnum1 = unSortedList1.OrderBy(f => f.Name);
            try
            {
                _stateList = sortedEnum1.ToList();
            }
            catch (Exception ex)
            {
                _stateList = unSortedList1;
            }
            State selectState = new State();
            selectState.Id = 0;
            selectState.Name = "Select";
            _stateList.Insert(0, selectState);
            return _stateList;
        }


        ////Getting a list of all OEMs

        private IList<Affiliate> GetAllOEM()
        {
             IList<Affiliate> _affiliateList;
             string _oem = Common.Constants.AffiliateType.OEM.ToString();
             AffiliateType _affiliateTypeObj = new MITSService<AffiliateType>().GetSingle(t => t.Type == _oem);
            IList<Affiliate> unSortedList = new MITSService<Affiliate>().GetAll(t => t.AffiliateTypeId == _affiliateTypeObj.Id);
            IEnumerable<Affiliate> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _affiliateList = sortedEnum.ToList();
            }
            catch
            {
                _affiliateList = unSortedList;
            }
            Affiliate selectOEM = new Affiliate();
            selectOEM.Id = 0;
            selectOEM.Name = "Select";
            _affiliateList.Insert(0, selectOEM);
            return _affiliateList;
        }
        
        //Getting a list of all Policies

        private IList<Policy> GetAllPolicy()
        {
             IList<Policy> policy ;
            IList<Policy> unSortedList = new MITSService<Policy>().GetAll();
            IEnumerable<Policy> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                policy = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                policy = unSortedList;
            }
            return policy;
        }
        
        
        
        //
        // GET: /OEMStatePolicy/

        public ActionResult Index()
        {
            if (AccessRightsOEMStatePolicy != null && AccessRightsOEMStatePolicy.Read.HasValue == true && AccessRightsOEMStatePolicy.Read == true)
            {
                //if (SessionParameters.AffiliateType != string.Empty && SessionParameters.AffiliateType == Constants.AffiliateType.Admin.ToString())
                //{
                    return View(new OEMStatePolicyViewModel());
                //}
                //else
                //{
                //    return RedirectToAction("AccessDenied", "Home");

                //}
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");

            }
        }
        
        //Get all active policies against a particulr OEM and State and send response back to view

        public ActionResult GetOEMPolicies(string state, string oem)
        {
            int _stateId = int.Parse(state);
            int _oemId = int.Parse(oem);
            var data = new MITSService<ManufacturerPolicy>().GetAll(x => x.StateId == _stateId & x.Active == true & x.AffiliateId == _oemId);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.PolicyId,


                    }).ToArray()
            };
            return Json(jsonData);
        }


        //Invokes 'Add' function of OEMStatePolicyBO and return response back to view

        public ActionResult Save(string oem, string state, string policies)
        {
            if (AccessRightsOEMStatePolicy != null && AccessRightsOEMStatePolicy.Add.HasValue == true && AccessRightsOEMStatePolicy.Add == true || (AccessRightsOEMStatePolicy.Update.HasValue == true && AccessRightsOEMStatePolicy.Update == true))
            {

                Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();



                try
                {
                    if (int.Parse(oem) == 0)
                    {
                        return Json(new
                        {
                            success = false,
                            message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                        });
                    }
                    else if (int.Parse(state) == 0)
                    {
                        return Json(new
                        {
                            success = false,
                            message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                        });
                    }
                    else
                    {
                        _returnDictionaryValue = _oemStatePolicyBO.Add(oem, state, policies);
                    }


                }
                catch (Exception ex)
                {
                    string log = ex.Message;
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordNotSaved"],
                    });
                }
                if (_returnDictionaryValue["duplicate"] == 0)
                {
                    var data = this.GetAllOEM();
                    var jsonData = new
                    {
                        success = true,
                        message = ConfigurationManager.AppSettings["RecordSaved"],
                        selected = _returnDictionaryValue["selectedId"],
                        //selectedState = returnValue["StateId"],
                        data = (
                            from m in data
                            select new
                            {
                                Value = m.Id,
                                Text = m.Name,

                            }).ToArray()
                    };
                    return Json(jsonData);
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = ConfigurationManager.AppSettings["RecordAlreadyExists"],
                    });

                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    AccessDenied = "AccessDenied",

                });
            }
        }
       

       
    }
}
