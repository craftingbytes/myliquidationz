﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Mits.BLL.BusinessObjects;
using Mits.Common;

namespace Mits.Web.ReminderWebService
{
    /// <summary>
    /// Summary description for ReminderWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ReminderWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public void SendReminder()
        {
            ReminderEmailBO reminderEmailBO = new ReminderEmailBO();
            reminderEmailBO.SendReminderEmail();
        }
    }
}
