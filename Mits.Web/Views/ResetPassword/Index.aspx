﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.ResetPasswordViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 <link rel="Stylesheet" type="text/css" href="../../Content/css/default.css" />
   <%-- <%using (Html.BeginForm())
      { %>--%>
    <h3 style="color: Gray; padding-top:20px;padding-left:20px" >
            Reset Password</h3>
    <table style="margin-top:20px; margin-left:265px">
        
    <td>
     
        <fieldset style="height: 310px; width: 400px;">
            <legend style="padding: 5px;"><b>Reset Password</b></legend>
        <table>
        <tr>
            <td colspan="2" style=" padding-top: 10px;">
                <div id="validationSummary" style="width: 300px; margin-left: 85px;" 
                    class="ValidationSummary">
                    <% if (Model.Messages.Count > 0)
                       {%><ul class="ValidationSummary">
                           <%foreach (var message in Model.Messages)
                             {%>
                           <li>
                               <%=message.Text %></li>
                           <%} %>
                       </ul>
                    <%} %>
                </div>
            </td>
        </tr>
            <tr height="40px">
            <td align="right" style=" padding-left:30px ;padding-top:10px">
                <span style='color:red'>*</span>Email :
            </td>
            <td style="padding-top:10px">
                <input type="text" id="txtEmail" name="txtEmail" style="width: 200px;" required="Email is required." maxlength="50" regexp="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                regexpmesg="Invalid Email format, Please try hello@mydomain.com"/>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
       <tr height="40px">
            <td align="right">
                Security Question :
            </td>
            <td >
             <%= Html.DropDownList("ddlQuestion", Model.SecurityQuestion, new { required = "Security Question is required.", style = "width:205px" })%>
            </td>
        </tr>
        <tr height="40px"  >
        
            <td align="right" >
           
               <span style='color:red'>*</span> Answer :
            </td>
         
            <td >
             
                 <input type="text" id="txtAnswer" name="txtAnswer"  style="width: 200px;" required="Answer is required." maxlength="50"/>
            </td>
            <td>
            </td>
        </tr>

        <tr height="40px">
            <td align="right">
            </td>
            <td>
               
                <br />
                  <br />
                <input  id="btnPassword" name="btnPassword" onclick="return SubmitForm('/ResetPassword/Index');" type="submit" value="Reset Password"  class="ui-button ui-widget ui-state-default ui-corner-all"/>
               
            </td>
            <td>
            </td>
        </tr>
         </table>
       </fieldset>
       
        </td>
       
    
    </table>
   <%--  <% } %>--%>
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    
</asp:Content>


