﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.DAL.EntityModels.RecyclerProductWeight>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 
 <div class="admin">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <h1 class="heading">Mass Balance Data Entry</h1>
    <div style="text-align:right; float:right; margin-top:-20px;"><b><%=(ViewData["Affiliate"] as Mits.DAL.EntityModels.Affiliate).Id %>&nbsp;&nbsp;<%=(ViewData["Affiliate"] as Mits.DAL.EntityModels.Affiliate).Name %></b></div>
    <table style="padding:0;">
        <tr>
            <td colspan="2" style="padding-left:174px;">
            <div id="validationSummary" style="width:500px;" class="ValidationSummary">
            <% if (ViewData["Messages"] != null)
               {
                   var Messages = ViewData["Messages"] as System.Collections.Generic.List<Mits.Common.Message>;
                   %><ul class="ValidationSummary">
                   <%foreach (var message in Messages)
                   {%> 
                   <li><%=message.Text %></li>
                   <%} %>
                   </ul>
            <%} %>
            </div>
            <label id="hidMess" name="hidMess" style="visibility:hidden"><%=ViewData["saved"] %></label>                
            </td>
        </tr>
        <tr>
            <td style="padding-left:120px;padding-bottom:0;"  valign="top">
                <table style="margin-top:6px;">
                    <tr style="height:35px;">
                        <td align="right">
                            <span style='color:red'>*</span>Month :
                        </td>
                        <td>
                            <input type="text" id="month" name="month" disabled="disabled" value='<%= ViewData["Month"] %>' style="width: 200px;"  maxlength="10" required="Month is required." regexp = "^(0[1-9]|1[012])[- /.](19|20)\d\d$" regexpmesg = "Invalid Month format(MM/yyyy)."/>                            
                            <input type="hidden" id="Year" name="Year" value='<%= Model.Year %>'/>
                            <input type="hidden" id="Month" name="Month" value='<%= Model.Month %>'/>
                            <input type="hidden" id="Id" name="Id" value='<%= Model.Id %>'/>
                        </td>                        
                    </tr>
                    <tr style="height:25px;">
                        <td align="right">
                            <b>Inbound Shipment&nbsp;&nbsp;</b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td align="right">
                            <span style='color:red'>*</span>Total Weight Received :
                        </td>
                        <td>
                            <input type="text" id="TotalWeightReceived" name="TotalWeightReceived" value='<%= Convert.ToInt32(Model.TotalWeightReceived) %>' style="width: 200px;text-align:right;"  maxlength="8" required="Total Weight Received  is required." regexp="^[0-9]+[0-9]*$" regexpmesg="Invalid Total Weight Received, please try a positive number"/>
                        </td>
                        <td>
                            Pound
                        </td>
                    </tr>
                    <tr style="height:10px;">
                        <td align="right">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height:25px;">
                        <td align="right">
                            <b>Inventory&nbsp;&nbsp;</b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td align="right">
                            <span style='color:red'>*</span>On Hand :
                        </td>
                        <td>
                            <input type="text" id="OnHand" name="OnHand" value='<%= Convert.ToInt32(Model.OnHand) %>' style="width: 200px;text-align:right;"  maxlength="8" required="On Hand is required." regexp="^[0-9]+[0-9]*$" regexpmesg="Invalid On Hand, please try a positive number"/>
                        </td>
                        <td>
                            Pound
                        </td>
                    </tr>
                    <tr style="height:10px;">
                        <td align="right">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height:25px;">
                        <td align="right">
                            <b>Outbound Shipment&nbsp;&nbsp;</b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td align="right">
                            <span style='color:red'>*</span>CRT Glass :
                        </td>
                        <td>
                            <input type="text" id="CRTGlass" name="CRTGlass" value='<%= Convert.ToInt32(Model.CRTGlass) %>' style="width: 200px;text-align:right;"  maxlength="8" required="CRT Glass is required." regexp="^[0-9]+[0-9]*$" regexpmesg="Invalid CRT Glass, please try a positive number"/>
                        </td>
                        <td>
                            Pound
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td align="right">
                            <span style='color:red'>*</span>Circuit Boards :
                        </td>
                        <td>
                            <input type="text" id="CircuitBoards" name="CircuitBoards" value='<%= Convert.ToInt32(Model.CircuitBoards) %>' style="width: 200px;text-align:right;"  maxlength="8" required="Circuit Boards is required." regexp="^[0-9]+[0-9]*$" regexpmesg="Invalid Circuit Boards, please try a positive number"/>
                        </td>
                        <td>
                            Pound
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td align="right">
                            <span style='color:red'>*</span>Batteries/Mercury Devices :
                        </td>
                        <td>
                            <input type="text" id="Batteries" name="Batteries" value='<%= Convert.ToInt32(Model.Batteries) %>' style="width: 200px;text-align:right;"  maxlength="8" required="Batteries/Mercury Devices is required." regexp="^[0-9]+[0-9]*$" regexpmesg="Invalid Batteries/Mercury Devices, please try a positive number"/>
                        </td>
                        <td>
                            Pound
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td align="right">
                            <span style='color:red'>*</span>Other Materials :
                        </td>
                        <td>
                            <input type="text" id="OtherMaterials" name="OtherMaterials" value='<%= Convert.ToInt32(Model.OtherMaterials) %>' style="width: 200px;text-align:right;"  maxlength="8" required="Other Materials is required." regexp="^[0-9]+[0-9]*$" regexpmesg="Invalid Other Materials, please try a positive number"/>
                        </td>
                        <td>
                            Pound
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr align="left">
            <td align="left" style="padding-left:205px;padding-top:20px;padding-bottom:0;">
                     
                 <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.Value)
                   { %>                             
                <input id="btnSubmit" name="btnSubmit" type="button" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all"/> 
                <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <%}
                   else
                   {%>
                   <input id="btnSubmit" name="btnSubmit" type="button" value="Submit" lang="smart" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/> 
                   <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/> 
                <%} %>
            </td>
        </tr>
   </table>
   <div id="saved" title="" style="display: none; padding-top:0;padding-bottom:0;">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
            font-size: 14px; color: #464646;">Record has been saved successfully.</span>
    </div>
    <input type="hidden" id="id" name="id" value="<%=ViewData["Id"] %>" />
</div>
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/recyclerproductweight.edit.js")%>' type="text/javascript"></script>
</asp:Content>

