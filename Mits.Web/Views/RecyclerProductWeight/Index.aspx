﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceholder" runat="server">
<div class="admin">
    <h1 class="heading">Search Weight Entry</h1>
    <table cellspacing="0" width="70%" style="margin-top: 5px">
        <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
                <fieldset style="height: 170px; width: 400px;">
                    <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                    <table style="width:400px; margin-top:10px">   
                        <tr height="40">
                            <td align="right">Month From :</td>
                            <td align="left"> 
                                <input type="text" id="txtFrom" size="30" style="width: 200px;"  />
                            </td>
                        </tr>
                        <tr height="40">
                            <td align="right">Month To :</td>
                            <td align="left"> 
                                <input type="text" id="txtTo" size="30" style="width: 200px;"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td  style="padding-top:20px" align="left">
                                <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                        </tr>
                    </table>
                </fieldset>            
            </td>
        </tr>
        <tr>
            <td style="padding-left:20px;padding-top:6px;">
            <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
            { %>    
            <input type="button" id="btnAdd" name="btnAdd" value="Enter Weight" class="ui-button ui-widget ui-state-default ui-corner-all"/>
            <%}
            else
            { %>
                <input type="button" id="btnAdd" name="btnAdd" value="Enter Weight" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/>
            <%} %>
            </td>
        </tr>
        <tr>
            <td style="padding-left:20px;padding-top:3px;">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table>
</div>

    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/recyclerproductweight.index.js")%>' type="text/javascript"></script>

</asp:Content>

