﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="homecontent">
        <div style="padding:20px;" >
            <a href="/CollectionEvent/Map" class="backlink"><img src="../../Content/images/back.png" border="0" /></a>
        </div>
        <div>
            <!--Place Section1 Div here -->
            <div class="main">
                <h4 style="font-size:15pt; text-align:center; margin:0; padding-bottom:30px;">Collection Events in <%= ViewData["StateName"] %></h4>
                <input type="hidden" id="hStateId" value='<%= ViewData["StateId"] %>' />
                <div style="padding-left:30px;padding-right:15px">
                    <table id="jqGrid" style="width: 80%" cellpadding="0" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="../../Scripts/forms/collectionevent.detail.js" type="text/javascript"></script>
</asp:Content>


