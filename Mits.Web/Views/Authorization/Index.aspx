﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<SelectList>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceholder" runat="server">
<div class="admin">
    <h1 class="heading"> Authorization</h1>

    <table cellspacing="0" cellpadding="5" width="100%">
        <tr>
            <td>
                <b>Select Role to View/Edit :</b><br />
                <%=Html.DropDownList("ddlRole", Model, new { style = "width:205px;" })%>
            </td>
        </tr>
        <tr>
            <td style = "padding-top: 15px">
                <span style='color:red'>*</span>Role Name :<br />
                <input type="text" id="txtName" name="txtName" maxlength="100" required="Role Name is required." style="width:200px"/>
            </td>
        </tr>
        <tr>
            <td >
                <table id="jqGrid">
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" id="txtPermissions" name = "txtPermissions" />
                <input type="button" id="btnCreate" name="btnCreate" value="Create" class="fr ui-button ui-widget ui-state-default ui-corner-all" <%= ViewData["CanAdd"] %>/>
                <input type="button" id="btnUpdate" name="btnUpdate" value="Save" class="fr ui-button ui-widget ui-state-default ui-corner-all" <%= ViewData["CanUpdate"] %>/>
            </td>
        </tr>
    </table>
</div>
    <%--<script type="text/javascript" src='<%=ResolveClientUrl("~/Scripts/js/grid.treegrid.js")%>'></script>--%>
    <%--<script src="../../Scripts/js/jquery.fillSelect.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../../Scripts/forms/Authorization.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        input[type=text], textarea, select
        {
            /*background-color: Transparent;*/
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 142px; 
        }
        select
        {
            width: 145px; 
        }
        
        td
        {
           /* padding-left: 10px;*/
        }
        td.right
        {
            border-right: solid 1px black;
           /* padding-right: 30px;*/
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(../../images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../images/calander.gif);
        }
        .minus
        {
        }
    </style>
</asp:Content>
