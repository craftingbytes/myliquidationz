﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
  <!--  <script type="text/javascript" src="../../Scripts/Map/FusionMaps.js"></script>
    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>-->
    <div class="rs">
    <h2 class="title">State Guidelines</h2>
        <div id="Div1" align="center">
            <div>
                <p style='vertical-align: middle; width: 856px; padding-top:15px;'>
                    <span style='font-style: italic; font-size: 11.0pt; font-family: "Calisto MT","serif"'>
                        E-World Online’s staff of industry experts has spent a significant amount of time
                        reviewing legislation, so you do not have to. Just click on the state you want to
                        find more information about below and a snap shot of the major legislative highlights
                        will appear.
                        <o:p></o:p>
                    </span>
                </p>
            </div>
            <div style="clear: both;">
                &nbsp;</div>
        </div>
        <div id="mapdiv" align="center">                                                     
            <iframe src="/Reports/GenericReportNoToolbar.aspx?ReportName=MapStateGuidelines&DisplayName=MapStateGuideLines"  width="100%" height="600px">
                    <p>Your browser does not support iframes.</p>
            </iframe>
        </div>
    </div>
    <br />
    <br />
 <!--   <script type="text/javascript">

        var currentTime = new Date();
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var hour = currentTime.getHours();
        var minute = currentTime.getMinutes();
        var second = currentTime.getSeconds();
        var time = month + "" + day + "" + year + "" + hour + "" + minute + "" + second;


        var map = new FusionMaps("../../Content/Flashfile/Map/FCMap_USA.swf", "Map1Id", "750", "460", "0", "0");

        var url = "../../Helpers/CustomerIndex.xml?" + time;
        map.setDataURL(url);
        map.render("mapdiv");
    </script>-->
    <!-- end #page -->
</asp:Content>
