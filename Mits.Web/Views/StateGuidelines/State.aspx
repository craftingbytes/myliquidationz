﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.StateGuidelinesViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .main{ font-family:Calisto MT,serif;color:Gray;  padding:20px 80px 40px 80px;}
        .main h4{ font-size:15pt; text-align:center; margin:0; padding:20px;}
        .main h5{ font-size:14pt; text-align:center; font-weight:normal; text-decoration:underline;}
        .main h6{ font-size:12pt; text-align:left; text-decoration:underline; margin-bottom:2pt; margin-top:24pt;}
        .main p{ margin:0px; padding:0; font-size:12pt !important;font-size:12pt; line-height:1.6em;}
        .main p img{ text-align:center; vertical-align:middle; margin-left:5px;}
        .main a{ text-decoration:underline;}
        .backlink{ display:block; padding:20px 40px;}
        .datatable{ width:470px;}
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="homecontent">
        <div style="POSITION: absolute" id="divBack">
            <a href="/StateGuidelines" class=="backlink"><img src="../../Content/images/back.png" border="0" /></a>
        </div>
        <div>
            <!--Place Section1 Div here -->
            <div class="main">
                <h4 >State Legislation Snapshot</h4>
                <h5><%= Model.State.Name %></h5>

                <%
                    if (Model.StateGuideline.UpdateDate != null)
                    {
                 %>
                <p style="text-align:center; margin-top:-20pt;"><span style="font-size:8pt;">Last Update: <%= ((DateTime)Model.StateGuideline.UpdateDate).ToString("MM/dd/yyyy hh:mm tt")%></span></p>
                <%
                    }
                 %>

                <h6>Governing Body:</h6>
                <p><span ><%= Model.StateGuideline.GoverningBody %></span></p>

                <h6 >Key Contact:</h6>
                <% foreach (var contact in Model.Contacts)
                   { %>
                <p>
                    <span>Name:</span><span ><%= contact.Name %></span>
                </p>
                <p>
                    <span>Phone:</span><span ><%= contact.Phone %></span>
                </p>
                <p>
                    <span>E-Mail:</span>
                    <span ><a href="mailto:<%= contact.EMail %>"><%= contact.EMail %></a></span>
                </p>
                <% } %>

                <h6 >Website: </h6>
                <p>
                    <span ><a href="<%= Model.StateGuideline.Website %>"><%= Model.StateGuideline.Website %></a></span>
                </p>
                <h6>Covered Devices: </h6>
                <p >
                    <span ><%= Model.StateGuideline.CoveredDevices %></span>
                </p>

                <h6 >OEM Obligation Summary:</h6>
                <p >
                    <span ><%= Model.StateGuideline.OEMObligation %></span>
                </p>

                <h6>Key Deadline Dates:</h6>
                <table  border="0" cellspacing="0" cellpadding="0" class="datatable" >
                    <% foreach (var deadLine in Model.DeadineDates) { %>
                    <tr >
                        <td width="320" valign="bottom" >
                            <p><span ><%= deadLine.Name %></span></p>
                        </td>
                        <td width="120" valign="bottom" >
                            <p align="right" style='text-align:right'><span><%= deadLine.Date %></span></p>
                        </td>
                    </tr>
                    <% } %>
                </table>

                <h6>Updates:</h6>
                <p><span ><%= Model.StateGuideline.Updates %></span></p>

                <h6>Regulation Type:</h6>
                <p><span ><%= Model.StateGuideline.RegulationType %></span></p>

                <h6>Regulation Change:</h6>
                <p><span ><%= Model.StateGuideline.RegulationChange %></span></p>

                <h6>Links</h6>
                <% foreach (var link in Model.Links) { %>
                <p><span ><a href="<%= link.Link %>" target="_blank"><%= link.Name %></a></span></p>
                <% } %>
                
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var menuYloc = $("#divBack").offset().top;
        $(window).scroll(function () {
            var offsetTop = 0;
            if ($(window).scrollTop() + 10 < menuYloc) {
                offsetTop = menuYloc + "px";
            } else {
                offsetTop = 10 + $(window).scrollTop() + "px";
            }

            $("#divBack").animate({ top: offsetTop }, { duration: 1, queue: false });
        });
    </script>
</asp:Content>


