﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <div class="main" style=" margin-left:20px; margin-right:26px;">
    <h3 style="color: Gray; padding-top:20px;">Add/Edit State Guideline</h3>

    <div style="width:100%">
        <table style="width: 100%">
            <tr style="width: 100%">
                <td align="center" style="padding-top:10px;padding-bottom:10px;">
                    <ul id="message" name="ulMessage" style="color:red; visibility:hidden">
                        <li>
                            <label id="lblMessage" style="visibility:visible; color:Red"></label>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
    <div style="width:100%; padding-bottom:10px;">
        <table style="width: 100%">
            <tr style="width: 100%">
                <td>
                    <b>Select State to Edit : </b><br /><%= Html.DropDownList("ddlState", ViewData["States"] as SelectList, new { style = "width:205px" })%>
                </td>
            </tr>
        </table>
    </div>
    <div style="float: left; width: 47%; ">
        <table style="width: 100%">
            <tr>
                <td align="left" valign="bottom" style="padding-top:10px">
                    Governing Body :
                </td>
            </tr>
            <tr>
                <td>
                    <textarea id="txtGoverningBody" rows="4" name="txtGoverningBody" style="height: 80px; width: 420px;" onkeypress="return imposeMaxLength(this, 2000);"></textarea>
                </td>
            </tr>
            <tr>
                <td align="left" valign="bottom" style="padding-top:10px">
                    Covered Devices :
                </td>
            </tr>
            <tr>
                <td>
                    <textarea id="txtCoveredDevices" rows="4" name="txtCoveredDevices" style="height: 80px; width: 420px;" onkeypress="return imposeMaxLength(this, 2000);"></textarea>
                </td>
            </tr>
            <tr>
                <td align="left" valign="bottom" style="padding-top:10px">
                    Updates :
                </td>
            </tr>
            <tr>
                <td>
                    <textarea id="txtUpdates" rows="4" name="txtUpdates" style="height: 80px; width: 420px;" onkeypress="return imposeMaxLength(this, 2000);"></textarea>
                </td>
            </tr>
            <tr>
                <td align="left" valign="bottom" style="padding-top:10px">
                    Regulation Change :
                </td>
            </tr>
            <tr>
                <td>
                    <textarea id="txtRegulationChange" rows="4" name="txtRegulationChange" style="height: 80px; width: 420px;" onkeypress="return imposeMaxLength(this, 2000);"></textarea>
                </td>
            </tr>
        </table>
    </div>
    <div style="float: left; width: 47%;margin-left:20px;">
        <table style="width: 100%;">
            <tr>
                <td align="left" valign="bottom" style="padding-top:10px">
                    Website :
                </td>
            </tr>
            <tr>
                <td>
                    <textarea id="txtWebsite" rows="4" name="txtWebsite" style="height: 80px; width: 420px;" onkeypress="return imposeMaxLength(this, 500);"></textarea>
                </td>
            </tr>
            <tr>
                <td align="left" valign="bottom" style="padding-top:10px">
                    OEM Obligation Summary :
                </td>
            </tr>
            <tr>
                <td>
                    <textarea id="txtOEMObligation" rows="4" name="txtOEMObligation" style="height: 80px; width: 420px;" onkeypress="return imposeMaxLength(this, 2000);"></textarea>
                </td>
            </tr>
            <tr>
                <td align="left" valign="bottom" style="padding-top:10px">
                    Regulation Type :
                </td>
            </tr>
            <tr>
                <td>
                    <textarea id="txtRegulationType" rows="4" name="txtRegulationType" style="height: 80px; width: 420px;" onkeypress="return imposeMaxLength(this, 2000);"></textarea>
                </td>
            </tr>
            <tr>
                <td align="left" valign="bottom" style="padding-top:10px">
                    Law State  :
                </td>
            </tr>
            <tr>
                <td>
                   <%= Html.DropDownList("ddlLawState", ViewData["LawStateItems"] as SelectList, new { style = "width:200px" })%>
                </td>
            </tr>
        </table>
    </div>
    <div style="clear:both; margin-top:30px; text-align:right;"><br /><br />
        <input type="button" id="btnAddContact" value="Add Contact" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <table id="contactGrid">
        </table>
    </div>
    <div style="float: left; width: 48%;  text-align:right;"><br /><br />
        <input type="button" id="btnAddDeadlineDate" value="Add Deadline Date" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <table id="deadlineDateGrid">
        </table>
    </div>
    <div style="float: left; width: 4%;"><br /></div>
    <div style="float: left; width: 48%; text-align:right;"><br /><br />
        <input type="button" id="btnAddLink" value="Add Link" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <table id="linkGrid">
        </table>
    </div>
    <div class="btns" style=" text-align:center; padding:10px; padding-top:20px; clear:both;"><input type="button" id="btnSubmit" name="btnSubmit" type="submit" value="Submit" class="fr ui-button ui-widget ui-state-default ui-corner-all"/>
                    <input type="button" id="btnClear" name="btnClear" value="Clear" class="fr ui-button ui-widget ui-state-default ui-corner-all"/></div>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/stateguideline.edit.js")%>' type="text/javascript"></script></div>
</asp:Content>
