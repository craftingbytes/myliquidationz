﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 
 <div class="admin">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <h1 class="heading">Manage News Feed</h1>
    <table style="padding:0;">
        <tr>
            <td colspan="2" style="padding-left:200px;">
            <div id="validationSummary" style="width:500px;" class="ValidationSummary">
            </div>            
            </td>
        </tr>
        <tr>
            <td style="padding-left:120px;padding-bottom:0;"  valign="top">
                <table style="margin-top:6px;">
                    
                    <tr style="height:35px;">
                        <td align="right">
                            <b>Select News Feed :</b>
                        </td>
                        <td>
                            <%= Html.DropDownList("ddlNewsFeed", ViewData["NewsFeed"] as SelectList, new { style = "width:205px" })%>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td align="right">
                            Date :
                        </td>
                        <td>
                            <input type="text" id="txtDate" name="txtDate" style="width: 199px;" maxlength="10" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td align="right">
                            Feed Link :
                        </td>
                        <td>
                            <input type="text" id="txtFeedLink" name="txtFeedLink" style="width: 350px;" maxlength="200" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height:135px;">
                        <td align="right" valign="top">
                            News :
                        </td>
                        <td>
                            <textarea id="txtNews" rows="4" name="txtNews" style="height: 120px; width: 350px;"  onkeydown='limitChars("txtNews", 200)' onchange='limitChars("txtNews", 200)' onpropertychange='limitChars("txtNews", 200)'></textarea>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr align="left">
            <td align="left" style="padding-left:234px;padding-top:20px;padding-bottom:0;">
                <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                   { %>                             
                <input id="btnSubmit" name="btnSubmit" type="button" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all"/> 
                <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <%}
                   else
                   {%>
                   <input id="btnSubmit" name="btnSubmit" type="button" value="Submit" lang="smart" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/> 
                   <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/> 
                <%} %>
            </td>
        </tr>
   </table>
</div>
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/newsfeed.index.js")%>' type="text/javascript"></script>
</asp:Content>

