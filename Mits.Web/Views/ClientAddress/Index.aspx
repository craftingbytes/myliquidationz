﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
    <%--<script src="../../Scripts/js/jquery.fillSelect.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../../Scripts/forms/clientaddress.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <table cellspacing="0" width="100%" cellpadding="5">
        <tr>
            <td style="padding-left: 20px; padding-top: 20px;">
                <h3 style="color: Gray"><%= ViewData["AffiliateName"] + " "%>Client Management</h3>
            </td>
        </tr>
        <%if (ViewData["ViewAll"].ToString() == "True")
          { %>
        <tr>
            <td align="center">
                <fieldset style="height: 130px; width: 400px;">
                    <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                    <table style="width:400px; margin-top:10px">
                        <tr height="40">
                            <td align="right">Processor :</td>
                            <td align="left"> 
                                <%= Html.DropDownList("ddlProcessor", (SelectList)ViewData["Processors"], new { style = "width:200px" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td  style="padding-top:10px" align="left">
                                <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                        </tr>
                    </table>
                </fieldset>            
            </td>
        </tr>
        <%} %>
        <tr>
            <td style="padding-left: 20px;padding-top:20px;">
                <input type="button" id="btnAdd" value="Add Client" class="ui-button ui-widget ui-state-default ui-corner-all" <%= ViewData["CanAdd"] %> />
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px;padding-top:0px;padding-right: 20px;">
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table>
    <div id="dlgPickUp" title="Client Pick Up/Ship From">
        <table cellspacing="0" width="100%" cellpadding="5">
            <tr>
                <td style="padding-left: 5px;padding-top:20px;">
                    <input type="button" id="btnAddPickUp" value="Add Pick Up/Ship From" class="ui-button ui-widget ui-state-default ui-corner-all" <%= ViewData["CanAdd"] %> />
                </td>
            </tr>
            <tr>
                <td style="padding-left: 5px;padding-top:0px;padding-right: 5px;">
                    <table id="jqGridPickUp">
                    </table>
                    <div id="pagerPickUp">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="ViewAll" name="ViewAll" value='<%= ViewData["ViewAll"] %>' />
    <input type="hidden" id="AffiliateId" name="AffiliateId" value='<%= ViewData["AffiliateId"] %>' />
</asp:Content>
