﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
<%--<script src="../../Scripts/js/jquery.fillSelect.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../../Scripts/forms/AffiliateTarget.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input[type=text], textarea, select
        {
            /*background-color: Transparent;*/
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 142px; 
        }
        select
        {
            width: 145px; 
        }
        
        td
        {
           /* padding-left: 10px;*/
        }
        td.right
        {
            border-right: solid 1px black;
           /* padding-right: 30px;*/
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(../../images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../images/calander.gif);
        }
        .minus
        {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   <table cellspacing="0" width="100%" cellpadding="5">
        <tr>
            <td style="padding-left: 20px;padding-top:20px;">
                <h3 style="color: Gray">
                    <% = (ViewData["SelectedAffiliate"] != null)? (ViewData["SelectedAffiliate"] as Mits.DAL.EntityModels.Affiliate).Name:"" %> Targets</h3>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px;padding-top:20px;">
            <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
              { %>
                <input type="button" id="btnAdd" value="Add Target" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%--<input type="button" id="btnAdd2" value="Add new (Inline)" class="ui-button ui-widget ui-state-default ui-corner-all" />--%>
                <%}
              else
              { %>
              <input type="button" id="btnAdd" value="Add Target" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled" />
                <%} %>
            </td>
        </tr>
        
        <tr>
            <td style="padding-left: 20px;padding-top:0px;">
                <div id="dialogProducts" title="Affilate Products">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
        <tr height="20px">
            <td style="padding-left: 20px;padding-top:0px;">
                <br />
                <input type="hidden" id="data" name="data" value='<%= ViewData["Data"] %>' />
                <% if (ViewData["Data"] != null && ViewData["Data"].ToString() != string.Empty)
                   { %>
                <input type="button" id="btnCancel" name="btnCancel" value="Return" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="Cancel();"/>
                <% } %>
            </td>
        </tr>
    </table>
</asp:Content>
