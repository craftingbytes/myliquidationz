﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceholder" runat="server">
<div class="admin">
     <h1 class="heading"> 
         <%= (bool)ViewData["IsOEM"] ? ViewData["AffiliateName"] : "Search OEM" %> Invoice
     </h1>
       <table cellspacing="0" width="70%" style="margin-top: 5px">
        <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td style="padding-left: 250px; padding-top:10px">
                  <fieldset style="height: 240px; width: 60%;">
                    <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                    <table cellspacing="0" cellpadding="5" style="margin-top:10px; margin-left:50px">
                      <tr id="trAffiliate">
                            <td align="right">
                                Entity :
                            </td>
                            <td >
                                 <%= Html.DropDownList("ddlAffiliates", new SelectList((IList<Mits.DAL.EntityModels.Affiliate>)ViewData["affiliates"], "Id", "Name")                                                          
                                    , new { style = "width:170px;" }) %>                                   
                            </td>     
                        </tr>
                        <tr  >
                            <td align="right">
                               Invoice Id :
                            </td>
                            <td >
                                <input type="text" id="txtInvoice" size="30" style="width: 165px;" />                                
                            </td>     
                        </tr>
                         <tr>
                            <td align="right">
                               State :
                            </td>
                            <td >
                               <%= Html.DropDownList("ddlStates", new SelectList((IList<Mits.DAL.EntityModels.State>)ViewData["States"], "Id", "Name")
                                                                  , new { style = "width:170px" })%>                                                              
                            </td>     
                        </tr>
                       <tr>
                            <td align="right">
                               Invoice Date From :
                            </td>
                            <td>
                              <input type="text" id="txtFrom" name="txtFrom" 
                              style="width: 165px;" maxlength="200" />                                                                             
                            </td>     
                        </tr>
                        <tr>
                            <td align="right">
                               Invoice Date To :
                            </td>
                            <td >
                               <input type="text" id="txtTo" name="txtTo" 
                                style="width: 165px;" maxlength="200" />                                                                      
                            </td>     
                        </tr>
                        
                    
                         <%-- <tr>
                            <td align="right">
                               Receivables :
                            </td>
                            <td>
                             <input type="checkbox" name="receivables" id="receivables"/>                                                                
                            </td>     
                        </tr>--%>
                        
                         <tr>
                            
                            <td>
                             
                            </td> 
                            <td style="padding-top:10px">
                            <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>    
                        </tr>
                         </table>
                         </fieldset>                  
                         

                    
            </td>
        </tr>
      
        <tr>
        
        <td style="padding-top:6px;">
          <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
            { %>    
            <input type="button" id="btnAdd" name="btnAdd" value="Create Invoice" class="ui-button ui-widget ui-state-default ui-corner-all"/>
            <%}
            else
            { %>
               <input type="button" id="btnAdd" name="btnAdd" value="Create Invoice" class="ui-button ui-widget ui-state-default ui-corner-all"/>
            <%} %>
        </td>
        </tr>
        <tr>
            <td style="padding-top:3px;">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table></div>
    <div id="dlgAdjustment" title="Add Invoice Adjustment" style="z-index:500">
        <table cellspacing="0" cellpadding="5" width="100%">
            <tr>
                <td style="padding-left: 20px;">
                    <div style="float:left; width:51%;">
                        <table cellspacing="0" cellpadding="5">
                            <tr>
                                <td align="Center" nowrap>
                                    &nbsp;
                                </td>
                                <td align="Center" nowrap>
                                    Description
                                </td>
                                <td align="Center" nowrap>
                                    Amount
                                </td>
                            </tr>
                            <tr>
                                <td align="right" nowrap>
                                    <font color="red">*</font>Invoice Adjustment :
                                </td>
                                <td>
                                    <input type="text" id="txtAdjustmentDesc" name="txtAdjustmentDesc" size="100" style="width: 300px;"  />
                                    <input type="hidden" id="currentInvoice" name="currentInvoice" value = "0"/>
                                </td>
                                <td>
                                    <input type="text" id="txtAdjustmentAmount" name="txtAdjustmentAmount" size="100" style="width: 100px;"  />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px; padding-top: 20px;" align="center">
                    <table style="padding: 0px; border:0">
                        <tr>
                            <td align="right">
                                <input type="button" id="dlgAdjBtnCancel" name="dlgAdjBtnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                            <td align="right">
                                <input type="button" id="dlgAdjBtnSave" name="dlgBtnSave" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="IsOEM" name="IsOEM" value='<%= ViewData["IsOEM"] %>' />
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
   <script src='<%=ResolveClientUrl("~/Scripts/forms/invoice.index.js")%>' type="text/javascript"></script>

</asp:Content>

