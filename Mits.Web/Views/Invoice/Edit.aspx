﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.InvoiceViewModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <%--<script src="../../Scripts/js/jquery.fillSelect.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../../Scripts/forms/Invoice.Edit.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceholder" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <table cellspacing="0" cellpadding="5" width="100%">
        <tr>
            <td style="padding-left: 20px; padding-top: 20px;">
                <h3 style="color: Gray">
                    <%if (Model.Invoice.Id > 0)
                      {%>
                    Edit
                    <%}
                      else
                      { %>
                    Create
                    <%} %>
                    Invoice
                </h3>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 130px;" colspan="2">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
                <div id="divDocuments" title="Download Attachments">
                    <%if (Model.Documents.Count > 0)
                      { %>
                    <ul>
                        <%foreach (Mits.DAL.EntityModels.Document doc in Model.Documents)
                          { %>
                        <li style="height: 20px;"><a href="javascript:Download('<%=doc.UploadFileName%>');"
                            style="color: #FF9900; text-decoration: none; _text-decoration: none;">
                            <img src='../../Content/images/d4.PNG' alt='Open selected file' title='Open selected file'
                                id='btnDel' border="0" />
                            : <font style="font-weight: bold; vertical-align: top;">
                                <%=doc.UploadFileName%></font> </a></li>
                        <%} %>
                    </ul>
                    <%}
                      else
                      { %>
                    No attachment found.
                    <%} %>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px;">
            <!-- Invoice Information -->
            <div style="float:left; width:47%;">
                <table cellspacing="0" cellpadding="5">
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Entity :
                        </td>
                        <td>
                            <%if (Model.Invoice.Id > 0)
                              { %>
                            <%=Html.DropDownList("Invoice.AffiliateId", Model.Affiliates, "Select", new { required = "Entity is required.", disabled = "disabled", style = "width:205px;", onchange = "CheckRecievable(this.value)" })%>
                            <%}
                              else
                              { %>
                            <%=Html.DropDownList("Invoice.AffiliateId", Model.Affiliates, "Select", new { required = "Entity is required.", style = "width:205px;", onchange = "CheckRecievable(this.value)" })%>
                            <%} %>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> State :
                        </td>
                        <td>
                            <%=Html.DropDownList("Invoice.StateId", Model.States, "Select", new { required = "State is required.", style = "width:205px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Invoice Date :
                        </td>
                        <td>
                            <%=Html.TextBox("Invoice.InvoiceDate", Model.Invoice.InvoiceDate.HasValue ? Model.Invoice.InvoiceDate.Value.ToString("MM/dd/yyyy") : "", new { required = "Invoice date is required.", regexp = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$", regexpmesg = "Invalid Invoice Date format.", style = "width:200px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Plan Year :
                        </td>
                        <td>
                            <%=Html.DropDownList("Invoice.PlanYear", Model.PlayYears, "Select", new { required = "Plan Year is required.", style = "width:205px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Invoice period from :
                        </td>
                        <td>
                            <%=Html.TextBox("Invoice.InvoicePeriodFrom", Model.Invoice.InvoicePeriodFrom.HasValue ? Model.Invoice.InvoicePeriodFrom.Value.ToString("MM/dd/yyyy") : "", new { required = "Invoice Period From is required.", regexp = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$", regexpmesg = "Invalid Invoice Period From Date format.", style = "width:200px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Invoice period to :
                        </td>
                        <td>
                            <%=Html.TextBox("Invoice.InvoicePeriodTo", Model.Invoice.InvoicePeriodTo.HasValue ? Model.Invoice.InvoicePeriodTo.Value.ToString("MM/dd/yyyy") : "", new { required = "Invoice Period To is required.", regexp = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$", regexpmesg = "Invalid Invoice Period To Date format.", comparewith = "Invoice_InvoicePeriodFrom", compareoperator = "greater", comparetype = "date", comparemessage = "Invoice Period To must be greater than Invoice Period From", style = "width:200px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Due Date :
                        </td>
                        <td>
                            <%=Html.TextBox("Invoice.InvoiceDueDate", Model.Invoice.InvoiceDueDate.HasValue ? Model.Invoice.InvoiceDueDate.Value.ToString("MM/dd/yyyy") : "", new { required = "Due Date is required.", regexp = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$", regexpmesg = "Invalid Due Date format.", style = "width:200px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Notes :
                        </td>
                        <td>
                            <%=Html.TextArea("Invoice.Notes", null, 5, 25, new { style = "width:200px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Show Notes :
                        </td>
                        <td>
                            <%=Html.CheckBox("Invoice.ShowNotes")%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Receivable :
                        </td>
                        <td>
                            <%=Html.CheckBox("Invoice.Receivable", new { disabled = "disabled"})%>
                        </td>
                    </tr>                    
                </table>
                </div>
            <!-- Invoice Information - End -->

            <%if(Model.ProcessingData != null) {%>
            <!-- ProcessingData clinet and pick up Information -->
            <div style="float:left; width:47%; margin:-15px 0 0 -30px; _margin:-15px 0 0 -30px;">
                </div>
            <!-- ProcessingData clinet and pick up Information - End -->

            <!-- ProcessingData other Information -->
                <table cellspacing="0" cellpadding="5" width="47%">
                    <tr>
                        <td class="style1" >
                            <!-- Client section -->
                            <fieldset style="float: left; height: 100%; width: 100%; margin-left: 15px; _margin-left: 5px;">
                                <legend style="padding: 5px;"><b>Client Address</b></legend>
                                <table cellpadding="3" style="width: 100%">
                                    <tr>
                                        <td align="right">
                                            <label for="ProcessingData.VendorCompanyName">
                                                <font color='red'>*</font>Company/Person Name :
                                            </label>
                                        </td>
                                        <td colspan="3">
                                        <%=Html.TextBox("ProcessingData.VendorCompanyName", Model.ProcessingData.VendorCompanyName, new { required = "Company Name is required.", regexpmesg = "Company Name is required.", style = "width:100px;" })%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <label for="txtAddress">
                                                <font color='red'>*</font>Address :
                                            </label>
                                        </td>
                                        <td>
                                        <%=Html.TextBox("ProcessingData.VendorAddress", Model.ProcessingData.VendorAddress, new { required = "Vendor Address is required.", regexpmesg = "Vendor Address is required.", style = "width:100px;" })%>
                                        </td>
                                        <td align="right">
                                            <label for="txtState">
                                                <font color='red'>*</font>State :
                                            </label>
                                        </td>
                                        <td>
                                        <%=Html.DropDownList("ProcessingData.VendorStateId", new SelectList((IList<Mits.DAL.EntityModels.State>)ViewData["States"],"Id","Name",Model.ProcessingData.VendorStateId), "Select", new { required = "State is required.", onchange = "GetCities(this);", style = "width:105px;" })%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <label for="txtCity">
                                                <font color='red'>*</font>City :
                                            </label>
                                        </td>

                                        <td>             
                                            <%=Html.DropDownList("ProcessingData.VendorCityId", new SelectList((IList<Mits.DAL.EntityModels.City>)ViewData["VendorCities"], "Id", "Name", Model.ProcessingData.VendorCityId), new { style = "width:104px", required = "Vendor City is required." })%>
                                            
                                        </td>
                                        <td align="right">
                                            <label for="txtZip">
                                                Zip:
                                            </label>
                                        </td>
                                        <td>
                                            <%=Html.TextBox("ProcessingData.VendorZip", Model.ProcessingData.VendorZip, new { maxlength = "5", regexp = "^(?!0{5})(\\d{5})(?!-?0{4})(-?\\d{4})?$", regexpmesg = "Invalid Zip format, Please try xxxxx.", style = "width:100px" })%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            <label for="txtPhone">
                                                Phone:
                                            </label>
                                        </td>
                                        <td class="style19">
                                            <%=Html.TextBox("ProcessingData.VendorPhone", Model.ProcessingData.VendorPhone, new { regexp = "^\\d{3}-\\d{7}$", regexpmesg = "Invalid Phone format, Please try xxx-xxxyyyy.", style = "width: 100px" })%>
                                            <br />
                                            <label style="font-size: smaller; font-family: Verdana">
                                                Format: xxx-xxxyyyy</label>
                                        </td>
                                    </tr>
                                </table>
                                <div class="cb">
                                </div>
                            </fieldset>
                            <!-- Client section - end -->
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <!-- Pick up section -->
                            <fieldset style="height: 100%; width: 100%; float: left; margin-left: 15px">
                                <legend style="padding: 5px;"><b>Pick Up/Ship From</b></legend>
                                <table cellpadding="3" align="right">
                                    <tr>
                                        <td align="right">
                                            <label for="txtPickUpCompanyName">
                                                <font color='red'>*</font>Company/Person Name :
                                            </label>
                                        </td>
                                        <td colspan="3">
                                        <%=Html.TextBox("ProcessingData.PickUpCompanyName", Model.ProcessingData.PickUpCompanyName, new { required = "Pick Up Company Name is required.", style = "width:100px;" })%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <label for="txtAddress">
                                                <font color='red'>*</font>Address :
                                            </label>
                                        </td>
                                        <td>
                                        <%=Html.TextBox("ProcessingData.PickupAddress", Model.ProcessingData.PickupAddress, new { required = "Pickup Address is required.", regexpmesg = "Pickup Address is required.", style = "width:100px;" })%>
                                        </td>
                                        <td align="right">
                                            <label for="txtState">
                                                <font color='red'>*</font>State :
                                            </label>
                                        </td>
                                        <td>
                                           <%=Html.DropDownList("ProcessingData.PickupStateId", new SelectList((IList<Mits.DAL.EntityModels.State>)ViewData["States"], "Id", "Name", Model.ProcessingData.PickupStateId), "Select", new { required = "State is required.", onchange = "LoadDeviceTypes(this);", style = "width:105px;" })%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <label for="txtCity">
                                                <font color='red'>*</font>City :
                                            </label>
                                        </td>
                                        <td>
                                            <%=Html.DropDownList("ProcessingData.PickupCityId", new SelectList((IList<Mits.DAL.EntityModels.City>)ViewData["PickUpCities"], "Id", "Name", Model.ProcessingData.PickupCityId), new { style = "width:104px", required = "Pickup City is required." })%>
                                        </td>
                                        <td align="right">
                                            <label for="txtZip">
                                                <font color='red'>*</font>Zip :
                                            </label>
                                        </td>
                                        <td>
                                            <%=Html.TextBox("ProcessingData.PickupZip", Model.ProcessingData.PickupZip.Trim(), new { required = "Pickup Zip is required.", maxlength = "5", regexp = "^(?!0{5})(\\d{5})(?!-?0{4})(-?\\d{4})?$", regexpmesg = "Invalid Zip format, Please try xxxxx or xxxxx-xxxx.", style = "width: 100px" })%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            <label for="txtPhone">
                                                <font color='red'>*</font>Phone:
                                            </label>
                                        </td>
                                        <td valign="top">
                                            <%=Html.TextBox("ProcessingData.PickupPhone", Model.ProcessingData.PickupPhone, new { required = "Phone No is required.", regexp = "^\\d{3}-\\d{7}$", regexpmesg = "Invalid Phone format, Please try xxx-xxxyyyy..", style = "width: 100px" })%>
                                            <br />
                                            <label style="font-size: smaller; font-family: Verdana">
                                                xxx-xxxyyyy</label>
                                        </td>
                                        <td align="right" valign="top">
                                            <label for="txtFax">
                                                Fax:
                                            </label>
                                        </td>
                                        <td valign="top">
                                            <%=Html.TextBox("ProcessingData.PickupFax", Model.ProcessingData.PickupFax, new { regexp = "^^(\\+\\d)*\\s*(\\(\\d{3}\\)\\s*)*\\d{3}(-{0,1}|\\s{0,1})\\d{2}(-{0,1}|\\s{0,1})\\d{2}$", regexpmesg = "Invalid Fax format, Please try xxxxx.", style = "width: 100px" })%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <label for="txtFax">
                                                Email:
                                            </label>
                                        </td>
                                        <td>
                                            <%=Html.TextBox("ProcessingData.PickupEmail", Model.ProcessingData.PickupEmail, new { style = "width: 100px" })%>
                                        </td>
                                        <td align="right">
                                            <label for="ddlCollectionMethod">
                                                <font color='red'>*</font>Collection Method :</label>
                                        </td>
                                        <td>
                                            <%=Html.DropDownList("ProcessingData.CollectionMethodId", new SelectList((IList<Mits.DAL.EntityModels.CollectionMethod>)ViewData["CollectionMethods"], "Id", "Method", Model.ProcessingData.CollectionMethodId), new { style = "width:104px", required = "Collection Method is required." })%>
                                        </td>
                                    </tr>
                                </table>
                                <div class="cb">
                                </div>
                            </fieldset>
                            <!-- Pick up section - end -->
                        </td>
                    </tr>
                </table>
             <table cellpadding="5" style="width: 100%">
                <tr>
                    <td align="right">
                        Client:
                    </td>
                    <td>
                        <%=Html.TextBox("ProcessingData.Client", Model.ProcessingData.Client, new { style = "width:100px;" })%>
                    </td>
                    <td align="right">
                        <font color='red'>*</font>Received By :
                    </td>
                    <td>
                        <%=Html.TextBox("ProcessingData.RecievedBy", Model.ProcessingData.RecievedBy, new { required="Received By is required.", style="width:100px;" })%>
                    </td>
                    <td>
                    </td>
                    <td align="right">
                         <font color='red'>*</font>Shipment # :
                    </td>
                    <td>
                        <%=Html.TextBox("ProcessingData.ShipmentNo", Model.ProcessingData.ShipmentNo, new { required = "Shipment No is required.", style = "width:100px;" })%>
                    </td>
                </tr>
            </table>
            <!-- ProcessingData other Information - End -->
            <%}%>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px; padding-top: 20px;" colspan="4">
                <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                  { %>
                <input type="button" id="btnAdd" value="Add Invoice Item" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <input type="button" id="btnAttachment" value="Attach File" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%}
                  else
                  {%>
                <input type="button" id="btnAdd" value="Add Invoice Item" class="ui-button ui-widget ui-state-default ui-corner-all"
                    disabled="disabled" />
                <input type="button" id="btnAttachment" value="Attach File" class="ui-button ui-widget ui-state-default ui-corner-all"
                    disabled="disabled" />
                <%} %>
                <input type="button" id="btnViewAttachments" value="View Attachments" class="ui-button ui-widget ui-state-default ui-corner-all"
                    onclick="ShowDocumentsDiv();" />
                <input type="hidden" id="txtUploadedFileName" name="txtUploadedFileName" />
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px; padding-top: 0px;" colspan="4">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-right: 0px; padding-top: 20px;" colspan="4" align="right">
                <%if (Model.Invoice.Id > 0 && Model.Invoice.Affiliate.AffiliateTypeId.GetValueOrDefault() == Mits.Common.Constants.AffiliateType.Processor.GetHashCode())
                  { %>
                <input type="button" id="btnViewReport" value="View Report" class="ui-button ui-widget ui-state-default ui-corner-all"
                    onclick="javascript:showReport('../../reports/RecyclerInvoice.aspx?InvoiceID=<%=Model.Invoice.Id %>', 'InvoiceReport');" />
                <%}
                  else if (Model.Invoice.Id > 0 && Model.Invoice.Affiliate.AffiliateTypeId.GetValueOrDefault() == Mits.Common.Constants.AffiliateType.OEM.GetHashCode())
                  {%>
                <input type="button" id="btnViewReport" value="View Report" class="ui-button ui-widget ui-state-default ui-corner-all"
                    onclick="javascript:showReport('../../reports/OEMInvoice.aspx?InvoiceID=<%=Model.Invoice.Id %>', 'InvoiceReport');" />
                <%} %>
                <%    if (Model.Invoice.Id == 0 && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                      { %>
                <input type="button" id="btnSubmit" onclick=" return submitForm();" value="Submit"
                    class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%}
                      else if (Model.Invoice.Id > 0 && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.Value)
                      { %>
                <input type="button" id="btnSubmit" onclick=" return submitForm();" value="Submit"
                    class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%}
                      else
                      {%>
                <input type="button" id="btnSubmit" onclick=" return submitForm();" value="Submit"
                    class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled" />
                <%}
                %>
                <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                  { %>
                <input type="button" id="btnCreate" value="Create Invoice" class="ui-button ui-widget ui-state-default ui-corner-all"
                    onclick="javascript:window.location.href='/Invoice/Edit'" />
                <%}
                  else
                  { %>
                <input type="button" id="btnCreate" value="Create Invoice" class="ui-button ui-widget ui-state-default ui-corner-all"
                    onclick="javascript:window.location.href='/Invoice/Edit'" disabled="disabled" />
                <%} %>
            </td>
        </tr>
    </table>
    <div id="divMessage">
    </div>

    <%if (!String.IsNullOrEmpty(Model.Message))
      {%>
    <script>
        ShowMessage('<%=Model.Message %>');
    </script>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input[type=text], textarea, select
        {
            /*background-color: Transparent;*/ /*border: #bbb 1px solid;*/
            margin: 3px 0 0 0;
            width: 200px;
            _width: 200;
        }
        select
        {
            width: 205px;
        }
        
        td
        {
            /* padding-left: 10px;*/
        }
        td.right
        {
            border-right: solid 1px black; /* padding-right: 30px;*/
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(../../images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../images/calander.gif);
        }
        .minus
        {
        }
        .style1
        {
            width: 495px;
        }
    </style>
</asp:Content>
