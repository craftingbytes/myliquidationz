﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceholder" runat="server">
<div class="admin">
     <h1 class="heading">
         <%= (bool)ViewData["IsProcessor"] ? ViewData["AffiliateName"] : "Search Processor"%> Invoice
     </h1>
        <table cellspacing="0" width="70%" style="margin-top: 5px">
        <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td style="padding-left: 250px; padding-top:10px">
                  <fieldset style="height: 240px; width: 60%;">
                    <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                    <table cellspacing="0" cellpadding="5" style="margin-top:10px; margin-left:50px">
                      <tr id="trAffiliate">
                            <td align="right">
                                Entity :
                            </td>
                            <td >
                                 <%= Html.DropDownList("ddlAffiliates", new SelectList((IList<Mits.DAL.EntityModels.Affiliate>)ViewData["affiliates"], "Id", "Name")                                                          
                                 , new { style = "width:170px" })%>                                   
                            </td>     
                        </tr>
                        <tr  >
                            <td align="right">
                               Invoice Id :
                            </td>
                            <td >
                                <input type="text" id="txtInvoice" size="30" style="width: 165px;" />                                
                            </td>     
                        </tr>
                         <tr>
                            <td align="right">
                               State :
                            </td>
                            <td >
                               <%= Html.DropDownList("ddlStates", new SelectList((IList<Mits.DAL.EntityModels.State>)ViewData["States"], "Id", "Name")
                                                                  , new { style = "width:170px" })%>                                                              
                            </td>     
                        </tr>
                       <tr>
                            <td align="right">
                               Invoice Date From :
                            </td>
                            <td>
                              <input type="text" id="txtFrom" name="txtFrom" 
                              style="width: 165px;" maxlength="200" />                                                                             
                            </td>     
                        </tr>
                        <tr>
                            <td align="right">
                               Invoice Date To :
                            </td>
                            <td >
                               <input type="text" id="txtTo" name="txtTo" 
                                style="width: 165px;" maxlength="200" />                                                                      
                            </td>     
                        </tr>
                        
                    
                         <%-- <tr>
                            <td align="right">
                               Receivables :
                            </td>
                            <td>
                             <input type="checkbox" name="receivables" id="receivables"/>                                                                
                            </td>     
                        </tr>--%>
                        
                         <tr>
                            
                            <td>
                             
                            </td> 
                            <td style="padding-top:10px">
                            <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>    
                        </tr>
                         </table>
                         </fieldset>                  
            </td>
        </tr>
      
        <tr>
        
        <td style="padding-top:6px;">
            &nbsp;
        </td>
        </tr>
        <tr>
            <td style="padding-top:3px;">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
        </table>
</div>
    <div id="dlgAdjustment" title="Add Invoice Adjustment" style="z-index:500">
        <table cellspacing="0" cellpadding="5" width="100%">
            <tr>
                <td style="padding-left: 20px;">
                    <div style="float:left; width:51%;">
                        <table cellspacing="0" cellpadding="5">
                            <tr>
                                <td align="Center" nowrap>
                                    &nbsp;
                                </td>
                                <td align="Center" nowrap>
                                    Description
                                </td>
                                <td align="Center" nowrap>
                                    Amount
                                </td>
                            </tr>
                            <tr>
                                <td align="right" nowrap>
                                    <font color="red">*</font>Invoice Adjustment :
                                </td>
                                <td>
                                    <input type="text" id="txtAdjustmentDesc" name="txtAdjustmentDesc" size="100" style="width: 300px;"  />
                                    <input type="hidden" id="currentInvoice" name="currentInvoice" value = "0"/>
                                </td>
                                <td>
                                    <input type="text" id="txtAdjustmentAmount" name="txtAdjustmentAmount" size="100" style="width: 100px;"  />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px; padding-top: 20px;" align="center">
                    <table style="padding: 0px; border:0">
                        <tr>
                            <td align="right">
                                <input type="button" id="dlgAdjBtnCancel" name="dlgAdjBtnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                            <td align="right">
                                <input type="button" id="dlgAdjBtnSave" name="dlgBtnSave" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="dialogFiles" title="Attached files" style="display: none;">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
            font-size: 14px; color: #464646;">
            <ul id="ulFiles">
            </ul>
        </span>
    </div>
    <!--
    <div id="dlgUploadFile"   style="margin-left:10px; margin-top:10px;">
        <div>
            <input type="hidden" id="currentPDId" name="currentPDId" />
            <input id="fileInput" name="fileInput" type="file" />
            <br />
            <input type="button" id="btnUpload" name="btnUpload" value="Upload" />
            <input type="button" id="btnClear" name="btnClear" value="Clear" />
        </div>
    </div>
    -->
    
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/invoice.processorindex.js")%>' type="text/javascript"></script>
    <!--
    <input type="hidden" id="IsProcessor" name="IsProcessor" value='<%= ViewData["IsProcessor"] %>' />
    <script src="<%=ResolveClientUrl("~/Scripts/js/swfobject.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveClientUrl("~/Scripts/js/jquery.uploadify.v2.1.0.min.js") %>" type="text/javascript"></script>
    -->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <!--
    <link rel="Stylesheet" type="text/css" href='<%=ResolveClientUrl("~/Content/css/uploadify.css")%>' />
    <link rel="Stylesheet" type="text/css" href='<%=ResolveClientUrl("~/Content/css/default.css")%>' />
    -->
        <style type="text/css">
        input[type=text], textarea, select
        {
            /*background-color: Transparent;*/
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 142px; 
        }
        select
        {
            width: 145px; 
        }
        
        td
        {
           /* padding-left: 10px;*/
        }
        td.right
        {
            border-right: solid 1px black;
           /* padding-right: 30px;*/
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(../../images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../images/calander.gif);
        }
        .minus
        {
        }
    </style>

</asp:Content>