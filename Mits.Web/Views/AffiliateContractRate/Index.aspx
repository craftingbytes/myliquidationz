﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
    <script type="text/javascript" src="../../Scripts/forms/AffiliateContractRate.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        input[type=text], textarea, select
        {
            /*background-color: Transparent;*/
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 142px; 
        }
        select
        {
            width: 145px; 
        }
        
        td
        {
           /* padding-left: 10px;*/
        }
        td.right
        {
            border-right: solid 1px black;
           /* padding-right: 30px;*/
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(../../images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../images/calander.gif);
        }
        .minus
        {
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table cellspacing="0" cellpadding="5" width="100%">
        <tr>
            <td style="padding-left: 20px;padding-top:20px;">
                <h3 style="color: Gray">
                <% = (ViewData["SelectedAffiliate"] != null)? (ViewData["SelectedAffiliate"] as Mits.DAL.EntityModels.Affiliate).Name:"" %> Contract Rates</h3>
                    
            </td>
        </tr>
        <%if (ViewData["CanAdd"] == "")
        { %>
        <tr>
            <td style="padding-left: 20px;padding-top:20px;">
                <input type="button" id="btnAdd" value="Add Contract Rate" class="ui-button ui-widget ui-state-default ui-corner-all" />
            </td>
        </tr>
        <%} %>
        <tr>
            <td style="padding-left: 20px;padding-top:0px;">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
        <tr height="20px">
            <td style="padding-left: 20px;padding-top:0px;">
                <br />
                <input type="hidden" id="data" name="data" value='<%= ViewData["Data"] %>' />
                <% if (ViewData["Data"] != null && ViewData["Data"].ToString() != string.Empty)
                   { %>
                <input type="button" id="btnCancel" name="btnCancel" value="Return" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="Cancel();"/>
                <% } %>
            </td>
        </tr>
    </table>
    <div id="dlgDetail" title="Contract Rates" style="z-index:500">
        <table cellspacing="0" cellpadding="5" width="100%">
            <tr>
                <td style="padding-left: 20px;">
                    <div style="float:left; width:47%;">
                        <table cellspacing="0" cellpadding="5">
                            <tr>
                                <td align="right" nowrap>
                                    <font color="red">*</font> State :
                                </td>
                                <td>
                                    <%= Html.DropDownList("ddlState", ViewData["States"] as SelectList, new { style = "width:205px", onchange = "ChangeState()" })%>
                                </td>
                                <td align="right" nowrap>
                                    <font color="red">*</font> Service Type :
                                </td>
                                <td>
                                    <%= Html.DropDownList("ddlServiceType", ViewData["ServiceTypes"] as SelectList, new { style = "width:205px" })%>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" nowrap>
                                    <font color="red">*</font> Plan Year :
                                </td>
                                <td>
                                    <select id="ddlPlanYear" style="width:173px;">
                                      <option>Select</option>
                                       <option>2015</option>
                                       <option>2014</option>
                                       <option>2013</option>
                                       <option>2012</option>
                                       <option>2011</option>
                                       <option>2010</option>
                                       <option>2009</option>
                                       <option>2008</option>       
                                    </select>
                                </td>
                                <td align="right" nowrap>
                                    <font color="red">*</font> Start Date :
                                </td>
                                <td>
                                    <input type="text" id="txtStartDate" name="txtStartDate" size="10" style="width: 200px;" tabIndex="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="right" nowrap>
                                    <font color="red">*</font> End Date :
                                </td>
                                <td>
                                    <input type="text" id="txtEndDate" name="txtEndDate" size="10" style="width: 200px;" tabIndex="2" />
                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px; padding-top: 20px;">
                    <div id="detailSearch">
                    </div>
                    <table id="detailGrid">
                    </table>
                    <div id="detailPager">
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-right: 20px; padding-top: 20px;" align="right">
                    <table style="padding: 0px; border:0">
                        <tr>
                            <td align="right">
                                <input type="button" id="dlgBtnCancel" name="dlgBtnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                            <td align="right">
                                <input type="button" id="dlgBtnCreate" name="dlgBtnCreate" value="Create" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>
                            <td align="right">
                                <input type="button" id="dlgBtnSave" name="dlgBtnSave" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="canUpdate" name="canUpdate" value='<%=ViewData["CanUpdate"]%>' />
    <input type="hidden" id="canAdd" name="canAdd" value='<%=ViewData["CanAdd"]%>' />
    <input type="hidden" id="canDelete" name="canDelete" value='<%=ViewData["CanDelete"]%>' />
</asp:Content>
