﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.StatePolicyViewModel>" %>




<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <%--<link rel="Stylesheet" type="text/css" href="../../css/default.css" />--%>
   <div class="admin">
     <h1 class="heading"> State Policy</h1>
  


    <table style="margin-top:20px;">
        <tr> 
            <td colspan="2"  style="padding-left:277px;padding-bottom:10px;">
           <ul id="message" name="ulMessage" style="color:red; visibility:hidden">
           <li>
            <label id="lblMessage" style="visibility:visible; color:Red"></label>
            </li>
            </ul>
            </td>
        </tr>
        <tr height="40px">
        
            <td align="right" width="300px">
                Select State :
            </td>
            <td>
             <%= Html.DropDownList("ddlState", Model.States  , new { style = "width:200px" }  )%>                                                          
                    
            </td>
            
        </tr>
       
      
        
        <tr height="40px">
            
            <td align="right" valign="top">
           <br />
                Policy :
            </td>
            <td>
            <br />
            <table >
            
                 <% foreach (var policy in Model.Policies) { %> 
                 <tr>
                 <td>
                    <input type="checkbox" name="selectedObjects" id="<%=policy.Id%>" value="<%=policy.Id%>"> 
                      <%= policy.Name%>   </input>
                                          
                                          </td><%}%>
                                        </tr>
                </table>
            
            
            
            </td>
            
        </tr>
        
        
        <tr height="40px">
          
        
            <td align="right">
            </td>
            <td>
         
                <br />
                <br />
                <input type="button" id="btnSubmit" name="btnSubmit" type="submit" value="Submit"     class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <input type="button" id="btnClear" name="btnClear" value="Clear" 
                class="ui-button ui-widget ui-state-default ui-corner-all"/>
            </td>
            <td>
            </td>
        </tr>
    </table>
 <div id="sidebar"></div>
 </div>
  
    
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <%--<script src='<%=ResolveClientUrl("../../Scripts/js/jquery.validate.js")%>' type="text/javascript"></script> --%>
  <script src='<%=ResolveClientUrl("~/Scripts/forms/statepolicy.index.js")%>' type="text/javascript"></script>
   
</asp:Content>


