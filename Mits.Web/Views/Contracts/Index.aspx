﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

        <link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />
    
    <script src="../../Scripts/js/ui.dropdownchecklist.js" type="text/javascript"></script>

     <h3 style="color:Gray; padding-left:30px; padding-top:20px" >
        <%=ViewData["Name"] %> Contracts</h3>
        <input id="hfshowdelbtn" type="hidden" />
    
    <table cellspacing="0" width="70%" style="margin-top: 5px; margin-left:30px;">
        <tr>
            <td align="center">
            </td>
        </tr>
       <tr>
              <td  align="center" style="padding-top:30px" >
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table>
    
       <script src='<%=ResolveClientUrl("~/Scripts/forms/contracts.index.js")%>' type="text/javascript" ></script>

</asp:Content>

