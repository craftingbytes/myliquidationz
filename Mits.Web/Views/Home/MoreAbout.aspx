﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>

                    <div id="content">
                        <div class="post">
                            <h2 class="title">
                                More About MITS</h2>
                            <div class="entry">
                                <div class="Section1">
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <i><b>The Leading Take Back Program in the U.S.</b></i><o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Manufacturers Interstate Takeback System or MITS, is an administrative  program developed by E-World Online. 
                                            In cooperation with electronics manufacturer sponsors and recycling partners, MITS offers a convenient nationwide solution allowing consumers to recycle their electronics responsibly. <span style='mso-spacerun: yes'>
                                            </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            By the end of 2010, the MITS program and its members will process over 18 million pounds of electronic waste through state mandated programs. 
                                            This represents nearly 25% of the total market share throughout the US. <span style='mso-spacerun: yes'> </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            MITS primary objective is to divert electronics from entering our landfills and promote proper environmentally sound disposal
                                             of this waste stream. MITS participants can expect to have available an array of convenient options for recycling end of life electronics. <span style='mso-spacerun: yes'></span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Our parent company, E-World Recyclers, is one of the first electronic recyclers in the U.S to achieve certification of the R2 standard. 
                                            All of our affiliate processers are in the queue to become R2 certified as a condition of their participation in the MITS program.<span style='mso-spacerun: yes'>
                                            </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            E-World Recyclers is an industry leader, with a successful track record of providing disposal and reuse solutions for our trusted clients. <span style='mso-spacerun: yes'>
                                            </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="centerright">
                        <h2>
                            Our Clients</h2>
                        <div align="center">
                            <a href="../Home/Clients">
                                <img src="../../Content/images/client-logos.jpg" width="240" height="214" border="0" /></a><br />
                            <br />
                            <a href="../Home/Clients">
                                <img src="../../Content/images/see-clients.jpg" width="143" height="28" border="0" /></a>
                        </div>
                    </div>
                    <!-- end #center right -->
                    <div style="clear: both;">
                        &nbsp;</div>

</asp:Content>

