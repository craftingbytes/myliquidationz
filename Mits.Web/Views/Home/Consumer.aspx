﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <h2></h2>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<div class="rs">
    <h2 class="title">Consumer Solutions </h2>
        <div id="page-bgtop">
          <div id="page-bgbtm">
            <div id="content">
                <div class="post">
                    <div class="entry">
                        If you are a consumer who would  like to recycle your end of life electronic device, our electronic manufacturing  sponsors have provided the following options at NO COST TO YOU: <br />
                        <br />
                        <ul>
                            <li><a href="/MailBack/Index">Mail back Program</a></li>
                            <li><a href="/DropOffLocations/Index">Convenient Drop Off Locations </a></li>
                            <li><a href="/collectionevent/map">Recycling Events</a></li>
                            <li><a href="/Static/BestBuyDropOffLocationsinMissouri.pdf" target="_blank">Retailer Locations</a></li>
                        </ul>
                        <p><br />
                            <h3>
                                New York Consumer Special Instructions</h3>
                            <p>
                                E-World Online's drop off locations and mail back programs are offered free of charge to any consumer in New York.  Consumers include: Households, Small Businesses (for-profit entities which have 49 or fewer full time employees) Not-for-profit corporations with 74 or fewer full time employees, or any not-for-profit corporations designated under section 501(c)(3) of the internal revenue code.  
                            </p>
                            <br />
                            <h3>
                                Consumer Education-Data Destruction (NY Consumers)
                            </h3>
                            <p>
                                For consumers that wish to destroy data stored on hard drives and other covered electronic equipment, which have internal memory there are several options in which the data can be destroyed.  If you wish to destroy the data before surrendering to a recycler or collector; you can purchase "Data wiping software" from local retailers or online websites.  An example of a retailer would be Best Buy or Circuit City.  If you wish to have your data destroyed as an added service, the MITS Network has contacted RCR&R.  To find your nearest drop off location, please access the MITS drop off location feature.
                            </p>
<p>The links provided below are in no way affiliated or have a relationship with E-World Online, LLC.  The links are provided in an effort to better educate consumers on the electronic recycling process.  E-World Online, LLC provides links and information of this nature in order to keep as many electronic devices out of landfills as possible.  Thank you for recycling.</p>
			<ul>
                            <li><a href="http://www.techsoup.org/learningcenter/software/page5724.cfm?cg=searchterms&sg=data%20wiping" target="_blank">Avoid Data Theft: Clean Your Old Hard Drives </a></li>
                            <li><a href="http://www.techsoup.org/learningcenter/software/page5726.cfm" target="_blank">Obliterate Your Hard Drive Data with Disk-Wiping Software </a></li>
   
                        </ul>
                            
                            <br />
                            <br />
                            <br />
                        </p>
                    </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="sidebar">
                        <div class="manusolu">
                                <h2><a href="\Home\Manufacturer">Manufacturer Solutions </a></h2>
                                <p>Our web based solution makes it easy for you to remain fully compliant with extended producer responsibility guidelines being enacted in many states. Track your recycling efforts, get customized reports and e-mail alerts that simplify your sustainability efforts. </p>
                        </div><div class="recysolu">
                              <h2><a href="\Home\Recycler">
                                  Recycler Solutions  </a></h2>
                                <p>Increase the volume of material that is being processed at your facility and let us help you keep your costs low through consolidation and strategic partnerships with other businesses in our network. </p>
                        </div>
                        <div class="twitter">  <h2> <a href="http://twitter.com/#!/EWorldOnline" target="_new"> E-World on Twitter</a></h2></div>
                    <!--div class="facebook"><h2> E-World on Facebook</h2></div-->
                    </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
                </div>
            </div>
            </div>
</asp:Content>
