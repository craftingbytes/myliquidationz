﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>
<div class="rs">
                    <h2 class="title">Contact us</h2>
                        <div id="content">
                        <div class="post">
                          
                     
              <div class="entry">
                            <div style="padding-top:5px; width:40%; float:left">
                                <h3>
                                    E-WORLD ONLINE</h3>
                                1390 Engineer St.<br />
                                Vista, Ca 92081<br /><br />
                                Office: (760) 599-0888<br />
                                Fax: (760) 599-0840
                                <br />
                                Email: <a href="mailto:info@e-worldonline.com">info@e-worldonline.com</a><br />
                                <!-- <br />
                                Toll free:<span style="color: #0072BC; font-size: medium; font-weight: bold">877-342-6756</span>
                                <br />-->
                                <br />
                                <a href="http://www.eworldrecyclers.com" target="_new" ><img src="/Content/images/eworldrecyclers.jpg" style="border-style: none"/></a>
                            </div> 
                        <div class="sonylink" style="float:right; width:48%; padding-top:0px;">
                      <div><img src="/Content/images/Sony.gif" class="no"  style=" margin-left:0px; margin-right:10px; vertical-align:top;" /></div> 
                       <p style="font-size:1.5em;">
  <strong><em><a href="../Content/images/sonytakebackprogram.pdf" target="_new">Sony  Program Details</a></em></strong><br />
 <strong><em><a href="/Home/SonyForm" target="_new">Sony  Program Application</a></em></strong></div><div style="clear: both;">
                        &nbsp;</div></div>
                        <!-- end #center right -->
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                  </div>
                        <div id="sidebar" style=" margin-right:40px;"><div class="twitter">  <h2> <a href="http://twitter.com/#!/EWorldOnline" target="_new"> E-World on Twitter</a></h2></div></div>
                        </div>  <div style="clear: both;">
                        &nbsp;</div>
        <!-- end #page -->
</asp:Content>

