﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <h2></h2>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<h2 class="title" id="p1">Conservation and Protection    </h2>
		<div id="page-bgtop">
        
        <div id="page-bgbtm">
<div id="content">
					<div class="post">
                   


					  <div class="entry">
					    <p>Today, whether we are at work, at home, or shopping at the mall, we are constantly surrounded by technology. Computers, cell phones, televisions, stereos and other electronic products are always with us. But what happens to these devices when they are no longer useful?</p>
					  </div>
					  <div class="entry">
  <p>Electronics recycling offers the opportunity to transform  old technology into a renewable resource. By reclaiming materials such as  plastics, glass, and metals, e-scrap processors mine these valuable commodities  for use in the next generation of electronic products. </p>
					  </div>
					  <div class="entry">
  <p>This conservation strategy also protects the environment by  keeping scrap materials out of city dumps and landfills. This is important  because many of these products contain potentially hazardous materials like  lead and mercury which must be disposed of safely.					  </p>
					  </div>
<div class="entry">
  <p>For more information about cooperative sustainability  efforts in the field of electronics,<a href="/Home/SharedRes"> click here</a>.&nbsp; </p>
</div>
					</div>
					<div style="clear: both;">&nbsp;</div>
				</div>
				<!-- end #content -->
				 <div id="sidebar">
				   <div class="twitter">
                               <h2 >
                                <a href="http://twitter.com/#!/EWorldOnline"> E-World on Twitter</a>     </h2>
</div>
<!--div class="facebook">
                               <h2>
                                   E-World on Facebook                            </h2>
</div-->
                       
              </div>

				<!-- end #sidebar -->
				<div style="clear: both;">&nbsp;</div>
			</div>
		</div>
</asp:Content>


