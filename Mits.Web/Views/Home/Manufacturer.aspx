﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <h2></h2>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<div class="mfs">
 <h2 class="title">Manufacturer Solutions</h2>
            <div id="page-bgtop">
              <div id="page-bgbtm">
                    <div id="content">
                        <div class="post">
                          
                     
              <div class="entry">
                                <p>
                                    MITS (Manufacturer Interstate Takeback System) recycled over 45 million pounds of electronics in 2011 and expects to recycle over 60 million pounds in 2012.  We offer compliance solutions for our clients in every state, whether or not legislation is in effect.  In most cases our administrative solutions come at no added cost to our manufacturing partners as we negotiate the lowest pricing due to our established network of partners.  </p>
                                <p>
                   <a name="section1" id="section1"> <strong>Clients Benefit from Economies of Scale</strong></a> MITS Preferred Network of recyclers are available in every state with current legislation. The network continues to expand as new laws are introduced throughout the country. Clients will benefit with lower recycling pricing due to volume discounting. </p>
                        <p><a name="section2" id="section2"><strong>Real Time Status Reporting Online</strong> </a>Reports can be accessed anytime via the secure MITS online portal. Reporting features include: <br />
              
                </p><ul type="disc">
                  <li><strong><em>Volume       requirement to volume recycled calculator </em></strong>– know       exactly what you need to recycle vs. what has already been recycled </li>
                          <li><strong><em>Payables/price       per pound/per state</em></strong> – know what you’ve paid, and what you still       need to pay in every state at the click of a mouse </li>
                          <li><strong><em>Source       documentation</em></strong> – know exactly what you are paying for! </li>
                          <li><strong><em>Brand Sampling Details</em></strong> –  Coordination       with       department representatives, 3rd party auditors and       recycling partners   to produce required quarterly sampling reports. </li>
                        </ul>
                        <p><a name="section3" id="section3"><strong>Detailed Source Documentation Eliminating Fraud</strong></a><strong> </strong> The MITS  program includes the most detailed source verification system on the market,  enabling our manufacturing  partners   to view exactly what kind of material they are paying to recycle, where it came  from, who transported it, who recycled it,    and  the total volume in pounds of all recycled materials.  Each of these reports  are readily  available online  and included  with  each invoice.<br />
                          <br />
                        Data cells required include:  Method of collection, date of collection, address, consumer information ( if requested and applicable), category of covered devices,  weight of covered devices, rural vs. non-rural classification and executed  certificates of processing. </p>
                        <p><a name="section4" id="section4"><strong>Customized Proprietary Software </strong></a> Designed  and developed exclusively for MITS, our customized online application can be  securely accessed by our  manufacturing  partners, state regulators and  processing  partners. Some of the functionality includes: </p>
                        <ul type="circle">
                          <li>  <em>E-Mail alerts to remind you when key paperwork is       due i</em><em>n every state.</em></li>
                          <li>  <em>Pre-populated templates make filing       your documents simple.</em></li>
                          <li>  <em>A unique on-line database of       legislative and recycling information gives your company multiple user       access to state run programs and your company data.</em></li>
                     
                          <li>A document  library exhibiting hundreds of the latest regulations, regulation amendments, plans and  department forms.</li>
                          <li>Detailed  reporting, with the ability to sort by preferred category. </li>
                        </ul>
                        <p><a name="section5" id="section5"><strong>On Demand Customer Service from Industry experts</strong></a><strong> </strong>Our staff is available via phone or  email at anytime to answer any questions regarding legislation, deadlines,  latest news and reporting requirements. By utilizing our   years of experience in  producer responsibility, the MITS program eliminates costs associated  with    additional  internal overhead,  missed  deadlines, software development   and the administrative  burden    of reporting and plan  drafting. </p>
                        <p>
                          <a name="section7" id="section7"><strong>Listing of 2,400  Convenient Drop Off Locations</strong></a><strong> </strong>As  a courtesy to the consumer, E-World Online provides a handy guide to locations  across the country that offers electronics recycling options. Consumers simply  type in their zip code and will be provided the location, distance and map view  of the drop off locations in their area. This function fulfills all state  requirements.</p>
                        <p>
                          <a name="section6" id="section6"> <strong>A Customized Mail Back System Compliant for 50 States </strong></a>Required in order to fulfill  manufacturer responsibility in multiple states. MITS has an easy to use mail  back system which meets the criteria in every state. The mail back program is  fully administered and utilizes recyclers in the MITS Preferred Network.</p>
                        <ul type="circle">
                          <li><em>Simple       zip code entry initiates service</em></li>
                          <li><em>Prepaid mailing label</em></li>
                          <li><em>Boxes provided upon request</em></li>
                        </ul>
                        <p><a name="section8" id="section8"><strong>Automated Calendar Detailing Key  Events and Deadline </strong></a> <strong> </strong>MITS   software delivers real time  alerts  relating  to    state regulatory deadlines via email. </p>
                        <ul>
                          <li>Key deadline dates apply in  every state, and include, but are not limited to submission of registration,  state plans, sampling reports, annual reporting, invoicing and volume updating.</li>
                          <li>These
                        
                        alerts  include  not just the deadline dates, but a series of   action items leading up to the deadline date along    with relevant documents. . </p>
                <p>&nbsp; </li></ul>
                        <p><br />
                          <br />
                          <br />
                          <br />
                          <br />
                        </p>
                    </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="sidebar">
                       <div class="conssolu">
                                <h2><a href="\Home\Consumer">Consumer Solutions </a></h2>
                                <p>Everyone wants to be green, but sometimes it’s hard to know  where to start. You can use this site to help you find recyclers in your area  who will take that old computer or fax machine off your hands</p>
</div>
                           <div class="recysolu">
                                <h2><a href="\Home\Recycler">Recycler Solutions&nbsp; </a></h2>
                                <p>Increase the  volume of material that is being processed at your facility and let us help you  keep your costs low through consolidation and strategic partnerships with other  businesses in our network. </p>
</div>
                   <div class="twitter">  <h2> <a href="http://twitter.com/#!/EWorldOnline" target="_new"> E-World on Twitter</a></h2></div>
<!--div class="facebook"><h2> E-World on Facebook</h2></div-->
    </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
                </div>
            </div>
            </div>
</asp:Content>
