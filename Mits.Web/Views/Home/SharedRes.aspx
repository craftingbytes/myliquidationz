﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <h2></h2>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<h2 class="title">Shared Responsibility</h2>
            <div id="page-bgtop">
              <div id="page-bgbtm">
                    <div id="content">
                      <div class="post">
                        <h3 id="p1">Conservation and Protection</h3>
                        <div class="entry">
                          <p>Electronics recycling offers the opportunity to transform  old technology into a renewable resource. By reclaiming materials such as <strong>plastics</strong>, <strong>glass</strong>, and <strong>metals</strong>,  e-scrap processors produce a valuable commodity for use in the next  generation of electronic products. </p>
                          <p>This conservation strategy also protects the environment by  keeping scrap materials out of city dumps and landfills. This is important  because many of these products contain potentially toxic materials like <strong>lead</strong> and <strong>mercury</strong> which must be disposed of safely. </p>
                          <p>On this page we hope to provide you with information about cooperative sustainability efforts in the field of electronics.</p>
                        </div>
                        <h3 id="p2">Certified Electronics Recyclers</h3>
                        <div class="entry">
                          <p>Recently, recycling certification  programs have been introduced to the marketplace. These programs are voluntary  and formalize best management practices that meet a high level of standards.  Many electronics recyclers recognize the value in obtaining these third-party  accredited certifications such as the R2-Responsible Recycling Guidelines and  eStewards.&nbsp; In addition, many electronics recyclers are also obtaining  certification through accredited programs like ISO, OHSAS and RIOS to prove  their commitment to health and safety of workers and concern for environmental  impacts.</p>
</div>
                        <h3 id="p4">Retailer Participation</h3>
                        <div class="entry">
                          <p>Consumer distribution channels such as electronics and  office equipment stores offer an easy way for end-users to recycle. Nationwide  chains such as <strong>Best Buy</strong> and <strong>Office Depot</strong> have recently made their  stores available as recycling drop-off locations for used equipment.</p>
                        </div>
                        <h3 id="p5">The Future: Designing Greener Electronics</h3>
                        <div class="entry">
                          <p>In the past, manufacturers often created their products with  little thought as to the impact of their eventual disposal. Today this is  changing. Efforts are now under way develop electronics that are designed for  ease of recycling. <strong>EPEAT</strong> represents  a collaborative effort on the part of producers, recyclers, and environmental  organizations to create the next generation of green electronics which will use  safer materials and will be much easier to disassemble and reuse. </p>
                        </div>
                      </div>
                      <div style="clear: both;">
                          &nbsp;</div>
                    </div>
                    <!-- end #content -->
                   <div id="sidebar">
                     <div class="twitter">  <h2> <a href="http://twitter.com/#!/EWorldOnline" target="_new"> E-World on Twitter</a></h2></div>
<!--div class="facebook"><h2> E-World on Facebook</h2></div-->
            </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
                </div>
            </div>
</asp:Content>
