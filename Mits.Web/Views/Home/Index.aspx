﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
	<!--<script src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>--> 
    <script src="/Scripts/js/slides.min.jquery.js"></script>
    <!--script type="text/javascript" src="/Scripts/js/jquery-scroll.pack.js"></script-->
    <script type="text/javascript">
        $('#clients a').append('<span class="hover"><\/span>').each(function () {
            var $span = $('> span.hover', this).css('opacity', 0);
            $(this).hover(function () {
                $span.stop().fadeTo(500, 1);
            }, function () {
                $span.stop().fadeTo(500, 0);
            });
        });
        $(function () {
            $("#slides").slides({
                play: 15000,
                effect: 'fade',
                fadeSpeed: 2500,
                crossfade: true
            });
        });
        function AutoScroll(obj) {
            $(obj).find("ul:first").animate({
                marginLeft: "-105px"
            }, 500, function () {
                $(this).css({ marginLeft: "0px" }).find("li:first").appendTo(this);
            });
        }
        (function ($) {
            $.fn.extend({
                Scroll: function (opt, callback) {
                    //参数初始化
                    if (!opt) var opt = {};
                    var _this = this.eq(0).find("ul:first");
                    var lineH = _this.find("li:first").height(), //获取行高
                        ultop = opt.ultop,
                        line = opt.line ? parseInt(opt.line, 10) : parseInt(this.height() / lineH, 10), //每次滚动的行数，默认为一屏，即父容器高度
                        speed = opt.speed ? parseInt(opt.speed, 10) : 800, //卷动速度，数值越大，速度越慢（毫秒）
                        timer = opt.timer ? parseInt(opt.timer, 10) : 3000; //滚动的时间间隔（毫秒）
                    if (line == 0) line = 1;
                    var upHeight = line * lineH;
                    //滚动函数
                    scrollUp = function () {
                        _this.animate({
                            marginTop: (ultop + 69)
                        }, speed, function () {
                            for (i = 1; i <= line; i++) {
                                _this.find("li:last").prependTo(_this);
                            }
                            _this.css({ marginTop: ultop });
                        });
                    }
                    //鼠标事件绑定
                    _this.hover(function () {
                        clearInterval(timerID);
                    }, function () {
                        timerID = setInterval("scrollUp()", timer);
                    }).mouseout();
                }
            })
        })(jQuery);
</script>
<style>
#clients{width:940px;height:60px;overflow:hidden; margin-left:20px;}
#clients li{height:60px;padding-left:0px; float:left; width:100px; list-style:none;}
#clients ul{ width:1100px;}
#newsfeed{width:100%;height:210px;overflow:hidden;clear:both;}
#newsfeed li{height:70px; padding-top:0px; list-style:none; border:none;}
#newsfeed ul {height:700px; margin-top:0px}
#newsfeed li div.txt{ height:50px; overflow:hidden; line-height:1.2em; clear:both;}
#newsfeed li div.date{ height:20px; margin-top:0px; clear:both; text-align:left; overflow:hidden;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

 
    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>

<div id="slides">
  <div class="slides_container">
     <img src="../content/images/homegraphic-3.jpg" alt="E-World Recyclers" width="960" height="262"
                    border="0"  />
                   
  <img src="../content/images/homegraphic-1.jpg" alt="E-World Recyclers" width="973" height="262"
                    border="0"  />
               
                  
             
                <img src="../content/images/homegraphic-4.jpg" alt="E-World Recyclers" width="960" height="262"
                    border="0" />
                  
                <img src="../content/images/homegraphic-5.jpg" alt="E-World Recyclers" width="960" height="262"
                    border="0" /></div></div>
                    <div id="clients">
             <ul>
<li> <a href="http://www.emachines.com/" target="_blank" class="emachines"></a></li>
<li> <a href="http://www.cvision.com/" target="_blank" class="cvision"></a></li>
<li> <a href="http://www.us.aiwa.com/" target="_blank" class="aiwa"></a></li>
<li> <a href="http://www.sony.com/" target="_blank" class="sony"></a></li>
<li> <a href="http://us.acer.com/ac/en/US/content/home" target="_blank" class="acer"></a></li>
<li> <a href="http://www.viewsonic.com/" target="_blank" class="viewsonic"></a></li>
<li> <a href="http://www.nec.com/" target="_blank" class="nec"></a></li>
<li> <a href="http://www.lg.com/" target="_blank" class="lg"></a></li>
<!--  <li> <a href="http://www.utobia.co.jp/" target="_blank" class="asi"></a></li> Commented by Randy 7-15-11  -->
<li> <a href="http://www.gateway.com/" target="_blank" class="gateway"></a></li>
<li> <a href="http://www.westinghouse.com/" target="_blank" class="westinghouse"></a></li>
<li> <a href="http://us.aoc.com/" target="_blank" class="envision"></a></li>
<li> <a href="http://www.viewsonic.com/products/desktop-monitors/lcd/optiquest-series/" target="_blank" class="optiquest"></a></li>
<li> <a href="http://craigelectronics.com/" target="_blank" class="craig"></a></li>
<li> <a href="http://www.pantechusa.com/" target="_blank" class="pantech"></a></li>
<li> <a href="http://www.onkyousa.com/" target="_blank" class="onkyo"></a></li>
<li> <a href="http://www.planardigitalsignage.com//" target="_blank" class="planar"></a></li>
<li> <a href="http://www.kcpi.us//" target="_blank" class="affinity"></a></li>
<li> <a href="http://www.kcpi.us//" target="_blank" class="kcpi"></a></li>
<li> <a href="http://www.haieramerica.com//" target="_blank" class="haier"></a></li>
          </ul></div>
        
            <div id="page-bgtop">
                <div id="page-bgbtm">
                    <div id="homecontent">
                        <!--<div class="post">
						
						<div class="entry">
					    
						</div>
					</div>-->
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="centerleft">
                      
                            
                            <div class="w2">
                                <h2>
                                 <a href="/StateGuidelines/Index">   State Regulations</a></h2>
                                Does my State  have an electronics recycling law?
                            </div><div class="w3">
                                <h2>
                                   <a href="/Home/SharedRes"> Shared Responsibility</a></h2>
                                <ul>
                                    <li>Certified Electronics Recyclers</li>
                                    <li>
                                     Manufacturer Responsibility
                                    </li>
                                    <li>
                                    Retailer Participation
                                    </li>
                                    <li>
                                     The Future: Designing Greener Electronics
                                    </li>
                                </ul>
                            </div>
                            <div class="w1">
                                <h2>
                                  <a href="/Home/Environmental">  Environmental</a></h2>
                               Conservation and Protection
                            </div>
                     
                  </div>
                    <!-- end #center left -->
                    <div id="centermiddle">
                    <h2>The MITS Network: A Solution for …
                   
                </h2>
                        <ul>
                           
                            <li class="w1"> <h3>
                              <a href="\Home\Consumer">Consumers</a></h3>
                                <p>Everyone wants to be green, but sometimes it’s hard to know  where to start. You can use this site to help you find recyclers in your area  who will take that old computer or television off your hands. </p>
</li>
                               <li class="w2"> <h3><a href="\Home\Manufacturer">
                                Manufacturers</a></h3>
                                <p>Our web based solution makes it easy for you to remain fully  compliant with extended producer responsibility guidelines being enacted in  many states. Track your recycling efforts, get customized reports and e-mail  alerts that simplify your sustainability efforts. </p>
</li>
  <li class="w3"> <h3>
                                    <a href="\Home\Recycler">Recyclers</a><br>
</h3>
          <p>Increase the volume of material that is being processed at your facility and let us help you keep your costs low through consolidation and strategic partnerships with other businesses in our network.  </p>
          <br>
<br>
<br>
                                    <br>
                                    <br>
                                </p>
                               
                            </li>
                        </ul>
                        <div></div>
                    </div>
                    <!-- end #center middle -->
                    <div id="centerright">
                    <div class="misson"><h2>Mission Statement</h2>
                    <p>MITS mission is to assist manufacturers with sustainability efforts by offering a nationwide network of collectors and recyclers to responsibly manage e-waste and provide convenient recycling options for consumers.</p>
                    </div>
                      <div class="w2">
                             <h2>
                                  <a href="http://twitter.com/#!/EWorldOnline" target="_new"> E-World on Twitter</a><br>
</h2>
</div>
<div class="w3">
                                <h2>News Feed </h2>
                                <div id="newsfeed">
                                <ul class="news" id="newslist">
                                <%
                                    var FeedNews = ViewData["NewsFeeds"] as System.Collections.Generic.List<Mits.DAL.EntityModels.NewsFeed>;
                                    foreach (var feed in FeedNews)
                                    {
                                 %>
                                <li>
                                    <div class="date"><p><%= ((DateTime)feed.Date).ToString("MM/dd/yyyy") %></p></div>
                                    <div class="txt"><p style="height: 30px; overflow:hidden;"><%= feed.News%></p>
                                <%
                                    if (!string.IsNullOrEmpty(feed.FeedLink))
                                    {
                                        var link = !feed.FeedLink.ToLower().StartsWith("http://") ? "http://" + feed.FeedLink : feed.FeedLink;
                                 %>
                                    <span><a href="<%= link %>" target="_blank" style="color:#14a7d1; font-size:11px">Read More</a></span></div>
                                <%
                                    }
                                 %>
                                </li>
                                <%
                                    }
                                %>
                                </ul></div>
</div>
                       
              </div>
                    <!-- end #center right -->
                    <div style="clear: both;">
                        &nbsp;</div>
                </div>
            </div>
<script>
    setInterval('AutoScroll("#clients")', 2000);
    var lilen = $("#newslist li").length;
    if (lilen > 3) {
        var ulheight = lilen * 70;
        var ultop = 3 * 70 - ulheight;
        $("#newslist").css({ height: ulheight });
        $("#newslist").css({ marginTop: ultop });
        $("#newsfeed").Scroll({ line: 1, speed: 800, timer: 8000, ultop: ultop });
    }
</script>
</asp:Content>
