﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/mits.master"%>

<asp:Content ID="errorContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table>
    <tr >
    <td>
    <h3 style="color:Gray; padding-left:20px; padding-top:20px">
    Error
    </h3>
     </td>
    </tr>
    <tr valign="top" >
    <td>    
    
   
    <p style="padding-left:20px; padding-top:50px; font-size:medium">
        Sorry, an error occurred while processing your request. <a href="javascript:history.go(-1);"> Go Back </a>
       
    </p>   
      <img src='<%=ResolveClientUrl("~/Content/images/error-icon.gif")%>'height="300" width="300" style="padding-left:200px" />
     </td>
     </tr>

     </table>
</asp:Content>
