﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>

        
                    <div id="content">
                        <div class="post">
                            <h2 class="title">
                                Clients</h2>
                            <div class="entry">
                                <p>
                                    MITS e-cycle offers manufacturers a turn-key solution for responsible recycling.
                                    <br />
                                    Our unique software based system will help you track your compliance with local
                                    regulations by municipality. It presents status, forecasting and data analysis information
                                    in an easy to read dashboard format.
                                    <br />
                                    <br />
                                    The E-World Online program currently recycles over 18 million pounds of electronics
                                    annually for the clients named. We are able to offer compliant solutions for our
                                    clients in every state, whether or not legislation is in effect. In most cases our
                                    administration solutions come at no cost to our manufacturing partners and we are
                                    able to negotiate low pricing due to our established network of partners.
                                    <br />
                                    <br />
                                    If you are a recycler or would like to have your facility listed as a collection
                                    site, please <a href="mailto:info@mitsecycle.com?subject=My%20Business%20as%20a%20Collection%20Site"
                                        title="Email us">click here</a>.</p>
                                For more information or to become a corporate partner with MITS, please call (760)
                                599-0888 or email us at info@mitsecycle.com<br />
                                <br />
                                <br />
                                <table style="margin-left: 40px; height: 504px;" width="519">
                                    <tr>
                                        <td align="center">
                                            <a target="_new" href="http://www.sony.net/SonyInfo/csr/environment/recycle/index.html"
                                                style="color: White">
                                                <img src="../../Content/images/Sony.gif" alt="" /></a>
                                        </td>
                                        <td align="center">
                                            <a target="_new" href="http://www.viewsonic.com/company/green/recycle-program/" style="color: White">
                                                <img src="../../Content/images/ViewSonic.jpg" alt="" /></a>
                                        </td>
                                        <td align="center">
                                            <a target="_new" href="http://www.necdisplay.com/EnvironmentalInitiatives/NECProductRecyclingInfo/"
                                                style="color: White">
                                                <img src="../../Content/images/NEC.gif" alt="" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <a target="_new" href="https://usspringpromo.acer.com/Recycle/Index.aspx" style="color: White">
                                                <img src="../../Content/images/Acer.gif" alt="" /></a>
                                        </td>
                                        <td align="center">
                                            <a target="_new" href="http://www.lg.com/global/sustainability/environment/take-back-recycling.jsp"
                                                style="color: White">
                                                <img src="../../Content/images/LG.gif" alt="" /></a>
                                        </td>
                                        <td align="center">
                                            <a target="_new" href="http://www.emachines.com/corporate/environment.html" style="color: White">
                                                <img src="../../Content/images/e-machines.gif" alt="" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <a target="_new" href="http://www.gateway.com/about/corp_responsibility/elec_waste_recycling_leg.php"
                                                style="color: White">
                                                <img src="../../Content/images/gateway.gif" alt="" /></a>
                                        </td>
                                        <td align="center">
                                            <a target="_new" href="http://www.sony.net/SonyInfo/csr/environment/recycle/index.html"
                                                style="color: White">
                                                <img src="../../Content/images/aiwa.gif" alt="" /></a>
                                        </td>
                                        <td align="center">
                                            <a target="_new" href="http://www.viewsonic.com/company/green/recycle-program/" style="color: White">
                                                <img src="../../Content/images/optiquest.gif" alt="" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                     <td align="center">
                                            <a target="_new" href="" style="color: White">
                                                <img src="../../Content/images/cvision.gif" alt="" /></a>
                                        </td>
                                         <td align="center">
                                            <a target="_new" href="" style="color: White">
                                                <img src="../../Content/images/utobia.gif" alt="" /></a>
                                        </td>
                                    
                                    </tr>
                                   
                                </table>
                                <br />
                            </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="sidebar">
                        <ul>
                            <li>
                                <h2>
                                    Manufacturer Solutions</h2>
                                <ul>
                                    <li><a href="../Home/ReadMore#section1">Client Benefit from Economies of Scale </a>
                                    </li>
                                    <li><a href="../Home/ReadMore#section2">Real Time Status Reporting in Every State</a></li>
                                    <li><a href="../Home/ReadMore#section3">Detailed Source Documentation Eliminating Fraud</a>
                                    </li>
                                    <li><a href="../Home/ReadMore#section4">Customized Proprietary Software Common to Processors,
                                        Manufacturers and Administration</a></li>
                                    <li><a href="../Home/ReadMore#section5">On Demand Customer Service from Industry experts
                                    </a></li>
                                    <li><a href="../Home/ReadMore#section6">A Customized Mail Back System Compliant for 50
                                        States</a></li>
                                    <li><a href="../Home/ReadMore#section7">Listing of 2,400 Convenient Drop Off Locations</a></li>
                                    <li><a href="../Home/ReadMore#section8">Automated Calendar Detailing Key Events and Deadlines</a></li>
                                </ul>
                                <div align="center">
                                    <a href="../Home/ReadMore">
                                        <img src="../../Content/images/read-more.jpg" border="0" /></a></div>
                            </li>
                        </ul>
                    </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
          
        <!-- end #page -->
   </asp:Content>

