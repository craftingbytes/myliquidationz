﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <h2></h2>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 <h2 class="title">Recycler Solutions </h2>
            <div id="page-bgtop">
              <div id="page-bgbtm">
                    <div id="content">
                        <div class="post">
                          
                     
              <div class="entry">
                                <p>All processors  within this managed network are audited for compliance to the eStewards or R2 certification  standards.<br />
This combined effort offers benefits for consumers, recyclers,  collectors and manufacturers alike.  The  network’s drop-off locations allow consumers many convenient and accountable  options for their end-of-life electronic devices.   Benefits to   processors  include:  decreasing downstream costs with access  to volume and full support  from the MITS team  when preparing reports   and source documentation.<br />
To express your  interest in joining the MITS Preferred Network please <a href="mailto:info@mitsecycle.com?subject=My%20Business%20as%20a%20Collection%20Site" title="Email us">click <em>(mail:info@e-worldonline.com)</em> here</a>.  <br />
For more  information or to become a corporate partner with MITS<strong><strong> :</p>
                <ol>
                                  <li>1-877-342-6756 </li>
                                  <li> info@e-worldonline.com</li>
                                </ol>
<p><strong>Explore  the links below to view current MITS Opportunities:</strong></p>
<br />
<div style=" float:left; margin-left:50px; margin-right:50px;"><p style="font-size:1.5em;"><br />
  <strong><em><a href="../Content/images/sonytakebackprogram.pdf" target="_new">Sony  Program Details</a></em></strong></p>
<p style="font-size:1.5em;"> <strong><em><a href="http://www.eworldrecyclers.com/content/webforms/sonyform.php" target="_new">Sony  Program Application</a></em></strong> <br /></div>
<div style=" float:left; margin:10px;">
<img src="../Content/images/Sony.gif" alt="" width="152" height="136"  /></div>

  <br />
  <br />
  <br />
</p>
              </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="sidebar">
                      <div class="conssolu">
                                <h2><a href="\Home\Consumer">Consumer Solutions </a></h2>
                                <p>Everyone wants to be green, but sometimes it’s hard to know  where to start. You can use this site to help you find recyclers in your area  who will take that old computer or fax machine off your hands</p>
</div><div class="manusolu">
                                <h2><a href="\Home\Manufacturer">
                                 Manufacturer Solutions</a></h2>
                                <p>Our web based  solution makes it easy for you to remain fully compliant with extended producer  responsibility guidelines being enacted in many states. Track your recycling  efforts, get customized reports and e-mail alerts that simplify your sustainability  efforts. </p>
</div>

      <div class="twitter">  <h2> <a href="http://twitter.com/#!/EWorldOnline" target="_new"> E-World on Twitter</a></h2></div>
<!--div class="facebook"><h2> E-World on Facebook</h2></div-->
        </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
                </div>
            </div>
</asp:Content>
