﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>
       
                    <div id="content">
                    <div class="post">
                <h1 style="text-align:center">
                    <b><u>Manufacturer Solutions</u></b>
                   
                </h1>
                <br />
                <h3>
                    <u><a name="section1">Clients Benefit from Economies of Scale</a></u>
                </h3>
                <p class="MsoNormal" style='vertical-align: middle'>
                    <span style='font-size: 12.0pt; font-family: "ariel","Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                        Lower pricing due to volume discounting. By having an established relationship with
                        over 40 different industry leaders, MITS is able to negotiate extremely low pricing
                        for new clients.
                        <o:p></o:p>
                    </span>
                </p>
                <h3>
                    <u><a name="section2">Real Time Status Reporting in Every State</a></u></h3>
                <p style='vertical-align: middle'>
                    <span style='font-size: 12.0pt; font-family: "ariel","Calisto MT","serif"; mso-bidi-font-family: "ariel"'>
                        In order to be a truly complete solution, E-World Online has developed a fully interactive
                        reporting system. Reports can be accessed anywhere anytime online and are secure.
                        Reporting features include:
                        <ul>
                            <li><i><b>Volume requirement to volume recycled calculator </b></i>– know exactly what
                                you need to recycle vs. what has already been recycled </li>
                            <li><i><b>Payables/price per pound/per state</b></i> – know what you’ve paid, and what
                                you still need to pay in every state at the click of a mouse </li>
                            <li><i><b>Source documentation</b></i> – know exactly what you are paying for! </li>
                            <li><i><b>Weight status</b></i> – Know exactly what has been recycled, where, and in
                                what state, real time, and day to day </li>
                            <li><i><b>Brand Sampling Details</b></i> – Quarterly sampling results combined with
                                other members reduces your payments by thousands of dollars
                                <o:p></o:p>
                            </li>
                        </ul>
                    </span>
                </p>
                <h3>
                    <u><a name="section3">Detailed Source Documentation Eliminating Fraud</a></u></h3>
                <p>
                    The E-World Online MITS program includes the most detailed source verification system
                    on the market, enabling our manufacturing clients to view exactly what kind of material
                    they are paying to recycle, where it came from, who transported it, who recycled
                    it, when, where, and the total volume in pounds of all recycled materials.</p>
                <p>
                    Data cells required include: Method of collection, date of collection, address,
                    consumer information (when applicable), category of covered devices, weight of covered
                    devices, rural vs. non-rural classification and executed certificates of processing.</p>
                <h3>
                    <u><a name="section4">Customized Proprietary Software Common to Processors, Manufacturers
                        and Administration</a></u></h3>
                <p>
                    E-World Online designed and developed a customized online application that can be
                    securely accessed by our OEM clients, state regulators and our processing partners.
                    Some of the functionality includes:
                    <li><i>E-Mail alerts to remind you when key paperwork is due in every state.</i>
                    </li>
                    <li><i>Pre-populated templates make filing your documents simple.</i></li>
                    <li><i>A unique on-line database of legislative and recycling information gives your
                        company multiple user access to state run program’s and your company data.</i></li>
                </p>
                <h3>
                    <u><a name="section5">On Demand Customer Service from Industry experts </a></u>
                </h3>
                <p>
                    Our staff is available via phone or email at anytime to answer any questions regarding
                    legislation, deadlines, latest news and reporting requirements. By utilizing the
                    MITS program the time and money associated with missed deadlines, software development,
                    research and report drafting is eliminated.</p>
                <h3>
                    <u><a name="section6">A Customized Mail Back System Compliant for 50 States</a></u></h3>
                <p>
                    Required in order to fulfill manufacturer responsibility in multiple states. E-world
                    Online has developed an easy to use mail back system which meets the criteria in
                    every state. The mail back program is fully administered and fulfilled internally
                    by E-World Online.</p>
                <li><i>A Mail-back system is required in order to fulfill manufacturer responsibility
                    in multiple states. E-world Online has developed an easy to use mail back system
                    which meets the criteria in every state. </i></li>
                <li><i>Geographically simple to use with an interactive map of the US </i></li>
                <li><i>Easy to use zip code locator – gives the three closest mail back destinations</i></li>
                <li><i>Mail back system has over 2400 programmed destinations including 41 E-World Online
                    partners• The mail back system can be adapted to run each OEM’s mail back – whether
                    in a program state or not The mail back program is fully administered and maintained
                    internally by E-World Online. </i></li>
                <h3>
                    <u><a name="section7">Listing of 2,400 Convenient Drop Off Locations</a></u></h3>
                <p>
                    Utilizing its nationwide network, E-World Online has developed and publishes a list
                    of over 2400 different “convenient drop off locations”, including instructions educating
                    customers about how and where to take their waste.
                </p>
                <p>
                    This function fulfills all state requirements and the only information needed is
                    a zip code.</p>
                <p>
                    Consumers simply type in their zip code, click on the submit button and the 3 closest
                    recycling locations are displayed.
                </p>
                <p>
                    Click on a location for an immediate map and driving directions.</p>
                <h3>
                    <u><a name="section8">Automated Calendar Detailing Key Events and Deadlines</a></u></h3>
                <p>
                    MITS administration software delivers real time alerts regarding all state regulatory
                    deadlines. Clients have access to a comprehensive document library featuring pre-populated
                    forms.</p>
                <p>
                    The Interactive Event Calendar provides clients with the ability to track key deadline
                    dates. E-World Online delivers alerts of action items leading up to the deadline
                    dates and sends clients pre-populated documents that need to be submitted via email:
                    Key deadline dates apply in every state, and include, but are not limited to submission
                    of registration, state plans, sampling reports, annual reporting, invoicing and
                    volume updating.</p>
            </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="sidebar">
                        <ul>
                            <li>
                                <h2>
                                    Manufacturer Solutions</h2>
                                <p>
                                    Our web-based software provides…</p>
                                <ul>
                                    <li><a href="../Home/ReadMore#section1">Clients Benefit from Economies of Scale </a>
                                    </li>
                                    <li><a href="../Home/ReadMore#section2">Real Time Status Reporting in Every State</a></li>
                                    <li><a href="../Home/ReadMore#section3">Detailed Source Documentation Eliminating Fraud</a>
                                    </li>
                                    <li><a href="../Home/ReadMore#section4">Customized Proprietary Software Common to Processors,
                                        Manufacturers and Administration</a></li>
                                    <li><a href="../Home/ReadMore#section5">On Demand Customer Service from Industry experts
                                    </a></li>
                                    <li><a href="../Home/ReadMore#section6">A Customized Mail Back System Compliant for 50
                                        States</a></li>
                                    <li><a href="../Home/ReadMore#section7">Listing of 2,400 Convenient Drop Off Locations</a></li>
                                    <li><a href="../Home/ReadMore#section8">Automated Calendar Detailing Key Events and Deadlines</a></li>
                                </ul>
                            </li>
                            <li>
                                <h2>
                                    Consumer Solutions</h2>
                                
                                <ul>
                                    <li>Mail back Program </li>
                                    <li>Convenient Drop Off Locations </li>
                                    <li>Recycling Events</li>
                                    <li>Retailer Locations</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
         
        <!-- end #page -->
</asp:Content>

