﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MITS - Items and Address</title>
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <link rel="SHORTCUT ICON" href="/favicon.ico" />
    <link href="<%=ResolveUrl("~/Content/css/style.css")%>" rel="stylesheet" type="text/css" />
    <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/cupertino/jquery-ui-1.8.2.custom.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/jquery.multiSelect.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/ui.jqgrid.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/highlighter/styles/shCore.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/highlighter/styles/shThemeDefault.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/uploadify.css")%>" />
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery-1.4.2.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery-ui-1.8.4.custom.min.js")%>'
        type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.validate.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.multiSelect.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/grid.locale-en.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.jqGrid.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/grid.subgrid.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/highlighter/scripts/shCore.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/highlighter/scripts/shBrushJScript.js")%>'
        type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/highlighter/scripts/shBrushCss.js")%>'
        type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.tree.js")%>' type="text/javascript"></script>
  
 
</head>
<body>
    <% using (Html.BeginForm())
       {%>
 
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td align="center">
                <table width="993px" border="0" cellpadding="0" cellspacing="0" style="text-align: left;">
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="">
                        <img src="<%=ResolveClientUrl("~/Content/images/logo.png")%>" alt="MITS" height="85" border="0" /></asp:HyperLink>
                        </td>
                        </tr>
                    <tr>
                        <td class="mainbg" colspan="2" valign="top">
                        <h2 style="padding-top:30px; padding-left:30px">Site is on Yearly maintenance.We will get back soon...</h2>
                        <br /><img src="../../Content/images/icon/maintenance.JPG" style="padding-top:20px; padding-left:30px" />
                            </td>
                    </tr>
                   
                    <tr>
                        <td class="footer">
                            <p>MITS Release 2.2 : Copyright (c) 2010 E-WORLD RECYCLERS All Rights Reserved</p>
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
     <%}%>
    </body>
</html>
