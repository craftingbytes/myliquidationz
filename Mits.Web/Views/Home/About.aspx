﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="aboutContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   
    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>

        <div id="content">
                        <div class="post">
                            <h2 class="title">
                                About E-Waste</h2>
                            <img src="../../Content/images/content-about.jpg" alt="" width="579" height="188" />
                            <div class="entry">
                                <p>
                                    According to the EPA, Americans generated 3.16 million tons of e‐waste in the U.S in 2008. Of this amount, 
                                    only 430,000 tons or 13.6 % was recycled, the rest was trashed – in landfills or incinerators. Each year we have seen an increase in the amout of ewaste generated, while the recovery rate remained unchanged..</p>
                                <p>
                                    Consumer electronics contain valuable resources that can be reclaimed and processed into new products. Your e-waste is like an urban ore which is much cleaner to recover than mining the same natural resources from the earth. 
                                </p>
                                <p>
                                    MITS and our corporate partners are committed to helping consumers and businesses find recycling opportunities for their used electronics in ways that are safe, sustainable and environmentally sound. 
                                </p>
                            </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="sidebar">
                        <ul>
                            <li>
                                <h2>
                                    Manufacturer Solutions</h2>
                                <p>
                                    Our web-based software provides…</p>
                                <ul>
                                    <li><a href="../Home/ReadMore#section1">Clients Benefit from Economies of Scale </a>
                                    </li>
                                    <li><a href="../Home/ReadMore#section2">Real Time Status Reporting in Every State</a></li>
                                    <li><a href="../Home/ReadMore#section3">Detailed Source Documentation Eliminating Fraud</a>
                                    </li>
                                    <li><a href="../Home/ReadMore#section4">Customized Proprietary Software Common to Processors,
                                        Manufacturers and Administration</a></li>
                                    <li><a href="../Home/ReadMore#section5">On Demand Customer Service from Industry experts
                                    </a></li>
                                    <li><a href="../Home/ReadMore#section6">A Customized Mail Back System Compliant for 50
                                        States</a></li>
                                    <li><a href="../Home/ReadMore#section7">Listing of 2,400 Convenient Drop Off Locations</a></li>
                                    <li><a href="../Home/ReadMore#section8">Automated Calendar Detailing Key Events and Deadlines</a></li>
                                </ul>
                            </li>
                            <li>
                                <h2>
                                    Consumer Solutions</h2>
                               
                                <ul>
                                    <li>Mail back Program </li>
                                    <li>Convenient Drop Off Locations </li>
                                    <li>Recycling Events</li>
                                    <li>Retailer Locations</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
      
        <!-- end #page -->
  
   </asp:Content>
