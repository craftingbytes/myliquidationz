﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

   
    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>
               
       
                    <div id="content">
                        <div class="post">
                            <h2 class="title">
                                Frequently Asked Questions</h2>
                            <div class="entry">
                                <div class="Section1">
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            How do I recycle my electronics?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Depending on the state you reside in, E-World Online provides up to several options
                                            in order to make it as convenient as possible.<span style='mso-spacerun: yes'> </span>
                                            Our program offers a mail back program, collection events, permanent drop off sites
                                            and sponsored retailer drop off locations.<span style='mso-spacerun: yes'> </span>
                                            Please call our office at (760) 599-0888 and we will assist you in identifying which
                                            method is easier for you.<o:p></o:p></span></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            What items can I recycle through E-World Online?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Legislation in each state varies with respect to the definition of what a qualified
                                            device is.<span style='mso-spacerun: yes'> </span>In most cases the devices listed
                                            below qualify and the consumer should not have to pay for the recycling of those
                                            devices.<o:p></o:p></span></p>
                                    <p class="MsoNormal" style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;
                                        margin-left: .25in; text-indent: -.25in; mso-list: l0 level1 lfo1; tab-stops: list .5in'>
                                        <![if !supportLists]><span style='font-size: 10.0pt; mso-bidi-font-size: 11.0pt;
                                            font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol'><span
                                                style='mso-list: Ignore'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span></span></span><![endif]><span style='font-size: 11.0pt; font-family: "Calisto MT","serif";
                                                    mso-bidi-font-family: "Lucida Sans Unicode"'>Television Sets
                                                    <o:p></o:p>
                                                </span>
                                    </p>
                                    <p class="MsoNormal" style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;
                                        margin-left: .25in; text-indent: -.25in; mso-list: l0 level1 lfo1; tab-stops: list .5in'>
                                        <![if !supportLists]><span style='font-size: 10.0pt; mso-bidi-font-size: 11.0pt;
                                            font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol'><span
                                                style='mso-list: Ignore'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span></span></span><![endif]><span style='font-size: 11.0pt; font-family: "Calisto MT","serif";
                                                    mso-bidi-font-family: "Lucida Sans Unicode"'>Computer Monitors
                                                    <o:p></o:p>
                                                </span>
                                    </p>
                                    <p class="MsoNormal" style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;
                                        margin-left: .25in; text-indent: -.25in; mso-list: l0 level1 lfo1; tab-stops: list .5in'>
                                        <![if !supportLists]><span style='font-size: 10.0pt; mso-bidi-font-size: 11.0pt;
                                            font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol'><span
                                                style='mso-list: Ignore'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span></span></span><![endif]><span style='font-size: 11.0pt; font-family: "Calisto MT","serif";
                                                    mso-bidi-font-family: "Lucida Sans Unicode"'>Computers
                                                    <o:p></o:p>
                                                </span>
                                    </p>
                                    <p class="MsoNormal" style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;
                                        margin-left: .25in; text-indent: -.25in; mso-list: l0 level1 lfo1; tab-stops: list .5in'>
                                        <![if !supportLists]><span style='font-size: 10.0pt; mso-bidi-font-size: 11.0pt;
                                            font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol'><span
                                                style='mso-list: Ignore'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span></span></span><![endif]><span style='font-size: 11.0pt; font-family: "Calisto MT","serif";
                                                    mso-bidi-font-family: "Lucida Sans Unicode"'>Laptops
                                                    <o:p></o:p>
                                                </span>
                                    </p>
                                    <p class="MsoNormal" style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;
                                        margin-left: .25in; text-indent: -.25in; mso-list: l0 level1 lfo1; tab-stops: list .5in'>
                                        <![if !supportLists]><span style='font-size: 10.0pt; mso-bidi-font-size: 11.0pt;
                                            font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol'><span
                                                style='mso-list: Ignore'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span></span></span><![endif]><span style='font-size: 11.0pt; font-family: "Calisto MT","serif";
                                                    mso-bidi-font-family: "Lucida Sans Unicode"'>Printers
                                                    <o:p></o:p>
                                                </span>
                                    </p>
                                    <p class="MsoNormal" style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;
                                        margin-left: .25in; text-indent: -.25in; mso-list: l0 level1 lfo1; tab-stops: list .5in'>
                                        <![if !supportLists]><span style='font-size: 10.0pt; mso-bidi-font-size: 11.0pt;
                                            font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol'><span
                                                style='mso-list: Ignore'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span></span></span><![endif]><span style='font-size: 11.0pt; font-family: "Calisto MT","serif";
                                                    mso-bidi-font-family: "Lucida Sans Unicode"'>Computer Peripherals
                                                    <o:p></o:p>
                                                </span>
                                    </p>
                                    <p class="MsoNormal" style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;
                                        margin-left: .25in; text-indent: -.25in; mso-list: l0 level1 lfo1; tab-stops: list .5in'>
                                        <![if !supportLists]><span style='font-size: 10.0pt; mso-bidi-font-size: 11.0pt;
                                            font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol'><span
                                                style='mso-list: Ignore'>·<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span></span></span><![endif]><span style='font-size: 11.0pt; font-family: "Calisto MT","serif";
                                                    mso-bidi-font-family: "Lucida Sans Unicode"'>Cell Phones
                                                    <o:p></o:p>
                                                </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Who is paying for my electronics to be recycled under the E-World Online Program?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            In all states listed below except
                                            <st1:place w:st="on"><st1:State w:st="on">California</st1:State></st1:place>
                                            , E-World Online’s electronic manufacturer partners sponsor the processing of your
                                            end of life electronic device.<span style='mso-spacerun: yes'> </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='margin-top: 6.0pt; margin-right: 0in; margin-bottom: 6.0pt;
                                        margin-left: 0in; line-height: 16.8pt'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Hawaii, California, Oregon, Washington, Texas, Oklahoma, Illinois, Missouri, Michigan,
                                            Indiana, Minnesota, Wisconsin, Virginia, West Virginia, Rhode Island, Vermont, New
                                            Jersey, New York, Connecticut, North Carolina, South Carolina, Maryland, Maine.<o:p></o:p></span></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <b style='mso-bidi-font-weight: normal'><span style='font-size: 11.0pt; font-family: "Calisto MT","serif";
                                            mso-bidi-font-family: "Lucida Sans Unicode"'>E-World Online’s partners are Sony,
                                            Acer, ViewSonic, NEC and LG.<o:p></o:p></span></b></p>
                                    <br />
                                    <p class="MsoNormal" style='margin-top: 6.0pt; margin-right: 0in; margin-bottom: 6.0pt;
                                        margin-left: 0in; line-height: 16.8pt'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Which states prohibit throwing away electronics?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='margin-top: 6.0pt; margin-right: 0in; margin-bottom: 6.0pt;
                                        margin-left: 0in; line-height: 16.8pt'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Hawaii, California, Oregon, Washington, Texas, Oklahoma, Illinois, Missouri, Michigan,
                                            Indiana, Minnesota, Wisconsin, Virginia, West Virginia, Rhode Island, Vermont, New
                                            Jersey, New York, Connecticut, North Carolina, South Carolina, Maryland, Maine.<o:p></o:p></span></p>
                                    <p class="MsoNormal" style='margin-top: 6.0pt; margin-right: 0in; margin-bottom: 6.0pt;
                                        margin-left: 0in; line-height: 16.8pt'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            How can I be sure the products I recycle through E-World Online are handled correctly?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='margin-top: 6.0pt; margin-right: 0in; margin-bottom: 6.0pt;
                                        margin-left: 18.75pt; line-height: 16.8pt'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            E-World Online gives you access to a list of qualified recyclers who have been through
                                            a rigorous downstream audit sponsored by our corporate partners.&nbsp;&nbsp;Using
                                            these qualified recyclers, your electronics will be handled in a manner consistent
                                            with all applicable regulations and the best practices for the recycling industry.<o:p></o:p></span></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            How can I be sure my personal data is secure?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='margin-top: 6.0pt; margin-right: 0in; margin-bottom: 6.0pt;
                                        margin-left: 18.75pt; line-height: 16.8pt'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            We encourage you to clear any sensitive or confidential information off your product
                                            prior to disposing of your product through us.&nbsp;&nbsp; In addition, recyclers
                                            qualified through MITS use DOD (Department of Defense) standard data wiping procedures
                                            to erase all received hard-disks.
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <div id="sidebar">
                        <ul>
                            <li>
                                <h2>
                                    Manufacturer Solutions</h2>
                                <p>
                                    Our web-based software provides…</p>
                                <ul>
                                    <li><a href="../Home/ReadMore#section1">Clients Benefit from Economies of Scale </a>
                                    </li>
                                    <li><a href="../Home/ReadMore#section2">Real Time Status Reporting in Every State</a></li>
                                    <li><a href="../Home/ReadMore#section3">Detailed Source Documentation Eliminating Fraud</a>
                                    </li>
                                    <li><a href="../Home/ReadMore#section4">Customized Proprietary Software Common to Processors,
                                        Manufacturers and Administration</a></li>
                                    <li><a href="../Home/ReadMore#section5">On Demand Customer Service from Industry experts
                                    </a></li>
                                    <li><a href="../Home/ReadMore#section6">A Customized Mail Back System Compliant for 50
                                        States</a></li>
                                    <li><a href="../Home/ReadMore#section7">Listing of 2,400 Convenient Drop Off Locations</a></li>
                                    <li><a href="../Home/ReadMore#section8">Automated Calendar Detailing Key Events and Deadlines</a></li>
                                </ul>
                            </li>
                            <li>
                                <h2>
                                    Consumer Solutions</h2>
                                
                                <ul>
                                    <li>Mail back Program </li>
                                    <li>Convenient Drop Off Locations </li>
                                    <li>Recycling Events</li>
                                    <li>Retailer Locations</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
      
        <!-- end #page -->
  

</asp:Content>

