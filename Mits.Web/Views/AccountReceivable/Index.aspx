﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceholder" runat="server">
<div class="admin">
    <h1 class="heading">Search Account Receivable</h1>
    <table cellspacing="0" width="100%" style="margin-top: 5px">
        <tr>     
            <td align="center">
                <fieldset style="height: 180px; width: 350px">
                    <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                    <table cellspacing="0" cellpadding="5" style="margin-top:10px; margin-left:10px">
                        
                        <tr >
                            <td align="right">Received From :</td>
                            <td >
                                 <%= Html.DropDownList("ddlReceived", new SelectList((IList<Mits.DAL.EntityModels.Affiliate>)ViewData["receivables"], "Id", "Name")                                                          
                                 , new { style = "width:170px" })%>                                   
                            </td>     
                        </tr>
                        <tr>
                            <td align="right">
                                From :
                            </td>
                            <td>
                              <input type="text" id="txtFrom" name="txtFrom" 
                              style="width: 165px;" maxlength="200" />                                                                             
                            </td>     
                        </tr>
                        <tr>
                            <td align="right">
                                To :
                            </td>
                            <td >
                               <input type="text" id="txtTo" name="txtTo" 
                                style="width: 165px;" maxlength="200" />                                                                      
                            </td>     
                        </tr>
                         <tr>
                            <td>
                             
                            </td> 
                            <td align="left">
                            <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>    
                        </tr>
                         </table>
                         </fieldset>  
                        </td>
                    </tr>
      
         <tr>
        
        <td style="padding-left:20px;padding-top:6px;">
         <%-- <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
            { %>   --%> 
            <input type="button" id="btnAdd" name="btnAdd" value="Apply Payment" class="ui-button ui-widget ui-state-default ui-corner-all"/>
            <%--<%}
            else
            { %>--%>
               <%-- <input type="button" id="btnAdd" name="btnAdd" value="Add Invoice" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/>--%>
           <%-- <%} %>--%>
        </td>
        </tr>
        <tr></table>
         
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
        
</div>
<script src='<%=ResolveClientUrl("~/Scripts/forms/accountreceivable.index.js")%>' type="text/javascript"></script>

</asp:Content>

