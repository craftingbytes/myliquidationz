﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.AccountReceivableViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input[type=text], textarea, select
        {
            /*background-color: Transparent;*/
            margin: 3px 0 0 0;
            width: 200px;
        }
        select
        {
            width: 205px;
        }
        
        td
        {
            /* padding-left: 10px;*/
        }
        td.right
        {
            border-right: solid 1px black; /* padding-right: 30px;*/
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(../../images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../images/calander.gif);
        }
        .minus
        {
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Scripts/forms/accountreceivable.Edit.js"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <div class="admin">
      <h1 class="heading">  Account Receivable</h1>
   
    <table cellspacing="0" cellpadding="5" width="100%">
     
        <tr>
            <td style="padding-left: 130px;" colspan="2">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
            </td>
        </tr>
        <tr>
            <td >
                <table cellspacing="0" cellpadding="5">
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Received From :
                        </td>
                        <td>
                            <%if (Model.Payment.Id > 0)
                              { %>
                            <%=Html.DropDownList("Payment.AffiliateId", Model.Affiliates, "Select", new { required = "Received From is required.", onchange = "OnAffliateChange(this.value)", disabled = "disabled", style = "width:205px;" })%>
                            <%}
                              else
                              { %>
                            <%=Html.DropDownList("Payment.AffiliateId", Model.Affiliates, "Select", new { required = "Received From is required.", onchange = "OnAffliateChange(this.value)", style = "width:205px;" })%>
                            <%} %>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Date :
                        </td>
                        <td>
                            <%=Html.TextBox("Payment.Date", Model.Payment.Date.HasValue ? Model.Payment.Date.Value.ToString("MM/dd/yyyy") : "", new { required = "Date is required.", regexp = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$", regexpmesg = "Invalid Invoice Date format.", style = "width:200px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Payment Method :
                        </td>
                        <td>
                            <%--Cheque
                Cash
                Credit--%>
                            <%=Html.DropDownList("Payment.PaymentTypeID", Model.PaymentTypes, "Select", new { required = "Payment method is required.", style = "width:205px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Reference # :
                        </td>
                        <td>
                            <%=Html.TextBox("Payment.ReferenceNumber", null, new { required = "Refernce # is required.", style = "width:200px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="red">*</font> Amount :
                        </td>
                        <td>
                            <%=Html.TextBox("Payment.Amount", Model.Payment.Amount.GetValueOrDefault(), new { required = "Amount is required.", regexp = "^\\d*[1-9]\\d*(\\.\\d+)?$", regexpmesg = "Amount is not valid.", style = "width:200px;" })%>
                            USD
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Note :
                        </td>
                        <td>
                            <%=Html.TextArea("Payment.Note", null, 5, 25, new { style = "width:200px;"})%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td  colspan="4">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-right: 0px; padding-top: 20px;" colspan="4" align="right">
                <input type="hidden" id="txtPaymentDetails" name="txtPaymentDetails" />
                <%if (Model.Payment.Id == 0 && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                  { %>
                <input type="button" id="btnSubmit" onclick=" return submitForm();" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%}
                  else if (Model.Payment.Id > 0 && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.Value)
                  { %>
                <input type="button" id="btnSubmit" onclick=" return submitForm();" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%}
                  else
                  {%>
                <input type="button" id="btnSubmit" onclick=" return submitForm();" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled" />
                  
                <%} %>

                 <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                  { %>
                <input type="button" id="btnCreate" value="Apply Payment" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="javascript:window.location.href='/AccountReceivable/Edit'"/>
                <%}
                  else
                  { %>
                  <input type="button" id="btnCreate" value="Apply Payment" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="javascript:window.location.href='/AccountReceivable/Edit'" disabled="disabled" />
                <%} %>
            </td>
        </tr>
    </table>
    <div id="divMessage"></div> </div>
    <%if (!String.IsNullOrEmpty(Model.Message))
      {%>
      <script>
          ShowMessage('<%=Model.Message %>');
      </script>
    <%} %>
</asp:Content>
