﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.ServiceTypeViewModel>" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

     <link rel="Stylesheet" type="text/css" href="../../css/default.css" />
   <div class="admin">
     <h1 class="heading">Add/Edit Service Type</h1>
    
    <table style="margin-top:20px;">
        <tr> 
             <td colspan="2"  style="padding-left:277px;padding-bottom:10px;">
           <ul id="message" name="ulMessage" style="color:red; visibility:hidden">
           <li>
            <label id="lblMessage" style="visibility:visible; color:Red"></label>
            </li>
            </ul>
            </td>
        </tr>
        <tr height= "40px">
            <td align="right" width="300px">
                Select Service Type:
            </td>
            <td>
             <%= Html.DropDownList("ddlServiceTypes", Model.ServiceTypes  , new { style = "width:205px" }  )%> 
            </td>
        </tr>
        <tr height= "40px">
            <td align="right"  style="padding-top:10px">
                <span style='color:red'>*</span>Name :
            </td>
            <td  style="padding-top:10px">
                <input type="text" id="txtName" name="txtName" style="width: 200px;" maxlength="100" />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr height= "40px">
            <td align="right" valign="top"  style="padding-top:20px">
                Description :
            </td>
            <td  style="padding-top:20px">
                <textarea id="txtDesc" rows="4" name="txtDesc" style="height: 100px;
                    width: 200px;" onkeypress="return imposeMaxLength(this, 300);"></textarea>
            </td>
            <td>&nbsp;
                </td>
        </tr>
        
        <tr height= "30px">
            
            <td align="right" valign="top"  style="padding-top:20px">
          
                <span style='color:red'>*</span>Associated Groups :
            </td>
           
            <td  style="padding-top:20px">
          
            <table >
            
                 <% foreach (var groups in Model.Groups) { %> 
                 <tr>
                 <td>
                    <input type="checkbox" name="selectedObjects" id="<%=groups.Id%>" value="<%=groups.Id%>"> 
                      <%= groups.Name%>   </input>
                                          
                                          </td><%}%>
                                        </tr>
                </table>
            
            
            
            </td>
            
        </tr>
        <tr height= "40px">
            <td align="right">
            </td>
            <td>
                
                <br />
                <br />
                <input type="button" id="btnSubmit" name="btnSubmit" type="submit" value="Submit"   class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <input type="button" id="btnClear" name="btnClear" value="Clear" 
                class="ui-button ui-widget ui-state-default ui-corner-all"/>
            </td>
            <td>
            </td>
        </tr>
    </table>
 <div id="sidebar"></div>
   </div>
 
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("../../Scripts/js/jquery.validate.js")%>' type="text/javascript"></script> 
     <script src='<%=ResolveClientUrl("~/Scripts/forms/servicetype.index.js")%>' type="text/javascript"></script>
</asp:Content>

