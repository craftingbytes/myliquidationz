﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.AffiliateContactViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <table style="padding:0;">
        <tr>
            <td colspan="2" style="padding-top: 20px;">
                <h3 style="color: Gray; padding-left: 20px;">
                 <%=ViewData["Name"] %> Users </h3>
                    <input type="hidden" id="hdAffiliateTypeId" value='<%=ViewData["AffiliateType"] %>' />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-left: 20px; padding-top: 6px;">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-top: 10px;">
                <h3 style="color: Gray; padding-left: 20px;">
                    Add/Edit User</h3>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-left: 160px; padding-top: 10px;">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                    <% if (Model.Messages.Count > 0)
                       {%><ul class="ValidationSummary">
                           <%foreach (var message in Model.Messages)
                             {%>
                           <li>
                               <%=message.Text %></li>
                           <%} %>
                       </ul>
                    <%} %>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 70px;">
                <table style="margin-top: 6px;">
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>First Name :
                        </td>
                        <td>
                            <input type="text" id="FirstName" name="FirstName" value="<%= Model.AffiliateContact.FirstName%>"
                                style="width: 200px;" maxlength="50" required="First Name is required." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>Last Name :
                        </td>
                        <td>
                            <input type="text" id="LastName" name="LastName" value="<%= Model.AffiliateContact.LastName%>"
                                style="width: 200px;" maxlength="50" required="Last Name is required." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>Address 1 :
                        </td>
                        <td>
                            <input type="text" id="Address1" name="Address1" value="<%= Model.AffiliateContact.Address1%>"
                                style="width: 200px;" maxlength="100" required="Address1 is required." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            Address 2 :
                        </td>
                        <td>
                            <input type="text" id="Address2" name="Address2" value="<%= Model.AffiliateContact.Address2%>"
                                style="width: 200px;" maxlength="100" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>State :
                        </td>
                        <td>
                            <%= Html.DropDownList("StateId", Model.States, new { required = "State is required.", style = "width:205px" })%>
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>City :
                        </td>
                        <td>
                            <%= Html.DropDownList("CityId", Model.Cities, new { required = "City is required.", style = "width:205px" })%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>Zip :
                        </td>
                        <td>
                            <input type="text" id="Zip" name="Zip" value="<%= Model.AffiliateContact.Zip%>" style="width: 200px;"
                                maxlength="10" required="Zip is required." regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                                regexpmesg="Invalid Zip format, Please try xxxxx or xxxxx-xxxx." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right" align="right" valign="top" style="padding-top:3px;">

                            <span style='color: red'>*</span>Phone :
                        </td>
                        <td>
                            <input type="text" id="Phone" name="Phone" value="<%= Model.AffiliateContact.Phone%>"
                                style="width: 200px;" maxlength="20" required="Phone is required."  />
                              <%--  regexp="\(?\b[0-9]{3}\)?\b[-. ]?[0-9]{3}[-. ]?[0-9]{4}"
                                regexpmesg="Invalid Phone format, Please try (xxx)xxx-xxxx or xxx-xxx-xxxx."
                                <br />
                                <label style=" font-size:smaller; font-family:Verdana">Format: (xxx)xxx-xxxx or xxx-xxx-xxxx</label>--%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right" align="right" valign="top" style="padding-top:3px;">

                            Fax :
                        </td>
                        <td>
                            <input type="text" id="Fax" name="Fax" value="<%= Model.AffiliateContact.Fax%>" style="width: 200px;"
                                maxlength="20"  />
                                <%--regexp="\(?\b[0-9]{3}\)?\b[-. ]?[0-9]{3}[-. ]?[0-9]{4}"
                                regexpmesg="Invalid Fax format, Please try (xxx)xxx-xxxx or xxx-xxx-xxxx."
                                <br />
                                <label style=" font-size:smaller; font-family:Verdana">Format: (xxx)xxx-xxxx or xxx-xxx-xxxx</label>--%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>User Name :
                        </td>
                        <td>
                            <input type="text" id="UserName" name="UserName" value="<%= Model.AffiliateContact.UserName%>"
                                required="User Name is required." style="width: 200px;" maxlength="50" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 55px;">
                        <td align="right" valign="top" style="padding-top:10px;">

                            <span style='color: red'>*</span>Email :
                        </td>
                        <td  colspan="2">
                            <input type="text" id="Email" name="Email" value="<%= Model.AffiliateContact.Email%>"
                                style="width: 200px;" required="Email is required." maxlength="50" regexp="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                regexpmesg="Invalid Email format, Please try hello@mydomain.com." />
                                 <br />
                                <label style=" font-size:smaller; font-family:Verdana">Format: hello@mydomain.com</label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                     <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>Role :
                        </td>
                        <td>
                            <%= Html.DropDownList("RoleId", Model.Role, new { required = "Role is required.", style = "width:205px" })%>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>Password :
                        </td>
                        <td>
                           

                                <%
                                    string loginType = ViewData["LoginUser"].ToString();
                                  if (Model.AffiliateContact.Id > 0 && string.IsNullOrEmpty(loginType))
                          { %>
                            <input type="password" id="Password" name="Password" value="<%= Model.AffiliateContact.Password%>"
                                required="Password is required." style="width: 200px;" maxlength="14" disabled="disabled" />
                                <%}
                          else
                          { %>
                          <input type="password" id="Password" name="Password" value="<%= Model.AffiliateContact.Password%>"
                                required="Password is required." style="width: 200px;" maxlength="14" />
                                <%} %>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            <span style='color: red'>*</span>Confirm Password :
                        </td>
                        <td>
                        <% if (Model.AffiliateContact.Id > 0 && string.IsNullOrEmpty(loginType))
                          { %>
                            <input type="password" id="ConfirmPassword" name="ConfirmPassword" value="<%= Model.AffiliateContact.Password%>"
                                required="Confirm Password is required." style="width: 200px;" maxlength="14" disabled="disabled" />
                                <%}
                          else
                          { %>
                          <input type="password" id="ConfirmPassword" name="ConfirmPassword" value="<%= Model.AffiliateContact.Password%>"
                                required="Confirm Password is required." style="width: 200px;" maxlength="14" />
                                <%} %>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            Security Question :
                        </td>
                        <td>
                            <%= Html.DropDownList("SecurityQuestionId", Model.SecurityQuestions, new { style = "width:342px;text-align:left;z-index:auto;"})%>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            Answer :
                        </td>
                        <td>
                            <input type="text" id="Answer" name="Answer" value="<%= Model.AffiliateContact.Answer%>"
                                style="width: 335px;" maxlength="50" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="right">
                            Active :
                        </td>
                        <td>
                            <%if (Model.AffiliateContact.Active == true || Model.AffiliateContact.Active == null)
                              {%>
                            <%= Html.CheckBox("Active", true)%>
                            <%}
                              else
                              {%>
                            <%= Html.CheckBox("Active", false)%>
                            <%} %>
                            <!--<%= Html.CheckBox("Active", true)%> -->
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td style="padding-left: 0px; margin-left: 0px; width: 400px;" align="left" valign="top">
                <table style="margin-top: 6px; width: 400px;">
                    <tr align="left">
                        <td align="right" valign="top" style="width: 100px; padding-top: 8px;">
                           Service Types :
                        </td>
                        <td style="width: 300px; padding-left: 10px; padding-top: 4px;" valign="top" align="left">
                            <table>
                                <% foreach (var serviceType in Model.ServiceTypes)
                                   { %>
                                <tr>
                                    <td>
                                        <%bool present = false;
                                          foreach (var affiliateService in Model.AffiliateServices)
                                          {
                                              if (affiliateService.Id == serviceType.Id)
                                              {
                                                  present = true;
                                                  break;
                                              }
                                          }
                                          if (present == true)
                                          {
                                              bool selected = false;
                                              foreach (var affiliateContactSrv in Model.AffiliateContactServices)
                                              {
                                                  if (affiliateContactSrv.ServiceTypeId == serviceType.Id)
                                                  {
                                                      selected = true;
                                                      break;
                                                  }
                                              }
                                              if (selected == false)
                                              {
                                        %>
                                        <input type="checkbox" name="selectedObjects" value="<%=serviceType.Id%>">
                                            <%= serviceType.Name%>
                                        </input>
                                        <%}
                                              else
                                              { %>
                                        <input type="checkbox" name="selectedObjects" value="<%=serviceType.Id%>" checked="checked">
                                            <%= serviceType.Name%>
                                        </input>
                                        <%} %>
                                        <%}
                                          else
                                          {%>
                                        <input type="checkbox" name="selectedObjects" value="<%=serviceType.Id%>" disabled="disabled">
                                            <%= serviceType.Name%>
                                        </input>
                                        <%} %>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr align="left">
            <td align="left" style="padding-left: 190px; padding-top: 20px;">
                <%  
                    if (Model.AffiliateContact.Id > 0)
                    {
                        if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.Value)
                        { 
                %>
                <input id="btnSubmit" name="btnSubmit" type="submit" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%
                        }
                        else
                        {
                %>
                <input id="btnSubmit" name="btnSubmit" type="submit" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/>
                <%
                        }
                    }
                    else
                    {
                        if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                        { 
                %>
                <input id="btnSubmit" name="btnSubmit" type="submit" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%
                        }
                        else
                        {
                %>
                <input id="btnSubmit" name="btnSubmit" type="submit" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/>
                <%
                        }
                    }

                    if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                    { 
                %>
                <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%
                    }
                    else
                    {
                %>
                <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/>
                <%
                    }
                %>


                <%
          if((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
          {
              %>
          <input type="button" id="btnAdd" name="btnAdd" value="Add User" class="ui-button ui-widget ui-state-default ui-corner-all" />
          <%
                    }
                    else
                    {
                %>
                <input type="button" id="btnAdd" name="btnAdd" value="Add User" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/>
                <%
                    }
                 %>
            <input type="hidden" id="data" name="data" value='<%= ViewData["Data"] %>' />
            <input type="button" id="btnCancel" name="btnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="Cancel();"/>
            </td>
            <td align="left">
            </td>
        </tr>
    </table>
    <div id="dialog-ContactAssociate" title="Add Associated Users" style="display: none;">
        <table>
           <tr>
               <td style="width:140px"></td>
                <td>         
                <select id="AffiliateSelect" style="width:140px;">
                    <option></option>
                </select>
                </td>
                <td> 
                    <select id="AssociatedContactSelect" style="width:140px;">
                    <option></option>
                </select>
                </td>
                <td>
                    <input type ="button" id="btnAddAssociate" name="btnAddAssociate" value="Add" />
                    <input type="hidden" id="hdMasterContactId" name="hdMasterContactId" />
                </td>
           </tr>
        </table>    
        <table id="jqgridContactAssociate">
        </table>
    </div>
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/affiliatecontact.index.js")%>'
        type="text/javascript"></script>
</asp:Content>
