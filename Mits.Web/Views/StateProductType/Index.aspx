﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <table cellspacing="0" width="100%" cellpadding="5">
        <tr>
            <td style="padding-left: 20px; padding-top: 20px;">
                <h3 style="color: Gray">State Product Types</h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <fieldset style="height: 120px; width: 400px;">
                    <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                    <table style="width:300px; margin-top:10px">
                        <tr height="30">
                            <td align="right">State :</td>
                            <td align="left"> 
                                <%= Html.DropDownList("ddlState", (SelectList)ViewData["State"], new { style = "width:200px" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td  style="padding-top:10px" align="left">
                                <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                        </tr>
                    </table>
                </fieldset>            
            </td>
        </tr>
        <%if (ViewData["CanAdd"] == "")
        { %>
        <tr>
            <td style="padding-left: 20px;padding-top:20px;">
                <input type="button" id="btnAdd" value="Add State Product Type" class="ui-button ui-widget ui-state-default ui-corner-all" />
            </td>
        </tr>
        <%} %>
        <tr>
            <td style="padding-left: 20px;padding-top:0px;padding-right: 20px;">
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="canUpdate" name="canUpdate" value='<%=ViewData["CanUpdate"]%>' />
    <input type="hidden" id="canAdd" name="canAdd" value='<%=ViewData["CanAdd"]%>' />
    <input type="hidden" id="canDelete" name="canDelete" value='<%=ViewData["CanDelete"]%>' />
    <%--<script src="../../Scripts/js/jquery.fillSelect.js" type="text/javascript"></script>--%>
    <script src="../../Scripts/forms/stateproducttype.js" type="text/javascript"></script>
</asp:Content>

