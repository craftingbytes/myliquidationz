﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">


</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
   <div class="admin">
     <h1 class="heading"> Reconciliation</h1>
  
    <table style="margin-top:5px;margin-left:210px; width: 700px">

        <tr>
        <td>

        <fieldset style="height: 560px; width: 550px;">
        <legend style="padding: 5px;"><b>Weight Calculations</b></legend>
        <table cellpadding="3">
        <tr style="height:50px;"> 
           
            <td colspan="2"  style="padding-top: 40px; padding-left:150px">
                <div id="validationSummary" style="width:300px;" class="ValidationSummary">
               
                
                </div>
            </td>
        </tr>
        <tr height= "40px">
        
            <td align="right" class="style1">
                <span style='color:red'>*</span>Entity :
            </td>
           <td>                            
               <select id="EntityId" style="width:173px;" required="Entity is required">
                    <option></option>
                </select>
            </td>
            
        </tr>
        <tr height= "40px">        
            <td align="right">
                State :
            </td>
            <td>         
                <select id="StateId" style="width:173px;">
                    <option></option>
                </select>
            </td>            
        </tr>
         <tr height= "40px">        
            <td align="right" >
                Plan Period :
            </td>
            <td >
                <select id="PeriodId" style="width:173px;">
                    <option></option>
                </select> 
            </td>            
        </tr>
        <tr height= "40px">
            <td align="right">
                Obligation Weight :
            </td>
            <td>
                <input type="text" id="TargetWeight" name="TargetWeight" 
                    style="width: 165px;" maxlength="200" readonly="readonly"/>
            </td>
        </tr>
         <tr height= "40px">
            <td align="right" class="style1">
                Weight Assigned :
            </td>
            <td>
                <input type="text" id="WeightCollected" name="WeightCollected" 
                    style="width: 165px;" maxlength="200" readonly="readonly" title="OEM weight which has been reconciled" />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
         <tr height= "40px">
            <td align="right" class="style1">
                Remaining Obligation Weight :
            </td>
            <td>
                <input type="text" id="WeightNotCollected" name="WeightNotCollected" 
                    style="width: 165px;" maxlength="200" readonly="readonly" title="OEM weight which has not been reconciled yet" />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
         <tr height= "40px">
            <td align="right" class="style1">
                Processed Weight Available :
            </td>
            <td>
            <!--value="<%=ViewData["AvailableWeight"] %>"-->
                <input type="text" id="AvailableWeight" name="AvailableWeight" 
                    style="width: 165px;" maxlength="200"  value=""  readonly="readonly" 
                    title="All processors weight which has not been reconciled yet in state selected above." />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
         <tr height= "40px">
            <td align="right" class="style1">
               <span style='color:red'>*</span>Assign Weight % :
            </td>
            <td>
                <input type="text" id="AssignWeightPercentage" name="AssignWeightPercentage" 
                    style="width: 165px;" maxlength="200" required="Assign Weight % is required" regexp="^100(\.0{0,2}?)?$|^\d{0,2}(\.\d{0,2})?$" 
                    regexpmesg="Invalid Assign Weight % , please try number from 0-100 with upto 2 decimal places" 
                    title="Percentage of weight to be reconciled"  />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
         <tr height= "40px">
            <td align="right" class="style1">
                <span style='color:red'>*</span>Assign Weight :
            </td>
            <td>
                <input type="text" id="AssignWeight" name="AssignWeight" 
                    style="width: 165px;" maxlength="200" required="Assign Weight is required" regexp="^[0-9]+[0-9]*$" 
                    regexpmesg="Invalid Assign Weight, please try a positive number" 
                    title="Weight to be reconciled"/>
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr height="40px">
          
        
            <td align="right" class="style1">
            </td>
            <td>
            
                
                <br />
                <input type="button" id="btnSubmit" name="btnSubmit" type="submit" value="Submit"     class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <input type="button" id="btnClear" name="btnClear" value="Clear" 
                class="ui-button ui-widget ui-state-default ui-corner-all"/>
            </td>
            <td>
            </td>
        </tr>    
        </table>
    </fieldset> 
    
        </td></tr>     
       </table>
         
     </div>
 
   <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
 <script src='<%=ResolveClientUrl("~/Scripts/forms/reconciliation.index.js")%>' type="text/javascript"></script>
    
</asp:Content>

