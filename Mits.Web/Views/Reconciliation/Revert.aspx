﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceholder" runat="server">

    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
   <div class="admin">
     <h1 class="heading"> Revert</h1>
   </div>

   <table style="margin-top:5px;margin-left:30px; width: 700px" border=0>
   <tr height= "40px">
        
            <td style="width:20px">
                <span style='color:red'>*</span>Entity :
            </td>
           <td>                            
               <select id="EntityId"  required="Entity is required">
                    <option></option>
                </select>
            </td>
        </tr>
   </table>
   <table style="margin-top:5px;margin-left:30px; width: 700px" border=0>     
        <tr><td>
    <div id="div1">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
    </td>
    </tr>
    </table>
   <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>
 <script src='<%=ResolveClientUrl("~/Scripts/forms/reconciliation.revert.js")%>' type="text/javascript"></script>
</asp:Content>


