﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.ReconciliationViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <form id="form1" runat="server">
   <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
   <div class="admin" style="padding-bottom: 0px">
     <h1 class="heading">Reconciliation</h1>
   </div>
   <table style="margin-top:5px; margin-left:30px; width:920px" border="0">
   <tr height= "40px">        
    <td><table><tr>
            <td align="right">
                Affiliate :
            </td>
            <td>         
                <select id="OemAffiliate" style="width:173px;">
                    <option></option>
                </select>
            </td>
            <td align="right">
                State :
            </td>
            <td>         
                <select id="StateId" style="width:173px;">
                    <option></option>
                </select>
            </td>
            <td align="right">Plan Year:
            </td>
            <td >
              <select id="PlanYear" style="width:173px;">
              <option>Select</option>
               <option>2015</option>
               <option>2014</option>
               <option>2013</option>
               <option>2012</option>
               <option>2011</option>
               <option>2010</option>
            </select>
            </td>
            <td>Include Reconciled Invoices
            </td>
            <td>
                <input id="chkIncludeReconciled" type="checkbox" />
            </td>
            </tr></table></td>
        </tr>
  <tr>
   <td>
   <div id="div1">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
                <input type="hidden" id="hidTotalTargetWeight" value="0" />
   </td>
  </tr>
  
   <tr align="left">
     <td>
     <table style=" white-space:nowrap"><tr><td>
     <b>Available Processed Weight:</b></td><td width="100%"> <div id="AvailableTotal">0</div></td>
     <td ><b>Assigning Weight:</b></td><td ><div id="AssigningWeight" style="width:40px;">0</div></td>
     </tr>
     </table>
     </td>
   </tr>
   <tr>
    <td>
     <div id="div2">
                </div>
                <table id="jqGrid2">
                </table>
                <div id="pager2">
                </div>
    </td>
   </tr>
   <tr>
    <td><hr /></td>
   </tr>
   <tr>
    <td align="right">
        <table border=0 style=" white-space:nowrap" >
            <tr>
                <td width="100%" align="center">
                <div id="resultSummary" class="ValidationSummary"></div>
                </td>
                <td>
                 <input type="button"  id="btnSubmit" value="Reconcile" />
                </td>
            </tr>
        </table>
    </td>
   </tr>
   </table>

 <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
 <script src='<%=ResolveClientUrl("~/Scripts/forms/reconciliation.index2.js")%>' type="text/javascript"></script>
    </form>
</asp:Content>
