﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Login.Master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.LoginViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table cellspacing="0" cellpadding="50" border="0" width="100%" style="text-align:left;">
        <tr>
            <td valign="top" style="padding-right:30px;" width="50%">
                <p style="color:#605F5B;font-size:11pt;">
                    If you are one of our manufacturing partners or a downstream affiliate please log-in
                    to administer your national operation or view reports and summaries for your product
                    volume.
                    <br />
                    <br />
                    If you are interested in partnering with MITS please call(760) 599-0888 or email
                    us at<br />
                    <br />
                    <a href="mailto:info@e-worldonline.com?subject=Questions%20about%20MITS%20partnership" style="color:#FF9900;text-decoration:none;_text-decoration:none;">
                        info@e-worldonline.com</a>
                </p>
            </td>
            <td valign="top" width="50%" align="center">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2" class="loginTab">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table cellpadding="5" cellspacing="0" class="loginBox" width="100%" border="0">
                                <tr>
                                    <td colspan="2" height="37px" style="color:Red; text-align:left;">
                                        <div id="validationSummary" class="ValidationSummary">
                                            <% if (Model.Messages.Count > 0)
                                               {%><ul class="ValidationSummary">
                                       <%foreach (var message in Model.Messages)
                                         {%>
                                       <li>
                                           <%=message.Text %></li>
                                       <%} %>
                                   </ul>
                                            <%} %>
                                        </div>
                                    </td>
                                </tr>
                                <tr valign="bottom">
                                    <td align="right" style="color:#052C55;font-weight:bold;">
                                        User Name / E-mail:
                                    </td>
                                    <td align="left">
                                        <input type="text" id="UserName" name="UserName" value="<%= Model.UserName%>" style="width: 200px;"
                                            maxlength="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="color:#052C55;font-weight:bold;">
                                        Password:
                                    </td>
                                    <td align="left">
                                        <input type="password" id="Password" name="Password" value="<%= Model.Password%>"
                                            style="width: 200px;" maxlength="50" />
                                   
                                    </td>
                                    
                                </tr>
                                <tr valign="top">
                                    <td>
                                       
                                    </td>
                                    <td align="left">
                                    <input type="submit" style="background-image:url('../../Content/images/Login/btn_login.png');width:107px;height:35px;background-color:Transparent;border:none;" value="" />
                                    </td> 
                                </tr>
                                <tr>
                                <td>
                                </td>
                                <td align="left"> 
                                    <a href="/ResetPassword/Index" style="color:#FF9900;text-decoration:none;_text-decoration:none;">Forgot your Password?</a>
                                </td> 
                                </tr>
                                <%--TODO CB: Account registration was not accessable in the app as CB recieved it.
                                    Moved here for reference after refactoring of the LoginController into the AccountController. --%>
<%--                                <tr>
                                <td>
                                </td>
                                <td align="left"> 
                                    <a href="/Account/Register" style="color:#FF9900;text-decoration:none;_text-decoration:none;">Register if you don't have an account.</a>
                                </td> 
                                </tr>--%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="loginLeftBottom">
                        </td>
                        <td class="loginRightBottom">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
