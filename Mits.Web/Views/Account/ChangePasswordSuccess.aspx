﻿<%@Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage" %>


<asp:Content ID="changePasswordSuccessContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <h2>Change Password</h2>
    <p>
        Your password has been changed successfully.
    </p>
</asp:Content>
