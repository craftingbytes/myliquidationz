﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
        td.right
        {
            border-right: solid 1px black;
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(/Content/images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../Content/images/calander.gif);
        }
        .minus
        {
        }
        .input-validation-error
        {
            background-color: #FFEEEE;
            border: 1px solid #FF0000;
        }
        #fileInputUploader
        {
            width: 90px;
        }
    </style>
    <%--<link rel="Stylesheet" type="text/css" href='<%=ResolveClientUrl("~/Content/css/uploadify.css")%>' />--%>
    <%--<link rel="Stylesheet" type="text/css" href='<%=ResolveClientUrl("~/Content/css/default.css")%>' />--%>
    <%-- <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
<div class="admin">
    <h1 class="heading">Add Audit Report</h1>
    <table>
        <tr>
            <td></td>
            <td align="left" style="padding-top:20px" >                                
                <div id="validationSummary" style="width:300px; " class="ValidationSummary" ></div>                    
            </td>
        </tr>
        <tr>
            <td align="right" width="20%">
                <font color='red'>*</font>State :
            </td>
            <td width="400px">
                <%--<%= Html.DropDownList("ddlState", (SelectList)ViewData["States"], new { style = "width:205px", required = "State is required." })%> --%> 
                <%= Html.DropDownList("ddlState", (IEnumerable<SelectListItem>)ViewData["States"] as SelectList, new {multiple="multiple",style="text-align:left;z-index:auto;width:145px", required = "State is required." })%>
            </td>
        </tr>
        <tr>
            <td align="right" style="padding-top:10px">Facility Name :</td>
            <td align="left" style="padding-top:10px"> 
                <input type="text" id="txtFacilityName" name="txtFacilityName" size="100" style="width: 200px; display:none; border: none" required = "Facility is required."  />
                <%= Html.DropDownList("ddlFacilityName", (SelectList)ViewData["Facilitys"], new { style = "width:205px" })%>  
            </td>
        </tr>
        <tr>
            <td align="right" style="padding-top:10px">Plan Year :</td>
            <td align="left" style="padding-top:10px"> 
                <input type="text" id="txtPlanYear" name="txtPlanYear" size="10" style="width: 200px;"  required = "Plan Year is required." />
            </td>
        </tr>
        <tr>
            <td align="right" style="padding-top:10px">Audit Date :</td>
            <td align="left" style="padding-top:10px"> 
                <input type="text" id="txtAuditDate" name="txtAuditDate" size="10" style="width: 200px;" required = "Audit Date is required."  />
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" style="padding-top:17px">Upload Files :</td>
            <td align="left" style="padding-top:10px">
                <input id="fileInput" name="fileInput" type="file" />
                <input type="hidden" id="txtFileNames" name="txtFileNames" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="padding-top:25px">
                <input type="button" id="btnCreate" name="btnUpload" value="Create" class="ui-button ui-widget ui-state-default ui-corner-all" />&nbsp;&nbsp;
                <input type="button" id="btnAdd" name="btnAdd" value="Add" class="ui-button ui-widget ui-state-default ui-corner-all" />&nbsp;&nbsp;
                <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all"/>
            </td>
        </tr>
    </table>
</div>
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/swfobject.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.uploadify.v2.1.0.min.js")%>' type="text/javascript"></script>--%>
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/uploadifive.css")%>" />
    <script src="<%=ResolveClientUrl("~/Scripts/UploadiFive/jquery.uploadifive.js") %>" type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/auditreport.create.js")%>' type="text/javascript" ></script>
</asp:Content>
