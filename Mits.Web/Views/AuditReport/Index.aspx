﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/auditreport.index.js")%>' type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Audit Reports</h1>
        <table cellspacing="0" width="98%" style="margin-top: 5px">
            <tr>
                <td align="center">
                    <fieldset style="height: 190px; width: 400px;">
                        <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                        <table style="width:400px; margin-top:5px">
                            <tr>
                                <td align="right">State:</td>
                                <td align="left">
                                    <%= Html.DropDownList("ddlState", (SelectList)ViewData["States"], new { style = "width:205px" })%>  
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="padding-top:5px">Facility Name:</td>
                                <td align="left" style="padding-top:5px"> 
                                    <input type="text" id="txtFacilityName" name="txtFacilityName" size="100" style="width: 200px; display:none; border: none"  />
                                    <%= Html.DropDownList("ddlFacilityName", (SelectList)ViewData["Facilitys"], new { style = "width:205px" })%>  
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="padding-top:5px">Plan Year</td>
                                <td align="left" style="padding-top:5px"> 
                                    <input type="text" id="txtPlanYear" name="txtPlanYear" size="10" style="width: 200px;"  />
                                </td>
                            </tr>
 <%--                           <tr>
                                <td align="right" style="padding-top:5px">Audit Date From:</td>
                                <td align="left" style="padding-top:5px"> 
                                    <input type="text" id="txtAuditDateFrom" name="txtAuditDateFrom" size="10" style="width: 200px;"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="padding-top:5px">Audit Date To:</td>
                                <td align="left" style="padding-top:5px"> 
                                    <input type="text" id="txtAuditDateTo" name="txtAuditDateTo" size="10" style="width: 200px;"  />
                                </td>
                            </tr>--%>
                            <tr>
                                <td></td>
                                <td style="padding-top:10px" align="left">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <%if (ViewData["CanAdd"] == "")
            { %>    
            <tr>
                <td style="padding-top:6px;">
                    <input type="button" id="btnAdd" name="btnAdd" value="Create Audit Report" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                </td>
            </tr>
            <%} %>
            <tr>
                <td style="padding-top:3px;">
                    <div id="dialogDocuments" title="Audit Report Documents">
                    </div>
                    <div id="divSearch"></div>
                    <table id="jqGrid"></table>
                    <div id="pager"></div>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="canUpdate" name="canUpdate" value='<%=ViewData["CanUpdate"]%>' />
    <input type="hidden" id="canAdd" name="canAdd" value='<%=ViewData["CanAdd"]%>' />
    <input type="hidden" id="canDelete" name="canDelete" value='<%=ViewData["CanDelete"]%>' />
</asp:Content>


