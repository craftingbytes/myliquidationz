﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>


<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
   <div class="admin">
     <h1 class="heading"> Change Password</h1>
   
   
 <div style="padding-left:240px">
  <fieldset style="height: 380px; width: 500px; float: left; margin-top:20px">
        <legend style="padding: 5px;"><b>Criteria</b></legend>

    <table cellspacing="0" cellpadding="5" style="margin-top:10px; margin-left:50px">
         <tr>
            <td style="padding-left: 80px;" colspan="2">
                <div id="validationSummary" style="width: 300px;" class="ValidationSummary">
                </div>
            </td>
        </tr>
        <tr>
            <td align="right">
               <span style='color:red'>*</span>Old Password :
            </td>
            <td>
                <input type="password" id="txtOldPassword" name="txtOldPassword" 
                    style="width: 200px;" maxlength="14" required="Old Password is required" />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
       
         <tr >
            <td align="right">
               <span style='color:red'>*</span>New Password :
            </td>
            <td  >
                <input type="password" id="txtNewPassword" name="txtNewPassword" 
                    style="width: 200px;" maxlength="14"required="New Password is required" />
                    <br />
                    <label>Password length: 6 – 14 alphanumeric characters</label>
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
         <tr align="right">
            <td >
               <span style='color:red'>*</span>Confirm Password:
            </td>
            <td align="left" >
                <input type="password" id="txtConfirmPassword" name="txtConfirmPassword" 
                    style="width: 200px;" maxlength="14" required="Confirm Password is required" />
                     <br />
                    <label>Password length: 6 – 14 alphanumeric characters</label>
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td align="right" style="padding-left: 0px;width:200px;">
               <span style='color:red'>*</span>Security Question :
            </td>
            <td align="left">
                 <%= Html.DropDownList("ddlQuestion", (SelectList)ViewData["Question"], new { required = "Security Question is required.", style = "width:345px" })%>
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
         <tr >
            <td align="right">
               <span style='color:red'>*</span>Answer :
            </td>
            <td align="left" >            
                <%= Html.TextBox("txtAnswer", ViewData["Answer"],new {style="width: 338px;",required="Answer is required.",maxlength="500"})%>                                    
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            <br />
                <br />
                <input type="button" id="btnSubmit" name="btnSubmit" type="submit" value="Submit"     class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <input type="button" id="btnClear" name="btnClear" value="Clear" 
                class="ui-button ui-widget ui-state-default ui-corner-all"/>
            </td>
            <td>
            </td>
        </tr>
    </table>
    </fieldset>
   </div>  <div id="sidebar"></div>
   </div>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/changepassword.index.js")%>' type="text/javascript"></script>

</asp:Content>

