﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: #bbb 1px solid;
            width: 142px;
        }
        .disable
        {
            background-color: #DDDDDD;
        }
        input[type=text].ui-pg-input
        {
            width: auto;
        }
        select
        {
            width: auto;
        }
        .mt10
        {
            margin-top: 10px;
        }
        .ml10
        {
            margin-left: 10px;
        }
        div.head > div
        {
            width: 150px;
            float: left;
        }
        .cb
        {
            clear: both;
        }
        .fl
        {
            float: left;
        }
        .fr
        {
            float: right;
        }
        .style1
        {
            width: 70px;
        }
    </style>
    <link rel="Stylesheet" type="text/css" href="../../Content/css/cupertino/jquery-ui-1.8.2.custom.css" />
    <link rel="stylesheet" type="text/css" href="../../Content/css/ui.jqgrid.css" media="screen" />
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="mfs">
        <h2 class="title">
            Mail your electronics for recycling.</h2>
           <div id="page-bgtop">
          <div id="page-bgbtm">
                    <div id="content">
                        <div class="post">
        <!-- <p>
            Thank you for choosing to recycle.&nbsp;&nbsp;To get started, please enter your
            Zip code then select the manufacturer and type of item you wish to dispose off,
            below.&nbsp; This information will be used to find eligible devices that can
            be recycled in your area. You can add multiple items one by one.&nbsp;
            <br />
            <strong>Note:</strong> Your privacy is important to us. Your address will never
            be sold or disclosed to an unauthorized third party.
        </p>-->
        <p>E-World Online has provided an option for consumers to receive packaging for devices at no price in qualified areas.  To utilize this option, please follow the instructions below:</p> 
        <p>Please access the "MailBack" tab on www.e-worldonline.com.  A new screen will appear requesting your zip code.  Once you enter your zip code, an "Add Record" screen will appear.  One of the fields required on the "Add Record" screen is the dimensions of your device.  Please enter the dimensions, so we can better assist you with the proper size box to ship to you.</p> 
        <p>If you are not able to determine the dimensions of your device, please call our live customer service help line at (877) 342-6755.  We are available Monday thru Friday from 9 am to 5 pm PST.</p>
        <p>
            <strong>Note:</strong> Your privacy is important to us. Your address will never
            be sold or disclosed to an unauthorized third party.   
        </p>
        <fieldset>
            <legend><span class="noWrap">Please enter your zip code.&nbsp;</span>
                <img src="../../Content/images/icon/sm/help.gif" class="help" title="Location and Manufacturer"
                    alt="Help" onclick="showHelp('addressInfo');" />
            </legend>&nbsp;
            <table border="0" width="100%" cellspacing="2px">
             <tr>
                            <td colspan="2">
                                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                                </div>
                            </td>
                        </tr>

                <tr>
                    <td class="style1">
                        <span style="color: red">*</span> Zip Code :
                    </td>
                    <td>
                        <input type="text" id="txtZipCode" required="Zip Code is required." regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"  regexpmesg= "Invalid Zip format" maxlength="5"
                            class="dataInput4" value="<%=ViewData["Zip"]%>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="right fontSize2">
                        <%-- <img src="/images/icon/sm/help.gif" class="help" title="Mits Privacy Policy" alt="Help"
                        onclick="helpWindow(event)" />&nbsp; <a href="javascript:void(0);" class="help" title="Mits Privacy Policy"
                            onclick="helpWindow(event)">Privacy</a>--%>
                    </td>
                </tr>
            </table>
        </fieldset>
        <div class="fl mt10">
            <input type="button" id="btnSave" value="Save & Next" class="ui-button ui-widget ui-state-default ui-corner-all" /></div>
        <div class="fl mt10 ml10 ">
            <input type="button" id="btnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" /></div>
        <div class="fl mt10 ml10 ">
            <input type="button" id="btnAdd" value="Add" class="ui-button ui-widget ui-state-default ui-corner-all" /></div>
        <div class="cb">
        </div>
        <div class="cb">
        </div>
        <br />
        <table id="devices">
        </table>
        <div id="devPager">
        </div>
        <div id="noValidZip" style="display: none" title="Invalid or un supported Zip Code">
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">Invalid or un supported Zip code.<br />
                <br />
                Please enter a valid Zip Code. </span>
        </div>
        <div id="noData" style="display: none" title="Devices Add Error!">
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span id="spanMsg" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">Please enter atleast 1 Device to proceed.
            </span>
        </div>
        <div class="cb">
        </div>
        <input id="hfAddressInfo" type="hidden" />

        <input id="editOnLoad" type="hidden" />
        <input id="city" type="hidden" />
        <input id="state" type="hidden" />
        <input id="zip" type="hidden" />
    </div>
    <div id="sidebar">
				   <div class="twitter"><h2 ><a href="http://twitter.com/#!/EWorldOnline"> E-World on Twitter</a></h2></div>
<!--div class="facebook"> <h2> E-World on Facebook</h2></div--></div>
    <div id="addressInfo" style="display: none" title="Location and Manufacturer">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #464646;">Please
            enter a street address or physical location. This tool uses "smart" mapping software,
            but works best if the address is entered in a standard format such as: 123 Easy
            St. Dallas, TX 75211.
            <br />
            <br />
            Please enter your product's manufacturer into the manufacturer entry box. If a list
            appears you can choose the appropriate item from the list. Choosing the manufacturer
            of the product allows us to see if there any special services offered by this manufacturer
            or if your local area has any limitations. </span>
    </div>
      <script type="text/javascript">
          $("#editOnLoad").val(<%=ViewData["editOnLoad"].ToString().ToLower() %>);

          $("#city").val(<%=ViewData["City"].ToString().ToLower() %>);
          $("#state").val(<%=ViewData["State"].ToString().ToLower() %>);
          $("#zip").val(<%=ViewData["Zip"].ToString().ToLower() %>);

      </script></div></div></div></div>
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <script src="../../Scripts/forms/mailback.index.js" type="text/javascript"></script>
</asp:Content>
