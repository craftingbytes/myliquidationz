﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">


<table cellspacing="5px" border="0" style="width:97%;margin-left:30px">
    <tr>
        <td>
           <%if (Convert.ToBoolean(ViewData["MailbackSuccess"]) == true)
             { %>
             <h1>Thank you for using our mail back system.</h1><p>One of our customer service staff will contact you shortly.&nbsp;&nbsp;
                                  If you have any suggestions or comments, please contact us at <% =ViewData["fromEmail"] %> or call us at 877-342-6756.</p>
                                  
           <%}
             else
             {%>
             <h1>An Error has Occured</h1><p>We were unable to process your request at the moment. Please try agian later</p>
           <%} %>
            
            <br />
            <p class="middle"><img src="../../Content/images/layout/arrow_right_orange.jpg">&nbsp;To recycle another product&nbsp;&ndash;&nbsp;<a href="../DropOffLocations/Index" style="color:#FF9900;text-decoration:none;_text-decoration:none;">click here.</a></p>
            <p class="middle"><img src="../../Content/images/layout/arrow_right_orange.jpg">&nbsp;For a list of products we accept&nbsp;&ndash;&nbsp;<a href="../Home/FAQ" style="color:#FF9900;text-decoration:none;_text-decoration:none;">click here. </a></p>
            <br />
            <br />
        </td>
        <td align="left"><img src="../../Content/images/marquee/bamboo.jpg" /></td>
    </tr>
</table>

</asp:Content>

