﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table cellspacing="0" cellpadding="3px" border="0" style="margin-left: 50px">
      
        <tr>
            <td colspan="2">
                <h1>
                    Ship-Back Confirmation</h1>
                <p>
                    That's all we need to start the process!<br />
                    Thank you for taking the time to enter all the necessary information into our system.&nbsp;&nbsp;
                    Please make sure all the information is correct before continuing.
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset>
                    <legend>Confirm Information</legend>
                    <table cellspacing="0" cellpadding="3px" border="0" class="dataInput4" width="100%">
                        <tr>
                            <td>
                                Name :
                            </td>
                            <td>
                               <%=ViewData["Name"] %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address :
                            </td>
                            <td>
                                <%=ViewData["Address"] %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email :
                            </td>
                            <td>
                                <%=ViewData["Email"] %>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
            <input type="button" id="btnPrevious" value="Previous" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="javascript:window.location.href='/MailBack/ShippingAddress'" />
            </td>
            <td class="right">
                        <input type="button" id="btnSubmit" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="javascript:window.location.href='/MailBack/ShippingComplete'" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>

    <script src="../../Scripts/forms/mailback.shippingvalidate.js" type="text/javascript"></script>
</asp:Content>
