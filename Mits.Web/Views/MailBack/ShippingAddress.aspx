﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <table cellspacing="0" cellpadding="3px" border="0" style="margin-left: 50px">

        <tr>
            <td colspan="2">
                <h1>
                    Ship-Back Label</h1>
                <br />
                <p>
                    <strong>Note:</strong> Your privacy is important to us. Your address will never
                    be sold or disclosed to an unauthorized third party.
                </p>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 130px;" colspan="2">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset>
                    <legend>Shipping Information
                        <img src="../../Content/images/icon/sm/help.gif" class="help" title="Shipping Label Information"
                            alt="Help" onclick="showHelp('shippingInfo');" /></legend>
                    <div class="bgAliceBlue">
                        <table cellspacing="0" cellpadding="3px" border="0" class="dataInput4" width="100%">
                            <tr>
                                <td align="right" style="padding-right:10px">
                                    <span style='color: red'>*</span>First Name :
                                </td>
                                <td>
                                    <input name="txtFirstName" required="First name is required." value='<% =ViewData["FirstName"] %>'  />
                                </td>
                            </tr>
                            <tr>
                                    <td align="right" style="padding-right:10px">
                                    <span style='color: red'>*</span>Last Name :
                                </td>
                                <td>
                                    <input name="txtLastName" required="Last name is required." value='<% =ViewData["LastName"] %>' />
                                </td>
                            </tr>
                            <tr>
                                   <td align="right" style="padding-right:10px">
                                    <span style='color: red'>*</span>Address :
                                </td>
                                <td>
                                    <input name="txtAddress" required="Street address is required." value='<% =ViewData["Address"] %>' />
                                </td>
                            </tr>
                            <tr>
                                    <td align="right" style="padding-right:10px">
                                    <span style='color: red'>*</span>City :
                                </td>
                                <td>
                                    <input name="txtCity" value='<%=ViewData["City"] %>' required="City is required."  />
                                </td>
                            </tr>
                            <tr>
                                    <td align="right" style="padding-right:10px">
                                    <span style='color: red'>*</span>State :
                                </td>
                                <td>
                                    <input name="txtState" value='<%=ViewData["State"] %>' required="State is required." />
                                </td>
                            </tr>
                            <tr>
                                    <td align="right" style="padding-right:10px">
                                    <span style='color: red'>*</span>Zip :
                                </td>
                                <td>
                                    <input name="txtPostalCode" value='<%=ViewData["Zip"] %>' maxlength="5" required="Zip code is required."
                                        regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$" regexpmesg="Invalid Zip format, Please try xxxxx." />
                                        <br />
                                <label style=" font-size:smaller; font-family:Verdana">Format: xxxxx</label>
                                </td>
                            </tr>
                            <tr>
                                    <td align="right" style="padding-right:10px">
                                    <span style='color: red'>*</span>Phone :
                                </td>
                                <td>
                                    <input name="txtPhone" required="Phone number is required." regexp="\(?\b[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}\b"
                                        regexpmesg="Invalid Phone format, Please try (xxx)xxx-xxxx or xxx-xxx-xxxx." value='<%= ViewData["Phone"] %>'  />
                                        <br />
                                <label style=" font-size:smaller; font-family:Verdana">Format: (xxx)xxx-xxxx or xxx-xxx-xxxx</label>
                                </td>
                            </tr>
                            <tr class="padBottom4">
                                   <td align="right" style="padding-right:10px">
                                    <span style='color: red'>*</span>Email :
                                </td>
                                <td>
                                    <input name="txtEmail" required="Please enter your email address" regexp="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        regexpmesg="Invalid Email format, Please try hello@mydomain.com"  value='<%= ViewData["Email"] %>'/>
                                        <br />
                                    <label style=" font-size:smaller; font-family:Verdana">Format: hello@mydomain.com</label>
                                </td>
                            </tr>
                            <tr class="padBottom4">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <span class="floatRight">
                                        <img src="../../Content/images/icon/sm/help.gif" class="help" title="Mits Privacy Policy"
                                            alt="Help" onclick="showHelpPrivacy('privacyInfo');" />&nbsp; <a href="javascript:void(0);" class="help"
                                                title="Mits Privacy Policy" onclick="showHelpPrivacy('privacyInfo');">Privacy</a>
                                        <button type="submit" id="submitBtn" style="display: none;">
                                        </button>
                                    </span><span class="fontSize2 fontRed">*All fields are required for shipping.</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="btnPrevious" value="Previous" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="javascript:window.location.href='/MailBack/Index'"/>
            </td>
            <td class="right">
                <input type="button" id="btnNext" value="Next" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="javascript:ValidateInfo()" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="shippingInfo" style="display: none" title="Ship-Back Label Information">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #464646;">Please
            enter your name and address information to facilitate shipping your product back
            for recycling.&nbsp;&nbsp;All fields are required.&nbsp;&nbsp; Unit or Suite number
            information should follow the street address.MITS e-cycle is committed to protecting
            your personal information from misuse.Please see the privacy link below for more
            information. </span>
    </div>
    <div id="privacyInfo" style="display: none" title="Mits Privacy Policy">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #464646;">MITS
            e-cycle is committed to protecting your personal information from misuse.&nbsp;
            Any name or address data recorded on our website will be kept in the strictest confidence
            and will not be sold or otherwise disseminated to third parties not involved in
            the recycling activities listed on this website. </span>
    </div>
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <script src="../../Scripts/forms/mailback.shippingaddress.js" type="text/javascript"></script>
</asp:Content>
