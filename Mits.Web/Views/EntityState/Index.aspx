﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.EntityStateViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <link rel="Stylesheet" type="text/css" href="../../Content/css/default.css" />
   
    <h3 style="color: Gray; padding-top:20px;padding-left:20px">
             Entity States</h3>


    <table style="margin-top:20px;">
       <tr>
            <td colspan="2" style=" padding-top: 10px;">
             <div id="validationSummary" style="width: 300px; margin-left: 275px;" 
                    class="ValidationSummary">
             </div>
            </td>
        </tr>
        <tr height="40px">
        
            <td align="right" width="300px">
                Select Entity :
            </td>
            <td >
             <%= Html.DropDownList("ddlEntity", Model.Entity  , new { style = "width:200px", required="Please select an entity"}   )%>                                                          
                    
            </td>
            
        </tr>
      <tr height="40px">
            
            <td align="right" valign="top">
           <br />
                State :
            </td>
            <td align="left">
            <br />
            <table>
            
            <% var count = 0;
               foreach (var states in Model.States)
               {
                   bool present = false;
                   foreach (var entityState in Model.EntityStates) { if (entityState.StateId == states.Id) { present = true; } }
                   if (count % 2 == 0)
                   {%>
                    <tr>
                    <%}

                   if (present == true)
                   {%>
                           <td><input type="checkbox" name="selectedObjects" id="<%=states.Id%>" value="<%=states.Id%>" checked="checked"> <%= states.Name%> </input></td>
                        <%}
                   else
                   {%>
                             <td><input type="checkbox" name="selectedObjects" id="<%=states.Id%>" value="<%=states.Id%>"> <%= states.Name%> </input></td>
                          <%}


                   if (count % 2 != 0)
                   {%>
                    </tr>
                    <%}
                   count++;
               }%>                     
                   </table>  
            </td>
            
        </tr>
        
        
        <tr height="40px">
          
        
            <td align="right">
            </td>
            <td >
         
                <br />
                <br />
                <input type="hidden" id="data" name="data" value='<%= ViewData["Data"] %>' />                
                <input type="button" id="btnSubmit" name="btnSubmit" type="submit" value="Submit"     class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <input type="button" id="btnCancel" name="btnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="Cancel();"/>
            </td>
            <td>
            </td>
        </tr>
    </table>

    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
   <script src='<%=ResolveClientUrl("~/Scripts/forms/entitystate.index.js")%>' type="text/javascript"></script>
   
</asp:Content>

