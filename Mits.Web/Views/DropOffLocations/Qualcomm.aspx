﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style type="text/css">
        #tblResults td
        {
            padding-bottom: 10px;
            width: 33.3%;
        }
        #tblResults #dis
        {
            padding-left: 10px;
        }
        #tblResults th
        {
            padding-bottom: 10px;
            text-align: left;
            vertical-align: top;
        }
    </style>
   <div class="rs">
       <h2 class="title"> Recycle your electronics</h2>


                         <div id="page-bgtop">
          <div id="page-bgbtm">
                    <div id="content">
                        <div class="post">
              
                   
              
                <p>
                    <b>Thank you for choosing to recycle! This site is sponsored by Qualcomm.</b><br />
                    <br />
                    Please, enter your zip code in order to see a list of the closest drop off locations.
                    The drop-off locations listed on our website are for purposes of providing information
                    and convenience to our users. E-World Online is not necessarily associated with
                    these facilities and we encourage you to call them directly for the details of their
                    recycling programs. E-World Online does not warrant or make any guarantee regarding
                    price, procedures or general information pertaining to these locations. If you need
                    to report an inaccurate listing or have questions or comments, please call our corporate
                    office.
                </p>
          
                <fieldset style="width:300px;">
                    <legend><span class="noWrap">Please enter your zipcode</span>
                        <img src="../../Content/images/icon/sm/help.gif" class="help" title="Location and Manufacturer"
                            alt="Help" onclick="showHelp('addressInfo');" />
                    </legend>
                    <table border="0" width="100%" cellspacing="2px" class="dol">
                        <tr>
                            <td colspan="2">
                                <div id="validationSummary"  class="ValidationSummary">
                                </div>
                            </td>
                        </tr>
                        <tr>
                          
                             <td style="width: 150px;">
                                <input id="txtZipCode" name="txtZipCode" class="dataInput4" required="Zip Code is required."
                                    maxlength="5" />
                            </td>
                              <td align="left">
                                 <a id="submit" class="ui-button ui-widget ui-state-default ui-corner-all" 
                                href="javascript:populateAddressAndResults();">Submit</a>
                                
                            </td>
                        </tr>
                    </table>
                </fieldset>
         
                <h1 style="color: Gray; padding-left: 20px;">
                    Sponsored By:</h1>
     
            <div style="margin-top: 30px; font-size: 15px;" align="center">
                <h1 style="color: Gray; font-size: 25px; padding-left: 20px;">
                    QUALCOMM</h1>
    </div>
                &nbsp;<input id="hfA" type="hidden" />
      
   
    <div id="addressInfo" style="display: none" title="Shipping Information">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #464646;">
        Please enter a street address or physical location. This tool uses "smart" mapping
        software, but works best if the address is entered in a standard format such as:
        123 Easy St. Dallas, TX 75211. <br /><br /> Please enter your product's manufacturer into the
        manufacturer entry box. If a list appears you can choose the appropriate item from
        the list. Choosing the manufacturer of the product allows us to see if there any
        special services offered by this manufacturer or if your local area has any limitations.
        </span>
    </div>
  </div>   </div>
                    <!-- end #content -->
                <div id="sidebar">
                       <div class="twitter">
                                <h2>
                                 <a href="http://twitter.com/#!/EWorldOnline" target="_new"> E-World on Twitter</a><br>
</h2>
</div>
<!--div class="facebook">
                                <h2>
                                   E-world on Facebook    </h2>
</div-->
            </div>
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
                </div>
            </div></div></div>

    <div id="dlgDropOff" title="Drop Off Locations" class="hidden">
        <div style=" position:absolute; right:10px;">
            <img  src="../../Content/images/4color.gif" width="140"></div>
        <table id="tblResults" style="width: 100%" cellpadding="0" cellspacing="0">
        </table>
        <div style="margin-top: 10px;">
            Are our Dropoff Location too far?<br />
            Click <a href="javascript:MailBack();" style="color: #006699; cursor: pointer;">here</a>
            to use our MailBack Service.</div>
    </div>
  
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <script src="../../Scripts/forms/dropofflocations.qualcomm.js" type="text/javascript"></script>
</asp:Content>
