﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table cellspacing="0" cellpadding="5px" border="0">
        <tr>
            <td colspan="2">
                <h1>
                    Collection or Recycling facilities near You.</h1>
                <p>
                    MITS has found the following facilities that can help you dispose of your equipment.&nbsp;&nbsp;"
                    + "Your location and the nearest facilities are shown on the map.&nbsp;&nbsp;" +
                    "Click on the letter next to each entry for a closer look.<br />
                    <p>
                        <span class="fontRed">*</span>This recycler/collector is provided for your information
                        only. No warranties or representations of the quality or prices at this facility
                        is being made by MITS or their affiliates. It is recommended that you contact this
                        recycler/collector for any additional information about recycling your product prior
                        to delivery.
                    </p>
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset>
                    <legend>Area Map&nbsp;<img src="../../Content/images/icon/sm/help.gif" class="help"
                        title="Locations Map" alt="Help" onclick="showHelp('addressInfo');" /></legend>
                    <div class="clickable" id="ConsumerLocationMapContainer">
                        <br />
                        <strong>&nbsp;Loading map...</strong>
                    </div>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <a id="LinkToPreviousPage" href="Index" style="visibility:hidden">&lt;&lt; Previous </a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="googleMapDirections">
                    &nbsp;</div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="ErrorMessage"></asp:Label>&nbsp;
                <input id="hfLat" type="hidden" />
                <input id="hfLng" type="hidden" />
            </td>
        </tr>
    </table>

    <div id="addressInfo" style="display: none" title="Location Map">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #464646;">
     You location shows on the map as a green arrow. 
     The facilities that can help you properly dispose of your electronics have red markers with letters on them. 
     The letters correspond to list of facilities on the left. For directions, click on the letter in the left hand list.
     
     The map window is fully interactive. 
     Click on the map and drag to reposition it.   
     You can also move the sliding vertical control bar to zoom in and out and the arrows in the circle to move North South East and West.   
     The tabs at top right of the map window allow you to see the area streets or a satellite image. 
     </span>
    </div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script language="javascript" type="text/javascript">
      
            $("#hfLat").val(<%=Request["lat"]%>);
            $("#hfLng").val(<%=Request["lng"]%>);
          

    </script>
    <script src="../../Scripts/forms/dropofflocations.locationmap.js" type="text/javascript"></script>
</asp:Content>
