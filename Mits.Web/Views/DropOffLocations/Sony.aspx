﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/sony.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<div id="page-bgtop">
          <div id="page-bgbtm">
                    <div id="content">
                        <div class="post">
                          
                     
              <div class="entry center">
                <p>To get started recycling your old electronics, please enter  your zip code:<br />
                </p>
                <table cellspacing="0" cellpadding="0">
                  <tr>
                    <td><fieldset>
                      <legend>Please enter your Zip Code:  <img src="http://www.e-worldonline.com/Content/images/icon/sm/help.gif" title="Location and Manufacturer" alt="Help" onclick="showHelp('addressInfo');" /></legend>
                      
                      <table width="100%" border="0" cellspacing="2px">
                        <tbody>
                          <tr>
                            <td colspan="3"><div id="validationSummary"> </div></td>
                          </tr>
                          <tr>
                            <td>
                            </td>
                            <td style="width: 100px;">
                                <input id="txtZipCode" name="txtZipCode" class="dataInput4" required="Zip code is required."
                                    regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$" regexpmesg="Invalid Zip format, Please try xxxxx."
                                    maxlength="5" />
                            </td>
                            <td align="left">
                                &nbsp;&nbsp;&nbsp; 
                                 <a id="submit" class="ui-button ui-widget ui-state-default ui-corner-all" 
                                    href="javascript:populateAddressAndResultsForSony();">Submit</a>
                                 <!--input id="submit" class="ui-button ui-widget ui-state-default ui-corner-all"  
                                  name="submit" onclick="populateAddressAndResultsForSony()" value="submit" type="button" /--> 
                                    </td>
                          </tr>
                        </tbody>
                      </table>
                    </fieldset></td>
                  </tr>
                </table>
          
<p><br />
                                  <br />
                                  <br />
                                  <br />
                                  <br />
                        
<br />
</p>
</div>
                        </div>
                   
                    </div>
                    <!-- end #content --><!-- end #sidebar -->
                  <div style="clear: both;">
                        &nbsp;</div>
                </div>
            </div></div>
    <div id="dlgDropOff" title="Drop Off Locations" class="hidden" style=" position:relative">
        <div style=" position:absolute; right:10px;">
        <img  src="../../Content/images/4color.gif" width="140"></div>
        <div style=" margin-right:80px;"> 
            <table id="tblResults" style="width: 100%" cellpadding="0" cellspacing="0">
            </table>
        </div>
    </div>
    <div id="dlgMappView" class="hidden" title="Map Locations">
    <a href="javascript:closeMapLocations();" style="color: #006699; cursor: pointer;">&lt;&lt;
        Back to Drop Of Locations</a>
    </div>
    <div id="addressInfo" style="display: none" title="Location and Manufacturer">
    <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
    <span style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #464646;">Please
        enter a street address or physical location. This tool uses "smart" mapping software,
        but works best if the address is entered in a standard format such as: 123 Easy
        St. Dallas, TX 75211.
        <br />
        <br />
        Please enter your product's manufacturer into the manufacturer entry box. If a list
        appears you can choose the appropriate item from the list. Choosing the manufacturer
        of the product allows us to see if there any special services offered by this manufacturer
        or if your local area has any limitations. </span>
 
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <script src="../../Scripts/forms/dropofflocations.qualcomm.js" type="text/javascript"></script>
</asp:Content>
