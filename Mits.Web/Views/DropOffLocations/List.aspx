﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <h2></h2>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">



<div class="admin">
     <h1 class="heading" >
      Drop Off Locations
     </h1>
        <input id="hfshowdelbtn" type="hidden" />
    
    <table cellspacing="0" width="70%" style="margin-top: 5px;">
        <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td  align="center" >
                <br />
                <fieldset style="width: auto">
                    <legend>Search Criteria</legend>
                    <table style="width:100%">
                        <tr>
                            <td align="right">
                                Name:
                            </td>
                            <td align="left" class="style1">
                               <input type="text" id="txtName" name="txtName" />
                               <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                            <td align="left">
                                
                            </td>
                        </tr>
                    </table>
                    <br />
                </fieldset>
            </td>
        </tr>
        <tr>
        <td><input type="button" id="btnAdd" name="btnAdd" value="Create New Drop Off Location" class="ui-button ui-widget ui-state-default ui-corner-all"/></td>
        </tr>
        <tr>
              <td  align="center" >
                <div id="div1">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table></div>


     <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <%--<script src="../../Scripts/js/jquery.fillSelect.js" type="text/javascript"></script>--%>
    <script src="../../Scripts/forms/dropofflocations.list.js" type="text/javascript"></script>

    </asp:Content>