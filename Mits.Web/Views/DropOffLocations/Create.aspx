﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.DropOffLocationViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <h2></h2>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <h3 style="color: Gray; padding-left: 20px; padding-top: 20px; padding-bottom: 6px;">
        
        <%=ViewData["Name"] %></h3>
    <!--<% using (Html.BeginForm()) { %>-->
    <table>
        <tr>
            <td colspan="2" style="padding-left: 174px;">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                    <% if (Model.Messages.Count > 0)
                       {%><ul class="ValidationSummary">
                   <%foreach (var message in Model.Messages)
                     {%>
                   <li>
                       <%=message.Text %></li>
                   <%} %>
               </ul>
                    <%} %>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 120px;" valign="top">
                <table style="margin-top: 6px;">
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>DropOff Location Name :
                        </td>
                        <td>
                            <input type="text" id="Name" name="Name" value="<%= Model.DropOffLoc.Name%>" style="width: 200px;"
                                maxlength="50" required="Affiliate Name is required." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>Address 1 :
                        </td>
                        <td>
                            <input type="text" id="Address1" name="Address1" value="<%= Model.DropOffLoc.Address1%>"
                                style="width: 200px;" maxlength="100" required="Address1 is required." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            Address 2 :
                        </td>
                        <td>
                            <input type="text" id="Address2" name="Address2" value="<%= Model.DropOffLoc.Address2%>"
                                style="width: 200px;" maxlength="100" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>State :
                        </td>
                        <td>
                            <%= Html.DropDownList("StateId", Model.States, new { required = "State is required.", style = "width:205px" })%>
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>City :
                        </td>
                        <td>
                            <%= Html.DropDownList("CityId", Model.Cities, new { required = "City is required.", style = "width:205px" })%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right">
                            <span style='color: red'>*</span>Zip :
                        </td>
                        <td>
                            <input type="text" id="Zip" name="Zip" value="<%= Model.DropOffLoc.Zip%>" style="width: 200px;"
                                maxlength="10" required="Zip is required." regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                                regexpmesg="Invalid Zip format, Please try xxxxx or xxxxx-xxxx." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>Phone :
                        </td>
                        <td>
                            <input type="text" id="Phone" name="Phone" value="<%= Model.DropOffLoc.Phone%>" style="width: 200px;"
                                maxlength="20"/>
                            <%--regexp="\(?\b[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}\b"  regexpmesg= "Invalid Phone format, Please try (xxx)xxx-xxxx or xxx-xxx-xxxx."
                <br />
                                <label style=" font-size:smaller; font-family:Verdana">Format: (xxx)xxx-xxxx or xxx-xxx-xxxx</label>--%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            Fax :
                        </td>
                        <td>
                            <input type="text" id="Fax" name="Fax" value="<%= Model.DropOffLoc.Fax%>" style="width: 200px;"
                                maxlength="20" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right" valign="top" style="padding-top: 3px;">
                            Email :
                        </td>
                        <td colspan="2">
                            <input type="text" id="Email" name="Email" value="<%= Model.DropOffLoc.Email%>" style="width: 200px;"
                                 maxlength="50" regexp="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                regexpmesg="Invalid Email format, Please try with correct format." />
                               <%-- required="Email is required."--%>
                            <br />
                            <label style="font-size: smaller; font-family: Verdana">
                                Format: hello@mydomain.com</label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right" valign="top" style="padding-top: 3px;">
                            Web Site :
                        </td>
                        <td colspan="2">
                            <input type="text" id="WebSite" name="WebSite" value="<%= Model.DropOffLoc.WebSite%>"
                                style="width: 200px;" maxlength="50" regexp="^(((h|H?)(t|T?)(t|T?)(p|P?)(s|S?))\://)?(www.|[a-zA-Z0-9].)[a-zA-Z0-9\-\.]+\.[a-zA-Z]*$"
                                regexpmesg="Invalid web site url format, Please try again." />
                            <br />
                            <label style="font-size: smaller; font-family: Verdana">
                                Format: www.mydomain.com</label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right">
                            Longitude :
                        </td>
                        <td colspan="2">
                            <input type="text" id="Longitude" name="Longitude" value="<%= Model.DropOffLoc.Longitude%>"
                                style="width: 200px;" maxlength="50"  />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right">
                            Latitude :
                        </td>
                        <td colspan="2">
                            <input type="text" id="Latitude" name="Latitude" value="<%= Model.DropOffLoc.Latitude%>"
                                style="width: 200px;" maxlength="50"  />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right">
                            Business Hours :
                        </td>
                        <td colspan="2">
                            <textarea name="BusinessHours" id="BusinessHours" style="width: 200px;" rows = "3"><%= Model.DropOffLoc.BusinessHours%></textarea>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td style="padding-left: 50px;" align="left" valign="top">
             <table style="margin-top: 6px;">
             <tr>
                 <td align="right" valign="top" style="width: 100px; padding-top: 4px;">
                        Location Types :
                 </td>
                 <td>
                    <table valign="top">
                            <% foreach (var type in Model.LocationTypes)
                                { %>
                            <tr>
                                <td valign="top">
                                    <%bool present = false;
                                        foreach (var locationType in Model.DropOffLocationTypes)
                                        {
                                            if (locationType.TypeId == type.Id)
                                            {
                                                present = true;
                                            }
                                        }
                                        if (present == true)
                                        {%>
                                    <input type="checkbox" name="selectedTypes" value="<%=type.Id%>" checked="checked">
                                        <%= type.LevelName%>
                                    </input>
                                    <%}
                        else
                        {%>
                                    <input type="checkbox" name="selectedTypes" value="<%=type.Id%>">
                                        <%= type.LevelName%>
                                    </input>
                                    <%} %>
                                </td>
                            </tr>
                            <%}%>
                        </table>
                 </td>
                </tr>
                <tr>
                    <td align="right" valign="top" style="width: 100px; padding-top: 4px;">
                        Device Types :
                    </td>
                    <td>
                            <table valign="top">
                            <% foreach (var device in Model.DropOffDevices)
                                { %>
                            <tr>
                                <td valign="top">
                                    <%bool present = false;
                                        foreach (var locationDevice in Model.DropOfflocationDevices)
                                        {
                                            if (locationDevice.DeviceId == device.Id)
                                            {
                                                present = true;
                                            }
                                        }
                                        if (present == true)
                                        {%>
                                    <input type="checkbox" name="selectedObjects" value="<%=device.Id%>" checked="checked">
                                        <%= device.DeviceName%>
                                    </input>
                                    <%}
                        else
                        {%>
                                    <input type="checkbox" name="selectedObjects" value="<%=device.Id%>">
                                        <%= device.DeviceName%>
                                    </input>
                                    <%} %>
                                </td>
                            </tr>
                            <%}%>
                        </table>
                    </td>
                </tr>
                <tr>
                        <td align="right" >
                            Active :
                        </td>
                        <td valign="top" style="padding-left:4px">
                            <%if (Model.DropOffLoc.Active == true || Model.DropOffLoc.Active == null)
                              {%>
                            <%= Html.CheckBox("Active", true)%>
                            <%}
                              else
                              {%>
                            <%= Html.CheckBox("Active", false)%>
                            <%} %>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
              </table>
            </td>
        </tr>
        <tr align="left">
         <td align="center" style="padding-left: 205px; padding-top: 20px; padding-bottom:30px;" colspan="2">
         <input type="hidden" id="data" name="data" value='<%= ViewData["Data"] %>' />      
           <input id="btnSubmit" name="btnSubmit" type="button" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" />
           <input type="button" id="btnBack" name="btnBack" value="Back" class="ui-button ui-widget ui-state-default ui-corner-all" />
         </td>
        </tr>
       
    </table>
    <!--<% } %>-->
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/dropofflocations.edit.js")%>' type="text/javascript"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>


</asp:Content>
