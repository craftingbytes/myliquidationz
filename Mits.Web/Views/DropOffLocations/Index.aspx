﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div style="padding-left:40px;width:640px;">
  
  <br />
    <p><strong>E-World Preferred Partners  <img src="/content/images/symbol.png" height="20px" width="20px" />:</strong>
    <br /> E-World Online manages a network of preferred partners across the country consisting of collectors, recyclers and drop off locations. These preferred partners have a direct relationship with E-World Online.  E-World Online shares responsibilities with preferred partners and endorses methods of collection, processing and downstream.  If convenient, please take your devices to an E-World Online Preferred Partner.   </p>
    <p><strong>Consumer Information:</strong> All drop off locations provided without the <img src="/content/images/symbol.png" height="20px" width="20px" /> are for purposes of consumer education only.  E-World Online is not associated with these facilities and, although we provide general information,  we encourage you to call them directly for the details regarding recycling programs. E-World Online does not warrant or make any guarantee regarding price, procedure, or general information pertaining to these locations.  </p>
     <p>Begin by selecting your state &nbsp; <%= Html.DropDownList("StateId", (SelectList)ViewData["States"], new { required = "State is required.", style = "width:180px" })%></p>
    <!-- <p style="color:Red"><strong>Michigan:</strong> For Michigan residents click <a href="/DropoffLocations/Michigan" style="color: #006699; cursor: pointer;">here</a>!</p> -->
    <p></p>
 </div>
 <iframe name="frameEarth911" id="frameEarth911" src="http://manage.earth911.com/recycling-locator/251071731b35d3c8/" width="980" height="600" scrolling="no" frameborder="0" marginwidth="10"style="background-color:#BABABA;"></iframe> 
 <iframe name="frameMichigan" id="frameMichigan" src="http://manage.earth911.com/recycling-locator/6f9eb28c2d1c5042/" width="980" height="600" scrolling="no" frameborder="0" marginwidth="10"style="background-color:#BABABA;"></iframe> 
 <script type="text/javascript">
     $(document).ready(function () {
         $('#frameEarth911').hide();
         $('#frameMichigan').hide();

         $('select[id$=StateId]').change(function (elem) {

             state = $("select#StateId").val();
             if (state == "27") {
                 $('#frameEarth911').hide();
                 $("#frameMichigan").attr('src', 'http://manage.earth911.com/recycling-locator/6f9eb28c2d1c5042/');
                 $('#frameMichigan').show();

             }
             else if (state == "0") {
                 $('#frameEarth911').hide();
                 $('#frameMichigan').hide();
             }
             else {
                 $('#frameMichigan').hide();
                 $("#frameEarth911").attr('src', 'http://manage.earth911.com/recycling-locator/251071731b35d3c8/');
                 $('#frameEarth911').show();
             }

         });
     });
</script>
</asp:Content>


