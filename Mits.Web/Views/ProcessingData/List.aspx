﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <%--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />

    <script src="../../Scripts/js/ui.dropdownchecklist.js" type="text/javascript"></script>--%>
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <div class="admin">
        <h1 class="heading">Search Processing Report</h1>
        <table width="100%" style="margin-top: 20px;">
            <tr>
                <td align="center">
                    <br />                                                                                                                                                                            
                    <fieldset style="width: 80%">
                    <legend><b>Search Criteria</b></legend>
                    <table style="margin-top:10px">
                        <tr>
                            <td align="right">
                                Enter Text Here to Search:<br />
                            </td>
                            <td>
                                <input type="text" id="txtSearch" size="30" style="width:140px;" />
                                <span style="font-size: 9px">(Client,Received By etc.)</span>
                            </td>
                            <td align="right">
                                State:
                            </td>
                            <td>
                                <%= Html.DropDownList("states", (IEnumerable<SelectListItem>)ViewData["statesList"] as SelectList, new {multiple="multiple",style="text-align:left;z-index:auto;width:145px;" })%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Beginning Date:
                            </td>
                            <td align="left">
                                <input type="text" id="txtBeginDate" class="date-pick" style="width:140px;" />
                            </td>
                            <td align="right">
                                Ending Date:
                            </td>
                            <td>
                                <input type="text" id="txtEngingDate" class="date-pick" style="width:140px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" style="padding-top: 10px; padding-left: 10px;">
                                <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                            
                        </tr>
                    </table>
                    <br />
                    </fieldset>
                    <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <input type="button" id="btnAddPd" name="btnAddPd" value="Add Processing Report"  class="ui-button ui-widget ui-state-default ui-corner-all" />
        <br />
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
           
    </div>
    <div id="dialog" title="Invalid Dates!" style="display: none;">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
            font-size: 14px; color: #464646;">Begining Date cannot be bigger than Ending Date.</span>
    </div>

  <div id="dialogFiles" title="Attached files" style="display: none;">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
            font-size: 14px; color: #464646;">
            <ul id="ulFiles">
           
            </ul>
            </span>
    </div>
    <input id="hfIsAdmin" name="hfIsAdmin"  type="hidden" />
    <input id="hfIsOem" name="hfIsOem"  type="hidden" />

       
		
    <script type="text/javascript">
    
        $("#hfIsAdmin").val('<%=ViewData["IsAdmin"] %>');
        $("#hfIsOem").val('<%=ViewData["IsOEM"] %>');

    </script>
    <script src="../../Scripts/forms/ProcessingData.List.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input[type=text], textarea, select
        {
            /*background-color: Transparent;*/
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 142px; 
        }
        select
        {
            width: 145px; 
        }
        
        td
        {
           /* padding-left: 10px;*/
        }
        td.right
        {
            border-right: solid 1px black;
           /* padding-right: 30px;*/
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            /*//background: white url(../../images/calendar.gif) no-repeat scroll 97% center;*/
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../images/calander.gif);
        }
        .minus
        {
        }
    </style>
</asp:Content>
