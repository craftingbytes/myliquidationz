﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master"  Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.ProcessingDataViewModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
    <script type="text/javascript">


    </script>
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <%--<script src="../../Scripts/js/jquery.fillSelect.js" type="text/javascript"></script>--%>
    <script src="../../Scripts/forms/ProcessingData.Edit.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        /* jqModal base Styling courtesy of;
	Brice Burgess <bhb@iceburg.net> *//* The Window's CSS z-index value is respected (takes priority). If none is supplied,
	the Window's z-index value will be set to 3000 by default (via jqModal.js). */
        input[type=text], textarea, select
        {
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 100px;
            _width: 100px;
        }
        .disable
        {
            background-color: #DDDDDD;
        }
        input[type=text].ui-pg-input
        {
            width: auto;
        }
        select
        {
            width: auto;
        }
        .mt10
        {
            margin-top: 10px;
        }
        .ml10
        {
            margin-left: 10px;
        }
        div.head > div
        {
            width: 150px;
            float: left;
        }
        .cb
        {
            clear: both;
        }
        .fl
        {
            float: left;
        }
        .fr
        {
            float: right;
        }
        .style1
        {
            width: 372px;
        }
        .style2
        {
            width: 93px;
        }
        .style3
        {
            width: 128px;
        }
        .style4
        {
            width: 78px;
        }
        .style5
        {
            width: 77px;
        }
        .style6
        {
            width: 75px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div style="display: <%=ViewData.ContainsKey("excp") ? "block": "none"%>">
        An error Occured while processing your request. Please try again later.<br />
    </div>
    <div class="admin">
      <h1 class="heading">Processing Report</h1>
    <table cellpadding="3" width="100%" border="0">
        <tr>
            <td style="padding-left: 130px;" colspan="2">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: middle; width: 50%;" align="center" >
            <div class="txtblock">
                <label id="Address">
                    <%=ViewData["CompName"]%><br />
                    <%=ViewData["StreetAddress"]%>,<br />
                    <%=ViewData["CityState"]%><br />
                    Phone:
                    <%=ViewData["Phone"]%><br />
                    <%=ViewData["Email"]%>
                </label></div>
            </td>
            <td style="width: 50%;">
                <table cellpadding="3" align="left" style="margin-left:65px">
                    <tr>
                        <td align="right">
                            <b>Processing Date:</b>
                        </td>
                        <td>
                            <input type="text" id="ProcessingDate" name="ProcessingDate" value="<%=Model.ProcessingData.ProcessingDate.HasValue ? Model.ProcessingData.ProcessingDate.Value.ToShortDateString() : "" %>"  req="true" required="Processing Date is required." />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b><font color="red">*</font> Plan Year :</b>
                        </td>
                        <td>
                            <select id="ddlPlanYear" name="ddlPlanYear" req="true" required="Plan Year is required." style="width:104px">
                            <option value="">Select</option>
                            <option value="2015">2015</option>
                            <option value="2014">2014</option>
                            <option value="2013">2013</option>
                            <option value="2012">2012</option>
                            <option value="2011">2011</option>
                            <option value="2010">2010</option>          
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>Comments:</b>
                        </td>
                        <td>
                            <textarea id="Comments" name="Comments" rows="5" style="width: 280px;" cols="20"
                                name="S1"><%=Model.ProcessingData.Comments%> </textarea>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <fieldset style="float: left; height: 220px; width: 48%;">
        <legend style="padding: 5px;"><b>Client Address</b></legend>
        <table cellpadding="3" style="width: 100%">
            <tr>
                <td align="right">
                    <label for="txtAddress">
                         <b>&nbsp;</b>
                    </label>
                </td>
                <td >
                    &nbsp;
                </td>
                <td align="right">
                    <label for="txtAddress">
                         <font color='red'>*</font><b>Company/Person Name :</b>
                    </label>
                </td>
                <td >
                    <input type="text" id="VendorCompanyName" name="VendorCompanyName" value="<%=Model.ProcessingData.VendorCompanyName%>" req="true" required="Company Name is required." style="width:100px" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="txtAddress">
                         <font color='red'>*</font><b>Address :</b>
                    </label>
                </td>
                <td>
                    <input type="text" id="VendorAddress" name="VendorAddress" value="<%=Model.ProcessingData.VendorAddress%>" req="true" required="Vendor Address is required." style="width:100px" />
                </td>
                <td align="right">
                    <label for="txtState">
                         <font color='red'>*</font><b>State :</b>
                    </label>
                </td>
                <td>
                    <%= Html.DropDownList("VendorStateId", (SelectList)ViewData["VendorStates"], new { style = "width:104px", onchange = "GetVendorCities();", required = "Vendor State is required." })%>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="txtCity">
                         <font color='red'>*</font><b>City :</b>
                    </label>
                </td>
                <td>
                    <%--<input type="text" id="txtVendorCity" name="txtVendorCity" req="true" />--%>
                    <%= Html.DropDownList("VendorCityId", (SelectList)ViewData["VendorCities"], new { style = "width:104px", required = "Vendor City is required." })%>
                </td>
                <td align="right">
                    <label for="txtZip">
                        <b>Zip:</b>
                    </label>
                </td>
                <td>
                    <input type="text" id="VendorZip" name="VendorZip" value="<%=Model.ProcessingData.VendorZip%>" maxlength="5" regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                        regexpmesg="Invalid Zip format, Please try xxxxx."  style="width:100px" />
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    <label for="txtPhone">
                        <b>Phone:</b>
                    </label>
                </td>
                <td class="style19">
                    <input type="text" id="VendorPhone" name="VendorPhone" value="<%=Model.ProcessingData.VendorPhone%>" style="width:100px"  regexp="^\d{3}-\d{7}$" regexpmesg="Invalid Phone format, Please try xxx-xxxyyyy."/>
                    <br /><label style="font-size: smaller; font-family: Verdana">xxx-xxxyyyy</label>
                </td>
            </tr>
        </table>
        <div class="cb">
        </div>
    </fieldset>
    <fieldset style="height: 240px; width: 48%; float: left; margin-left:15px; _margin-left:5px;">
        <legend style="padding: 5px;"><b>Pick Up/Ship From</b></legend>
        <table cellpadding="3" align="right">
            <tr>
                <td align="right">
                    <label for="txtAddress">
                         <b>&nbsp;</b>
                    </label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td align="right">
                    <label for="txtPickUpCompanyName">
                         <font color='red'>*</font><b>Company/Person Name :</b>
                    </label>
                </td>
                <td>
                    <input type="text" id="PickUpCompanyName" name="PickUpCompanyName"  value="<%=Model.ProcessingData.PickUpCompanyName%>" req="true" required="Pick Up Company Name is required." style="width:100px" /
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="txtAddress">
                        <font color='red'>*</font><b>Address :</b>
                    </label>
                </td>
                <td>
                    <input type="text" id="PickupAddress" name="PickupAddress" value="<%=Model.ProcessingData.PickupAddress%>" req="true" required="Pickup Address is required."  style="width:100px" />
                </td>
                <td align="right">
                    <label for="txtState">
                         <font color='red'>*</font><b>State :</b>
                    </label>
                </td>
                <td>
                    <%= Html.DropDownList("ddlPickupState", (SelectList)ViewData["PickupStates"], new { style = "width:104px", required = "Pickup State is required." })%>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="txtCity">
                         <font color='red'>*</font><b>City :</b>
                    </label>
                </td>
                <td>
                    <%--  <input type="text" id="txtPickupCity" name="txtPickupCity" req="true" />--%>
                    <%= Html.DropDownList("PickupCityId", (SelectList)ViewData["PickUpCities"], new { style = "width:104px", required = "Pickup City is required." })%>
                </td>
                <td align="right">
                    <label for="txtZip">
                         <font color='red'>*</font><b>Zip :</b>
                    </label>
                </td>
                <td>
                    <input type="text" id="PickupZip" name="PickupZip" value="<%=Model.ProcessingData.PickupZip%>" req="true" 
                        required="Pickup Zip is required." regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$" regexpmesg="Invalid Zip format, Please try xxxxx or xxxxx-xxxx."  style="width:100px" />
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    <label for="txtPhone">
                        <font color='red'>*</font><b>Phone:</b>
                    </label>
                </td>
                <td valign="top">
                    <input type="text" id="PickupPhone" name="PickupPhone" value="<%=Model.ProcessingData.PickupPhone%>" regexp="^\d{3}-\d{7}$" regexpmesg="Invalid Phone format, Please try xxx-xxxyyyy." req="true" required="Phone No is required." style="width:100px" />
                    
                </td>
                <td align="right" valign="top">
                    <label for="txtFax">
                        <b>Fax:</b>
                    </label>
                </td>
                <td valign="top">
                    <input type="text" id="PickupFax" name="PickupFax" value="<%=Model.ProcessingData.PickupFax%>" regexp="^(\+\d)*\s*(\(\d{3}\)\s*)*\d{3}(-{0,1}|\s{0,1})\d{2}(-{0,1}|\s{0,1})\d{2}$"
                        regexpmesg="Invalid Fax format, Please try xxxxx."  style="width:100px" /><br />
                </td>
            </tr>
            <tr>
                <td></td><td></td>
                <td align="right">
                    <label for="ddlWeightCategories">
                        <b>Weight Category:</b>
                    </label>
                </td>
                <td>
                    <%= Html.DropDownList("ddlWeightCategories", (SelectList)ViewData["WeightCategories"], new { style = "width:104px"})%>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="txtPickupEmail">
                        <b>Email:</b>
                    </label>
                </td>
                <td>
                    <input type="text" id="PickupEmail" name="PickupEmail" value="<%=Model.ProcessingData.PickupEmail%>"  style="width:100px" />
                </td>
                <td align="right">
                    <label for="ddlCollectionMethod">
                        <font color='red'>*</font><b>Collection Method :</b></label>
                </td>
                <td>
                    <%= Html.DropDownList("CollectionMethodId", (SelectList)ViewData["CollectionMethods"], new { style = "width:104px", required = "Collection Method is required." })%>
                </td>
            </tr>
        </table>
        <div class="cb">
        </div>
    </fieldset>
    <table cellpadding="5" style="width: 100%">
       
        <tr>
            <td align="right">
                <b>Client:</b>
            </td>
            <td>
                <input type="text" id="Client" name="Client" value="<%=Model.ProcessingData.Client%>"  style="width:100px;"/>
            </td>
            <td align="right">
                <font color='red'>*</font> <b>Received By :</b>
            </td>
            <td>
                <input type="text" id="RecievedBy" name="RecievedBy" value="<%=Model.ProcessingData.RecievedBy%>"  req="true" required="Received By is required." style="width:100px;"/>
            </td>
            <td>
            </td>
            <td align="right">
                 <font color='red'>*</font><b>Shipment # :</b>
            </td>
            <td>
                <input type="text" id="ShipmentNo" name="ShipmentNo" value="<%=Model.ProcessingData.ShipmentNo%>"  req="true" required="Shipment No is required." style="width:100px;"/>
            </td>
            <td align="right">
                 <b>Processed #:</b>
            </td>
            <td>
                <input type="text" id="InvoiceId" name="InvoiceId" value="<%=Model.ProcessingData.InvoiceId%>" readonly="readonly" style="width:100px;" />
            </td>
        </tr>
    </table>
    <div style="width: 95%; margin-left:-7px; ">
        <br />
        <input type="button" id="btnAdd" value="Add" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <input type="button" id="btnExcel" value="Import From Excel" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        <br />
        <input type="button" id="btnReapplyRates" value="Re-Apply Rates" class="fr ui-button ui-widget ui-state-default ui-corner-all" />
        <input type="button" id="btnSubmit" value="Submit" class="fr ui-button ui-widget ui-state-default ui-corner-all" />
        <br />
    </div>
    <div id="dialog-changestate" title="Confirm Pickup Change!" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">Changing Pickup State will delete all your item entries.<br />
                <br />
                Do you want to proceed ?</span>
        </p>
    </div>
  </div>
  <input type="hidden" id="Id" name="Id" value="<%=Model.ProcessingData.Id%>" />
  <!--<input type="hidden" id="VendorCityId" name="VendorCityId" value="<%=Model.ProcessingData.VendorCityId%>" />-->
  <!--<input type="hidden" id="VendorStateId" name="VendorStateId" value="<%=Model.ProcessingData.VendorStateId%>" />-->
  <!--<input type="hidden" id="PickupCityId" name="PickupCityId" value="<%=Model.ProcessingData.PickupCityId%>" />-->
  <input type="hidden" id="PickupStateId" name="PickupStateId" value="<%=Model.ProcessingData.PickupStateId%>" />
  <input type="hidden" id="StateWeightCategoryId" name="StateWeightCategoryId" value="<%=Model.ProcessingData.StateWeightCategoryId%>" />
  <!--<input type="hidden" id="CollectionMethodId" name="CollectionMethodId" value="<%=Model.ProcessingData.CollectionMethodId%>" />-->
  <input type="hidden" id="PlanYear" name="PlanYear" value = "<%=Model.ProcessingData.Invoice.PlanYear%>" />
  <input type="hidden" id="AffiliateTypeId" name="AffiliateTypeId" value="<%=ViewData["AffiliateTypeId"]%>" />
</asp:Content>
