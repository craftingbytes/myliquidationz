﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <h2></h2>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />

    <script src="../../Scripts/js/ui.dropdownchecklist.js" type="text/javascript"></script>
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <h1 class="heading"  >
       Detailed Processing Report</h1>
        
    <table width="100%" style="margin-top: 20px;">
    
        <tr>
            <td align="center">
                <br />
                <fieldset style="width: 80%">
                    <legend><b>Report Criteria</b></legend>
                    <table>
                        <tr>
                           <td align="right">
                            Plan Year :
                        </td>
                        <td align="left">
                            <select id="ddlPlanYear" name="ddlPlanYear" req="true" required="Plan Year is required." style="width:140px">
                            <option value="">All</option>
                            <option value="2015">2015</option>
                            <option value="2014">2014</option>
                            <option value="2013">2013</option>
                            <option value="2012">2012</option>
                            <option value="2011">2011</option>
                            <option value="2010">2010</option>
                            </select>
                        </td>
                        <td></td>
                            <td align="right">
                                State:
                            </td>
                            <td>
                            <%= Html.DropDownList("StateId", (SelectList)ViewData["statesList"], new { required = "State is required.", style = "width:180px" })%>                     
                              
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Beginning Date:
                            </td>
                            <td align="left">
                                <input type="text" id="txtBeginDate" class="date-pick" style="width:140px;" />
                            </td>
                            <td></td>
                            <td align="right">
                                Ending Date:
                            </td>
                            <td align="left">
                                <input type="text" id="txtEngingDate" class="date-pick" style="width:140px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" style="padding-top: 20px; padding-left: 10px;">
                                <input type="button" id="btnReport" name="btnReport" value="Report" onclick="GenerateReport()"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                        </tr>
                    </table>
                    <br />
                </fieldset>
                <br />
            </td>
            <td>
            </td>
        </tr>
        
    </table>
    <div id="dialog" title="Invalid Dates!" style="display: none;">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
            font-size: 14px; color: #464646;">Begining Date cannot be bigger than Ending Date.</span>
    </div>

  <div id="dialogFiles" title="Attached files" style="display: none;">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
        <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
            font-size: 14px; color: #464646;">
            <ul id="ulFiles">
           
            </ul>
            </span>
    </div>
    <input id="hfIsAdmin" name="hfIsAdmin"  type="hidden" />
    <input id="hfIsOem" name="hfIsOem"  type="hidden" />

       
		
    <script type="text/javascript">

        $("#hfIsAdmin").val('<%=ViewData["IsAdmin"] %>');
        $("#hfIsOem").val('<%=ViewData["IsOEM"] %>');

    </script>
    <script src="../../Scripts/forms/ProcessingData.Report.js" type="text/javascript"></script>
    <script src="../../Scripts/forms/ProcessingData.List.js" type="text/javascript"></script>

</asp:Content>
