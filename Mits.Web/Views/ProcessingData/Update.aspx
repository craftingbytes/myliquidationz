﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 142px;
        }
        input[type=text].ui-pg-input
        {
            width: auto;
        }
        select
        {
            width: 145px;
        }
        tr
        {
            width: 100%;
        }
        td
        {
            padding-left: 10px;
        }
        td.right
        {
            border-right: solid 1px black;
            padding-right: 30px;
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(../../images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../images/calander.gif);
        }
        .minus
        {
        }
        .input-validation-error
        {
            background-color: #FFEEEE;
            border: 1px solid #FF0000;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   
    <table title="" style="margin-top:20px; margin-left:100px;">
        
        <tr>
            <td style="padding-left: 130px;" colspan="3">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
            </td>
        </tr>

        <tr>
            <td align="right" width="30%">
                Recyler:
            </td>
            <td>
                <label id="Address">
                    E-Word Recyclers<br />
                </label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                Processing Date:
            </td>
            <td>
                <%=ViewData["ProcessingDate"] %>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                Invoice #:
            </td>
            <td>
                <%=ViewData["InvoiceId"] %>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                Shipment #:
            </td>
            <td>
                <%=ViewData["ShipmentNo"] %>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                Comments*:
            </td>
            <td>
                <textarea id="txtComments" rows="3" name="txtComments" required="Comments are required." style="height: 39px; width: 318px;"></textarea>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                Reason To Update*:
            </td>
            <td>
                <textarea id="txtReason" rows="3" name="txtReason" required="Reason is required." style="height: 39px; width: 318px;"></textarea>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
                <input type="button" id="btnUpdate" name="btnUpdate" value="Request For Update"  class="ui-button ui-widget ui-state-default ui-corner-all" />
            </td>
            <td>
                <input id="Admin" type="hidden" value=" <%=ViewData["Admin"] %>" />
                <input id="Id" type="hidden" value="<%=ViewData["Id"] %>" />
                <input id="InvoiceId" type="hidden" value="<%=ViewData["InvoiceId"] %>" />
                <input id="ProcessingDate" type="hidden" value="<%=ViewData["ProcessingDate"] %>" />
            </td>
        </tr>
    </table>
    <div id="dialog-choice" title="Confirm Request! " style="display: none;">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">You are about to Send an Update Request.<br />
                <br />
                Are you sure you want to proceed ?</span>
        </p>
    </div>
    <script src="../../Scripts/forms/Validation.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {

            $("button, input:submit, input:button").button();

            $("td:contains(*)").each(
                function () {
                    $(this).html(
                        $(this).html().replace('*', "<span style='color:red'>*</span>")
                    );
                }
            );

            $("#btnUpdate").click(function () {

                var isValid = ValidateForm(); //true;

                if (isValid == true) {
                    $("#dialog-choice").dialog({
                        modal: true,
                        width: 400,
                        resizable: false,
                        position: 'center',
                        buttons: {
                            "No": function () {
                                $(this).dialog("close");
                            },
                            "Yes": function () {
                                var invoiceId = $("#InvoiceId").val();
                                var processingDate = $("#ProcessingDate").val();
                                //Request["InvoiceId"]
                                $("form").attr("action", "/ProcessingData/Update?Id=" + $("#Id").val() + "&InvoiceId=" + invoiceId + "&ProcessingDate=" + processingDate);
                                $("form").submit();

                                $(this).dialog("close");
                            }
                        }
                    });
                }

            });

        });

                        
    </script>

</asp:Content>
