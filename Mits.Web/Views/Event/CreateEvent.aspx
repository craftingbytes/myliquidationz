﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/mits.master" Title="MITS - Items & Address "
    Inherits="System.Web.Mvc.ViewPage" %>



<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

     <link rel="Stylesheet" type="text/css" href='<%=ResolveClientUrl("~/Content/css/uploadify.css")%>' />
   

    <style type="text/css">



  
em { font-weight: bold; padding-right: 1em; vertical-align: top; }

        input[type=text], textarea, select
        {
           /* background-color: Transparent;*/
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 142px;
        }
        input[type=text].ui-pg-input
        {
            width: auto;
        }
        select
        {
            width: 145px;
        }
        tr
        {
            width: 100%;
        }
        
        td.right
        {
    
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(../../Content/images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(../../Content/images/calander.gif);
        }
        .minus
        {
        }
        .pad
        {
            padding-left:120px;
            }
    </style>

     <link href="../../Content/css/tree.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .demo
        {
            float: left;
            width: 260px;
        }
        .docs
        {
            margin-left: 265px;
        }
        a.button
        {
            font-size: 0.8em;
            margin-right: 4px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   
    <input type="hidden" value="" name="EventId" id="EventId" />
    <div class="admin">
        <h1  class="heading">
        Create Event</h1>
    
    <div style="width:100%; margin-top:15px;  text-align:right; ">
    Load From Template : 
    <%= Html.DropDownList("ddlTemplate", (SelectList)ViewData["Templates"], new { style = "width:600px" })%>
          
              <%--  <input type="button" id="btnLoad" name="btnLoad" value="Load Template" />--%>
    </div>
    
    <div class="error" style="color:Red;">
    </div>
    
 <fieldset>
                    <legend><b>Event Scheduling</b></legend>
    

    <table cellspacing="0" style="" id="mainTable">
    
  
    <tr>
 
            <td ><b>Event Info</b>
           
            </td>
            <td > 
            </td>
          
            <td style="padding-left:23px;" >
              <b>Reccurence Info</b>   
              <input type="radio" id="rbtnNone" name="rbtnRec" checked="checked" value="None" />None  
            </td> 
            <td >
           
            </td>
            
            
        </tr>
        <tr>
            <td >
                <span style='color:red'>*</span>Title :
            </td>
            <td >
                <input type="text" id="txtTitle" name="txtTitle"  maxlength="100" required="Title is required." style="width: 331px" />
            </td>
            <td class="pad">
                <input type="radio" id="rbtnSingleRec" name="rbtnRec" value="Single Recurrence" />Single Recurrence :
            </td>
            <td>
                <input id="txtSingleRecur" readonly="readonly" name="txtSingleRecur" class="date-pick" />
            </td>
        </tr>
        <tr>
            <td >
                <span style='color:red'>*</span>Date :
            </td>
            <td  >
                <input id="txtDate" name="txtDate" readonly="readonly" required="Date is required." style="width: 331px" onchange="txtDateChange(this)" class="date-pick" />
            </td>
            <td  class="pad">
                <input type="radio" id="rbtnEvery" name="rbtnRec" value="Every" />Every :
            </td>
            <td>
                <input type="text" id="txtEveryDays" name="txtEveryDays" maxlength="2" style="width: 30px" />
                Days
            </td>
        </tr>
        <tr>
            <td >
               <span style='color:red'>*</span> State :
            </td>
            <td  >
               <%= Html.DropDownList("ddlStates", (SelectList)ViewData["States"], new { style = "width: 335px;" , required="State is required."})%>
            </td>
            <td  class="pad">
                <input type="radio" id="rbtnMonthly" name="rbtnRec"  value="Monthly On" />Monthly On 
            </td>
            <td>
                <input type="text" id="txtMonthly" name="txtMonthly" readonly="readonly" style="width: 30px" maxlength="2" />
                (e.g. 10, 15)
            </td>
        </tr>
        <tr>
            <td >
                <span style='color:red'>*</span>Type :
                            </td>
            <td >
               <%= Html.DropDownList("ddlEventType", (SelectList)ViewData["EventTypes"], new { style = "width: 335px;", required = "Type is required." })%>
            </td>
            <td  class="pad"> 
                <input type="radio" id="rbtnQuaterly" name="rbtnRec"  value="Quarterly On" />Quarterly On :
            </td>
            <td>
                <input type="text" id="txtQuaterly"  maxlength="2" name="txtQuaterly" readonly="readonly" style="width: 30px" />
            </td>
        </tr>
        <tr>
            <td >
                Description :
            </td>
            <td rowspan="2" >
                 <textarea id="txtDescription" rows="3" name="txtDescription"    maxlength="300" style="height: 39px;
                    width: 331px;"></textarea>
            </td>
            <td valign="top"  class="pad">
                <input type="radio" id="rbtnYearly"  name="rbtnRec" value="Yearly On" /> Yearly On :
            </td>
            <td valign="top">
                <input id="txtYearly" name="txtYearly"  readonly="readonly" style="width: 140px" />
            </td>
        </tr>
        <tr>
       
       <td>
       </td>
      
            <td valign="top" align="right" style="padding-right:58px;" > Ends On:
                
            </td>
            <td valign="top"  >
            
                <input type="text" id="txtEndsOn" readonly="readonly" name="txtEndsOn" class="date-pick" />
            </td>
        </tr>
        <tr>
            <td >
            </td>
            
            <td>
            </td>
            
            <td valign="top" align="right" style="" >
           
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="border-top: solid 1px White;">
            </td>
        </tr>
        <tr>
            <td colspan="4" >
            </td>
        </tr>
        <tr>
        <td colspan="4" align="right" >
           
        <input type="button" name="btnSave" id="btnSave" value="Save" 
                style="width: 60px;"  class="ui-button ui-widget ui-state-default ui-corner-all" />
        <input type="button" style="margin-left: 4px;" id="btnCal"  value="Show Calendar" onclick="document.location.href=$('#calUrl').attr('href')"  class="ui-button ui-widget ui-state-default ui-corner-all" />
              <a id="calUrl" href="<%= Html.AttributeEncode(Url.Action("ViewEvents", "Event")) %>" style="display:none;"></a>
        <input type="button" id="btnSaveTemplate" name="btnSaveTemplate" style="margin-left: 4px;"
            value="Save As Template"  class="ui-button ui-widget ui-state-default ui-corner-all" />
            
    <input type="button" id="btnDel" name="btnDel" style="margin-left: 4px;"
            value="Delete"  class="ui-button ui-widget ui-state-default ui-corner-all" />



        </td>
        </tr>
        <tr>
            <td colspan="6">
                
            </td>
        </tr>
    </table>
 
                    <b></b>
 
</fieldset>

<br />

<fieldset id="pnlReminder"  style="display:none;">
<legend><b>Reminders</b> </legend> 
<div  title="Reminders" >
                    <br />
                    <input type="button" id="AddMember" value="Add Reminder" onclick="AddChildRecord()"  class="ui-button ui-widget ui-state-default ui-corner-all"  />
                    <table id="jqGrid">
                    </table>
                    <div id="pager" style="text-align: center;">
                    </div>
                </div>
</fieldset>


    <div id="treePopup" style="width: 350px; height: 350px; display: none;">
       
        <div id="tree" style="width: 350px; height: 330px; overflow: auto;">
        </div>
        <div>
            <input type="submit" id="btnOK" name="btnOK" value="Add Members"  class="ui-button ui-widget ui-state-default ui-corner-all" />
        </div>
       
    </div>




    <div id="filePopup" style="width: 350px; height: 350px; display: none;">
        <input id="fileInput" name="fileInput" type="file" />
        <input type="button" id="btnUpload" name="btnUpload" value="Upload"  class="ui-button ui-widget ui-state-default ui-corner-all"  />
        <input type="button" id="btnClear" name="btnClear" value="Clear"  class="ui-button ui-widget ui-state-default ui-corner-all"  />
    </div>
    

    <div id="delPopup" style="width: 350px; height: 350px; display: none;">
       
       The Event and all its reminders will be deleted. Delete this event?
       <br />   <br />
       <div style="margin-left:150px;">
        <input type="button" id="btnYes" name="btnYes" value="Yes"  class="ui-button ui-widget ui-state-default ui-corner-all" />
        <input type="button" id="btnNo" name="btnNo" value="No"  class="ui-button ui-widget ui-state-default ui-corner-all" />
        </div>
    </div>
    
    <div id="popup" style="width: 320px; height: 320px; display: none;">
        <fieldset style="overflow: auto;">
            <legend>
                <strong>
                    Files List</strong>
            </legend>
            <ul id="files">
            </ul>
        </fieldset>
        <p>
            *Click on the file name to download.
        </p>
        <input style="margin-left: 200px;" type="button" id="btnPopup" name="btnPopup" value="Close"
            class="ui-button ui-widget ui-state-default ui-corner-all" />
    </div>

    <div id="emailPopup" style="width: 350px; height: 500px; display: none;">
       
       <table align="center">
       <tr>
           <td style="text-align:center">
               <%= Html.DropDownList("EntityTypeId",new SelectList((IList<Mits.DAL.EntityModels.AffiliateType>)ViewData["EntityType"], "Id", "Type") ,new { onchange = "OnAffliateTypeChange()",style = "width:205px" })%>  
           </td>
       </tr>
       <tr>
            <td style="text-align:center">
                <select id="ddlAffiliates" style="width:205px;" onChange="OnAffliateChange()">
                    <option></option>
                </select>
            </td>
       </tr>
       <tr>
            <td style="text-align:center">
                <select id="ddlAffiliateEmails" style="width:205px;">
                    <option></option>
                </select>
            </td>
       </tr>
           <tr>
            <td style="text-align:center">
                <input type ="button" name="btnAddEmail" id="btnAddEmail" value="Add Email"  class="ui-button ui-widget ui-state-default ui-corner-all" />
            </td>
       </tr>
       <tr>
           <td style="text-align:center">
            <select style="width:300px; height:200px" id="emails" name="emails" multiple="multiple" onDblClick="removeEmail()">
            </select> <br />
            ***Double click to remove.

            <input id="remId" name="remId" value='' type="hidden" />
           </td>
       </tr>
       </table>
     
        
       
    </div>
    




     <div id="popupTemplate"  style="width: 320px; height: 200px; display: none; ">
     <table>
     <tr><td colspan="2" >   <div class="errorTemp" style="color:Red;">
                             </div>
     </td>
     </tr>
     <tr>
     <td> Template Name * :</td>    <td>  <input type="text" name="txtTemplateVal" id="txtTemplateVal" maxlength="100"  style="width:200px;" /></td>
     </tr>
        <tr><td></td><td></td>
     </tr>
        <tr><td></td><td></td>
     </tr>
     <tr>
     <td>
     </td>
       <td>     
      <input type="button" name="btnSaveTemplatePopup" id="btnSaveTemplatePopup" value="Submit"  class="ui-button ui-widget ui-state-default ui-corner-all" />
       <input type="button" name="btnCancelTemplate" id="btnCancelTemplate" value="Cancel"  class="ui-button ui-widget ui-state-default ui-corner-all" />
     </td>
     
     </tr>
     </table>
 
     </div>
 </div>

    <script src="../../Scripts/js/additional-methods.js" type="text/javascript"></script>
 <%--   <script src="../../Scripts/js/ui.dropdownchecklist.js" type="text/javascript"></script>--%>
    <script src="../../Scripts/js/jquery.multiSelect.js" type="text/javascript"></script>
    <script src="../../Scripts/js/jquery.tree.js" type="text/javascript"></script>
   <%-- <script src="<%=ResolveClientUrl("~/Scripts/js/jquery.maskedinput-1.2.2.js") %>" type="text/javascript"></script>--%>
    <script src="<%=ResolveClientUrl("~/Scripts/js/jquery.alphanumeric.js") %>" type="text/javascript"></script>
<%--    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.uploadify.v2.1.0.min.js")%>' type="text/javascript"></script>--%>
    <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/js/CreateEvent.js") %>"> </script>

</asp:Content>
