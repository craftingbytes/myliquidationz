<%@ Page Language="C#" MasterPageFile="~/Views/Shared/mits.master"
    Title="MITS - Items & Address " Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/fullcalendar.css" rel="stylesheet" type="text/css" />
    
    <style type='text/css'>
        #loading
        {
            position: absolute;
            top: 5px;
            right: 5px;
        }
        #calendar
        {
            width: 900px;
            margin: 0 auto;
        }
        .fc-event, .fc-agenda .fc-event-time, .fc-event a
        {
            background-color: Transparent; /* background color */
            border-color: Black; /* border color */
            color: White;
            background: url('../../Content/css/cupertino/images/ui-bg_highlight-soft_80_006699_1x100.png') repeat-x scroll 50% 50% #006699; /* text color */
        }
        .fc-header-title
        {
            margin-top: 5px;
            margin-bottom: 8px;
            white-space: nowrap;
            color: #7F7F81;
        }
        </style>
    <div id='loading' style="color: White; display: none; position: fixed; top: 0px;
        left: 600px;">
        <span style="background: url('../../Content/css/cupertino/images/ui-bg_highlight-soft_80_006699_1x100.png') repeat-x scroll 50% 50% #006699;">
            Loading....... </span>
    </div>
    <div class="admin">
      <h1 class="heading"> View Events</h1>
  
    <table align="center" width="100%">
    
        <tr style="margin-top:0px">
            <td align="center" colspan="2" style="margin-top:0px;">
                <!--<div style="padding-left: 10px; float: none; width: 900px;">-->
                    <fieldset>
                        <legend>Search Criteria</legend>
                        <table cellpadding="5">
                            <tr>
                                <td>
                                    Enter Text to Search Event:
                                </td>
                                <td>
                                    <input type="text" id="SearchText" name="SearchText" title="Please Enter Event Title, Type or Description of an Event" />
                                </td>
                                <td>
                                    Select State:
                                </td>
                                <td>
                                    <%= Html.DropDownList("states", (IEnumerable<SelectListItem>)ViewData["statesList"] as SelectList, new {multiple="multiple",style="text-align:left;z-index:auto;" })%>
                                </td>
                                <td>
                                    <input id="searchButton" type="button" title="Search" value="Search" onclick="SearchEvents()" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                                </td>
                            </tr>
                        </table>
                        &nbsp;
                        <%--<asp:Label ID="Label1" runat="server"  Text="Filter by state"></asp:Label>--%>
                    </fieldset>
                <!--</div>-->
            </td>
        </tr>
        <tr  style="margin-top:20px">
            <td  style="margin-top:20px">&nbsp;
                
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
    
        <tr  style="margin-top:20px">
            <td colspan="2"   style="margin-top:20px">
                <div id='calendar' style="clear: both; font-size: 12px; font-family: Lucida Grande,Helvetica,Arial,Verdana,sans-serif;">
                </div>
            </td>
        </tr>
    </table>
    <div style="text-align: left; font-size: small;" id="dialog" title="Basic dialog">
        <b>Title:-</b><span style="text-align: left;" id="eventTitle"></span><br />
        <b>Type:-</b><span style="text-align: left;" id="eventType"></span><br />
        <b>State:-</b><span style="text-align: left;" id="state"></span><br />
        <b>Date:-</b><span id="eventDate"></span><br />
        <b>Description:-</b><span id="eventDesc"></span>
    </div>
</div>
    <%--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />--%>

    <script src="../../Scripts/js/fullcalendar.min.js" type="text/javascript"></script>

    <%--<script src="../../Scripts/js/ui.dropdownchecklist.js" type="text/javascript"></script>--%>

    <script src="../../Scripts/js/dateformat.js" type="text/javascript"></script>



    <script type='text/javascript'>

        function InitializeCalendar() {


            $('#calendar').fullCalendar('destroy');
            $('#calendar').fullCalendar({

                header: {
                    left: 'prevYear,prev,next,nextYear  today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                selectable: true,
                selectHelper: true,
                editable: true,
                loading: function (bool) {
                    if (bool) {

                        $('#loading').show();
                    }
                    else $('#loading').hide();
                },
                eventMouseover: function (event, jsEvent, view) {
                    $("#dialog #eventTitle").text(event.title);
                    $("#dialog #eventDesc").text(event.description);
                    $("#dialog #eventType").text(event.eventType);
                    $("#dialog #state").text(event.stateName);
                    $("#dialog #eventDate").text(dateFormat(event.start, 'dddd, mmmm dS, yyyy'));

                    $("#dialog").dialog('open');
                },
                eventMouseout: function (event, jsEvent, view) {

                    //   $("#dialog").dialog('close');
                }

            });

        }


        $(document).ready(function () {

            $('#searchButton').button();
            $("#states").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 120, width: 100 });
            InitializeCalendar();
            $("#dialog").dialog({ position: ['right', 'centre'], title: 'Event Detail', autoOpen: false, height: 300, width: 200, resizable: false });
            $('#SearchText').keypress(function (event) {
                if (event.keyCode == '13') {
                    event.preventDefault();
                    SearchEvents();
                }

            });
            SearchEvents();

        });

        function SearchEvents() {
            InitializeCalendar();
            $('#calendar').fullCalendar('addEventSource', 'searchevents?states=' + $("#states").val() + '&searchtext=' + $('#SearchText').val() + '&_');
            return true;

        }
    </script>
</asp:Content>
