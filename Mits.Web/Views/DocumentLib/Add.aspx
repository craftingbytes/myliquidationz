﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: #bbb 1px solid;
            margin: 3px 0 0 0;
            width: 142px;
        }
        input[type=text].ui-pg-input
        {
            width: auto;
        }
        select
        {
            width: 145px;
        }
        td
        {
            /*padding-left: 10px;*/
        }
        td.right
        {
            border-right: solid 1px black;
          /*  padding-right: 30px;*/
            text-align: left;
        }
        tr.ba100
        {
            border: solid 1px black;
        }
        .date-pick
        {
            background: white url(/Content/images/calendar.gif) no-repeat scroll 97% center;
            border: 1px solid #A7A6AA;
        }
        .plus, .minus
        {
            height: 10px;
            width: 10px;
        }
        .plus
        {
            background-image: url(/Content/images/calander.gif);
        }
        .minus
        {
        }
        .input-validation-error
        {
            background-color: #FFEEEE;
            border: 1px solid #FF0000;
        }
        #fileInputUploader
        {
            width: 90px;
        }
    </style>
    <link href="/Content/css/default.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<div class="admin">

   <h1 class="heading"  >
        Load New Document</h1>
    <table>
          <tr>
    <td  style="padding-left:120px; padding-top:20px" colspan="2" >                                
            <div id="validationSummary" style="width:300px; " class="ValidationSummary" ></div>                    
    </td>
    </tr>

        <tr>
            <td align="right" width="30%">
            
                <font color='red'>*</font>Title :
            </td>
            <td>
                <input type="text" id="txtTitle" name="txtTitle" style="width: 318px;" maxlength="200" required="Title is required." />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td align="right">
                Tags :
            </td>
            <td>
                <input id="txtTags" name="txtTags" style="width: 318px;" type="text" maxlength="200" />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td align="right">
                Description :
            </td>
            <td>
                <textarea id="txtDescription" rows="3" name="txtDescription" style="height: 39px;
                    width: 318px;" onkeypress="return imposeMaxLength(this, 300);"></textarea>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                Upload Date :
            </td>
            <td>
                <input type="text" id="txtUploadDate" name="txtUploadDate" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                Plan Year :
            </td>
            <td>
                <input type="text" id="txtPlanYear" name="txtPlanYear" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                <font color='red'>*</font>State :
            </td>
            <td>
                <%= Html.DropDownList("ddlVendorState", (SelectList)ViewData["States"], new { style = "width:145px", required = "State is required." })%>
            </td>
        </tr>
        <tr>
            <td align="right">
                File :
            </td>
            <td>
                <input id="fileInput" name="fileInput" type="file" />
                <br />
                <%-- <asp:Button ID="btnUpload" runat="server" name="btnUpload"  Text="Upload"
                        ValidationGroup="Upload" UseSubmitBehavior="False" />--%>
                <input type="button" id="btnUpload" name="btnUpload" value="Upload" onclick="Valid()"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                
            </td>
            <td>
            </td>
        </tr>
    </table>
<%--    <link rel="Stylesheet" type="text/css" href='<%=ResolveClientUrl("~/Content/css/uploadify.css")%>' />
    <link rel="Stylesheet" type="text/css" href='<%=ResolveClientUrl("~/Content/css/default.css")%>' />--%>
  <div id="sidebar"></div>
</div>
    <%--<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/general.css")%>"  />--%>
    <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/cupertino/jquery-ui-1.10.2.custom.css")%>" />
    <link rel="Stylesheet" type="text/css" href=" <%=ResolveUrl("~/Content/css/uploadifive.css")%>" />
    
<%--    <script src='<%=ResolveClientUrl("~/Scripts/jquery-1.10.2.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/jquery-ui-1.10.2.custom.min.js")%>' type="text/javascript"></script>--%>

    <script src="<%=ResolveClientUrl("~/Scripts/UploadiFive/jquery.uploadifive.js") %>" type="text/javascript"></script>


    <script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>

    <script src='<%=ResolveClientUrl("~/Scripts/forms/DocumentLib.Add.js")%>' type="text/javascript" ></script>
    

  <script type="text/javascript" >
      function Valid() {
          var fv = $("#fileInputQueue").text();
          if (fv == "")
              $("#validationSummary").html("<ul class='ValidationSummary'><li>Please select a file first.</li></ul>");
              
      }
    </script>
</asp:Content>
