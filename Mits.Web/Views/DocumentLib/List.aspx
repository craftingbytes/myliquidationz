<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/mits.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<%--    <link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />
    
    <script src="../../Scripts/js/ui.dropdownchecklist.js" type="text/javascript"></script>--%>
<div class="admin">
     <h1 class="heading" >
        Search Document Library</h1>
        <input id="hfshowdelbtn" type="hidden" />
    
    <table   style="margin-top: 5px; border-collapse: collapse; border-spacing: 0;">
        <tr>
            <td style="text-align:center">
            </td>
        </tr>
        <tr>
            <td  style="text-align:center" >
                <br />
                <fieldset style="width: auto">
                    <legend>Search Criteria</legend>
                    <table style="width:100%">
                        <tr>
                            
                            <td style="text-align:right">
                                State:
                            </td>
                            <td style="width:56px; text-align:left">
                                <%= Html.DropDownList("state", (SelectList)ViewData["States"], new { multiple = "multiple", style = "text-align:left;z-index:auto;" })%>
                            </td>
                            <td style="text-align:right">
                                Year:
                            </td>
                            <td style="text-align:left;width:50px">
                                <input type="text" id="txtYear" size="5" />
                            </td>
                            
                            <td style="text-align:right;width:200px;">
                                Enter text here to search file:
                            </td>
                            <td style="text-align:left;">
                                <input type="text" id="txtSearch" size="30" />
                                <%--<span style="font-size: 9px">(Title, Tag, Description etc)</span>--%>
                            </td>
                            <td style="text-align:right">
                                <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                        </tr>
                    </table>
                    <br />
                </fieldset>
            </td>
        </tr>
        <tr>
              <td  style="text-align:center" >
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table></div>
    
      <script type="text/javascript" >
    
     $("#hfshowdelbtn").val( <%=ViewData["showdelbtn"] %>);
     </script>
    
       <script src='<%=ResolveClientUrl("~/Scripts/forms/DocumentLib.List.js")%>' type="text/javascript" ></script>
</asp:Content>
