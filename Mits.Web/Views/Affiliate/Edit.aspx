﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.AffiliateViewModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>--%>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/affiliate.js")%>' type="text/javascript"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <h3 style="color: Gray; padding-left: 20px; padding-top: 20px; padding-bottom: 6px;">
        Edit
        <%=ViewData["Name"] %></h3>
    <!--<% using (Html.BeginForm()) { %>-->
    <table>
        <tr>
            <td colspan="2" style="padding-left: 174px;">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                    <% if (Model.Messages.Count > 0)
                       {%><ul class="ValidationSummary">
                   <%foreach (var message in Model.Messages)
                     {%>
                   <li>
                       <%=message.Text %></li>
                   <%} %>
               </ul>
                    <%} %>
                </div>
                <div id="divDocuments" title="Download Attachments">
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 120px;" valign="top">
                <table style="margin-top: 6px;">
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>Entity Name :
                        </td>
                        <td>
                            <input type="text" id="Name" name="Name" value="<%= Model.Affiliate.Name%>" style="width: 200px;"
                                maxlength="50" required="Affiliate Name is required." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>Address 1 :
                        </td>
                        <td>
                            <input type="text" id="Address1" name="Address1" value="<%= Model.Affiliate.Address1%>"
                                style="width: 200px;" maxlength="100" required="Address1 is required." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            Address 2 :
                        </td>
                        <td>
                            <input type="text" id="Address2" name="Address2" value="<%= Model.Affiliate.Address2%>"
                                style="width: 200px;" maxlength="100" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>State :
                        </td>
                        <td>
                            <%= Html.DropDownList("StateId", Model.States, new { required = "State is required.", style = "width:205px" })%>
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>City :
                        </td>
                        <td>
                            <%= Html.DropDownList("CityId", Model.Cities, new { required = "City is required.", style = "width:205px" })%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right">
                            <span style='color: red'>*</span>Zip :
                        </td>
                        <td>
                            <input type="text" id="Zip" name="Zip" value="<%= Model.Affiliate.Zip%>" style="width: 200px;"
                                maxlength="10" required="Zip is required." regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                                regexpmesg="Invalid Zip format, Please try xxxxx or xxxxx-xxxx." />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right" valign="top" style="padding-top: 3px;">
                            <span style='color: red'>*</span>Phone :
                        </td>
                        <td>
                            <input type="text" id="Phone" name="Phone" value="<%= Model.Affiliate.Phone%>" style="width: 200px;"
                                maxlength="20" required="Phone is required." />
                            <%--regexp="\(?\b[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}\b"  regexpmesg= "Invalid Phone format, Please try (xxx)xxx-xxxx or xxx-xxx-xxxx."
                <br />
                                <label style=" font-size:smaller; font-family:Verdana">Format: (xxx)xxx-xxxx or xxx-xxx-xxxx</label>--%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            Fax :
                        </td>
                        <td>
                            <input type="text" id="Fax" name="Fax" value="<%= Model.Affiliate.Fax%>" style="width: 200px;"
                                maxlength="20" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right" valign="top" style="padding-top: 3px;">
                            Email :
                        </td>
                        <td colspan="2">
                            <input type="text" id="Email" name="Email" value="<%= Model.Affiliate.Email%>" style="width: 200px;"
                                 maxlength="50" regexp="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                regexpmesg="Invalid Email format, Please try with correct format." />
                               <%-- required="Email is required."--%>
                            <br />
                            <label style="font-size: smaller; font-family: Verdana">
                                Format: hello@mydomain.com</label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td align="right" valign="top" style="padding-top: 3px;">
                            Web Site :
                        </td>
                        <td colspan="2">
                            <input type="text" id="WebSite" name="WebSite" value="<%= Model.Affiliate.WebSite%>"
                                style="width: 200px;" maxlength="50" regexp="^(((h|H?)(t|T?)(t|T?)(p|P?)(s|S?))\://)?(www.|[a-zA-Z0-9].)[a-zA-Z0-9\-\.]+\.[a-zA-Z]*$"
                                regexpmesg="Invalid web site url format, Please try again." />
                            <br />
                            <label style="font-size: smaller; font-family: Verdana">
                                Format: www.mydomain.com</label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>Entity Type :
                        </td>
                        <td>
                            <%= Html.DropDownList("AffiliateTypeId", Model.AffiliateTypes, new { required = "Affiliate Type is required.", style = "width:205px" })%>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height:40px;">
                        <td align="right">
                            Entity Role :
                        </td>
                        <td>
                            <%= Html.DropDownList("AffiliateRoleId", Model.AffiliateRoles, new { style = "width:205px" })%>  
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            <span style='color: red'>*</span>Terms :
                        </td>
                        <td>
                            <%= Html.DropDownList("PaymentTermId", Model.PaymentTerms, new { required = "Payment Term is required.", style = "width:205px" })%>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height: 35px;">
                        <td align="right">
                            Active :
                        </td>
                        <td>
                            <%if (Model.Affiliate.Active == true || Model.Affiliate.Active == null)
                              {%>
                            <%= Html.CheckBox("Active", true)%>
                            <%}
                              else
                              {%>
                            <%= Html.CheckBox("Active", false)%>
                            <%} %>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td style="padding-left: 50px;" align="left" valign="top">
                <table style="margin-top: 6px;">
                    <tr>
                        <td align="right" valign="top" style="width: 100px; padding-top: 4px;">
                           Service Types :
                        </td>
                        <td>
                            <table valign="top">
                                <% foreach (var serviceType in Model.ServiceTypes)
                                   { %>
                                <tr>
                                    <td valign="top">
                                        <%bool present = false;
                                          foreach (var affiliateService in Model.AffiliateServices)
                                          {
                                              if (affiliateService.ServiceTypeId == serviceType.Id)
                                              {
                                                  present = true;
                                              }
                                          }
                                          if (present == true)
                                          {%>
                                        <input type="checkbox" name="selectedObjects" value="<%=serviceType.Id%>" checked="checked">
                                            <%= serviceType.Name%>
                                        </input>
                                        <%}
                          else
                          {%>
                                        <input type="checkbox" name="selectedObjects" value="<%=serviceType.Id%>">
                                            <%= serviceType.Name%>
                                        </input>
                                        <%} %>
                                        <%}%>
                                        <br />
                                        <input type="text" id="Longitude" name="Longitude" value="<%= Model.Affiliate.Longitude%>"
                                            style="visibility: hidden;" />
                                        <br />
                                        <input type="text" id="Latitude" name="Latitude" value="<%= Model.Affiliate.Latitude%>"
                                            style="visibility: hidden;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr align="left">
            <td align="left" style="padding-left: 205px; padding-top: 20px;" colspan="2">
                <input type="hidden" id="data" name="data" value='<%= ViewData["Data"] %>' />                
                <input type="button" id="btnViewAttachments" value="View Attachments" class="ui-button ui-widget ui-state-default ui-corner-all"
                    onclick="ShowDocumentsDiv();" />
                <input type="hidden" id="txtUploadedFileName" name="txtUploadedFileName" />
                <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Update.Value)
                  { %>
                  <input type="button" id="btnAttachment" value="Attach File" class="ui-button ui-widget ui-state-default ui-corner-all" />
                
                <input id="btnSubmit" name="btnSubmit" type="button" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%}
                  else
                  { %>
                  <input type="button" id="btnAttachment" value="Attach File" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/>
                
                <input id="btnSubmit" name="btnSubmit" type="button" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all"
                    disabled="disabled" />
                <%} %>
                <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
                  { %>
                <input type="button" id="btnAdd" name="btnAdd" value="Create Entity" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <%}
                  else
                  { %>
                <input type="button" id="Button1" name="btnAdd" value="Create Entity" class="ui-button ui-widget ui-state-default ui-corner-all"
                    disabled="disabled" />
                <%} %>
                <input type="button" id="btnCancel" name="btnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" />
            </td>
        </tr>
    </table>
    <!--<% } %>-->
</asp:Content>
