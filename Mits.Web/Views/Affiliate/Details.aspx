﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.DAL.EntityModels.Affiliate>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <h2>
        Details</h2>
    <table>
        <tr>
            <td style="width: 30%">
                Id
            </td>
            <td style="width: 70%">
                <%: Model.Id %>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                Name
            </td>
            <td style="width: 70%">
                <%: Model.Name %>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                Address1
            </td>
            <td style="width: 70%">
                <%: Model.Address1 %>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                Phone
            </td>
            <td style="width: 70%">
                <%: Model.Phone %>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                Fax
            </td>
            <td style="width: 70%">
                <%: Model.Fax %>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                WebSite
            </td>
            <td style="width: 70%">
                <%: Model.WebSite %>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                StateId
            </td>
            <td style="width: 70%">
                <%:((IList<Mits.Dal.EntityModels.State>)ViewData["StateList"]).FirstOrDefault(s => s.Id == Model.StateId).Name%>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                CityId
            </td>
            <td style="width: 70%">
                <%: Model.CityId %>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                Zip
            </td>
            <td style="width: 70%">
                <%: Model.Zip %>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                AffiliateTypeId
            </td>
            <td style="width: 70%">
                <%:((IList<Mits.Dal.EntityModels.AffiliateType>)ViewData["AffiliateTypeList"]).FirstOrDefault(s => s.Id == Model.AffiliateTypeId).Type%>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                ServiceTypeId
            </td>
            <td style="width: 70%">
                <%:((IList<Mits.Dal.EntityModels.ServiceType>)ViewData["ServiceTypeList"]).FirstOrDefault(s => s.Id == Model.ServiceTypeId)==null?
                    
                  "":
                    ((IList<Mits.Dal.EntityModels.ServiceType>)ViewData["ServiceTypeList"]).FirstOrDefault(s => s.Id == Model.ServiceTypeId).Name%>
            </td>
        </tr>
    </table>
    <p>
        <%: Html.ActionLink("Edit", "Edit", new { id=Model.Id }) %>
        |
        <%: Html.ActionLink("Back to List", "Index") %>
    </p>
</asp:Content>
