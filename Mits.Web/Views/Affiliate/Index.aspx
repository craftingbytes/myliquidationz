﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" %>


    <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<div class="admin">
    <h1 class="heading">Search Entity</h1>
     
    <table cellspacing="0" width="70%" style="margin-top: 5px">
       <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
            <fieldset style="height: 150px; width: 350px;">
        <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                    <table style="margin-top:10px">
                       
                        <tr>
                        <td align="right">
                                Entity Name:
                            </td>
                            <td align="left">
                                <input type="text" id="txtSearch" size="30" style="width: 200px;"  />                                
                            </td>
                           </tr>
                            <tr height="40">
                            <td align="right">
                                Entity Type:
                            </td>
                            <td align="left"> 
                            <%= Html.DropDownList("EntityTypeId",new SelectList((IList<Mits.DAL.EntityModels.AffiliateType>)ViewData["EntityType"], "Id", "Type") ,new { style = "width:205px" })%>  
                            </td>
                            </tr>
                            <tr>
                            <td>
                            </td>
                            <td align="left">
                                <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                        </tr>
                    </table>
                     </fieldset>            
            </td>
        </tr>
       <tr>
        <td style="padding-left:20px;padding-top:6px;">
            <input type="hidden" id="Data" name="Data" value='<%=ViewData["Data"] %>'/>
          <%if ((ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Mits.BLL.Authorization.AccessRights).Add.Value)
            { %>    
            <input type="button" id="btnAdd" name="btnAdd" value="Create Entity" class="ui-button ui-widget ui-state-default ui-corner-all"/>
            <%}
            else
            { %>
                <input type="button" id="btnAdd" name="btnAdd" value="Create Entity" class="ui-button ui-widget ui-state-default ui-corner-all" disabled="disabled"/>
            <%} %>
        </td>
        </tr>
        <tr>
            <td style="padding-top:3px;">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table>
     <%--<div class="contextMenu" id="MenuJqGrid"  style="width:200px;">

        <ul>
            <li id="editEntity" style="font-family: Arial,Helvetica,sans-serif;font-size:11px;color:Gray;">
                <!--<img src="themes/contextMenu/edit.png" />-->
                Edit Entity</li>
            <li id="editUsers" style="font-family: Arial,Helvetica,sans-serif;font-size:11px;color:Gray;">                
                Add/Edit Entity Users</li>
            <li id="editContractRates" style="font-family: Arial,Helvetica,sans-serif;font-size:11px;color:Gray;">                
                Add/Edit Entity Contract Rates</li>
            <li id="editTargets" style="font-family: Arial,Helvetica,sans-serif;font-size:11px;color:Gray;">                
                Add/Edit Entity Targets</li>
            <li id="editEntityState" style="font-family: Arial,Helvetica,sans-serif;font-size:11px;color:Gray;">                
                Edit Entity States</li>
        </ul>
    </div></div>--%>
    <%--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.contextmenu.r2.js")%>' type="text/javascript"></script>--%>
   <script src='<%=ResolveClientUrl("~/Scripts/forms/affiliate.index.js")%>' type="text/javascript"></script>

    
</asp:Content>

