﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.DAL.EntityModels.Affiliate>" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <h2>Delete</h2>

    <h3>Are you sure you want to delete this?</h3>
    <fieldset>
        <legend>Fields</legend>
        
        <div class="display-label">Id</div>
        <div class="display-field"><%: Model.Id %></div>
        
        <div class="display-label">Name</div>
        <div class="display-field"><%: Model.Name %></div>
        
        <div class="display-label">Address1</div>
        <div class="display-field"><%: Model.Address1 %></div>
        
        <div class="display-label">Address2</div>
        <div class="display-field"><%: Model.Address2 %></div>
        
        <div class="display-label">Phone</div>
        <div class="display-field"><%: Model.Phone %></div>
        
        <div class="display-label">Fax</div>
        <div class="display-field"><%: Model.Fax %></div>
        
        <div class="display-label">WebSite</div>
        <div class="display-field"><%: Model.WebSite %></div>
        
        <div class="display-label">Latitude</div>
        <div class="display-field"><%: String.Format("{0:F}", Model.Latitude) %></div>
        
        <div class="display-label">Longitude</div>
        <div class="display-field"><%: String.Format("{0:F}", Model.Longitude) %></div>
        
        <div class="display-label">Active</div>
        <div class="display-field"><%: Model.Active %></div>
        
        <div class="display-label">StateId</div>
        <div class="display-field"><%: Model.StateId %></div>
        
        <div class="display-label">CityId</div>
        <div class="display-field"><%: Model.CityId %></div>
        
        <div class="display-label">Zip</div>
        <div class="display-field"><%: Model.Zip %></div>
        
        <div class="display-label">AffiliateTypeId</div>
        <div class="display-field"><%: Model.AffiliateTypeId %></div>
        
        <div class="display-label">PaymentTermId</div>
        <div class="display-field"><%: Model.PaymentTermId %></div>
        
        <div class="display-label">ServiceTypeId</div>
        <div class="display-field"><%: Model.ServiceTypeId %></div>
        
    </fieldset>
    <% using (Html.BeginForm()) { %>
        <p>
		    <input type="submit" value="Delete" /> |
		    <%: Html.ActionLink("Back to List", "Index") %>
        </p>
    <% } %>

</asp:Content>

