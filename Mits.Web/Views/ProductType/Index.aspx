﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/mits.master" Inherits="System.Web.Mvc.ViewPage<Mits.BLL.ViewModels.ProductTypeViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

  <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
   <div class="admin">
     <h1 class="heading"> Add/Edit Product Type</h1>
   

    <table style="margin-top:20px;">
        <tr> 
             <td colspan="2"  style="padding-left:277px;padding-bottom:10px;">
           <ul id="message" name="ulMessage" style="color:red; visibility:hidden">
           <li>
            <label id="lblMessage" style="visibility:visible; color:Red"></label>
            </li>
            </ul>
            </td>
        </tr>
    
        <tr height="40px">
            <td align="right" width="300px">
                Select Product to Edit:
            </td>
            <td>
             <%= Html.DropDownList("ddlProductType", Model.ProductType, new { style = "width:205px" })%>
            </td>
        </tr>
        <tr height="40px">
            <td align="right">
                <span style='color:red'>*</span>Name :
            </td>
            <td>
                <input type="text" id="txtName" name="txtName" style="width: 200px;" maxlength="100" />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr height="40px">
            <td align="right" valign="top" style="padding-top:10px">
            
                Description :
            </td>
            <td style="padding-top:10px">
            
                <textarea id="txtDesc" rows="4" name="txtDesc" style="height: 100px;
                    width: 200px;" onkeypress="return imposeMaxLength(this, 300);"></textarea>
            </td>
            <td>
            </td>
        </tr>
        
         <tr height="40px">
            <td align="right" valign="top" style="padding-top:20px">
            
                Notes :
            </td>
            <td style="padding-top:20px">
             
                <textarea id="txtNotes" rows="4" name="txtNotes" style="height: 100px;
                    width: 200px;" onkeypress="return imposeMaxLength(this, 250);"></textarea>
            </td>
            <td>
            </td>
        </tr>
       
        <tr height="40px">
            <td align="right" width="300px">
             <br />
                Site Image ID:
            </td>
            <td>
             <br />
             <%= Html.DropDownList("ddlImageId", Model.SiteImageId, new { style = "width:205px" })%>
            </td>
        </tr>

        <tr height="40px">
            <td align="right" style="padding-top:10px">
                Estimated Weight :
            </td>
            <td style="padding-top:10px">
                <input type="text" id="txtWeight" name="txtWeight" style="width: 200px;" maxlength="100" regexp="^[0-9]+[0-9]*$" 
                    regexpmesg="Invalid Estimated Weight, please try a positive number"  />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>

        
         <tr height="40px">
            <td align="right" width="300px" style="padding-top:10px">
                Weight Quantity Type:
            </td>
            <td style="padding-top:10px">
             <%= Html.DropDownList("ddlWeightQuantityType", Model.WeightQuantityType, new { style = "width:205px" })%>
            </td>
        </tr>

        <tr height="40px"   >
            <td align="right" style="padding-top:10px">
                Estimated Item Per Container :
            </td>
            <td style="padding-top:10px">
                <input type="text" id="txtContainer" name="txtContainer" style="width: 200px;" maxlength="100"  regexp="^[0-9]+[0-9]*$" 
                    regexpmesg="Invalid Estimated Item Per Container , please try a positive number" />
            </td>
            <td>&nbsp;
                
            </td>
        </tr>
        
        
          <tr height="40px" >
            <td align="right" width="300px" style="padding-top:10px">
                Container Type:
            </td>
            <td style="padding-top:10px">
             <%= Html.DropDownList("ddlContainerType", Model.ContainerType, new { style = "width:205px" })%>
            </td>
        </tr>
        <tr height="40px">
            <td align="right">
            </td>
            <td>
           <br />  <br />
                <input type="button" id="btnSubmit" name="btnSubmit" type="submit" value="Submit"   class="ui-button ui-widget ui-state-default ui-corner-all"/>
                <input type="button" id="btnClear" name="btnClear" value="Clear" 
                class="ui-button ui-widget ui-state-default ui-corner-all"/>
            </td>
            <td>
            </td>
        </tr>
    </table>  <div id="sidebar"></div>
   </div>
   <script src='<%=ResolveClientUrl("~/Scripts/forms/producttype.index.js")%>' type="text/javascript"></script>
    
</asp:Content>

