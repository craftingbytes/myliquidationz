﻿var fileInputUploader = $("#fileInputUploader");
var rootPath = "/";
var rowid = 0;
var uploadRowId = '';
var fileName = '';
var periodList = '';
var statusList = '';
var oemList = [];
var processorList = [];
var collectorList = [];
var affiliatesList = [];
var emailList = [];
var subgrids = [];

var stateList = [];

var adminList = [];

$.ajax({
    type: "GET",

    url: rootPath + 'Event/LoadPeriodList',
    dataType: "text",
    success: function (result) {

        periodList = result;


    },
    async: false
});

$.ajax({
    type: "GET",

    url: rootPath + 'Event/LoadReminderStatusList',
    dataType: "text",
    success: function (result) {

        statusList = result;


    },
    async: false
});


emailList = '1:info@sony.com; 2:info@lg.com; 3:info@hp.com';

//todo on demand calls 
$.ajax({
    type: "GET",
    cache: false,
    url: rootPath + 'Event/LoadOEM',
    dataType: "json",
    success: function (result) {

        oemList = result;


    },
    async: false
});

$.ajax({
    type: "GET",
    cache: false,
    url: rootPath + 'Event/LoadProcessor',
    dataType: "json",
    success: function (result) {

        processorList = result;


    },
    async: false
});


$.ajax({
    type: "GET",
    cache: false,
    url: rootPath + 'Event/LoadCollector',
    dataType: "json",
    success: function (result) {

        collectorList = result;


    },
    async: false
});


$.ajax({
    type: "GET",
    cache: false,
    url: rootPath + 'Event/LoadAffiliates',
    dataType: "json",
    success: function (result) {

        affiliatesList = result;


    },
    async: false
});


$.ajax({
    type: "GET",
    cache: false,
    url: rootPath + 'Event/LoadStates',
    dataType: "json",
    success: function (result) {

        stateList = result;


    },
    async: false
});

$.ajax({
    type: "GET",
    cache: false,
    url: rootPath + 'Event/LoadAdmins',
    dataType: "json",
    success: function (result) {

        adminList = result;


    },
    async: false
});


var arr = [];

function createNode() {

    var root = {
        "id": "0",
        "text": "All",
        "value": "0",
        "showcheck": true,
        complete: true,
        "isexpand": true,
        "checkstate": 0,
        "hasChildren": true
    };





    var arrOEM = [];
    for (var i in oemList) {

        arrOEM.push({
            "id": "oem" + oemList[i].AffiliateId.toString(),
            "text": oemList[i].Name,
            "value": oemList[i].AffiliateId.toString(),
            "showcheck": true,
            complete: true,
            "isexpand": false,
            "checkstate": 0,
            "hasChildren": false
        });
    }


    var arrProcessor = [];
    for (var i in processorList) {

        arrProcessor.push({
            "id": "proc" + processorList[i].AffiliateId.toString(),
            "text": processorList[i].Name,
            "value": processorList[i].AffiliateId.toString(),
            "showcheck": true,
            complete: true,
            "isexpand": false,
            "checkstate": 0,
            "hasChildren": false
        });
    }


    var arrCollector = [];
    for (var i in collectorList) {

        arrCollector.push({
            "id": "col" + collectorList[i].AffiliateId.toString(),
            "text": collectorList[i].Name,
            "value": collectorList[i].AffiliateId.toString(),
            "showcheck": true,
            complete: true,
            "isexpand": false,
            "checkstate": 0,
            "hasChildren": false
        });
    }

    var arrState = [];
    for (var i in stateList) {

        arrState.push({
            "id": "col" + stateList[i].AffiliateId.toString(),
            "text": stateList[i].Name,
            "value": stateList[i].AffiliateId.toString(),
            "showcheck": true,
            complete: true,
            "isexpand": false,
            "checkstate": 0,
            "hasChildren": false
        });
    }

    var arrAdmin = [];
    for (var i in adminList) {

        arrAdmin.push({
            "id": "col" + adminList[i].AffiliateId.toString(),
            "text": adminList[i].Name,
            "value": adminList[i].AffiliateId.toString(),
            "showcheck": true,
            complete: true,
            "isexpand": false,
            "checkstate": 0,
            "hasChildren": false
        });
    }



    arr.push({
        "id": "OEM",
        "text": "OEM",
        "value": "-1",
        "showcheck": true,
        complete: true,
        "isexpand": false,
        "checkstate": 0,
        "hasChildren": true,
        "ChildNodes": arrOEM
    });

    arr.push({
        "id": "Processor",
        "text": "Processor",
        "value": "-2",
        "showcheck": true,
        complete: true,
        "isexpand": false,
        "checkstate": 0,
        "hasChildren": true,
        "ChildNodes": arrProcessor
    });

    arr.push({
        "id": "Collector",
        "text": "Collector",
        "value": "-3",
        "showcheck": true,
        complete: true,
        "isexpand": false,
        "checkstate": 0,
        "hasChildren": true,
        "ChildNodes": arrCollector
    });


    arr.push({
        "id": "State",
        "text": "State",
        "value": "-4",
        "showcheck": true,
        complete: true,
        "isexpand": false,
        "checkstate": 0,
        "hasChildren": true,
        "ChildNodes": arrState
    });


    arr.push({
        "id": "Admin",
        "text": "Admin",
        "value": "-5",
        "showcheck": true,
        complete: true,
        "isexpand": false,
        "checkstate": 0,
        "hasChildren": true,
        "ChildNodes": arrAdmin
    });


    root["ChildNodes"] = arr;

    return root;
}


var $j = window.jQuery;

function load() {

    var o = { showcheck: true

    };
    o.data = treedata;
    $("#tree").treeview(o);


}



$('#btnDel').hide();
function populateFields(eventId) {

    var eventObj;
    $.ajax({
        type: "GET",
        cache: false,

        url: rootPath + 'Event/GetEvent?eventId=' + eventId,
        dataType: "json",
        success: function (result) {

            eventObj = result;


        },
        async: false
    });


    $("#txtTitle").val(eventObj.Title);
    if (eventObj.RecSingle != null)
        $("#txtSingleRecur").val(eventObj.RecSingle);
    $("#txtDate").val(eventObj.Date);
    if (eventObj.RecDay != null)
        $("#txtEveryDays").val(eventObj.RecDay);
    $("#ddlStates").val(eventObj.StateId);
    if (eventObj.RecMonth != null)
        $("#txtMonthly").val(eventObj.RecMonth);
    $("#ddlEventType").val(eventObj.EventTypeId);
    if (eventObj.RecQuater != null)
        $("#txtQuaterly").val(eventObj.RecQuater);
    if (eventObj.Description != '' && eventObj.Description != null)
        $("#txtDescription").val(eventObj.Description);

    if (eventObj.RecYearly != null)
        $("#txtYearly").val(eventObj.RecYearly);

    if (eventObj.EndsOn != null && eventObj.EndsOn != '') {
        // $("#chkEndOn").attr("checked", "checked");
        $("#txtEndsOn").val(eventObj.EndsOn);
    }
    if (eventObj.RecSingle != null && eventObj.RecSingle != '') {
        $('input:radio[id$=rbtnSingleRec]').attr('checked', 'checked');

    }


    if (eventObj.RecDay != null && eventObj.RecDay != '') {
        $('input:radio[id$=rbtnEvery]').attr('checked', 'checked');

    }
    if (eventObj.RecMonth != null && eventObj.RecMonth != '') {
        $('input:radio[id$=rbtnMonthly]').attr('checked', 'checked');

    }

    if (eventObj.RecQuater != null && eventObj.RecQuater != '') {
        $('input:radio[id$=rbtnQuaterly]').attr('checked', 'checked');

    }

    if (eventObj.RecYearly != null && eventObj.RecYearly != '') {
        $('input:radio[id$=rbtnYearly]').attr('checked', 'checked');

    }

    $('#btnDel').show();
    //    if (eventObj.HasReminders) {

    //        $("#pnlReminder").show();
    //        $("#EventId").val(eventId);
    //        LoadData();
    //    }


}

function txtDateChange(txt) {
    var recType = $('input:radio[name=rbtnRec]:checked').val();
    if (recType == 'Monthly On') {
        var actiondate = $("#txtDate").val();
        var actionDay = actiondate.split("/");

        $("#txtMonthly").val(actionDay[1]);

        $("#txtSingleRecur").val('');

        $("#txtEveryDays").val('');

        $("#txtQuaterly").val('');

        $("#txtYearly").val('');

        $("#txtEndsOn").val('');

    }

    if (recType == 'Quarterly On') {
        var actiondate = $("#txtDate").val();
        var actionDay = actiondate.split("/");
        $("#txtQuaterly").val(actionDay[1]);
        $("#txtSingleRecur").val('');
        $("#txtEveryDays").val('');

        $("#txtMonthly").val('');



        $("#txtYearly").val('');

        $("#txtEndsOn").val('');


    }


    if (recType == 'Yearly On') {
        var actiondate = $("#txtDate").val();
        $("#txtYearly").val(actiondate);
        $("#txtSingleRecur").val('');

        $("#txtEveryDays").val('');

        $("#txtMonthly").val('');

        $("#txtQuaterly").val('');



        $("#txtEndsOn").val('');

    }


}

$("input:radio[name=rbtnRec]").change(function () {

    var recType = $('input:radio[name=rbtnRec]:checked').val();


    if (recType == 'None') {

        $("#txtSingleRecur").val('');

        $("#txtEveryDays").val('');

        $("#txtMonthly").val('');

        $("#txtQuaterly").val('');

        $("#txtYearly").val('');

        $("#txtEndsOn").val('');

    }


    if (recType == 'Every') {

        $("#txtSingleRecur").val('');



        $("#txtMonthly").val('');

        $("#txtQuaterly").val('');

        $("#txtYearly").val('');

        $("#txtEndsOn").val('');

    }

    if (recType == 'Monthly On') {
        var actiondate = $("#txtDate").val();
        var actionDay = actiondate.split("/");

        $("#txtMonthly").val(actionDay[1]);

        $("#txtSingleRecur").val('');

        $("#txtEveryDays").val('');

        $("#txtQuaterly").val('');

        $("#txtYearly").val('');

        $("#txtEndsOn").val('');

    }

    if (recType == 'Quarterly On') {
        var actiondate = $("#txtDate").val();
        var actionDay = actiondate.split("/");
        $("#txtQuaterly").val(actionDay[1]);
        $("#txtSingleRecur").val('');
        $("#txtEveryDays").val('');

        $("#txtMonthly").val('');



        $("#txtYearly").val('');

        $("#txtEndsOn").val('');


    }


    if (recType == 'Yearly On') {
        var actiondate = $("#txtDate").val();
        $("#txtYearly").val(actiondate);
        $("#txtSingleRecur").val('');

        $("#txtEveryDays").val('');

        $("#txtMonthly").val('');

        $("#txtQuaterly").val('');



        $("#txtEndsOn").val('');

    }

});



function getQueryString() {
    str = new RegExp('\\?([^#]*)').exec(window.location.href);
    if (!str) return '';
    return str[1].split('&')[0];
}


function GetEventIdFromQuery() {

    var queryId = getQueryString();

    if (queryId != null && queryId != '') {
        if (eventId == null || eventId == '' || eventId == 'undefined') {
            eventId = queryId.split('=')[1];
        }

    }

}

function openMemberPopup() {
    $('#treePopup').dialog({ height: 420, width: 400 });
    //load data
    load();


}

/////////////////////////////////////////////////////// File Upload //////////////////////////////////////////////




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function DistinctArray(array, ignorecase) {

    if (typeof ignorecase == "undefined" || array == null || array.length == 0) return null;

    if (typeof ignorecase == "undefined") ignorecase = false;

    var sMatchedItems = "";

    var foundCounter = 0;

    var newArray = [];

    for (var i = 0; i < array.length; i++) {

        var sFind = ignorecase ? array[i].toLowerCase() : array[i];

        if (sMatchedItems.indexOf("|" + sFind + "|") < 0) {

            sMatchedItems += "|" + sFind + "|";

            newArray[foundCounter++] = array[i];

        }

    }

    return newArray;


}

var eventId = $("#EventId").val();
if (eventId != null && eventId != '') {

    eventId = GetEventIdFromQuery();
}


var queryId = getQueryString();

if (queryId != null && queryId != '') {

    eventId = queryId.split('=')[1];
    populateFields(eventId);
}

GetEventIdFromQuery();
var currentGroupId = 1;


if (eventId != null && eventId != '') {

    $("#pnlReminder").show();
}
else {

    $("#pnlReminder").hide();

}


function post(affiliates) {
    eventId = $("#EventId").val();
    GetEventIdFromQuery();

    GetEventIdFromQuery()

    var grpId = '';
    var data = { Affiliates: affiliates, EventId: eventId, RowId: rowid };
    $.ajax({
        type: "POST",
        url: '../../Event/SaveGroupReminder',
        data: data,
        success: function (result) {
            grpId = result;
        },
        async: false
    });

    return grpId;

}

function postReminder(data) {
    var remId = '';

    $.ajax({
        type: "POST",
        url: '../../Event/SaveReminder',
        data: data,
        success: function (result) {
            remId = result;
        },
        async: false
    });

    return remId;

}

function LoadData() {
    eventId = $("#EventId").val();
    GetEventIdFromQuery();
    $("#jqGrid").setGridParam({ url: "/../../Event/GetGroups?eventId=" + eventId, page: 1
    }).trigger("reloadGrid");
}

$("#btnOK").click(

function () {
    var eventId = GetEventIdFromQuery();
    if (eventId == '') {
        
        $("#treePopup").dialog('close');
        return false;
    }

    AddMemberRow();


}


);
function AddMemberRow() {
    GetEventIdFromQuery();
    if (eventId == '' || eventId == 'undefined')
        return false;

    var affiliates = $("#tree").getCheckedNodes();

    if (affiliates == null || affiliates.toString() == '') {
        return false;

    }


    var affiliatesText = [];
    for (var i in affiliatesList) {

        for (var j in affiliates)
            if (affiliatesList[i].AffiliateId.toString() == affiliates[j].toString()) {

                affiliatesText.push(affiliatesList[i].Name);

            }

    }

    var res = DistinctArray(affiliatesText, false);
    affiliates = DistinctArray(affiliates, false);

    if (affiliatesText == null || affiliatesText == '')
        return;

    affiliatesText = res.join(" ; ");


    if (affiliates == null) {
        affiliates = "";
        return;
    }
    else {
        affiliates = affiliates.join(",");
    }


    var grpId = post(affiliates);

    currentGroupId = grpId;


    //    $('#jqGrid').addRowData(currentGroupId, "sony,button");

    //    be = "<input style='height:22px' type='button' value='Add Reminder' onclick=\"AddChildRecord('" + currentGroupId + "');\" />";
    //    $j("#jqGrid").jqGrid('setRowData', currentGroupId, { GroupId: currentGroupId, Members: affiliatesText, Action: be, MembersValues: affiliates });
    //    $j('input[value=Add Reminder]').button();

    // $('#jqGrid').trigger("reloadGrid");
    LoadData();
    rowid++;


    //$("#treePopup").dialog('close');
}



function AddFiles(parentRow) {

    // $("#filePopup").dialog({ height: 350, width: 400 });

    var w = window.open("../../Upload.aspx?fromReminder=true", "Window1",
"height=250,width=370,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");

    uploadRowId = parentRow.toString().split(',')[1];

}




$("#btnEmail").click(function () {

    var emailList = '';
    $('#emails :selected').each(function (i, selected) {
        emailList = emailList + $(selected).text() + ';';
    });

    var remId = $("#remId").val();

    var data = { RemiderId: remId, Emails: emailList };

    $.ajax({
        type: "POST",
        url: '../../Event/SaveReminderEmails',
        data: data,
        success: function (result) {

        },
        async: false
    });

    $("#emailPopup").dialog('close');

    LoadData();
    // LoadReminderData();

});




//$('#fileInput').uploadify({
//    'uploader': '../../Scripts/js/uploadify.swf',
//    'cancelImg': '../../Content/images/cancel.png',
//    'auto': false,
//    'multi': true,
//    'simUploadLimit': 4,
//    'sizeLimit': 10485760,
//    'script': '../Helpers/Upload.ashx',
//    'folder': '../Uploads',
//    onComplete: function (e, q, f, d) {
//        fileName = f.name;
//        return true;
//    },
//    onAllComplete: function (event, data) {
//        // alert(data.filesUploaded + " file(s) successfully uploaded ");
//        if (data.filesUploaded > 0) {
//            postFileData();
//        }
//    }


//});



$j("#ctl00_ContentPlaceHolder_EventId").change(

    function () {
        // alert('dsfsdf');

    }

    );


function postFileData(fName) {

    var data = { RemiderId: uploadRowId, FilePath: fName, FileName: fName };
    $.ajax({
        type: "POST",
        url: '../../Event/UploadFile',
        data: data,
        success: function (result) {

            $("#filePopup").dialog('close');

        },
        async: false
    });

    LoadData();

}

$('input[id$=btnClear]').click(function () {
    // $('#fileInput').uploadifyCancel();
    $('#fileInput').uploadifyClearQueue();

});


$('#btnUpload').click(function () {


    $('#fileInput').uploadifyUpload();



});

function Download(id) {
    if (id != '') {
        id = id.replace('#', '<~>');
        window.location = '../Event/Download?FileName=' + id;
    }
}


function openDialog(remId) {

    var list = [];

    $.ajax({
        type: "GET",
        url: rootPath + 'Event/LoadFiles?reminderId=' + remId,
        dataType: "json",
        success: function (result) {

            list = result;


        },
        async: false
    });



    $("#files").html('');

    for (var i in list) {
        $("#files").append("<li><a style=\"cursor:hand\" onclick=\"Download('" + list[i] + "');\" >" + list[i] + "</a>" + '</li>');
    }

    $("#popup").dialog({ height: 320, width: 340 });

}

$("#btnPopup").click(function () {

    $("#popup").dialog('close');


});


$(document).ready(function () {

    treedata = [createNode()];

    $("div.error").hide();
    function invalidMsg() {
        msg = 'Please specify values for the required fields marked with *. <br/>';
        $("div.error").show();
        $("div.error").html(msg);
        return false;

    }
    //    $("label:contains(*), td:contains(*)").each(
    //function () {
    //    $(this).html(
    //$(this).html().replace('*', "<span style='color:red'>*</span>")
    //);
    //}
    //);



    //        $("#txtSingleRecur").val(eventObj.RecSingle);
    //    $("#txtDate").val(eventObj.Date);

    $("#txtEveryDays").numeric();

    $("#txtMonthly").numeric();

    $("#txtQuaterly").numeric();

    function saveEvent(isTemplate) {

        var msg = '';

        var showMsg = false;
        eventId = $("#EventId").val();

        GetEventIdFromQuery();
        var title = $("#txtTitle").val();

        var recType = $('input:radio[name=rbtnRec]:checked').val();
        var singleRecur = $("#txtSingleRecur").val();
        var date = new Date($("#txtDate").val());
        var everyDays = $("#txtEveryDays").val();
        var state = $("#ddlStates").val();
        var monthly = $("#txtMonthly").val();
        var eventType = $("#ddlEventType").val();
        var quaterly = $("#txtQuaterly").val();
        var templateName = $("#txtTemplateVal").val();
        var desc = $("#txtDescription").val();
        var yearly = $("#txtYearly").val();

        var endsOnChk = false;
        var endsOn = new Date($("#txtEndsOn").val());

        if (title == null || title == '') {
            msg = msg + 'Please specify Title. <br/>';
            showMsg = true;

        }


        if (date == null || date == '') {
            msg = msg + 'Please specify Date. <br/>';
            showMsg = true;

        }

        //        if (desc == null || desc == '') {
        //            msg = msg + 'Please specify Description. <br/>';
        //            showMsg = true;

        //        }




        if (recType != '') {


            if (recType == 'Every' && everyDays == '') {

                msg = msg + 'Please specify value for Days Recurrence.<br/>';
                showMsg = true;

            }

            if (recType == 'Single Recurrence' && singleRecur == '') {

                msg = msg + 'Please specify value for Single Recurrence.<br/>';
                showMsg = true;

            }
            if (recType == 'Monthly On' && monthly == '') {
                msg = msg + 'Please specify value for Monthly Recurrence.<br/>';
                showMsg = true;
            }

            if (recType == 'Quarterly On' && quaterly == '') {
                msg = msg + 'Please specify value for Quarterly Recurrence.<br/>';
                showMsg = true;

            }


            if (recType == 'Yearly On' && yearly == '') {
                msg = msg + 'Please specify value for Yearly Recurrence.<br/>';
                showMsg = true;
            }





        }


        if (recType == 'Every') {

            monthly = '';
            yearly = '';
            quaterly = '';
            singleRecur = '';
        }

        if (recType == 'Monthly On') {
            everyDays = '';
            yearly = '';
            quaterly = '';
            singleRecur = '';
        }

        if (recType == 'Quarterly On') {
            everyDays = '';
            yearly = '';
            monthly = '';
            singleRecur = '';

        }


        if (recType == 'Yearly On') {
            everyDays = '';
            monthly = '';
            quaterly = '';
            singleRecur = '';
        }
        if (recType == 'Single Recurrence') {
            yearly = '';
            monthly = '';
            quaterly = '';
            everyDays = '';
        }

        if (endsOn == '') {
            endsOnChk = false;

        }
        else {
            endsOnChk = true;


        }

        if (recType == 'Monthly On' || recType == 'Quarterly On' || recType == 'Yearly On' || recType == 'Every') {
            if (endsOn == '') {
                msg = msg + 'Please specify Ends On (date).<br/>';
                showMsg = true;
            }

            if (endsOn <= date) {

                msg = msg + 'Ends On date must be greater than event date.<br/>';
                showMsg = true;

            }


        }

        var endsOn = $("#txtEndsOn").val();
        var date = $("#txtDate").val();



        if (showMsg) {
            $("div.error").show();
            $("div.error").html(msg);

            return false;
        }



        var data = { EventId: eventId, Title: title, Desc: desc, Date: date, RecType: recType, SingleRec: singleRecur,
            EveryDays: everyDays, State: state, Monthly: monthly, EventType: eventType, Quaterly: quaterly, Yearly: yearly,
            EndsOn: endsOnChk, EndsOnValue: endsOn, IsTemplate: isTemplate, TemplateName: templateName
        };
        $.ajax({
            type: "POST",
            url: '../../Event/SaveEvent?EventId=' + eventId,
            data: data,
            success: function (result) {

                if (result != null && result != '' && isTemplate == false) {
                    $("#pnlReminder").show();
                    $("#EventId").val(result);
                    $("div.error").hide();
                    window.location.href = "/Event/CreateEvent?EventId=" + result;


                    $('#btnDel').show();
                }
                else {
                    //  CloneGroupsAndRemiders(result, eventId);

                    window.location.href = "/Event/CreateEvent";

                }

            },
            async: false
        });






    }

    $('#btnSave').click(function () {

        saveEvent(false);


    });

    $('#btnDel').click(function () {

        $('#delPopup').dialog();



    });

    $('#btnYes').click(function () {

        $('#delPopup').dialog('close');

        //send call to del
        eventId = $("#EventId").val();

        GetEventIdFromQuery();
        $.ajax({
            type: "POST",
            url: '../../Event/DeleteEvent?eventId=' + eventId,

            success: function (result) {


                window.location.href = "/Event/CreateEvent";



            },
            async: false
        });




    });

    $('#btnNo').click(function () {

        $('#delPopup').dialog('close');



    });

    $('#ddlTemplate').change(function () {

        var eventId = $("#ddlTemplate").val();

        populateFields(eventId);



    });

    $("#btnSaveTemplatePopup").click(function () {

        var msgText = 'Please specify value for Template Name.';
        if ($("#txtTemplateVal").val() == '' || $("#txtTemplateVal").val() == null || $("#txtTemplateVal").val() == 'undefined') {

            $("div.errorTemp").show();

            $("div.errorTemp").html(msgText);
            return false;
        }

        //check for duplicate template name
        var isDuplicate;
        $.ajax({
            type: "GET",
            cache: false,
            url: rootPath + 'Event/IsEventTemplateDuplicate?name=' + $("#txtTemplateVal").val(),
            dataType: "json",
            success: function (result) {

                isDuplicate = result;


            },
            async: false

        });

        if (isDuplicate) {

            msgText = "A template with the same name already exists. Please change the template name.";

            $("div.errorTemp").show();

            $("div.errorTemp").html(msgText);
            return false;

        }


        saveEvent(true);
        $('#popupTemplate').dialog('close');


    });

    $("#btnSaveTemplate").click(function () {
        $("div.errorTemp").hide();
        $('#popupTemplate').dialog({ height: 200, width: 400 });


    });

    $("#btnCancelTemplate").click(function () {

        $('#popupTemplate').dialog('close');


    });

    $j("button, input:submit, input:button").button();

    $("input[id$=txtDate]").datepicker({ minDate: 0, dateFormat: 'mm/dd/yy' });
    //$("input[id$=txtYearly]").datepicker({ minDate: 0, dateFormat: 'mm/dd' });
    $("input[id$=txtEndsOn]").datepicker({ minDate: 0, dateFormat: 'mm/dd/yy' });
    $("input[id$=txtSingleRecur]").datepicker({ minDate: 0, dateFormat: 'mm/dd/yy' });

    //    $("input[id$=txtDate]").mask("99/99/9999");
    //$("input[id$=txtYearly]").mask("99/99");
    //    $("input[id$=txtEndsOn]").mask("99/99/9999");
    //    $("input[id$=txtSingleRecur]").mask("99/99/9999");


    $j("select[id$=lstMembers]").multiSelect(
            {
                selectAll: false,
                noneSelected: 'Add Members!',
                oneOrMoreSelected: '*',
                optGroupSelectable: true
            });
    ///////////////////////////////////////////////////
    GetEventIdFromQuery();


    $j("input[id$=btnSaveAs]").click(function () {

    });

    $("#jqGrid").jqGrid({
        //data: mydata,
        datatype: 'json',
        mtype: 'GET',
        url: '/Event/GetReminderByEvent?eventId=' + eventId,
        height: 150,

        colNames: ['ReminderId', 'ReminderGroupId', 'Number', 'Period', 'Email Subject', 'Status', 'Show on Calander', 'Emails', 'Attachments', 'Email', 'Attachment'],
            colModel: [
                { name: "ReminderId", index: "ReminderId", key: true, sortable: false, editable: false, hidden: true },
                { name: "ReminderGroupId", index: "ReminderGroupId", key: true, sortable: false, editable: false, hidden: true },
                { name: "Number", index: "Number", width: 57, sortable: false, editable: true, editrules: { required: true, integer: true, minValue: 0, maxValue: 100} },
                { name: "Period", index: "Period", width: 53, sortable: false, editable: true, formatter: 'select', edittype: 'select', editoptions: { value: periodList} },
                { name: "Title", index: "Title", width: 200, sortable: false, editable: true, editrules: { required: true, maxlength: 150 }, editoptions: { maxlength: 100} },
                { name: "Status", index: "Status", width: 70, sortable: false, editable: true, formatter: 'select', edittype: 'select', editoptions: { value: statusList} },
                { name: "ShowonCal", index: "ShowonCal", width: 10, align: 'center', sortable: false, editable: false, hidden: true, edittype: "checkbox", editOptions: { value: 'Yes:No'} },
                { name: "Emails", index: "Emails", width: 245, sortable: false, editable: false, hidden: false },

                { name: "Attachments", index: "Attachments", width: 100, sortable: false, editable: false },
                { name: "EmailButton", index: "EmailButton", width: 98, sortable: false, editable: false, formatter: EmailFormatter },
                { name: "Attachment", index: "Attachment", width: 90, sortable: false, editable: false, edittype: 'button' }

            ],
        rowNum: 10000,
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        editurl: '/Event/SaveReminder',
        width: 900,
        height: 300 
    });


});
//function AddChildRecord(rowId) {
//    jQuery('#jqGrid_' + rowId + '_t').jqGrid('editGridRow', 'new', { height: 280, reloadAfterSubmit: false });
//}

function AddChildRecord() {
    jQuery("#jqGrid").jqGrid('editGridRow', 'new', {
        height: 200,
        recreateForm: true,
        reloadAfterSubmit: true,
        addCaption: "Add Record",
        editCaption: "Edit Record",
        bSubmit: "Add",
        bCancel: "Done",
        bClose: "Close",
        saveData: "Data has been changed! Save changes?",
        bYes: "Yes",
        bNo: "No",
        bExit: "Cancel",
        modal: true,
        addedrow: 'last',
        closeAfterEdit: false,
        editData: { eventId: eventId },
        url: '/Event/SaveReminder',
        afterSubmit: function (response, postdata) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            //$('#TotalWeight').val(result.totalWeight);
            return [null, null,null];
        },
        afterShowForm: function (formid) {
            //setPalletNumber();
        },
        afterComplete: function (response, postdata, formid) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            return true;
        },
        viewPagerButtons: false
    });

}
function deleteGroup(rowId) {
    //$('#delGroupPopup').dialog();
    $('#jqGrid').delGridRow(rowId, { reloadAfterSubmit: true, url: '../../Event/DeleteGroup', afterSubmit: function (response, postdata) {
        if (response.responseText == "Success") {
            alert("Success:Group Deleted");
            return [true];
        }
        else {
            alert("Delete Failed:" + response.responseText);
            return [false];
        }
    }
    });
}

function EmailFormatter(cellvalue, options, rowdata) {
    //var actionMenuHtml = "<img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/>";
    //var actionMenuHtml  = "<img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/>";
    var actionMenuHtml = "<input style='height:22px' type='button' value='Add Emails' onclick=\"AddEmails('" + options.rowId + ',' + rowdata[7] + "');\" />";
    return actionMenuHtml;
}


function AddEmails(parentRow) {
    $("#remId").val(parentRow.toString().split(',')[0]);
    var existingEmails = parentRow.toString().split(',')[1];
    var list = existingEmails.split(';');

    $('#emails').empty();

    for (var i in list) {
        if (list[i] != "" && list[i] != null)
            $('#emails').append($("<option></option>").attr("value", list[i]).text(list[i]));
    }
    $("#emailPopup").dialog({ height: 400, width: 350 });
}
   
function OnAffliateTypeChange() {
    LoadAffiliates();
    
}



function LoadAffiliates() {
    var affiliateTypeId = 0;
    if ($("#EntityTypeId").val() > 0)
        affiliateTypeId = $("#EntityTypeId").val();

    $.ajax(
    {
        url: '/Event/GetAffiliatesByType/',
        dataType: 'json',
        type: "POST",
        data: ({ affiliateTypeId: affiliateTypeId }),
        async: false,
        success: function (result) {
            if (result.data) {
                $("select#ddlAffiliates").fillSelect(result.data);
            }
        }
    });
}

function OnAffliateChange() {
    LoadAffiliateEmails();

}

function LoadAffiliateEmails() {
    var affiliateId = 0;
    if ($("#ddlAffiliates").val() > 0)
        affiliateId = $("#ddlAffiliates").val();

    $.ajax(
    {
        url: '/Event/GetAffiliateEmails/',
        dataType: 'json',
        type: "POST",
        data: ({ affiliateId: affiliateId }),
        async: false,
        success: function (result) {
            if (result.data) {
                $("select#ddlAffiliateEmails").fillSelect(result.data);
            }
        }
    });
}

$("#btnAddEmail").click(function () {

    emailId = $("#ddlAffiliateEmails").val();
    email = $("#ddlAffiliateEmails :selected").text();
    $('#emails').append($("<option></option>").attr("value", emailId).text(email));
    updateEmail();
});

function removeEmail() {
    $('#emails :selected').remove();
    updateEmail();
}


function updateEmail() {

    var emailList = '';
    //$('#emails :selected').each(function (i, selected) {
    //    emailList = emailList + $(selected).text() + ';';
    //});

    $("#emails option").each(function(i){
        emailList = emailList + $(this).text() + ';';
    });
    var remId = $("#remId").val();

    var data = { RemiderId: remId, Emails: emailList };

    $.ajax({
        type: "POST",
        url: '/Event/SaveReminderEmails',
        data: data,
        success: function (result) {
            $('#jqGrid').trigger("reloadGrid");
        },
        async: false
    });

    //$("#emailPopup").dialog('close');

    //LoadData();
    // LoadReminderData();

}