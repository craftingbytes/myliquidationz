﻿
$(document).ready(function () {
    $("#ddlState").dropdownchecklist({ firstItemChecksAll: false, maxDropHeight: 120, width: 200 });
    $('#fileInput').uploadifive({
        //'uploader': '../../Scripts/js/uploadify.swf',
        'cancelImg': '/Content/images/cancel.png',
        'buttonImg': '/Content/images/browse.jpg',
        'auto': false,
        'multi': false,
        'simUploadLimit': 1,
        'sizeLimit': 10737418240,
        'checkScript': '/FileUpload/FileExists',
        'onCheck': function (file, exists) {
            if (exists) {
                alert("File with the same name already exists. \nPlease, rename your file.");
                //document.getElementById(jQuery(event.target).attr('id') + "Uploader").clearFileUploadQueue(true);
                $('#fileInput').uploadifive('clearQueue');
                //return false;
            }
        },
        'onError': function (event, ID, fileObj, errorObj) {

            alert("An error occured.");
            alert(errorObj.type + ' Error: ' + errorObj.info);
        },

        'uploadScript': '/Helpers/Upload.ashx',
        'folder': '/Uploads',
        onUploadComplete: function (file, data) {
            var fileNames = $('#txtFileNames').val();
            $('#txtFileNames').val(fileNames + ',' + file.name);
            return true;
        },
        onQueueComplete: function (uploads) {
            //if (data.filesUploaded > 0) {
            if (uploads.count > 0) {
                createAuditReport();
            }
        }
    });

    $('#btnCreate').click(function () {
        $('#ddlState').css("background-color", "white");
        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;
        //if (isValid) {
        //    var fv = $("#fileInput").val();
        //    if (fv == "") {
        //        isValid = false;
        //        $("#validationSummary").html("<ul class='ValidationSummary'><li>Please select at least a file.</li></ul>");
        //    }
        //}
        if (isValid) {
            $('#txtFileNames').val('');
            $('#fileInput').uploadifive('upload');
            //$('#fileInput').uploadifyUpload();
            //createAuditReport();
        }
        locateFacilitySelect();
    });

    $('#btnAdd').click(function () {
        $('#ddlState').css("background-color", "white");
        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;
        //if (isValid) {
        //    var fv = $("#fileInput").val();
        //    if (fv == "") {
        //        isValid = false;
        //        $("#validationSummary").html("<ul class='ValidationSummary'><li>Please select at least a file.</li></ul>");
        //    }
        //}
        if (isValid) {
            $('#txtFileNames').val('');
            //$('#fileInput').uploadifive('upload');
            //$('#fileInput').uploadifyUpload();
            createAuditReport();
        }
        locateFacilitySelect();
    });

    $('#btnClear').click(function () {
        $('#txtFileNames').val('');
        //$('#fileInput').uploadifive('clearQueue');
        //$('#fileInput').uploadifyClearQueue();
        $("#ddlState").val('0');
        $("#txtFacilityName").val('');
        $("#txtAuditDate").val('');
        $('#ddlState').css("background-color", "white");
        $("#validationSummary").html("");
        locateFacilitySelect();
    });

    $("#txtAuditDate").datepicker({ buttonImage: '/Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });

});

$(function () {
    locateFacilitySelect();
});

function locateFacilitySelect() {
    var offset = $("#ddlFacilityName").offset();
    var width = $("#ddlFacilityName").width() - 20;
    var height = $("#ddlFacilityName").height() - 2;
    var left = offset.left + 2;
    var top = offset.top + 1;
    $("#txtFacilityName").css({
        'display': 'block',
        'position': 'absolute',
        'left': left,
        'top': top,
        'height': height,
        'width': width
    });
    $("#ddlFacilityName").change(function () {
        var text = $("#ddlFacilityName option:selected").text();
        $("#txtFacilityName").val(text);
    });
}

function createAuditReport() {
    var state = $("#ddlState").val().join(', ');
    var facilityName = $('#txtFacilityName').val();
    var auditDate = $('#txtAuditDate').val();
    var fileNames = $('#txtFileNames').val();
    var planyear = $('#txtPlanYear').val();

    var data = { State: state, FacilityName: facilityName, AuditDate: auditDate, FileNames: fileNames, PlanYear: planyear };
    $.ajax({
        type: "POST",
        url: '/AuditReport/Add',
        data: data,
        success: function (result) {
            if (result.message == "AccessDenied") {
                var rootPath = window.location.protocol + "//" + window.location.host;
                var url = rootPath + '/Home/AccessDenied';
                window.location.href = url;
            } else {
                alert(result.message);
                if (result.type == 'Success') {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/AuditReport/Index';
                    window.location.href = url;
                }
            }
        },
        async: false
    });
}