﻿//********************************************************************************************************************************************* //
//********************************************************** Affliate Target JQ Grid - Start ************************************************** //
//********************************************************************************************************************************************* //
$(document).ready(function () {

    jQuery("#jqGrid").jqGrid({
        treeGrid: true,
        treeGridModel: 'adjacency',
        ExpandColumn: 'Name',
        url: "/Authorization/GetPermissions",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id', 'Area Name', 'Read', 'Create', 'Update', 'Delete', 'Actions'],
        colModel: [
        { name: 'id', width: 1, hidden: true, key: true },
   		{ name: 'Name', index: 'Name', editable: false, width: 115, sortable: false },
   		//{ name: 'Read', index: 'Read', editable: true, width: 115, sortable: false, edittype: 'custom', editoptions: { custom_element: CheckboxInput, custom_value: CheckboxValue }, formatter: CheckboxFormatter, unformat: unformatCheckbox, align: 'center' },
        { name: 'Read', index: 'Read', editable: true, width: 115, sortable: false, edittype: 'custom', editoptions: { custom_element: CheckboxInput, custom_value: CheckboxValue }, formatter: CheckboxFormatter, unformat: unformatCheckbox, align: 'center' },
   		{ name: 'Add', index: 'Add', editable: true, width: 115, sortable: false, edittype: 'custom', editoptions: { custom_element: CheckboxInput, custom_value: CheckboxValue }, formatter: CheckboxFormatter, unformat: unformatCheckbox, align: 'center' },
   		{ name: 'Update', index: 'Update', editable: true, width: 85, sortable: false, edittype: 'custom', editoptions: { custom_element: CheckboxInput, custom_value: CheckboxValue }, formatter: CheckboxFormatter, unformat: unformatCheckbox, align: 'center' },
   		{ name: 'Delete', index: 'Delete', width: 115, editable: true, sortable: false, edittype: 'custom', editoptions: { custom_element: CheckboxInput, custom_value: CheckboxValue }, formatter: CheckboxFormatter, unformat: unformatCheckbox, align: 'center' },
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }
   	],
        rowNum: 100,
        autowidth: true,
        height: 300,
        editurl: 'clientArray'
    });

    $("#ddlRole").change(function () {
        if ($("#ddlRole").val() == '0') {
            $('#txtName').val('');
        } else {
            $('#txtName').val($("#ddlRole").find("option:selected").text());
        }
        $('#jqGrid').setGridParam({ postData: { roleId: $("#ddlRole").val()} }).trigger("reloadGrid");
    });

    $("#btnCreate").click(function () {
        var roleName = $('#txtName').val();
        if (roleName == '') {
            alert('Role Name is required.');
            return;
        }
        SaveRow(lastSel);
        var rowIds = $("#jqGrid").getDataIDs();
        var permissions = '';
        if (rowIds && rowIds.length > 0) {
            for (var i = 0; i < rowIds.length; i++) {
                var row = $("#jqGrid").getRowData(rowIds[i]);
                permissions += row.id + ":";
                permissions += '' + row.Read + row.Add + row.Update + row.Delete + ',';
            }
        }
        $.ajax({
            url: '/Authorization/CreateRole',
            dataType: 'json',
            type: "POST",
            data: ({ roleName: roleName, permissions: permissions }),
            async: false,
            success: function (result) {
                if (result.message == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else if (result.roleId) {
                    alert(result.message);
                    $('#ddlRole').fillSelect(result.roleList);
                    $('#ddlRole').val(result.roleId);
                } else {
                    alert(result.message);
                }
            }
        });
    });

    $("#btnUpdate").click(function () {
        var roleId = $("#ddlRole").val();
        if (roleId < 1) {
            alert('Please select a role to update.');
            return;
        }
        var roleName = $('#txtName').val();
        if (roleName == '') {
            alert('Role Name is required.');
            return;
        }
        SaveRow(lastSel);
        var rowIds = $("#jqGrid").getDataIDs();
        var permissions = '';
        if (rowIds && rowIds.length > 0) {
            for (var i = 0; i < rowIds.length; i++) {
                var row = $("#jqGrid").getRowData(rowIds[i]);
                permissions += row.id + ":";
                permissions += '' + row.Read + row.Add + row.Update + row.Delete + ',';
            }
        }

        $.ajax({
            url: '/Authorization/UpdateRole',
            dataType: 'json',
            type: "POST",
            data: ({ roleId: roleId, roleName: roleName, permissions: permissions }),
            async: false,
            success: function (result) {
                if (result.message == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                } else {
                    alert(result.message);
                }
            }
        });
    });
});

//*************************************************** Custom Action Menu Formatter for JQ Grid - Start **************************************************** //

/*
cellvalue - the cell value
options - a set of options containing
rowId - the id of the row
colModel - the colModel for this column
rowData - the data for this row
*/

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    if (rowdata[6] == false) {
        return '-';
    }
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' class='edit'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function CheckboxFormatter(cellvalue, options, rowdata) {
    if (cellvalue == '-') {
        return '-';
    }
    if (cellvalue == "1") {
        return '<input type="checkbox" disabled="" offval="no" value="1" checked="checked">';
    } else if (cellvalue == "0") {
        return '<input type="checkbox" disabled="" offval="no" value="0">';
    }
}

function unformatCheckbox(cellValue, options, cellObject) {
    if (cellValue == '-') {
        return '-';
    }
    var checkboxHtml = $(cellObject).html();
    var checkboxObj = $(checkboxHtml);
    //var checked = checkboxObj.attr("checked");
     var checked = checkboxObj.prop('checked');
    if (checked) {
        return "1";
    } else {
        return "0";
    }
}

function CheckboxInput(value, options) {
    if (value == '-') {
        return $('<span>-</span>');
    }
    if (value == "1") {
        return $('<input type="checkbox" offval="no" checked="checked">');
    } else if (value == "0") {
        return $('<input type="checkbox" offval="no" >');
    }
}

function CheckboxValue(value) {
    //var checked = value.attr("checked");
    var checked = value.prop('checked');
    if (checked) {
        return "1";
    } else {
        return "0"
    }
}


//var to store last selected row id
var lastSel;

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function SaveRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').saveRow(rowId);
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function AfterSaveRow(result) {
    if (result.responseText.split(':')[0] == 'True') {
        $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
        $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
        alert(result.responseText.split(':')[1]);
    } else {
        alert(result.responseText.split(':')[1]);
    }
    $('#jqGrid').setGridParam({ url: "/Authorization/GetPermissions", postData: { roleName: $("#ddlRole").val()} }).trigger("reloadGrid");
    return false;
}
//*************************************************** Custom Action Menu Formatter for JQ Grid - End **************************************************** //


//********************************************************************************************************************************************* //
//********************************************************** Affliate Target JQ Grid - End **************************************************** //
//********************************************************************************************************************************************* //

