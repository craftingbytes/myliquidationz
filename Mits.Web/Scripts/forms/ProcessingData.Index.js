﻿//var to store last selected row id
var lastSel;
var inEditMode = false;
var weightCategoryId = 0;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;

function SaveAttachment(fileName, processDataId) {
    $('#txtUploadedFileName').val('');
    $('#lblFileName').html('');
    $("#ulAttachment").css({ display: "block" });
    $('#txtUploadedFileName').val(fileName);
    $('#lblFileName').html(fileName);
}

function LoadData(fileName) {
    var index = fileName.lastIndexOf('.');
    if (index > -1) {
        var s = fileName.substring(0, index);
        var values = s.split('-');
        if (values.length == 4) {
            $('#txtDate').val(values[0].substring(0, 2) + '/' + values[0].substring(2, 4) + '/' + values[0].substring(4, 8));
            $('#ddlPlanYear').val(values[1]);
            $('#txtRecievedBy').val(values[2]);
            $('#txtShipmentNo').val(values[3]);
        }
    }
    $("#jqGrid").setGridParam({ url: "/ProcessingData/ExcelImport?import=true&fileName=" + fileName, page: 1
    }).trigger("reloadGrid");
}

$(function () {
    if ($("#hfInvoiceId").val() == 'true') {
        $("#saved").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                }
            }
        });
    }


    $('#btnClear').button();
    $('input[id$=btnClear]').click(function () {
        // $('#fileInput').uploadifyCancel();
        $('#fileInput').uploadifyClearQueue();


    });

    $("#btnExcel").button().click(function () {
        Dialog(false);

    });

    $("#btnAttachFile").button().click(function () {
        Dialog(true);

    });




    $("input[id$=btnUpload]").button().click(function () {

        $('#fileInput').uploadifyUpload();


    });


    $("#btnAdd").button().click(function () {
        if (inEditMode == false) {
            inEditMode = true;
            var rowCount = $("#jqGrid").getGridParam('reccount');
            var mydata = [
		            { PalletUnitNumber: '', Description: "", RNonMetro: "false", Qty: "", Tare: "", TotalWeigtht: "" }
		        ];
            $("#jqGrid").jqGrid('addRowData', rowCount + 1, mydata[0]);
            var colProp = $('#jqGrid').getColProp('Description')
            if (colProp.editoptions.value == '') {
                $("#jqGrid").jqGrid('delRowData', rowCount + 1);
                alert("Selected Pickup/Ship From State is not support at the moment. Please select a supported State");
                return;
            }

            if (rowCount + 1 !== lastSel || rowCount == 0) {
                //$('#jqGrid').jqGrid('restoreRow', lastSel);
                EditRow(rowCount + 1);
                //            $.fn.fmatter.rowactions(rowCount + 1, 'jqGrid', 'edit', false);
                lastSel = rowCount + 1;

                var state = $('select#ddlPickupState').val().toLowerCase(); //Represents RegionID
                if (state != '57' && state != '28') {     //57 represents Wisconsin(WI) State, 28 Represents Minnesota(MN) have to apply special Logic

                    $('input[id=' + lastSel + '_RNonMetro]').attr('checked', 'checked');
                    $('input[id=' + lastSel + '_RNonMetro]').attr('disabled', 'disabled');
                }

            }
        } else {
            alert('There is already an item opened in edit mode. Please save or cancel it first.');
        }
    });

    $('input[id$=txtDate]').datepicker({});
    $('input[id$=txtDate]').datepicker('setDate', new Date());


    $('input[type=text][req=true]').blur(function (e) {
        if ($(this).val() == '') {
            $(this).css("background-color", "#FFEEEE");
            $(this).data("isValid", false);
        } else {
            $(this).css("background-color", "#FFFFFF");
            $(this).data("isValid", true);
        }
    });

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;

        if (isValid) {
            var d = new Date($("#txtDate").val());
            var now = new Date();
            var flag = d > now;
            if (flag) {
                $("#validationSummary").html("<ul><li>Processing Date must be less than or equal to today date</li></ul>");
                isValid = false;
            }
            else
                isValid = true;
        }
        var isFocused = false;
        isFocused = false;

        if (!isValid) {
            return false;
        }
        if ($("input#chkIAgree:checked").length != 1) {
            $("#agree").dialog({
                modal: true,
                width: 400,
                resizable: false,
                position: 'center',
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }

        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');
        if (!isAllSaved()) {

            return false;
        }
        if (inEditMode) {
            alert('There is an item opened in edit mode. Please save or cancel it first.');
            return false;
        }
        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    $("form").attr("action", rootPath + "/ProcessingData/Create");
                    compileGridData();
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });

    $('#ddlCompanyName').change(function () {
        FillClientAddressData();
    });

    $('#ddlPickUpCompanyName').change(function () {
        FillPickUpAddressData();
    });

    InitGrid();
    GetAttachments();
});

function InitGrid() {
    $("#jqGrid").jqGrid({
        //data: mydata,
        datatype: 'json',
        mtype: 'GET',
        url: rootPath + '/ProcessingData/ExcelImport?import=false',
        height: 150,

        colNames: ['Pallet Unit #*', 'Product*', 'Metro', 'Total Qty/Weight*', 'Tare*', 'Net Qty/Weight', 'Actions'],
        colModel: [
   		            { name: 'PalletUnitNumber', index: 'PalletUnitNumber', editable: true, width: 195, align: 'right', sortable: false, editrules: { integer: true, required: true} },
   		            { name: 'Description', index: 'Description', editable: true, width: 200, editrules: { required: true }, sortable: false, formatter: 'select', edittype: 'select' },
   		            { name: 'RNonMetro', index: 'RNonMetro', editable: true, width: 200, edittype: 'checkbox', sortable: false, formatter: 'checkbox', align: 'center', editoptions: { defaultValue: 'true', value: "true:false"} },
   		            { name: 'Qty', index: 'Qty', width: 212, editable: true, align: 'right', sortable: false, editrules: { required: true, integer: true} },
   		            { name: 'Tare', index: 'Tare', width: 200, editable: true, align: "right", sortable: false, editrules: { required: true, integer: true} },
   		            { name: 'TotalWeight', index: 'TotalWeight', width: 200, editable: false, sortable: false, align: 'right' },
//         		 { name: 'Action', index: 'Action', editable: false, width: 350, sortable: false, align: 'center'},//formatter: 'actions', formatoptions: { keys: false, editbutton: true, delbutton: true}
              { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],
        rowNum: -1,
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        //  editurl: rootPath + 'Rack/PerformAction',
        width: 925,
        height: 300,
        afterInsertRow: function (rowid, aData) {
            be = "<input id='" + rowid + "_edRow' style='height:22px;padding:0' type='button' value='Edit' onclick=\"editRow('" + rowid + "');\" title='Edit Record' />";
            se = "<input id='" + rowid + "_savRow' style='height:22px;padding:0' type='button' value='Save' onclick=\"saveRow('" + rowid + "');\" title='Save Record' />";
            ce = "<input id='" + rowid + "_canRow' style='height:22px;padding:0' type='button' value='Cancel' onclick=\"restoreRow('" + rowid + "');\" title='Cancel Edit' />";
            TotalWeight = aData["Qty"] - aData["Tare"] + 0;
            $("#jqGrid").jqGrid('setRowData', rowid, { TotalWeight: TotalWeight, Action: be + se + ce });
            $("input[type=button]").button();

        }
    });

    $("#jqGrid").footerData('set', { "Description": 'Totals', "Qty": '0', "Tare": '0', 'TotalWeight': '0' }, false);
    LoadDeviceTypes();
    
}

function Dialog(attachfile) {

    var w = window.open("../../Upload.aspx?attachfile=" + attachfile, "Window1", "height=300,width=450,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");

}
function isAllSaved() {
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    if (numberOfRecords == 0) {
        $("#noData").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                }
            }
        });
        return false;
    }
    else {
        var isSaved = true;
        for (i = 1; i <= numberOfRecords; i++) {
            var savedData = jQuery("#jqGrid").getRowData(i);
            if (savedData.Description.indexOf("<select") >= 0) {
                isSaved = false;
                break;
            }
        }
        if (!isSaved) {
            $("#unsaved").dialog({
                modal: true,
                width: 400,
                resizable: false,
                position: 'center',
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    }

    return isSaved;
}
function editRow(rowId) {
    $("#jqGrid").editRow(rowId);
}
function saveRow(rowId) {
    var oldData = $("#jqGrid").getRowData(rowId);

    if ($("#jqGrid").saveRow(rowId, false, 'clientArray')) {
        alert('');
        var newData = $("#jqGrid").getRowData(rowId);
        TotalWeight = newData["Qty"] - newData["Tare"] + 0;
        $("#jqGrid").jqGrid('setRowData', rowId, { TotalWeight: TotalWeight });

        //            var footerData = $("#jqGrid").footerData('get');

        //            footerData["Qty"] = footerData["Qty"] * 1 - oldData["Qty"] + newData["Qty"];
        //            footerData["Tare"] = footerData["Tare"] * 1 - oldData["Tare"] + newData["Tare"];
        //            footerData["TotalWeight"] = footerData["TotalWeight"] * 1 - oldData["TotalWeight"] + newData["TotalWeight"];

        //            $("#jqGrid").footerData('set', footerData, false);
    }
}
function restoreRow(rowId) {
    $.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.PalletUnitNumber == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
    }
}
function compileGridData() {
    //get total rows in you grid
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    var lineItems = '';
    for (i = 1; i <= numberOfRecords; i++) {

        var rowData = $("#jqGrid").getRowData(i);
        lineItems = 'PalletUnitNumber:' + rowData['PalletUnitNumber'];
        lineItems += ',Description:' + rowData['Description'];
        lineItems += ',RNonMetro:' + rowData['RNonMetro'];
        lineItems += ',Qty:' + rowData['Qty'];
        lineItems += ',Tare:' + rowData['Tare'];

        $('<input type="hidden" />').attr('name', 'Item_' + i).attr('value', lineItems).appendTo('#hiddenDivId');
    }

}
function LoadDeviceTypes(elem) {

    state = $("select#ddlPickupState").val();       //Represents RegionID
    city = $("input#txtPickupCity").val();
    zip = $("input#txtPickupZip").val();

    $("#jqGrid").clearGridData(true);


    $.ajax({
        url: rootPath + '/ProcessingData/GetDevicesList',
        dataType: 'text',
        type: "POST",
        data: ({ city: city, state: state, zip: zip }),
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('Description', { editoptions: { value: result} });
        }
    });

    GetCities(elem);
}

function GetWeightCategories() {
    state = $("select#ddlPickupState").val(); 
    $.ajax({
        url: rootPath + '/ProcessingData/GetWeightCategories',
        dataType: 'json',
        type: "POST",
        data: ({ state: state }),
        async: false,
        success: function (result) {
            if (result.data) {
                    $("select#ddlWeightCategories").fillSelect(result.data);
            }
        }
    });
    
}

function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            $("select#" + elem.name).clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        if (elem.name == "ddlPickupState") {
                            $("select#ddlPickupCity").fillSelect(result.data);
                            GetWeightCategories();
                        }
                        else
                            $("select#ddlVendorCity").fillSelect(result.data);

                    }
                }
            });
        }
    }
}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}


//*************************************************** Custom Action Menu Formatter for JQ Grid - Start **************************************************** //

/*
cellvalue - the cell value
options - a set of options containing
rowId - the id of the row
colModel - the colModel for this column
rowData - the data for this row
*/

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}



function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
    inEditMode = true;
}

function SaveRow(rowId) {
    //$('#jqGrid').saveRow(rowId, AfterSaveRow);
    
    if ($("#jqGrid").saveRow(rowId, false, 'clientArray')) {
        var newData = $("#jqGrid").getRowData(rowId);
        if (parseInt(newData["Qty"]).toString() == 'NaN' || parseInt(newData["Tare"]).toString() == 'NaN' || parseInt(newData["PalletUnitNumber"]).toString() == 'NaN') {
            return;
        }
        else {
             inEditMode = false;
            TotalWeight = newData["Qty"] - newData["Tare"] + 0;
            $("#jqGrid").jqGrid('setRowData', rowId, { TotalWeight: TotalWeight });
            $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
            $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
        }
    }
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    //$('#jqGrid').restoreRow(rowId);

    $.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.PalletUnitNumber == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
    }
    inEditMode = false;
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { delData: { InvoiceItemID: $("#jqGrid").getRowData(rowId).Id} });
}

function AfterSaveRow(result) {

    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    $('#jqGrid').trigger("reloadGrid")
}
//*************************************************** Custom Action Menu Formatter for JQ Grid - End **************************************************** //


$("#divDocuments").dialog({
    modal: false,
    width: '300',
    height: '300',
    resizable: false,
    position: 'center',
    autoOpen: false
}
);

function ShowDocumentsDiv() {
    $("#divDocuments").dialog({ buttons: { "Ok": function () { $(this).dialog("close"); } } });
    $("#divDocuments").dialog('open');
}

function GetAttachments() {
    $.ajax({
        url: '/ProcessingData/GetAttachmentsHTML/',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            //$("#divDocuments").html(result);
        }
    });
}

function DeleteAttachment(docID) {
    var ans = confirm('Are you sure?');
    if (ans) {
        $.ajax({
            url: '/ProcessingData/DeleteAttachment/',
            dataType: 'json',
            type: "POST",
            data: { docID: docID },
            async: false,
            success: function (result) {
                GetAttachments();
            }
        });
    }
}

function Download(fileName) {
    fileName = fileName.replace('#', '<~>');
    if (fileName != "-" && fileName != "")
        window.location = "/Affiliate/Download?FileName=" + fileName;

}

function FillClientAddressData() {
    var clientId = $('#ddlCompanyName').val();
    $("#txtCompanyName").val($("#ddlCompanyName").find("option:selected").text());
    if (clientId == 0) {
        $('#txtVendorAddress').val("");
        if ($("#ddlVendorState").get(0).options[0].selected != true) {
            $("#ddlVendorState").get(0).options[0].selected = true;
            $("#ddlVendorState").trigger("change");
        }
        $("#ddlVendorCity").val(0);
        $('#txtVendorZip').val("");
        $('#txtVendorPhone').val("");
        var clear = { data: [{ Value: 0, Text: 'Select'}] };
        $('#ddlPickUpCompanyName').fillSelect(clear.data);
        $("#ddlPickUpCompanyName").trigger("change");
    } else {
        $.ajax({
            url: '/ProcessingData/GetClientAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: clientId },
            async: false,
            success: function (result) {
                $('#txtVendorAddress').val(result.address);
                if ($('#ddlVendorState').val() != result.data) {
                    $('#ddlVendorState').val(result.state);
                    $("#ddlVendorState").trigger("change");
                }
                $('#ddlVendorCity').val(result.city);
                $('#txtVendorZip').val(result.zip);
                $('#txtVendorPhone').val(result.phone);
                $('#ddlPickUpCompanyName').fillSelect(result.pickUpAddresses);
                $("#ddlPickUpCompanyName").trigger("change");
            }
        });
    }
}

function FillPickUpAddressData() {
    var pickUpId = $('#ddlPickUpCompanyName').val();
    $("#txtPickUpCompanyName").val($("#ddlPickUpCompanyName").find("option:selected").text());
    if (pickUpId == 0) {
        $('#txtPickupAddress').val("");
        if ($('#ddlPickupState').get(0).options[0].selected != true) {
            $('#ddlPickupState').get(0).options[0].selected = true;
            $("#ddlPickupState").trigger("change");
        }
        $('#ddlPickupCity').val(0);
        $('#txtPickupZip').val("");
        $('#txtPickupPhone').val("");
        $('#txtPickupFax').val("");
        $('#txtPickupEmail').val("");
        $('#ddlCollectionMethod').val(0);
    } else {
        $.ajax({
            url: '/ProcessingData/GetPickUpAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: pickUpId },
            async: false,
            success: function (result) {
                $('#txtPickupAddress').val(result.address);
                if ($('#ddlPickupState').val() != result.data) {
                    $('#ddlPickupState').val(result.state);
                    $("#ddlPickupState").trigger("change");
                }
                $('#ddlPickupCity').val(result.city);
                $('#txtPickupZip').val(result.zip);
                $('#txtPickupPhone').val(result.phone);
                $('#txtPickupFax').val(result.fax);
                $('#txtPickupEmail').val(result.mail);
                $('#ddlCollectionMethod').val(result.method);
            }
        });
    }
}