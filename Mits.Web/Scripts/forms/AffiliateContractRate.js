﻿//********************************************************************************************************************************************* //
//********************************************************** Affliate Target JQ Grid - Start ************************************************** //
//********************************************************************************************************************************************* //
$(document).ready(function () {

    jQuery("#jqGrid").jqGrid({
        url: "/AffiliateContractRate/GetContractRates",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id', 'StateId', 'State', 'Start Date', 'End Date', 'Plan Year', 'ServiceTypeId', 'Service Type', 'Actions'],
        colModel: [
   		    { name: 'Id', index: 'Id', width: 85, hidden: true },
            { name: 'StateId', index: 'StateId', width: 85, hidden: true },
   		    { name: 'State', index: 'State', editable: true, width: 100, sortable: false, edittype: 'select' },
            { name: 'StartDate', index: 'StartDate', width: 100, sortable: false, editable: true, datefmt: 'd/M/yyyy' },
   		    { name: 'EndDate', index: 'End Date', width: 110, sortable: false, editable: true, datefmt: 'd/M/yyyy' },
            { name: 'PlanYear', index: 'Plan Year', width: 80, sortable: false, editable: true },
            { name: 'ServiceTypeId', index: 'ServiceTypeId', width: 85, hidden: true },
            { name: 'ServiceType', index: 'ServiceType', editable: true, width: 100, sortable: false, edittype: 'select' },
            { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 50, editable: false }
   	    ],
        rowNum: 50,
        rowList: [10, 25, 50, 100],
        autowidth: true,
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        //width: 953,
        height: 250,
        editurl: "/AffiliateContractRate/EditAffiliateContractRate"
    });

    $("#jqGrid").jqGrid('navGrid', '#pager', { edit: false, add: false, del: false, search: false, refresh: true });

    jQuery("#detailGrid").jqGrid({
        url: "/AffiliateContractRate/GetContractRateDetails",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['ProductTypeId', 'Product Type*', 'Quantity Type*', 'Price Rate*', 'Actions'],
        colModel: [
            { name: 'ProductTypeId', index: 'ProductTypeId', editable: false, width: 100, hidden: true },
   		    { name: 'ProductType', index: 'ProductType', editable: false, width: 100, sortable: false },
            { name: 'QuantityType', index: 'QuantityType', width: 100, editable: true, sortable: false, editrules: { required: true }, edittype: 'select' },
            { name: 'PriceRate', index: 'PriceRate', align: 'right', width: 100, editable: true, sortable: false, editrules: { required: true, number: true, minValue: 0, maxValue: 922337203685477.9999 }, editoptions: { size: 30, maxlength: 18} },
            { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: DetailActionMenuFormatter, title: true, align: 'center', width: 25, editable: false }
   	    ],
        rowNum: 200,
        width: 680,
        height: 230,
        editurl: 'clientArray',
        gridComplete: function () { LoadDropdownData(); }
    });

    $("#txtStartDate").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
    $("#txtEndDate").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });

    $("#dlgDetail").dialog({
        modal: true,
        width: 750,
        height: 480,
        zIndex: 500,
        resizable: false,
        position: 'center',
        autoOpen: false,
        show: 'fold',
        hide: 'fold',
        Ok: function () {
            $(this).dialog('close');
        }
    });

    $("#btnAdd").click(function () {
        $("#ddlState").attr('disabled', false);
        $("#ddlServiceType").attr('disabled', false);
        $('#dlgBtnSave').css({ display: "none" });
        if ($('#canAdd').val() == '') {
            $('#dlgBtnCreate').css({ display: "block" });
            $('#detailGrid').jqGrid('showCol', ["ActionMenu"]);
        } else {
            $('#dlgBtnAdd').css({ display: "none" });
            $('#detailGrid').jqGrid('hideCol', ["ActionMenu"]);
        }
        $("#ddlState").val('0');
        $("#ddlServiceType").val('0');
        $("#ddlPlanYear").val('Select');
        $("#txtStartDate").val('');
        $("#txtEndDate").val('');
        $("#ddlState").trigger("change");
        $("#dlgDetail").dialog('open');
        $("#gbox_detailGrid").removeClass("ui-widget");
    });

    $("#dlgBtnCancel").click(function () {
        $('#dlgDetail').dialog('close');
    });

    $("#dlgBtnSave").click(function () {
        EditContractRates('edit');
    });

    $("#dlgBtnCreate").click(function () {
        EditContractRates('new');
    });

    $('select[id$=ddlPlanYear]').change(function (elem) {
        ChangePlanYear();
    });

});

datePick = function (elem) {
    jQuery(elem).datepicker();
}

function LoadDropdownData() {
    $.ajax({
        url: '/AffiliateContractRate/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#detailGrid').setColProp('QuantityType', { editoptions: { value: result.quantityTypes} });
        }
    });
}

//*************************************************** Custom Action Menu Formatter for JQ Grid - Start **************************************************** //

/*
cellvalue - the cell value
options - a set of options containing
rowId - the id of the row
colModel - the colModel for this column
rowData - the data for this row
*/

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' class='edit'/>"
    if ($('#canDelete').val() == '') {
        actionMenuHtml += "&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete' title='Delete' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/>";
    }
    actionMenuHtml += "</div>";
    return actionMenuHtml;
}


function EditRow(rowId) {
    var row = $("#jqGrid").getRowData(rowId);
    var stateId = row.StateId;
    var startDate = row.StartDate;
    var endDate = row.EndDate;
    var planyear = row.PlanYear;
    var serviceTypeId = row.ServiceTypeId;

    $("#ddlState").attr('disabled', false);
    if ($('#canUpdate').val() == '') {
        $('#dlgBtnSave').css({ display: "block" });
        $('#detailGrid').jqGrid('showCol', ["ActionMenu"]);
    } else {
        $('#dlgBtnSave').css({ display: "none" });
        $('#detailGrid').jqGrid('hideCol', ["ActionMenu"]);
    }
    $('#dlgBtnCreate').css({ display: "none" });

    $("#ddlState").val(stateId);
    $("#ddlServiceType").val(serviceTypeId);
    $("#ddlPlanYear").val(planyear);
    $("#txtStartDate").val(startDate);
    $("#txtEndDate").val(endDate);
    $("#dlgDetail").dialog('open');
    $("#ddlState").attr('disabled', true);
    $("#ddlServiceType").attr('disabled', true);
    $("#txtEndDate").attr('disabled', true);
    $("#txtStartDate").attr('disabled', true);
    
    $("#gbox_detailGrid").removeClass("ui-widget");
    jQuery("#detailGrid").setGridParam({ postData: { oper: 'edit', stateId: stateId, startDate: startDate, endDate: endDate, serviceTypeId: serviceTypeId }, page: 1 }).trigger("reloadGrid");
}

function DeleteRow(rowId) {
    var row = $("#jqGrid").getRowData(rowId);
    var stateId = row.StateId;
    var startDate = row.StartDate;
    var endDate = row.EndDate;
    var serviceTypeId = row.ServiceTypeId;

    if (confirm("Delete selected record?")) {
        $.ajax({
            url: '/AffiliateContractRate/EditContractRates',
            dataType: 'json',
            type: "POST",
            data: ({ oper: 'del', stateId: stateId, startDate: startDate, endDate: endDate, serviceTypeId: serviceTypeId }),
            async: false,
            success: function (result) {
                if (result.message == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else if (result.status == "True") {
                    if (result.message != '') {
                        alert(result.message);
                    }
                    jQuery("#jqGrid").trigger("reloadGrid");
                } else {
                    alert(result.message);
                }
            }
        });
    }
}

function Cancel() {
    var url = '/Affiliate/Index/';
    window.location.href = url + "?data=" + $('#data').val();
}


function ChangePlanYear() {
    var stateid = $('#ddlState').val();
    var planyear = $('#ddlPlanYear').val();
    $.getJSON("/AffiliateContractRate/GetPlanYear",
        { planYear: planyear, stateId: stateid },
        function (data) {
            $("#txtStartDate").val(data.startDate);
            $("#txtEndDate").val(data.endDate);
        }
    );
}


function ChangeState() {
    var stateId = $('#ddlState').val();
    jQuery("#detailGrid").setGridParam({ postData: { stateId: stateId, oper: 'new' }, page: 1 }).trigger("reloadGrid");
    ChangePlanYear();
}

function DetailActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='Detail_ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:DetailEditRow(\"" + options.rowId + "\")' class='edit'/></div>";
    actionMenuHtml += "<div id='Detail_SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:DetailSaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel' title='Cancel' style='border:none;' onclick='javascript:DetailCancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

var detailLastSel;

function DetailEditRow(rowId) {
    if (rowId && rowId !== detailLastSel) {
        DetailCancelRow(detailLastSel);
        detailLastSel = rowId;
    }
    $('#Detail_ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#Detail_SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#detailGrid').editRow(rowId);
}

function DetailSaveRow(rowId) {
    $('#detailGrid').saveRow(rowId, null, null, null, DetailSaveRowCallback);
}

function DetailSaveRowCallback() {
    $('#Detail_ED_ActionMenu_' + detailLastSel).css({ display: "block" });
    $('#Detail_SC_ActionMenu_' + detailLastSel).css({ display: "none" });
}

function DetailCancelRow(rowId) {
    $('#Detail_ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#Detail_SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#detailGrid').restoreRow(rowId);
}

function EditContractRates(oper) {
    if (!ValidateDetailInputs()) {
        return;
    }
    DetailCancelRow(detailLastSel);
    var rowIds = $("#detailGrid").getDataIDs();
    var rates = '';
    if (rowIds && rowIds.length > 0) {
        for (var i = 0; i < rowIds.length; i++) {
            var row = $("#detailGrid").getRowData(rowIds[i]);
            if (row.PriceRate == '') {
                alert('Please enter valid information for the product type [' + row.ProductType + ']');
                return;
            }
        }
       
        var quantityTypeOptions = $('#detailGrid').getColProp('QuantityType').editoptions.value;
        var quantityTypeValues = quantityTypeOptions.split(';');

        for (var i = 0; i < rowIds.length; i++) {
            var row = $("#detailGrid").getRowData(rowIds[i]);
            var productType = row.ProductTypeId;
            var quantityType = GetSelectKey(quantityTypeValues, row.QuantityType);
            var priceRate = row.PriceRate;
            rates += rowIds[i] + ":" + productType + ":" + quantityType + ":" + priceRate + ";";
        }
    } else {
        alert("No Contract Rate(s).");
        return;
    }
    var stateId = $('#ddlState').val();
    var startDate = $('#txtStartDate').val();
    var endDate = $('#txtEndDate').val();
    var serviceTypeId = $('#ddlServiceType').val();
    var planYear = $('#ddlPlanYear').val();

    $.ajax({
        url: '/AffiliateContractRate/EditContractRates',
        dataType: 'json',
        type: "POST",
        data: ({ oper: oper, stateId: stateId, startDate: startDate, endDate: endDate, serviceTypeId: serviceTypeId, rates: rates, planYear: planYear }),
        async: false,
        success: function (result) {
            if (result.message == "AccessDenied") {
                var rootPath = window.location.protocol + "//" + window.location.host;
                var url = rootPath + '/Home/AccessDenied';
                window.location.href = url;
            }
            else if (result.status == "True") {
                if (result.message != '') {
                    alert(result.message);
                }
                $('#dlgDetail').dialog('close');
                jQuery("#jqGrid").trigger("reloadGrid");
            } else {
                alert(result.message);
            }
        }
    });
}

function ValidateDetailInputs() {
    var stateId = $('#ddlState').val();
    var startDate = $('#txtStartDate').val();
    var endDate = $('#txtEndDate').val();
    var serviceTypeId = $('#ddlServiceType').val();
    var errIndex = 0;
    var errMsg = '';
    if (stateId < 1) {
        errIndex++;
        errMsg += errIndex + ". State is required.\n";
    }
    if (serviceTypeId < 1) {
        errIndex++;
        errMsg += errIndex + ". Service Type is required.\n";
    }
    if (startDate == '') {
        errIndex++;
        errMsg += errIndex + ". Start date is required.\n";
    }
    if (endDate == '') {
        errIndex++;
        errMsg += errIndex + ". End date is required.\n";
    }
    if (startDate != '' && endDate != '') {
        if (new Date(startDate) >= new Date(endDate)) {
            errIndex++;
            errMsg += errIndex + ". Start date must be less than end date.\n";
        }
    }
    if (errMsg != '') {
        alert(errMsg);
        return false;
    }
    return true;
}

function GetSelectKey(values, value) {
    for (var i = 1; i < values.length; i++) {
        var option = values[i].split(':');
        if (option[1] == value) {
            return option[0];
        }
    }
    return 0;
}

//*************************************************** Custom Action Menu Formatter for JQ Grid - End **************************************************** //


//********************************************************************************************************************************************* //
//********************************************************** Affliate Target JQ Grid - End **************************************************** //
//********************************************************************************************************************************************* //

