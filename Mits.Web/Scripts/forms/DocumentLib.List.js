﻿$(document).ready(function () {

    var showdelbtn = $("#hfshowdelbtn").val();

    $("#state").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 120, width: 100 });

    $("button, input:submit, input:button").button();


    jQuery("#jqGrid").jqGrid({
        url: "/DocumentLib/GetDocumentLibList",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id', 'State', 'Title', 'Description', 'Tag', 'Year', '  ', 'Actions'],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
        { name: 'State', index: 'State', width: 100, editable: true, sortable: true, editrules: { required: true }, edittype: "select", editoptions: { maxlength: 125} },
   		{ name: 'Title', index: 'Title', width: 125, editable: true, sortable: true, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Description', index: 'Description', width: 150, editable: true, sortable: false },
   		{ name: 'Tags', index: 'Tags', width: 125, editable: true, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		//{ name: 'Year', index: 'Year', width: 85, align: "right", sortable: true, editable: true,
   		//    editoptions: {
   		//        dataInit: function (elem) { $(elem).datepicker({ buttonImage: '../../Content/images/calendar.gif', altFormat: 'mm/dd/yyyy', buttonImageOnly: true }); }
   		//    }
   		//},
        { name: 'Year', index: 'Year', width: 85, align: "right", sortable: true, editable: true},
   		{ name: 'AttachmentPath', index: 'AttachmentPath', sortable: false, formatter: myformatter, title: true, align: 'center', width: 25, editable: false },
  		{ name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 60, editable: false }
        //

   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 900,
        height: 230,
        sortname: 'Year',
        sortorder: 'desc',
        editurl: "/DocumentLib/PerformAction",
        gridComplete: function () { LoadDropdownsData(); }

    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();

    $("#txtSearch").keypress(function (event) {
        if (event.keyCode == '13') {

            var searchVal = $("#txtSearch").val();
            state = $("#state option:selected").text();
            var planyear = $("#txtYear").val();
            jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal, State: state, PlanYear: planyear }, page: 1 }).trigger("reloadGrid");

        }

    });
    $("#btnSearch").click(function () {
        var searchVal = $("#txtSearch").val();
        state = $("#state option:selected").text();
        var planyear = $("#txtYear").val();
        jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal, State: state, PlanYear: planyear  }, page: 1 }).trigger("reloadGrid");

    });

});

datePick = function (elem) {
    jQuery(elem).datepicker();
}

function myformatter(cellvalue, options, rowObject) {
   
    return "<img src='../../Content/images/d4.PNG'  alt='Open selected file'  title='Open selected file'  id='btnDel'   onclick=\"Download('" + cellvalue + "');\"    />";

}
function Download(cellvalue) {
   if (cellvalue != "-")
   {
       //window.location = "../DocumentLib/Download?FileName=" + cellvalue;
//       $.ajax({
//           url: '/DocumentLib/Download',
//           dataType: 'json',
//           type: "POST",
//           data: { cellvalue: cellvalue},
//           async: false,
//           success: function (result) {
////               $('#jqGrid').setColProp('State', { editoptions: { value: result.states} });
////               GetUserAccessRights();
//           }
//       });
       var rootPath = window.location.protocol + "//" + window.location.host;
       cellvalue = cellvalue.replace('#', '<~>');
       var url = rootPath + '/DocumentLib/Download/?FileName=' + cellvalue;
       window.location.href = url;
       }
}

function LoadDropdownsData(elem) {
    $.ajax({
        url: '/DocumentLib/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('State', { editoptions: { value: result.states} });
            GetUserAccessRights();
        }
    });

}

function GetUserAccessRights() {
    $.ajax({
        url: '/DocumentLib/GetUserAccessRights',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            ShowHideCommandButton(result.showEdit, result.showDelete);
        }
    });
}

function ShowHideCommandButton(showEdit, showDelete) {

    if (showEdit == false && showDelete == false) {
        $("#jqGrid").hideCol('ActionMenu');
    }
    else {
        if (showEdit == false)
            $("#jqGrid").find('div > img').filter(".edit").css("display", "none");
        if (showDelete == false)
            $("#jqGrid").find('div > img').filter(".delete").css("display", "none");
    }
}


//*************************************************** Custom Action Menu Formatter for JQ Grid - Start **************************************************** //

/*
cellvalue - the cell value
options - a set of options containing
rowId - the id of the row
colModel - the colModel for this column
rowData - the data for this row
*/

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete'  title='Delete'  onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

//var to store last selected row id
var lastSel;

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#jqGrid').editRow(rowId);
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });

}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { delData: { InvoiceItemID: $("#jqGrid").getRowData(rowId).Id} });
}

function AfterSaveRow(result) {
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    $('#jqGrid').trigger("reloadGrid");
    return false;
}
//*************************************************** Custom Action Menu Formatter for JQ Grid - End **************************************************** //
