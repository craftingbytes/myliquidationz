﻿//var to store last selected row id
var lastSel;
var inEditMode = false;
var weightCategoryId = 0;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;



$(document).ready(function () {
    if ($("#hfInvoiceId").val() == 'true') {
        $("#saved").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    $('input[id$=txtDate]').datepicker({});
    //$('input[id$=txtDate]').datepicker('setDate', new Date());

    $('input[type=text][req=true]').blur(function (e) {
        if ($(this).val() == '') {
            $(this).css("background-color", "#FFEEEE");
            $(this).data("isValid", false);
        } else {
            $(this).css("background-color", "#FFFFFF");
            $(this).data("isValid", true);
        }
    });

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;

        if (isValid) {
            var d = new Date($("#txtDate").val());
            var now = new Date();
            var flag = d > now;
            if (flag) {
                $("#validationSummary").html("<ul><li>Processing Date cannot be in the future.</li></ul>");
                isValid = false;
            }
            else
                isValid = true;
        }
        var isFocused = false;
        isFocused = false;

        if (!isValid) {
            return false;
        }
        if ($("input#chkIAgree:checked").length != 1) {
            $("#agree").dialog({
                modal: true,
                width: 400,
                resizable: false,
                position: 'center',
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }

        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');

        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    $("form").attr("action", rootPath + "/ProcessingData/Create");
                    //compileGridData();
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });

    $('#ddlCompanyName').change(function () {
        FillClientAddressData();
    });

    $('#ddlPickUpCompanyName').change(function () {
        FillPickUpAddressData();
    });

});


function GetPickupCities(elem) {
    GetCities(elem);
    GetWeightCategories();
}

function GetWeightCategories() {
    state = $("select#ddlPickupState").val(); 
    $.ajax({
        url: rootPath + '/ProcessingData/GetStateWeightCategories',
        dataType: 'json',
        type: "POST",
        data: ({ state: state }),
        async: false,
        success: function (result) {
            if (result.data) {
                    $("select#ddlWeightCategories").fillSelect(result.data);
            }
        }
    });
    
}

function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            $("select#" + elem.name).clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        if (elem.name == "ddlPickupState") {
                            $("select#ddlPickupCity").fillSelect(result.data);    
                        }
                        else
                            $("select#ddlVendorCity").fillSelect(result.data);

                    }
                }
            });
        }
    }
}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}



function FillClientAddressData() {
    var clientId = $('#ddlCompanyName').val();
    $("#txtCompanyName").val($("#ddlCompanyName").find("option:selected").text());
    if (clientId == 0) {
        $('#txtVendorAddress').val("");
        if ($("#ddlVendorState").get(0).options[0].selected != true) {
            $("#ddlVendorState").get(0).options[0].selected = true;
            $("#ddlVendorState").trigger("change");
        }
        $("#ddlVendorCity").val(0);
        $('#txtVendorZip').val("");
        $('#txtVendorPhone').val("");
        var clear = { data: [{ Value: 0, Text: 'Select'}] };
        $('#ddlPickUpCompanyName').fillSelect(clear.data);
        $("#ddlPickUpCompanyName").trigger("change");
    } else {
        $.ajax({
            url: '/ProcessingData/GetClientAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: clientId },
            async: false,
            success: function (result) {
                $('#txtVendorAddress').val(result.address);
                if ($('#ddlVendorState').val() != result.data) {
                    $('#ddlVendorState').val(result.state);
                    $("#ddlVendorState").trigger("change");
                }
                $('#ddlVendorCity').val(result.city);
                $('#txtVendorZip').val(result.zip);
                $('#txtVendorPhone').val(result.phone);
                $('#ddlPickUpCompanyName').fillSelect(result.pickUpAddresses);
                $("#ddlPickUpCompanyName").trigger("change");
            }
        });
    }
}

function FillPickUpAddressData() {
    var pickUpId = $('#ddlPickUpCompanyName').val();
    $("#txtPickUpCompanyName").val($("#ddlPickUpCompanyName").find("option:selected").text());
    if (pickUpId == 0) {
        $('#txtPickupAddress').val("");
        if ($('#ddlPickupState').get(0).options[0].selected != true) {
            $('#ddlPickupState').get(0).options[0].selected = true;
            $("#ddlPickupState").trigger("change");
        }
        $('#ddlPickupCity').val(0);
        $('#txtPickupZip').val("");
        $('#txtPickupPhone').val("");
        $('#txtPickupFax').val("");
        $('#txtPickupEmail').val("");
        $('#ddlCollectionMethod').val(0);
    } else {
        $.ajax({
            url: '/ProcessingData/GetPickUpAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: pickUpId },
            async: false,
            success: function (result) {
                $('#txtPickupAddress').val(result.address);
                if ($('#ddlPickupState').val() != result.data) {
                    $('#ddlPickupState').val(result.state);
                    $("#ddlPickupState").trigger("change");
                }
                $('#ddlPickupCity').val(result.city);
                $('#txtPickupZip').val(result.zip);
                $('#txtPickupPhone').val(result.phone);
                $('#txtPickupFax').val(result.fax);
                $('#txtPickupEmail').val(result.mail);
                $('#ddlCollectionMethod').val(result.method);
            }
        });
    }
}