﻿$(document).ready(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;



    $.ajax(
            {
                url: rootPath + '/Reconciliation/GetAllStates/',
                dataType: 'json',
                type: "POST",
                data: ({}),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#StateId").fillSelect(result.data);
                    }
                }
            });
    $.ajax(
            {
                url: rootPath + '/Reconciliation/GetAllOemAffiliates/',
                dataType: 'json',
                type: "POST",
                data: ({}),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#OemAffiliate").fillSelect(result.data);
                    }
                }
            });



    $("#jqGrid").jqGrid({
        url: '/Reconciliation/GetOEMTargetAssignedWeight',
        datatype: 'json',
        mtype: 'GET',

        colNames: ['', 'OEM Id', 'OEM Name', 'Target Weight', 'Pace Weight', 'Assigned Weight', 'Remaining Target Weight', 'Recommended Weight', 'Reconciliation Weight', '', ''],
        colModel: [
                  { name: 'checkbox', sortable: false, width: 30, align: 'center', formatter: TargetWeightCheckBoxFormatter },
                  { name: 'OEMId', width: 50 },
                  { name: 'Name', width: 100 },
                  { name: 'TargetWeight', width: 60, align: 'right', sortable: false },
                  { name: 'PaceWeight', width: 60, align: 'right', sortable: false },
                  { name: 'AssignedWeight', width: 60, sortable: false, align: 'right' },
                  { name: 'RemainingTargetWeight', width: 110, sortable: false, align: 'right' },
                  { name: 'RecommendedWeight', width: 110, sortable: false, align: 'right' },
                  { name: 'IsPermanentDropoffLocation', hidden: true },
                  { name: 'ReconciliationWeight', width: 100, align: 'center', formatter: OEMReconciliationWeightTextBoxFormatter, sortable: false },    
                  { name: 'Action', formatter: OEMRevertFormatter, hidden: true, width: 20, align: 'center' },
                  

      ],
        rowNum: 10000,
        rowList: [10, 25, 50, 100],
        pager: "",
        viewrecords: true,
        rownumbers: false,
        width: 900,
        height: 180
    });

    $("#jqGrid2").jqGrid({
        url: '/Reconciliation/GetAvailableProcessedWeight',
        datatype: 'json',
        mtype: 'GET',

        colNames: ['', 'InvoiceId', 'Status', 'Assigned For', 'Processed Date', 'Collection Method', 'Available Weight', 'Assigning Weight', ''],
        colModel: [
                  { name: 'checkbox', sortable: false, hidden: true, width: 20, align: 'center', formatter: InvoiceCheckBoxFormatter },
                  { name: 'InvoiceId', width: 40 },
                  { name: 'Status', width: 40, formatter: ProcesserStatusFormatter },
                  { name: 'AssignedFor', width: 60, formatter: InitAssignedForOemsFormatter },
                  { name: 'ProcessedDate', width: 80, align: 'center', sortable: false },
                  { name: 'CollectionMethod', sortable: false, width: 100, align: 'center' },
                  { name: 'AvailableWeight', sortable: false, width: 100, align: 'right' },
                  { name: 'AssigningWeight', sortable: false, width: 100, align: 'center', formatter: ProcesserInvoiceAssigningWeightTextBoxFormatter },
                  { name: 'Action', formatter: RevertFormatter, width: 20, align: 'center' }

      ],
        rowNum: 1000000,
        rowList: [10, 25, 50, 100],
        pager: "",
        viewrecords: true,
        rownumbers: false,
        width: 900,
        height: 200
    });

    $('select[id$=StateId]').change(function (elem) {
        $("#resultSummary").text("");
        LoadReconciliationData();
    });

    $('select[id$=PlanYear]').change(function (elem) {
        $("#resultSummary").text("");
        LoadReconciliationData();
    });

    $('select[id$=OemAffiliate]').change(function (elem) {
        $("#resultSummary").text("");
        LoadReconciliationData();
    });

    $('#chkIncludeReconciled').click(function () {
        $("#resultSummary").text("");
        LoadReconciliationData();
    });

})

function ProcesserStatusFormatter(cellvalue, options, rowObject) {
    if (cellvalue == 0)
        return "";
    else
        return "Assigned";
}

function LoadReconciliationData() {

    state = $("select#StateId").val();
    planyear = $("select#PlanYear").val();
    oemAffiliate = $("select#OemAffiliate").val();
    displayReconciled = $("#chkIncludeReconciled").prop('checked');
    if (state==0 || planyear == 0)
        return;
    jQuery("#jqGrid").setGridParam({ postData: { oemid: oemAffiliate, stateId: state, planYear: planyear }, page: 1 }).trigger("reloadGrid");
    jQuery("#jqGrid2").setGridParam({ postData: { stateId: state, planYear: planyear, displayReconciled: displayReconciled }, page: 1 }).trigger("reloadGrid");
    var rootPath = window.location.protocol + "//" + window.location.host;
    $.ajax(
                        {
                            url: rootPath + '/Reconciliation/GetAvailableProcessedWeight/',
                            dataType: 'json',
                            type: "POST",
                            data: ({ stateId: state, planyear: planyear, displayReconciled: displayReconciled }),
                            async: false,
                            success: function (result) {
                                $("#AvailableTotal").html("<b>" + commafy(result.TotalAvailableWeight) + "</b>");
                                $("#AssigningWeight").html("<b>" + 0 + "</b>");
                            }
                        });
    $.ajax(
                        {
                            url: rootPath + '/Reconciliation/GetOEMTotalTargetWeight/',
                            dataType: 'json',
                            type: "POST",
                            data: ({ oemid: oemAffiliate, stateId: state, planyear: planyear }),
                            async: false,
                            success: function (result) {
                                $("#hidTotalTargetWeight").val(result.totalTargetWeight);
                            }
                        }
                        );

}




function AssignedWeightFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    return "<div id='AssignedWeight" + rowid + "'>" + cellvalue + "</div>";
}

function InitAssignedForOemsFormatter(cellvalue, options, rowObject) {
    var rows = jQuery("#jqGrid").jqGrid('getRowData');
    var rowid = options.rowId;
    var retStr = "<select id=assignedforoems" + rowid + " onchange=\"AssignedforChanged('" + rowid + "')\">";
    retStr += "<option value=" + 0 + ">select</option>";
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var oemid = row.OEMId;
        retStr += "<option value=" + oemid + ">" + oemid + "</option>";
    }
    retStr += "</select>";
    retStr += "<input type='hidden' id=hidassignedforoems" + rowid + " value=" + 0 + " />";
    return retStr;
}

function AssignedforChanged(rowid) {
    var oldoemid = $("#hidassignedforoems" + rowid).val();
    var newoemid = $("#assignedforoems" + rowid).val();
    $("#hidassignedforoems" + rowid).val(newoemid);

    var AssigningWeight = $("#assigning" + rowid).val();
    AssigningWeight = ToNumber(AssigningWeight);

    if (oldoemid != "0") {
        var OldReconWeight = $("#Rec" + oldoemid).val();
        OldReconWeight = ToNumber(OldReconWeight);
        OldReconWeight = Number(OldReconWeight) - Number(AssigningWeight);
        $("#Rec" + oldoemid).val(OldReconWeight.toFixed(2));
        if (newoemid == 0) {
            var assingWeight = $("#assigning" + rowid).val();
            jQuery("#jqGrid2").jqGrid('setRowData', rowid, { Status: 0, AvailableWeight: assingWeight });
            $("#assigning" + rowid).val(0);
            var totalAssigningWeight = $("#AssigningWeight").text();
            totalAssigningWeight = Number(ToNumber(totalAssigningWeight)) - Number(ToNumber(AssigningWeight));
            $("#AssigningWeight").html("<b>" + commafy(totalAssigningWeight.toFixed(2)) + "</b>");
        }
    }
    else {
        var row = jQuery("#jqGrid2").jqGrid('getRowData', rowid);
        var avalWeight = row.AvailableWeight;
        $("#assigning" + rowid).val(avalWeight);
        jQuery("#jqGrid2").jqGrid('setRowData', rowid, { Status: 1, AvailableWeight: 0 });
        AssigningWeight = avalWeight;

        var totalAssigningWeight = $("#AssigningWeight").text();
        totalAssigningWeight = Number(ToNumber(totalAssigningWeight)) + Number(ToNumber(AssigningWeight));
        $("#AssigningWeight").html("<b>" + commafy(totalAssigningWeight.toFixed(2)) + "</b>");
    }

    var NewReconWeight = $("#Rec" + newoemid).val();
    if (NewReconWeight) {
        NewReconWeight = ToNumber(NewReconWeight);
        NewReconWeight = Number(ToNumber(NewReconWeight)) + Number(ToNumber(AssigningWeight));
        $("#Rec" + newoemid).val(NewReconWeight.toFixed(2));
    }

}

function OEMRevertFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var status = options.Status;
    var oemid = options.AssignedFor;

    return "<div><a href='javascript:OEMRevertReconcilation(\"" + rowid + "\")'><img src='../../Content/images/icon/oemrevert.png'  alt='Revert'  title='Revert'  style='border:none;'/> </a></div>";
}

function OEMRevertReconcilation(rowid) {
    var result = confirm("Are you confirmed to revert the reconciliation for the OEM?");
    if (result == false)
        return;
    $("#resultSummary").text("Reverting reconciliation...");
    var stateId = $("select#StateId").val();
    var planYear = $("select#PlanYear").val();

    var rootPath = window.location.protocol + "//" + window.location.host;
    $.ajax({
        url: rootPath + '/Reconciliation/RevertRecocilation/',
        dataType: 'json',
        type: "POST",
        data: ({ stateId: stateId, planYear: planYear, OemId: rowid }),
        async: false,
        success: function (result) {
            if (result) {
                LoadReconciliationData();
                $("#resultSummary").text("Revert succeed.");
            }
            else {
                $("#resultSummary").text("Revert failed.");
            }
        }
    });

}

function RevertFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var status = options.Status;
    var oemid = options.AssignedFor;

    return "<div><a href='javascript:RevertProcesserInvoice(\"" + rowid + "\")'><img src='../../Content/images/icon/revert.ico'  alt='Revert'  title='Revert'  style='border:none;'/> </a></div>";

    //return "<input type='button' id='InvoiceBtn" + rowid + "' value='Reset' onclick=\"ResetProcesserInvoice('" + rowid + "')\"/>";
}

function RemainingTargetWeightFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    return "<div id='RemainingTargetWeight" + rowid + "'>" + cellvalue + "</div>";
}

function TargetWeightCheckBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var RecommWeight = rowObject[7];
    var IsPermanent = rowObject[8];
    RecommWeight = ToNumber(RecommWeight);
    return "<input type='checkbox' Name='OEM' id='oemCheck" + rowid + "' onclick=\"OEMCheckBoxClick(" + rowid + "," + RecommWeight + "," + IsPermanent + ")\" />";

}

function InvoiceIdFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var options;
    for (i = 0; i < cellvalue.length; i++) {
        options = options + "<option>" + cellvalue[i] + "</option>";
    }
    return "<select id='ddlInvoice" + rowid + "' >" + options +"</select>";
}

function OEMReconciliationWeightTextBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var RecommWeight = rowObject[6];
    RecommWeight = ToNumber(RecommWeight);
    //return "<input type='text' id='Rec" + options.rowId + "' value='0' onblur=\"blurReconValue(" + rowid + ")\" />";
    return "<input type='text' id='Rec" + options.rowId + "' value='0' readonly='true' />";
}

function InvoiceCheckBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    return "<input type='checkbox' id='InvoiceCheck" + rowid + "' />";
        //return "<input type='checkbox' id='InvoiceCheck" + rowid + "' onclick=\"resetValue()\" />";
}

function ProcesserInvoiceAssigningWeightTextBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    //retrun "<input type='text' id='assigning" + options.rowId + "' onblur=\"resetValue()\" value='0' />"
    return "<input type='text' id='assigning" + options.rowId + "' value='0' readonly='true' />" + 
    "<input type='hidden' id='assigningHid"+rowid+"' value="+ rowObject[4]+" />";
}

function RevertProcesserInvoice(rowid) {
    var row = jQuery("#jqGrid2").jqGrid('getRowData', rowid);
    var status = row.Status;
    var oemid = $("#assignedforoems" + rowid).val();
    if (status == "Assigned") {
        var assignQual = $("#assigning" + rowid).val();
        jQuery("#jqGrid2").jqGrid('setRowData', rowid, { Status: 0, AssignedFor: 0, AvailableWeight: assignQual });
        $("#assigning" + rowid).val(0);
        var oldRecQual = $("#Rec" + oemid).val();
        var newRecQual = Number(ToNumber(oldRecQual)) - Number(ToNumber(assignQual));
        $("#Rec" + oemid).val(newRecQual.toFixed(2));

        var currentAssignWeight = $("#AssigningWeight").text();
        var newAssigningWeight = Number(ToNumber(currentAssignWeight)) - Number(ToNumber(assignQual));
        $("#AssigningWeight").html("<b>" + commafy(newAssigningWeight.toFixed(2)) + "</b>");
    }
}


function OEMCheckBoxClick(rowid, recommWeight, isPermanent) {
    var chkbox = document.getElementById("oemCheck" + rowid);
    if (chkbox.checked) {
        $("#Rec" + rowid).val(recommWeight);
        AutoReconcileWeight(recommWeight, rowid, isPermanent);
    }
    else {
        LostReconcileWeight(recommWeight, rowid);
    }
    TotalAssigningWeight();
}

function blurReconValue(rowid) {
    var assigningWeight = $("#Rec" + rowid).val();
    AutoReconcileWeight(assigningWeight, rowid);
    TotalAssigningWeight();
}

function TotalAssigningWeight() {
    var rows = jQuery("#jqGrid").jqGrid('getRowData');
    var totalWeight = 0;
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var rowid = row.OEMId;
        var chkbox = document.getElementById("oemCheck" + rowid);
        if (chkbox.checked) {
            var weight = $("#Rec" + rowid).val();
            totalWeight = totalWeight +  Number(weight);
        }
    }
    $("#AssigningWeight").html("<b>" + commafy(totalWeight.toFixed(2)) + "</b>");
}

function LostReconcileWeight(Weight, OEMId) {
    var rows = jQuery("#jqGrid").jqGrid('getRowData');
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var rowid = row.OEMId;
        $("#Rec" + rowid).val(0);
        var chkbox = document.getElementById("oemCheck"+rowid);
        if (chkbox.checked) {
            chkbox.checked = false;
        }
    }
    var stateId = $("select#StateId").val();
    var planYear = $("select#PlanYear").val();
    jQuery("#jqGrid2").setGridParam({ postData: { stateId: state, planYear: planyear }, page: 1 }).trigger("reloadGrid");

}

function AutoReconcileWeight(Weight,OEMId,isPermanent) {
    var rows = jQuery("#jqGrid2").jqGrid('getRowData');
    var remainedQual = Number(Weight);
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var rowid = row.InvoiceId;
        var status = row.Status;
        var assignedfor = row.AssignedFor;
        var processedDate = row.ProcessedDate;
        var collectionMethod = row.CollectionMethod;
        if (isPermanent == true && collectionMethod != "Permanent drop off location") {
            continue;
        }
        if (status == "Assigned") {
            continue;
        }
        var availableWeight = row.AvailableWeight;

        var chkbox = document.getElementById("InvoiceCheck" + rowid);
        if (remainedQual <= 0) {
            chkbox.checked = false;
            $("#assigning" + rowid).val(0);
            continue;
        }

        $("#assigning" + rowid).val(commafy(row.AvailableWeight));
        var aval = Number(ToNumber(row.AvailableWeight));
        jQuery("#jqGrid2").jqGrid('setRowData', rowid, { Status: 1, AssignedFor: OEMId, AvailableWeight: 0 });
        $("#assignedforoems" + rowid).val(OEMId);
        $("#hidassignedforoems" + rowid).val(OEMId);
        var qual = remainedQual - aval;
        if (aval != 0)
            chkbox.checked = true;
        else
            chkbox.checked = false;
        if (qual < 0) {
            var remQual = remainedQual.toFixed(2);
            $("#assigning" + rowid).val(commafy(remQual));
            if (rowid.indexOf("_") > 0) {
                var arr = rowid.split("_");
                var id = Number(arr[1]);
                var newid = id + 1;
                if (newid < 10) {
                    newid = "0" + newid;
                }
                var newInvoiceId = arr[0] + "_" + newid ;

                var newaval = Number(aval) - Number(remainedQual);
                newaval = commafy(newaval.toFixed(2));
                jQuery("#jqGrid2").jqGrid('setRowData', rowid, { checkbox: true, InvoiceId: rowid, Status: 1, AssignedFor: OEMId, ProcessedDate: processedDate, CollectionMethod: collectionMethod, AvailableWeight: 0, AssigningWeight: "" });
                jQuery("#jqGrid2").jqGrid('addRowData', newInvoiceId, { checkbox: true, InvoiceId: newInvoiceId, Status: 0, AssignedFor: "", ProcessedDate: processedDate, CollectionMethod: collectionMethod, AvailableWeight: newaval, AssigningWeight: "" }, "after", rowid);
                $("#assigning" + rowid).val(commafy(remQual));
                $("#assignedforoems" + rowid).val(OEMId);
                $("#hidassignedforoems" + rowid).val(OEMId);
                var chkbox = document.getElementById("InvoiceCheck" + rowid);
                chkbox.checked = true;
                
            }
            else {
                var newInvoiceId1 = rowid + "_" + "01";
                jQuery("#jqGrid2").jqGrid('addRowData', newInvoiceId1, { checkbox: true, InvoiceId: newInvoiceId1, Status: 1, AssignedFor: OEMId, ProcessedDate: processedDate, CollectionMethod: collectionMethod, AvailableWeight: "0", AssigningWeight: remQual }, "after", rowid);
                var chkbox = document.getElementById("InvoiceCheck" + newInvoiceId1);
                chkbox.checked = true;
                chkbox.readOnly = true;
                $("#assigning" + newInvoiceId1).val(commafy(remQual));
                $("#assignedforoems" + newInvoiceId1).val(OEMId);
                $("#hidassignedforoems" + newInvoiceId1).val(OEMId);

                var newInvoiceId2 = rowid + "_" + "02";
                var newaval = Number(aval) - Number(remainedQual);
                newaval = commafy(newaval.toFixed(2));
                jQuery("#jqGrid2").jqGrid('addRowData', newInvoiceId2, { checkbox: true, InvoiceId: newInvoiceId2, Status: 0, AssignedFor: "", ProcessedDate: processedDate, CollectionMethod: collectionMethod, AvailableWeight: newaval, AssigningWeight: "0" }, "after", newInvoiceId1);
                var chkbox2 = document.getElementById("InvoiceCheck" + newInvoiceId2);
                $("#assigning" + newInvoiceId2).val(0);
                jQuery("#jqGrid2").jqGrid('delRowData', rowid);
            }
        }
        remainedQual = remainedQual - aval;
    }

}



function resetValue() {
    var rows = jQuery("#jqGrid2").jqGrid('getRowData');
    var totalAssigningWeight=0;
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var rowid = row.InvoiceId;
        var currentRowValue = $("#assigning" + rowid).val();
        var availableWeight = row.AvailableWeight;
        if (Number(currentRowValue) > Number(availableWeight)) {
            alert("Assigning weight should be less than available weight.");
            var hidval = $("#assigningHid" + rowid).val();
            $("#assigning" + rowid).val(hidval);
            return;
        }
        $("#assigningHid" + rowid).val(currentRowValue);
        var chkbox = document.getElementById("InvoiceCheck" + rowid);
        if (chkbox.checked) {
            totalAssigningWeight = totalAssigningWeight + Number(currentRowValue);
        }
    }
    $("#AssigningWeight").html(totalAssigningWeight.toFixed(2));

    var totalTargetWeight = $("#hidTotalTargetWeight").val();
    var rowsTarget = jQuery("#jqGrid").jqGrid('getRowData');
    for (var i = 0; i < rowsTarget.length; i++) {
        var row = rowsTarget[i];
        var rowid = row.OEMId;
        var currentTargetWeight = row.TargetWeight;
        var currentRemainingWeight = row.RemainingTargetWeight;
        var assigningWeight = ((Number(currentTargetWeight) / Number(totalTargetWeight)) * totalAssigningWeight).toFixed(2);
        if (Number(currentRemainingWeight) > 0) {
            if (Number(assigningWeight) < Number(currentRemainingWeight)) {
                $("#Rec" + rowid).val(assigningWeight);
            }
            else {
                $("#Rec" + rowid).val(currentRemainingWeight);
            }
        }
        else {
            $("#Rec" + rowid).val(0);
        }
    }
}

function commafy(num) {
    num = num + "";
    var re = /(-?\d+)(\d{3})/
    while (re.test(num)) {
        num = num.replace(re, "$1,$2")
    }
    return num;
}
function ToNumber(num) {
    while (num.indexOf(",") > 0) {
        num = num.replace(",", "");
    }
    return num;
}

$('input[id$=btnSubmit]').click(function () {
    $("#resultSummary").text("Reconciling...");
    var stateId = $("select#StateId").val();
    var planYear = $("select#PlanYear").val();

    var strAssignQual = "";
    var rows = jQuery("#jqGrid2").jqGrid('getRowData');
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var rowid = row.InvoiceId;
        var status = row.Status;
        if (status == "Assigned") {
            var assignedfor = $("#assignedforoems" + rowid).val();
            var assignQual = $("#assigning" + rowid).val();
            var oemRecQual = $("#Rec" + assignedfor).val();
            var InvoiceQual = rowid + "#" + assignQual + "#" + assignedfor + "#" + oemRecQual;
            strAssignQual = strAssignQual + InvoiceQual;
            strAssignQual = strAssignQual + ";";
        }
    }

    var rootPath = window.location.protocol + "//" + window.location.host;
    $.ajax({
        url: rootPath + '/Reconciliation/ExeReconcile/',
        dataType: 'json',
        type: "POST",
        data: ({ StateId: stateId, PlanYear: planYear, AssignQual: strAssignQual }),
        async: false,
        success: function (result) {
            if (result.AccessDenied == "AccessDenied") {
                var rootPath = window.location.protocol + "//" + window.location.host;
                var url = rootPath + '/Home/AccessDenied';
                window.location.href = url;
            }
            else {
                if (result) {
                    $("#resultSummary").text("Reconcile succeed.");
                    LoadReconciliationData();
                }
                else {
                    $("#resultSummary").text("Reconcile failed.");
                }
            }
        }
    });

});