﻿$(document).ready(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        
        //'Accreditation dropdown onchange function' for getting name and description against a particular Accreditation

    $('select[id$=ddlAccreditation]').change(function (elem) {



        $('input[id$=txtName]').css("background-color", "white");
        $("label#lblMessage").text("");
        $("input#txtName").val("");
        $("textarea#txtDesc").val("");
        $("ul#message").css("visibility", "hidden");


        accreditation = $("select#ddlAccreditation").val();

        $.ajax({
            url: rootPath + '/Accreditation/GetDetail',
            dataType: 'json',
            type: "POST",
            data: ({ accreditation: accreditation }),
            async: false,
            success: function (result) {
                $("input#txtName").val(result.Name);
                $("textarea#txtDesc").val(result.Desc);
            }
        });
    });

    //'Clear button onclick function' for clearing all necessary fields

    $('input[id$=btnClear]').click(function () {
        $("input#txtName").val("");
        $("textarea#txtDesc").val("");
        $("select#ddlAccreditation").val(0);
        $("label#lblMessage").text("");
        $('input[id$=txtName]').css("background-color", "white");
        $("ul#message").css("visibility", "hidden");

    });


    //'Submit button onclick function' for submitting necessary data to the controller action

    $('input[id$=btnSubmit]').click(function () {


        if ($('input[id$=txtName]').val() == "" || $.trim($('input[id$=txtName]').val()) == '') {

            $('input[id$=txtName]').css("background-color", "#FFEEEE");
            $("label#lblMessage").text("Name is required").css("color", "Red");
            $("ul#message").css("visibility","visible");
            return false;
        }
        else {

            $('input[id$=txtName]').css("background-color", "white");
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");

            accreditation = $("select#ddlAccreditation").val();
            name = $("input#txtName").val();
            desc = $("textarea#txtDesc").val();

            $.ajax({
                url: rootPath + '/Accreditation/Save',
                dataType: 'json',
                type: "POST",
                data: ({ accreditation: accreditation, name: name, desc: desc }),
                async: false,
                success: function (result) {
                    if (result.AccessDenied == "AccessDenied") {
                        var rootPath = window.location.protocol + "//" + window.location.host;
                        var url = rootPath + '/Home/AccessDenied';
                        window.location.href = url;
                    }
                    else {
                        $("label#lblMessage").text(result.message);
                        $("ul#message").css("visibility", "visible");
                        if (result.data) {
                            $("select#ddlAccreditation").fillSelect(result.data);
                            $("select#ddlAccreditation").val(result.selected);
                        }
                    }
                }
            });
        }



    });
});

//Text area max length 

function imposeMaxLength(obj, maxLen) {
    return (obj.value.length <= maxLen);
}

