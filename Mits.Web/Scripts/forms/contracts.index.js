﻿$(document).ready(function () {

   $("button, input:submit, input:button").button();


    jQuery("#jqGrid").jqGrid({
        url: "/Contracts/GetDocument",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id', 'File Name', 'Uploaded Date', '  '],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
   		{ name: 'Upload File Name', index: 'Upload File Name', width: 130, editable: true, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Upload File Date', index: 'Upload File Date', width: 125, align: "left", sortable: false, editable: true,
   		    editoptions: {
   		        dataInit: function (elem) { $(elem).datepicker({ buttonImage: '../../Content/images/calendar.gif', altFormat: 'mm/dd/yyyy', buttonImageOnly: true }); }
   		    }
   		},
   		{ name: 'AttachmentPath', index: 'AttachmentPath', sortable: false, formatter: myformatter, title: true, align: 'center', width: 25, editable: false }
  		
   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 900,
        height: 230,
        editurl: "PerformAction",
        afterInsertRow: function (rowid, rowdata, rowelem) {

        }

    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();

});

datePick = function (elem) {
    jQuery(elem).datepicker();
}

function myformatter(cellvalue, options, rowObject) {

    return "<img src='../../Content/images/d4.PNG'  alt='Open selected file'  title='Open selected file'  id='btnDel'   onclick=\"Download('" + cellvalue + "');\"    />";

}
function Download(cellvalue) {
    if (cellvalue != "-") {
        //window.location = "../DocumentLib/Download?FileName=" + cellvalue;
        //       $.ajax({
        //           url: '/DocumentLib/Download',
        //           dataType: 'json',
        //           type: "POST",
        //           data: { cellvalue: cellvalue},
        //           async: false,
        //           success: function (result) {
        ////               $('#jqGrid').setColProp('State', { editoptions: { value: result.states} });
        ////               GetUserAccessRights();
        //           }
        //       });
        var rootPath = window.location.protocol + "//" + window.location.host;
        cellvalue = cellvalue.replace('#', '<~>');
        var url = rootPath + '/Contracts/Download?FileName='+ cellvalue;
        window.location.href = url;
    }
}

//function GetUserAccessRights() {
//    $.ajax({
//        url: '/DocumentLib/GetUserAccessRights',
//        dataType: 'json',
//        type: "POST",
//        data: {},
//        async: false,
//        success: function (result) {
//            ShowHideCommandButton(result.showEdit, result.showDelete);
//        }
//    });
//}

//function ShowHideCommandButton(showEdit, showDelete) {

//    if (showEdit == false && showDelete == false) {
//        $("#jqGrid").hideCol('ActionMenu');
//    }
//    else {
//        if (showEdit == false)
//            $("#jqGrid").find('div > img').filter(".edit").css("display", "none");
//        if (showDelete == false)
//            $("#jqGrid").find('div > img').filter(".delete").css("display", "none");
//    }
//}


