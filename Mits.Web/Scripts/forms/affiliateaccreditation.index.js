﻿
$(document).ready(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;


    //'Affiliate dropdown onchange fucntion' for getting detail of accreditations against a particular affiliate

    $('select[id$=ddlAffiliate]').change(function (elem) {
        affiliate = $("select#ddlAffiliate").val();

        if (affiliate == "0") {
            $("input[name=selectedObjects]").attr('checked', false);
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");
        }
        else {
            $("input[name=selectedObjects]").attr('checked', false);
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");
            $.ajax({
                url: rootPath + '/AffiliateAccreditation/GetAccreditation',
                dataType: 'json',
                type: "POST",
                data: ({ affiliate: affiliate }),
                async: false,
                success: function (result) {
                    if (result.data) {

                        $("input[name=selectedObjects]").attr('checked', false);
                        $.each(result.data, function (index, item) {


                            $("input[id=" + item.Value + "]").attr('checked', true);


                        });
                    }

                }
            });
        }
    });

    //'Clear button onclick function' for clearing necessary fields

    $('input[id$=btnClear]').click(function () {
        $("select#ddlAffiliate").val(0);
        $("input[name=selectedObjects]").attr('checked', false);
        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");
    });


    //'Submit button onclick function' for submitting data to controller action

    $('input[id$=btnSubmit]').click(function () {


        affiliate = $("select#ddlAffiliate").val();
        if (affiliate == "0") {
            $("label#lblMessage").text("No Affiliate Selected").css("color", "Red");
            $("ul#message").css("visibility", "visible");


            return;
        }
        if ($("input:checkbox:checked").length == 0) {
            $("label#lblMessage").text("No Accreditation Selected").css("color", "Red");
            $("ul#message").css("visibility", "visible");


            return;
        }

        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");

        var accreditations = "";
        $("input:checkbox:checked").each(function () {
            if (accreditations == "")
                accreditations = $(this).val();
            else
                accreditations = accreditations + ',' + $(this).val();


        });

        $.ajax({
            url: rootPath + '/AffiliateAccreditation/Save',
            dataType: 'json',
            type: "POST",
            data: ({ affiliate: affiliate, accreditations: accreditations }),
            async: false,
            success: function (result) {
                if (result.AccessDenied == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else {
                    $("label#lblMessage").text(result.message);
                    $("ul#message").css("visibility", "visible");
                    if (result.data) {


                        $("select#ddlAffiliate").fillSelect(result.data);
                        $("select#ddlAffiliate").val(result.selected);
                        $("input#selectedObjects").attr('checked', true);

                    }
                }
            }
        });


    });
});

