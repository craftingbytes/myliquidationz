﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;

    $("button, input:submit, input:button").button();

    jQuery("#jqGrid").jqGrid({
        url: "/AccountReceivable/GetReceivables",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id','OEM', 'Payment Date', 'Payment Method', 'Reference #', 'Amount', 'Action'],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
        { name: 'OEM', index: 'OEM', width: 100, hidden: false },
   		{ name: 'Payment Date', index: 'Payment Date', width: 80, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
        { name: 'Payment Method', index: 'Payment Method', width: 75, editable: false, sortable: false },
   		{ name: 'Refernce #', index: 'Refernce #', width: 85, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Amount', index: 'Amount', width: 75, editable: false, align: 'right', sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125 } },
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: myformatter, title: true, align: 'center', width: 30, editable: false }

   	],
        rowNum: 100,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 230,
        editurl: "PerformAction",
        afterInsertRow: function (rowid, rowdata, rowelem) {

        }


    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();

});

//$("#txtSearch").keypress(function (event) {
//    if (event.keyCode == '13') {

//        var searchVal = $("#txtSearch").val();
//        //state = $("#state option:selected").text();            
//        jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal }, page: 1 }).trigger("reloadGrid");

//    }

//});
$("#btnSearch").click(function () {
    var receivedVal = $("#ddlReceived").val();
    var dateFrom = $("#txtFrom").val();
    var dateTo = $("#txtTo").val();
    if (dateFrom > dateTo) {
        alert("From Date should be less than To Date");
        return false;
    }

    //state = $("#state option:selected").text();            
    jQuery("#jqGrid").setGridParam({ postData: { searchReceivable: receivedVal, searchDateTo: dateTo, searchDateFrom: dateFrom }, page: 1 }).trigger("reloadGrid");
});

$('input[id$=btnAdd]').click(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/AccountReceivable/Edit';
    window.location.href = url;




});
function editAccountReceivable(t) {

    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/AccountReceivable/Edit/' + t;
    window.location.href = url;

}

function deletePayment(rowId) {
    $('#jqGrid').delGridRow(rowId, { reloadAfterSubmit: true, url: 'DeletePayment', afterSubmit: function (response, postdata) {

        if (response.responseText == "Success") {
            alert("Success:Payment Deleted");

            return [true];
        }
        else {
            alert("Delete Failed:" + response.responseText);
            return [false];
        }

    }
    });
}

$(function () {

    //    $("#txtTo").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#txtTo").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
    $("#txtFrom").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
});

function myformatter(cellvalue, options, rowObject) {
    return "<div><a href='javascript:deletePayment(" + rowObject[0] + ")'><img src='../../Content/images/icon/delete.png'  alt='Delete'  title='Delete Payment' style='border:none;'/> </a> <a href='javascript:editAccountReceivable(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Account Receivable'  style='border:none;'/> </a>  </div>";

}

