﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;

    function ActionMenuFormatter(cellvalue, options, rowdata) {
        var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")' class='delete'/></div>";
        actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
        return actionMenuHtml;
    }

    if ($('#IsOEM').val() != 'True') {
        $('select[id$=ddlStates]').clearSelect();
    } else {
        $('#trAffiliate').css('display', 'none');
    }

    $("button, input:submit, input:button").button();

    jQuery("#jqGrid").jqGrid({
        url: "/Invoice/GetInvoices",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Invoice Id', 'Entity Name', 'Invoice Date', 'State', 'Amount', 'Receivable', '', 'Actions'],
        colModel: [
   		{ name: 'Invoice Id', index: 'Invoice Id', width: 50, hidden: false },
   		{ name: 'Entity', index: 'Entity', width: 125, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100 }, hidden: ($('#IsOEM').val() == 'True' ? true : false) },
        { name: 'Invoice Date', index: 'Invoice Date', width: 65, editable: false, sortable: false },
   		{ name: 'State', index: 'State', width: 85, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Amount', index: 'Amount', width: 65, editable: false, align:'right', sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125} },
        { name: 'Receivables', index: 'Receivables', width: 65, hidden: true, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125} },
        { name: 'Entity Type', index: 'Entity Type', width: 50, hidden: true },
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: myformatter, title: true, align: 'center', width: 40, editable: false }


   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 230,
        editurl: "PerformAction",
        afterInsertRow: function (rowid, rowdata, rowelem) {

        }


    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();

    $("#dlgAdjustment").dialog({
        modal: true,
        width: 800,
        height: 240,
        zIndex: 500,
        resizable: false,
        position: 'center',
        autoOpen: false,
        show: 'fold',
        hide: 'fold',
        Ok: function () {
            $(this).dialog('close');
        }
    });

    $("#dlgAdjBtnCancel").click(function () {
        $('#dlgAdjustment').dialog('close'); ;
        $("#txtCollectorAdjDesc").val('');
        $("#txtCollectorAdjAmount").val('');

    });

    $("#dlgAdjBtnSave").click(function () {
        AddAdjustment();
    });

});

$('select[id$=ddlAffiliates]').change(function (elem) {

    affiliate = $('select[id$=ddlAffiliates]').val();
    if (affiliate == "0") {
        $('select[id$=ddlStates]').clearSelect();
    }

    else {

        $('select[id$=ddlStates]').clearSelect();
        $.ajax({
            url: "/Invoice/GetEntityStates",
            dataType: 'json',
            type: "POST",
            data: ({ affiliate: affiliate }),
            async: false,
            success: function (result) {
                if (result._invoiceList) {
                    $("select#ddlStates").fillSelect(result._invoiceList);
                }
            }
        });
    }
});

//$("#txtSearch").keypress(function (event) {
//    if (event.keyCode == '13') {

//        var searchVal = $("#txtSearch").val();
//        //state = $("#state option:selected").text();            
//        jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal }, page: 1 }).trigger("reloadGrid");

//    }

//});
$("#btnSearch").click(function () {
    var entityVal = $("#ddlAffiliates").val();
    var invoiceVal = $("#txtInvoice").val();
    var receivableVal = $("#receivables").attr('checked');
    var stateVal = $("#ddlStates").val();
    var dateFrom = $("#txtFrom").val();
    var dateTo = $("#txtTo").val();
   //state = $("#state option:selected").text();            
    jQuery("#jqGrid").setGridParam({ postData: { searchEntity: entityVal, searchInvoice: invoiceVal, searchReceivable: receivableVal, searchState: stateVal, searchDateTo: dateTo, searchDateFrom: dateFrom }, page: 1 }).trigger("reloadGrid");
});

$('input[id$=btnAdd]').click(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/Invoice/Edit';
    window.location.href = url;




});

function editInvoice(t) {

    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/Invoice/Edit/' + t;
    window.location.href = url;

}


function deleteInvoice(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });

    if ($('#IsOEM').val() == 'True') {
        alert("Access Denied");
    }
    else {
        $('#jqGrid').delGridRow(rowId, { reloadAfterSubmit: true, url: 'DeleteInvoice', afterSubmit: function (response, postdata) {
            if (response.responseText == "Success") {
                alert("Success:Invoice Deleted");
                return [true];
            }
            else {
                alert("Delete Failed:" + response.responseText);
                return [false];
            } 
        } 
        });
    }
}





$(function () {

//    $("#txtTo").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#txtTo").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
    $("#txtFrom").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
});  

function myformatter(cellvalue, options, rowObject) {
    var InvoiceNumber = rowObject[0];
    var type = rowObject[6];
    if ($('#IsOEM').val() == 'True') {
        var url = "return showReport('../Reports/OEMInvoice.aspx?InvoiceID=" + InvoiceNumber + "', 'InvoiceReport');";
        return "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
    }
    else if (type == "OEM" && InvoiceNumber <= "2047") {
        var url = "return showReport('../Reports/OEMInvoice.aspx?InvoiceID=" + InvoiceNumber + "', 'InvoiceReport');";
        //return "<div><a href='javascript:editInvoice(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Invoice Entity'  style='border:none;'/> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
        return "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
    }
    else if (type == "OEM") {
        var url = "return showReport('../Reports/OEMInvoice.aspx?InvoiceID=" + InvoiceNumber + "', 'InvoiceReport');";
        return "<div><a href='javascript:deleteInvoice(" + rowObject[0] + ")'><img src='../../Content/images/icon/delete.png'  alt='Delete'  title='Delete Invoice Entity'  style='border:none;'/> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:displayAddAdjustment(\"" + InvoiceNumber + "\")' href='#' ><img src='../../Content/images/icon/sm/plus.gif'  alt='Add Adjustment'  title='Add Adjustment'  style='border:none;'/></a></div>";
        
        //return "<div><a href='javascript:editInvoice(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Invoice Entity'  style='border:none;'/> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
        //return "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
    }
    //else {
    //    var url = "return showReport('../Reports/RecyclerInvoice.aspx?InvoiceID=" + InvoiceNumber + "', 'InvoiceReport');";
    //    return "<div><a href='javascript:editInvoice(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Invoice Entity'  style='border:none;'/> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
    //    //return "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
    //} 
}


function displayAddAdjustment(invoiceNumber) {
    $("#currentInvoice").val(invoiceNumber);
    $("#dlgAdjustment").dialog('open');
}

function AddAdjustment() {
    if (!ValidateAdjDetailInputs()) {
        return;
    }
    var invoiceId = $("#currentInvoice").val();
    var adjDesc = $("#txtAdjustmentDesc").val();
    var adjAmount = $("#txtAdjustmentAmount").val();
    var fAdjAmount = parseFloat(adjAmount);
    $.ajax({
        url: '/Invoice/AddAdustment',
        dataType: 'json',
        type: "POST",
        data: ({ InvoiceId: invoiceId, Description: adjDesc, Amount: fAdjAmount }),
        async: false,
        success: function (result) {
            if (result.message == "AccessDenied") {
                var rootPath = window.location.protocol + "//" + window.location.host;
                var url = rootPath + '/Home/AccessDenied';
                window.location.href = url;
            }
            else if (result.status == "True") {
                if (result.message != '') {
                    alert(result.message);
                    $('#dlgAdjustment').dialog('close');
                    $("#txtAdjustmentDesc").val('');
                    $("#txtAdjustmentAmount").val('');
                }
                jQuery("#jqGrid").trigger("reloadGrid");
            } else {
                alert(result.message);
            }
        }
    });
}

function ValidateAdjDetailInputs() {
    var invoiceId = $("#currentInvoice").val();
    var collectorAdjDesc = $("#txtAdjustmentDesc").val();
    var adjAmount = $("#txtAdjustmentAmount").val();
    var fAdjAmount = parseFloat(adjAmount);
    var errIndex = 0;
    var errMsg = '';
    if (collectorAdjDesc < 1) {
        errIndex++;
        errMsg += errIndex + ". Description is required.\n";
    }
    if (isNaN(fAdjAmount)) {
        errIndex++;
        errMsg += errIndex + ". Amount not a valid Quantity.\n";
    }
    if (errMsg != '') {
        alert(errMsg);
        return false;
    }
    return true;
}