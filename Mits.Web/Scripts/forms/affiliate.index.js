﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;
    var eventsMenu = {
        bindings: {
            'editEntity': function (t) {
                var url = rootPath + '/Affiliate/Edit/' + t.id;
                window.location.href = url;
                //alert('Edit Affiliate ' + t.id);
            },
            //             'deleteEntity': function (t) {
            //                var url = rootPath + '/Affiliate/Delete/' + t.id;
            //                window.location.href = url;
            //                //alert('Edit Affiliate ' + t.id);
            //            },
            'editUsers': function (t) {
                var url = rootPath + '/AffiliateContact/Index/' + t.id;
                window.location.href = url;
                //alert('Add/Edit Affiliate Contacts ' + t.id);
            },
            'editContractRates': function (t) {
                var url = rootPath + '/AffiliateContractRate/Index/' + t.id;
                window.location.href = url;
            },
            'editTargets': function (t) {
                var url = rootPath + '/AffiliateTarget/Index/' + t.id;
                window.location.href = url;
            },
            'editEntityState': function (t) {
                var url = rootPath + '/EntityState/Index/' + t.id;
                window.location.href = url;
            },
            'showMap': function (t) {
                var url = rootPath + '/Affiliate/Map/' + t.id;
                window.location.href = url;
            }


        }
    };

    $("button, input:submit, input:button").button();

    var searchVal = '';
    var data = eval('(' + $("#Data").val() + ')');
    if (data.name) {
        searchVal = data.name;
        $("#txtSearch").val(data.name);

    }
    var entity = '0';
    if (data.type) {
        entity = data.type;
        $("#EntityTypeId").val(data.type);
    }
    var page = 1;
    var rows = 50;
    if (data.page) {
        page = data.page;
    }
    if (data.rows) {
        rows = data.rows;
    }

    jQuery("#jqGrid").jqGrid({
        url: "/Affiliate/GetAffiliates",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id', 'Entity Name', 'Entity Type', 'Address', 'State', 'Phone', 'Actions'],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
   		{ name: 'Entity Name', index: 'Entity Name', width: 130, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
        { name: 'Entity Type', index: 'Entity Type', width: 75, editable: false, sortable: false },
   		{ name: 'Address', index: 'Address', width: 130, editable: false, sortable: false },
   		{ name: 'State', index: 'State', width: 65, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Phone', index: 'Phone', width: 60, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125} },
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: myformatter, title: true, align: 'center', width: 80, editable: false }

   	],
        rowNum: rows,
        page: page,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 230,
        editurl: "/Affiliate/PerformAction",
        postData: { searchString: searchVal, searchEntity: entity }
        //,
        //afterInsertRow: function (rowid, rowdata, rowelem) {
        //    $('#' + rowid).contextMenu('MenuJqGrid', eventsMenu);
        //}
    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();

});
//$("#txtSearch").keyup(function () {
//    var elem = $("#txtSearch").val();
//    LoadEntities(elem);
//});

var availableTags=[ "saad",
                    "yousaf",
                    "Ahsan",
                    "Mahar",
                    "Umer"
                    ];
$("#txtSearch").autocomplete({
     source: availableTags
  
});

$("#txtSearch").keypress(function (event) {
    if (event.keyCode == '13') {

        var searchVal = $("#txtSearch").val();
        var entity = $("#EntityTypeId").val();
        //state = $("#state option:selected").text();            
        jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal, searchEntity: entity }, page: 1 }).trigger("reloadGrid");

    }

});
$("#btnSearch").click(function () {
    var searchVal = $("#txtSearch").val();
    var entity = $("#EntityTypeId").val();
    //state = $("#state option:selected").text();            
    jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal, searchEntity: entity }, page: 1 }).trigger("reloadGrid");
});

$('input[id$=btnAdd]').click(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/Affiliate/Create/';
        window.location.href = url;
});

    function editEntity(t) {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/Affiliate/Edit/' + t;
        var name = $("#jqGrid").getGridParam("postData").searchString;
        var type = $("#jqGrid").getGridParam("postData").searchEntity;
        var page = $("#jqGrid").getGridParam("page");
        var rows = $("#jqGrid").getGridParam("rowNum");
        url = url + '?data=name:"' + name + '",type:"' + type + '",page:"' + page + '",rows:"' + rows + '"';
        window.location.href = url;
    }
    function deleteEntity(rowId) {
        $('#jqGrid').delGridRow(rowId, { delData: { InvoiceItemID: $("#jqGrid").getRowData(rowId).Id} });
        
//        var rootPath = window.location.protocol + "//" + window.location.host;
//        var url = rootPath + '/Affiliate/Edit/' + t;
//        window.location.href = url;
       
    }
    function editUsers(t) {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/AffiliateContact/Index/' + t;
        var name = $("#jqGrid").getGridParam("postData").searchString;
        var type = $("#jqGrid").getGridParam("postData").searchEntity;
        var page = $("#jqGrid").getGridParam("page");
        var rows = $("#jqGrid").getGridParam("rowNum");
        url = url + '?data=name:"' + name + '",type:"' + type + '",page:"' + page + '",rows:"' + rows + '"';
        window.location.href = url;
    }
    function editContractRates(t) {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/AffiliateContractRate/Index/' + t;
        var name = $("#jqGrid").getGridParam("postData").searchString;
        var type = $("#jqGrid").getGridParam("postData").searchEntity;
        var page = $("#jqGrid").getGridParam("page");
        var rows = $("#jqGrid").getGridParam("rowNum");
        url = url + '?data=name:"' + name + '",type:"' + type + '",page:"' + page + '",rows:"' + rows + '"';
        window.location.href = url;
    }
    function editTargets(t) {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/AffiliateTarget/Index/' + t;
        var name = $("#jqGrid").getGridParam("postData").searchString;
        var type = $("#jqGrid").getGridParam("postData").searchEntity;
        var page = $("#jqGrid").getGridParam("page");
        var rows = $("#jqGrid").getGridParam("rowNum");
        url = url + '?data=name:"' + name + '",type:"' + type + '",page:"' + page + '",rows:"' + rows + '"';
        window.location.href = url;
    }
    function editEntityState(t) {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/EntityState/Index/' + t;
        var name = $("#jqGrid").getGridParam("postData").searchString;
        var type = $("#jqGrid").getGridParam("postData").searchEntity;
        var page = $("#jqGrid").getGridParam("page");
        var rows = $("#jqGrid").getGridParam("rowNum");
        url = url + '?data=name:"' + name + '",type:"' + type + '",page:"' + page + '",rows:"' + rows + '"';
        window.location.href = url;
    }
    function showMap(t) {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/Affiliate/Map/' + t;
        var name = $("#jqGrid").getGridParam("postData").searchString;
        var type = $("#jqGrid").getGridParam("postData").searchEntity;
        var page = $("#jqGrid").getGridParam("page");
        var rows = $("#jqGrid").getGridParam("rowNum");
        url = url + '?data=name:"' + name + '",type:"' + type + '",page:"' + page + '",rows:"' + rows + '"';
        window.location.href = url;
    }
    function myformatter(cellvalue, options, rowObject) {
        return "<div><a href='javascript:editEntity(" + rowObject[0] 
        + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Entity'  style='border:none;'/> </a><a href='javascript:deleteEntity(" + rowObject[0]
        + ")'><img src='../../Content/images/icon/delete.png'  alt='Edit'  title='Delete Entity' style='border:none;'/> </a><a href='javascript:editUsers(" + rowObject[0]
        + ")'><img src='../../Content/images/icon/user.png'  alt='Edit'  title='Add/Edit Entity Users'  style='border:none;'/></a><a href='javascript:editContractRates(" + rowObject[0]
        + ")'><img src='../../Content/images/icon/pricing2.png'  alt='Edit'  title='Add/Edit Entity Contract Rates'  style='border:none;'/></a><a href='javascript:editTargets(" + rowObject[0]
        + ")'><img src='../../Content/images/icon/target2.png'  alt='Edit'  title='Add/Edit Entity Targets' style='border:none;'/> </a><a href='javascript:editEntityState(" + rowObject[0]
        + ")'><img src='../../Content/images/state.png'  alt='Edit'  title='Edit Entity States' style='border:none;'/> </a><a href='javascript:showMap(" + rowObject[0] 
        + ")'><img src='../../Content/images/icon/map.png'  alt='Edit'  title='Targets Map' style='border:none;'/> </a></div>";
    }

