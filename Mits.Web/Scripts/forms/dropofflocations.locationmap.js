﻿$(function () {
    var geocoder = new google.maps.Geocoder();

    var lat = $("#hfLat").val();
    var lng = $("#hfLng").val();

    var latlng = new google.maps.LatLng(lat, lng);

    if (latlng) {
        var myOptions = {
            /*zoom: 10,*/
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP

        };
        map = new google.maps.Map(document.getElementById("ConsumerLocationMapContainer"), myOptions);
        geocoder.geocode({ latLng: latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                createMarker(latlng); //create the user location marker
                //createMarker(latlngLoc);
                markLocation(latlng);
            } else {
                alert(lat + ' , ' + lng + ' not found');
               
            }
        });
    } else {
        innerHTML(document.getElementById('ConsumerLocationMapContainer'),
                "<br/>There was an error retrieving your map.&nbsp;&nbsp;" +
                "If <a href=\"javascript:location.reload(true)\">reloading or refreshing</a> this page does not solve this issue," +
                "Please contact us via the link at the bottom of the page.<br/>" +
                "Thanks<br/>" +
                "MITS Development Team.");
    }

});
        function markLocation(latlng) {
            var map_bounds = new google.maps.LatLngBounds();
            map_bounds.extend(latlng);    //make sure the users location is visible on the map
            var marker;

            var lat = $("#hfLat").val();
            var lng = $("#hfLng").val();
         
            var latlngLoc = new google.maps.LatLng(lat, lng);
            
            if (lng && lat) {
                map_bounds.extend(new google.maps.LatLng(lat, lng));
                marker = createMarker(new google.maps.LatLng(lat, lng), 'P', '');
            }
            map.fitBounds(map_bounds);

        }
        function openInfoWindow(marker,map){
            var markerLatLng = marker.getPosition();
            var tr = $('tr[longitude='+markerLatLng.lng()+'][latitude='+markerLatLng.lat()+']');
            td = $("td:nth-child(2)",tr);
            //debugger;
            marker.infoWindow.setContent(td.html());
            marker.infoWindow.open(map, marker);
        }
        function showHelp(elemId) {
            $("#" + elemId).dialog({
                modal: false,
                width: 400,
                resizable: false,
                position: 'center',
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }