﻿var manufacturerList = '';
var typeList = '';

var zip = '';
var city = '';
var state = '';
var latitude = '';
var longitude = '';

$(function () {

    $("button, input:submit, input:button, a#submit").button();

    initResultGrid();
});


function getNearestLocations(action) {

    var mfgrId = "";
    var productTypes = "";

    $.ajax({
        url: action,
        dataType: 'json',
        type: "POST",
        data: ({ lat: latitude, lgt: longitude, city: city, state: state, prods: productTypes, mfgr: mfgrId }),
        async: false,
        success: function (data) {

            initResultGrid();

            for (var i = 0; i <= data.length; i++) {
                $("#tblResults").jqGrid('addRowData', i + 1, data[i]);
                //alert(data[i]);
            }

            showResultsDialog();
        }
    });
}

function getNearestLocationsForSony(action) {

    var mfgrId = "";
    var productTypes = "";

    $.ajax({
        url: action,
        dataType: 'json',
        type: "POST",
        data: ({ lat: latitude, lgt: longitude, city: city, state: state, prods: productTypes, mfgr: mfgrId }),
        async: false,
        success: function (data) {

            initResultGrid();

            for (var i = 0; i <= data.length; i++) {
                $("#tblResults").jqGrid('addRowData', i + 1, data[i]);
            }

            showResultsDialogForSony();
        }
    });
}

function showMapLocations(lng, lat) {
    //$("#dlgDropOff").dialog('close');

    var form = document.forms[0];
    if (form) {
        form.target = "_blank";
        form.action = 'LocationMap?lng=' + lng + '&lat=' + lat;
        form.submit();
        
    }
}


function populateAddressAndResults() {
    zip = $("input[id$=txtZipCode]").val();

    var isValid = ValidateForm();

    if (isValid) {
        geocoder = new google.maps.Geocoder();

        geocoder.geocode({ 'address': ', ' + zip }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {

                var geoResults = results[0];


                city = getAddressPart(geoResults.address_components, 'city');
                state = getAddressPart(geoResults.address_components, 'state');


                point = geoResults.geometry.location;

                latitude = point.lat();
                longitude = point.lng();

                $("#hfAddressInfo").val(latitude + "+" + longitude + "+" + city + "+" + state);

                getNearestLocations('GetMichiganLocations');
            }
        });
    }
}

function populatePointForGetlalo() {

    geocoder = new google.maps.Geocoder();

    $("#listLocs option").each(function () {

        var add = this.value;

        geocoder.geocode({ 'address': ', ' + add }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                var geoResults = results[0];
                city = getAddressPart(geoResults.address_components, 'city');
                state = getAddressPart(geoResults.address_components, 'state');
                point = geoResults.geometry.location;
                latitude = point.lat();
                longitude = point.lng();

                saveLalo(add, point.lat(), point.lng());
            }
        });
    });
}

function saveLalo(add, lat, lng) {
    var mfgrId = "";
    var productTypes = "";

    $.ajax({
        url: 'SaveLaLo',
        dataType: 'json',
        type: "POST",
        data: ({ lat: lat, lgt: lng, address: add }),
        async: false,
        success: function () {

            //alert("Save Succeed!");
        }
    });

}

function populateAddressAndResultsForAcer() {
    zip = $("input[id$=txtZipCode]").val();

    var isValid = ValidateForm();

    if (isValid) {
        geocoder = new google.maps.Geocoder();

        geocoder.geocode({ 'address': ', ' + zip }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {

                var geoResults = results[0];


                city = getAddressPart(geoResults.address_components, 'city');
                state = getAddressPart(geoResults.address_components, 'state');


                point = geoResults.geometry.location;

                latitude = point.lat();
                longitude = point.lng();

              
                getNearestLocations('GetLocationsForAcer');
            }
        });
    }
}

function populateAddressAndResultsForSony() {
    zip = $("input[id$=txtZipCode]").val();

    var isValid = ValidateForm();

    if (isValid) {
        geocoder = new google.maps.Geocoder();

        geocoder.geocode({ 'address': ', ' + zip }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {

                var geoResults = results[0];


                city = getAddressPart(geoResults.address_components, 'city');
                state = getAddressPart(geoResults.address_components, 'state');


                point = geoResults.geometry.location;

                latitude = point.lat();
                longitude = point.lng();

                $("#hfAddressInfo").val(latitude + "+" + longitude + "+" + city + "+" + state);

                getNearestLocationsForSony('GetLocationsForSony');
            }
        });
    }
}

function showResultsDialog() {

    $("#dlgDropOff").dialog({
        width: '840px'
            , height: '600'
            , moveable: true
            , draggable: true
            , close: function () {
                $("#tblResults").GridUnload();
            }
    });

    $("#dlgDropOff").dialog('open');

    //setGridBackgroundcolorAccordingToLocationType();
}

function showResultsDialogForSony() {

    $("#dlgDropOff").dialog({
        width: '840px'
            , height: '550'
            , moveable: true
            , draggable: false
            , close: function () {
                $("#tblResults").GridUnload();
            }
    });

    $("#dlgDropOff").dialog('open');
     
    //setGridBackgroundcolorAccordingToLocationType();
    
}
function setGridBackgroundcolorAccordingToLocationType()
{
    var n = $("#tblResults").getGridParam("records");          
     
    for (var i = 0; i < n; i++) {
        var rowid = i+1;
        var aryLocType = $("#" + rowid).children("td").eq(3).html();
       // for (var j = 0; j < aryLocType.length; ++j) {
            var eleImg = '&nbsp;<img src="../../Content/images/0.gif">';
            if (aryLocType.indexOf('1') != -1) {
                //eleImg = '&nbsp;<img src="../../Content/images/1.gif">';
                $("#" + rowid).children("td").eq(2).append('&nbsp;<img src="../../Content/images/1.gif">'); //css("background-color", "#edfbfd"); 
            }
            else {
                $("#" + rowid).children("td").eq(2).append(eleImg); //css("background-color", "#edfbfd"); 
            }

            if (aryLocType.indexOf('2') != -1) {
                //eleImg = '&nbsp;<img src="../../Content/images/2.gif">';
                $("#" + rowid).children("td").eq(2).append('&nbsp;<img src="../../Content/images/2.gif">'); //css("background-color", "#f1ffe9"); 
            }
            else {
                $("#" + rowid).children("td").eq(2).append(eleImg); //css("background-color", "#edfbfd"); 
            }

            if (aryLocType.indexOf('3') != -1) {
                //eleImg = '&nbsp;<img src="../../Content/images/3.gif">';
                $("#" + rowid).children("td").eq(2).append('&nbsp;<img src="../../Content/images/3.gif">'); //css("background-color", "#faeee7"); 
            }
            else {
                $("#" + rowid).children("td").eq(2).append(eleImg); //css("background-color", "#edfbfd"); 
            }

            if (aryLocType.indexOf('4') != -1) {
                //eleImg = '&nbsp;<img src="../../Content/images/4.gif">';
                $("#" + rowid).children("td").eq(2).append('&nbsp;<img src="../../Content/images/4.gif">'); //css("background-color", "#efe8fb"); 
            }
            else {
                $("#" + rowid).children("td").eq(2).append(eleImg); //css("background-color", "#edfbfd"); 
            }

        //}
    }

}

function initResultGrid() {
    $("#tblResults").jqGrid(
                    {
                        url: 'Default1.aspx', //Dummy URL
                        datatype: "local",
                        mtype: 'GET',
                        colNames: ['Location', 'Distance', 'Type', 'LocationType', 'View on Map', 'Long', 'Lat'],
                        colNames: ['Location', 'Distance', 'View on Map', 'Long', 'Lat'],
                        colModel: [
                                    { name: 'Location', index: 'Location', width: 450, sortable: false, title: false },
                                    { name: 'Distance', index: 'Distance', width: 100, sortable: false, title: false },
                                    //{ name: 'LocationTypeDisplay', index: 'LocationTypeDisplay', width: 130, sortable: false, title: false },
                                    //{ name: 'LocationType', index: 'LocationType', width: 130, sortable: false, hidden: true, title: false },
                                    { name: 'ViewOnMap', index: 'ViewOnMap', width: 130, sortable: false, title: false },
                                    { name: 'Longitude', index: 'Longitude', width: 0, sortable: false, hidden: true, title: false },
                                    { name: 'Latitude', index: 'Latitude', width: 0, sortable: false, hidden: true, title: false }
                        ],
                        jsonReader: {
                            repeatitems: false
                        },
                        rowNum: 10,
                        recordpos: 'left',
                        viewrecords: true,
                        width: 650,
                        height: 450,
                        hoverrows: false
                    });
}

function MailBack() {
    var form = document.forms[0];
    if (form)
        form.action = 'MailBack/Index';
    form.submit();
}

function showHelp(elemId) {
    $("#" + elemId).dialog({
        modal: false,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });
}