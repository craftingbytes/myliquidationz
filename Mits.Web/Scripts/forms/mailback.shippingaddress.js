﻿function ValidateInfo() {
    var isValid = ValidateForm(); //true;
    
    if (isValid) {
        var rootPath = window.location.protocol + "//" + window.location.host;
        document.forms[0].action = rootPath + '/Mailback/ShippingAddress';
        document.forms[0].submit();

//        $('#Form1').attr("action", rootPath + '/Mailback/ShippingAddress');
//        $('#Form1').submit();

    }
}
function showHelp(elemId) {
    $("#" + elemId).dialog({
        modal: false,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });
}
function showHelpPrivacy(elemId) {
    $("#" + elemId).dialog({
        modal: false,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });
}
$(function () {
    $("#hlPrevious").button();
})
