﻿$(document).ready(function () {


    var rootPath = window.location.protocol + "//" + window.location.host;

    //'Clear button onclick function' for clearing all necessary fields

    $('input[id$=btnClear]').click(function () {
        $(this.form).find(':input').each(function () {
            switch (this.type) {
                case 'select-multiple':
                case 'select-one':
                    {
                        $(this).css("background-color", "white");
                        $(this).val(0);
                        break;
                    }
                case 'password':
                case 'text':
                case 'textarea':
                    {
                        $(this).val('');
                        $(this).css("background-color", "white");
                        break;
                    }
                case 'checkbox':
                    if (this.name != 'Active')
                        this.checked = false;
                    break;
            }
        });
        $("#validationSummary").html("");

    });

    $('input[id$=btnSubmit]').click(function () {
        var check = "true";
        var dynamicHTML = "";
        dynamicHTML += "<ul class=ValidationSummary>";

        if ($('input[id$=txtOldPassword]').val() == "") {
            dynamicHTML += "<li>" + "Old passord is required" + "</li>"
            $('input[id$=txtOldPassword]').css("background-color", "#FFEEEE");
            $("#validationSummary").html(dynamicHTML);
            check = "false";

        }
        if ($('input[id$=txtNewPassword]').val() == "") {
            dynamicHTML += "<li>" + "New passord is required" + "</li>"
            $('input[id$=txtNewPassword]').css("background-color", "#FFEEEE");
            $("#validationSummary").html(dynamicHTML);
            check = "false";
        }
        if ($('input[id$=txtConfirmPassword]').val() == "") {
            dynamicHTML += "<li>" + "Confirm passord is required" + "</li>"
            $('input[id$=txtConfirmPassword]').css("background-color", "#FFEEEE");
            $("#validationSummary").html(dynamicHTML);
            check = "false";
        }
        if ($("select#ddlQuestion").val() == "0") {
            dynamicHTML += "<li>" + "Security Question is required" + "</li>"
            $("select#ddlQuestion").css("background-color", "#FFEEEE");
            $("#validationSummary").html(dynamicHTML);
            check = "false";
        }
        if ($('input[id$=txtAnswer]').val() == "") {
            dynamicHTML += "<li>" + "Answer is required" + "</li>"
            $('input[id$=txtAnswer]').css("background-color", "#FFEEEE");
            $("#validationSummary").html(dynamicHTML);
            check = "false";
        }

        if (check == "false")
            return false;

        oldPassword = $('input[id$=txtOldPassword]').val();
        newPassword = $('input[id$=txtNewPassword]').val();
        confirmPassword = $('input[id$=txtConfirmPassword]').val();
        answer = $('input[id$=txtAnswer]').val();
        questionId = $("select#ddlQuestion").val();

        if (newPassword != confirmPassword) {

            dynamicHTML += "<li>" + "New and Confirm Password must be same" + "</li>"
            $('input[id$=txtNewPassword]').css("background-color", "#FFEEEE");
            $('input[id$=txtConfirmPassword]').css("background-color", "#FFEEEE");
            $("#validationSummary").html(dynamicHTML);
            return false;
        }
        else if (newPassword == oldPassword || confirmPassword == oldPassword) {
            dynamicHTML += "<li>" + "Old and New password are same" + "</li>"
            $('input[id$=txtNewPassword]').css("background-color", "#FFEEEE");
            $('input[id$=txtOldPassword]').css("background-color", "#FFEEEE");
            $("#validationSummary").html(dynamicHTML);
            return false;
        }
        else if (newPassword.length < 6 || confirmPassword.length < 6) {
            dynamicHTML += "<li>" + "Password must be at least 6 characters long" + "</li>"
            $("#validationSummary").html(dynamicHTML);
            return false;
        }

        $.ajax({
            url: rootPath + '/ChangePassword/SaveCriteria1',
            dataType: 'json',
            type: "POST",
            data: ({ oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword, questionId: questionId, answer: answer }),
            async: false,
            success: function (result) {
                dynamicHTML += "<li>" + result.message + "</li>";
                $('input[id$=txtOldPassword]').val("");
                $('input[id$=txtNewPassword]').val("");
                $('input[id$=txtConfirmPassword]').val("");

                $('input').each(function () {
                    $(this).css("background-color", "white");
                });

            }

        });
        dynamicHTML += "</ul>";
        $("#validationSummary").html(dynamicHTML);
    });

    //Text area max length 

    function imposeMaxLength(obj, maxLen) {
        return (obj.value.length <= maxLen);
    }
}
)
