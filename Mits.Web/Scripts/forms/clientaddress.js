﻿var lastSel;
var lastSelRow
var editState;
var editCity;

//********************************************************************************************************************************************* //
//*************************************************** Client Address - Start ****************************************************************** //
//********************************************************************************************************************************************* //
$(document).ready(function () {

    jQuery("#jqGrid").jqGrid({
        url: "/ClientAddress/GetClientAddresses",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['AffiliateId', 'Affiliate*', 'StateId', 'CityId', 'Company/Person Name*', 'Address*', 'State*', 'City*', 'Zip', 'Phone', 'Edit Pick Up', 'Actions'],
        colModel: [
        { name: 'AffiliateId', index: 'AffiliateId', editable: false, width: 80, sortable: false, hidden: true },
   		{ name: 'Affiliate', index: 'Affiliate', editable: false, width: 110, sortable: false, hidden: ($('#ViewAll').val() == "False"), editrules: { custom: true, custom_func: validate }, edittype: 'select' },
        { name: 'StateId', index: 'StateId', editable: false, width: 110, sortable: false, hidden: true },
        { name: 'CityId', index: 'CityId', editable: false, width: 110, sortable: false, hidden: true },
        { name: 'Company', index: 'Company', editable: true, width: 155, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 250} },
   		{ name: 'Address', index: 'Address', width: 110, editable: true, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 200} },
   		{ name: 'State', index: 'State', width: 110, editable: true, sortable: false,
   		    editrules: { custom: true, custom_func: validate },
   		    edittype: 'select',
   		    editoptions: { value: { 0: 'Select' }, dataInit: function (elem) { BeforeEditState(elem) } }
   		},
   		{ name: 'City', index: 'City', width: 110, editable: true, sortable: false,
   		    editrules: { custom: true, custom_func: validate },
   		    edittype: 'select',
   		    editoptions: { value: { 0: 'Select' }, dataInit: function (elem) { BeforeEditCity(elem) } }
   		},
   		{ name: 'Zip', index: 'Zip', width: 90, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 10} },
        { name: 'Phone', index: 'Phone', width: 90, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 20} },
        { name: 'PickUp', index: 'PickUp', sortable: false, width: 80, editable: false, formatter: pickUpFormatter, title: true, align: 'center' },
        { name: 'ActionMenu', index: 'ActionMenu', editable: false, sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 50 }
   	    ],
        rowNum: 50,
        rowList: [10, 25, 50, 100],
        autowidth: true,
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        //width: 850,
        height: 230,
        editurl: "/ClientAddress/EditClientAddress",
        onSelectRow: function (ids) { GetPickUpAddresses(ids); },
        gridComplete: function () { InitAffiliateCol(); },
        loadComplete: function () { ReloadPickUpAddresses(); }
    });

    $("#jqGrid").jqGrid('navGrid', '#pager', { edit: false, add: false, del: false, search: false, refresh: true });

    ////////////////////////////////////////jqGrid Pick Up//////////////////////////////////////////////
    jQuery("#jqGridPickUp").jqGrid({
        url: "/ClientAddress/GetPickUpAddresses",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['ClientId', 'StateId', 'CityId', 'Company/Person Name*', 'Address*', 'State*', 'City*', 'Zip*', 'Phone*', 'Fax', 'Email', 'Collection Method*', 'Actions'],
        colModel: [
        { name: 'ClientId', index: 'ClientId', editable: false, width: 110, sortable: false, hidden: true },
        { name: 'StateId', index: 'StateId', editable: false, width: 110, sortable: false, hidden: true },
        { name: 'CityId', index: 'CityId', editable: false, width: 110, sortable: false, hidden: true },
        { name: 'PickUpCompany', index: 'PickUpCompany', editable: true, width: 140, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 250} },
   		{ name: 'PickUpAddress', index: 'PickUpAddress', width: 80, editable: true, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 200} },
   		{ name: 'PickUpState', index: 'PickUpState', width: 70, editable: true, sortable: false,
   		    editrules: { custom: true, custom_func: validate },
   		    edittype: 'select',
   		    editoptions: { value: { 0: 'Select' }, dataInit: function (elem) { PickUpBeforeEditState(elem) } }
   		},
   		{ name: 'PickUpCity', index: 'City', width: 70, editable: true, sortable: false,
   		    editrules: { custom: true, custom_func: validate },
   		    edittype: 'select',
   		    editoptions: { value: { 0: 'Select' }, dataInit: function (elem) { PickUpBeforeEditCity(elem) } }
   		},
   		{ name: 'PickUpZip', index: 'PickUpZip', width: 60, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 10} },
        { name: 'PickUpPhone', index: 'PickUpPhone', width: 60, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 20} },
        { name: 'Fax', index: 'Fax', width: 60, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 20} },
        { name: 'Email', index: 'Email', width: 70, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 100} },
        { name: 'Collection', index: 'Collection', width: 120, editable: true, sortable: false,
            editrules: { custom: true, custom_func: validate },
            edittype: 'select',
            editoptions: { value: { 0: 'Select'} }
        },
        { name: 'PickUpActionMenu', index: 'PickUpActionMenu', editable: false, sortable: false, formatter: PickUpActionMenuFormatter, title: true, align: 'center', width: 50 }
   	    ],
        rowNum: 50,
        rowList: [10, 25, 50, 100],
        //autowidth: true,
        pager: "#pagerPickUp",
        viewrecords: true,
        rownumbers: true,
        width: 950,
        height: 280,
        editurl: "/ClientAddress/EditPickUpAddress",
        gridComplete: function () { InitMethodCol(); }
    });

    $("#jqGridPickUp").jqGrid('navGrid', '#pagerPickUp', { edit: false, add: false, del: false, search: false, refresh: true });

    $("#btnSearch").click(function () {
        var affiliateId = $("#ddlProcessor").val();
        jQuery("#jqGrid").setGridParam({ postData: { affiliateId: affiliateId }, page: 1 }).trigger("reloadGrid");
    });

    $("#btnAdd").click(function () {
        CancelRow(lastSel);
        lastSel = 0; // new row
        $('#jqGrid').setColProp('Affiliate', { editable: true });
        $("#jqGrid").jqGrid('editGridRow', "new", { height: 240, width: 360, closeAfterAdd: true, beforeShowForm: BeforeShowFrom, beforeSubmit: BeforeSubmitFrom, afterSubmit: AfterSaveRow });
    });

    $("#btnAddPickUp").click(function () {
        var selrow = $("#jqGrid").jqGrid('getGridParam', 'selrow');
        if (!selrow) {
            alert('Please select a Client to add a Pick Up/Ship From.');
            return;
        }
        var url = '/ClientAddress/EditPickUpAddress?clientId=' + selrow;
        CancelRow2(lastSel2);
        lastSel2 = 0; // new row
        $("#jqGridPickUp").jqGrid('editGridRow', "new", { height: 330, width: 360, url: url, closeAfterAdd: true, beforeShowForm: BeforeShowFrom2, beforeSubmit: BeforeSubmitFrom2, afterSubmit: AfterSaveRow2 });
    });

    $("#dlgPickUp").dialog({
        modal: false,
        width: 975,
        height: 400,
        zIndex: 500,
        resizable: false,
        position: 'center',
        autoOpen: false,
        show: 'fold',
        hide: 'fold',
        Ok: function () {
            $(this).dialog('close');
        }
    });
});

function InitAffiliateCol() {
    $.ajax({
        url: '/ClientAddress/GetAffiliates',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('Affiliate', { editoptions: { value: result.data, dataEvents: [
                      { type: 'change',
                          fn: function (formid) {
                              OnAffiliateChange();
                          }
                      }]
            }
            });
        }
    });
}

function BeforeShowFrom(formid) {
    $(formid).find('select').css('width', '176px');
    $(formid).find('input').css('width', '170px');

    editState = $(formid).find('#State');
    editCity = $(formid).find('#City');

    var clear = { data: [{ Value: 0, Text: 'Select'}] };
    editCity.fillSelect(clear.data);
    if ($('#ViewAll').val() == "False") {
        var affiliateId = $('#AffiliateId').val();
        FillSupportedStates(affiliateId, editState);
    } else {
        editState.fillSelect(clear.data);
    }

    editState.unbind('change');
    editState.bind('change', function () {
        FillCities(editState, editCity);
    });
}

function BeforeSubmitFrom(param1, form) {
    return [true, ""];
}

function AfterSaveRow(result) {
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    var msg = result.responseText.split(':')[1];
    if (msg == "AccessDenied") {
        var url = '/Home/AccessDenied';
        window.location.href = url;
    } else {
        alert(msg);
    }
    $('#eData').click();

    return true;
}

function BeforeEditState(elem) {
    if (lastSel == 0) {
        return;
    }
    editState = $(elem);
    
    if (lastSel > 0) {
        FillSupportedStates(lastSelRow.AffiliateId, editState, lastSelRow.StateId);
    }
    
    editState.change(function () {
        FillCities(editState, editCity);
    });
}

function BeforeEditCity(elem) {
    if (lastSel == 0) {
        return;
    }
    editCity = $(elem);
    if (lastSel > 0) {
        FillCities(editState, editCity, lastSelRow.CityId);
    }
}

function pickUpFormatter(cellvalue, options, rowObject) {
    var rowId = options.rowId;
    var docHtml = "<div>";
    docHtml += "<img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='View Pick Up' title='View Pick Up' onclick='viewPickUps();' />";
    docHtml += "</div>";
    return docHtml;
}

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' class='edit' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")'/>&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete'  title='Delete' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function viewPickUps() {
    $("#dlgPickUp").dialog('open');
    $("#gbox_jqGridPickUp").removeClass("ui-widget");
}

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#jqGrid').setColProp('Affiliate', { editable: false });
    lastSelRow = $("#jqGrid").getRowData(rowId);
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { afterSubmit: AfterSaveRow });
}

function OnAffiliateChange() {
    var affiliateId = 0;
    if (lastSel > 0) {
        var newData = $("#jqGrid").getRowData(lastSel);
        affiliateId = $(newData["Affiliate"]).val();
    } else if (lastSel == 0) {
        affiliateId = $('#Affiliate').val();
    }
    var clear = { data: [{ Value: 0, Text: 'Select'}] };
    editState.fillSelect(clear.data);
    editCity.fillSelect(clear.data);
    if (affiliateId != 0) {
        FillSupportedStates(affiliateId, editState);
    }
}

function FillSupportedStates(affiliateId, state, selected) {

    $.ajax({
        url: '/ClientAddress/GetSupportedStates',
        dataType: 'json',
        type: "POST",
        data: ({ affiliateId: affiliateId }),
        async: false,
        success: function (result) {
            if (result.data) {
                state.fillSelect(result.data);
                if (selected) {
                    state.val(selected);
                }
            }
        }
    });
}

function FillCities(state, city, selected) {
    var stateId = state.val();
    $.ajax({
        url: '/ClientAddress/GetCities',
        dataType: 'json',
        type: "POST",
        data: ({ stateId: stateId }),
        async: false,
        success: function (result) {
            if (result.data) {
                city.fillSelect(result.data);
                if (selected) {
                    city.val(selected);
                }
            }
        }
    });
}

function GetPickUpAddresses(ids) {
    if (ids != null) {
        jQuery("#jqGridPickUp").jqGrid('setGridParam', { url: "/ClientAddress/GetPickUpAddresses?clientId=" + ids, page: 1 });
        $('#jqGridPickUp').trigger("reloadGrid");
    }
}

/////////////////////////////jqGrid Pick Up//////////////////////////////////////

var lastSel2;
var lastSelRow2
var editState2;
var editCity2;

function InitMethodCol() {
    $.ajax({
        url: '/ClientAddress/GetCollectionMethods',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGridPickUp').setColProp('Collection', { editoptions: { value: result.data} });
        }
    });
}

function BeforeShowFrom2(formid) {
    $(formid).find('select').css('width', '176px');
    $(formid).find('input').css('width', '170px');

    editState2 = $(formid).find('#PickUpState');
    editCity2 = $(formid).find('#PickUpCity');

    var selrow = $("#jqGrid").jqGrid('getGridParam', 'selrow');
    var affiliateId = $("#jqGrid").getRowData(selrow).AffiliateId;

    FillSupportedStates(affiliateId, editState2);
    var clear = { data: [{ Value: 0, Text: 'Select'}] };
    editCity2.fillSelect(clear.data);

    editState2.unbind('change');
    editState2.bind('change', function () {
        FillCities(editState2, editCity2);
    });
}

function BeforeSubmitFrom2(param1, form) {
    return [true, ""];
}

function AfterSaveRow2(result) {
    $('#ED_ActionMenu2_' + lastSel2).css({ display: "block" });
    $('#SC_ActionMenu2_' + lastSel2).css({ display: "none" });

    var msg = result.responseText.split(':')[1];
    if (msg == "AccessDenied") {
        var url = '/Home/AccessDenied';
        window.location.href = url;
    } else {
        alert(msg);
    }

    $('#eData').click();
    return true;
}

function PickUpBeforeEditState(elem) {
    if (lastSel2 == 0) {
        return;
    }
    editState2 = $(elem);

    if (lastSel2 > 0) {
        var selrow = $("#jqGrid").jqGrid('getGridParam', 'selrow');
        if (selrow) {
            var affiliateId = $("#jqGrid").getRowData(selrow).AffiliateId;
            FillSupportedStates(affiliateId, editState2, lastSelRow2.StateId);
        }
    }

    editState2.change(function () {
        FillCities(editState2, editCity2);
    });
}

function PickUpBeforeEditCity(elem) {
    if (lastSel2 == 0) {
        return;
    }
    editCity2 = $(elem);
    if (lastSel2 > 0) {
        FillCities(editState2, editCity2, lastSelRow2.CityId);
    }
}

function PickUpActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu2_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' class='edit' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow2(\"" + options.rowId + "\")'/>&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete'  title='Delete' onclick='javascript:DeleteRow2(\"" + options.rowId + "\")'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu2_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow2(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow2(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function EditRow2(rowId) {
    if (rowId && rowId !== lastSel2) {
        CancelRow2(lastSel2);
        lastSel2 = rowId;
    }
    lastSelRow2 = $("#jqGridPickUp").getRowData(rowId);
    $('#ED_ActionMenu2_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu2_' + rowId).css({ display: "block" });
    $('#jqGridPickUp').editRow(rowId);
}

function CancelRow2(rowId) {
    $('#ED_ActionMenu2_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu2_' + rowId).css({ display: "none" });
    $('#jqGridPickUp').restoreRow(rowId);
}

function SaveRow2(rowId) {
    var newData = $("#jqGridPickUp").getRowData(rowId);
    $('#jqGridPickUp').saveRow(rowId, AfterSaveRow2);
}

function DeleteRow2(rowId) {
    $('#ED_ActionMenu2_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu2_' + rowId).css({ display: "none" });
    $('#jqGridPickUp').delGridRow(rowId, { afterSubmit: AfterSaveRow2, reloadAfterSubmit:false });
}

function ReloadPickUpAddresses() {
    var selrow = $("#jqGrid").jqGrid('getGridParam', 'selrow');
    if (selrow != null) {
        jQuery("#jqGridPickUp").jqGrid('setGridParam', { url: "/ClientAddress/GetPickUpAddresses?clientId=" + selrow, page: 1 });
        $('#jqGridPickUp').trigger("reloadGrid");
    } else {
        jQuery("#jqGridPickUp").jqGrid('setGridParam', { url: "/ClientAddress/GetPickUpAddresses", page: 1 });
        $('#jqGridPickUp').trigger("reloadGrid");
    }
}

function validate(value, colName) {
    if (colName == "Affiliate*") {
        if ($('#ViewAll').val() == "True") {
            if (value == null || value == 0) {
                return [false, "Affiliate*: Field is required"];
            }
        }
    }
    if (colName == "State*") {
        if (value == null || value == 0) {
            return [false, "State*: Field is required"];
        }
    }
    if (colName == "City*") {
        if (value == null || value == 0) {
            return [false, "City*: Field is required"];
        }
    }
    if (colName == "Zip*") {
        if (value == null || value == "") {
            return [false, "Zip*: Field is required"];
        }
        var zipReg = /^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$/;
        if (!zipReg.test(value)) {
            return [false, "Zip*: Invalid Zip format, Please try xxxxx."];
        }
    }
    if (colName == "Zip") {
        if (value != null && value != "") {
            var zipReg = /^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$/;
            if (!zipReg.test(value)) {
                return [false, "Zip: Invalid Zip format, Please try xxxxx."];
            }
        }
    }
    if (colName == "Phone*") {
        if (value == null || value == "") {
            return [false, "Phone*: Field is required"];
        }
        var phoneReg = /^\d{3}-\d{7}$/;
        if (!phoneReg.test(value)) {
            return [false, "Phone*: Invalid Phone format, Please try xxx-xxxyyyy."];
        }
    }
    if (colName == "Phone") {
        if (value != null && value != "") {
            var phoneReg = /^\d{3}-\d{7}$/;
            if (!phoneReg.test(value)) {
                return [false, "Phone: Invalid Phone format, Please try xxx-xxxyyyy."];
            }
        }
    }

    if (colName == "Email") {
        if (value != null && value != "") {
            var mailReg = /^(.+)@(.+)$/;
            if (!mailReg.test(value)) {
                return [false, "Email: is not a valid e-mail"];
            }
        }
    }
    if (colName == "Collection Method*") {
        if (value == null || value == 0) {
            return [false, "Collection Method*: Field is required"];
        }
    }
    return [true, ""];
}