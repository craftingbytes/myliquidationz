﻿
//********************************************************************************************************************************************* //
//*************************************************** Collection Event - Start ****************************************************************** //
//********************************************************************************************************************************************* //
$(document).ready(function () {

    
    jQuery("#jqGrid").jqGrid({
        url: "/CollectionEvent/GetCollectionEvents",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['State*', 'CityId', 'City*', 'Address*', 'Location Name', 'Zip Code*', 'Date and Hours*', 'Note', 'Actions'],
        colModel: [
   		    { name: 'State', index: 'State', width: 40, editable: true, sortable: false,
   		        editrules: { custom: true, custom_func: validate },
   		        edittype: 'select',
   		        editoptions: { value: { 0: 'Select' }, dataInit: function (elem) { BeforeEditState(elem) } }
   		    },
            { name: 'CityId', index: 'CityId', editable: false, width: 10, sortable: false, hidden: true },
   		    { name: 'City', index: 'City', width: 40, editable: true, sortable: false,
   		        editrules: { custom: true, custom_func: validate },
   		        edittype: 'select',
   		        editoptions: { value: { 0: 'Select' }, dataInit: function (elem) { BeforeEditCity(elem) } }
   		    },
            { name: 'Address', index: 'Address', width: 60, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 200} },
            { name: 'Location', index: 'Location', width: 50, editable: true, sortable: false, editoptions: { size: 30, maxlength: 200} },
   		    { name: 'Zip', index: 'Zip', width: 30, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 10} },
            { name: 'Hours', index: 'Hours', width: 70, editable: true, sortable: false, editrules: { custom: true, custom_func: validate }, editoptions: { size: 30, maxlength: 200} },
            { name: 'Note', index: 'Note', width: 50, editable: true, sortable: false, editoptions: { size: 30, maxlength: 200} },
            { name: 'ActionMenu', index: 'ActionMenu', editable: false, sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 30, hidden: ($('#canDelete').val() == 'disabled' && $('#canUpdate').val() == 'disabled') }
   	    ],
        rowNum: 50,
        rowList: [10, 20, 50, 100],
        autowidth: true,
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        //width: 850,
        height: 230,
        editurl: "/CollectionEvent/EditCollectionEvent",
        gridComplete: function () { InitStateCol(); }
    });

    $("#jqGrid").jqGrid('navGrid', '#pager', { edit: false, add: false, del: false, search: false, refresh: true });
});

function InitStateCol() {
    $.ajax({
        url: '/CollectionEvent/GetStates',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('State', {
                editoptions: { value: result.data,
                    dataInit: function (elem) {
                        BeforeEditState(elem);
                    }
                }
            });
        }
    });
}

var lastSel;
var lastSelRow
var editState;
var editCity;

$("#btnSearch").click(function () {
    var stateId = $("#ddlState").val();
    jQuery("#jqGrid").setGridParam({ postData: { stateId: stateId }, page: 1 }).trigger("reloadGrid");
});

$("#btnAdd").click(function () {
    CancelRow(lastSel);
    lastSel = 0; // new row
    $("#jqGrid").jqGrid('editGridRow', "new", { height: 250, width: 360, closeAfterAdd: true, beforeShowForm: BeforeShowFrom, beforeSubmit: BeforeSubmitFrom, afterSubmit: AfterSaveRow });
});

function BeforeShowFrom(formid) {
    $(formid).find('select').css('width', '176px');
    $(formid).find('input').css('width', '170px');

    editState = $(formid).find('#State');
    editCity = $(formid).find('#City');

    var selrow = $("#jqGrid").jqGrid('getGridParam', 'selrow');
   
    var clear = { data: [{ Value: 0, Text: 'Select'}] };
    editCity.fillSelect(clear.data);

    editState.unbind('change');
    editState.bind('change', function () {
        FillCities(editState, editCity);
    });
}

function BeforeSubmitFrom(param1, form) {
    return [true, ""];
}

function AfterSaveRow(result) {
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });

    var msg = result.responseText.split(':')[1];
    if (msg == "AccessDenied") {
        var url = '/Home/AccessDenied';
        window.location.href = url;
    } else {
        alert(msg);
    }

    $('#eData').click();
    return true;
}

function BeforeEditState(elem) {
    if (lastSel == 0) {
        return;
    }
    editState = $(elem);

    if (lastSel > 0) {
        var selrow = $("#jqGrid").jqGrid('getGridParam', 'selrow');
    }

    editState.change(function () {
        FillCities(editState, editCity);
    });
}

function BeforeEditCity(elem) {
    if (lastSel == 0) {
        return;
    }
    editCity = $(elem);
    if (lastSel > 0) {
        FillCities(editState, editCity, lastSelRow.CityId);
    }
}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'>";
    if ($('#canUpdate').val() == '') {
        actionMenuHtml += "<img src='../../Content/images/icon/edit.png' class='edit' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")'/>&nbsp;&nbsp;";
    }
    if ($('#canDelete').val() == '') {
        actionMenuHtml += "<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete'  title='Delete' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/>";
    }
    actionMenuHtml += "</div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    lastSelRow = $("#jqGrid").getRowData(rowId);
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function SaveRow(rowId) {
    var newData = $("#jqGrid").getRowData(rowId);
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { afterSubmit: AfterSaveRow, reloadAfterSubmit:false });
}

function validate(value, colName) {
    if (colName == "State*") {
        if (value == null || value == 0) {
            return [false, "State*: Field is required"];
        }
    }
    if (colName == "City*") {
        if (value == null || value == 0) {
            return [false, "City*: Field is required"];
        }
    }
    if (colName == "Address*") {
        if (value == null || value == "") {
            return [false, "Address*: Field is required"];
        }
    }
    if (colName == "Zip Code*") {
        if (value == null || value == "") {
            return [false, "Zip Code*: Field is required"];
        }
        var zipReg = /^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$/;
        if (!zipReg.test(value)) {
            return [false, "Zip Code*: Invalid Zip format, Please try xxxxx."];
        }
    }
    if (colName == "Hour*") {
        if (value == null || value == "") {
            return [false, "Hour*: Field is required"];
        }
    }
    return [true, ""];
}

function FillCities(state, city, selected) {
    var stateId = state.val();
    $.ajax({
        url: '/ClientAddress/GetCities',
        dataType: 'json',
        type: "POST",
        data: ({ stateId: stateId }),
        async: false,
        success: function (result) {
            if (result.data) {
                city.fillSelect(result.data);
                if (selected) {
                    city.val(selected);
                }
            }
        }
    });
}