﻿$(document).ready(function () {

    //'State dropdown onchange function' for getting guideline of state
    $("#ddlState").change(function (elem) {
        loadStateGuideline();
        loadGrids();
    });

    //'Clear button onclick function' for clearing all necessary fields
    $("#btnClear").click(function () {
        $("#txtGoverningBody").val("");
        $("#txtWebsite").val("");
        $("#txtCoveredDevices").val("");
        $("#txtOEMObligation").val("");
        $("#txtUpdates").val("");
        $("#txtRegulationType").val("");
        $("#txtRegulationChange").val("");
        $("#ddlLawState").val("");
        $("#message").css("visibility", "hidden");
        $("#lblMessage").text("");
    });


    //'Submit button onclick function' for submitting necessary data to the controller action

    $("#btnSubmit").click(function () {
        $("#lblMessage").text("");
        $("#message").css("visibility", "hidden");

        state = $("#ddlState").val();
        governingBody = $("#txtGoverningBody").val();
        website = $("#txtWebsite").val();
        coveredDevices = $("#txtCoveredDevices").val();
        oemObligation = $("#txtOEMObligation").val();
        updates = $("#txtUpdates").val();
        regulationType = $("#txtRegulationType").val();
        regulationChange = $("#txtRegulationChange").val();
        lawState = $("#ddlLawState").val();
        $.ajax({
            url: 'SaveStateGuideline',
            dataType: 'json',
            type: "POST",
            data: ({ stateId: state, governingBody: governingBody, website: website, coveredDevices: coveredDevices, oemObligation: oemObligation, updates: updates, lawState: lawState, regulationType: regulationType, regulationChange: regulationChange }),
            async: false,
            success: function (result) {
                if (result.AccessDenied == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else {
                    $("#lblMessage").text(result.message);
                    $("#message").css("visibility", "visible");
                }
                window.scrollTo(0);
            }
        });

    });

    $("#btnAddContact").click(function () {
        if (contactEditMode == false) {
            contactEditMode = true;
            var emptyData = [
		            { Name: '', Phone: "", Email: "" }
		        ];
            contactLastSel = "0";  //rowid = 0 indicate the row is new inserted
            $("#contactGrid").addRowData("0", emptyData[0]);
            editContactRow(contactLastSel);
        } else {
            alert('There is already an item opened in edit mode. Please save or cancel it first.');
        }
    });

    state = $("select#ddlState").val();

    $("#contactGrid").jqGrid({
        url: 'GetContacts?state=' + state,
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Name*', 'Phone', 'EMail', 'Actions'],
        colModel: [
   		            { name: 'Name', index: 'Name', width: 300, editable: true, sortable: false, editoptions: { maxlength: 50 }, editrules: { custom: true, custom_func: contactCheck} },
   		            { name: 'Phone', index: 'Phone', width: 300, editable: true, sortable: false, editoptions: { maxlength: 50} },
   		            { name: 'EMail', index: 'Email', width: 300, editable: true, sortable: false, editoptions: { maxlength: 200 }, editrules: { custom: true, custom_func: contactCheck} },
                    { name: 'Actions', index: 'Actions', width: 50, editable: false, sortable: false, align: 'center', formatter: contactActionsFormatter }
   	            ],
        rowNum: -1,
        width: 930,
        height: 100
    });

    $("#btnAddDeadlineDate").click(function () {
        if (deadlineDateEditMode == false) {
            deadlineDateEditMode = true;
            var emptyData = [
		            { Name: '', Date: "" }
		        ];
            deadlineDateLastSel = "0";  //rowid = 0 indicate the row is new inserted
            $("#deadlineDateGrid").addRowData("0", emptyData[0]);
            editDeadlineDateRow(deadlineDateLastSel);
        } else {
            alert('There is already an item opened in edit mode. Please save or cancel it first.');
        }
    });

    $("#deadlineDateGrid").jqGrid({
        url: 'GetDeadlineDates?state=' + state,
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Name*', 'Date', 'Actions'],
        colModel: [
   		            { name: 'Name', index: 'Name', width: 200, editable: true, sortable: false, editoptions: { maxlength: 200 }, editrules: { custom: true, custom_func: deadlineDateCheck} },
   		            { name: 'Date', index: 'Date', width: 200, editable: true, sortable: false, editoptions: { maxlength: 200} },
                    { name: 'Actions', index: 'Actions', width: 50, editable: false, sortable: false, align: 'center', formatter: deadlineDateActionsFormatter }
   	            ],
        rowNum: -1,
        width: 445,
        height: 100
    });

    $("#btnAddLink").click(function () {
        if (linkEditMode == false) {
            linkEditMode = true;
            var emptyData = [
		            { Name: '', Link: "" }
		        ];
            linkLastSel = "0";  //rowid = 0 indicate the row is new inserted
            $("#linkGrid").addRowData("0", emptyData[0]);
            editLinkRow(linkLastSel);
        } else {
            alert('There is already an item opened in edit mode. Please save or cancel it first.');
        }
    });

    jQuery("#linkGrid").jqGrid({
        url: 'GetLinks?state=' + state,
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Name*', 'Link*', 'Actions'],
        colModel: [
   		            { name: 'Name', index: 'Name', width: 200, editable: true, sortable: false, editoptions: { maxlength: 200 }, editrules: { custom: true, custom_func: linkCheck} },
   		            { name: 'Link', index: 'Link', width: 200, editable: true, sortable: false, editoptions: { maxlength: 500 }, editrules: { custom: true, custom_func: linkCheck} },
                    { name: 'Actions', index: 'Actions', width: 50, editable: false, sortable: false, align: 'center', formatter: linkActionsFormatter }
   	            ],
        rowNum: -1,
        width: 445,
        height: 100
    });

    loadStateGuideline();
});

function loadStateGuideline() {
    state = $("select#ddlState").val();
    $("#btnClear").click();
    $("#state").val(state);

    $.ajax({
        url: 'GetStateGuideline',
        dataType: 'json',
        type: "POST",
        data: ({ state: state }),
        async: false,
        success: function (result) {
            $("#txtGoverningBody").val(result.GoverningBody);
            $("#txtWebsite").val(result.Website);
            $("#txtCoveredDevices").val(result.CoveredDevices);
            $("#txtOEMObligation").val(result.OEMObligation);
            $("#txtUpdates").val(result.Updates);
            $("#ddlLawState").val(result.LawState);
            $("#txtRegulationType").val(result.RegulationType);
            $("#txtRegulationChange").val(result.RegulationChange);
        }
    });
}

// Setting grids start
var contactLastSel;
var contactEditMode = false;
var contactChecked = true;
var deadlineDateLastSel;
var deadlineDateEditMode = false;
var deadlineDateChecked = true;
var linkLastSel;
var linkEditMode = false;
var linkChecked = true;

function loadGrids() {
    contactLastSel = null;
    contactEditMode = false;
    deadlineDateLastSel = null;
    deadlineDateEditMode = false;
    linkLastSel = null;
    linkEditMode = false;
    state = $("select#ddlState").val();
    $("#contactGrid").setGridParam({ url: 'GetContacts', postData: { state: state }, page: 1 }).trigger("reloadGrid");
    $("#deadlineDateGrid").setGridParam({ url: 'GetDeadlineDates', postData: { state: state }, page: 1 }).trigger("reloadGrid");
    $("#linkGrid").setGridParam({ url: 'GetLinks', postData: { state: state }, page: 1 }).trigger("reloadGrid");
}

// Setting contact grid start
function contactActionsFormatter(cellvalue, options, rowdata) {
    var rowId = options.rowId;
    var actionsHtml = "<div id='ED_ContactActions_" + rowId + "'>";
    actionsHtml += "<img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:editContactRow(\"" + rowId + "\")' style='border:none;' class='edit'/>";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete' title='Delete' onclick='javascript:deleteContactRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    actionsHtml += "<div id='SC_ContactActions_" + rowId + "' style='display:none;'>";
    actionsHtml += "<img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save' title='Save' onclick='javascript:saveContactRow(\"" + rowId + "\")' style='border:none;' />";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/cancel.png' style='cursor:hand;' alt='Cancel' title='Cancel' onclick='javascript:cancelContactRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    return actionsHtml;  
}

function editContactRow(rowId) {
    if (rowId && rowId !== contactLastSel) {
        cancelContactRow(contactLastSel);
        contactLastSel = rowId;
    }
    $('#ED_ContactActions_' + rowId).css({ display: "none" });
    $('#SC_ContactActions_' + rowId).css({ display: "block" });
    $('#contactGrid').editRow(rowId);
    contactEditMode = true;
}

function cancelContactRow(rowId) {
    $('#ED_ContactActions_' + rowId).css({ display: "block" });
    $('#SC_ContactActions_' + rowId).css({ display: "none" });

    $.fn.fmatter.rowactions(rowId, 'contactGrid', 'cancel', false);
    if (rowId == "0") {
        $("#contactGrid").delRowData(rowId);
    }
    contactEditMode = false;
}

function saveContactRow(rowId) {
    state = $("#ddlState").val();
    $("#contactGrid").saveRow(rowId, null, 'SaveContact', { state: state }, saveContactCallback);
    if (!contactChecked) {
        return;
    }
    contactEditMode = false;
    $('#ED_ContactActions_' + rowId).css({ display: "block" });
    $('#SC_ContactActions_' + rowId).css({ display: "none" });
}

function saveContactCallback(rowId, result) {
    if (result.responseText != "failture" && parseInt(result.responseText).toString() != 'NaN') {
        if (rowId == "0") {
            var rowData = $("#contactGrid").getRowData(rowId);
            // replace local new rowId with server rowId
            $("#contactGrid").delRowData(rowId);
            $("#contactGrid").addRowData(result.responseText, rowData);
        }
    }
}

function deleteContactRow(rowId) {
    $("#contactGrid").delGridRow(rowId,{reloadAfterSubmit:false,  url:'DeleteContact'}); 
}

function contactCheck(value, colName) {
    contactChecked = true;
    if (colName == "Name*") {
        if (value == null || value == "") {
            contactChecked = false;
            return [false, "Name*: Field is required"];
        }
    }
    if (colName == "EMail") {
        if (value != null && value != "") {
            var mailReg = /^(.+)@(.+)$/;
            if (!mailReg.test(value)) {
                contactChecked = false;
                return [false, "EMail: is not a valid e-mail"];
            }
        }
    }
    return [true, ""];
}
// Setting contact grid end
// Setting deadlineDate grid start
function deadlineDateActionsFormatter(cellvalue, options, rowdata) {
    var rowId = options.rowId;
    var actionsHtml = "<div id='ED_DeadlineDateActions_" + rowId + "'>";
    actionsHtml += "<img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:editDeadlineDateRow(\"" + rowId + "\")' style='border:none;' class='edit'/>";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete' title='Delete' onclick='javascript:deleteDeadlineDateRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    actionsHtml += "<div id='SC_DeadlineDateActions_" + rowId + "' style='display:none;'>";
    actionsHtml += "<img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save' title='Save' onclick='javascript:saveDeadlineDateRow(\"" + rowId + "\")' style='border:none;' />";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/cancel.png' style='cursor:hand;' alt='Cancel' title='Cancel' onclick='javascript:cancelDeadlineDateRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    return actionsHtml;
}

function editDeadlineDateRow(rowId) {
    if (rowId && rowId !== deadlineDateLastSel) {
        cancelDeadlineDateRow(deadlineDateLastSel);
        deadlineDateLastSel = rowId;
    }
    $('#ED_DeadlineDateActions_' + rowId).css({ display: "none" });
    $('#SC_DeadlineDateActions_' + rowId).css({ display: "block" });
    $('#deadlineDateGrid').editRow(rowId);
    deadlineDateEditMode = true;
}

function cancelDeadlineDateRow(rowId) {
    $('#ED_DeadlineDateActions_' + rowId).css({ display: "block" });
    $('#SC_DeadlineDateActions_' + rowId).css({ display: "none" });

    //$.fn.fmatter.rowactions(rowId, 'deadlineDateGrid', 'cancel', false);
    if (rowId == "0") {
        $("#deadlineDateGrid").delRowData(rowId);
    }
    deadlineDateEditMode = false;
}

function saveDeadlineDateRow(rowId) {
    state = $("#ddlState").val();
    $("#deadlineDateGrid").saveRow(rowId, null, 'SaveDeadlineDate', { state: state }, saveDeadlineDateCallback);
    if (!deadlineDateChecked) {
        return;
    }
    deadlineDateEditMode = false;
    $('#ED_DeadlineDateActions_' + rowId).css({ display: "block" });
    $('#SC_DeadlineDateActions_' + rowId).css({ display: "none" });
}

function saveDeadlineDateCallback(rowId, result) {
    if (result.responseText != "failture" && parseInt(result.responseText).toString() != 'NaN') {
        if (rowId == "0") {
            var rowData = $("#deadlineDateGrid").getRowData(rowId);
            // replace local new rowId with server rowId
            $("#deadlineDateGrid").delRowData(rowId);
            $("#deadlineDateGrid").addRowData(result.responseText, rowData);
        }
    }
}

function deleteDeadlineDateRow(rowId) {
    $("#deadlineDateGrid").delGridRow(rowId, { reloadAfterSubmit: false, url: 'DeleteDeadlineDate' });
}

function deadlineDateCheck(value, colName) {
    deadlineDateChecked = true;
    if (colName == "Name*") {
        if (value == null || value == "") {
            deadlineDateChecked = false;
            return [false, "Name*: Field is required"];
        }
    }
    return [true, ""];
}
// Setting deadlineDate grid end
// Setting link grid start
function linkActionsFormatter(cellvalue, options, rowdata) {
    var rowId = options.rowId;
    var actionsHtml = "<div id='ED_LinkActions_" + rowId + "'>";
    actionsHtml += "<img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:editLinkRow(\"" + rowId + "\")' style='border:none;' class='edit'/>";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete' title='Delete' onclick='javascript:deleteLinkRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    actionsHtml += "<div id='SC_LinkActions_" + rowId + "' style='display:none;'>";
    actionsHtml += "<img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save' title='Save' onclick='javascript:saveLinkRow(\"" + rowId + "\")' style='border:none;' />";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/cancel.png' style='cursor:hand;' alt='Cancel' title='Cancel' onclick='javascript:cancelLinkRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    return actionsHtml;
}

function editLinkRow(rowId) {
    if (rowId && rowId !== linkLastSel) {
        cancelLinkRow(linkLastSel);
        linkLastSel = rowId;
    }
    $('#ED_LinkActions_' + rowId).css({ display: "none" });
    $('#SC_LinkActions_' + rowId).css({ display: "block" });
    $('#linkGrid').editRow(rowId);
    linkEditMode = true;
}

function cancelLinkRow(rowId) {
    $('#ED_LinkActions_' + rowId).css({ display: "block" });
    $('#SC_LinkActions_' + rowId).css({ display: "none" });

    $.fn.fmatter.rowactions(rowId, 'linkGrid', 'cancel', false);
    if (rowId == "0") {
        $("#linkGrid").delRowData(rowId);
    }
    linkEditMode = false;
}

function saveLinkRow(rowId) {
    state = $("#ddlState").val();
    $("#linkGrid").saveRow(rowId, null, 'SaveLink', { state: state }, saveLinkCallback);
    if (!linkChecked) {
        return;
    }
    linkEditMode = false;
    $('#ED_LinkActions_' + rowId).css({ display: "block" });
    $('#SC_LinkActions_' + rowId).css({ display: "none" });
}

function saveLinkCallback(rowId, result) {
    if (result.responseText != "failture" && parseInt(result.responseText).toString() != 'NaN') {
        if (rowId == "0") {
            var rowData = $("#linkGrid").getRowData(rowId);
            // replace local new rowId with server rowId
            $("#linkGrid").delRowData(rowId);
            $("#linkGrid").addRowData(result.responseText, rowData);
        }
    }
}

function deleteLinkRow(rowId) {
    $("#linkGrid").delGridRow(rowId, { reloadAfterSubmit: false, url: 'DeleteLink' });
}

function linkCheck(value, colName) {
    linkChecked = true;
    if (colName == "Name*") {
        if (value == null || value == "") {
            linkChecked = false;
            return [false, "Name*: Field is required"];
        }
    }
    if (colName == "Link*") {
        if (value == null || value == "") {
            linkChecked = false;
            return [false, "Link*: Field is required"];
        }
    }
    return [true, ""];
}
// Setting link grid end
// Setting grid end

//Text area max length
function imposeMaxLength(obj, maxLen) {
    return (obj.value.length <= maxLen);
}