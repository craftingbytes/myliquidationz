﻿
var planyear = '';
var stateid = '';
var startdate = '';
var enddate = '';

function GenerateReport() {

    planyear = $("#ddlPlanYear").val();
    if (planyear == 'All')
        planyear = '';
    stateid = $("#StateId").val();
    if (stateid == '0')
        stateid = '';
    startdate = $("#txtBeginDate").val();
    enddate = $("#txtEngingDate").val();

    var src = '/Reports/DetailedProcessingReport.aspx?PlanYear=' + planyear + '&StateId=' + stateid + '&StartDate=' + startdate + '&enddate=' + enddate;
    return showReport(src, 'ProcessingReport');

}