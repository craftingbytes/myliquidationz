﻿

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var type;
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                type = oRequired.getAttribute('type');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || (oRequired.value == "0" && type != "text")) {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"
                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }

}

$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;

    $('input[id$=btnClear]').click(function () {
        $(this.form).find(':input').each(function () {
            var month = $('#month').val();
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                    $(this).val(0);
                    break;
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                    if (this.name != 'Active')
                        this.checked = false;
                    break;
            }
            $('#month').val(month);
        });

        $("#validationSummary").html("");
    });


    $('input[id$=btnSubmit]').click(function () {

        var isValid = ValidateForm(document.forms[0]);
        if (isValid) {
            document.forms[0].submit();
        }
        else {
            return false;
        }
    });

    if ($("#hidMess").text() == "true") {

        $("#saved").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "OK": function () {
                    var url = rootPath + '/RecyclerProductWeight/Index/';
                    window.location.href = url;
                }
            }
        });
        return false;
    }

});
