﻿
//********************************************************************************************************************************************* //
//*************************************************** Collection Event - Start ****************************************************************** //
//********************************************************************************************************************************************* //
$(document).ready(function () {

    jQuery("#jqGrid").jqGrid({
        url: "/StateProductType/GetStateProductTypes",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['State*', 'Product Type*', 'Actions'],
        colModel: [
   		    { name: 'State', index: 'State', width: 100, editable: true, sortable: false,
   		        editrules: { custom: true, custom_func: validate },
   		        edittype: 'select',
   		        editoptions: { value: { 0: 'Select'} }
   		    },
            { name: 'ProductType', index: 'ProductType', width: 100, editable: true, sortable: false,
                editrules: { custom: true, custom_func: validate },
                edittype: 'select',
                editoptions: { value: { 0: 'Select'} }
            },
            { name: 'ActionMenu', index: 'ActionMenu', editable: false, sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 40, hidden: ($('#canDelete').val() == 'disabled' && $('#canUpdate').val() == 'disabled') }
   	    ],
        rowNum: 50,
        rowList: [10, 20, 50, 100],
        autowidth: true,
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 650,
        height: 230,
        editurl: "/StateProductType/EditStateProductType",
        gridComplete: function () { InitStateCol(); InitProductTypeCol(); }
    });

    $("#jqGrid").jqGrid('navGrid', '#pager', { edit: false, add: false, del: false, search: false, refresh: true });

    $("#btnSearch").click(function () {
        var stateId = $("#ddlState").val();
        jQuery("#jqGrid").setGridParam({ postData: { stateId: stateId }, page: 1 }).trigger("reloadGrid");
    });

    $("#btnAdd").click(function () {
        CancelRow(lastSel);
        lastSel = 0; // new row
        $("#jqGrid").jqGrid('editGridRow', "new", { height: 200, width: 360, closeAfterAdd: true, beforeShowForm: BeforeShowFrom, beforeSubmit: BeforeSubmitFrom, afterSubmit: AfterSaveRow });
    });
});

function InitStateCol() {
    $.ajax({
        url: '/StateProductType/GetStates',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('State', {
                editoptions: { value: result.data }
            });
        }
    });
}

function InitProductTypeCol() {
    $.ajax({
        url: '/StateProductType/GetProductTypes',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('ProductType', {
                editoptions: { value: result.data }
            });
        }
    });
}

var lastSel;

function BeforeShowFrom(formid) {
    $(formid).find('select').css('width', '176px');
    $(formid).find('input').css('width', '170px');
}

function BeforeSubmitFrom(param1, form) {
    return [true, ""];
}

function AfterSaveRow(result) {
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });

    var msg = result.responseText.split(':')[1];
    if (msg == "AccessDenied") {
        var url = '/Home/AccessDenied';
        window.location.href = url;
    } else {
        alert(msg);
    }

    $('#eData').click();
    return true;
}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'>";
    if ($('#canUpdate').val() == '') {
        actionMenuHtml += "<img src='../../Content/images/icon/edit.png' class='edit' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")'/>&nbsp;&nbsp;";
    }
    if ($('#canDelete').val() == '') {
        actionMenuHtml += "<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete'  title='Delete' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/>";
    }
    actionMenuHtml += "</div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    lastSelRow = $("#jqGrid").getRowData(rowId);
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function SaveRow(rowId) {
    var newData = $("#jqGrid").getRowData(rowId);
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { afterSubmit: AfterSaveRow, reloadAfterSubmit:false });
}

function validate(value, colName) {
    if (colName == "State*") {
        if (value == null || value == 0) {
            return [false, "State*: Field is required"];
        }
    }
    if (colName == "Product Type*") {
        if (value == null || value == 0) {
            return [false, "Product Type*: Field is required"];
        }
    }
    return [true, ""];
}
