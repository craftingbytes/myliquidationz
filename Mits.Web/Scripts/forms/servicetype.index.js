﻿
$(document).ready(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;

   

    //'Service Type dropdown onchange function' for Getting Details name,description and associated groups for a particular Service Types

    $('select[id$=ddlServiceTypes]').change(function (elem) {

        serviceType = $("select#ddlServiceTypes").val();

        if (serviceType == "0") {
            $("input[name=selectedObjects]").attr('checked', false);
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");
            $("#txtName").val("");
            $("#txtDesc").val("");
            return false;
        }
        else {
            $("input[name=selectedObjects]").attr('checked', false);
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");
            $("#txtName").val("");
            $("#txtDesc").val("");
            $.ajax({
                url: rootPath + '/ServiceType/GetDetail',
                dataType: 'json',
                type: "POST",
                data: ({ servicetype: serviceType }),
                async: false,
                success: function (result) {

                    $("input#txtName").val(result.Name);
                    $("textarea#txtDesc").val(result.Desc);
                    if (result.newData) {
                        $.each(result.newData, function (index, item) {


                            $("input[id=" + item.Value + "]").attr('checked', true);


                        });
                    }
                }
            });
        }
    });


    //'Clear button onclick function' for clearing necessary fields

    $('input[id$=btnClear]').click(function () {
        $("#txtName").val("");
        $("#txtDesc").val("");
        $("select#ddlServiceTypes").val(0);
        $("input[name=selectedObjects]").attr('checked', false);
        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");
    });


    //'Submit button onclick function' for submitting data to the controller action

    $('input[id$=btnSubmit]').click(function () {


        serviceType = $("select#ddlServiceTypes").val();

        if ($("input:checkbox:checked").length == 0) {
            $("label#lblMessage").text("No Associated Group Selected").css("color", "Red");
            $("ul#message").css("visibility", "visible");
            return;
        }
        if ($("#txtName").val() == "") {
            $("label#lblMessage").text("Name is required field").css("color", "Red");
            $("ul#message").css("visibility", "visible");
            return;
        }


        var associatedGroups = "";
        $("input:checkbox:checked").each(function () {
            if (associatedGroups == "")
                associatedGroups = $(this).val();
            else
                associatedGroups = associatedGroups + ',' + $(this).val();


        });
        var name = $("input#txtName").val();
        var desc = $("textarea#txtDesc").val();
        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");
        $.ajax({
            url: rootPath + '/ServiceType/Save',
            dataType: 'json',
            type: "POST",
            data: ({ servicetype: serviceType, associatedGroups: associatedGroups, name: name, desc: desc }),
            async: false,
            success: function (result) {
                if (result.AccessDenied == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else {
                    $("label#lblMessage").text(result.message);
                    $("ul#message").css("visibility", "visible");
                    if (result.data) {
                        $("select#ddlServiceTypes").fillSelect(result.data);
                        $("select#ddlServiceTypes").val(result.selected);
                        $("input#selectedObjects").attr('checked', true);
                    }
                }
            }
        });


    });
});


//Text Area Max length 

function imposeMaxLength(Object, MaxLen) {
    return (Object.value.length <= MaxLen);
}
