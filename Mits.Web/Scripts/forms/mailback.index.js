﻿var $j = window.jQuery;
var editOnLoad = $("#editOnLoad").val(); // <%=ViewData["editOnLoad"].ToString().ToLower() %>;
var lastsel;
var prodModelsUrl = ''
var mfgList = '';
var deviceList = '';
var boxSize = '';
var isGridLoaded = false;
$j(function () {
    $j("input[id$=btnSave]").click(function () {

        var isValid = validateGridRows();
        var numberOfRecords = $("#devices").getGridParam("reccount");
        if (numberOfRecords == 0 || isValid == false) {

            $("#noData").dialog({
                modal: true,
                width: 400,
                resizable: false,
                position: 'center',
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        } else {
            window.location.href = "/Mailback/ShippingAddress?addressInfo=" + $("#hfAddressInfo").val();
        }
    }).button();

    $j("input[id$=btnCancel]").click(function () {
        $j.ajax({
            url: '../../MailBack/ClearDevices',
            dataType: 'text',
            type: "GET",
            async: false
        });

        window.location.href = "/DropOffLocations/Index";
    }).button();
    $j("input[id$=btnAdd]").click(function () {

        //                var zipRegex = new RegExp(/^\d{5}$/);
        //                var zipCode = $j("input[id$=txtZipCode]").val()

        //                if ((zipCode == '' || !zipRegex.test(zipCode))) {
        //                    InValidZipCode();
        //                    return;
        //                } else {
        //  $j("#devices").jqGrid('editGridRow', "new", { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 300, width: 400 });
        //}

        var isValid = ValidateForm();
        if (isValid) {
            $j("#validationSummary").html("");

            //                    GetDevices($("#city").val(), $("#state").val(), $("#zip").val());
            //                    initDeviceGrid("GetSelectedDevices");
            //                    $j("#devices").jqGrid('editGridRow', "new", { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 300, width: 400 });
            FetchStateByZip();
        }

    }).button();

    $j("input[id$=txtZipCode]").change(function () {

        $j("#devices").jqGrid("clearGridData", true);
        $j.ajax({
            url: '../../MailBack/ClearDevices',
            dataType: 'text',
            type: "GET",
            async: true
        });
        FetchStateByZip();
    });

    $j.ajax({
        url: 'GetManufacturerList',
        dataType: 'text',
        async: false,
        success: function (result) {
            mfgList = result;
        }
    });

    //Fetch the Devices for no state

    //GetDevices($("#city").val(), $("#state").val(), $("#zip").val());

    initDeviceGrid(); //dummy action

});

function GetDevices(city, state, zip) {
    $j.ajax({
        url: 'GetDevicesList',
        dataType: 'text',
        type: "POST",
        data: ({ city: city, state: state, zip: zip }),
        async: false,
        success: function (result) {
            deviceList = result;
        }
    });
}
function FetchStateByZip() {

    zip = $("input[id$=txtZipCode]").val();
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': ', ' + zip }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {

            var geoResults = results[0]
            city = getAddressPart(geoResults.address_components, 'city');
            state = getAddressPart(geoResults.address_components, 'state');
            zip = getAddressPart(geoResults.address_components, 'postalcode');

            $("#hfAddressInfo").val(city + "+" + state + "+" + zip);

            GetDevices(city, state, zip);


            $('#devices').setColProp('Type', { editoptions: { defaultValue: 'Please Select', value: deviceList} });

            $j("#devices").jqGrid('editGridRow', "new", { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 300, width: 400 });

        } else {
            InValidZipCode();
        }
    });
}
function InValidZipCode() {
    $("#noValidZip").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "OK": function () {
                $(this).dialog("close");
                $('input[id$=txtZipCode]').focus();
            }
        }
    });
}
function validateGridRows() {

    $j("#noData").html("Please enter atleast 1 Device to proceed.");

    var ids = $("#devices").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        //get values for edit mode
        var model = $("#" + i + "_Model").val();
        var size = $("#" + i + "_Size").val();
        var qty = $("#" + i + "_Quantity").val();


        //get values for normal mode.
        var model_n = $('#devices').getCell(i, 'Model')
        var size_n = $('#devices').getCell(i, 'Size')
        var qty_n = $('#devices').getCell(i, 'Quantity')


        model = ($.trim(model) == '' && model_n.indexOf("input") == -1) ? model_n : $.trim(model);
        size = ($.trim(size) == '' && size_n.indexOf("input") == -1) ? size_n : $.trim(size);
        qty = ($.trim(qty) == '' && qty_n.indexOf("input") == -1) ? qty_n : $.trim(qty);

        if (model == '' || size == '' || qty == '') {

            $j("#noData").html("Please enter Device(s) informtion.");

            return false;
        }
        else if (isNaN(qty)) {
            $j("#noData").html("Please enter valid information for the Device(s).");
            return false;
        }
    }

    return true;
}

//*************************************************** Custom Action Menu Formatter for JQ Grid - Start **************************************************** //

/*
cellvalue - the cell value
options - a set of options containing
rowId - the id of the row
colModel - the colModel for this column
rowData - the data for this row
*/

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete'  title='Delete'  onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

//var to store last selected row id
var lastSel;

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#devices').editRow(rowId);
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });

}

function SaveRow(rowId) {
    $('#devices').saveRow(rowId, AfterSaveRow);
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#devices').restoreRow(rowId);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#devices').delGridRow(rowId, { delData: { InvoiceItemID: $("#devices").getRowData(rowId).Id} });
}

function AfterSaveRow(result) {
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    $('#devices').trigger("reloadGrid");
    return false;
}
//*************************************************** Custom Action Menu Formatter for JQ Grid - End **************************************************** //

function initDeviceGrid() {
    $j("#devices").jqGrid(
    {
        url: jQuery.rootPath + '/MailBack/GetSelectedDevices',
        datatype: "json",
        mtype: 'GET',
        colNames: ['Manufacturer', 'Type', 'Model*', 'Size*', 'Quantity*', 'Weight*', 'Dimension*', 'Need Boxes?', 'Box Size', 'Boxes Qty.', 'Actions'],
        colModel: [
                    { name: 'Manufacturer', index: 'Manufacturer', width: 100, sortable: false, editable: true, edittype: 'select', formatter: 'select', editrules: { required: true }, editoptions: { value: mfgList} },
                    { name: 'Type', index: 'Type', width: 100, editable: true, sortable: false, editable: true, edittype: 'select', formatter: 'select', editoptions: { value: deviceList }, editrules: { required: true} },
                    { name: 'Model', index: 'Model', width: 100, editable: true, sortable: false, editrules: { required: true} },
                    { name: 'Size', index: 'Size', width: 100, editable: true, sortable: false, editrules: { required: true} },
                    { name: 'Quantity', index: 'Quantity', width: 80, editable: true, sortable: false, align: 'center', editrules: { required: true, integer: true} },
                    { name: 'Weight', index: 'Weight', width: 100, editable: true, sortable: false, editrules: { custom: true, custom_func: validate} },
                    { name: 'Dimension', index: 'Dimension', width: 100, editable: true, sortable: false, editrules: { custom: true, custom_func: validate} },
                    { name: 'NeedBox', index: 'NeedBox', width: 80, editable: true, sortable: false, align: 'center', formatter: 'checkbox', edittype: 'checkbox', editoptions: { value: 'Yes:No' }, editrules: { required: true }, hidden: true },
                    { name: 'BoxSize', index: 'BoxSize', width: 100, editable: true, sortable: false, align: 'center', formatter: 'select', edittype: 'select', stype: 'select', editoptions: { value: boxSize }, hidden: true },
                    { name: 'BoxesQuantity', index: 'BQuantity', width: 80, editable: true, sortable: false, align: 'center', hidden: true },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 60, editable: false }

        ],
        caption: 'Devices to be Mailed Back',
        width: 930,
        viewrecords: true,
        //pager: '#devPager',
        rownumbers: true,
        loadComplete: function () {
            isGridLoaded = true;
        },
        afterInsertRow: function (rowid, aData) {
            if (isGridLoaded === false && editOnLoad === true) {
                $j.fn.fmatter.rowactions(rowid, 'devices', 'edit', false);
            }

            //$j("div.ui-inline-del", "tr[id=" + rowid + "]")[0].setAttribute("onclick", "deleteRow('" + rowid + "');");

        },
        editurl: jQuery.rootPath + '/MailBack/Save',

        beforeSubmit: function (postdata, formid) {
            alert(postdata);

        }

    });
}
function showHelp(elemId) {
    $("#" + elemId).dialog({
        modal: false,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });
}

function validate(value, colName) {
    if (colName == "Weight*") {
        if (value == null || value == "") {
            return [false, "Weight*: Field is required"];
        }
        var weightReg = /^\d+\.{0,1}\d*$/;
        if (!weightReg.test(value)) {
            return [false, "Weight*: Invalid Weight value."];
        }
    }
    if (colName == "Dimension*") {
        if (value == null || value == "") {
            return [false, "Dimension*: Field is required"];
        }
        var dimensionReg = /^\d+\.{0,1}\d*[xX]\d+\.{0,1}\d*[xX]\d+\.{0,1}\d*$/;
        if (!dimensionReg.test(value)) {
            return [false, "Dimension*: Invalid Dimension format, Please try length x width x height(3x2x1)."];
        }
    }
    return [true, ""];
}