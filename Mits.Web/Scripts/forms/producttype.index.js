﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;

    //'ProductType dropdown onchange function' for getting details against a particular product

    $('select[id$=ddlProductType]').change(function (elem) {


        productType = $("select#ddlProductType").val();


        $('input[id$=btnClear]').click();

        $("select#ddlProductType").val(productType);

        $.ajax({
            url: rootPath + '/ProductType/GetDetail',
            dataType: 'json',
            type: "POST",
            data: ({ productType: productType }),
            async: false,
            success: function (result) {
                $("input#txtName").val(result.Name);
                $("textarea#txtDesc").val(result.Desc);
                $("textarea#txtNotes").val(result.Notes);
                $("select#ddlImageId").val(result.Image);
                $("input#txtWeight").val(result.Weight);
                $("select#ddlWeightQuantityType").val(result.WeightQuantity);
                $("input#txtContainer").val(result.Container);
                $("select#ddlContainerType").val(result.ContainerType);



            }
        });
    });

    //'Clear button onclick function' for clearing all necessary fields

    $('input[id$=btnClear]').click(function () {

        $("select#ddlProductType").val(0);
        $("input#txtName").val("");
        $("textarea#txtDesc").val("");
        $("textarea#txtNotes").val("");
        $("select#ddlImageId").val(0);
        $("input#txtWeight").val("");
        $("select#ddlWeightQuantityType").val(0);
        $("input#txtContainer").val("");
        $("select#ddlContainerType").val(0);
        $("ul#message").css("visibility", "hidden");
        $("label#lblMessage").text("");
        $('input[id$=txtName]').css("background-color", "white");

    });


    //'Submit button onclick function' for submitting necessary data to the controller action

    $('input[id$=btnSubmit]').click(function () {

        if ($('input[id$=txtName]').val() == "") {

            $('input[id$=txtName]').css("background-color", "#FFEEEE");
            $("label#lblMessage").text("Name is required").css("color", "Red");
            $("ul#message").css("visibility", "visible");
            return false;
        }
        var oForm = document.forms[0];
        var oRequired;
        var Id = '';

        var bValidated = true;
        if (oForm) {
            for (var iControl = 0; iControl < oForm.length; iControl++) {
                oRequired = oForm[iControl];

                if (oRequired) {
                    var regularExpAtt = oRequired.getAttribute('regexp');
                    if (regularExpAtt) {
                        var reg = regularExpAtt;
                        if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                        {
                            bValidated = false;
                            Id = oRequired.getAttribute('Id');
                            $("#" + Id + "").css("background-color", "#FFEEEE");
                            $("label#lblMessage").text(oRequired.getAttribute('regexpmesg')).css("color", "Red");
                            $("ul#message").css("visibility", "visible");
                            return bValidated;
                        }
                    }
                }
            }
        }

    

        $('input[id$=txtName]').css("background-color", "white");
        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");

        productType = $("select#ddlProductType").val();
        name = $("input#txtName").val();
        desc = $("textarea#txtDesc").val();
        notes = $("textarea#txtNotes").val();
        imageId = $("select#ddlImageId").val();
        weight = $("input#txtWeight").val();
        weightQuantity = $("select#ddlWeightQuantityType").val();
        container = $("input#txtContainer").val();
        containerType = $("select#ddlContainerType").val();
       
        $.ajax({
            url: rootPath + '/ProductType/Save',
            dataType: 'json',
            type: "POST",
            data: ({ productType: productType, name: name, desc: desc, notes: notes, imageId: imageId, weight: weight, weightQuantity: weightQuantity, container: container, containerType: containerType }),
            async: false,
            success: function (result) {
                if (result.AccessDenied == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else {
                    $("label#lblMessage").text(result.message);
                    $("ul#message").css("visibility", "visible");
                    if (result.data) {
                        $("select#ddlAccreditation").fillSelect(result.data);
                        $("select#ddlAccreditation").val(result.selected);
                    }
                }
            }
        });




    });

});

//Text area max length 

function imposeMaxLength(obj, maxLen) {
    return (obj.value.length <= maxLen);
}