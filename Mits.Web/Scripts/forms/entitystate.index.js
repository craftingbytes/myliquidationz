﻿
$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;

    //'State dropdown onchange function' For getting policies against a particular state

    $('select[id$=ddlEntity]').change(function (elem) {

        entity = $("select#ddlEntity").val();
        entity = parseInt(entity);
        if (entity == "0") {
            $("input[name=selectedObjects]").prop('checked', false);
            $("#validationSummary").html("");
        }
        else {

            $.ajax({
                url: rootPath + '/EntityState/GetStates',
                dataType: 'json',
                type: "POST",
                data: ({ entity: entity }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("input[name=selectedObjects]").prop('checked', false);
                        $.each(result.data, function (index, item) {
                            $("input[id=" + item.Value + "]").prop('checked', true);
                        });
                    }
                }
            });
        }
    });

    //'Clear button onclick function' For clearing necessary fields

    $('input[id$=btnClear]').click(function () {
        $(this.form).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                    $(this).val(0);
                    break;
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                    this.checked = false;
                    break;
            }
        });
        $("#validationSummary").html("");
    });

    //'Submit button onclick function' For submitting data to the controller action

    $('input[id$=btnSubmit]').click(function () {


        var dynamicHTML = "";
        dynamicHTML += "<ul class=ValidationSummary>";


        if (!ValidateForm(document.forms[0]))
            return false;
        if ($("input:checkbox:checked").length == 0) {
            dynamicHTML += "<li>Please check atleast one state</li>";
            $("#validationSummary").html(dynamicHTML);
            return false;
        }
        entity = $("select#ddlEntity").val();
        var states = "";
        $("input:checkbox:checked").each(function () {
            if (states == "")
                states = $(this).val();
            else
                states = states + ',' + $(this).val();
        });

        $.ajax({
            url: rootPath + '/EntityState/Save',
            dataType: 'json',
            type: "POST",
            data: ({ entity: entity, states: states }),
            async: false,
            success: function (result) {
                if (result.AccessDenied == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else {
                    if (result.data) {
                        $("select#ddlEntity").fillSelect(result.data);
                        $("select#ddlEntity").val(entity);
                        $("input#selectedObjects").prop('checked', true);
                        dynamicHTML += "<li>" + result.message + "</li>";
                        dynamicHTML += "</ul>";
                        $("#validationSummary").html(dynamicHTML);
                    }
                }
            }
        });


    });
});
function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var type;
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                type = oRequired.getAttribute('type');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || (oRequired.value == "0" && type != "text")) {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }

                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}

function Cancel() {
    var url = '/Affiliate/Index/';
    window.location.href = url + "?data=" + $('#data').val();
}