﻿//var to store last selected row id
var lastSel;
//var inEditMode = false;
var weightCategoryId = 0;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;

var sel;


$(document).ready(function () {


    $('input[id$=ProcessingDate]').datepicker({});

    var planyear = $("#PlanYear").val();
    $("#ddlPlanYear").val(planyear);


    $("#ddlPlanYear").change(function () {
        ChangePlanYear($("#ddlPlanYear").val());
    });


    $('input[type=text][req=true]').blur(function (e) {
        if ($(this).val() == '') {
            $(this).css("background-color", "#FFEEEE");
            $(this).data("isValid", false);
        } else {
            $(this).css("background-color", "#FFFFFF");
            $(this).data("isValid", true);
        }
    });

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var newZip = $("#PickupZip").val();
        $("#PickupZip").val($.trim(newZip));
        var isValid = ValidateForm(); //true;


        if (isValid) {
            var d = new Date($("#ProcessingDate").val());
            var now = new Date();
            var flag = d > now;
            if (flag) {
                $("#validationSummary").html("<ul><li>Processing Date cannot be in the future.</li></ul>");
                isValid = false;
            }
            else
                isValid = true;
        }

        var isFocused = false;
        isFocused = false;


        if (!isValid) {
            return false;
        }
        else {
            $("form").attr("action", rootPath + "/ProcessingData/Edit");
            $("form").submit();
        }



    });

    $("#btnAdd").button().click(function () {
        addRow();
    });


    if ($('#AffiliateTypeId').val() == 5) //recycler
        $("#btnReapplyRates").show();
    else
        $("#btnReapplyRates").hide();

    $("#btnReapplyRates").button().click(function () {
        ReApplyRates();
    });


    $('#ddlWeightCategories').change(function () {
        ChangeWeightCategory();
    });

    $("#btnExcel").button().click(function () {
        Dialog(false);

    });


    sel = $("#ddlPickupState");
    sel.data("prev", sel.val());
    sel.change(function (data) {
        var jqThis = $(this);

        $("#dialog-changestate").removeClass('ui-icon ui-icon-circle-check');

        $("#dialog-changestate").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            close: function (event, ui) {
                $("#ddlPickupState").val(jqThis.data("prev"));
            },
            buttons: {
                "No": function () {
                    //$("#ddlPickupState").val(jqThis.data("prev"));
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    jqThis.data("prev", jqThis.val());
                    $(this).dialog("close");
                    window.location = '#';

                    ChangePickupState(jqThis.val());

                }
            }
        });
    });
    InitGrid();

});


function ChangeWeightCategory() {
    var stateWeightCategoryId = $('#ddlWeightCategories').val();
    $.ajax({
        url: rootPath + '/ProcessingData/ChangeWeightCategory',
        dataType: 'json',
        type: "POST",
        data: ({ stateWeightCategoryId: stateWeightCategoryId }),
        async: false,
        success: function (result) {
            if (result.success) {
                $("#jqGrid").trigger("reloadGrid");
                alert(result.message);
            }
            else {
                alert(result.message);
                $('#ddlWeightCategories').val(result.stateWeightCategoryId);
            }
            
        }
    });
    
}

function InitGrid() {
    $("#jqGrid").jqGrid({
        //data: mydata,
        datatype: 'json',
        mtype: 'GET',
        url: rootPath + '/ProcessingData/GetInvoiceItems',
        height: 150,

        colNames: ['Pallet Unit #*', 'Product*', 'Total Qty/Weight*', 'Tare*', 'Net Qty/Weight', 'Rate', 'Actions' ],
        colModel: [
   		            { name: 'PalletUnitNumber', index: 'PalletUnitNumber', editable: true, width: 195, align: 'right', sortable: false, editrules: { integer: true, required: true} },
   		            { name: 'Description', index: 'Description', editable: true, width: 200, editrules: { required: true }, sortable: false, formatter: 'select', edittype: 'select' },
   		            { name: 'Qty', index: 'Qty', width: 100, editable: true, align: 'right', sortable: false, editrules: { required: true, integer: true} },
   		            { name: 'Tare', index: 'Tare', width: 100, editable: true, align: "right", sortable: false, editrules: { required: true, integer: true} },
   		            { name: 'TotalWeight', index: 'TotalWeight', width: 100, editable: false, sortable: false, align: 'right' },
                    { name: 'Rate', index: 'Rate', width: 100, editable: false, align: 'right', hidden: ($('#AffiliateTypeId').val() == 5 ? false : true) },
                    {name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],
        rowNum: 10000,
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        editurl: rootPath + '/ProcessingData/EditInvoiceItem',
        width: 925,
        height: 300 //,
        //afterInsertRow: function (rowid, aData) {
        //    alert("insert");
        //    be = "<input id='" + rowid + "_edRow' style='height:22px;padding:0' type='button' value='Edit' onclick=\"editRow('" + rowid + "');\" title='Edit Record' />";
        //    se = "<input id='" + rowid + "_savRow' style='height:22px;padding:0' type='button' value='Save' onclick=\"saveRow('" + rowid + "');\" title='Save Record' />";
        //    ce = "<input id='" + rowid + "_canRow' style='height:22px;padding:0' type='button' value='Cancel' onclick=\"restoreRow('" + rowid + "');\" title='Cancel Edit' />";
        //    //TotalWeight = aData["Qty"] - aData["Tare"] + 0;
        //    $("#jqGrid").jqGrid('setRowData', rowid, { TotalWeight: TotalWeight, Action: be + se + ce });
        //    $("input[type=button]").button();
        //
        //}
    });

    //$("#jqGrid").footerData('set', { "Description": 'Totals', "Qty": '0', "Tare": '0', 'TotalWeight': '0' }, false);
    LoadDeviceTypes();

}


function addRow() {
    jQuery("#jqGrid").jqGrid('editGridRow', 'new', {
        height: 200,
        recreateForm: true,
        reloadAfterSubmit: false,
        addCaption: "Add Record",
        editCaption: "Edit Record",
        bSubmit: "Add",
        bCancel: "Done",
        bClose: "Close",
        saveData: "Data has been changed! Save changes?",
        bYes: "Yes",
        bNo: "No",
        bExit: "Cancel",
        modal: true,
        addedrow: 'last',
        closeAfterEdit: false,
        url: rootPath + '/ProcessingData/EditInvoiceItem',
        afterSubmit: function (response, postdata) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            //$('#TotalWeight').val(result.totalWeight);
            return [result.success, result.message, result.invoiceItemId];
        },
        afterShowForm: function (formid) {
            //setPalletNumber();
        },
        afterComplete: function (response, postdata, formid) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            $("#jqGrid").jqGrid("setCell", result.invoiceItemId, 'TotalWeight', result.totalWeight);
            $("#jqGrid").jqGrid("setCell", result.invoiceItemId, 'Rate', result.rate);
            return true;
        },
        viewPagerButtons: false
    });

}





function EditRow(rowId) {
    if (rowId && rowId != lastSel) {
        lastSel = rowId;
    }
    jQuery("#jqGrid").jqGrid('editGridRow', rowId, {
        height: 280,
        recreateForm: true,
        reloadAfterSubmit: false,
        addCaption: "Add Record",
        editCaption: "Edit Record",
        bSubmit: "Save",
        bCancel: "Cancel",
        bClose: "Close",
        saveData: "Data has been changed! Save changes?",
        bYes: "Yes",
        bNo: "No",
        bExit: "Cancel",
        modal: true,
        closeAfterEdit: true,
        viewPagerButtons: false,
        afterSubmit: function (response, postdata) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            //$('#TotalWeight').val(result.totalWeight);
            return [result.success, result.message, rowId]
        },
        onClose: function (response, postdata) {
            CancelRow(rowId);
        },
        afterComplete: function (response, postdata, formid) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            $("#jqGrid").jqGrid("setCell", rowId, 'TotalWeight', result.totalWeight);
            $("#jqGrid").jqGrid("setCell", rowId, 'Rate', result.rate);
            //SaveRow(rowId);
        }

    });
    //inEditMode = true;
}

function CancelRow(rowId) {
    //$.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.ItemId == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
        lastSel = lastSel - 1;
    }
    //inEditMode = false;
}


function GetPickupCities(elem) {
    GetCities(elem);
    GetWeightCategories();
}

function GetWeightCategories() {
    state = $("select#ddlPickupState").val(); 
    $.ajax({
        url: rootPath + '/ProcessingData/GetStateWeightCategories',
        dataType: 'json',
        type: "POST",
        data: ({ state: state }),
        async: false,
        success: function (result) {
            if (result.data) {
                    $("select#ddlWeightCategories").fillSelect(result.data);
            }
        }
    });

}

function ChangePickupState(pickupStateId) {
    //var pickupStateId = $('#ddlPickupState').val();
    $.ajax({
        url: rootPath + '/ProcessingData/ChangePickupState',
        dataType: 'json',
        type: "POST",
        data: ({ pickupStateId: pickupStateId }),
        async: false,
        success: function (result) {
            if (result.success) {
                GetCities();
                GetWeightCategories();
                $("#jqGrid").trigger("reloadGrid");
                LoadDeviceTypes();
            }
            else {
                alert(result.message);
                $('#ddlPickupState').val(result.pickupStateId);
                sel.data("prev", pickupStateId);
            }

        }
    });
}

function ChangePlanYear(planYear) {
    $.ajax({
        url: rootPath + '/ProcessingData/ChangePlanYear',
        dataType: 'json',
        type: "POST",
        data: ({ planYear: planYear }),
        async: false,
        success: function (result) {
            if (result.success) {
                $("#jqGrid").trigger("reloadGrid");
                LoadDeviceTypes();
            }
            else {
                alert(result.message);
                $('#ddlPlanYear').val(result.planYear);
            }

        }
    });
}


function ReApplyRates() {
    planYear = $('#ddlPlanYear').val();
    $.ajax({
        url: rootPath + '/ProcessingData/ChangePlanYear',
        dataType: 'json',
        type: "POST",
        data: ({ planYear: planYear }),
        async: false,
        success: function (result) {
            if (result.success) {
                $("#jqGrid").trigger("reloadGrid");
            }
            else {
                alert("An error occurred re-applying rates");
            }

        }
    });
}


function GetCities() {
    state = $('#ddlPickupState').val();
    $.ajax({
        url: rootPath + '/Affiliate/GetCities',
        dataType: 'json',
        type: "POST",
        data: ({ state: state }),
        async: false,
        success: function (result) {
            if (result.data) {
                    $("select#PickupCityId").fillSelect(result.data);    
            }
        }
    });
}

function GetVendorCities() {
    state = $('#VendorStateId').val();
    $.ajax({
        url: rootPath + '/Affiliate/GetCities',
        dataType: 'json',
        type: "POST",
        data: ({ state: state }),
        async: false,
        success: function (result) {
            if (result.data) {
                $("select#VendorCityId").fillSelect(result.data);
            }
        }
    });

}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}

//function ActionMenuFormatter(cellvalue, options, rowdata) {
    //var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/></div>";
    //actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    //return actionMenuHtml;
//    return "&nbsp";
//}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/>";
    //var actionMenuHtml  = "<img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/>";
    return actionMenuHtml;
}


function LoadDeviceTypes(elem) {

    state = $("select#ddlPickupState").val();       //Represents RegionID
    city = $("input#txtPickupCity").val();
    zip = $("input#txtPickupZip").val();
    planyear = $("select#ddlPlanYear").val(); 
    $.ajax({
        url: rootPath + '/ProcessingData/GetDevicesList',
        dataType: 'text',
        type: "POST",
        data: ({ city: city, state: state, zip: zip, planyear: planyear }),
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('Description', { editoptions: { value: result} });
        }
    });

}

function LoadData(fileName) {
    $.ajax({
        url: rootPath + '/ProcessingData/ExcelImport2',
        dataType: 'json',
        type: "POST",
        data: ({ fileName: fileName }),
        async: false,
        success: function (result) {
            if (result.success) {
                $("#jqGrid").trigger("reloadGrid");
            }
            else {
                alert(result.message);
            }

        }
    });
}

function Dialog(attachfile) {

    var w = window.open("../../Upload.aspx?attachfile=" + attachfile, "Window1", "height=300,width=450,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");

}

function SaveRow(rowId) {
    if ($("#jqGrid").saveRow(rowId, false, 'clientArray')) {
        var newData = $("#jqGrid").getRowData(rowId);
        if (parseInt(newData["Qty"]).toString() == 'NaN') {
            return;
        }
        else {
            return;
            //inEditMode = false;

            //$('#ED_ActionMenu_' + lastSel).css({ display: "block" });
            //$('#SC_ActionMenu_' + lastSel).css({ display: "none" });
        }
    }
}

function DeleteRow(rowId) {
    $.ajax({
        url: "/ProcessingData/DeleteInvoiceItem",
        dataType: 'json',
        type: "POST",
        data: ({ invoiceItemId: rowId }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.success)
                jQuery("#jqGrid").jqGrid('delRowData', rowId);
            else
                alert(result.message);
        }
    });

}