﻿$(document).ready(function () {
    
    jQuery("#jqGrid").jqGrid({
        url: "/AuditReport/GetAuditReports",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id','PlanYear', 'State', 'Facility Name', 'Audit Date', 'Documents', 'Actions'],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
        { name: 'PlanYear', index: 'PlanYear', width: 80, editable: true, sortable: false, editrules: { required: true, number:true }},
   		{ name: 'State', index: 'State', width: 80, editable: true, sortable: false, editrules: { required: true }, edittype: 'select' },
   		{ name: 'FacilityName', index: 'FacilityName', width: 150, editable: true, sortable: false, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'AuditDate', index: 'AuditDate', width: 100, editable: true, sortable: false, 
            editoptions: {
   		        dataInit: function (elem) { $(elem).datepicker({ buttonImage: '../../Content/images/calendar.gif', altFormat: 'mm/dd/yyyy', buttonImageOnly: true }); }
   		    }
   		},
        { name: 'Documents', index: 'Documents', sortable: false, width: 50, editable: false, formatter: documentFormatter, title: true, align: 'center' },
  		{ name: 'ActionMenu', index: 'ActionMenu', sortable: false, width: 50, editable: false, formatter: actionFormatter, title: true, align: 'center', hidden: ($('#canUpdate').val() == 'disabled' && $('#canDelete').val() == 'disabled') }
   	    ],
        rowNum: 50,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 900,
        height: 230,
        editurl: "/AuditReport/EditAuditReport",
        gridComplete: function () { LoadDropdownsData(); }
    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });
    $(".ui-paging-info", '#pager_left').remove();

    $("#btnSearch").click(function () {
        var stateId = $('#ddlState').val();
        var facilityName = $('#txtFacilityName').val();
        var planyear = $('#txtPlanYear').val();
        //var auditDateFrom = $('#txtAuditDateFrom').val();
        //var auditDateTo = $('#txtAuditDateTo').val();
        jQuery("#jqGrid").setGridParam({ postData: { stateId: stateId, facilityName: facilityName, planyear: planyear }, page: 1 }).trigger("reloadGrid");
    });

    $('#btnAdd').click(function () {
        window.location.href = '/AuditReport/Add';
    });

    $("#dialogDocuments").dialog({
        modal: false,
        width: 400,
        height: 300,
        resizable: false,
        position: 'center',
        autoOpen: false,
        show: 'fold',
        hide: 'fold',
        Ok: function () {
            $(this).dialog('close');
        }
    });
});

$(function () {
    //$("#txtAuditDateFrom").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
    //$("#txtAuditDateTo").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
});

$(function () {
    var offset = $("#ddlFacilityName").offset();
    var width = $("#ddlFacilityName").width() - 20;
    var height = $("#ddlFacilityName").height() - 2;
    var left = offset.left + 2;
    var top = offset.top + 1;
    $("#txtFacilityName").css({
        'display':'block',
        'position':'absolute',
        'left':left,
        'top':top,
        'height':height,
        'width':width
    });
    $("#ddlFacilityName").change(function () {
        var text = $("#ddlFacilityName option:selected").text();
        $("#txtFacilityName").val(text);
    });
});



function documentFormatter(cellvalue, options, rowObject) {
    var rowId = options.rowId;
    var docHtml = "<div>";
    docHtml += "<img src='../Content/images/icon/sm/paperclip.gif' style='cursor:hand;' alt='Open Documents' title='Open Documents' onclick=\"openDocuments('" + rowId + "');\" />";
    if ($('#canAdd').val() == "") {
        docHtml += "&nbsp;&nbsp;<img src='../Content/images/icon/sm/attach_file.gif' style='cursor:hand;' alt='Add Document' title='Add Document' onclick=\"openFileDialog('" + rowId + "');\" />";
    }
    docHtml += "</div>";
    return docHtml;
}

function actionFormatter(cellvalue, options, rowObject) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'>";
    if ($('#canUpdate').val() == "") {
        actionMenuHtml += "<img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' class='edit'/>";
    }
    if ($('#canDelete').val() == "") {
        actionMenuHtml += "&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete' title='Delete' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/>";
    }
    actionMenuHtml += "</div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function LoadDropdownsData(elem) {
    $.ajax({
        url: '/AuditReport/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('State', { editoptions: { value: result.states} });
        }
    });
}

var lastSel;

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#jqGrid').editRow(rowId);
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });

}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { afterSubmit: AfterSaveRow });
}

function AfterSaveRow(result) {
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    var msg = result.responseText.split(':')[1];
    if (msg == "AccessDenied") {
        var url = '/Home/AccessDenied';
        window.location.href = url;
    } else {
        alert(msg);
    }
    $('#eData').click();
    return true;
}

function openDocuments(auditReportId) {
    var docsHtml = "<table width='100%'>";
    $.ajax({
        url: '/AuditReport/GetDocuments',
        dataType: 'json',
        type: "POST",
        data: ({ auditReportId: auditReportId }),
        async: false,
        success: function (result) {
            if (result.AccessDenied == "AccessDenied") {
                window.location.href = '/Home/AccessDenied';
                return;
            }
            $.each(result.rows, function (index, entity) {
                var fileName = entity.UploadFileName.replace("'", "\\\'");
                docsHtml += "<tr><td width='85%'><a href=\"javascript:download('" + fileName + "');\" style='color:#FF9900;text-decoration:none;_text-decoration:none;'>" + entity.UploadFileName + "</a></td>";
                if ($('#canDelete').val() == "") {
                    docsHtml += "<td width='15%'><img src='../../Content/images/icon/delete.png' alt='Remove attachment' style='cursor:hand;' title='Remove attachment' id='btnDel' border='0' onclick='javascript:delDocument(" + auditReportId + "," + entity.DocumentID + ");' /></td>";
                }
                docsHtml += "</tr>";
            });
        }
    });
    docsHtml += "</table>"

    $("#dialogDocuments").html(docsHtml);
    $("#dialogDocuments").dialog('open');
}

function download(fileName) {
    fileName = fileName.replace('#', '<~>');
    if (fileName != "-" && fileName != "") {
        window.location = "/Affiliate/Download?FileName=" + fileName;
    }
}

function delDocument(auditReportId, documentId) {
    var ans = confirm('Are you sure to delete the document?');
    if (ans) {
        $.ajax({
            url: '/AuditReport/DeleteDocument/',
            dataType: 'json',
            type: "POST",
            data: { documentId: documentId },
            async: false,
            success: function (result) {
                if (result.AccessDenied == "AccessDenied") {
                    window.location.href = '/Home/AccessDenied';
                    return;
                }
                openDocuments(auditReportId);
            }
        });
    }
}

function openFileDialog(auditReportId) {
    var w = window.open("../../Upload.aspx?auditReportId=" + auditReportId, "Window1", "height=300,width=450,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");
}

function addDocument(fileName, auditReportId) {
    $.ajax({
        url: '/AuditReport/AddDocument',
        dataType: 'json',
        type: "POST",
        data: ({ auditReportId: auditReportId, fileName: fileName }),
        async: false,
        success: function (result) {
            if (result.AccessDenied == "AccessDenied") {
                window.location.href = '/Home/AccessDenied';
            } else {
                alert(result.message);
            }
        }
    });
}