﻿//********************************************************************************************************************************************* //
//*************************************************** Affliate Contract Rate JQ Grid - Start ************************************************** //
//********************************************************************************************************************************************* //
var selectState;
var selectPlanYear;
var currentSelRow;

$(document).ready(function () {

    jQuery("#jqGrid").jqGrid({
        url: "/AffiliateTarget/GetAllAffiliateTargets",
        datatype: 'json',
        mtype: 'GET',

        colNames: [
            'AffTargetId',
            'State*',
            'Policy*',
            'Service Type*',
            'Quantity Type*',
            'Pace',
            'Quantity*',
            'Start Date*',
            'End Date*',
            'PlanYear*',
            'P Drop',
            'Products*',
            'Actions'],
        colModel: [
   		{ name: 'AffTargetId', index: 'AffTargetId', align: 'right', hidden: true, editable: true, sortable: false },
        { name: 'State', index: 'State', editable: true, width: 120, sortable: false, editrules: { required: true }, edittype: 'select', editoptions: { maxlength: 100 } },
        { name: 'Policy', index: 'Policy', editable: true, width: 115, sortable: false, editrules: { required: true }, edittype: 'select', editoptions: { maxlength: 100 } },
   		{ name: 'ServiceType', index: 'ServiceType', width: 115, editable: true, sortable: false, editrules: { required: true }, edittype: 'select', editoptions: { maxlength: 100 } },
   		{ name: 'QuantityType', index: 'QuantityType', width: 115, editable: true, sortable: false, editrules: { required: true }, edittype: 'select', editoptions: { maxlength: 100 } },
        { name: 'Pace', index: 'Pace', align: 'right', width: 30, editable: true, sortable: false, editrules: { required: true, number: true }, editoptions: { size: 30, maxlength: 5 } },
   		{ name: 'Weight', index: 'Weight', align: 'right', width: 105, editable: true, sortable: false, editrules: { required: true, number: true }, editoptions: { size: 30, maxlength: 17} },
        { name: 'StartDate', index: 'StartDate', width: 105, sortable: false, hidden:true, editable: true, datefmt: 'd/M/yyyy'
        },
   		{ name: 'EndDate', index: 'EndDate', width: 105, sortable: false, hidden:true, editable: true, datefmt: 'd/M/yyyy'
   		},
        { name: 'Planyear', index: 'Planyear', editable: true, width: 60, edittype: 'select', editrules: { custom: true, custom_func: validate }, editoptions: { maxlength: 50} },
        { name: 'Permanent', index: 'Permanent', editable: true, width: 80, edittype: 'checkbox', sortable: false, formatter: 'checkbox', align: 'center', editoptions: { defaultValue: 'false', value: "true:false" }, title: 'Permanent Drop Off Location' },
   		{ name: 'Products', index: 'Products', width: 105, align: 'center', editable: false, sortable: false},
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

        //

   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        autowidth: true,
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        //width: 850,
        height: 250,
        editurl: "/AffiliateTarget/EditAffiliateTarget"
        ,gridComplete: function () { LoadDropdownsData(); }
    });

    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $("#btnAdd").click(function () {
        $("#jqGrid").jqGrid('editGridRow', "new", { height: 280, width: 350, closeAfterAdd: true, beforeShowForm: BeforeShowFrom, beforeSubmit: BeforeSubmitFrom, afterSubmit: AfterSaveRow });
    });

    $("#btnAdd2").click(function () {
        AddNewRow();
    });

    $("#dialogProducts").dialog({
        modal: false,
        width: 400,
        height: 350,
        resizable: false,
        position: 'center',
        autoOpen: false,
        show: 'fold',
        hide: 'fold',
        Ok: function () {
            $(this).dialog('close');
        }

    });

});
datePick = function (elem) {
    jQuery(elem).datepicker();
}

function LoadDropdownsData(elem) {

    $.ajax({
        url: '/AffiliateTarget/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('State', { editoptions: { value: result.states, dataInit: function (elem) { BeforeEditState(elem)
            }
            }
            });
            $('#jqGrid').setColProp('Policy', { editoptions: { value: result.policies} });
            $('#jqGrid').setColProp('ServiceType', { editoptions: { value: result.serviceTypes} });
            $('#jqGrid').setColProp('QuantityType', { editoptions: { value: result.quantityTypes} });
            $('#jqGrid').setColProp('Planyear', { editoptions: { value: { 0: 'Select' }, dataInit: function (elem) { BeforeEditPlanYear(elem) } } });

            GetUserAccessRights();
        }
    });
}

function BeforeEditState(elem) {
    //editState = $(elem);
    selectState = $(elem);

    selectState.change(function () {
        FillPlanyears(selectState, selectPlanYear);
    });
}

function BeforeEditPlanYear(elem) {
    var stateId = selectState.val();
    selectPlanYear = $(elem);
    if (currentSelRow != null) {
        FillPlanYearContent(stateId, currentSelRow.Planyear);
    }
}

function FillPlanYearContent(stateId, selected) {
    $.ajax({
        url: '/AffiliateTarget/GetPlanyears',
        dataType: 'json',
        type: "POST",
        data: { stateId: stateId },
        async: false,
        success: function (result) {
            selectPlanYear.fillSelect(result.data);
            if (selected) {
                selectPlanYear.val(selected);
            }
        }

    });
}

function FillPlanyears(editState, selectPlanYear) {
    var stateId = editState.val();
    $.ajax({
        url: '/AffiliateTarget/GetPlanyears',
        dataType: 'json',
        type: "POST",
        data: ({ stateId: stateId }),
        async: false,
        success: function (result) {
            if (result.data) {
                selectPlanYear.fillSelect(result.data);
            }
        }
    });
}

function BeforeShowFrom(formid) {
    $(formid).find('select').css('width', '175px');
    $(formid).find('input').css('width', '100px');
    
    $.each(
    $(formid).find('select'),
    function (index, item) {
        item.selectedIndex = 0;
    }
    )
}

function BeforeSubmitFrom(param1, form) {
    var sDate = new Date(form.find('#StartDate').val());
    var eDate = new Date(form.find('#EndDate').val());
    if (sDate >= eDate)
        return [false, "Start date must be less than end date."];
    else
        return [true, ""];
}

function AddNewRow() {
    //colNames: ['Id', 'State', 'Policy', 'Service Type', 'Quantity Type', 'Quantity', 'StartDate', 'EndDate', 'Products', ''],
    var newRow = { Id: "_empty", State: '', Policy: '', ServiceType: '', QuantityType: '', Quantity: '0', StartDate: '', EndDate: '' };
    var rowCount = $("#jqGrid").getGridParam('reccount');
    $("#jqGrid").jqGrid('addRowData', rowCount + 1, newRow);
    $("#jqGrid").jqGrid('editRow', rowCount + 1);

}



function GetUserAccessRights() {
    $.ajax({
        url: '/AffiliateTarget/GetUserAccessRights',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            ShowHideCommandButton(result.showEdit, result.showDelete);
        }
    });
}

function ShowHideCommandButton(showEdit, showDelete) {

    if (showEdit == false && showDelete == false) {
        $("#jqGrid").hideCol('ActionMenu');
    }
    else {
        if (showEdit == false)
            $("#jqGrid").find('div > img').filter(".edit").css("display", "none");
        if (showDelete == false)
            $("#jqGrid").find('div > img').filter(".delete").css("display", "none");
    }

}


//*************************************************** Custom Action Menu Formatter for JQ Grid - Start **************************************************** //

/*
cellvalue - the cell value
options - a set of options containing
rowId - the id of the row
colModel - the colModel for this column
rowData - the data for this row
*/

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' class='edit' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")'/>&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' class='delete' style='cursor:hand;' alt='Delete'  title='Delete' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

//var to store last selected row id
var lastSel;

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    currentSelRow = $("#jqGrid").getRowData(rowId);
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function SaveRow(rowId) {
    var newData = $("#jqGrid").getRowData(rowId);
    var sDate = new Date($(newData["StartDate"]).val());
    var eDate = new Date($(newData["EndDate"]).val());
    if (sDate >= eDate)
        alert("Start date must be less than end date.");
    else {
        $('#jqGrid').saveRow(rowId, AfterSaveRow);
    }
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { delData: { InvoiceItemID: $("#jqGrid").getRowData(rowId).Id} });
}

function AfterSaveRow(result) {
    var msg = result.responseText.split(':')[1];
    if (msg == 'No Affiliate Contract Rate') {
        if (confirm("There aren't Contract Rate for the data criteria or date range.\nAre you want to add a contract rate?")) {
            window.location.href = "/AffiliateContractRate/Index";
        }
    } else {
        $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
        $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
        $('#jqGrid').trigger("reloadGrid");
        alert(msg);
    }
    return false;
}
//*************************************************** Custom Action Menu Formatter for JQ Grid - End **************************************************** //


//********************************************************************************************************************************************* //
//*************************************************** Affliate Contract Rate JQ Grid - End **************************************************** //
//********************************************************************************************************************************************* //



//********************************************************************************************************************************************* //
//********************************************************** Affliate Target Popups - Start *************************************************** //
//********************************************************************************************************************************************* //
function CreateProductsDivHTML(productTypes, canEdit) {
    var product = '';
    var productListHTML = "";

    for (i = 0; i < productTypes.split(';').length; i++) {
        var id = productTypes.split(';')[i].split(':')[0];
        var name = productTypes.split(';')[i].split(':')[1];
        if (canEdit)
            productListHTML += '<input type="checkbox" name="productType" value="' + id + '" /><label>' + name + '</label><br>';
        else
            productListHTML += '<input type="checkbox" name="productType" value="' + id + '" disabled=true /><label>' + name + '</label><br>';
    }
    $("#dialogProducts").html(productListHTML);

}

var affilateTargetID = null;

function ShowProductTypesDialog(rowID, productIDs) {
    $.ajax({
        url: '/AffiliateTarget/GetProductTypesByTargetId',
        dataType: 'json',
        type: "POST",
        data: {targetId : rowID},
        async: false,
        success: function (result) {
            CreateProductsDivHTML(result.productTypes, result.canEdit);
        }
    });
    $("#dialogProducts").dialog({
            buttons: {
                "Ok": function () { SubmitProducts(affilateTargetID, GetSelectedProduts()); $(this).dialog("close"); }
            }
        });
    
    affilateTargetID = rowID;
    $("#dialogProducts").dialog('open');
    SetAffilateTargetProducts(productIDs);
}


function SetAffilateTargetProducts(ids) {
    $("#dialogProducts input[type = checkbox]").each(function () {

        for (i = 0; i < ids.split(',').length; i++) {
            var id = ids.split(',')[i];
            this.checked = false;
            if (id == this.value) {
                this.checked = true;
                break;
            }
        }
    });
}

function GetSelectedProduts() {
    var pIds = new Array()
    $("#dialogProducts input[type = checkbox]:checked").each(function () {
        pIds.push(this.value);
    });
    return pIds;
}


function SubmitProducts(affiliateTargetID, productIDs) {
    $.ajax({
        url: '/AffiliateTarget/SaveAffliateProducts',
        dataType: 'json',
        type: "POST",
        data: { affiliateTargetID: affiliateTargetID, productIDs: productIDs.toString() },
        async: false,
        success: function (result) {
            $("#jqGrid").clearGridData(true);
            jQuery("#jqGrid").trigger("reloadGrid");
        }
    });
}

function Cancel() {
    var url = '/Affiliate/Index/';
    window.location.href = url + "?data=" + $('#data').val();
}

function validate(value, colName) {
    if (colName == "PlanYear*") {
        if (value == null || value == 0) {
            return [false, "PlanYear*: Field is required"];
        }
    }
    return [true, ""];
}
//********************************************************************************************************************************************* //
//********************************************************** Affliate Target Popups - End ***************************************************** //
//********************************************************************************************************************************************* //
