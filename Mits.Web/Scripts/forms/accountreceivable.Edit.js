﻿
//********************************************************************************************************************************************* //
//********************************************************** Account Receivable JQ Grid - Start ************************************************** //
//********************************************************************************************************************************************* //
$(document).ready(function () {

    jQuery("#jqGrid").jqGrid({
        url: "/AccountReceivable/GetAllUnPaidInvoices/" + GetAffiliateID(),
        datatype: 'json',
        mtype: 'GET',

        colNames: ['PaymentDetailID', 'Invoice Id', 'Due Date', 'Amount', 'Paid Amount', 'Unpaid Amount', 'Payment','', 'Action'],
        colModel: [
   		{ name: 'PaymentDetailID', index: 'Id', width: 50, align: 'left', hidden: true },
   		{ name: 'InvoiceId', index: 'InvoiceId', width: 50, align: 'left' },
   		{ name: 'DueDate', index: 'Due Date', align: "left", width: 60, sortable: false, editable: false, datefmt: 'd/M/yyyy' },
   		{ name: 'Amount', index: 'Amount', editable: false, align: 'left', width: 40, sortable: false },
   		{ name: 'PaidAmount', index: 'PaidAmount', align: 'left', width: 40, editable: false, sortable: false },
   		{ name: 'UnPaidAmount', index: 'UnPaidAmount', align: 'left', width: 40, editable: false, sortable: false },
   		{ name: 'Payment', index: 'Payment', align: 'left', width: 40, editable: true, sortable: false, editrules: { required: true, number: true }, editoptions: { size: 20, maxlength: 100 } },
   		{ name: 'OldPayment', index: 'OldPayment', width: 50, align: 'right', hidden: true },
         { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 20, editable: false }
   	],
        rowNum: 10000,
        rowList: [10, 25, 50, 100],
        autowidth: true,
        //pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        //width: 953,
        height: 220,
        //editurl: "/AccountReceivable/UpdatePaymentDetail"
        gridComplete: function () { GetUserAccessRights(); }
    });

    //$("#jqGrid").jqGrid('navGrid', '#pager',
    //            { edit: false, add: false, del: false, search: false, refresh: true });

});

// Get user access rights from server and show hide grid menu action buttons
function GetUserAccessRights() {
    $.ajax({
        url: '/AccountReceivable/GetUserAccessRights',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            ShowHideCommandButton(result.showEdit, result.showDelete);
        }
    });
}

function ShowHideCommandButton(showEdit, showDelete) {

    if (showEdit == false && showDelete == false) {
        $("#jqGrid").hideCol('ActionMenu');
    }
    else {
        if (showEdit == false)
            $("#jqGrid").find('div > img').filter(".edit").css("display", "none");
        if (showDelete == false)
            $("#jqGrid").find('div > img').filter(".delete").css("display", "none");
    }
}


datePick = function (elem) {
    jQuery(elem).datepicker();
}

function ReLoadJQGrid() {
    jQuery("#jqGrid").setGridParam({ url: "/AccountReceivable/GetAllUnPaidInvoices/" + GetAffiliateID() }).trigger("reloadGrid")
}

$(function () {
    $("#Payment_Date").datepicker({ dateFormat: 'mm/dd/yy' });
});

//*************************************************** Custom Action Menu Formatter for JQ Grid - Start **************************************************** //

/*
cellvalue - the cell value
options - a set of options containing
rowId - the id of the row
colModel - the colModel for this column
rowData - the data for this row
*/

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/> <img src='../../Content/images/icon/sm/plus.gif' style='cursor:hand;'  alt='AddPayment'  title='AddPayment' onclick='javascript:AddPaymentAmount(\"" + options.rowId + "\")' style='border:none;'class='edit'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

//var to store last selected row id
var lastSel;

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function AddPaymentAmount(rowId) {
    var oldData = $("#jqGrid").getRowData(rowId);
    var unPaidAmount = parseFloat(oldData["UnPaidAmount"]);
    jQuery("#jqGrid").jqGrid('setRowData', rowId, { Payment: unPaidAmount });
}

function SaveRow(rowId) {

    var oldData = $("#jqGrid").getRowData(rowId);
    
    if ($("#jqGrid").saveRow(rowId, false, 'clientArray')) {
        
        var newData = $("#jqGrid").getRowData(rowId);
        var payment = parseFloat(newData["Payment"]);
        var amount = parseFloat(newData["Amount"]);
        var paidAmount = parseFloat(newData["PaidAmount"]);
        var unPaidAmount = parseFloat(newData["UnPaidAmount"]);

        if (isNaN(payment)) {
            EditRow(rowId)
        } else if (payment < 0) {
            payment = parseFloat(newData["OldPayment"]);
            $("#jqGrid").jqGrid('setRowData', rowId, { Payment: payment });
            EditRow(rowId)
            alert('Payment must be non negative.');
        } else if (payment > unPaidAmount) {
            payment = parseFloat(newData["OldPayment"]);
            $("#jqGrid").jqGrid('setRowData', rowId, { Payment: payment });
            EditRow(rowId)
            alert('Payment must be less than Unpaid amount.');
        }
        else {
            paidAmount = paidAmount + payment;
            unPaidAmount = unPaidAmount - payment;
            $("#jqGrid").jqGrid('setRowData', rowId, { Payment: payment });
            AfterSaveRow();
        }
    }
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { delData: { InvoiceItemID: $("#jqGrid").getRowData(rowId).Id} });
}

function AfterSaveRow() {

        $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
        $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
}
//*************************************************** Custom Action Menu Formatter for JQ Grid - End **************************************************** //

function submitForm() {
    if (lastSel) {
        CancelRow(lastSel);
    }
    
    var paymentDetails = '';
    $("#jqGrid").find('tr').each(function (index, ele) {
        if ($("#jqGrid").getRowData(index)["PaymentDetailID"]) {
            paymentDetails += $("#jqGrid").getRowData(index)["PaymentDetailID"] + ":" + $("#jqGrid").getRowData(index)["InvoiceId"] + ":" + $("#jqGrid").getRowData(index)["Payment"] + ";";
        }
    });
    $("#txtPaymentDetails").val(paymentDetails);
    var rootPath = window.location.protocol + "//" + window.location.host;
    var isValid = ValidateForm();

    if (isValid) {
        if (ValidateAmount()) {
            document.forms[0].action = rootPath + '/AccountReceivable/Edit';
            document.forms[0].submit();
        }
    }

    return false;
}

function OnAffliateChange(id) {
    ReLoadJQGrid();
}

function GetAffiliateID() {
    if ($('#Payment_AffiliateId').val() == '')
        return 0;
    else
        return $('#Payment_AffiliateId').val();
}

function ValidateAmount() {
    var Payment = 0;
    $("#jqGrid").find('tr').each(function (index, ele) {
        if ($("#jqGrid").getRowData(index)["PaymentDetailID"]) {
            Payment = Payment + parseFloat($("#jqGrid").getRowData(index)["Payment"]);
        }
    });
    var Amount = parseFloat($("#Payment_Amount").val());
    if (Math.round(Payment*100)/100 > Math.round(Amount*100)/100) {
        alert('Amount must be greater than or equal to sum of payment');
        return false;
    }
    return true;
}

$("#divMessage").dialog({
    modal: true,
    width: 'auto',
    resizable: false,
    position: 'center',
    autoOpen: false
}
);


function ShowMessage(message) {
    $("#divMessage").html(message);
    $("#divMessage").dialog({ buttons: { "Ok": function () { $(this).dialog("close"); } } });
    $("#divMessage").dialog('open');
}