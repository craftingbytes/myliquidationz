﻿$(document).ready(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;

    $.ajax(
            {
                url: rootPath + '/Reconciliation/GetEntities/',
                dataType: 'json',
                type: "POST",
                data: ({}),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#EntityId").fillSelect(result.data);
                    }
                }
            });



            $("#jqGrid").jqGrid({
                url: '/Reconciliation/GetReconciliationHistory',
                datatype: 'json',
                mtype: 'GET',

                colNames: ['OEM Invoice','Reconcile Date','Reconcile total amount','Action'],
                colModel: [
                  { name: 'InvoiceId', width: 50 },
                  { name: 'ReconcileDate', width: 100 },
                  { name: 'ReconcileTotalAmount', width: 60, align: 'right', sortable: false },
                  { name: 'Action', hidden: false,formatter:RevertFormatter, width: 20, align: 'center' }

      ],
                rowNum: 10000,
                rowList: [10, 25, 50, 100],
                pager: "",
                viewrecords: true,
                rownumbers: false,
                width: 900,
                height: 180
            });

            $('select[id$=EntityId]').change(function (elem) {
                LoadReconciliationData();
            });
        })


        function LoadReconciliationData() {
            var oemId = $("select#EntityId").val();
            jQuery("#jqGrid").setGridParam({ postData: { oemId: oemId }, page: 1 }).trigger("reloadGrid");
            
        }


        function RevertFormatter(cellvalue, options, rowObject) {
            var invoiceId = options.rowId;
            return "<div><a href='javascript:OEMRevertReconcilation(\"" + invoiceId + "\")'><img src='../../Content/images/icon/oemrevert.png'  alt='Revert'  title='Revert'  style='border:none;'/> </a></div>";
        }

        function OEMRevertReconcilation(rowid) {
            var result = confirm("Are you confirmed to revert the reconciliation for the OEM?");
            if (result == false)
                return;
            var oemId = $("select#EntityId").val();
            var invoiceId = rowid;

            var rootPath = window.location.protocol + "//" + window.location.host;
            $.ajax({
                url: rootPath + '/Reconciliation/RevertReconcilationByInvoiceId/',
                dataType: 'json',
                type: "POST",
                data: ({ invoiceId: invoiceId}),
                async: false,
                success: function (result) {
                    if (result) {
                        LoadReconciliationData();
                    }
                    else {
                        
                    }
                }
            });

        }