﻿var mydata = [
	{ ProcessingDate: "02/12/2010", Client: 'Michael Fallen', RecievedBy: "Craig Millan", Invoice: "2501", Shipment: "6058", Action: "<b><u>Update</u></b>", View: 'View Report' },
	{ ProcessingDate: "02/12/2010", Client: 'Michael Fallen', RecievedBy: "Craig Millan", Invoice: "2501", Shipment: "6058", Action: "<b><u>Update</u></b>", View: 'View Report' }
];
        
var isAdmin = $("#hfIsAdmin").val();

var isOem = $("#hfIsOem").val();


$(document).ready(function () {


    $("#states").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 120, width: 125 });
    $("button, input:submit, input:button").button();

    var myDate = new Date();
    var month = myDate.getMonth();
    var day = myDate.getDay();
    var year = myDate.getFullYear() - 1;
    var myDate = month + "/" + day + "/" + year;

    $("#txtBeginDate").datepicker();
    $('#txtBeginDate').datepicker('setDate', new Date());
    $("#txtEngingDate").datepicker();
    $('#txtEngingDate').datepicker('setDate', new Date());

    jQuery("#jqGrid").jqGrid({
        url: "GetProcessingDataList",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Processing Date', 'Client', 'Received By', 'State', 'Invoice #', 'Shipment', 'Processing Report', 'Invoice Report', ' ', 'Verify ', ' ', ' ', 'Approved','Verified'],
        colModel: [
   		    { name: 'ProcessingDate', index: 'ProcessingDate', width: 120, align: 'Left', sortable: false },
   		    { name: 'Client', index: 'Client', width: 105, sortable: false },
   		    { name: 'RecievedBy', index: 'RecievedBy', width: 115, sortable: false },
   		    { name: 'State', index: 'State', width: 105, sortable: false },
   		    { name: 'InvoiceID', index: 'InvoiceID', width: 85, align: 'Left', sortable: false },
   		    { name: 'ShipmentNo', index: 'ShipmentNo', width: 105, align: 'Left', sortable: false },
   		    { name: 'ProcessingData', index: 'ProcessingDataID', width: 135, editable: false, sortable: false, align: 'center', formatter: reportProcessingFmatter },
   		    { name: 'ProcessingDataID', index: 'ProcessingDataID', width: 110, editable: false, sortable: false, align: 'center', formatter: reportInvoiceFmatter },
   		    { name: 'EditActionMenu', index: 'EditActionMenu', width: 70, editable: false, sortable: false, align: 'center', formatter: editFmatter },
            { name: 'VerifiedCheck', index: 'VerifiedCheck', width: 30, editable: false, sortable: false, align: 'center', formatter: VerifiedCheckBoxFormatter },        
            
   		    { name: 'DownloadFile', index: 'ProcessingDataID', width: 30, editable: false, sortable: false, align: 'center' },
   		    { name: 'AttachFile', index: 'ProcessingDataID', width: 30, editable: false, sortable: false, align: 'center' },
            //{ name: 'ProcessingDataID', index: 'ProcessingDataID', width: 30, editable: false, sortable: false, align: 'center', formatter: delFmatter },
            {name: 'Approved', index: 'Approved', width: 85, align: 'Left', sortable: false, hidden: true },
            { name: 'Verified', index: 'Verified', width: 85, align: 'Left', sortable: false, hidden: true }
                    
   	    ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        //  editurl: rootPath + 'Rack/PerformAction',
        width: 910,
        height: 240

    });


    $("#jqGrid").jqGrid('navGrid', '#pager',
        { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    if (isOem == 'True') {
        jQuery("#jqGrid").jqGrid('hideCol', ["ProcessingDataID"]);
    }

    //if (isAdmin == 'False') {
    //    jQuery("#jqGrid").jqGrid('hideCol', ["AttachFile"]);
    //}


    ///jQuery("#jqGrid").jqGrid('hideCol',["AttachFile"]);

    $(".ui-paging-info", '#pager_left').remove();


    $("#btnSearch").click(function () {
        Search();

    });

    $("#txtSearch").keypress(function (event) {
        if (event.keyCode == '13') {
            Search();

        }

    });

    $('input[id$=btnAddPd]').click(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/ProcessingData/Index';
        window.location.href = url;
    });

    if (isOem == 'True' || isAdmin == 'True') {
        $("#btnAddPd").hide();
    }
    else {
        $("#btnAddPd").show();
    }
            
});

function Search() {
    var searchVal = $("#txtSearch").val();
    var state = '';
    state = $("#states option:selected").text();
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEngingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);
    if (beginingDate > endingDate) {

        $("#dialog").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "OK": function() {
                    $(this).dialog("close");
                }
            }
        });
        return false;
    }
    jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal, State: state, BeginDate: strBeginingDate, EndDate: strEndingDate }, page: 1 }).trigger("reloadGrid");

}
function editFmatter(cellvalue, options, rowObject) {
    var ProcessingDataId = rowObject[6];
    var url = '/ProcessingData/edit?Id=' + ProcessingDataId;

    if (ProcessingDataId == undefined)
        return cellvalue;
    if (rowObject[12] == true)
        return "Approved";
    else if (rowObject[13] == true)
        return "Verified";
    else
        return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='" + url + "' >Edit</a>";
}

function delFmatter(cellvalue, options, rowObject) {

    var id = cellvalue;
    var url = "<img src='../Content/images/Delete.png' title='Delete' onclick=\"DeleteProcessingData(" + id + ")\"></img>"
    return url;
}

function DeleteProcessingData(Id) {
    var answer = confirm("Delete this processing data?");
    if (answer) {
        $.ajax({
            url: 'Delete',
            dataType: 'json',
            type: "POST",
            data: ({ ProcessId: Id }),
            async: false,
            success: function (result) {
                if (result.showResult) {
                    Search();
                    //$("#jqGrid").trigger("reloadGrid");
                }
                else {
                    alert(result.showMsg);
                }
            }
        });
    }
}

function reportProcessingFmatter(cellvalue, options, rowObject) {

    var InvoiceNumber = rowObject[4];
    // var url = "http://dcimqavm/ReportServer_SQL2008/Pages/ReportViewer.aspx?%2fMits%2fProcessingReport1&rs:Command=Render&InvoiceID=" + InvoiceNumber;

    url = "return showReport('../reports/ProcessingReport.aspx?InvoiceID=" + InvoiceNumber + "', 'ProcessingReport');"

    // return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick= \"" + url + "\" href='#' >Processing<br /> Report</a>";
    return "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Processing Report'  title='Processing Report'  style='border:none;'/></a> </div>";
    //return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#' >Processing<br /> Report</a>";

}
function reportInvoiceFmatter(cellvalue, options, rowObject) {

    var InvoiceNumber = rowObject[4];
    var url = "return showReport('../reports/RecyclerInvoice.aspx?InvoiceID=" + InvoiceNumber + "', 'InvoiceReport');";
    if (rowObject[12] == true)
        return "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
    else
        return "&nbsp;"
}
        
function openFiles(pDataId) {
    //window.location.href =url;
    alert(docsHtml);
    $("#dialogFiles").html("<ul>"+docsHtml+"</ul>");                    
    $("#dialogFiles").dialog({
        modal: false,
        width: 400,
        resizable: false,
        position: 'center'
        }
    );
        
}
        
function openFileDialog(dataProcessReportId) {
    var w = window.open("../../Upload.aspx?processDataId="+dataProcessReportId +"&attachfile=true" , "Window1", "height=300,width=450,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");
}
var lastProcessDataId = null;
function GetAttachments(processDataId) {
    lastProcessDataId = processDataId;
        var rootPath = window.location.protocol + "//" + window.location.host;
        var docsHtml="<ul>"

        $.ajax({
            url: rootPath + '/ProcessingData/GetAttachments',
            dataType: 'json',
            type: "POST",
            data: ({ processDataId: processDataId }),
            async: false,
            success: function (result) {
                docsHtml += "<table width='100%'>";
                $.each(result.rows, function (index, entity) {
                    docsHtml += "<tr><td width='85%'><a href=javascript:Download('" + entity.UploadFileName + "'); style='color:#FF9900;text-decoration:none;_text-decoration:none;'>" + entity.UploadFileName + "</a></td>";
                    docsHtml += "<td width='15%'><img src='../../Content/images/icon/delete.png' alt='Remove attachment' style='cursor:hand;' title='Remove attachment' id='btnDel' border='0' onclick='javascript:DeleteAttachment(" + entity.DocumentID + ");' /></td></tr>";
                    //                         docsHtml += "<li><a href='/uploads/" + entity.UploadFileName + "'>" + entity.UploadFileName + "</a></li>";
                });

                docsHtml += "</table>"

            }
        });

        $("#dialogFiles").html(docsHtml);
            
        $("#dialogFiles").dialog({
            modal: false,
            width: 400,
            resizable: false,
            position: 'center'
            }
        );
                
}
function Download(fileName) {
    fileName = fileName.replace('#', '<~>');
    if (fileName != "-" && fileName != "") {
        window.location = "/Affiliate/Download?FileName=" + fileName;
        //$.ajax({
        //    url: '/Affiliate/IsFileExist?FileName=' + fileName,
        //    dataType: 'json',
        //    type: 'POST',
        //    //data: ({}),
        //    async: false,
        //    success: function (isFileExist) {
        //        if (isFileExist) {
        //            window.location = "/Affiliate/Download?FileName=" + fileName;
        //        }
        //        else{
        //            alert('This file is not exist.'); //data.toString()
        //        }
        //    }
        //});
    }

}
function DeleteAttachment(docID) {
    var ans = confirm('Are you sure?');
    if (ans) {
        $.ajax({
            url: '/Affiliate/DeleteAttachment/',
            dataType: 'json',
            type: "POST",
            data: { docID: docID },
            async: false,
            success: function (result) {
                GetAttachments(lastProcessDataId);
                $("#jqGrid").trigger("reloadGrid");

            }
        });
    }
}
function SaveAttachment(fileName, processDataId) {
            var rootPath = window.location.protocol + "//" + window.location.host;

            $.ajax({
                url: rootPath + '/ProcessingData/SaveAttachment',
                dataType: 'text',
                type: "POST",
                data: ({ fileName: fileName, processDataId: processDataId }),
                async: false,
                success: function (result) {
                    $("#jqGrid").trigger("reloadGrid");
                }
            });
}

function VerifiedCheckBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var invoiceid = cellvalue;
    if (rowObject[12] == 1)
        return "<input type='checkbox' Name='Approved' id='verifiedCheck" + rowid + "' onclick=\"UpdateVerify(" + rowid + "," + invoiceid + ")\"  checked DISABLED />";
    else if (rowObject[13] == 1)
        return "<input type='checkbox' Name='Approved' id='verifiedCheck" + rowid + "' onclick=\"UpdateVerify(" + rowid + "," + invoiceid + ")\"  checked />";
    else
        return "<input type='checkbox' Name='Approved' id='verifiedCheck" + rowid + "' onclick=\"UpdateVerify(" + rowid + "," + invoiceid + ")\"  />";
}


function UpdateVerify(rowId, invoiceid) {
    var chkbox = document.getElementById("verifiedCheck" + rowId);
    $.ajax({
        url: '/ProcessingData/UpdateVerify',
        dataType: 'json',
        type: "POST",
        data: "id=" + invoiceid,
        async: false,
        success: function (result) {
            if (result.success) {
                alert("Success:Processing Report Updated");
                reformatEditAction(rowId, invoiceid, result.verified, result.pdId);

            }
            else {
                alert("Update Failed:" + result.message);
                chkbox.checked = !(chkbox.checked);
            }
        }

    });
}

function reformatEditAction(rowId, invoiceId, verified, pdId) {
    var isVerified = verified;
    var InvoiceNumber = invoiceId;
    var ProcessingDataId = pdId

    var formattedAction;
    if (isVerified == true)
        formattedAction = "Verified";
    else {
        var url = '/ProcessingData/edit?Id=' + ProcessingDataId;
        formattedAction = "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='" + url + "' >Edit</a>";
    }
    $("#jqGrid").jqGrid("setCell", rowId, 'EditActionMenu', formattedAction);

}