﻿
$(document).ready(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;

   
    //'ServiceTypeGroup dropdown onchange function' For getting details of name and desc against a particular group

    $('select[id$=ddlServiceTypeGroup]').change(function (elem) {

        $('input[id$=txtName]').css("background-color", "white");
        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");

        serviceTypeGroup = $("select#ddlServiceTypeGroup").val();
        $.ajax({
            url: rootPath + '/ServiceTypeGroup/GetDetail',
            dataType: 'json',
            type: "POST",
            data: ({ servicetypegroup: serviceTypeGroup }),
            async: false,
            success: function (result) {
                $("input#txtName").val(result.Name);
                $("textarea#txtDesc").val(result.Desc);
            }
        });
    });


    //'Clear button onclikc function' For clearing necessary fields

    $('input[id$=btnClear]').click(function () {

        $("#txtName").val("");
        $("#txtDesc").val("");
        $("#ddlServiceTypeGroup").val(0);
        $("#lblMessage").text("");
        $('input[id$=txtName]').css("background-color", "white");
        $("ul#message").css("visibility", "hidden");

    });


    //'Submit button onclick function' For submitting data to the controller action 

    $('input[id$=btnSubmit]').click(function () {



        if ($('input[id$=txtName]').val() == "") {

            $('input[id$=txtName]').css("background-color", "#FFEEEE");
            $("label#lblMessage").text("Name is required").css("color", "Red");
            $("ul#message").css("visibility", "visible");

        }
        else {
            $('input[id$=txtName]').css("background-color", "white");
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");

            var serviceTypeGroup = $("select#ddlServiceTypeGroup").val();
            var name = $("input#txtName").val();
            var desc = $("textarea#txtDesc").val();

            $.ajax({
                url: rootPath + '/ServiceTypeGroup/Save',
                dataType: 'json',
                type: "POST",
                data: ({ servicetypegroup: serviceTypeGroup, name: name, desc: desc }),
                async: false,
                success: function (result) {
                    if (result.AccessDenied == "AccessDenied") {
                        var rootPath = window.location.protocol + "//" + window.location.host;
                        var url = rootPath + '/Home/AccessDenied';
                        window.location.href = url;
                    }
                    else {
                        $("label#lblMessage").text(result.message);
                        $("ul#message").css("visibility", "visible");
                        if (result.data) {
                            $("select#ddlServiceTypeGroup").fillSelect(result.data);
                            $("select#ddlServiceTypeGroup").val(result.selected);
                        }
                    }
                }
            });
        }



    });

});


//Text area max length

function imposeMaxLength(Object, MaxLen) {
        return (Object.value.length <= MaxLen);
    }


