﻿var lastSel;
var inEditMode = false;
var _Checked;

$(document).ready(
function () {


    $("#jqGrid").jqGrid({
        url: '/DropOffLocations/GetDropOffLocations',
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Name', 'Address', 'Phone', 'Fax', 'Website', 'Longitude', 'Latitude', 'State', 'Zip','Email',''],
        colModel: [
                  { name: 'Name', index: 'Name', width: 125, editable: true, sortable: true, editrules: { required: true },  editoptions: { size: 30, maxlength: 125} },
                  { name: 'Address', index: 'Address', width: 100, editable: true, sortable: false, editoptions: { maxlength: 50} },
                  { name: 'Phone', index: 'Phone', sortable: false, editable: true, editoptions: { maxlength: 50} },
                  { name: 'Fax', index: 'Fax', sortable: false, editable: true, editoptions: { maxlength: 50} },
                  { name: 'Website', index: 'Website', sortable: false, editable: true, editoptions: { maxlength: 50} },
                  { name: 'Longitude', index: 'Longitude', sortable: false, editable: true, editoptions: { maxlength: 50} },
                  { name: 'Latitude', index: 'Latitude', sortable: false, editable: true, editoptions: { maxlength: 50} },
                  { name: 'State', index: 'State', sortable: false, editable: true, editoptions: { maxlength: 50} },
                  { name: 'Zip', index: 'Zip', sortable: false, editable: true },
                  { name: 'Email', index: 'Email', sortable: false, editable: true,hidden: true },
                  { name: 'Actions', index: 'Actions', width: 100, editable: false, sortable: false, align: 'center', formatter: ActionsFormatter }
      ],
        rowNum: 50,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 350,
        editurl: "/DropOffLocations/PerformActions",
        gridComplete: function () { LoadDropdownsData(); }
    });

    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });



    $('input[id$=btnAdd]').click(function () {

        var url = '/DropOffLocations/Create/';
        window.location.href = url;


    });

    $("#btnSearch").click(function () {

        var searchStr = $("#txtName").val();
        jQuery("#jqGrid").setGridParam({ postData: { searchString: searchStr }, page: 1 }).trigger("reloadGrid");

    });

}
);



function LoadDropdownsData(elem) {
    $.ajax({
        url: '/PlanYear/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('State', { editoptions: { value: result.states} });
        }
    });

}



function ActionsFormatter(cellvalue, options, rowdata) {
    var rowId = options.rowId;
    var actionsHtml = "<div id='ED_Actions_" + rowId + "'>";
    actionsHtml += "<img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:editRow(\"" + rowId + "\")' style='border:none;' class='edit'/>";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete' title='Delete' onclick='javascript:deleteRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    actionsHtml += "<div id='SC_Actions_" + rowId + "' style='display:none;'>";
    actionsHtml += "<img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save' title='Save' onclick='javascript:saveRow(\"" + rowId + "\")' style='border:none;' />";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/cancel.png' style='cursor:hand;' alt='Cancel' title='Cancel' onclick='javascript:cancelRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    return actionsHtml;
}


function editRow1(rowId) {
    if (rowId && rowId !== lastSel) {
        cancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_Actions_' + rowId).css({ display: "none" });
    $('#SC_Actions_' + rowId).css({ display: "block" });
    //$('#jqGrid').editRow(rowId);
    $("#jqGrid").jqGrid('editGridRow', "new", { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 300, width: 400 });
    inEditMode = true;
}

function editRow(rowId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/DropOffLocations/Edit/' + rowId;
    window.open(url);
}

function cancelRow(rowId) {
    $('#ED_Actions_' + rowId).css({ display: "block" });
    $('#SC_Actions_' + rowId).css({ display: "none" });

    $.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    if (rowId == "0") {
        $("#jqGrid").delRowData(rowId);
    }
    inEditMode = false;
}

function deleteRow(rowId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/DropOffLocations/Delete/' + rowId
    $("#jqGrid").delGridRow(rowId, { reloadAfterSubmit: false, url: url });
}

function saveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(result) {
    $('#ED_Action_' + lastSel).css({ display: "block" });
    $('#SC_Action_' + lastSel).css({ display: "none" });
    $('#jqGrid').trigger("reloadGrid");
    inEditMode = false;
    return false;
}

function saveCallback(rowId, result) {
    if (result.responseText != "failture" && parseInt(result.responseText).toString() != 'NaN') {
        if (rowId == "0") {
            var rowData = $("#jqGrid").getRowData(rowId);
            $("#jqGrid").delRowData(rowId);
            $("#jqGrid").addRowData(result.responseText, rowData);
        }
    }
}

function Check(value, colName) {
    _Checked = true;
    if (colName == "State") {
        if (value == null || value == "") {
            _Checked = false;
            return [false, "State: Field is required"];
        }
    }
    return [true, ""];
}