﻿var manufacturerList = '';
var typeList = '';

var zip = '';
var city = '';
var state = '';
var latitude = '';
var longitude = '';

$(function () {


    $("button, input:submit, input:button, a#submit").button();

    $('select[id$=lstDevices]').multiSelect(
            {
                selectAll: false,
                noneSelected: 'Select Devices!',
                oneOrMoreSelected: '% Device(s) Selected',
                optGroupSelectable: true,
                customHTML: function (option) { return '<img src="../../images/Calendar.gif " />&nbsp;' + option.text; }
            });


    $.ajax({
        url: '../../MailBack/GetManufacturerList',
        dataType: 'text',
        async: false,
        success: function (data) {
            manufacturerList = data;
        }
    });




    var lastsel;
    var productTypeId;

    //populateDevices();

    initDeviceGrid();

    var rowID = 0;

    $("#btnAdd").click(function () {


        var isFormValid = ValidateForm();
        if (isFormValid) {

            $("#validationSummary").html("");
            fetchDevicesByLocation();
            //jQuery("#jqGrid").editGridRow("new", { closeAfterAdd: true, closeOnEscape: true, addedrow: 'last' });
        }

    });

    $("#btnDelete").click(function () {

        //get selected row and delete it otherwise prompt message to select a row

        var ids = $("#jqGrid").getDataIDs();
        var id = $("#jqGrid").getGridParam("selrow");

        if (id != '' && id != null) {
            jQuery('#jqGrid').jqGrid('delGridRow', id);

            if (count == 0) {
                $("#imgType").attr('src', '../../images/layout/mitslogo_sm.gif');
            }
        }
        else {
            alert('Please select a row first');
        }
        //var savedData = jQuery("#GridPowerCable").getRowData();
        //   $("#GridPowerCable").jqGrid('clearGridData');
        //  $("#GridPowerCable").jqGrid('addRowData', 'rn', savedData);


    });


    initResultGrid();
});

function getDevices() {

    $.ajax({
        url: '../../MailBack/GetDevicesList',
        dataType: 'text',
        type: "POST",
        data: ({ city: '', state: state, zip: zip }),
        async: false,
        success: function (data) {
            typeList = data;

            $('#jqGrid').setColProp('Type', { editoptions: { defaultValue: 'Please Select', value: typeList} });
        }
    });

}
function getImagePath(rowID) {
    var rowData = jQuery("#jqGrid").getRowData(rowID);
    var data = { ProductTypeId: rowData.Type };
    $.ajax({
        url: '../../MailBack/GetDeviceImage',
        dataType: 'text',
        data: data,
        async: false,
        success: function (result) {

            src = result.toString();
            $("#imgType").attr('src', src)
        }
    });

}

function SetMfg() {
    var opt = $("select[id$=ddlManufacturer] option:selected");
    $("input[id$=hidProductMfgrID]").val(opt.val());
    $("input[id$=txtProductMfgr]").val(opt.text());
}

function fetchDevicesByLocation() {

    zip = $("input[id$=txtZipCode]").val();
    geocoder = new google.maps.Geocoder();

    geocoder.geocode({ 'address': ', ' + zip }, function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            var geoResults = results[0];

            city = getAddressPart(geoResults.address_components, 'city');
            state = getAddressPart(geoResults.address_components, 'state');


            point = geoResults.geometry.location;

            latitude = point.lat();
            longitude = point.lng();

            $("#hfAddressInfo").val(latitude + "+" + longitude + "+" + city + "+" + state);

            getDevices();

            jQuery("#jqGrid").editGridRow("new", { closeAfterAdd: true, closeOnEscape: true, addedrow: 'last' });

        }
    });

}

function getNearestLocations() {

    var columnData = $("#jqGrid").getCol("Type");
    if (columnData.length == 0) {
        alert("Please Select atleast one Device Type.");
        return;

    }


    var mfgrId = $('select[id$=ddlManufacturer] option:selected').text();
    var productTypes = $("#jqGrid").getCol("Type");


    $.ajax({
        url: 'GetLocations',
        dataType: 'json',
        type: "POST",
        data: ({ lat: latitude, lgt: longitude, city: city, state: state, prods: productTypes, mfgr: mfgrId }),
        async: false,
        success: function (data) {
            initResultGrid();
            for (var i = 0; i <= data.length; i++) {
                $("#tblResults").jqGrid('addRowData', i + 1, data[i]);
            }
            
            showResultsDialog();
        }
    });


}

function populateDevices() {
    var zipCode = $("input[id$=txtZipCode]").val();
    if (zipCode.length == 5) {
        fetchDevicesByLocation();

    }
}

function initDeviceGrid() {

    // populateDevices(); // load devices based on location for Type cloumn pf foloowing grid

    jQuery("#jqGrid").jqGrid({
        url: jQuery.rootPath + '/mailback/GetSelectedDevices',
        datatype: 'json',
        colNames: ['Manufacturer', 'Type'],
        colModel: [
   		            { name: 'Manufacturer', index: 'Manufacturer', width: 125, sortable: false, formatter: 'select', editable: true, edittype: 'select', editoptions: { defaultValue: 'Please Select', value: manufacturerList} },
   		            { name: 'Type', index: 'Type', width: 125, editable: true, edittype: 'select', sortable: false, formatter: 'select', editrules: { required: true }, editoptions: { defaultValue: 'Please Select', value: typeList} },

  	            ],
        width: 550,
        height: 230,
        rowNum: 0,
        rownumbers: true,
        editurl: jQuery.rootPath + '/mailback/save',
        pgbuttons: false,
        onSelectRow: function (id) {
            getImagePath(id);
        },
        gridComplete: function () { populateDevices(); }
    });
}

function showMapLocations(lng, lat) {
    $("#dlgDropOff").dialog('close');

    var form = document.forms[0];
    if (form) {
        form.action = 'LocationMap?lng=' + lng + '&lat=' + lat;
        form.submit();
    }
}

function populateAddressInfo() {
    zip = $("input[id$=txtZipCode]").val();
    geocoder = new google.maps.Geocoder();

    geocoder.geocode({ 'address': ', ' + zip }, function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            var geoResults = results[0];

            city = getAddressPart(geoResults.address_components, 'city');
            state = getAddressPart(geoResults.address_components, 'state');


            point = geoResults.geometry.location;

            latitude = point.lat();
            longitude = point.lng();

            $("#hfAddressInfo").val(latitude + "+" + longitude + "+" + city + "+" + state);



        }
    });

}


function showResultsDialog() {

    $("#dlgDropOff").dialog({
        width: '700px'
            , height: '600'
            , moveable: true
            , draggable: false
            , close: function () {
                $("#tblResults").GridUnload();

            }
    });

    $("#dlgDropOff").dialog('open');
}

function initResultGrid() {
    $("#tblResults").jqGrid(
                    {
                        url: 'Default1.aspx', //Dummy URL
                        datatype: "local",
                        mtype: 'GET',
                        colNames: ['Location', 'Distance', 'View on Map', 'Long', 'Lat'],
                        colModel: [
                                    { name: 'Location', index: 'Location', width: 450, sortable: false, title: false },
                                    { name: 'Distance', index: 'Distance', width: 100, sortable: false, title: false },
                                    { name: 'ViewOnMap', index: 'ViewOnMap', width: 130, sortable: false, title: false },
                                    { name: 'Longitude', index: 'Longitude', width: 0, sortable: false, hidden: true, title: false },
                                    { name: 'Latitude', index: 'Latitude', width: 0, sortable: false, hidden: true, title: false }
                        ],
                        jsonReader: {
                            repeatitems: false
                        },
                        rowNum: 10,
                        recordpos: 'left',
                        viewrecords: true,
                        width: 650,
                        height: 450,
                        hoverrows: false
                    });
                }

                function MailBack() {
                    var form = document.forms[0];
                    if (form)
                        form.action = '../../MailBack/Index';
                    form.submit();
                }