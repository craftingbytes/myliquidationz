﻿var rootPath = window.location.protocol + "//" + window.location.host;

$(document).ready(function () {

    $("#txtDate").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });

    $('#ddlNewsFeed').change(function () {
        getNewsFeed();
    });

    //'Clear button onclick function' for clearing all necessary fields
    $('input[id$=btnClear]').click(function () {
        $("#txtNews").val('');
        $("#txtFeedLink").val('');
        $("#txtDate").val('');
        $("#validationSummary").html("");
    });

    //'Submit button onclick function' for submitting necessary data to the controller action

    $('input[id$=btnSubmit]').click(function () {
        saveNewsFeed();
    });

    getNewsFeed();
});

function getNewsFeed() {
    var feedId = $("#ddlNewsFeed").val();
    
    $('#btnClear').click();

    $.ajax({
        url: rootPath + '/NewsFeed/GetNewsFeed',
        dataType: 'json',
        type: "POST",
        data: ({ Id: feedId }),
        async: false,
        success: function (result) {
            if (result.message == "Server error occured.") {
                var url = rootPath + '/Home/Error';
                window.location.href = url;
            } else {
                $("#txtNews").val(result.News);
                $("#txtFeedLink").val(result.FeedLink);
                $("#txtDate").val(result.Date);
            }
        }
    });
}

function saveNewsFeed() {
    var date = $("#txtDate")[0];
    if ($.trim(date.value) != '') {
        var reg = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$";
        if (date.value.search(reg) == -1) {
            date.style.backgroundColor = '#FFEEEE';            
            $("#validationSummary").html("<ul class=ValidationSummary><li>Invalid Date format.</li></ul>");
            return false;
        }
    }

    $('#txtDate').css("background-color", "white");
    $("#validationSummary").html("");

    var id = $("#ddlNewsFeed").val();
    var news = $("#txtNews").val();
    var feedLink = $("#txtFeedLink").val();
    var date = $("#txtDate").val();

    $.ajax({
        url: rootPath + '/NewsFeed/SaveNewsFeed',
        dataType: 'json',
        type: "POST",
        data: ({ Id: id, News: news, FeedLink: feedLink, Date: date }),
        async: false,
        success: function (result) {
            if (result.AccessDenied == "AccessDenied") {
                var url = rootPath + '/Home/AccessDenied';
                window.location.href = url;
            }
            else {
                if (result.message == "Server error occured.") {
                    var url = rootPath + '/Home/Error';
                    window.location.href = url;
                } else {
                    $("#validationSummary").html("<ul class=ValidationSummary><li>" + result.message + "</li></ul>");
                }
            }
        }
    });
}

//Text area max length 
function imposeMaxLength(obj, maxLen) {
    return (obj.value.length <= maxLen);
}

function limitChars(id, count) {
    var obj = document.getElementById(id);
    if (obj.value.length > count) {
        obj.value = obj.value.substr(0, count);
    }
}  