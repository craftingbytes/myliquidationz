﻿
$(document).ready(function () {


    var rootPath = window.location.protocol + "//" + window.location.host;

    //'State dropdown onchange function' Invokes OnChange' function

    $('select[id$=ddlState]').change(function (elem) {



        state = $("select#ddlState").val();
        oem = $("select#ddlOEM").val();

        if (state == 0) {

            $("input[name=selectedObjects]").attr('checked', false);
            $("label#lblMessage").text("");
            $("select#ddlOEM").val(0);
            $("ul#message").css("visibility", "hidden");
            return false;
        }
        OnChange();


    });

    //'OEM dropdown onchange function' Invokes OnChange' function

    $('select[id$=ddlOEM]').change(function (elem) {

        if ($('select[id$=ddlOEM]').val() == 0) {
            $("input[name=selectedObjects]").attr('checked', false);
            $("label#lblMessage").text("");
            $("select#ddlState").val(0);
            $("ul#message").css("visibility", "hidden");
            return false;

        }
        OnChange();

    });

    //'Clear button onclick function' For clearing necessary fields

    $('input[id$=btnClear]').click(function () {
        $("select#ddlState").val(0);
        $("select#ddlOEM").val(0);
        $("input[name=selectedObjects]").attr('checked', false);
        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");
    });

    //'Submit button onclick function' For submitting data to controller action

    $('input[id$=btnSubmit]').click(function () {



        state = $("select#ddlState").val();
        oem = $("select#ddlOEM").val();
        if (oem == "0") {
            $("label#lblMessage").text("No OEM Selected").css("color", "Red");
            $("ul#message").css("visibility", "visible");
            return false;

        }
        else if (state == "0") {
            $("label#lblMessage").text("No State Selected").css("color", "Red");
            $("ul#message").css("visibility", "visible");
            return false;
        }

        else if ($("input:checkbox:checked").length == 0) {
            $("label#lblMessage").text("No Policy Selected").css("color", "Red");
            $("ul#message").css("visibility", "visible");
            return false;


        }

        var policies = "";
        $("input:checkbox:checked").each(function () {
            if (policies == "")
                policies = $(this).val();
            else
                policies = policies + ',' + $(this).val();


        });


        $.ajax({
            url: rootPath + '/OEMStatePolicy/Save',
            dataType: 'json',
            type: "POST",
            data: ({ oem: oem, state: state, policies: policies }),
            async: false,
            success: function (result) {
                if (result.AccessDenied == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else {
                    $("label#lblMessage").text(result.message);
                    $("ul#message").css("visibility", "visible");
                    if (result.data) {


                        $("select#ddlOEM").fillSelect(result.data);
                        $("select#ddlOEM").val(result.selected);

                        $("input#selectedObjects").attr('checked', true);

                    }
                }
            }
        });


    });


    //For getting policies against a particular OEM and state

    function OnChange() {

        state = $("select#ddlState").val();
        oem = $("select#ddlOEM").val();

        $("input[name=selectedObjects]").attr('checked', false);
        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");





        $.ajax({
            url: rootPath + '/OEMStatePolicy/GetOEMPolicies',
            dataType: 'json',
            type: "POST",
            data: ({ state: state, oem: oem }),
            async: false,
            success: function (result) {
                if (result.data) {


                    $.each(result.data, function (index, item) {


                        $("input[id=" + item.Value + "]").attr('checked', true);


                    });
                }

            }
        });


    }


});

