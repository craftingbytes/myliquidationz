﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;


    $("button, input:submit, input:button").button();

    jQuery("#jqGrid").jqGrid({
        url: "/AffiliateContact/GetAffiliateContacts",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id', 'First Name', 'Last Name', 'Address', 'State', 'Phone', 'Role', 'Actions'],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
   		{ name: 'First Name', index: 'First Name', width: 85, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
        { name: 'Last Name', index: 'Last Name', width: 85, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Address', index: 'Address', width: 170, editable: false, sortable: false },
   		{ name: 'State', index: 'State', width: 130, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Phone', index: 'Phone', width: 130, editable: false, align: 'left',sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125 } },
        { name: 'Role', index: 'Role', width: 85, hidden: true },
        //{ name: 'Select', index: 'Select', width: 40, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125} },
        {name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: myformatter, title: true, align: 'center', width: 35, editable: false },
        //{ name: 'AttachmentPath', index: 'AttachmentPath', sortable: false, formatter: myformatter, title: true, align: 'center', width: 25, editable: false },
        //{ name: 'Action', index: 'Action', width: 40, editable: false, sortable: false, align: 'center', formatter: 'actions', formatoptions: { keys: false, editbutton: true, delbutton: true} }
        //

   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 925,
        height: 70,
        editurl: "PerformAction"


    });
    //m.Id,
    //m.AffiliateContact.UserName,
    //m.AffiliateContact1.Affiliate.Name,
    //m.AffiliateContact1.UserName,
    //m.DefaultUser
    jQuery("#jqgridContactAssociate").jqGrid({
        url: "/AffiliateContact/GetContactMasters",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id', 'UserName', 'Associated Affiliate', 'Associated Username', 'Default','Actions'],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
   		{ name: 'UserName', index: 'UserName', width: 75, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100 } },
        { name: 'Affiliate', index: 'Affiliate', width: 100, editable: false, sortable: false, editrules: { required: false }, editoptions: { size: 30, maxlength: 100 } },
   		{ name: 'AsscUsername', index: 'AsscUsername', width: 100, editable: true, sortable: false },
        { name: 'DefaultUser', index: 'DefaultUser', width: 100, editable: false, sortable: false, formatter: DefaultCheckBoxFormatter },
        {name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: acformatter, title: true, align: 'center', width: 35, editable: false }
        ],
        rowNum: 1000,
        //rowList: [10, 25, 50, 100],
        //pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 600,
        height: 200,
        editurl: "PerformAction"


    });

    $('select[id$=AffiliateSelect]').change(function (elem) {
        GetAssociatedContactsByAffiliateId();
    });


});


    $('select[id$=StateId]').change(function (elem) {
        var rootPath = window.location.protocol + "//" + window.location.host;  
        state = $("select#StateId").val();
        if (state == "0") {
            $("select#CityId").clearSelect();
        }
        else {
            var rootPath = window.location.protocol + "//" + window.location.host; 
            $.ajax({
                url: rootPath + '/Affiliate/GetCities/',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#CityId").fillSelect(result.data);
                    }
                }
            });
        }

    });

    $("select[id=SecurityQuestionId]").change(function () {
        if ($("select#SecurityQuestionId").val() == 0) {
            $("#Answer").val("");
        }
    });

    $("#Password").focus(function (event) {
        $("#Password").select();
    })

    $("#ConfirmPassword").focus(function (event) {
        $("#ConfirmPassword").select();
    })

    $('input[id$=btnClear]').click(function () {

        $(this.form).find(':input').each(function () {
            switch (this.type) {
               
                case 'select-multiple':
                case 'select-one':
                    $(this).val(0);
                    break;
                case 'password':
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                    if (this.name != 'Active')
                        this.checked = false;
                    break;
            }
        });
        var state = $("select#StateId").val();
        if (state == "0") {
            $("select#CityId").clearSelect();
        }
        $("#validationSummary").html("");
        //var affiliateId = <% = Session["AffiliateId"]; %>; 

//        //alert(affiliateId);
//        var rootPath = window.location.protocol + "//" + window.location.host;
//        var url = rootPath + '/AffiliateContact/Clear';
//        window.location.href = url;

    });

    $('input[id$=btnAdd]').click(function () {

        $(this.form).find(':input').each(function () {
            switch (this.type) {
               
                case 'select-multiple':
                case 'select-one':
                    $(this).val(0);
                    break;
                case 'password':
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                    if (this.name != 'Active')
                        this.checked = false;
                    break;
            }
        });
        var state = $("select#StateId").val();
        if (state == "0") {
            $("select#CityId").clearSelect();
        }
        $("#validationSummary").html("");
        //var affiliateId = <% = Session["AffiliateId"]; %>; 

        //alert(affiliateId);
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/AffiliateContact/Clear' + "?data=" + $('#data').val(); ;
        window.location.href = url;

    });


    function IsServiceTypeSelected() {
        var selected;
        $(":checkbox").each(function () {
            if (this.checked && this.name != "Active") {
                selected = this.value;
            }
        });
        if (selected) {
            return true;
        }
        return false;
    }

    $('input[id$=btnSubmit]').click(function () {

        for (var iForm = 0; iForm < document.forms.length; iForm++) {//look through all the forms
            return ValidateSingleFormEntry(iForm);
        }

    });

    function ValidateSingleFormEntry(form) {
        var oForm;
        if (!form) form = 0; //default to first form
        switch (typeof form) {
            case 'object':
                oForm = form;
                break;
            case 'number':
                oForm = document.forms[form];
                break;
            default:
                oForm = document.forms[0];
                break
        }
        var dynamicHTML = "";
        dynamicHTML += "<ul class=ValidationSummary>";

        var oRequired;
        var sRequired = '';
        var type;
        var bValidated = true;
        if (oForm) {
            for (var iControl = 0; iControl < oForm.length; iControl++) {
                oRequired = oForm[iControl];

                if (oRequired) {

                    sRequired = oRequired.getAttribute('required');
                    type = oRequired.getAttribute('type');
                    if (sRequired) {//is it required
                        try {
                            if (!oRequired.value || $.trim(oRequired.value) == '' || (oRequired.value == "0" && type!="text")) {//is it empty
                                bValidated = false;
                                //sAlert = sRequired.replace("~", '\r\n'); //set var so the carriage returns work
                                //alert(sAlert); //let the user know to fill it in
                                oRequired.style.backgroundColor = '#FFEEEE';
                                //var oFocus = oRequired.onfocus; //get the focus event if there is one
                                // if (oFocus) oRequired.onfocus = null; //focusing triggers the event and can cause an error
                                // oRequired.focus(); //highlight the control
                                //oRequired.onfocus = oFocus; //return it
                                dynamicHTML += "<li>" + sRequired + "</li>"
                                //break; //get out


                            } else {
                                oRequired.style.backgroundColor = '#FFFFFF';
                            }
                        } catch (err) {
                            bValidated = false;
                            //alert('Unable to validate the form.\r\n' + err.name + ":" + err.message);
                            //alert(oRequired.id)
                        }
                    }
                    var regularExpAtt = oRequired.getAttribute('regexp');
                    if (regularExpAtt) {
                        var reg = regularExpAtt;
                        if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                        {
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                        }
                    }
                }
            }
            //following if is logic related to current page, you should not use it
//            if (!IsServiceTypeSelected()) {
//                bValidated = false;
//                dynamicHTML += "<li>Please check at least one service type.</li>";
//            }
            if ($("input#Password").val().toLowerCase() != $("input#ConfirmPassword").val().toLowerCase()) {
                bValidated = false;
                dynamicHTML += "<li>Password and Confirm Password should be same.</li>";
            }
            if ($("input#Password").val().length < 6 || $("input#ConfirmPassword").val().length < 6) {
                bValidated = false;
                dynamicHTML += "<li>Password must be at least 6 characters long.</li>";
            }
            dynamicHTML += "</ul>";
            //alert(dynamicHTML);
            if (bValidated == false) {
                $("#validationSummary").html(dynamicHTML);
            }
            return bValidated;
        }  

    }

    function Edit(affiliateContactId) {        
        var rootPath = window.location.protocol + "//" + window.location.host;
        if (affiliateContactId != "-") {
           window.location.href = rootPath + '/AffiliateContact/Edit/' + affiliateContactId + "?data=" + $('#data').val();
        }

    }

    function myformatter(cellvalue, options, rowObject) {
        var returnstring = "";
        var roleTypeId = rowObject[6];
        returnstring += "<div align=left>&nbsp;&nbsp;<a href='javascript:Edit(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit' style='border:none;'/> </a>";
        if (roleTypeId == 12)
            returnstring += "<a href='javascript:openContactAssociate(" + rowObject[0] + ")'><img src='../../Content/images/icon/user.png'  alt='Edit'  title='Add/Edit Associated Users'  style='border:none;'/></a>"
        returnstring += "</div>";
        return returnstring;
    }

    function Cancel() {
        var url = '/Affiliate/Index/';
        window.location.href = url + "?data=" + $('#data').val();
    }

    function openContactAssociate(contactId) {
        $("#hdMasterContactId").val(contactId);
        loadAffiliates();
        jQuery("#jqgridContactAssociate").setGridParam({ postData: { contactId: contactId }, page: 1 }).trigger("reloadGrid");
        $("#dialog-ContactAssociate").dialog({
            modal: true,
            width: 625,
            resizable: false,
            position: { my: 'center', at: 'center', of: window },
            buttons: {
                "Close": function () {
                    window.location = '#';
                    $(this).dialog("close");
                }
            },
            beforeClose: function (event, ui) {
                //var BeginDate = $("#jqGrid2").getGridParam("postData").BeginDate;
                //var EndDate = $("#jqGrid2").getGridParam("postData").EndDate;
                //var BeginId = $("#jqGrid2").getGridParam("postData").BeginId;
                //var EndId = $("#jqGrid2").getGridParam("postData").EndId;
                //var ShipmentType = $("#jqGrid2").getGridParam("postData").ShipmentType;
                //var page = $("#jqGrid2").getGridParam("page");
                //var rows2 = $("#jqGrid2").getGridParam("rowNum");
                //jQuery("#jqGrid2").setGridParam({ postData: { BeginDate: BeginDate, EndDate: EndDate, BeginId: BeginId, EndId: EndId, ShipmentType: ShipmentType }, page: page, rowNum: rows2 }).trigger("reloadGrid");

                //var rows = $("#jqGrid").getGridParam("rowNum");
                //$("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");


            }
        });
    }

    function loadAffiliates() {
        var affiliateTypeId = 0;
        if ($("#hdAffiliateTypeId").val() > 0)
            affiliateTypeId = $("#hdAffiliateTypeId").val();
        $.ajax(
        {
            url: '/AffiliateContact/GetAffiliatesByType/',
            dataType: 'json',
            type: "POST",
            data: ({ affiliateTypeId: 1 }),
            async: false,
            success: function (result) {
                if (result.data) {
                    $("select#AffiliateSelect").fillSelect(result.data);
                }
            }
        });
    }

    function GetAssociatedContactsByAffiliateId() {
        var affiliateId = 0;
        if ($("#AffiliateSelect").val() > 0)
            affiliateId = $("#AffiliateSelect").val();

        $.ajax(
        {
            url: '/AffiliateContact/GetAssociatedContactsByAffiliateId/',
            dataType: 'json',
            type: "POST",
            data: ({ affiliateId: affiliateId }),
            async: false,
            success: function (result) {
                if (result.data) {
                    $("select#AssociatedContactSelect").fillSelect(result.data);
                }
            }
        });
    }

    $('input[id$=btnAddAssociate]').click(function () {

        if ($("#AffiliateSelect").val() > 0 && $("#AssociatedContactSelect").val() > 0) {
            var masterContactId = $("#hdMasterContactId").val();
            var affiliateId = $("#AffiliateSelect").val();
            var associateId = $("#AssociatedContactSelect").val();
            $.ajax({
                url: "/AffiliateContact/AddMasterContact",
                dataType: 'json',
                type: "POST",
                data: ({ masterContactId: masterContactId,  associateId: associateId }),
                async: false,
                cache: false,
                success: function (result) {
                    if (result.status == true) {
                        
                        $("#jqgridContactAssociate").setGridParam({ page: 1 }).trigger("reloadGrid");
                    }
                    else
                        alert(result.message);

                }
            });
        } else {

            alert("Please select an Affiliate then a User.");
        }

    });

    function DefaultCheckBoxFormatter(cellvalue, options, rowObject) {
        var rowid = options.rowId;
        if (cellvalue == 1)
            return "<input type='checkbox' Name='Approved' id='masterDefaultCheck" + rowid + "'  checked disabled='disabled' />";
        else
            return "<input type='checkbox' Name='Approved' id='masterDefaultCheck" + rowid + "' onclick=\"SetMasterDefaultUser(" + rowid + ")\"  />";
    }

    function SetMasterDefaultUser(id)
    {
        var masterContactId = $("#hdMasterContactId").val();
        
        var associateId = $("#AssociatedContactSelect").val();
        $.ajax({
            url: "/AffiliateContact/SetMasterContactDefault",
            dataType: 'json',
            type: "POST",
            data: ({ masterContactId: masterContactId, id: id }),
            async: false,
            cache: false,
            success: function (result) {
                if (result.status == true) {

                    $("#jqgridContactAssociate").setGridParam({ page: 1 }).trigger("reloadGrid");
                }
                else
                    alert(result.message);

            }
        });

    }

    function acformatter(cellvalue, options, rowObject) {
        var returnstring = "";
        var recordCount = jQuery("#jqgridContactAssociate").jqGrid('getGridParam', 'records');
        var isDefault = rowObject[4];
        if (recordCount > 1 && isDefault == true)
            returnstring += "<div>&nbsp;"
        else
            returnstring += "<div align=left>&nbsp;&nbsp;<a href='javascript:DeleteMasterAssociate(" + rowObject[0] + ")'><img src='/Content/images/icon/Cancel.png'  alt='Delete'  title='Delete' style='border:none;'/> </a>";
        returnstring += "</div>";
        return returnstring;
    }

    function DeleteMasterAssociate(id) {
        $.ajax({
            url: "/AffiliateContact/DeleteMasterContact",
            dataType: 'json',
            type: "POST",
            data: ({ id: id }),
            async: false,
            cache: false,
            success: function (result) {
                if (result.status == true) {

                    $("#jqgridContactAssociate").setGridParam({ page: 1 }).trigger("reloadGrid");
                }
                else
                    alert(result.message);

            }
        });

    }

