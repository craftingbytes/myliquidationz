﻿$(document).ready(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;


    $("button, input:submit, input:button").button();

    jQuery("#jqGrid").jqGrid({
        url: "/RecyclerProductWeight/GetProductWeights",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Month', 'Total Weight Received', 'On Hand', 'CRT Glass', 'Circuit Boards', 'Batteries/Mercury Devices', 'Other Materials', 'Actions'],
        colModel: [
   		{ name: 'Month', index: 'Month', width: 40 },
        { name: 'Total Weight Received', index: 'Total Weight Received', width: 90, align: 'right', formatter: 'integer', formatoptions: { thousandsSeparator: ","} },
        { name: 'On Hand', index: 'On Hand', width: 60, align: 'right', formatter: 'integer', formatoptions: { thousandsSeparator: ","} },
        { name: 'CRT Glass', index: 'CRT Glass', width: 60, align: 'right', formatter: 'integer', formatoptions: { thousandsSeparator: ","} },
        { name: 'Circuit Boards', index: 'Circuit Boards', width: 70, align: 'right', formatter: 'integer', formatoptions: { thousandsSeparator: ","} },
        { name: 'Batteries/Mercury Devices', index: 'Batteries/Mercury Devices', width: 110, align: 'right', formatter: 'integer', formatoptions: { thousandsSeparator: ","} },
        { name: 'Other Materials', index: 'Other Materials', width: 70, align: 'right', formatter: 'integer', formatoptions: { thousandsSeparator: ","} },
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: actionFormatter, align: 'center', width: 40, editable: false }

   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 230,
        editurl: "PerformAction",
        afterInsertRow: function (rowid, rowdata, rowelem) {

        }


    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();
});

$(function () {
    $("#txtFrom").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/yy', buttonImageOnly: true, changeMonth: true, changeYear: true });
});
$(function () {
    $("#txtTo").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/yy', buttonImageOnly: true, changeMonth: true, changeYear: true });
});

function actionFormatter(cellvalue, options, rowObject) {
    var rowId = options.rowId;
    return "<div><a href='javascript:editProduct(" + rowId + ")'><img src='../../Content/images/icon/edit.png' alt='Edit' title='Edit Product Weight' style='border:none;'/></a></div>";
}

$("#btnSearch").click(function () {
    var fromdate = $("#txtFrom").val();
    var todate = $("#txtTo").val();
    jQuery("#jqGrid").setGridParam({ postData: {searchFrom: fromdate, searchTo: todate }, page: 1 }).trigger("reloadGrid");
});

$('input[id$=btnAdd]').click(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/RecyclerProductWeight/Create';
    window.location.href = url;
});

function editProduct(rowId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/RecyclerProductWeight/Edit/' + rowId;
    window.location.href = url;
}