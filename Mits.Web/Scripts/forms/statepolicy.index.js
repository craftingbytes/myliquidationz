﻿
$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;

    //'State dropdown onchange function' For getting policies against a particular state

    $('select[id$=ddlState]').change(function (elem) {

        state = $("select#ddlState").val();

        if (state == "0") {
            $("input[name=selectedObjects]").attr('checked', false);
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");
        }

        else {
            $("input[name=selectedObjects]").attr('checked', false);
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");
            $.ajax({
                url: rootPath + '/StatePolicy/GetPolicies',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {


                        $.each(result.data, function (index, item) {


                            $("input[id=" + item.Value + "]").prop('checked', true);


                        });
                    }

                }
            });
        }
    });

    //'Clear button onclick function' For clearing necessary fields

    $('input[id$=btnClear]').click(function () {
        $("select#ddlState").val(0);
        $("input[name=selectedObjects]").prop('checked', false);
        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");
    });


    //'Submit button onclick function' For submitting data to the controller action

    $('input[id$=btnSubmit]').click(function () {



        state = $("select#ddlState").val();
        if (state == "0") {
            $("label#lblMessage").text("No State Selected").css("color", "Red");

            $("ul#message").css("visibility", "visible");
            return;
        }
        if ($("input:checkbox:checked").length == 0) {
            $("label#lblMessage").text("No Policy Selected").css("color", "Red");

            $("ul#message").css("visibility", "visible");
            return;
        }


        var policies = "";
        $("input:checkbox:checked").each(function () {
            if (policies == "")
                policies = $(this).val();
            else
                policies = policies + ',' + $(this).val();


        });

        $("label#lblMessage").text("");
        $("ul#message").css("visibility", "hidden");

        $.ajax({
            url: rootPath + '/StatePolicy/Save',
            dataType: 'json',
            type: "POST",
            data: ({ state: state, policies: policies }),
            async: false,
            success: function (result) {
                if (result.AccessDenied == "AccessDenied") {
                    var rootPath = window.location.protocol + "//" + window.location.host;
                    var url = rootPath + '/Home/AccessDenied';
                    window.location.href = url;
                }
                else {
                    $("label#lblMessage").text(result.message);
                    $("ul#message").css("visibility", "visible");
                    if (result.data) {
                        $("select#ddlState").fillSelect(result.data);
                        $("select#ddlState").val(result.selected);
                        $("input#selectedObjects").attr('checked', true);
                    }
                }
            }
        });


    });
});

