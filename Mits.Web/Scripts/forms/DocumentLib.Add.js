﻿
var fileName = '';
$(document).ready(function () {


//    $("td:contains(*)").each(
//                function () {
//                    $(this).html(
//                        $(this).html().replace('*', "<font color='red'>*</font>")
//                    );
//                }
//            );
    //$('#fileInput').uploadify({
    $('#fileInput').uploadifive({
        //'uploader': '../../Scripts/js/uploadify.swf',
        'cancelImg': '/Content/images/cancel.png',
        'buttonImg': '/Content/images/browse.jpg',
        'auto': false,
        'multi': false,
        'simUploadLimit': 4,
        'sizeLimit': 10737418240,
        'checkScript': '/FileUpload/FileExists',
        'onCheck': function (file, exists) {
            if (exists) {
            alert("File with the same name already exists. \nPlease, rename your file.");
            //document.getElementById(jQuery(event.target).attr('id') + "Uploader").clearFileUploadQueue(true);
            $('#fileInput').uploadifive('clearQueue');
                //return false;
            }

        },
        'uploadScript': '/Helpers/Upload.ashx',
        //'script': '/Helpers/Upload.ashx',
        'folder': '/Uploads',
        onUploadComplete: function (file, data) {
            fileName = file.name;
            return true;
        },
        onQueueComplete: function (uploads) {
            if (uploads.count > 0) {
                post();
            }
            // alert(data.filesUploaded + " file(s) successfully uploaded ");
            //if (data.filesUploaded > 0) {
            //    post();
            //}
        }


    });


    $('input[id$=btnUpload]').click(function () {

        var isValid = ValidateForm(); //true;

        if ($('input[id$=txtTitle]').val() != "" && isValid) {

            //$('#fileInput').uploadifyUpload();
            $('#fileInput').uploadifive('upload');
            //$('#fileInput').uploadifive();
        }
        else {

            if ($('input[id$=txtTitle]').val() == "") {
                //$('input[id$=txtTitle]').addClass("input-validation-error");
                $('input[id$=txtTitle]').css("background-color", "#FFEEEE");
            }

            //            if ($('textarea[id$=txtDescription]').val() == "") {
            //                //$('textarea[id$=txtDescription]').addClass("input-validation-error");
            //                $('textarea[id$=txtDescription]').css("background-color", "#FFEEEE");
            //            }
        }

    });

    $('input[id$=btnClear]').click(function () {
        // $('#fileInput').uploadifyCancel();
        $('#fileInput').uploadifive('clearQueue');
        //$('#fileInput').uploadifyClearQueue();
        $("#txtTitle").val('');
        $("#txtTags").val('');
        $("#txtDescription").val('');
        $('input[id$=txtTitle]').css("background-color", "white");
        $("#validationSummary").html("");

    });

    $('input[id$=txtUploadDate]').datepicker({

});
$('input[id$=txtUploadDate]').datepicker('setDate', new Date());
$("button, input:submit, input:button").button();




});

function post() {
    var title = $('input[id$=txtTitle]').val();
    var description = $('textarea[id$=txtDescription]').val();
    var tags = $('input[id$=txtTags]').val();
    var uploadDate = $('input[id$=txtUploadDate]').val();
    var state = $('#ddlVendorState :selected').text();
    var planyear = $('input[id$=txtPlanYear]').val();
   
    var data = { Title: title, Desc: description, Tags: tags, UpDate: uploadDate, filePath: fileName, State: state, PlanYear:planyear };
    $.ajax({
        type: "POST",
        url: '/DocumentLib/Add',
        data: data,
        success: function (result) {
            window.location = '/DocumentLib/list';
        },
        async: false
    });

}

function imposeMaxLength(Object, MaxLen) {
    return (Object.value.length <= MaxLen);
}