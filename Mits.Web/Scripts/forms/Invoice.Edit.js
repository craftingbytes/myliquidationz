﻿var rootPath = window.location.protocol + "//" + window.location.host;

//********************************************************************************************************************************************* //
//********************************************************** Affliate Target JQ Grid - Start ************************************************** //
//********************************************************************************************************************************************* //
$(document).ready(function () {

    jQuery("#jqGrid").jqGrid({
        url: "/Invoice/GetInvoiceItems",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['InvoiceItemID', 'Service Type', 'Quantity Type', 'Product Type', 'Date', 'Quantity', 'Rate', 'Adj %', 'Actions'],
        colModel: [
   		{ name: 'InvoiceItemID', index: 'InvoiceItemID', width: 85, hidden: true },
   		{ name: 'ServiceType', index: 'ServiceType', editable: true, sortable: false, editrules: { required: true }, edittype: 'select', editoptions: { size: 30, maxlength: 100} },
   		{ name: 'QuantityType', index: 'QuantityType', editable: true, sortable: false, editrules: { required: true }, edittype: 'select', editoptions: { size: 30, maxlength: 100} },
   		{ name: 'ProductType', index: 'ProductType', editable: true, sortable: false, editrules: { required: true }, edittype: 'select', editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Date', index: 'Date', sortable: false, editable: true, datefmt: 'M/d/yyyy', editrules: { required: true, date: true },
   		    editoptions: {
   		        dataInit: function (elem) { $(elem).datepicker({ buttonImage: '../../Content/images/calendar.gif', altFormat: 'd/M/yyyy', buttonImageOnly: true }); }
   		    }
   		},
   		{ name: 'Quantity', index: 'Quantity', align: 'left', editable: true, sortable: false, editrules: { required: true, number: true }, editoptions: { size: 30, maxlength: 100} },
        { name: 'Rate', index: 'Rate', width: 85, align: 'left', editable: true, sortable: false, editrules: { required: true, number: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'QuantityAdjustPercent', index: 'QuantityAdjustPercent', width: 70, editable: true, sortable: false, editrules: { required: true, number: true, minValue: 0, maxValue: 100 }, edittype: 'text', editoptions: { size: 30, maxlength: 100} },
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }
   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        autowidth: true,
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 250,
        editurl: "/Invoice/EditInvoiceItem",
        gridComplete: function () { LoadDropdownsData(); }
    });


    $("#jqGrid").jqGrid('navGrid', '#pager', { edit: false, add: false, del: false, search: false, refresh: true });

    $("#btnAdd").click(function () {
        $("#jqGrid").jqGrid('editGridRow', "new", { height: 280, width: 350, closeAfterAdd: true, beforeShowForm: beforeShowAddPopup });
    });
    
});

function beforeShowAddPopup(formid) {

    $(formid).find('select').css('width', '175px');
    $(formid).find('input').css('width', '100px');

    $.each(
    $(formid).find('select'),
    function (index, item) {
        item.selectedIndex = 0;
    }
    )
}


//*************************************************** Custom Action Menu Formatter for JQ Grid - Start **************************************************** //

/*
cellvalue - the cell value
options - a set of options containing
    rowId - the id of the row
    colModel - the colModel for this column
rowData - the data for this row
*/

//Custom Formatter for action menus
function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")' class='delete'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

//var to store last selected row id
var lastSel;

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) 
    {
        CancelRow(lastSel) ;
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none"});
    $('#SC_ActionMenu_' + rowId).css({ display: "block"});
    $('#jqGrid').editRow(rowId);
}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId,AfterSaveRow);
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function DeleteRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').delGridRow(rowId, { delData: { InvoiceItemID: $("#jqGrid").getRowData(rowId).Id} });
}

function AfterSaveRow(result) {
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    $('#jqGrid').trigger("reloadGrid")
}
//*************************************************** Custom Action Menu Formatter for JQ Grid - End **************************************************** //


datePick = function (elem) {
    jQuery(elem).datepicker();
}

// Fill dropdown data to show in grid
function LoadDropdownsData() {
    $.ajax({
        url: '/Invoice/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('State', { editoptions: { value: result.states} });
            $('#jqGrid').setColProp('ServiceType', { editoptions: { value: result.serviceTypes} });
            $('#jqGrid').setColProp('PriceUnit', { editoptions: { value: result.priceUnitTypes} });
            $('#jqGrid').setColProp('QuantityType', { editoptions: { value: result.quantityTypes} });
            $('#jqGrid').setColProp('ProductType', { editoptions: { value: result.productTypes} });
            GetUserAccessRights();
        }
    });

}

// Get user access rights from server and show hide grid menu action buttons
function GetUserAccessRights() {
    $.ajax({
        url: '/Invoice/GetUserAccessRights',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            ShowHideCommandButton(result.showEdit, result.showDelete);
        }
    });
}

function CheckRecievable(affiliateID) {
    if (affiliateID != '') {
        $.ajax({
            url: '/Invoice/CheckRecievable',
            dataType: 'json',
            type: "POST",
            data: { affiliateID: affiliateID },
            async: false,
            success: function (result) {
                $("#Invoice_Receivable").attr({ checked: result.result });
            }
        });
    }
}


function ShowHideCommandButton(showEdit, showDelete) {

    if (showEdit == false && showDelete == false) {
        $("#jqGrid").hideCol('ActionMenu');
    }
    else {
        if (showEdit == false)
            $("#jqGrid").find('div > img').filter(".edit").css("display", "none");
        if (showDelete == false)
            $("#jqGrid").find('div > img').filter(".delete").css("display", "none");
    }
}


//********************************************************************************************************************************************* //
//********************************************************** Affiliate Target JQ Grid - End **************************************************** //
//********************************************************************************************************************************************* //

 $(function () {
        $("#Invoice_InvoiceDate").datepicker({ dateFormat: 'mm/dd/yy' });
        $("#Invoice_InvoicePeriodFrom").datepicker({ dateFormat: 'mm/dd/yy' });
        $("#Invoice_InvoicePeriodTo").datepicker({ dateFormat: 'mm/dd/yy' });
        $("#Invoice_InvoiceDueDate").datepicker({ dateFormat: 'mm/dd/yy' });
        });

        function submitForm() {
            var rootPath = window.location.protocol + "//" + window.location.host;
            var isValid = ValidateForm();
            if (isValid) {
                var dueDate = new Date($("#Invoice_InvoiceDueDate").val());
                var date = new Date($("#Invoice_InvoiceDate").val());
                if (dueDate < date) {
                    isValid = false;
                    $("#validationSummary").html("<ul class=ValidationSummary><li>Due date must be greater than Invoice date </li></ul>");
                }

                if (isValid) {
                    document.forms[0].action = rootPath + '/Invoice/Edit';
                    document.forms[0].submit();
                }

                return false;
            }
        }
$("#divMessage").dialog({
    modal: true,
    width: 'auto',
    resizable: false,
    position: 'center',
    autoOpen: false}
);
$("#divDocuments").dialog({
    modal: false,
    width: '300',
    height: '300',
    resizable: false,
    position: 'center',
    autoOpen: false
}
);

function ShowMessage(message) {
    $("#divMessage").html(message);
    $("#divMessage").dialog({ buttons: { "Ok": function () { $(this).dialog("close"); } } });
    $("#divMessage").dialog('open');
}
function ShowDocumentsDiv() {
    $("#divDocuments").dialog({ buttons: { "Ok": function () { $(this).dialog("close"); } } });
    $("#divDocuments").dialog('open');
}

//********************************************************************************************************************************************* //
//**************************************************************** File Upload - Start ******************************************************** //
//********************************************************************************************************************************************* //

$(document).ready(function () {

    $('#btnAttachment').click(function () {
        var w = window.open("../../Upload.aspx?fromInvoice=true", "Window1", "height=300,width=450,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");
    });

});
function FileUploaded(fileName) {
    $('#txtUploadedFileName').val(fileName);
    alert('File has been uploaded successfully.Please click submit to save.');
}
function Download(fileName) {
    if (fileName != "-" && fileName != "") {
        fileName = fileName.replace('#', '<~>');
        window.location = "/Invoice/Download?FileName=" + fileName;
    }
}
function CheckRecievable(affiliateID) {
    if (affiliateID != '') {
        $.ajax({
            url: '/Invoice/SaveAttachment',
            dataType: 'json',
            type: "POST",
            data: { affiliateID: affiliateID },
            async: false,
            success: function (result) {
                $("#Invoice_Receivable").attr({ checked: result.result });
            }
        });
    }
}
//********************************************************************************************************************************************* //
//**************************************************************** File Upload - End ********************************************************* //
//********************************************************************************************************************************************* //

//*************************************************** Custom Action Get City list **************************************************** //

function GetCities(elem) {
    if (elem != null && elem.id != null) {
        state = $("select#" + elem.id).val();
        if (state == "0") {
            $("select#" + elem.id).clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        if (elem.id == "ProcessingData_PickupStateId")
                            $("select#ProcessingData_PickupCityId").fillSelect(result.data);
                        else
                            $("select#ProcessingData_VendorCityId").fillSelect(result.data); 

                    }
                }
            });
        }
    }
}

function LoadDeviceTypes(elem) {

    state = $("select#" + elem.id).val();       //Represents RegionID
    city = $("select#ProcessingData.PickupCityId").val();
    zip = $("input#ProcessingData.PickupZip").val();

    $("#jqGrid").clearGridData(true);


    $.ajax({
        url: rootPath + '/ProcessingData/GetDevicesList',
        dataType: 'text',
        type: "POST",
        data: ({ city: city, state: state, zip: zip }),
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('Description', { editoptions: { value: result} });
        }
    });

    GetCities(elem);
}

//*************************************************** Custom Action Get City list - End **************************************************** //