﻿
//********************************************************************************************************************************************* //
//*************************************************** Collection Event - Start ****************************************************************** //
//********************************************************************************************************************************************* //
$(document).ready(function () {

    var stateId = $('#hStateId').val();

    jQuery("#jqGrid").jqGrid({
        url: "/CollectionEvent/GetCollectionEvent?stateId=" + stateId,
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Address', 'City', 'Location Name', 'Zip', 'Date and Hours', 'Note'],
        colModel: [
            { name: 'Address', index: 'Address', width: 90, editable: false, sortable: false },
   		    { name: 'City', index: 'City', width: 50, editable: false, sortable: false },
            { name: 'Location', index: 'Location', width: 50, editable: false, sortable: false },
   		    { name: 'Zip', index: 'Zip', width: 30, editable: false, sortable: false },
            { name: 'Hours', index: 'Hours', width: 120, editable: false, sortable: false },
            { name: 'Note', index: 'Note', width: 90, editable: false, sortable: false }
   	    ],
        autowidth: true,
        viewrecords: true,
        rownumbers: false,
        //width: 850,
        height: 400
    });

});

