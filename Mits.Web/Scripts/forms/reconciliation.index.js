﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;
    $.ajax(
            {
                url: rootPath + '/Reconciliation/GetEntities/',
                dataType: 'json',
                type: "POST",
                data: ({}),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#EntityId").fillSelect(result.data);
                    }
                }
            });

    $('select[id$=EntityId]').change(function (elem) {
        $("input#WeightNotCollected").val("");
        $("input#AssignWeight").val("");
        $("input#AssignWeightPercentage").val("");

        entity = $("select#EntityId").val();
        if (entity == "0") {
            $(this.form).find(':input').each(function () {
                switch (this.type) {
                    case 'password':
                    case 'select-multiple':
                    case 'select-one':
                        $(this).val(0);
                        break;
                    case 'text':
                        $(this).val('');
                        break;
                    case 'textarea':
                        $(this).val('');
                        break;
                    case 'checkbox':
                        if (this.name != 'Active')
                            this.checked = false;
                        break;
                }
            });
            $("select#StateId").clearSelect();
            $("select#PeriodId").clearSelect();
            $("#validationSummary").html("");
        }
        else {
            $.ajax(
                        {
                            url: rootPath + '/Reconciliation/GetTargetStates/',
                            dataType: 'json',
                            type: "POST",
                            data: ({ entity: entity }),
                            async: false,
                            success: function (result) {
                                if (result.data) {
                                    $("select#StateId").fillSelect(result.data);
                                }
                                else {
                                    $("select#StateId").clearSelect();
                                }
                            }
                        });

            state = $("select#StateId").val();
            if (state != null) {
                $.ajax(
                                    {
                                        url: rootPath + '/Reconciliation/GetStatesTargetPeriods/',
                                        dataType: 'json',
                                        type: "POST",
                                        data: ({ entity: entity, state: state }),
                                        async: false,
                                        success: function (result) {
                                            if (result.data) {
                                                $("select#PeriodId").fillSelect(result.data);
                                            }
                                        }
                                    });

                targetid = $("select#PeriodId").val();
                $.ajax(
                        {
                            url: rootPath + '/Reconciliation/GetAvailableWeight/',
                            dataType: 'json',
                            type: "POST",
                            data: ({ state: state }),
                            async: false,
                            success: function (result) {
                                if (result.availableProcessorsWeight) {
                                    $("input#AvailableWeight").val(result.availableProcessorsWeight);
                                }
                            }
                        });

                target = $("select#PeriodId").val();
                if (target != null) {
                    $.ajax(
                                        {
                                            url: rootPath + '/Reconciliation/GetTargetWeight/',
                                            dataType: 'json',
                                            type: "POST",
                                            data: ({ target: target }),
                                            async: false,
                                            success: function (result) {
                                                if (result.TargetWeight) {
                                                    $("input#TargetWeight").val(result.TargetWeight);
                                                    $("input#WeightCollected").val(result.WeightCollected);
                                                    $("input#WeightNotCollected").val(result.WeightNotCollected);
                                                }
                                            }
                                        });
                }
                else {
                    $("input#TargetWeight").val("100");
                }

            }
            else {
                $("select#PeriodId").clearSelect();
            }
        }
    });

    $('select[id$=StateId]').change(function (elem) {

        entity = $("select#EntityId").val();
        if (entity == "0") {
            $("select#StateId").clearSelect();
            $("select#PeriodId").clearSelect();
        }
        else {
            state = $("select#StateId").val();
            if (state != null) {
                $.ajax(
                        {
                            url: rootPath + '/Reconciliation/GetStatesTargetPeriods/',
                            dataType: 'json',
                            type: "POST",
                            data: ({ entity: entity, state: state }),
                            async: false,
                            success: function (result) {
                                if (result.data) {
                                    $("select#PeriodId").fillSelect(result.data);
                                }
                            }
                        });
                target = $("select#PeriodId").val();
                $.ajax(
                        {
                            url: rootPath + '/Reconciliation/GetAvailableWeight/',
                            dataType: 'json',
                            type: "POST",
                            data: ({ state: state, targetId: target }),
                            async: false,
                            success: function (result) {
                                if (result.availableProcessorsWeight) {
                                    $("input#AvailableWeight").val(result.availableProcessorsWeight);
                                }
                            }
                        });
                target = $("select#PeriodId").val();
                if (target != null) {
                    $.ajax(
                                        {
                                            url: rootPath + '/Reconciliation/GetTargetWeight/',
                                            dataType: 'json',
                                            type: "POST",
                                            data: ({ target: target }),
                                            async: false,
                                            success: function (result) {
                                                if (result.TargetWeight) {
                                                    $("input#TargetWeight").val(result.TargetWeight);
                                                    $("input#WeightCollected").val(result.WeightCollected);
                                                    $("input#WeightNotCollected").val(result.WeightNotCollected);
                                                }
                                            }
                                        });
                }
            }
            else {
                $("select#PeriodId").clearSelect();
            }
        }
    });

    $('select[id$=PeriodId]').change(function (elem) {

        state = $("select#StateId").val();
        if (state != null) {
            target = $("select#PeriodId").val();
            $.ajax(
                        {
                            url: rootPath + '/Reconciliation/GetAvailableWeight/',
                            dataType: 'json',
                            type: "POST",
                            data: ({ state: state, targetId: target }),
                            async: false,
                            success: function (result) {
                                if (result.availableProcessorsWeight) {
                                    $("input#AvailableWeight").val(result.availableProcessorsWeight);
                                }
                            }
                        });
            target = $("select#PeriodId").val();
            if (target != null) {
                $.ajax(
                                        {
                                            url: rootPath + '/Reconciliation/GetTargetWeight/',
                                            dataType: 'json',
                                            type: "POST",
                                            data: ({ target: target }),
                                            async: false,
                                            success: function (result) {
                                                if (result.TargetWeight) {
                                                    $("input#TargetWeight").val(result.TargetWeight);
                                                    $("input#WeightCollected").val(result.WeightCollected);
                                                    $("input#WeightNotCollected").val(result.WeightNotCollected);
                                                }
                                            }
                                        });
            }
        }
    });

    $('input[id$=btnClear]').click(function () {

        $(this.form).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                    $(this).val(0);
                    break;
                case 'text':
                    $(this).val('');
                    break;
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                    if (this.name != 'Active')
                        this.checked = false;
                    break;
            }
        });
        $("select#StateId").clearSelect();
        $("select#PeriodId").clearSelect();
        $("#validationSummary").html("");
    });

    $('input[id$=AssignWeightPercentage]').blur(function () {
        var assignWeightPercentage = $("input#AssignWeightPercentage").val();
        var availableWeight = $("input#AvailableWeight").val();
        var assignWeight = "";
        if (availableWeight != "" && availableWeight != "0") {
            assignWeight = Math.floor(parseInt(availableWeight) * parseFloat(assignWeightPercentage) / 100);

            if (assignWeight.toString() != "NaN")
                $("input#AssignWeight").val(assignWeight);
            else
                $("input#AssignWeight").val("");
        }


    });
    $('input[id$=AssignWeight]').blur(function () {
        var assignWeight = $("input#AssignWeight").val();
        var availableWeight = $("input#AvailableWeight").val();
        var assignWeightPercentage = "";
        if (availableWeight != "" && availableWeight != "0") {
            assignWeightPercentage = (parseInt(assignWeight) * 100) / parseInt(availableWeight);
            assignWeightPercentage = roundNumber(assignWeightPercentage, 2);
            if (assignWeightPercentage.toString() != "NaN")
                $("input#AssignWeightPercentage").val(assignWeightPercentage);
            else
                $("input#AssignWeightPercentage").val("");
        }

    });



    $('input[id$=btnSubmit]').click(function () {

        if (!ValidateForm(document.forms[0]))
            return false;
        entity = $("select#EntityId").val();
        state = $("select#StateId").val();
        target = $("select#PeriodId").val();
        weightToReconcile = $("input#AssignWeight").val();
        if (entity != null && entity != "0") {
            weightNotCollected = $("input#WeightNotCollected").val();
            weightCollected = $("input#WeightCollected").val();
            availableWeight = $("input#AvailableWeight").val();
            $.ajax({
                url: rootPath + '/Reconciliation/Reconcile/',
                dataType: 'json',
                type: "POST",
                data: ({ entity: entity, state: state, target: target, availableWeight: availableWeight, weightNotCollected: weightNotCollected, weightCollected: weightCollected, weightToReconcile: weightToReconcile }),
                async: false,
                success: function (result) {
                    if (result.AccessDenied == "AccessDenied") {
                        var rootPath = window.location.protocol + "//" + window.location.host;
                        var url = rootPath + '/Home/AccessDenied';
                        window.location.href = url;
                    }
                    else {
                        //$("label#lblMessage").text(result.message);
                        //$("ul#message").css("visibility", "visible");
                        if (result) {
                            $("#validationSummary").html("<ul class=ValidationSummary><li>Weight has been reconciled successfully.</li></ul>");
                            $("input#WeightNotCollected").val(result.updatedWeightNotCollected);
                            $("input#WeightCollected").val(result.updatedWeightCollected);
                            $("input#AvailableWeight").val(result.updatedAvailableWeight)
                            $("input#AssignWeight").val("");
                            $("input#AssignWeightPercentage").val("");

                        }
                    }
                }
            });
        }
        else {
            $("#validationSummary").html("<ul class=ValidationSummary><li>Entity is required.</li></ul>");
        }



    });


});

function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";
  
    var oRequired;
    var sRequired = '';
    var type;
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {            
                sRequired = oRequired.getAttribute('required');
                type = oRequired.getAttribute('type');
                if (sRequired) {//is it required
                    try {                    
                        if (!oRequired.value || $.trim(oRequired.value) == '' || (oRequired.value == "0" && type != "text")) {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        weightToReconcile = $("input#AssignWeight").val();
        weightNotCollected = $("input#WeightNotCollected").val();
        availableWeight = $("input#AvailableWeight").val();
        if (parseInt(weightNotCollected) < parseInt(availableWeight) &&
        parseInt(weightNotCollected) < parseInt(weightToReconcile)) {
            bValidated = false;
            $("input#AssignWeight").css("background-color", "#FFEEEE");
            dynamicHTML += "<li>" + "Assign weight should be less than Remaining Obligation Weight" + "</li>";
        }
        if (parseInt(weightNotCollected) > parseInt(availableWeight) &&
        parseInt(availableWeight) < parseInt(weightToReconcile)) {
            bValidated = false;
            $("input#AssignWeight").css("background-color", "#FFEEEE");
            dynamicHTML += "<li>" + "Assign weight should be less than Processed Weight Available" + "</li>";

        }
    
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}