﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;

    /*
    if ($('#IsProcessor').val() != 'True') {
        $('select[id$=ddlStates]').clearSelect();
    } else {
        $('#trAffiliate').css('display', 'none');
    }
    */

    //$("button, input:submit, input:button").button();

    jQuery("#jqGrid").jqGrid({
        url: "/Invoice/GetProcessorInvoices",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Invoice Id', 'Entity Name', 'InvoiceDate', 'State', 'Amount', 'Receivable', '', 'Approved', ' ', ' ', 'Actions',''],
        colModel: [
   		{ name: 'Invoice Id', index: 'Invoice Id', width: 40, hidden: false },
   		{ name: 'Entity', index: 'Entity', width: 125, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100 }, hidden: ($('#IsProcessor').val() == 'True' ? true : false) },
        { name: 'InvoiceDate', index: 'InvoiceDate', width: 50, editable: true, sortable: false, align: "center", editoptions: { dataInit: function (element) { $(element).datepicker(); } } },
   		{ name: 'State', index: 'State', width: 30, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Amount', index: 'Amount', width: 30, editable: false, align:'right', sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125 }, align: "right" },
        { name: 'Receivables', index: 'Receivables', width: 65, editable: false, hidden: true, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125} },
        { name: 'Entity Type', index: 'Entity Type', width: 50, hidden: true },
        { name: 'Approved', index: 'Approved', sortable: false, width: 30, align: 'center', formatter: ApprovedCheckBoxFormatter },
        { name: 'DownloadFile', index: 'ProcessingDataID', width: 30, editable: false, sortable: false, align: 'center' },
   		{ name: 'AttachFile', index: 'ProcessingDataID', width: 30, editable: false, sortable: false, align: 'center' },
        { name: 'Verified', index: 'Verified', width: 85, align: 'Left', sortable: false, hidden: true },
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: myformatter, title: true, align: 'center', width: 60, editable: false, align: "right" }
        

   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 230,
        editurl: "PerformAction",
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: "/Invoice/UpdateDate",
        afterInsertRow: function (rowid, rowdata, rowelem) {

        },
        afterSubmitCell: function (serverStatus, aPostData) {

            var json = serverStatus.responseText;
            var result = eval("(" + json + ")");
            return [result.success, result.message, ""];
        }

    });
    
    $("#jqGrid").jqGrid('navGrid', '#pager',
    { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();
    
    $("#dlgAdjustment").dialog({
    modal: true,
    width: 800,
    height: 240,
    //zIndex: 500,
    resizable: false,
    position: 'center',
    autoOpen: false,
    show: 'fold',
    hide: 'fold',
    Ok: function () {
    $(this).dialog('close');
    }
    });


    

    $("#dlgAdjBtnCancel").click(function () {
        $('#dlgAdjustment').dialog('close'); ;
        $("#txtCollectorAdjDesc").val('');
        $("#txtCollectorAdjAmount").val('');

    });

    $("#dlgAdjBtnSave").click(function () {
        AddAdjustment();
    });
    
});

$('select[id$=ddlAffiliates]').change(function (elem) {

    affiliate = $('select[id$=ddlAffiliates]').val();
    if (affiliate == "0") {
        $('select[id$=ddlStates]').clearSelect();
    }
    else {

        $('select[id$=ddlStates]').clearSelect();
        $.ajax({
            url: "/Invoice/GetEntityStates",
            dataType: 'json',
            type: "POST",
            data: ({ affiliate: affiliate }),
            async: false,
            success: function (result) {
                if (result._invoiceList) {
                    $("select#ddlStates").fillSelect(result._invoiceList);
                }
            }
        });
    }
});

//$("#txtSearch").keypress(function (event) {
//    if (event.keyCode == '13') {
//        var searchVal = $("#txtSearch").val();
//        state = $("#state option:selected").text();            
//        jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal }, page: 1 }).trigger("reloadGrid");
//    }
//});
$("#btnSearch").click(function () {
    var entityVal = $("#ddlAffiliates").val();
    var invoiceVal = $("#txtInvoice").val();
    var receivableVal = $("#receivables").attr('checked');
    var stateVal = $("#ddlStates").val();
    var dateFrom = $("#txtFrom").val();
    var dateTo = $("#txtTo").val();
    //state = $("#state option:selected").text();            
    jQuery("#jqGrid").setGridParam({ postData: { searchEntity: entityVal, searchInvoice: invoiceVal, searchReceivable: receivableVal, searchState: stateVal, searchDateTo: dateTo, searchDateFrom: dateFrom }, page: 1 }).trigger("reloadGrid");
});

$('input[id$=btnAdd]').click(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/Invoice/Edit';
    window.location.href = url;
});

function editInvoice(t) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/Invoice/Edit/' + t;
    window.location.href = url;
}
//$(function () {
    //    $("#txtTo").datepicker({ dateFormat: 'yy-mm-dd' });
//    $("#txtTo").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
//    $("#txtFrom").datepicker({ buttonImage: '../../Content/images/calendar.gif', dateFormat: 'mm/dd/yy', buttonImageOnly: true });
//});

function myformatter(cellvalue, options, rowObject) {
    var isApproved = rowObject[7];
    var InvoiceNumber = rowObject[0];
    var type = rowObject[6];

    if (InvoiceNumber == undefined)
        return cellvalue;
    if (type == "OEM") {
        var url = "return showReport('../Reports/OEMInvoice.aspx?InvoiceID=" + InvoiceNumber + "', 'InvoiceReport');";
        return "<div><a href='javascript:editInvoice(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Invoice Entity'  style='border:none;'/> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> </div>";
    }
    else {
        var url = "return showReport('../Reports/RecyclerInvoice.aspx?InvoiceID=" + InvoiceNumber + "', 'InvoiceReport');";
        var urlpr = "return showReport('../Reports/ProcessingReport.aspx?InvoiceID=" + InvoiceNumber + "', 'ProcessingReport');"
        if (isApproved == 1)
            return "<div> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + urlpr + "\" href='#' ><img src='../../Content/images/report.png'  alt='Processing Report'  title='Processing Report'  style='border:none;'/></a>  <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:displayAddAdjustment(\"" + InvoiceNumber + "\")' href='#' ><img src='../../Content/images/icon/sm/plus.gif'  alt='Add Adjustment'  title='Add Adjustment'  style='border:none;'/></a></div>";
        else
            return "<div><a href='javascript:editInvoice(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Processing Report'  style='border:none;'/> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + urlpr + "\" href='#' ><img src='../../Content/images/report.png'  alt='Processing Report'  title='Processing Report'  style='border:none;'/></a>  <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:displayAddAdjustment(\"" + InvoiceNumber + "\")' href='#' ><img src='../../Content/images/icon/sm/plus.gif'  alt='Add Adjustment'  title='Add Adjustment'  style='border:none;'/></a></div>";
    
    }
}

function reformatActionMenu(rowId, invoiceId, approved, invoiceDate) {
    var isApproved = approved;
    var InvoiceNumber = invoiceId;

    var formattedAction;
    var url = "return showReport('../Reports/RecyclerInvoice.aspx?InvoiceID=" + InvoiceNumber + "', 'InvoiceReport');";
    var urlpr = "return showReport('../Reports/ProcessingReport.aspx?InvoiceID=" + InvoiceNumber + "', 'ProcessingReport');"
    if (isApproved == 1)
        formattedAction = "<div> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + urlpr + "\" href='#' ><img src='../../Content/images/report.png'  alt='Processing Report'  title='Processing Report'  style='border:none;'/></a>  <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:displayAddAdjustment(\"" + InvoiceNumber + "\")' href='#' ><img src='../../Content/images/icon/sm/plus.gif'  alt='Add Adjustment'  title='Add Adjustment'  style='border:none;'/></a></div>";
    else
        formattedAction = "<div><a href='javascript:editInvoice(" + InvoiceNumber + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Processing Report'  style='border:none;'/> </a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + urlpr + "\" href='#' ><img src='../../Content/images/report.png'  alt='Processing Report'  title='Processing Report'  style='border:none;'/></a>  <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Invoice Report'  title='Invoice Report'  style='border:none;'/></a> <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:displayAddAdjustment(\"" + InvoiceNumber + "\")' href='#' ><img src='../../Content/images/icon/sm/plus.gif'  alt='Add Adjustment'  title='Add Adjustment'  style='border:none;'/></a></div>";


    $("#jqGrid").jqGrid("setCell", rowId, 'ActionMenu', formattedAction);
    $("#jqGrid").jqGrid("setCell", rowId, 'InvoiceDate', invoiceDate);
}

function UpdateApproval(rowId, invoiceid) {
    var chkbox = document.getElementById("approvedCheck" + rowId);
    $.ajax({
        url: '/Invoice/UpdateApproval',
        dataType: 'json',
        type: "POST",
        data: "id=" + invoiceid,
        async: false,
        success: function (result) {
            if (result.success) {
                alert("Success:Invoice Updated");
                reformatActionMenu(rowId, invoiceid, result.approved, result.invoicedate);
                
            }
            else {
                alert("Update Failed:" + result.message);
                chkbox.checked = !(chkbox.checked);
            }
        }

    });
}

function ApprovedCheckBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var invoiceid = rowObject[0];
    if (rowObject[10] != 1)
        return "&nbsp;";
    if (cellvalue == 1)
        return "<input type='checkbox' Name='Approved' id='approvedCheck" + rowid + "' onclick=\"UpdateApproval(" + rowid + "," + invoiceid + ")\"  checked />";
    else
        return "<input type='checkbox' Name='Approved' id='approvedCheck" + rowid + "' onclick=\"UpdateApproval(" + rowid + "," + invoiceid + ")\"  />";
}

function displayAddAdjustment(invoiceNumber) {
    $("#currentInvoice").val(invoiceNumber);
    $("#dlgAdjustment").dialog('open');
}

function AddAdjustment() {
    if (!ValidateAdjDetailInputs()) {
        return;
    }
    var invoiceId = $("#currentInvoice").val();
    var adjDesc = $("#txtAdjustmentDesc").val();
    var adjAmount = $("#txtAdjustmentAmount").val();
    var fAdjAmount = parseFloat(adjAmount);
    $.ajax({
        url: '/Invoice/AddAdustment',
        dataType: 'json',
        type: "POST",
        data: ({ InvoiceId: invoiceId, Description: adjDesc, Amount: fAdjAmount }),
        async: false,
        success: function (result) {
            if (result.message == "AccessDenied") {
                var rootPath = window.location.protocol + "//" + window.location.host;
                var url = rootPath + '/Home/AccessDenied';
                window.location.href = url;
            }
            else if (result.status == "True") {
                if (result.message != '') {
                    alert(result.message);
                    $('#dlgAdjustment').dialog('close');
                    $("#txtAdjustmentDesc").val('');
                    $("#txtAdjustmentAmount").val('');
                }
                jQuery("#jqGrid").trigger("reloadGrid");
            } else {
                alert(result.message);
            }
        }
    });
}

function ValidateAdjDetailInputs() {
    var invoiceId = $("#currentInvoice").val();
    var collectorAdjDesc = $("#txtAdjustmentDesc").val();
    var adjAmount = $("#txtAdjustmentAmount").val();
    var fAdjAmount = parseFloat(adjAmount);
    var errIndex = 0;
    var errMsg = '';
    if (collectorAdjDesc < 1) {
        errIndex++;
        errMsg += errIndex + ". Description is required.\n";
    }
    if (isNaN(fAdjAmount)) {
        errIndex++;
        errMsg += errIndex + ". Amount not a valid Quantity.\n";
    }
    if (errMsg != '') {
        alert(errMsg);
        return false;
    }
    return true;
}

function openFileDialog(dataProcessReportId) {
    var w = window.open("../../Upload.aspx?processDataId=" + dataProcessReportId + "&attachfile=true", "Window1", "height=300,width=450,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");
}

function openFiles(pDataId) {
    alert(docsHtml);
    $("#dialogFiles").html("<ul>" + docsHtml + "</ul>");
    $("#dialogFiles").dialog({
            modal: false,
            width: 400,
            resizable: false,
            position: 'center'
        }
    );
}

function GetAttachments(processDataId) {
    lastProcessDataId = processDataId;
    var rootPath = window.location.protocol + "//" + window.location.host;
    var docsHtml = "<ul>"

    $.ajax({
        url: rootPath + '/ProcessingData/GetAttachments',
        dataType: 'json',
        type: "POST",
        data: ({ processDataId: processDataId }),
        async: false,
        success: function (result) {
            docsHtml += "<table width='100%'>";
            $.each(result.rows, function (index, entity) {
                docsHtml += "<tr><td width='85%'><a href=javascript:Download('" + entity.UploadFileName + "'); style='color:#FF9900;text-decoration:none;_text-decoration:none;'>" + entity.UploadFileName + "</a></td>";
                docsHtml += "<td width='15%'><img src='../../Content/images/icon/delete.png' alt='Remove attachment' style='cursor:hand;' title='Remove attachment' id='btnDel' border='0' onclick='javascript:DeleteAttachment(" + entity.DocumentID + ");' /></td></tr>";
            });

            docsHtml += "</table>"

        }
    });

    $("#dialogFiles").html(docsHtml);

    $("#dialogFiles").dialog({
        modal: false,
        width: 400,
        resizable: false,
        position: 'center'
    }
                );

}

function Download(fileName) {
    fileName = fileName.replace('#', '<~>');
    if (fileName != "-" && fileName != "") {
        window.location = "/Affiliate/Download?FileName=" + fileName;
    }
}
function DeleteAttachment(docID) {
    var ans = confirm('Are you sure?');
    if (ans) {
        $.ajax({
            url: '/Affiliate/DeleteAttachment/',
            dataType: 'json',
            type: "POST",
            data: { docID: docID },
            async: false,
            success: function (result) {
                GetAttachments(lastProcessDataId);
                $("#jqGrid").trigger("reloadGrid");

            }
        });
    }
}

function SaveAttachment(fileName, processDataId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    $.ajax({
        url: rootPath + '/ProcessingData/SaveAttachment',
        dataType: 'text',
        type: "POST",
        data: ({ fileName: fileName, processDataId: processDataId }),
        async: false,
        success: function (result) {
            $("#jqGrid").trigger("reloadGrid");
        }
    });
}