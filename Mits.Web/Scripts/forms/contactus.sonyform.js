﻿
$(document).ready(function () {

    $("#btnSubmit").click(function () {

        var _poNumber = $("#txtCompanyName").val();
        var _Requester = $("#txtContactName").val();
        var _Email = $("#txtEmail").val();
        var _ContactPhone = $("#txtContactPhone").val();
        var _ItemName = $("#txtItemName").val();
        var _Cost = $("#txtHours").val();
        var _PurchaseFrom = $("#txtCertification").val();
        var _DateRequired = $("#txtDateItem").val();
        var _Description = $("#txtDescription").val();

        $.ajax({
            url: "/Home/SendSonyEmail",
            dataType: 'json',
            type: "POST",
            data: ({ PoNumber: _poNumber, Requester: _Requester, Email: _Email, ContactPhone: _ContactPhone, ItemName: _ItemName, Cost: _Cost, PurchaseFrom: _PurchaseFrom, DateRequired: _DateRequired, Description: _Description }),
            async: false,
            success: function (result) {
                if (result.showResult) {
                    $("#sendResult").text("Message successfully sent!");
                }
                else {
                    $("#sendResult").text("Message sending failed!");
                }

            }
        });
    });
});


function FormatPhone() {
    try {
        var oPhone = event.srcElement
        var iKeyCode = event.keyCode
        var sText = oPhone.value
        switch (sText.length) {
            case 0: //first paren
                if (iKeyCode != 40) {//make sure they are not already adding a paren
                    oPhone.value = "("
                }
                break
            case 4: //second paren
                if (iKeyCode != 41) {//make sure they are not already adding a paren
                    if (sText.indexOf(")") == -1) {//only add one if there isn't one
                        oPhone.value += ")"
                    }
                    if (iKeyCode != 32) {//see if we need to add a space			
                        oPhone.value += " "
                    }
                    if (iKeyCode == 45) {//if they are putting in a dash, cancel it		
                        event.keyCode = 0
                    }
                }
                break
            case 9:
                if (iKeyCode != 45) {//make sure they are not already adding a dash
                    oPhone.value += "-"
                }
                if (iKeyCode != 32) {//if they are putting in a space, cancel it		
                    event.keyCode = 0
                }
                break
        }

    } catch (err) {
        //do nothing
    }
}

function isNumeric(strTest) {
    /**************************************************************************
    * Author        	Daniel Tweddell
    * Revision Date	10/26/2004 
    * 
    * Used to test if a value numeric or not, ignores white space
    **************************************************************************/
    return (strTest.match(eval(/\d/)) == strTest); //numeric chars or .
}

function SetPoNumber() {
    var sPoNumber = new Date(); //seed the randomizer
    sPoNumber = sPoNumber.getTime()
    //substring(sPoNumber.toString(),2,8)+","+
    document.all.txtPoNumber.value = sPoNumber.toString().slice(2, 10)
}
