﻿


function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var type;
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                type = oRequired.getAttribute('type');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || (oRequired.value == "0" && type != "text")) {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        //following if is logic related to current page, you should not use it
        //        if (!IsServiceTypeSelected()) {
        //            bValidated = false;
        //            dynamicHTML += "<li>Please check at least one service type.</li>";
        //        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }

}

function LongLatByZip() {

    var zip = $("input[id$=Zip]").val();
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': ', ' + zip }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var geoResults = results[0];
            var point = geoResults.geometry.location;

            latitude = point.lat();
            longitude = point.lng();

            $("input[id$=Longitude]").val(longitude);
            $("input[id$=Latitude]").val(latitude);

            document.forms[0].submit();

            //keep in some hdden field

        } else {
            //InValidZipCode();
            //alert("invalid zip");
        }
    });
}

$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;

    if ($("#hidMess").text() == "true") {

        $("#saved").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "OK": function () {
                    var url = rootPath + '/DropOffLocations/List/';
                    window.location.href = url;
                }
            }
        });
        return false;
    }


    $('select[id$=StateId]').change(function (elem) {

        state = $("select#StateId").val();
        if (state == "0") {
            $("select#CityId").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities/',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#CityId").fillSelect(result.data);
                    }
                }
            });
        }

    });

    $('input[id$=btnSubmit]').click(function () {

        var isValid = ValidateForm(document.forms[0]);
        if (isValid) {
            LongLatByZip();
        }
        else {
            return false;
        }
    });

    $('#btnCancel').click(function () {
        //        var url = rootPath + '/DropOffLocations/List/';
        //        window.location.href = url + "?data=" + $('#data').val();
        window.close();

    });

    $('#btnBack').click(function () {
        var url = rootPath + '/DropOffLocations/List/';
        window.location.href = url + "?data=" + $('#data').val();

    });

});
