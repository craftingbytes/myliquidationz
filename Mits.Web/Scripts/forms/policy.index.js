﻿$(document).ready(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;

    //'Accreditation dropdown onchange function' for getting name and description against a particular Accreditation

    $('select[id$=ddlPolicy]').change(function (elem) {



        $('input[id$=txtName]').css("background-color", "white");
        $("label#lblMessage").text("");
        $("input#txtName").val("");
        $("textarea#txtDesc").val("");
        $("ul#message").css("visibility", "hidden");


       policy = $("select#ddlPolicy").val();

        $.ajax({
            url: rootPath + '/Policy/GetDetail',
            dataType: 'json',
            type: "POST",
            data: ({ policy: policy }),
            async: false,
            success: function (result) {
                $("input#txtName").val(result.Name);
                $("textarea#txtDesc").val(result.Desc);
            }
        });
    });

    //'Clear button onclick function' for clearing all necessary fields

    $('input[id$=btnClear]').click(function () {
        $("input#txtName").val("");
        $("textarea#txtDesc").val("");
        $("select#ddlPolicy").val(0);
        $("label#lblMessage").text("");
        $('input[id$=txtName]').css("background-color", "white");
        $("ul#message").css("visibility", "hidden");

    });


    //'Submit button onclick function' for submitting necessary data to the controller action

    $('input[id$=btnSubmit]').click(function () {


        if ($('input[id$=txtName]').val() == "" || $.trim($('input[id$=txtName]').val()) == '') {

            $('input[id$=txtName]').css("background-color", "#FFEEEE");
            $("label#lblMessage").text("Name is required").css("color", "Red");
            $("ul#message").css("visibility", "visible");
            return false;
        }
        else {

            $('input[id$=txtName]').css("background-color", "white");
            $("label#lblMessage").text("");
            $("ul#message").css("visibility", "hidden");

            policy = $("select#ddlPolicy").val();
            name = $("input#txtName").val();
            desc = $("textarea#txtDesc").val();

            $.ajax({
                url: rootPath + '/Policy/Save',
                dataType: 'json',
                type: "POST",
                data: ({ policy: policy, name: name, desc: desc }),
                async: false,
                success: function (result) {
                    if (result.AccessDenied == "AccessDenied") {
                        var rootPath = window.location.protocol + "//" + window.location.host;
                        var url = rootPath + '/Home/AccessDenied';
                        window.location.href = url;
                    }
                    else {
                        $("label#lblMessage").text(result.message);
                        $("ul#message").css("visibility", "visible");
                        if (result.data) {
                            $("select#ddlPolicy").fillSelect(result.data);
                            $("select#ddlPolicy").val(result.selected);
                        }
                    }
                }
            });
        }



    });
});

//Text area max length 

function imposeMaxLength(obj, maxLen) {
    return (obj.value.length <= maxLen);
}

