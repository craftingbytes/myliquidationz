﻿var lastSel;
var inEditMode = false;
var _Checked;

$(document).ready(
function () {


    $("#jqGridRate").jqGrid({
        url: '/SpecificStateRate/GetSpecificRates',
        datatype: 'json',
        mtype: 'GET',

        colNames: ['State', 'Urban', 'Rural', 'Metro', 'NonMetro', 'Actions'],
        colModel: [
                  { name: 'State', index: 'State', width: 125, editable: true, sortable: false, editrules: { required: true }, edittype: 'select', editoptions: { size: 30, maxlength: 125} },
                  { name: 'Urban', index: 'Urban', width: 100, editable: true, sortable: false, editoptions: { maxlength: 50} },
                  { name: 'Rural', index: 'Rural', width: 100, editable: true, sortable: false, editoptions: { maxlength: 50} },
                  { name: 'Metro', index: 'Metro', width: 100, editable: true, sortable: false, editoptions: { maxlength: 50} },
                  { name: 'NonMetro', index: 'NonMetro', width: 100, editable: true, sortable: false, editoptions: { maxlength: 50} },
                  { name: 'Actions', index: 'Actions', width: 50, editable: false, sortable: false, align: 'center', formatter: ActionsFormatter }
      ],
        rowNum: 0,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 230,
        editurl: "/SpecificStateRate/PerformAction",
        gridComplete: function () { LoadDropdownsData(); }
    });

    $("#jqGridRate").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });



    $("#btnAdd").button().click(function () {
        if (inEditMode == false) {
            inEditMode = true;
            var emptyData = [
		            { State: "", Urban: "", Rural: "", Metro: "", NonMetro: "" }
		        ];
            lastSel = "0";
            $("#jqGridRate").addRowData("0", emptyData[0]);
            editRow(lastSel);
        } else {
            alert('There is already an item opened in edit mode. Please save or cancel it first.');
        }

    });

    $("#btnSearch").click(function () {

        var state = $("#StateId").val();
        jQuery("#jqGridRate").setGridParam({ postData: { State: state }, page: 1 }).trigger("reloadGrid");

    });

}
);



function LoadDropdownsData(elem) {
    $.ajax({
        url: '/SpecificStateRate/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGridRate').setColProp('State', { editoptions: { value: result.states} });
        }
    });

}



function ActionsFormatter(cellvalue, options, rowdata) {
    var rowId = options.rowId;
    var actionsHtml = "<div id='ED_Actions_" + rowId + "'>";
    actionsHtml += "<img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:editRow(\"" + rowId + "\")' style='border:none;' class='edit'/>";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete' title='Delete' onclick='javascript:deleteRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    actionsHtml += "<div id='SC_Actions_" + rowId + "' style='display:none;'>";
    actionsHtml += "<img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save' title='Save' onclick='javascript:saveRow(\"" + rowId + "\")' style='border:none;' />";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/cancel.png' style='cursor:hand;' alt='Cancel' title='Cancel' onclick='javascript:cancelRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    return actionsHtml;
}


function editRow(rowId) {
    if (rowId && rowId !== lastSel) {
        cancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_Actions_' + rowId).css({ display: "none" });
    $('#SC_Actions_' + rowId).css({ display: "block" });
    $('#jqGridRate').editRow(rowId);
    inEditMode = true;
}

function cancelRow(rowId) {
    $('#ED_Actions_' + rowId).css({ display: "block" });
    $('#SC_Actions_' + rowId).css({ display: "none" });

    $.fn.fmatter.rowactions(rowId, 'jqGridRate', 'cancel', false);
    if (rowId == "0") {
        $("#jqGridRate").delRowData(rowId);
    }
    inEditMode = false;
}

function deleteRow(rowId) {
    $("#jqGridRate").delGridRow(rowId, { reloadAfterSubmit: false, url: 'Delete' });
}

function saveRow(rowId) {
    $('#jqGridRate').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(result) {
    $('#ED_Action_' + lastSel).css({ display: "block" });
    $('#SC_Action_' + lastSel).css({ display: "none" });
    $('#jqGridRate').trigger("reloadGrid");
    inEditMode = false;
    return false;
}

function saveCallback(rowId, result) {
    if (result.responseText != "failture" && parseInt(result.responseText).toString() != 'NaN') {
        if (rowId == "0") {
            var rowData = $("#jqGridRate").getRowData(rowId);
            $("#jqGridRate").delRowData(rowId);
            $("#jqGridRate").addRowData(result.responseText, rowData);
        }
    }
}

function Check(value, colName) {
    _Checked = true;
    if (colName == "State") {
        if (value == null || value == "") {
            _Checked = false;
            return [false, "State: Field is required"];
        }
    }
    return [true, ""];
}