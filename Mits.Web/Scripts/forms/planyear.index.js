﻿var lastSel;
var inEditMode = false;
var _Checked;

$(document).ready(
function () {


    $("#jqGrid").jqGrid({
        url: '/PlanYear/GetPlanYears',
        datatype: 'json',
        mtype: 'GET',

        colNames: ['State', 'PlanYear', 'StartDate', 'EndDate', 'Actions'],
        colModel: [
                  { name: 'State', index: 'State', width: 125, editable: true, sortable: false, editrules: { required: true }, edittype: 'select' },
                  { name: 'PlanYear', index: 'PlanYear', width: 100, editable: true, sortable: false, editoptions: { maxlength: 50} },
                  { name: 'StartDate', index: 'StartDate', sortable: false, editable: true, datefmt: 'M/d/yyyy', editrules: { required: true, date: true },
                      editoptions: {
                          dataInit: function (elem) { $(elem).datepicker({ buttonImage: '../../Content/images/calendar.gif', altFormat: 'd/M/yyyy', buttonImageOnly: true }); }
                      }
                  },
                  { name: 'EndDate', index: 'EndDate', sortable: false, editable: true, datefmt: 'M/d/yyyy', editrules: { required: true, date: true },
                      editoptions: {
                          dataInit: function (elem) { $(elem).datepicker({ buttonImage: '../../Content/images/calendar.gif', altFormat: 'd/M/yyyy', buttonImageOnly: true }); }
                      }
                  },
                  { name: 'Actions', index: 'Actions', width: 50, editable: false, sortable: false, align: 'center', formatter: ActionsFormatter }
      ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 230,
        editurl: "/PlanYear/PerformAction"
        ,
        gridComplete: function () { LoadDropdownsData(); }
    });

    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });



    $("#btnAdd").button().click(function () {
        cancelRow(lastSel);
        lastSel = 0; // new row
        $("#jqGrid").jqGrid('editGridRow', "new", { height: 200, width: 300, closeAfterAdd: true, beforeShowForm: BeforeShowFrom, beforeSubmit: BeforeSubmitFrom, afterSubmit: AfterSaveRow });
    });

    function BeforeShowFrom(formid) {
        $(formid).find('select').css('width', '176px');
        $(formid).find('input').css('width', '170px');
    }

    function BeforeSubmitFrom(param1, form) {
        return [true, ""];
    }

    $("#btnSearch").click(function () {

        var state = $("#StateId").val();
        jQuery("#jqGrid").setGridParam({ postData: { State: state }, page: 1 }).trigger("reloadGrid");

    });

}
);



function LoadDropdownsData(elem) {
    $.ajax({
        url: '/PlanYear/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('State', { editoptions: { value: result.states} });
        }
    });

}



function ActionsFormatter(cellvalue, options, rowdata) {
    var rowId = options.rowId;
    var actionsHtml = "<div id='ED_Actions_" + rowId + "'>";
    actionsHtml += "<img src='../../Content/images/icon/edit.png' style='cursor:hand;' alt='Edit' title='Edit' onclick='javascript:editRow(\"" + rowId + "\")' style='border:none;' class='edit'/>";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete' title='Delete' onclick='javascript:deleteRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    actionsHtml += "<div id='SC_Actions_" + rowId + "' style='display:none;'>";
    actionsHtml += "<img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save' title='Save' onclick='javascript:saveRow(\"" + rowId + "\")' style='border:none;' />";
    actionsHtml += "&nbsp;&nbsp;";
    actionsHtml += "<img src='../../Content/images/icon/cancel.png' style='cursor:hand;' alt='Cancel' title='Cancel' onclick='javascript:cancelRow(\"" + rowId + "\")' style='border:none;'/>";
    actionsHtml += "</div>";
    return actionsHtml;
}


function editRow(rowId) {
    if (rowId && rowId !== lastSel) {
        cancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_Actions_' + rowId).css({ display: "none" });
    $('#SC_Actions_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
    inEditMode = true;
}

function cancelRow(rowId) {
    $('#ED_Actions_' + rowId).css({ display: "block" });
    $('#SC_Actions_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
    //$.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    if (rowId == "0") {
        $("#jqGrid").delRowData(rowId);
    }
    inEditMode = false;
}

function deleteRow(rowId) {
    $("#jqGrid").delGridRow(rowId, { reloadAfterSubmit: false, url: 'Delete' });
}

function saveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(result) {
    $('#ED_Action_' + lastSel).css({ display: "block" });
    $('#SC_Action_' + lastSel).css({ display: "none" });
    $('#jqGrid').trigger("reloadGrid");
    inEditMode = false;
    return false;
}

function saveCallback(rowId, result) {
    if (result.responseText != "failture" && parseInt(result.responseText).toString() != 'NaN') {
        if (rowId == "0") {
            var rowData = $("#jqGrid").getRowData(rowId);
            $("#jqGrid").delRowData(rowId);
            $("#jqGrid").addRowData(result.responseText, rowData);
        }
    }
}

function Check(value, colName) {
    _Checked = true;
    if (colName == "State") {
        if (value == null || value == "") {
            _Checked = false;
            return [false, "State: Field is required"];
        }
    }
    return [true, ""];
}