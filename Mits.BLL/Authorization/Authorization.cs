﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Mits.Common;
using Mits.BLL.Authorization;
using System.IO;
using Mits.DAL.EntityModels;

namespace Mits.BLL.Authorization
{
    public class PermissionNode
    {
        public PermissionNode Parent { get; set; }
        public List<PermissionNode> Children { get; set; }
        public Permission RolePermission { get; set; }
        public Permission AffiliatePermission { get; set; }
        public Area Area { get; set; }
        public bool Leaf { get; set; }
        public int Level { get; set; }

        public PermissionNode()
        {
        }
    }

    public static class Authorization
    {
        private static Logger mitsLogger = new Logger(typeof(Authorization));

        public static Menu GetAuthorizationMenu(ISessionCookieValues sessionValues)
        {
            Menu menu = new Menu();

            List<PermissionNode> rootItems = new List<PermissionNode>();
            MITSService<Area> areaService = new MITSService<Area>();
            List<Area> areaList = areaService.GetAll().ToList();
            MITSService<Permission> permissionService = new MITSService<Permission>();
            int affiliateRoleId = sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            List<Permission> permissionList = permissionService.GetAll(x => x.RoleId == affiliateRoleId).ToList();
            var rootAreaNodes = (from m in areaList
                                 where !m.ParentId.HasValue && m.IsMenu.Value
                                 select m).OrderBy(x => x.Order);
            foreach (Area rootAreaNode in rootAreaNodes)
            {
                PermissionNode node = new PermissionNode();
                node.Area = rootAreaNode;
                var permission = (from p in permissionList
                                  where p.AreaId == rootAreaNode.Id
                                  select p).ToList();
                if (permission.Count > 0)
                {
                    node.RolePermission = permission[0];
                }
                var children = GetChildMenuItems(areaList, permissionList, node);
                if (children.Count > 0)
                {
                    node.Children = children;
                    rootItems.Add(node);
                }
                else if (CanShowPermission(node.RolePermission))
                {
                    rootItems.Add(node);
                }
            }
            FilleMenuWithNodes(sessionValues, menu, rootItems);
            return menu;
        }

        public static AccessRights GetRights(Int32 roleID, Mits.Common.Constants.AreaName areaName)
        {
            try
            {
                AccessRights _accessRights = null;
                string _areaName = areaName.ToString();
                Area _area = new MITSService<Area>().GetSingle(X => X.Name == _areaName);
                if (_area != null)
                {
                    Permission _permission = new MITSService<Permission>().GetSingle(X => X.RoleId == roleID && X.AreaId == _area.Id);
                    if (_permission != null)
                    {
                        _accessRights = new AccessRights();
                        _accessRights.Read = _permission.Read;
                        _accessRights.Add = _permission.Add;
                        _accessRights.Update = _permission.Update;
                        _accessRights.Delete = _permission.Delete;
                    }
                }
                return _accessRights;
            }
            catch { throw; }
        }

        public static AccessRights GetRights(Int32 roleID, string areaName)
        {
            try
            {
                AccessRights _accessRights = null;
                string _areaName = areaName.ToString();
                Area _area = new MITSService<Area>().GetSingle(X => X.Name == _areaName);
                if (_area != null)
                {
                    Permission _permission = new MITSService<Permission>().GetSingle(X => X.RoleId == roleID && X.AreaId == _area.Id);
                    if (_permission != null)
                    {
                        _accessRights = new AccessRights();
                        _accessRights.Read = _permission.Read;
                        _accessRights.Add = _permission.Add;
                        _accessRights.Update = _permission.Update;
                        _accessRights.Delete = _permission.Delete;
                    }
                }
                return _accessRights;
            }
            catch { throw; }
        }

        public static int CreateRole(ISessionCookieValues sessionValues, string roleName, List<Permission> permissionList)
        {
            try
            {
                //SessionParameters.AffiliateId
                Role role = new Role();
                role.RoleName = roleName;
                role.CreateByAffiliateId = sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

                MITSService<Role> roleService = new MITSService<Role>();
                roleService.MitsEntities.Roles.Add(role);

                MITSService<Permission> permissionService = new MITSService<Permission>();
                foreach (Permission permission in permissionList)
                {
                    permission.Role = role;
                    permissionService.MitsEntities.Permissions.Add(permission);
                }
                permissionService.MitsEntities.SaveChanges();
                return role.Id;
            }
            catch (Exception e)
            {
                return -1;
            }

        }

        public static int UpdateRole(ISessionCookieValues sessionValues, int roleId, string roleName, List<Permission> permissionList)
        {
            try
            {
                MITSService<Role> roleService = new MITSService<Role>();
                Role role = roleService.GetSingle(x => x.Id == roleId);
                if (role == null)
                {
                    return -1;
                }
                role.RoleName = roleName;
                MITSService<Permission> permissionService = new MITSService<Permission>();
                var oldPermissions = permissionService.GetAll(x => x.RoleId == roleId);
                foreach (Permission permission in oldPermissions)
                {
                    permissionService.MitsEntities.Permissions.Remove(permission);
                }
                foreach (Permission permission in permissionList)
                {
                    permission.RoleId = roleId;
                    permissionService.MitsEntities.Permissions.Add(permission);
                }
                permissionService.MitsEntities.SaveChanges();
                mitsLogger.Info(sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactName) + " updated the role:" + roleName);
                return roleId;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public static List<PermissionNode> GetPermissions(ISessionCookieValues sessionValues, Int32? roleId)
        {
            try
            {
                List<PermissionNode> permissions = new List<PermissionNode>();
                List<PermissionNode> rootNodes = GetPermissionTree(sessionValues, roleId);
                FillPermissionListWithNodes(permissions, rootNodes);
                return permissions;
            }
            catch { throw; }
        }

        private static List<PermissionNode> GetPermissionTree(ISessionCookieValues sessionValues, Int32? roleId)
        {
            List<PermissionNode> rootPermissionNodes = new List<PermissionNode>();

            MITSService<Area> areaService = new MITSService<Area>();
            List<Area> areaList = areaService.GetAll().ToList();
            MITSService<Permission> permissionService = new MITSService<Permission>();
            int _roleId = roleId.HasValue ? roleId.Value : 0;
            List<Permission> permissionList = permissionService.GetAll(x => x.RoleId == _roleId).ToList();

            List<Permission> affiliatePermissionList = null;
            bool allowShowAllPermissions = false;
            if (sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) == (int)Constants.AffiliateType.Eworld)
            {
                allowShowAllPermissions = true;
            }
            else
            {
                int affiliateId = sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                MITSService<AffiliateRole> affiliateRoleService = new MITSService<AffiliateRole>();
                AffiliateRole affiliateRole = affiliateRoleService.GetSingle(x => x.AffiliateId == affiliateId);
                Role affRole = null;
                if (affiliateRole == null)
                {
                    // Get affiliate default role
                    MITSService<Role> roleService = new MITSService<Role>();
                    int affiliateTypeId = sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                    affRole = roleService.GetSingle(x => x.AffiliateTypeId == affiliateTypeId);
                }
                else
                {
                    // Use the specificate role for the affiliate
                    affRole = affiliateRole.Role;
                }
                affiliatePermissionList = permissionService.GetAll(x => x.RoleId == affRole.Id).ToList();
            }

            var rootAreaNodes = (from m in areaList
                                 where !m.ParentId.HasValue
                                 select m).OrderBy(x => x.Order);
            foreach (Area rootAreaNode in rootAreaNodes)
            {
                PermissionNode node = new PermissionNode();
                node.Area = rootAreaNode;
                node.Level = 0;
                node.Leaf = false;
                var permission = (from p in permissionList
                                  where p.AreaId == rootAreaNode.Id
                                  select p).ToList();
                if (permission.Count > 0)
                {
                    node.RolePermission = permission[0];
                }
                if (allowShowAllPermissions)
                {
                    Permission p = new Permission();
                    p.Read = true;
                    p.Add = true;
                    p.Update = true;
                    p.Delete = true;
                    node.AffiliatePermission = p;
                }
                else
                {
                    var _permission = (from p in affiliatePermissionList
                                       where p.AreaId == rootAreaNode.Id
                                       select p).ToList();
                    if (_permission.Count > 0)
                    {
                        node.AffiliatePermission = _permission[0];
                    }
                }
                var children = GetChildPermissionNodes(areaList, permissionList, allowShowAllPermissions, affiliatePermissionList, node);
                if (children.Count == 0)
                {
                    node.Leaf = true;
                    if (rootAreaNode.IsArea.HasValue && rootAreaNode.IsArea.Value && (allowShowAllPermissions || CanShowPermission(node.AffiliatePermission)))
                    {
                        rootPermissionNodes.Add(node);
                    }
                }
                else
                {
                    node.Children = children;
                    rootPermissionNodes.Add(node);
                }
            }

            return rootPermissionNodes;
        }

        private static List<PermissionNode> GetChildPermissionNodes(List<Area> areaList, List<Permission> permissionList, bool allowShowAllPermissions, List<Permission> affiliatePermissionList, PermissionNode parent)
        {
            List<PermissionNode> childNodes = new List<PermissionNode>();
            if (parent != null && parent.Area != null)
            {
                int parentAreaId = parent.Area.Id;
                var childAreaNodes = (from m in areaList
                                      where m.ParentId == parentAreaId
                                      select m).OrderBy(x => x.Order);
                foreach (Area childAreaNode in childAreaNodes)
                {
                    PermissionNode node = new PermissionNode();
                    node.Area = childAreaNode;
                    node.Level = parent.Level + 1;
                    node.Parent = parent;
                    node.Leaf = false;
                    var permission = (from p in permissionList
                                      where p.AreaId == childAreaNode.Id
                                      select p).ToList();
                    if (permission.Count > 0)
                    {
                        node.RolePermission = permission[0];
                    }
                    if (allowShowAllPermissions)
                    {
                        Permission p = new Permission();
                        p.Read = true;
                        p.Add = true;
                        p.Update = true;
                        p.Delete = true;
                        node.AffiliatePermission = p;
                    }
                    else
                    {
                        var _permission = (from p in affiliatePermissionList
                                           where p.AreaId == childAreaNode.Id
                                           select p).ToList();
                        if (_permission.Count > 0)
                        {
                            node.AffiliatePermission = _permission[0];
                        }
                    }
                    var children = GetChildPermissionNodes(areaList, permissionList, allowShowAllPermissions, affiliatePermissionList, node);
                    if (children.Count == 0)
                    {
                        node.Leaf = true;
                        if (childAreaNode.IsArea.HasValue && childAreaNode.IsArea.Value && (allowShowAllPermissions || CanShowPermission(node.AffiliatePermission)))
                        {
                            childNodes.Add(node);
                        }
                    }
                    else
                    {
                        node.Children = children;
                        childNodes.Add(node);
                    }
                }
            }
            return childNodes;
        }

        private static List<PermissionNode> GetChildMenuItems(List<Area> areaList, List<Permission> permissionList, PermissionNode parent)
        {
            List<PermissionNode> childItems = new List<PermissionNode>();
            if (parent != null && parent.Area != null)
            {
                int parentAreaId = parent.Area.Id;
                var childAreaNodes = (from m in areaList
                                      where m.ParentId == parentAreaId && m.IsMenu.Value
                                      select m).OrderBy(x => x.Order);
                foreach (Area childAreaNode in childAreaNodes)
                {
                    PermissionNode node = new PermissionNode();
                    node.Area = childAreaNode;
                    node.Parent = parent;
                    if (parent.Area.Name == Constants.AreaName.DocumentLib.ToString())
                    {
                        if (parent.RolePermission == null)
                        {
                            continue;
                        }
                        if (parent.RolePermission.Read.HasValue && parent.RolePermission.Read.Value && childAreaNode.Name == "DocumentList")
                        {
                            childItems.Add(node);
                        }
                        if (parent.RolePermission.Add.HasValue && parent.RolePermission.Add.Value && childAreaNode.Name == "DocumentUpload")
                        {
                            childItems.Add(node);
                        }
                        continue;
                    }
                    if (parent.Area.Name == Constants.AreaName.Event.ToString())
                    {
                        if (parent.RolePermission == null)
                        {
                            continue;
                        }
                        if (parent.RolePermission.Read.HasValue && parent.RolePermission.Read.Value && childAreaNode.Name == "ViewEvents")
                        {
                            childItems.Add(node);
                        }
                        if (parent.RolePermission.Add.HasValue && parent.RolePermission.Add.Value && childAreaNode.Name == "CreateEvent")
                        {
                            childItems.Add(node);
                        }
                        continue;
                    }
                    var permission = (from p in permissionList
                                      where p.AreaId == childAreaNode.Id
                                      select p).ToList();
                    if (permission.Count > 0)
                    {
                        node.RolePermission = permission[0];
                    }
                    var children = GetChildMenuItems(areaList, permissionList, node);
                    if (children.Count == 0)
                    {
                        //if (SessionParameters.AffiliateRoleId == (int)Constants.AffiliateContactRole.Administrator && childAreaNode.Name == Constants.AreaName.Authorization.ToString())
                        //{
                        //    childItems.Add(node);
                        //}
                        //else 
                        if (childAreaNode.Name == "ChangePassword")
                        {
                            childItems.Add(node);
                        }
                        else if (childAreaNode.IsMenu.HasValue && childAreaNode.IsMenu.Value && CanShowPermission(node.RolePermission))
                        {
                            childItems.Add(node);
                        }
                    }
                    else
                    {
                        node.Children = children;
                        childItems.Add(node);
                    }
                }
            }
            return childItems;
        }

        private static void FillPermissionListWithNodes(List<PermissionNode> permissions, List<PermissionNode> rootNodes)
        {
            if (rootNodes == null)
            {
                return;
            }
            foreach (PermissionNode node in rootNodes)
            {
                permissions.Add(node);
                FillPermissionListWithNodes(permissions, node.Children);
            }
        }

        private static void FilleMenuWithNodes(ISessionCookieValues sessionValues, Menu parent, List<PermissionNode> nodes)
        {
            List<Menu> childMenuItems = parent.MenuItems;
            foreach(PermissionNode node in nodes)
            {
                Menu childMenuItem = new Menu();
                Area area = node.Area;
                childMenuItem.Name = area.Name;
                childMenuItem.Caption = area.Caption;
                string url = area.URL;
                if (!string.IsNullOrEmpty(url))
                {
                    if (url.StartsWith("javascript"))
                    {
                        childMenuItem.OnClick = url;
                    }
                    else
                    {
                        childMenuItem.URL = url;
                    }
                }
                childMenuItem.Show = true;
                if (sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId) == (int)Constants.AffiliateType.Eworld)
                {
                    if (area.Name == Constants.AreaName.EntityContact.ToString() ||
                        area.Name == Constants.AreaName.ContractRate.ToString() ||
                        area.Name == Constants.AreaName.Target.ToString() ||
                        area.Name == Constants.AreaName.EntityState.ToString() ||
                        area.Name == Constants.AreaName.Map.ToString())
                    {
                        childMenuItem.Show = false;
                    }

                }
                if (childMenuItem.Show)
                {
                    childMenuItems.Add(childMenuItem);
                }
                if (node.Children != null && node.Children.Count > 0)
                {
                    FilleMenuWithNodes(sessionValues, childMenuItem, node.Children);
                }
            }
        }

        private static bool CanShowPermission(Permission permission)
        {
            if (permission != null)
            {
                if (permission.Read.HasValue && permission.Read.Value)
                {
                    return true;
                }
                if (permission.Add.HasValue && permission.Add.Value)
                {
                    return true;
                }
                if (permission.Update.HasValue && permission.Update.Value)
                {
                    return true;
                }
                if (permission.Delete.HasValue && permission.Delete.Value)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
