﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mits.BLL
{
    public class UserInfo
    {

        private int userAffiliateID = 1;

        public int UserAffiliateID
        {
            get { return userAffiliateID; }
            set { userAffiliateID = value; }
        }

        private int userInvoiceAffiliateID = 0;

        public int UserInvoiceAffiliateID
        {
            get { return userInvoiceAffiliateID; }
            set { userInvoiceAffiliateID = value; }
        }

        private int userImpersonationAffiliateID = 0;

        public int UserImpersonationAffiliateID
        {
            get { return userImpersonationAffiliateID; }
            set { userImpersonationAffiliateID = value; }
        }

        private int userCompanyAffiliateID = 23;

        public int UserCompanyAffiliateID
        {
            get { return userCompanyAffiliateID; }
            set { userCompanyAffiliateID = value; }
        }
     
        

        //For Roles
        private bool isUserAdmin=true;

        public bool IsUserAdmin
        {
            get { return isUserAdmin; }
            set { isUserAdmin = value; }
        }
        private bool isUserSuper = true;

        public bool IsUserSuper
        {
            get { return isUserSuper; }
            set { isUserSuper = value; }
        }
        private bool isOEM = false;

        public bool IsOEM
        {
            get { return isOEM; }
            set { isOEM = value; }
        }
        private bool isProcessor = false;

        public bool IsProcessor
        {
            get { return isProcessor; }
            set { isProcessor = value; }
        }
        private bool isCollector = false;

        public bool IsCollector
        {
            get { return isCollector; }
            set { isCollector = value; }
        }
    }
}
