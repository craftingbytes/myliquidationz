﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using Mits.DAL.EntityModels;

namespace Mits.BLL
{
    public class MapXMLHandler
    {
        public static void CreateCustomerMapXMLFile()
        {
            StringBuilder strXML = new StringBuilder();
            strXML.Append("<map borderColor='FFFFFF' connectorColor='000000' fillAlpha='70' hoverColor='FFFFFF' showBevel='0'>");
            strXML.Append("<colorRange>");
            strXML.Append("<color minValue='10' maxValue='19' displayValue='Law Introduced/Not Passed' color='039f99' />");
            strXML.Append("<color minValue='20' maxValue='29' displayValue='No Law Introduced' color='FFCC33' />");
            strXML.Append("<color minValue='30' maxValue='39' displayValue='Law Passed/Effective Soon' color='069F06' />");
            strXML.Append("<color minValue='40' maxValue='49' displayValue='Law in Effect' color='CC0001' />");
            strXML.Append("<color minValue='50' maxValue='59' displayValue='Voluntary/Program Running' color='9966FF' />");
            strXML.Append("</colorRange>");
            strXML.Append("<data>");
            strXML.Append("</data>");
            strXML.Append("<styles>");
            strXML.Append("<definition>");
            strXML.Append("<style type='animation' name='animX' param='_xscale' start='0' duration='1' />");
            strXML.Append("<style type='animation' name='animY' param='_yscale' start='0' duration='1' />");
            strXML.Append("</definition>");
            strXML.Append("<application>");
            strXML.Append("<apply toObject='PLOT' styles='animX,animY' />");
            strXML.Append("</application>");
            strXML.Append("</styles>");
            strXML.Append("</map>");

            string _sourceFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Helpers/StatesData.xml");
            FileInfo fi = new FileInfo(_sourceFilePath);
            string _destinationFilePath = fi.Directory.FullName + "\\" + "CustomerIndex.xml";
            XmlTextReader reader = new XmlTextReader(_sourceFilePath);
            XmlDocument _xmlDocumnet = new XmlDocument();
            _xmlDocumnet.LoadXml(strXML.ToString());
            XmlElement _rootElement = _xmlDocumnet.DocumentElement;
            XmlNode _dataNode = _rootElement.SelectSingleNode("data");

            MITSEntities mitsEntities = new MITSEntities();
            var mapEntities = (from state in mitsEntities.States
                               join guideline in mitsEntities.StateGuidelines
                               on state.Id equals guideline.StateId into joinTemp
                               from temp in joinTemp.DefaultIfEmpty()
                               select new { state.Id, state.Abbreviation, state.Name, temp.CoveredDevices, temp.Updates, temp.LawState }).ToList();
            foreach (var entity in mapEntities)
            {
                XmlNode _entity = _xmlDocumnet.CreateElement("entity");
                XmlAttribute id = _xmlDocumnet.CreateAttribute("id");
                id.Value = entity.Abbreviation;
                _entity.Attributes.Append(id);

                XmlAttribute toolText = _xmlDocumnet.CreateAttribute("toolText");
                String sToolText =  "State name: " + entity.Name;
                if (!string.IsNullOrEmpty(entity.CoveredDevices))
                {
                    string devices = entity.CoveredDevices.Replace("’", "'");
                    sToolText += "\n\nCovered Devices: \n" + WrapLine(devices, 125);
                }
                if (!string.IsNullOrEmpty(entity.Updates) && entity.Updates.Trim().Length > 0)
                {
                    string updates = WrapLine(entity.Updates, 125);
                    sToolText += "\n\nUpdates: \n" + updates;
                }
                toolText.Value = sToolText;
                _entity.Attributes.Append(toolText);

                XmlAttribute value = _xmlDocumnet.CreateAttribute("value");
                value.Value = entity.LawState == null ? "0" : entity.LawState.ToString();
                _entity.Attributes.Append(value);

                XmlAttribute link = _xmlDocumnet.CreateAttribute("link");
                link.Value = "../StateGuidelines/State/" + entity.Abbreviation;
                _entity.Attributes.Append(link);
                _dataNode.AppendChild(_entity);
            }
            _xmlDocumnet.Save(_destinationFilePath);
        }

        public static void CreateCollectionEventMapXmlFile()
        {
            StringBuilder strXML = new StringBuilder();
            strXML.Append("<map borderColor='FFFFFF' connectorColor='000000' fillAlpha='70' hoverColor='FFFFFF' showBevel='0'>");
            strXML.Append("<colorRange>");
            strXML.Append("<color minValue='10' maxValue='19' displayValue='Has Collection Event' color='CC0001' />");
            strXML.Append("<color minValue='20' maxValue='29' displayValue='No Collection Event' color='039f99' />");
            strXML.Append("</colorRange>");
            strXML.Append("<data>");
            strXML.Append("</data>");
            strXML.Append("<styles>");
            strXML.Append("<definition>");
            strXML.Append("<style type='animation' name='animX' param='_xscale' start='0' duration='1' />");
            strXML.Append("<style type='animation' name='animY' param='_yscale' start='0' duration='1' />");
            strXML.Append("</definition>");
            strXML.Append("<application>");
            strXML.Append("<apply toObject='PLOT' styles='animX,animY' />");
            strXML.Append("</application>");
            strXML.Append("</styles>");
            strXML.Append("</map>");

            string _sourceFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Helpers/StatesData.xml");
            FileInfo fi = new FileInfo(_sourceFilePath);
            string _destinationFilePath = fi.Directory.FullName + "\\" + "CollectionEventMap.xml";
            XmlTextReader reader = new XmlTextReader(_sourceFilePath);
            XmlDocument _xmlDocumnet = new XmlDocument();
            _xmlDocumnet.LoadXml(strXML.ToString());
            XmlElement _rootElement = _xmlDocumnet.DocumentElement;
            XmlNode _dataNode = _rootElement.SelectSingleNode("data");

            var states = new MITSService<State>().GetAll();
            var collectionEvents = new MITSService<CollectionEvent>().GetAll();

            foreach (var state in states)
            {
                var events = (from c in collectionEvents
                              where c.StateId == state.Id
                              select c).ToList();

                XmlNode _entity = _xmlDocumnet.CreateElement("entity");
                XmlAttribute id = _xmlDocumnet.CreateAttribute("id");
                id.Value = state.Abbreviation;
                _entity.Attributes.Append(id);

                XmlAttribute toolText = _xmlDocumnet.CreateAttribute("toolText");
                String sToolText = "State name: " + state.Name;
                foreach (var e in events)
                {
                    sToolText += "\n\nLocation: " + e.Address + ", " + e.City.Name + ", " + e.ZipCode + "\nDate: " + e.Hours;
                }
                toolText.Value = sToolText;
                _entity.Attributes.Append(toolText);

                XmlAttribute value = _xmlDocumnet.CreateAttribute("value");
                value.Value = events.Count > 0 ? "10" : "20";
                _entity.Attributes.Append(value);

                if (events.Count > 0)
                {
                    XmlAttribute link = _xmlDocumnet.CreateAttribute("link");
                    link.Value = "../CollectionEvent/Detail/" + state.Abbreviation;
                    _entity.Attributes.Append(link);
                }

                _dataNode.AppendChild(_entity);
            }
            _xmlDocumnet.Save(_destinationFilePath);
        }

        public static void CreateMapXMLFile(Int32 affiliateID, List<AffiliateTarget> _affiliateTargets)
        {
            string _sourceFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Helpers/StatesData.xml");
            FileInfo fi = new FileInfo(_sourceFilePath);
            string _destinationFilePath = fi.Directory.FullName + "\\" + affiliateID.ToString() + "_" + fi.Name;
            
            XmlTextReader reader = new XmlTextReader(_sourceFilePath);

            XmlDocument _xmlDocumnet = new XmlDocument();

            _xmlDocumnet.Load(_sourceFilePath);

            XmlElement _rootElement = _xmlDocumnet.DocumentElement;

            foreach (XmlNode _node in _rootElement.SelectNodes("data/entity"))
            {
                if (_affiliateTargets.Where(X => X.State.Abbreviation == _node.Attributes["id"].Value).Count() > 0)
                    if (_node.Attributes["toolText"] != null)
                        _node.Attributes["toolText"].Value = "";

                foreach (AffiliateTarget _a in _affiliateTargets.Where(X => X.State.Abbreviation == _node.Attributes["id"].Value))
                {
                    if (_node.Attributes["toolText"] != null)
                    {
                        _node.Attributes["value"].Value = "15";

                        if (_a.StartDate.HasValue)
                            _node.Attributes["toolText"].Value += " Start Date: " + _a.StartDate.Value.ToString("MM/dd/yyyy");

                        if (_a.EndDate.HasValue)
                            _node.Attributes["toolText"].Value += "\r\n End Date: " + _a.EndDate.Value.ToString("MM/dd/yyyy");

                        if (_a.State != null)
                            _node.Attributes["toolText"].Value += "\r\n State Name: " + _a.State.Name;

                        if (_a.Affiliate != null)
                            _node.Attributes["toolText"].Value += "\r\n Entity Name: " + _a.Affiliate.Name;

                        if (_a.ServiceType != null)
                            _node.Attributes["toolText"].Value += "\r\n Service Type: " + _a.ServiceType.Name;

                        if (_a.QuantityType != null)
                            _node.Attributes["toolText"].Value += "\r\n Unit of Measurement: " + _a.QuantityType.Name;

                        if (_a.Weight.HasValue)
                            _node.Attributes["toolText"].Value += "\r\n Obligation Weight : " + _a.Weight.Value.ToString();

                        if (_a.Weight.HasValue)
                            _node.Attributes["toolText"].Value += "\r\n Weight Processed : " + GetTargetWeightCollected(_a.Id).ToString();

                        _node.Attributes["toolText"].Value += "\n --------------------------------- \r\n";
                    }
                }

            }

            //foreach (AffiliateTarget _a in _affiliateTargets)
            //{
            //    XmlNode _node = _rootElement.SelectSingleNode("data/entity[@id='"+_a.State.Abbreviation +"']");
            //    if (_node != null && _node.Attributes["toolText"] != null)
            //    {
            //        _node.Attributes["value"].Value = "15";

            //        if (_a.State != null)
            //            _node.Attributes["toolText"].Value = " State Name: " + _a.State.Name;

            //        if (_a.Affiliate != null)
            //            _node.Attributes["toolText"].Value += "\r\n Entity Name: " + _a.Affiliate.Name;

            //        if (_a.ServiceType != null)
            //            _node.Attributes["toolText"].Value += "\r\n Service Type: " + _a.ServiceType.Name;

            //        if (_a.QuantityType != null)
            //            _node.Attributes["toolText"].Value += "\r\n Unit of Measurement: " + _a.QuantityType.Name;

            //        if (_a.Weight.HasValue )
            //            _node.Attributes["toolText"].Value += "\r\n Target Weight : " + _a.Weight.Value.ToString();

            //        if (_a.StartDate.HasValue)
            //            _node.Attributes["toolText"].Value += "\r\n Start Date: " + _a.StartDate.Value.ToString("MM/dd/yyyy");

            //        if (_a.EndDate.HasValue)
            //            _node.Attributes["toolText"].Value += "\r\n End Date: " + _a.EndDate.Value.ToString("MM/dd/yyyy");
            //    }
            //}
            _xmlDocumnet.Save(_destinationFilePath);
        }

        private static double GetTargetWeightCollected(int targetId)
        {
            AffiliateTarget target = new MITSService<AffiliateTarget>().GetSingle(x => x.Id == targetId);
            // making OEM's Invoice Items list
            List<InvoiceReconciliation> oemInvoiceItems = new List<InvoiceReconciliation>();

            IList<Invoice> oemInvoices = new MITSService<Invoice>().GetAll(x => x.AffiliateId == target.AffiliateId
                                                                                 && x.PlanYear == target.PlanYear  
                                                                                 && x.StateId == target.StateId);

            foreach (Invoice invoice in oemInvoices)
            {
                foreach (InvoiceReconciliation invoiceItem in invoice.InvoiceReconciliations)
                {
                    if (!oemInvoiceItems.Contains(invoiceItem))
                        oemInvoiceItems.Add(invoiceItem);
                }
            }
            double weightCollected = 0;
            // traverse OEM's Invoice Items list to calculate Weight Not Collected yet
            foreach (InvoiceReconciliation invoiceItem in oemInvoiceItems)
            {
                double reconciledWeight = 0;
                // getting entries from reconciliation table against an OEM invoice item
                //IList<InvoiceItemReconciliation> oemReconciledEntries = new MITSService<InvoiceItemReconciliation>().GetAll(x => x.ReconcileToInvoiceItemID == invoiceItem.Id);
                // calculating reconciled weight by adding up all the reconciliation entries against current OEM invoice item
                //foreach (InvoiceItemReconciliation reconciledEntry in oemReconciledEntries)
                //{
                //    reconciledWeight = reconciledWeight + (double)reconciledEntry.Quantity;
                //}
                reconciledWeight = (double)invoiceItem.Quantity;
                weightCollected += reconciledWeight;
            }
            return weightCollected;
        }

        private static string WrapLine(string str, int length)
        {
            if (string.IsNullOrEmpty(str) || str.Length <= length)
            {
                return str;
            }

            int index = str.LastIndexOf(" ", length);
            if (index < 0)
            {
                index = str.LastIndexOf(",", length);
            }
            string wrapStr;
            if (index > -1)
            {
                wrapStr = str.Substring(0, index + 1) + "\n";
                return wrapStr += WrapLine(str.Substring(index + 1), length);
            }
            return str;
        }
    }
}
