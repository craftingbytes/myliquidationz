﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;

namespace Mits.BLL
{
    //public class AffiliateService : ServiceBase<IAffiliateModel>, IAffiliateService
    public class MITSService<T>: IService<T> where T : class
    {

        private IRepository<T> _mitsRepository;
        MITSEntities _mitsEntities = null;
    
        public MITSService()
            : this(new MITSRepository<T>())
        {
            _mitsEntities = (MITSEntities)_mitsRepository.ObjectContext;
        }


        public MITSService(IRepository<T> repository)
        {
            _mitsRepository = repository;
            _mitsEntities = (MITSEntities)_mitsRepository.ObjectContext;
        }


        public T GetSingle(Expression<Func<T, bool>> whereCondition)
        {
            return _mitsRepository.GetSingle(whereCondition);
        }
       
        public void Add(T entity)
        {
            _mitsRepository.Add(entity);
        }
        public void Save(T entity)
        {
            _mitsRepository.Save(entity);
        }
        public void Delete(T entity)
        {
            _mitsRepository.Delete(entity);
        }

        public IList<T> GetAll(Expression<Func<T, bool>> whereCondition)
        {
            return _mitsRepository.GetAll(whereCondition);
        }

        public IList<T> GetAll()
        {
            return _mitsRepository.GetAll();
        }

        public IQueryable<T> GetQueryable(Expression<Func<T, bool>> whereCondition)
        {
            return _mitsRepository.GetQueryable(whereCondition);
        }

        public long Count(Expression<Func<T, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public long Count()
        {
            throw new NotImplementedException();
        }

        public MITSEntities MitsEntities
        {
            get { return _mitsEntities; }
            set { _mitsEntities = value; }
        }

      
    }
}
