﻿
using Mits.Common;
using Mits.DAL;
using Mits.DAL.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Mits.BLL.BusinessObjects
{
    public class ChangePasswordBO
    {
       MITSRepository<AffiliateContact> mitsAffiliateContactRepo = new MITSRepository<AffiliateContact>();
        ISessionCookieValues _sessionValues = null;

        public ChangePasswordBO(ISessionCookieValues sessionValues)
        {
            _sessionValues = sessionValues;
        }

        private bool SendEmail(AffiliateContact _aff)
        {
            try
            {
                Dictionary<object, object> _dictioanry = new Dictionary<object, object>();
               
                StringBuilder message = new StringBuilder();
                message.Append(Utility.BodyStart());
                message.Append(Utility.AddTextLine("Dear User,"));
                message.Append(Utility.AddBreak(2));
                message.Append(Utility.AddTextLine("The password to your MITS account has been changed. If you did not change your password, then please contact our Customer Support immediately."));
                message.Append(Utility.AddBreak(2));
                message.Append(Utility.AddTextLine("Thank you,"));
                message.Append(Utility.AddBreak(1));
                message.Append(Utility.AddTextLine("E-World Online"));
                message.Append(Utility.AddBreak(2));
                message.Append(Utility.AddTextLine("Manufacturer's Interstate Takeback System (MITS)"));
                message.Append(Utility.BodyEnd());
                if ((EmailHelper.SendMail("Account Password Changed", message.ToString(), new string[] { _aff.Email }, null, null) == true))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IList<SecurityQuestion> GetAllQuestions()
        {
            IList<SecurityQuestion> _questions = null;
            _questions = new MITSService<SecurityQuestion>().GetAll();
            IOrderedEnumerable<SecurityQuestion> _questionsSorted = _questions.OrderBy(x => x.Question);
            SecurityQuestion _question = new SecurityQuestion();

            IList<SecurityQuestion> questionsList = _questionsSorted.ToList();
            _question.Id = 0;
            _question.Question = "Select";
            questionsList.Insert(0, _question);
            return questionsList;

        }
        public string AddCriteria1(string oldPassword, string newPassword, string confirmPassword)
        {
            string _returnValue;
            if (ConfigHelper.ComputePwdHash == "true")
            {
                oldPassword = Utility.CreateHash(oldPassword, false);
                newPassword = Utility.CreateHash(newPassword, false);
            }

            int affiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
             AffiliateContact _affiliateContact = new MITSService<AffiliateContact>().GetSingle(t => t.Id == affiliateContactId);
               if (_affiliateContact.Password != oldPassword)
               {
                   _returnValue = "The old password is incorrect";
                   return _returnValue;
               }
               else if (_affiliateContact.Password == newPassword)
               {
                   _returnValue = "This password already Exists";
                   return _returnValue;
               }

               else
               {
                   _affiliateContact.Password = newPassword;
                   new MITSService<AffiliateContact>().Save(_affiliateContact);
                   _returnValue = "The password has been changed successfully";
                   return _returnValue;
               }           
        }

        public string AddCriteria2(string _answer, string _questionId)
        {
            string returnValue;
            bool success=false;
            int id = int.Parse(_questionId);
            int affiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
            AffiliateContact _aff = mitsAffiliateContactRepo.GetSingle(t => t.Id == affiliateContactId);
            if (_aff.SecurityQuestionId != id)
            {
                returnValue = "Security Question does not match with our record";
                return returnValue;
            }
            else if (_aff.Answer.ToLower() != _answer.ToLower())
            {
                returnValue = "Answer is not correct";
                return returnValue;
            }
            else
            {
                string password = RandomPassword.Generate(6);
                success = this.SendEmail(_aff);
                if (success == true)
                {
                    if (ConfigHelper.ComputePwdHash == "true")
                    {
                        password = Utility.CreateHash(password);
                    }
                    _aff.Password = password;
                    mitsAffiliateContactRepo.Save(_aff);
                    returnValue = "Your password has been changed and it has been sent at your mailing address";
                    return returnValue;
                }
                else
                {
                    returnValue = Constants.Messages.TryLater.ToString();
                    return returnValue;
                }
            }
        }

        public string AddCriteria3(string oldPassword, string newPassword, string confirmPassword, string questionId, string answer)
        {
            string _returnValue;
            bool success = false;

            //check the password question and answer
            int id = int.Parse(questionId);
            int affiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
            AffiliateContact _affiliateContact = new MITSService<AffiliateContact>().GetSingle(t => t.Id == affiliateContactId);
            if (id > 0)
            {
                SecurityQuestion question = new MITSService<SecurityQuestion>().GetSingle(q => q.Id == id);
                if (question != null && !string.IsNullOrEmpty(answer))
                {
                    _affiliateContact.SecurityQuestionId = id;
                    _affiliateContact.SecurityQuestion = question;
                    _affiliateContact.Answer = answer;
                }
                else
                {
                    _returnValue = "Security Question or Answer is incorrect";
                    return _returnValue;
                }
            }
            else
            {
                _returnValue = "Security Question is required";
                return _returnValue;
            }
            
            //check and new password
            if (ConfigHelper.ComputePwdHash == "true")
            {
                oldPassword = Utility.CreateHash(oldPassword, false);
                newPassword = Utility.CreateHash(newPassword, false);
            }

            if (_affiliateContact.Password != oldPassword)
            {
                _returnValue = "The old password is incorrect";
                return _returnValue;
            }
            else if (_affiliateContact.Password == newPassword)
            {
                _returnValue = "This password already Exists";
                return _returnValue;
            }

            else
            {
                success = this.SendEmail(_affiliateContact);
                if (success)
                {
                    _affiliateContact.Password = newPassword;
                    new MITSService<AffiliateContact>().Save(_affiliateContact);
                    _returnValue = "The password has been changed successfully";
                    return _returnValue;
                }
                else
                {
                    _returnValue = Constants.Messages.TryLater.ToString();
                    return _returnValue;
                }
            }
        }
    }
}
