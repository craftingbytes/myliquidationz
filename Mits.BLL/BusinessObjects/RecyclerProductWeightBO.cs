﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mits.DAL;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class RecyclerProductWeightBO
    {
        Logger mitsLogger = null;

        public RecyclerProductWeightBO()
        {
            mitsLogger = new Logger(this.GetType());
        }

        public RecyclerProductWeight GetProductWeight(int Id)
        {
            MITSService<RecyclerProductWeight> Service = new MITSService<RecyclerProductWeight>();
            var product = Service.GetSingle(x => x.Id == Id);            
            return product;
        }

        public Affiliate GetAffiliate(int Id)
        {
            MITSService<Affiliate> Service = new MITSService<Affiliate>();
            var Affiliate = Service.GetSingle(x => x.Id == Id);
            return Affiliate;
        }

        public IList<RecyclerProductWeight> GetProductWeightPaged(int pageIndex, int pageSize, int affiliateId, string searchFrom, string searchTo, out int totalRecords)
        {
            DateTime _From = default(DateTime);
            DateTime _To = default(DateTime);

            bool _isFrom = DateTime.TryParse(searchFrom, out _From);
            bool _isTo = DateTime.TryParse(searchTo, out _To);

            IList<RecyclerProductWeight> productList = new List<RecyclerProductWeight>();
            totalRecords = 0;

            if (!string.IsNullOrEmpty(searchFrom) && !_isFrom)
            {
                return productList;
            }
            if (!string.IsNullOrEmpty(searchTo) && !_isTo)
            {
                return productList;
            }

            if (string.IsNullOrEmpty(searchFrom) && string.IsNullOrEmpty(searchTo))
            {
                productList = new MITSService<RecyclerProductWeight>().GetAll(x => x.Affiliate.Id == affiliateId);
                productList = productList.OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
            }
            else
            {
                productList = new MITSService<RecyclerProductWeight>().GetAll(x => x.Affiliate.Id == affiliateId);
                if (_isFrom)
                {
                    string fromDate = _From.ToString("yyyy") + _From.ToString("MM");
                    productList = productList.Where(x => (x.Year + x.Month).CompareTo(fromDate) >= 0).ToList();
                }
                if (_isTo)
                {
                    string toDate = _To.ToString("yyyy") + _To.ToString("MM");
                    productList = productList.Where(x => (x.Year + x.Month).CompareTo(toDate) <= 0).ToList();
                }
            }
            if (productList != null)
            {
                totalRecords = productList.Count;
                return productList.OrderBy(x => x.Year).ThenBy(x => x.Month).Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }
            totalRecords = 0;
            return productList;
        }

        public List<Message> Add(RecyclerProductWeight ProductWeight)
        {
            List<Message> messages = new List<Message>();
            try
            {
                MITSService<RecyclerProductWeight> Service = new MITSService<RecyclerProductWeight>();
                int recycler = ProductWeight.RecyclerId;
                string year = ProductWeight.Year;
                string month = ProductWeight.Month;
                RecyclerProductWeight exists = Service.GetSingle(x => (x.RecyclerId == recycler && x.Year == year && x.Month == month));
                if (exists != null)
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.RecordExist));
                    return messages;
                }

                Service.Add(ProductWeight);
                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));

            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                mitsLogger.Error("Failed to create Product Weight", ex);
            }
            return messages;
        }

        public List<Message> Save(RecyclerProductWeight ProductWeight)
        {
            List<Message> messages = new List<Message>();
            try
            {
                MITSService<RecyclerProductWeight> Service = new MITSService<RecyclerProductWeight>();

                int id = ProductWeight.Id;
                RecyclerProductWeight update = Service.GetSingle(x => x.Id == id);
                update.TotalWeightReceived = ProductWeight.TotalWeightReceived;
                update.OnHand = ProductWeight.OnHand;
                update.CRTGlass = ProductWeight.CRTGlass;
                update.CircuitBoards = ProductWeight.CircuitBoards;
                update.Batteries = ProductWeight.Batteries;
                update.OtherMaterials = ProductWeight.OtherMaterials;

                Service.MitsEntities.SaveChanges();
                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                mitsLogger.Error("Failed to save Product Weight", ex);
            }
            return messages;
        }
    }
}
