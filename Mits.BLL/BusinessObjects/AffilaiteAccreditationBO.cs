﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
 
    public class AffiliateAccreditationBO
    {

        MITSRepository<AffiliateAccreditation> mitsAffiliateAccreditationRepo = null;
        Logger mitsLogger = null;
      
        public AffiliateAccreditationBO()
        {
            mitsAffiliateAccreditationRepo = new MITSRepository<AffiliateAccreditation>();
            mitsLogger = new Logger(this.GetType());
        }

        public IQueryable GetAllAffiliateAccrediatation()
        {
            IQueryable _affAccreditation = null;
            try
            {
                _affAccreditation= mitsAffiliateAccreditationRepo.GetAll() as IQueryable;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get entity accreditation list", ex);
            }
            return _affAccreditation;
        }

        #region Add AffiliateAccreditations

        public Dictionary<string,int> Add(string affiliate, string accreditations)
        {

            string[] _selectedAccreditations;
            bool _newEntry = true;
            bool _samePolicy=false;
            int _id = int.Parse(affiliate);
            
            //Return multiple values via Dictionary object
            Dictionary<string, int> returnDictionaryValue = new Dictionary<string, int>();
            returnDictionaryValue["selectedId"] = _id;
            returnDictionaryValue["duplicate"] = 0;
            
            _selectedAccreditations = accreditations.Split(',');
            try
            {
                AffiliateAccreditation _check = new MITSService<AffiliateAccreditation>().GetSingle(t => t.AffiliateId == _id);


                //Check for new entry

                if (_check == null)
                {
                    //New Entry of Affiliate Accreditations

                    foreach (var _accreditation in _selectedAccreditations)
                    {
                        AffiliateAccreditation _affiliateAccreditationObj = new AffiliateAccreditation();
                        _affiliateAccreditationObj.AffiliateId = _id;
                        _affiliateAccreditationObj.AccreditationTypeId = int.Parse(_accreditation);
                        _affiliateAccreditationObj.Active = true;
                        new MITSService<AffiliateAccreditation>().Add(_affiliateAccreditationObj);

                    }
                }

                //Old Entry

                else
                {
                    MITSService<AffiliateAccreditation> _saveAffiliateAccreditation = new MITSService<AffiliateAccreditation>();
                    IList<AffiliateAccreditation> _existingAccreditations = new MITSService<AffiliateAccreditation>().GetAll(t => t.AffiliateId == _check.AffiliateId & t.Active == true);

                    //Checks if the same policy is being submitted again then return else continue 

                    if (_existingAccreditations.Count == _selectedAccreditations.Length)
                    {

                        foreach (var _selectAccreditation in _selectedAccreditations)
                        {
                            _samePolicy = false;
                            foreach (AffiliateAccreditation _eAffAccreditation in _existingAccreditations)
                            {
                                if (int.Parse(_selectAccreditation) == _eAffAccreditation.AccreditationTypeId)
                                {

                                    _samePolicy = true;
                                }
                            }
                            if (_samePolicy == false)
                            {
                                break;
                            }
                        }
                    }
                    if (_samePolicy == true)
                    {
                        returnDictionaryValue["duplicate"] = 1;
                    }
                    else
                    {

                        //Make all accreditations of a particular affiliate inactive

                        IList<AffiliateAccreditation> _existingAffiliateAccreditations = new MITSService<AffiliateAccreditation>().GetAll(t => t.AffiliateId == _check.AffiliateId);

                        foreach (AffiliateAccreditation _accreditation in _existingAffiliateAccreditations)
                        {
                            _accreditation.Active = false;
                            _saveAffiliateAccreditation.Save(_accreditation);
                        }



                        foreach (var _selectAccreditation in _selectedAccreditations)
                        {
                            _newEntry = true;
                            foreach (AffiliateAccreditation _eAffAccreditation in _existingAffiliateAccreditations)
                            {
                                if (int.Parse(_selectAccreditation) == _eAffAccreditation.AccreditationTypeId)
                                {

                                    _eAffAccreditation.Active = true;
                                    _saveAffiliateAccreditation.Save(_eAffAccreditation);
                                    _newEntry = false;



                                }
                            }

                            if (_newEntry == true)
                            {
                                AffiliateAccreditation _affiliateAccreditationObj = new AffiliateAccreditation();
                                _affiliateAccreditationObj.AffiliateId = _id;
                                _affiliateAccreditationObj.AccreditationTypeId = int.Parse(_selectAccreditation);
                                _affiliateAccreditationObj.Active = true;
                                new MITSService<AffiliateAccreditation>().Add(_affiliateAccreditationObj);


                            }
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to add entity "+affiliate+" accreditations", ex);
            }
            return returnDictionaryValue;

        }

        #endregion


    }
}
