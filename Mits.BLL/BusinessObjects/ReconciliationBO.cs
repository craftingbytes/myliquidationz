﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System.Web.Mvc;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class ReconciliationBO
    {        
        public ReconciliationBO()
        {            
        }

        public IList<Affiliate> GetEntities()
        {
            string type = Constants.AffiliateType.OEM.ToString();
            IList<Affiliate> sortedData;
            IList<Affiliate> unsortedData = new MITSService<Affiliate>().GetAll(x => x.AffiliateType != null && x.AffiliateType.Type == type);
            IEnumerable<Affiliate> enumList = unsortedData.OrderBy(f => f.Name);
            try
            {
                sortedData = enumList.ToList();
            }
            catch (Exception ex)
            {
                sortedData = unsortedData;
            }
            Affiliate select = new Affiliate();
            select.Id = 0;
            select.Name = "Select";
            sortedData.Insert(0, select);
            return sortedData;
        }

        public IList<AffiliateTarget> GetEntityTargets(int oemid, int stateId, int planYear)
        {
            IList<AffiliateTarget> targets = null;
            if (oemid == 0){
                targets = new MITSService<AffiliateTarget>().GetAll(x => x.StateId == stateId && x.PlanYear == planYear && x.Affiliate.AffiliateTypeId == 1);
            }
            else {
                targets = new MITSService<AffiliateTarget>().GetAll(x => x.AffiliateId == oemid && x.StateId == stateId && x.PlanYear == planYear && x.Affiliate.AffiliateTypeId == 1);
            }

            return targets;
        }

        public decimal GetAvailableProcessedWeight(int InvoiceId)
        {
            IList<InvoiceItem> processorsInvoiceItems = new MITSService<InvoiceItem>().GetAll(x => x.InvoiceId == InvoiceId);

            decimal availableWeight = 0;
            decimal reconciledWeight = 0;
            foreach (InvoiceItem invoiceItem in processorsInvoiceItems)
            {
                availableWeight = availableWeight + (invoiceItem.Quantity.HasValue ? invoiceItem.Quantity.Value : 0);
                IList<InvoiceReconciliation> processerRecon = new MITSService<InvoiceReconciliation>().GetAll(x => x.ProcesserInvoiceItemId == invoiceItem.Id);
                foreach (InvoiceReconciliation recon in processerRecon)
                {
                    reconciledWeight = reconciledWeight + (recon.Quantity.HasValue ? recon.Quantity.Value : 0);
                }
            }
            return availableWeight - reconciledWeight;
        }

        public IList<Invoice> GetAvailableInvoices(int stateId,int planyear)
        {
            IList<Invoice> processorsInvoices = new MITSService<Invoice>().GetAll(x => x.Affiliate.AffiliateTypeId == 2 && x.StateId == stateId && x.PlanYear == planyear && x.Approved == true);
            return processorsInvoices;
        }

        public IList<int> GetOEMInvoiceId(int OEMId,int stateId,int planyear)
        {
           IList<Invoice> invoices = new MITSService<Invoice>().GetAll(x => x.AffiliateId == OEMId && x.StateId == stateId && x.PlanYear == planyear);
           IList<int> ids = new List<int>();
           foreach (Invoice inv in invoices)
           {
               ids.Add(inv.Id);
           }
           return ids;
        }

        public IList<ReconciledItem> GetReconcileHistory(int oemId)
        {
            IList<ReconciledItem> items = new List<ReconciledItem>();
            IList<InvoiceReconciliation> Recs = new MITSService<InvoiceReconciliation>().GetAll(x => x.Invoice.AffiliateId == oemId);

            var query = from r in Recs
                        group r by new { OEMInvoiceId = r.OEMInvoiceId, ReconcileDate = r.ReconcileDate }
                            into grp
                            select new { 
                             OEMInvoiceId = grp.Key.OEMInvoiceId,
                             ReconcileDate = grp.Key.ReconcileDate,
                             TotalQuantity = grp.Sum(t=>t.Quantity)
                            };
            foreach (var item in query)
            {
                ReconciledItem rec = new ReconciledItem();
                rec.OemInvoiceid = item.OEMInvoiceId;
                rec.ReconcileDate = item.ReconcileDate.HasValue ? item.ReconcileDate.Value.ToString() : "";
                rec.ReconcileTotalQuantity = item.TotalQuantity.Value;
                items.Add(rec);
            }
            return items;
        }

        //public string GetAvailableWeight(int stateId,int targetId)
        public string GetAvailableWeight(int stateId)
        {
            //AffiliateTarget afftarget = new MITSService<AffiliateTarget>().GetSingle(x => x.Id == targetId);
            //DateTime startDt = (DateTime)afftarget.StartDate;
            //int year = startDt.Year;

            //getting all processors invoices
            //IList<Invoice> processorsInvoices = new MITSService<Invoice>().GetAll(x => x.Affiliate.AffiliateTypeId == 2 && x.StateId == stateId && x.PlanYear == year);
            IList<Invoice> processorsInvoices = new MITSService<Invoice>().GetAll(x => x.Affiliate.AffiliateTypeId == 2 && x.StateId == stateId && x.Approved == true);

            // making list of invoice items from all processors invoices
            List<InvoiceItem> processorsInvoiceItems = new List<InvoiceItem>();
            foreach (Invoice invoice in processorsInvoices)
            {
                foreach (InvoiceItem invoiceItem in invoice.InvoiceItems)
                {
                    if (!processorsInvoiceItems.Contains(invoiceItem) && invoiceItem.ServiceTypeId == 3)
                        processorsInvoiceItems.Add(invoiceItem);
                }                
            }

            double availableWeight = 0;
            // traverse processors Invoice Items list to Available Weight (which has not been reconciled yet)
            foreach (InvoiceItem invoiceItem in processorsInvoiceItems)
            {
                double reconciledWeight = 0;
                // getting entries from reconciliation table against an invoice item
                IList<InvoiceItemReconciliation> processorsReconciledEntries = new MITSService<InvoiceItemReconciliation>().GetAll(x => x.ReconcileFromInvoiceItemID == invoiceItem.Id);
                // calculating reconciled weight by adding up all the reconciliation entries against current invoice item
                foreach (InvoiceItemReconciliation reconciledEntry in processorsReconciledEntries)
                {
                    reconciledWeight = reconciledWeight + (double)reconciledEntry.Quantity;
                }
                // getting non reconciled weight by subtraction

                double quantity = 0;
                if (invoiceItem.QuantityAdjustPercent.HasValue)
                {
                    quantity = (double)(invoiceItem.Quantity * invoiceItem.QuantityAdjustPercent);
                }
                else
                {
                    quantity = (double)(invoiceItem.Quantity * 1);
                }
                double nonReconciledWeight = quantity - reconciledWeight;
                // adding non reconciled weight againt current invoice item into Available Weight
                availableWeight = availableWeight + nonReconciledWeight;
            }
            return availableWeight.ToString();
        }

        public decimal GetTargetWeight(int targetId)
        {
            AffiliateTarget target = new MITSService<AffiliateTarget>().GetSingle(x => x.Id == targetId);
            decimal targetWeight = 0;
            if (target != null)            
                targetWeight = (decimal)target.Weight;
            
            return targetWeight;
        }

        public decimal GetOEMAssignedWeight(int OEMId,int planYear,int stateId)
        {
            IList<Invoice> Invoices = new MITSService<Invoice>().GetAll(x => x.AffiliateId == OEMId && x.PlanYear == planYear && x.StateId == stateId);
            IList<InvoiceReconciliation> InvoiceRecs = new List<InvoiceReconciliation>();
            decimal AssignedWeight = 0;
            foreach (Invoice invoice in Invoices)
            {
                IList<InvoiceReconciliation> Recs = new MITSService<InvoiceReconciliation>().GetAll(x => x.OEMInvoiceId == invoice.Id);
                foreach (InvoiceReconciliation rec in Recs)
                {
                    AssignedWeight += rec.Quantity.HasValue ? rec.Quantity.Value : 0;
                }
            }
            return AssignedWeight;
        }

        public void DeleteRecocilation(int stateId, int planYear, int OemId)
        {
            IList<InvoiceReconciliation> OemRecons = 
                new MITSService<InvoiceReconciliation>().GetAll(x => x.Invoice.AffiliateId == OemId && x.Invoice.StateId == stateId && x.Invoice.PlanYear == planYear).ToList();
            IList<InvoiceUnpackId> UnpackIds =
                new MITSService<InvoiceUnpackId>().GetAll(x => x.Invoice.AffiliateId == OemId);

            foreach (InvoiceReconciliation Rec in OemRecons)
            {
                new MITSService<InvoiceReconciliation>().Delete(Rec);
            }
            foreach (InvoiceUnpackId UnpackId in UnpackIds)
            {
                new MITSService<InvoiceUnpackId>().Delete(UnpackId);
            }
        }

        public void DeleteRecocilation(int invoiceId)
        {
            IList<InvoiceReconciliation> OemRecons =
                new MITSService<InvoiceReconciliation>().GetAll(x => x.OEMInvoiceId == invoiceId).ToList();
            IList<InvoiceUnpackId> UnpackIds =
                new MITSService<InvoiceUnpackId>().GetAll(x => x.OEMInvoiceId == invoiceId);

            foreach (InvoiceReconciliation Rec in OemRecons)
            {
                new MITSService<InvoiceReconciliation>().Delete(Rec);
            }
            foreach (InvoiceUnpackId UnpackId in UnpackIds)
            {
                new MITSService<InvoiceUnpackId>().Delete(UnpackId);
            }
        }

        

        private List<InvoiceItemForReconcile> GetProcessorsInvoiceItemsForReconcile(decimal weightToRec, int stateId)
        {
            //getting all processors invoices by checking receivable field            
            IList<Invoice> processorsInvoices = new MITSService<Invoice>().GetAll(x => x.Affiliate.AffiliateTypeId == 2 && x.StateId == stateId);

            // making list of invoice items from all processors invoices
            List<InvoiceItem> processorsInvoiceItems = new List<InvoiceItem>();
            foreach (Invoice invoice in processorsInvoices)
            {
                foreach (InvoiceItem invoiceItem in invoice.InvoiceItems)
                {
                    if (!processorsInvoiceItems.Contains(invoiceItem) && invoiceItem.ServiceTypeId == 3)
                        processorsInvoiceItems.Add(invoiceItem);
                } 
            }
            
            List<InvoiceItemForReconcile> processorsInvoiceItemsForReconcile = new List<InvoiceItemForReconcile>();
            // traverse processors Invoice Items list to Available Weight (which has not been reconciled yet)
            decimal availableWeight = 0;            
            foreach (InvoiceItem invoiceItem in processorsInvoiceItems)
            {
                decimal reconciledWeight = 0;
                // getting entries from reconciliation table against an invoice item
                IList<InvoiceItemReconciliation> processorsReconciledEntries = new MITSService<InvoiceItemReconciliation>().GetAll(x => x.ReconcileFromInvoiceItemID == invoiceItem.Id);

                // calculating reconciled weight by adding up all the reconciliation entries against current invoice item
                foreach (InvoiceItemReconciliation reconciledEntry in processorsReconciledEntries)                
                    reconciledWeight = reconciledWeight + (decimal)reconciledEntry.Quantity;
                
                InvoiceItemForReconcile item = new InvoiceItemForReconcile();
                item.InvoiceItemId = invoiceItem.Id;                
                if (invoiceItem.QuantityAdjustPercent.HasValue)
                {
                    item.Quantity = (invoiceItem.Quantity * invoiceItem.QuantityAdjustPercent);
                }
                else
                {
                    item.Quantity = (invoiceItem.Quantity * 1);
                }
                item.Quantity = invoiceItem.Quantity;
                item.ReconciledQuantity = reconciledWeight;
                item.NonReconciledQuantity = invoiceItem.Quantity - reconciledWeight;

                if (item.NonReconciledQuantity > 0)
                    processorsInvoiceItemsForReconcile.Add(item);
                else
                    continue;

                availableWeight = availableWeight + (decimal)item.NonReconciledQuantity;
                if (availableWeight >= weightToRec)
                    break;                    
            }
            return processorsInvoiceItemsForReconcile;
        }

        public double GetWeightNotCollected(int targetId, ref double weightCollected)
        {
            AffiliateTarget target = new MITSService<AffiliateTarget>().GetSingle(x => x.Id == targetId);
            // making OEM's Invoice Items list
            List<InvoiceItem> oemInvoiceItems = new List<InvoiceItem>();

            IList<Invoice> oemInvoices = new MITSService<Invoice>().GetAll(x => x.AffiliateId == target.AffiliateId
                && x.InvoicePeriodFrom >= target.StartDate && x.InvoicePeriodTo <= target.EndDate && x.StateId == target.StateId);

            foreach (Invoice invoice in oemInvoices)
            {
                foreach (InvoiceItem invoiceItem in invoice.InvoiceItems)
                {
                    if (!oemInvoiceItems.Contains(invoiceItem) &&
                        (invoiceItem.ServiceTypeId == 10 || invoiceItem.ServiceTypeId == 1 || invoiceItem.ServiceTypeId == 2 ||
                        invoiceItem.ServiceTypeId == 3 || invoiceItem.ServiceTypeId == 5))
                        oemInvoiceItems.Add(invoiceItem);
                }
            }
            double weightNotCollected = 0;
            weightCollected = 0;
            // traverse OEM's Invoice Items list to calculate Weight Not Collected yet
            foreach (InvoiceItem invoiceItem in oemInvoiceItems)
            {
                double reconciledWeight = 0;
                // getting entries from reconciliation table against an OEM invoice item
                IList<InvoiceItemReconciliation> oemReconciledEntries = new MITSService<InvoiceItemReconciliation>().GetAll(x => x.ReconcileToInvoiceItemID == invoiceItem.Id);
                // calculating reconciled weight by adding up all the reconciliation entries against current OEM invoice item
                foreach (InvoiceItemReconciliation reconciledEntry in oemReconciledEntries)
                {
                    reconciledWeight = reconciledWeight + (double)reconciledEntry.Quantity;
                }
                //calculating reconciled weight
                weightCollected = weightCollected + reconciledWeight;
                // getting non reconciled weight by subtraction
                double nonReconciledWeight = (double)invoiceItem.Quantity - reconciledWeight;
                // adding non reconciled weight againt current OEM invoice item into weight not collected
                weightNotCollected = weightNotCollected + nonReconciledWeight;
            }

            return weightNotCollected;
        }

        private List<InvoiceItemForReconcile> GetOEMInvoiceItemsForReconcile(int targetId, decimal weightToRec)
        {

            AffiliateTarget target = new MITSService<AffiliateTarget>().GetSingle(x => x.Id == targetId);
            // making OEM's Invoice Items list
            List<InvoiceItem> oemInvoiceItems = new List<InvoiceItem>();

            IList<Invoice> oemInvoices = new MITSService<Invoice>().GetAll(x => x.AffiliateId == target.AffiliateId
                && x.InvoicePeriodFrom >= target.StartDate && x.InvoicePeriodTo <= target.EndDate && x.StateId == target.StateId);

            foreach (Invoice invoice in oemInvoices)
            {
                foreach (InvoiceItem invoiceItem in invoice.InvoiceItems)
                {
                    if (!oemInvoiceItems.Contains(invoiceItem) &&
                        (invoiceItem.ServiceTypeId == 10 || invoiceItem.ServiceTypeId == 1 || invoiceItem.ServiceTypeId == 2 ||
                        invoiceItem.ServiceTypeId == 3 || invoiceItem.ServiceTypeId == 5))
                        oemInvoiceItems.Add(invoiceItem);
                } 
            }
            List<InvoiceItemForReconcile> oemInvoiceItemsForReconcile = new List<InvoiceItemForReconcile>();
            decimal availableWeight = 0;            
            // traverse OEM's Invoice Items list to calculate Weight Not Collected yet
            foreach (InvoiceItem invoiceItem in oemInvoiceItems)
            {
                decimal reconciledWeight = 0;
                // getting entries from reconciliation table against an OEM invoice item
                IList<InvoiceItemReconciliation> oemReconciledEntries = new MITSService<InvoiceItemReconciliation>().GetAll(x => x.ReconcileToInvoiceItemID == invoiceItem.Id);
                // calculating reconciled weight by adding up all the reconciliation entries against current OEM invoice item
                foreach (InvoiceItemReconciliation reconciledEntry in oemReconciledEntries)                
                    reconciledWeight = reconciledWeight + (decimal)reconciledEntry.Quantity;               

                InvoiceItemForReconcile item = new InvoiceItemForReconcile();
                item.InvoiceItemId = invoiceItem.Id;
                item.Quantity = invoiceItem.Quantity;
                item.ReconciledQuantity = reconciledWeight;
                item.NonReconciledQuantity = invoiceItem.Quantity - reconciledWeight;

                if (item.NonReconciledQuantity > 0)
                    oemInvoiceItemsForReconcile.Add(item);
                else
                    continue;

                availableWeight = availableWeight + (decimal)item.NonReconciledQuantity;
                if (availableWeight >= weightToRec)
                    break;    
            }

            return oemInvoiceItemsForReconcile;
        }

        public decimal Reconcile(int entityId, int stateId, int targetId, string weightNotCollected, string weightToReconcile)
        {            
            decimal updatedWeightNotCollected = decimal.Parse(weightNotCollected);
            decimal weightToRec = decimal.Parse(weightToReconcile);
            
            // prepare list of processors invoice items which can qualify for reconciliation
            List<InvoiceItemForReconcile> processorsInvoiceItemsForReconcile = this.GetProcessorsInvoiceItemsForReconcile(weightToRec, stateId);
            // prepare list of oem invoice items which can qualify for reconciliation                       
            List<InvoiceItemForReconcile> oemInvoiceItemsForReconcile = this.GetOEMInvoiceItemsForReconcile(targetId, weightToRec);
            // reconcile the items, and get list of reconciled entries
            List<InvoiceItemReconciliation> reconciliationEntries = this.GetReconciledInvoiceItems(processorsInvoiceItemsForReconcile, oemInvoiceItemsForReconcile, weightToRec);

            // add reconciled entries in InvoiceItemsReconciliation table in database
            MITSService<InvoiceItemReconciliation> mitsSvcInvoiceItemsReconciliation = new MITSService<InvoiceItemReconciliation>();
            foreach (InvoiceItemReconciliation item in reconciliationEntries)
                mitsSvcInvoiceItemsReconciliation.MitsEntities.InvoiceItemReconciliations.Add(item);

            mitsSvcInvoiceItemsReconciliation.MitsEntities.SaveChanges();

            updatedWeightNotCollected = updatedWeightNotCollected - weightToRec;
            return updatedWeightNotCollected;
        }

        // This is the basic reconciliation algorithm
        private List<InvoiceItemReconciliation> GetReconciledInvoiceItems(List<InvoiceItemForReconcile> processorsInvoiceItemsForReconcile
            , List<InvoiceItemForReconcile> oemInvoiceItemsForReconcile, decimal weightToRec)
        {
            List<InvoiceItemReconciliation> reconciliationEntries = new List<InvoiceItemReconciliation>();
            int k = 0;
            for (int i = 0; i < oemInvoiceItemsForReconcile.Count; i++)
            {
                // quit algorithm when weight to reconcile approaches to zero
                if (weightToRec == 0)
                    break;

                // if current oem invoice item's non-reconciled quantity is greater than or equal to weight to reconcile                
                if (oemInvoiceItemsForReconcile[i].NonReconciledQuantity >= weightToRec)
                {
                    #region
                    // traverse processors items to reconcile
                    for (int j = k; j < processorsInvoiceItemsForReconcile.Count; j++)
                    {
                        // if current processor invoice item's non-reconciled quantity is greater than or equal to weight to reconcile
                        //then reconciliation would get completed in this step
                        if (processorsInvoiceItemsForReconcile[j].NonReconciledQuantity >= weightToRec)
                        {
                            InvoiceItemReconciliation invoiceItemReconciliation = new InvoiceItemReconciliation();
                            invoiceItemReconciliation.ReconcileFromInvoiceItemID = processorsInvoiceItemsForReconcile[j].InvoiceItemId;
                            invoiceItemReconciliation.ReconcileToInvoiceItemID = oemInvoiceItemsForReconcile[i].InvoiceItemId;
                            decimal quantity = weightToRec;
                            invoiceItemReconciliation.Quantity = quantity;

                            reconciliationEntries.Add(invoiceItemReconciliation);

                            weightToRec = weightToRec - weightToRec;
                            //quantity has been reconciled so quit algorithm
                            break;
                        }
                        // if current processor invoice item's non-reconciled quantity is less than weight to reconcile                        
                        else if (processorsInvoiceItemsForReconcile[j].NonReconciledQuantity < weightToRec)
                        {
                            InvoiceItemReconciliation invoiceItemReconciliation = new InvoiceItemReconciliation();
                            invoiceItemReconciliation.ReconcileFromInvoiceItemID = processorsInvoiceItemsForReconcile[j].InvoiceItemId;
                            invoiceItemReconciliation.ReconcileToInvoiceItemID = oemInvoiceItemsForReconcile[i].InvoiceItemId;
                            decimal quantity = (decimal)processorsInvoiceItemsForReconcile[j].NonReconciledQuantity;
                            invoiceItemReconciliation.Quantity = quantity;

                            reconciliationEntries.Add(invoiceItemReconciliation);

                            // update weight to reconcile as some portion has been reconciled in above steps
                            weightToRec = weightToRec - quantity;
                        }
                    }
                    #endregion
                }
                // if oem current invoice item' non-reconciled quantity is less than weight to reconcile
                else if (oemInvoiceItemsForReconcile[i].NonReconciledQuantity < weightToRec)
                {
                    for (int j = k; j < processorsInvoiceItemsForReconcile.Count; j++)
                    {
                        // if processor current invoice item' non-reconciled quantity is less than weight to reconcile
                        if (processorsInvoiceItemsForReconcile[j].NonReconciledQuantity < weightToRec)
                        {
                            if (processorsInvoiceItemsForReconcile[j].NonReconciledQuantity < oemInvoiceItemsForReconcile[i].NonReconciledQuantity)
                            {
                                #region
                                InvoiceItemReconciliation invoiceItemReconciliation = new InvoiceItemReconciliation();
                                invoiceItemReconciliation.ReconcileFromInvoiceItemID = processorsInvoiceItemsForReconcile[j].InvoiceItemId;
                                invoiceItemReconciliation.ReconcileToInvoiceItemID = oemInvoiceItemsForReconcile[i].InvoiceItemId;
                                decimal quantity = (decimal)processorsInvoiceItemsForReconcile[j].NonReconciledQuantity;
                                invoiceItemReconciliation.Quantity = quantity;

                                reconciliationEntries.Add(invoiceItemReconciliation);

                                // update processor item's non-reconciled and reconciled quantities
                                processorsInvoiceItemsForReconcile[j].NonReconciledQuantity = 0;
                                processorsInvoiceItemsForReconcile[j].ReconciledQuantity = quantity;

                                // update oem item's non-reconciled and reconciled quantities
                                oemInvoiceItemsForReconcile[i].NonReconciledQuantity = oemInvoiceItemsForReconcile[i].NonReconciledQuantity - quantity;
                                oemInvoiceItemsForReconcile[i].ReconciledQuantity = oemInvoiceItemsForReconcile[i].ReconciledQuantity + quantity;

                                // update weight to reconcile as some portion has been reconciled in above steps
                                weightToRec = weightToRec - quantity;

                                //to do : prepare list to insert

                                // to start the outer loop with the same oem item
                                i = i - 1;
                                // to start the inner loop with next processor item
                                k = j + 1;
                                break;
                                #endregion
                            }
                            else if (processorsInvoiceItemsForReconcile[j].NonReconciledQuantity > oemInvoiceItemsForReconcile[i].NonReconciledQuantity)
                            {
                                #region
                                InvoiceItemReconciliation invoiceItemReconciliation = new InvoiceItemReconciliation();
                                invoiceItemReconciliation.ReconcileFromInvoiceItemID = processorsInvoiceItemsForReconcile[j].InvoiceItemId;
                                invoiceItemReconciliation.ReconcileToInvoiceItemID = oemInvoiceItemsForReconcile[i].InvoiceItemId;
                                decimal quantity = (decimal)oemInvoiceItemsForReconcile[i].NonReconciledQuantity;
                                invoiceItemReconciliation.Quantity = quantity;

                                reconciliationEntries.Add(invoiceItemReconciliation);

                                // update oem item's non-reconciled and reconciled quantities
                                oemInvoiceItemsForReconcile[i].NonReconciledQuantity = 0;
                                oemInvoiceItemsForReconcile[i].ReconciledQuantity = quantity;

                                // update processor's item non-reconciled and reconciled quantities
                                processorsInvoiceItemsForReconcile[j].NonReconciledQuantity = processorsInvoiceItemsForReconcile[j].NonReconciledQuantity - quantity;
                                processorsInvoiceItemsForReconcile[j].ReconciledQuantity = processorsInvoiceItemsForReconcile[j].ReconciledQuantity + quantity;

                                // update weight to reconcile as some portion has been reconciled in above steps
                                weightToRec = weightToRec - quantity;

                                //to do : prepare list to insert

                                // to start the inner loop with next processor item
                                k = j;
                                break;
                                #endregion
                            }
                            else if (processorsInvoiceItemsForReconcile[j].NonReconciledQuantity == oemInvoiceItemsForReconcile[i].NonReconciledQuantity)
                            {
                                #region
                                InvoiceItemReconciliation invoiceItemReconciliation = new InvoiceItemReconciliation();
                                invoiceItemReconciliation.ReconcileFromInvoiceItemID = processorsInvoiceItemsForReconcile[j].InvoiceItemId;
                                invoiceItemReconciliation.ReconcileToInvoiceItemID = oemInvoiceItemsForReconcile[i].InvoiceItemId;
                                decimal quantity = (decimal)oemInvoiceItemsForReconcile[i].NonReconciledQuantity;
                                invoiceItemReconciliation.Quantity = quantity;

                                reconciliationEntries.Add(invoiceItemReconciliation);

                                // update oem item's non-reconciled and reconciled quantities
                                oemInvoiceItemsForReconcile[i].NonReconciledQuantity = 0;
                                oemInvoiceItemsForReconcile[i].ReconciledQuantity = quantity;

                                // update processor's item non-reconciled and reconciled quantities
                                processorsInvoiceItemsForReconcile[j].NonReconciledQuantity = 0;
                                processorsInvoiceItemsForReconcile[j].ReconciledQuantity = quantity;

                                // update weight to reconcile as some portion has been reconciled in above steps
                                weightToRec = weightToRec - quantity;

                                //to do : prepare list to insert

                                // to start the inner loop with next processor item and break the loop, inner loop will start from k next time
                                k = j + 1;
                                break;
                                #endregion
                            }
                        }
                        // if processor current invoice item' non-reconciled quantity is greater than weight to reconcile
                        else if (processorsInvoiceItemsForReconcile[j].NonReconciledQuantity > weightToRec)
                        {
                            #region
                            InvoiceItemReconciliation invoiceItemReconciliation = new InvoiceItemReconciliation();
                            invoiceItemReconciliation.ReconcileFromInvoiceItemID = processorsInvoiceItemsForReconcile[j].InvoiceItemId;
                            invoiceItemReconciliation.ReconcileToInvoiceItemID = oemInvoiceItemsForReconcile[i].InvoiceItemId;
                            decimal quantity = (decimal)oemInvoiceItemsForReconcile[i].NonReconciledQuantity;
                            invoiceItemReconciliation.Quantity = quantity;

                            reconciliationEntries.Add(invoiceItemReconciliation);

                            // update processor item's non-reconciled and reconciled quantities
                            oemInvoiceItemsForReconcile[i].NonReconciledQuantity = 0;
                            oemInvoiceItemsForReconcile[i].ReconciledQuantity = quantity;

                            // update oem item's non-reconciled and reconciled quantities
                            processorsInvoiceItemsForReconcile[j].NonReconciledQuantity = processorsInvoiceItemsForReconcile[j].NonReconciledQuantity - quantity;
                            processorsInvoiceItemsForReconcile[j].ReconciledQuantity = processorsInvoiceItemsForReconcile[j].ReconciledQuantity + quantity;

                            // update weight to reconcile as some portion has been reconciled in above steps
                            weightToRec = weightToRec - quantity;

                            //to do : prepare list to insert

                            // to start the inner loop with same processor item
                            k = j;
                            break;
                            #endregion
                        }
                    }

                }
            }
            return reconciliationEntries;
        }
    }

    public class InvoiceItemForReconcile
    {
        public InvoiceItemForReconcile() { }
        public int InvoiceItemId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? ReconciledQuantity { get; set; }
        public decimal? NonReconciledQuantity { get; set; }
    }

    public class ReconciledItem
    {
        public int OemInvoiceid;
        public string ReconcileDate;
        public decimal ReconcileTotalQuantity;
    }
}
