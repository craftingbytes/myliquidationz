﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Web;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using System.Collections;
using Mits.DAL.EntityModels;
using Mits.Common;
using System.ComponentModel;

namespace Mits.BLL.BusinessObjects
{

    public class ReminderEmailBO
    {

        MITSService<V_ReminderEmail> mitsReminderEmailSvc = null;
        MITSEntities mitsDataContext = null;
        Logger logger = null;

        public ReminderEmailBO()
        {            
            logger = new Logger();
            //logger.Info("In reminder BO constructor.");
            mitsReminderEmailSvc = new MITSService<V_ReminderEmail>();
            //logger.Info("After creating MitsService.");
            mitsDataContext = mitsReminderEmailSvc.MitsEntities;            
        }

        string logPrefix = "ReminderEmailProcessTest->";


        public void SendReminderEmail()
        {
            logPrefix += "SendReminderEmail->";

            try
            {

                var data = mitsReminderEmailSvc.GetAll();
                var ReminderGroupIDs = (from m in data
                                        orderby m.ReminderGroupId
                                        select m.ReminderGroupId).Distinct().ToList();


                if (data.Count == 0)
                    logger.Info(logPrefix + "No Pending Reminder Found.");

                string returnMessage = string.Empty;
                bool mailSuccess = false;

                Hashtable mailSent = new Hashtable();

                foreach (var id in ReminderGroupIDs)
                {
                    var affiliateData = data.Where(c => c.ReminderGroupId == id);
                    foreach (V_ReminderEmail item in affiliateData)
                    {
                        if (!mailSent.ContainsKey(item.ReminderId))
                            mailSent.Add(item.ReminderId, item.ReminderId);
                        else
                            continue;

                        if (!string.IsNullOrEmpty(item.Emails))
                        {
                            ProcessReminder(item);
                            //if (item.PeriodId == 5 || item.PeriodId == 6)
                            //{
                            //    int number = Convert.ToInt16(item.Number);
                            //    if (item.PeriodId == 6) //if week multiply by 7
                            //        number = number * 7;
                            //    mailSuccess = false;
                            //    DateTime sendDate, occuranceDate;
                            //    occuranceDate = Convert.ToDateTime(item.OccurranceDate).Date;
                            //    sendDate = Convert.ToDateTime(item.SendDate);
                            //    List<string> attachments = getAttachments(item.ReminderId);
                            //    returnMessage = SendEmail(item.Emails, item.Title, item.EventTitle, occuranceDate, item.eType, item.EventDescription, attachments, out mailSuccess);
                            //    LogReminderEmail(logPrefix, item.RecurrenceTypeId, item.PeriodId, false, item, returnMessage);

                            //    if (mailSuccess)
                            //    {
                            //        switch (item.RecurrenceTypeId)
                            //        {
                            //            case 6://single
                            //                {
                            //                    if (occuranceDate == Convert.ToDateTime(item.SingleRecurrenceValue).Date)
                            //                        UpdateStatus(item.ReminderId);
                            //                    else
                            //                        occuranceDate = occuranceDate = Convert.ToDateTime(item.SingleRecurrenceValue).Date;
                            //                    sendDate = occuranceDate.AddDays(-number);
                            //                }
                            //                break;
                            //            case 7://every
                            //                {
                            //                    int recurrenceIntervalForDay = Convert.ToInt32(item.RecurrenceIntervalForDay);
                            //                    occuranceDate = occuranceDate = occuranceDate.AddDays(recurrenceIntervalForDay);
                            //                    sendDate = occuranceDate.AddDays(-number);
                            //                }
                            //                break;
                            //            case 8://monthly
                            //                {
                            //                    occuranceDate = occuranceDate.AddMonths(1);
                            //                    sendDate = occuranceDate.AddDays(-number);
                            //                }
                            //                break;
                            //            case 9://quarterly
                            //                {
                            //                    occuranceDate = occuranceDate.AddMonths(3);
                            //                    sendDate = occuranceDate.AddDays(-number);
                            //                }
                            //                break;
                            //            case 10://yearly
                            //                {
                            //                    occuranceDate = occuranceDate.AddYears(1);
                            //                    sendDate = occuranceDate.AddDays(-number);
                            //                }
                            //                break;
                            //            default:
                            //                {
                            //                    UpdateStatus(item.ReminderId);
                            //                }
                            //                break;
                            //        }

                            //        DateTime? reminderDate = null;
                            //        reminderDate = DateTime.Now.Date;
                            //        UpdateSentInformation(sendDate, reminderDate, occuranceDate, item.ReminderId);

                            //        if (occuranceDate > item.DateEnd && item.DateEnd != null)
                            //            UpdateStatus(item.ReminderId);

                            //    }
                            //}
                        }
                        else
                        {
                            logger.Info(logPrefix + "No Emails Selcted for the reminder Id." + item.ReminderId);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info(logPrefix + " Exception: " + ex.Message);
                // logger.Info(logPrefix + " Inner Exception: " + ex.InnerException == null ? "" : ex.InnerException.Message);
            }


        }

        public static bool UpdateStatus(int reminderId)
        {
            
            MITSEntities db = new MITSEntities();
            Logger log = new Logger();
            string logPrefix = "ReminderEmailProcessTest->";

            Reminder reminderEntity = new Reminder();
            reminderEntity = db.Reminders.Include("ReminderStatu").FirstOrDefault(e => e.Id == reminderId);
            ReminderStatu _status = db.ReminderStatus.FirstOrDefault(r => r.Status == "Completed");

            if (_status != null)
            {
                reminderEntity.ReminderStatu = db.ReminderStatus.FirstOrDefault(r => r.Id == _status.Id); //"
                db.SaveChanges();

                log.Info(logPrefix + "UpdateStatus called with Success for ReminderId:" + reminderId.ToString());

                return true;
            }

            return false;
        }

        public static bool UpdateSentInformation(DateTime? sendDate, DateTime? sentReminderDate, DateTime? occurranceDate, int reminderId)
        {

            Reminder reminderEntity = new Reminder();
            MITSEntities db = new MITSEntities();
            string logPrefix = "ReminderEmailProcessTest->";

            Logger log = new Logger();
            reminderEntity = db.Reminders.FirstOrDefault(x => x.Id == reminderId);
            if (sendDate != null)
            {
                reminderEntity.SendDate = sendDate;
            }
            if (sentReminderDate != null)
            {
                reminderEntity.SentReminderDate = sentReminderDate;
            }
            if (occurranceDate != null)
            {
                reminderEntity.OccurranceDate = occurranceDate;
            }            
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                log.Info(logPrefix + " Exception: " + ex.Message);
                log.Info(logPrefix + " Inner Exception:" + ex.InnerException == null ? "" : ex.InnerException.Message);
            }            
            return true;
        }

        //public string SendEmail(string emails, string reminderTitle, string eventTitle, DateTime occurenceDate,
        //              string evetntTpye, string description, List<string> attachments, out bool success)
        public void SendEmail(string emails, string reminderTitle, string eventTitle, DateTime occurenceDate,
                      string evetntTpye, string description, List<string> attachments, V_ReminderEmail reminder)
        {
            logPrefix += "SendEmail->";


            logger.Info(logPrefix + " To Emails" + emails);

            Dictionary<object, object> _dictioanry = new Dictionary<object, object>(); 
            StringBuilder sendMessage = new StringBuilder();
            sendMessage.Append(Utility.BodyStart());
            sendMessage.Append(Utility.AddTextLine("Dear Sir/Madam,"));
            sendMessage.Append(Utility.AddBreak(2));
            sendMessage.Append(Utility.AddTextLine("This is an automated reminder email for the below event from E-World Online MITS application."));
            sendMessage.Append(Utility.AddBreak(2));
            sendMessage.Append(Utility.AddTextLine("Action Item: "+reminderTitle));
            sendMessage.Append(Utility.AddBreak(2));
            sendMessage.Append(Utility.AddTextLine("Event Information:"));
            _dictioanry.Add("Title",eventTitle);
            _dictioanry.Add("Due Date", occurenceDate.ToShortDateString());
            _dictioanry.Add("Type", evetntTpye);
            _dictioanry.Add("Description", description);

            sendMessage.Append(Utility.ConvertToHtmlTable(_dictioanry));

            sendMessage.Append(Utility.AddBreak(2));
            //sendMessage.Append(Utility.AddTextLine("Please find attached the files listed below."));
            sendMessage.Append(Utility.AddBreak(2));            
            logger.Info(logPrefix + " Attachments Count: " + attachments.Count);

            if (attachments.Count > 0)
            {
                sendMessage.Append(Utility.UnOrderListStart());
                for (int i = 0; i < attachments.Count; i++)
                {
                    sendMessage.Append(Utility.ListItemStart());
                    sendMessage.Append(Utility.AddTextLine("" + Convert.ToString(i + 1) + " . " + attachments[i] + ""));
                    sendMessage.Append(Utility.ListItemEnd());
                }
                sendMessage.Append(Utility.UnOrderListEnd());
            }
            
            sendMessage.Append(Utility.AddBreak(3));
            sendMessage.Append(Utility.AddTextLine("Thank You,"));
            sendMessage.Append(Utility.AddBreak(2));
            sendMessage.Append(Utility.AddTextLine("E-World Online"));
            sendMessage.Append(Utility.BodyEnd());
          
            String retMessage = "";

            List<string> emailTo = new List<string>();
            emailTo.Add(Convert.ToString(ConfigurationManager.AppSettings["toEventEmail"]));
            string[] emailsColls = new string[] { };
            
            List<Attachment> fileAttachments = new List<Attachment>();


            try
            {
                logger.Info(logPrefix + " To Emails:" + emails);

                if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["ReminderTestMails"])))
                {

                    emails = Convert.ToString(ConfigurationManager.AppSettings["ReminderTestMails"]);
                }

                emailsColls = emails.Split(new char[] { ';' });

                foreach (var fname in attachments)
                {
                    try
                    {
                        logger.Info(logPrefix + " Attachment path: " + ConfigurationManager.AppSettings["FilesUploadPath"] + fname);

                        fileAttachments.Add(new Attachment(ConfigurationManager.AppSettings["FilesUploadPath"] + fname));
                    }
                    catch (Exception ex)
                    {

                        logger.Info(logPrefix + " Exception on attachment: " + fname + " " + ex.Message + "");

                        logger.Info(logPrefix + " Inner Exception on attachment: " + fname + " " + ex.InnerException == null ? "" : ex.InnerException.Message);

                    }
                }

                logger.Info(logPrefix + " Seinding Email: MSG body:" + sendMessage.ToString());

                SendMailAsync(reminderTitle, sendMessage.ToString(), emailTo.ToArray(), null, emailsColls, reminder, fileAttachments);
                

                ////success = EmailHelper.SendMail(reminderTitle, sendMessage.ToString(), emailsColls, null, null, fileAttachments);

                ////if (success)
                //retMessage = "Message was successfuly sent at " + DateTime.Now.ToString() + ".";

                //logger.Info(logPrefix + " Sucess: " + true);
            }
            catch (Exception ex)
            {
                //retMessage = ex.Message;
                //success = false;

                logger.Info(logPrefix + " Exception: " + ex.Message);
                logger.Info(logPrefix + " Inner Exception:" + ex.InnerException == null ? "" : ex.InnerException.Message);

            }

            //return retMessage;
        }

        public void LogReminderInfo(string logPrefix, int? recurrenceTypeId, int? periodId, bool basicFlow,
           DateTime? eventOccurenceDate, DateTime? reminderSendDate, DateTime? reminderSentDate)
        {
            string recurrenceType = string.Empty;
            string period = string.Empty;

            switch (recurrenceTypeId)
            {
                case 6: recurrenceType = "Single"; break;
                case 7: recurrenceType = "Every"; break;
                case 8: recurrenceType = "Monthly"; break;
                case 9: recurrenceType = "Quarterly"; break;
                default: recurrenceType = "Default"; break;
            }
            switch (periodId)
            {
                case 4: period = "Hours"; break;
                case 5: period = "Days"; break;
                case 6: period = "Weeks"; break;
            }


            logger.Info(logPrefix + "->" + recurrenceType + "->" + period + "->" + (basicFlow ? "IF" : "ELSE")
                + " NextOccurenceDate:" + Convert.ToString(eventOccurenceDate) + " Next Send date: "
                + Convert.ToString(reminderSendDate) + " ReminderSentDate: " + Convert.ToString(reminderSentDate));

            logger.Info("--------------------");
        }

        public void LogReminderEmail(string logPrefix, int? recurrenceTypeId, int? periodId, bool basicFlow,
           V_ReminderEmail mailItem, string returnMessage)
        {
            string recurrenceType = string.Empty;
            string period = string.Empty;

            switch (recurrenceTypeId)
            {
                case 6: recurrenceType = "Single"; break;
                case 7: recurrenceType = "Every"; break;
                case 8: recurrenceType = "Monthly"; break;
                case 9: recurrenceType = "Quarterly"; break;
                default: recurrenceType = "Default"; break;
            }
            switch (periodId)
            {
                case 4: period = "Hours"; break;
                case 5: period = "Days"; break;
                case 6: period = "Weeks"; break;
            }


            StringBuilder sendMessage = new StringBuilder();

            sendMessage.Append("Event Id: " + mailItem.EventId + "\r\n");
            sendMessage.Append("Event Title: " + mailItem.EventTitle + "\r\n");
            sendMessage.Append("Event Date: " + mailItem.OccurranceDate + "\r\n");
            sendMessage.Append("Event Type: " + mailItem.eType + "\r\n");
            sendMessage.Append("Event Description: " + mailItem.EventDescription + "\r\n");
            sendMessage.Append("Event To Mails: " + mailItem.Emails + "\r\n");



            logger.Info(logPrefix + "->" + recurrenceType + "->" + period + "->" + (basicFlow ? "IF" : "ELSE")
                + " MailMessage:" + sendMessage + " Return Message: " + returnMessage);

            logger.Info("--------------------");
        }

        public static void SendMailAsync(string subject, string markup, string[] toAddress, string[] ccAddress, string[] bccAddress, V_ReminderEmail reminder, List<Attachment> attachments = null)
        {
            try
            {

                SmtpClient smtpClient = new SmtpClient(ConfigHelper.SmptServer, ConfigHelper.SmtpPort);

                if (ConfigHelper.SmtpAuth)
                {

                    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigHelper.SmtpUserName, ConfigHelper.SmtpPassword);
                    smtpClient.Credentials = credentials;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = credentials;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                }

                MailMessage message = new MailMessage();

                string _fromEmail = ConfigurationManager.AppSettings["fromEmail"];

                MailAddress fromAddress = new MailAddress(ConfigHelper.FromEmail);

                message.From = fromAddress;

                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");

                // To address collection of MailAddress
                if (toAddress != null)
                {
                    foreach (string toAdd in toAddress)
                    {
                        message.To.Add(toAdd);
                    }
                }
                if (ccAddress != null)
                {
                    foreach (string toAdd in ccAddress)
                    {
                        MailAddress ma = new MailAddress(toAdd);
                        message.CC.Add(ma);
                    }
                }
                if (bccAddress != null)
                {
                    foreach (string toAdd in bccAddress)
                    {
                        message.Bcc.Add(toAdd);
                    }
                }

                if (attachments != null)
                {
                    foreach (var file in attachments)
                    {

                        message.Attachments.Add(file);
                    }
                }

                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = markup;

                // Send SMTP mail
                smtpClient.SendCompleted += new SendCompletedEventHandler(SmtpClient_OnCompleted);
                object userstate = reminder;

                smtpClient.SendAsync(message, userstate);
                string somestring = subject;

            }
            catch (Exception ex)
            {
                //_success = false;
                throw ex.InnerException;

            }



        }

        public static void SmtpClient_OnCompleted(object sender, AsyncCompletedEventArgs e)
        {
            ////Get the Original MailMessage object
            //MailMessage mail = (MailMessage)e.UserState;

            V_ReminderEmail item = (V_ReminderEmail)e.UserState;
            bool mailSuccess = !(e.Cancelled);

            DateTime sendDate, occuranceDate;
            occuranceDate = Convert.ToDateTime(item.OccurranceDate).Date;
            sendDate = Convert.ToDateTime(item.SendDate);

            int number = Convert.ToInt16(item.Number);

            if (mailSuccess)
            {
                switch (item.RecurrenceTypeId)
                {
                    case 6://single
                        {
                            if (occuranceDate == Convert.ToDateTime(item.SingleRecurrenceValue).Date)
                                UpdateStatus(item.ReminderId);
                            else
                                occuranceDate = occuranceDate = Convert.ToDateTime(item.SingleRecurrenceValue).Date;
                            sendDate = occuranceDate.AddDays(-number);
                        }
                        break;
                    case 7://every
                        {
                            int recurrenceIntervalForDay = Convert.ToInt32(item.RecurrenceIntervalForDay);
                            occuranceDate = occuranceDate = occuranceDate.AddDays(recurrenceIntervalForDay);
                            sendDate = occuranceDate.AddDays(-number);
                        }
                        break;
                    case 8://monthly
                        {
                            occuranceDate = occuranceDate.AddMonths(1);
                            sendDate = occuranceDate.AddDays(-number);
                        }
                        break;
                    case 9://quarterly
                        {
                            occuranceDate = occuranceDate.AddMonths(3);
                            sendDate = occuranceDate.AddDays(-number);
                        }
                        break;
                    case 10://yearly
                        {
                            occuranceDate = occuranceDate.AddYears(1);
                            sendDate = occuranceDate.AddDays(-number);
                        }
                        break;
                    default:
                        {
                            UpdateStatus(item.ReminderId);
                        }
                        break;
                }

                DateTime? reminderDate = null;
                reminderDate = DateTime.Now.Date;
                UpdateSentInformation(sendDate, reminderDate, occuranceDate, item.ReminderId);

                if (occuranceDate > item.DateEnd && item.DateEnd != null)
                    UpdateStatus(item.ReminderId);

            }



            

            ////write out the subject
            //string subject = mail.Subject;
            string logPrefix = "ReminderEmailProcessTest->";
            Logger log = new Logger();
            if (e.Cancelled)
            {



                log.Info(logPrefix + " Send canceled for mail to " + item.Emails);
                //Console.WriteLine("Send canceled for mail with subject [{0}].", subject);
            }
            if (e.Error != null)
            {
                log.Info(logPrefix + " Error " + e.Error.ToString());
                //Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString());
            }

        }

        public void ProcessReminder(V_ReminderEmail item)
        {
            
            if (item.PeriodId == 5 || item.PeriodId == 6)
            {
                int number = Convert.ToInt16(item.Number);
                if (item.PeriodId == 6) //if week multiply by 7
                    number = number * 7;
                //mailSuccess = false;
                DateTime sendDate, occuranceDate;
                occuranceDate = Convert.ToDateTime(item.OccurranceDate).Date;
                sendDate = Convert.ToDateTime(item.SendDate);
                List<string> attachments = getAttachments(item.ReminderId);


                SendEmail(item.Emails, item.Title, item.EventTitle, occuranceDate, item.eType, item.EventDescription, attachments, item);


                // all this should go into delegate
                //LogReminderEmail(logPrefix, item.RecurrenceTypeId, item.PeriodId, false, item, returnMessage);

                //if (mailSuccess)
                //{
                //    switch (item.RecurrenceTypeId)
                //    {
                //        case 6://single
                //            {
                //                if (occuranceDate == Convert.ToDateTime(item.SingleRecurrenceValue).Date)
                //                    UpdateStatus(item.ReminderId);
                //                else
                //                    occuranceDate = occuranceDate = Convert.ToDateTime(item.SingleRecurrenceValue).Date;
                //                sendDate = occuranceDate.AddDays(-number);
                //            }
                //            break;
                //        case 7://every
                //            {
                //                int recurrenceIntervalForDay = Convert.ToInt32(item.RecurrenceIntervalForDay);
                //                occuranceDate = occuranceDate = occuranceDate.AddDays(recurrenceIntervalForDay);
                //                sendDate = occuranceDate.AddDays(-number);
                //            }
                //            break;
                //        case 8://monthly
                //            {
                //                occuranceDate = occuranceDate.AddMonths(1);
                //                sendDate = occuranceDate.AddDays(-number);
                //            }
                //            break;
                //        case 9://quarterly
                //            {
                //                occuranceDate = occuranceDate.AddMonths(3);
                //                sendDate = occuranceDate.AddDays(-number);
                //            }
                //            break;
                //        case 10://yearly
                //            {
                //                occuranceDate = occuranceDate.AddYears(1);
                //                sendDate = occuranceDate.AddDays(-number);
                //            }
                //            break;
                //        default:
                //            {
                //                UpdateStatus(item.ReminderId);
                //            }
                //            break;
                //    }

                //    DateTime? reminderDate = null;
                //    reminderDate = DateTime.Now.Date;
                //    UpdateSentInformation(sendDate, reminderDate, occuranceDate, item.ReminderId);

                //    if (occuranceDate > item.DateEnd && item.DateEnd != null)
                //        UpdateStatus(item.ReminderId);

                //}
            }
        
        
        }

        public List<string> getAttachments(int reminderId)
        {
            List<string> reminderAttachments = (from m in mitsDataContext.ReminderAttachments.Include("Reminder")
                                                where m.Reminder.Id == reminderId
                                                select m.AttachmentName).ToList<string>();

            return reminderAttachments;

        }

    }
}
