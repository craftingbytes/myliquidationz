﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;

namespace Mits.BLL.BusinessObjects
{
   public class ProductTypeBO
    {

       MITSRepository<ProductType> mitsProductTypeRepo = new MITSRepository<ProductType>();

        public ProductTypeBO()
        {
            
        }

        //Getting all product types from ProductType

        public IQueryable GetAllProductType()
        {
            return mitsProductTypeRepo.GetAll() as IQueryable;
        }

        //Getting a list of all service types from ServiceType    

        public IList<ProductType> GetAll()
        {
             //Sorting List
             IList<ProductType> _productTypeObj;
            IList<ProductType> unSortedList = new MITSService<ProductType>().GetAll();
            IEnumerable<ProductType> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _productTypeObj = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _productTypeObj = unSortedList;
            }

            ProductType _selectProduct = new ProductType();
            _selectProduct.Id = 0;
            _selectProduct.Name = "Select";
            _productTypeObj.Insert(0, _selectProduct);
            return _productTypeObj;
        }

        //Checking If product type already exists        

        public bool Exists(string name)
        {
            IList<ProductType> _productTypeObj = new MITSService<ProductType>().GetAll();
            bool _existsFlag = false;
            for (var i = 0; i < _productTypeObj.Count; i++)
            {
                if ((name.Equals(_productTypeObj[i].Name, StringComparison.OrdinalIgnoreCase)))
                    _existsFlag = true;
            }
            return _existsFlag;

        }

       //Checking if any change to product is made 

        public bool Change(string name, string desc, string notes, int? _imageId, decimal? _weight, int? _weightQuantity, int? _container, int? _containerType)
        {
            IList<ProductType> _productTypeObj = new MITSService<ProductType>().GetAll();
            bool _existsFlag = false;
            for (var i = 0; i < _productTypeObj.Count; i++)
            {
                if ((name.Equals(_productTypeObj[i].Name, StringComparison.OrdinalIgnoreCase)))
                {
                    if (desc.Equals(_productTypeObj[i].Description) || notes.Equals(_productTypeObj[i].Notes) || _imageId.Equals(_productTypeObj[i].SiteImageID)
                        || _weight.Equals(_productTypeObj[i].EstimatedWeight) || _weightQuantity.Equals(_productTypeObj[i].WeightQuantityTypeID)
                        || _container.Equals(_productTypeObj[i].EstimatedItemsPerContainer) || _containerType.Equals(_productTypeObj[i].ContainerQuantityTypeId))
                    {
                        _existsFlag = true;
                        break;
                    }
                    else
                    {
                        _existsFlag = false;
                        break;
                    }
                    

                }
                   
            }
            return _existsFlag;
        }

        //Returning Id of existing Product Type       

        public int ReturnId(string name)
        {
            IList<ProductType> _productList = new MITSService<ProductType>().GetAll();
            int _returnValue = 0;
            for (var i = 0; i < _productList.Count; i++)
            {
                if ((name.Equals(_productList[i].Name, StringComparison.OrdinalIgnoreCase)))
                    _returnValue = _productList[i].Id;
            }
            return _returnValue;

        }
       
       
       #region Add Product Type 


        //Add service type and associated groups to ServiceTypeGroupRelation and return mulitple values via Dictionary

        public Dictionary<string, int> Add(string productType, string name, string desc, string notes, string imageId, string weight, string weightQuantity, string container, string containerType)
        {
            int _productType = int.Parse(productType);
            int? _imageId = int.Parse(imageId);
            int? _weightQuantity = int.Parse(weightQuantity);
            int? _containerType = int.Parse(containerType);
            
            if (weight == "")
                weight = "0";
            decimal? _weight = decimal.Parse(weight);
            
            if (container == "")
                container = "0";
            int? _container = int.Parse(container);
            
          

            int _returnId = default(int);

            //Dictionary Object to hold the return values

            Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();
            _returnDictionaryValue["duplicate"] = 0;
            _returnDictionaryValue["selectedId"] = _productType;

            //Check for New Entry via entering value in the text box of name

            if (_productType == 0)
            {
                if (this.Exists(name) != true)
                {
                    
                    //New entry of product type 

                    //if (_weight == 0)
                    //    _weight = null;
                    //if (_container == 0)
                    //    _container = null;
                    if (_imageId == 0)
                        _imageId = null;
                    if (_weightQuantity == 0)
                        _weightQuantity = null;
                    if (_containerType == 0)
                        _containerType = null;
                   
                    ProductType _productTypeObj = new ProductType();
                    _productTypeObj.Name = name;
                    _productTypeObj.Description = desc;
                    _productTypeObj.Notes = notes;
                    _productTypeObj.SiteImageID = _imageId;
                    _productTypeObj.EstimatedWeight = _weight;
                    _productTypeObj.WeightQuantityTypeID = _weightQuantity;
                    _productTypeObj.EstimatedItemsPerContainer = _container;
                    _productTypeObj.ContainerQuantityTypeId = _containerType;
                    _productTypeObj.ConsumerSelectable = true;
                    new MITSService<ProductType>().Add(_productTypeObj);


                    _returnDictionaryValue["selectedId"] = _productTypeObj.Id;
                    return _returnDictionaryValue;
                }

                //Check for old entry of product type 

                else if (this.Change(name, desc, notes, _imageId, _weight, _weightQuantity, _container, _containerType) == true)
                {
                    _returnId = this.ReturnId(name);

                    //if (_weight == 0)
                    //    _weight = null;
                    //if (_container == 0)
                    //    _container = null;
                    if (_imageId == 0)
                        _imageId = null;
                    if (_weightQuantity == 0)
                        _weightQuantity = null;
                    if (_containerType == 0)
                        _containerType = null;
                    
                    
                    ProductType _productTypeObj = new MITSService<ProductType>().GetSingle(t => t.Id == _returnId);
                    _productTypeObj.Name = name;
                    _productTypeObj.Description = desc;
                    _productTypeObj.Notes = notes;
                    _productTypeObj.SiteImageID = _imageId;
                    _productTypeObj.EstimatedWeight = _weight;
                    _productTypeObj.WeightQuantityTypeID = _weightQuantity;
                    _productTypeObj.EstimatedItemsPerContainer = _container;
                    _productTypeObj.ContainerQuantityTypeId = _containerType;
                    _productTypeObj.ConsumerSelectable = true;
                    new MITSService<ProductType>().Save(_productTypeObj);

                    _returnDictionaryValue["selectedId"] = _productTypeObj.Id;
                    return _returnDictionaryValue;
                }
                else
                {
                    _returnDictionaryValue["duplicate"] = 1;
                    return _returnDictionaryValue;
                }

            }
            //Old entry of product type selected via dropdown
            else
            {
                ProductType _product = new MITSService<ProductType>().GetSingle(t => t.Id == _productType);



                if (name.Equals(_product.Name) && desc.Equals(_product.Description) && notes.Equals(_product.Notes) && _imageId.Equals(_product.SiteImageID) 
                    && _weight.Equals(_product.EstimatedWeight) && _weightQuantity.Equals(_product.WeightQuantityTypeID) && _container.Equals(_product.EstimatedItemsPerContainer)
                    && _containerType.Equals(_product.ContainerQuantityTypeId))
                {
                    _returnDictionaryValue["duplicate"] = 1;
                    return _returnDictionaryValue;
                }
                else
                {
                    //if (_weight == 0)
                    //    _weight = null;
                    //if (_container == 0)
                    //    _container = null;
                    if (_imageId == 0)
                        _imageId = null;
                    if (_weightQuantity == 0)
                        _weightQuantity = null;
                    if (_containerType == 0)
                        _containerType = null;
                    
                    
                    
                    _product.Name = name;
                    _product.Description = desc;
                    _product.Notes = notes;
                    _product.SiteImageID = _imageId;
                    _product.EstimatedWeight = _weight;
                    _product.WeightQuantityTypeID = _weightQuantity;
                    _product.EstimatedItemsPerContainer = _container;
                    _product.ContainerQuantityTypeId = _containerType;
                    _product.ConsumerSelectable = true;
                    new MITSService<ProductType>().Save(_product);

                    _returnDictionaryValue["selectedId"] = _product.Id;
                    return _returnDictionaryValue;


                   
                }
            }
            
            
            
            
            
        

        }
          #endregion



    }
}
