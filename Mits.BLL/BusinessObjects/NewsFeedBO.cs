﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class NewsFeedBO
    {
        private Logger mitsLogger = null;

        public NewsFeedBO()
        {
            mitsLogger = new Logger(this.GetType());
        }

        public IList<NewsFeed> GetAllNewsFeed()
        {
            var service = new MITSService<NewsFeed>();
            var feeds = service.GetAll(x=>!string.IsNullOrEmpty(x.News) && x.Date != null).OrderBy(x=>x.Date);
            return feeds.ToList();
        }

        public NewsFeed GetNewsFeed(int Id)
        {
            try
            {
                var service = new MITSService<NewsFeed>();
                var feed = service.GetSingle(x => x.Id == Id);
                return feed;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get news feed by id:" + Id, ex);
                throw ex;
            }
        }

        public Message SaveNewsFeed(NewsFeed Feed)
        {
            Message message = null;
            try
            {
                int Id = Feed.Id;
                var service = new MITSService<NewsFeed>();
                var _feed = service.GetSingle(x => x.Id == Id);
                if (_feed == null)
                {
                    service.Add(Feed);
                }
                else
                {
                    _feed.News = Feed.News;
                    _feed.FeedLink = Feed.FeedLink;
                    _feed.Date = Feed.Date;
                    service.Save(_feed);
                }
                message = new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved);
            }
            catch (Exception ex)
            {
                message = new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError);
                mitsLogger.Error("Failed to save News Feed", ex);
            }
            return message;
        }
    }
}