﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class ContactUsBO
    {
        public bool SendEmail(SonyMailContent _sonymail)
        {
            try
            {
                Dictionary<object, object> _dictioanry = new Dictionary<object, object>();

                StringBuilder message = new StringBuilder();
                message.Append(Utility.BodyStart());
                message.Append(Utility.AddTextLine("Product Information"));
                message.Append(Utility.AddBreak(1));


                _dictioanry.Add("PoNumber", _sonymail.PoNumber);
                _dictioanry.Add("Requester", _sonymail.Requester);
                _dictioanry.Add("Email", _sonymail.Email);
                _dictioanry.Add("ContactPhone", _sonymail.ContactPhone);
                _dictioanry.Add("ItemName", _sonymail.ItemName);
                _dictioanry.Add("Cost", _sonymail.Cost);
                _dictioanry.Add("PurchaseFrom", _sonymail.PurchaseFrom);
                _dictioanry.Add("DateRequired", _sonymail.DateRequired);
                _dictioanry.Add("Description", _sonymail.Description);
                message.Append(Utility.ConvertToHtmlTable(_dictioanry));
                message.Append(Utility.AddBreak(1));
              
                message.Append(Utility.BodyEnd());
                if ((EmailHelper.SendMail("MITS Non-CED Take Back for Sony", message.ToString(),new string[] { _sonymail.Email }, null, null) == true))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

    public class SonyMailContent
    {
        public string PoNumber;
        public string Requester;
        public string Email;
        public string ContactPhone;
        public string ItemName;
        public string Cost;
        public string PurchaseFrom;
        public string DateRequired;
        public string Description;
    }
}
