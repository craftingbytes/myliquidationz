﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;

namespace Mits.BLL.BusinessObjects
{
    public class ServiceTypeGroupBO
    {
        MITSRepository<ServiceTypeGroup> mitsServiceTypeGroupRepo = new MITSRepository<ServiceTypeGroup>();

        public ServiceTypeGroupBO()
        {

        }

        public IQueryable GetAllServiceTypeGroup()
        {
            return mitsServiceTypeGroupRepo.GetAll() as IQueryable;
        }
        
        
        //Checking If Name already exists
        
        public bool Exists(string name)
        {
            IList<ServiceTypeGroup> _serviceTypeGroupObj = new MITSService<ServiceTypeGroup>().GetAll();
            bool _existsFlag = false;
            for (var i = 0; i < _serviceTypeGroupObj.Count; i++)
            {
                if ((name.Equals(_serviceTypeGroupObj[i].Name, StringComparison.OrdinalIgnoreCase)))
                    _existsFlag = true;
            }
            return _existsFlag;

        }

        //Checking If Description already exists

        public bool DescExists(string desc)
        {
            IList<ServiceTypeGroup> _serviceTypeGroupObj = new MITSService<ServiceTypeGroup>().GetAll();
            bool _existsFlag = false;
            for (var i = 0; i < _serviceTypeGroupObj.Count; i++)
            {
                if ((desc.Equals(_serviceTypeGroupObj[i].Description)))
                    _existsFlag = true;
            }
            return _existsFlag;

        }

        //Returning Id of existing ServiceTypeGroup

        public int ReturnId(string name)
        {
            IList<ServiceTypeGroup> _serviceTypeGroupObj = new MITSService<ServiceTypeGroup>().GetAll();
           int _returnValue = 0;
            for (var i = 0; i < _serviceTypeGroupObj.Count; i++)
            {
                if ((name.Equals(_serviceTypeGroupObj[i].Name, StringComparison.OrdinalIgnoreCase)))
                    _returnValue = _serviceTypeGroupObj[i].Id;
            }
            return _returnValue;

        }


        //Add ServiceTypeGroup detail in ServiceTypeGroup and return multiple values via Dictionary object

        public Dictionary<string, int> Add(string servicetypegroup, string name, string desc)
        {
            int _id = int.Parse(servicetypegroup);
            int _returnValue = default(int);

            //Return multiple values via Dictionary Object

            Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();
            _returnDictionaryValue["selectedId"] = _id;
            _returnDictionaryValue["duplicate"] = 0;

            //Check for New Entry

            if (_id == 0)
            {
                //New Entry by entering info in text boxes

                if ((this.Exists(name)) != true)
                {

                    ServiceTypeGroup _serviceTypeGroupObj = new ServiceTypeGroup();
                    _serviceTypeGroupObj.Name = name;
                    _serviceTypeGroupObj.Description = desc;
                    new MITSService<ServiceTypeGroup>().Add(_serviceTypeGroupObj);
                    _returnDictionaryValue["selectedId"] = _serviceTypeGroupObj.Id;
                }

                //Old Entry by entering info in text boxes

                else if((this.DescExists(desc))!=true)
                {
                    _returnValue = this.ReturnId(name);
                    ServiceTypeGroup _serviceTypeGroupObj = new MITSService<ServiceTypeGroup>().GetSingle(x => x.Id == _returnValue);
                    _serviceTypeGroupObj.Name = name;
                    _serviceTypeGroupObj.Description = desc;
                    new MITSService<ServiceTypeGroup>().Save(_serviceTypeGroupObj);
                    _returnDictionaryValue["selectedId"] = _serviceTypeGroupObj.Id;
                }
                else
                {
                    _returnDictionaryValue["duplicate"] = 1;

                }

            }
            
            //Check for old entry
            else
            {

                ServiceTypeGroup _serviceTypeGroupObj = new MITSService<ServiceTypeGroup>().GetSingle(x => x.Id == _id);
                
                if (_serviceTypeGroupObj.Description == null && desc != null)
                {
                    _serviceTypeGroupObj.Name = name;
                    _serviceTypeGroupObj.Description = desc;
                    new MITSService<ServiceTypeGroup>().Save(_serviceTypeGroupObj);
                    _returnDictionaryValue["selectedId"] = _serviceTypeGroupObj.Id;
                }
                else if (!(_serviceTypeGroupObj.Name.Equals(name, StringComparison.OrdinalIgnoreCase) && _serviceTypeGroupObj.Description.Equals(desc)))
                {
                    _serviceTypeGroupObj.Name = name;
                    _serviceTypeGroupObj.Description = desc;
                    new MITSService<ServiceTypeGroup>().Save(_serviceTypeGroupObj);
                    _returnDictionaryValue["selectedId"] = _serviceTypeGroupObj.Id;
                }

                else
                {
                    _returnDictionaryValue["duplicate"] = 1;
                }
            }
            return _returnDictionaryValue;
        }





    }
}
