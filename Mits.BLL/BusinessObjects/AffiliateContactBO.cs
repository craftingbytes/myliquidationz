﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System.Web.Mvc;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class AffiliateContactBO
    {
        MITSService<AffiliateContact> mitsAffliateContactSvc = null;
        MITSService<AffiliateMasterContact> mitsAffiliateMcService = null;
        MITSEntities entities = null;
        Logger mitsLogger = null;
        ISessionCookieValues _sessionValues;
        public AffiliateContactBO(ISessionCookieValues sessionValues)
        {
            _sessionValues = sessionValues;
            mitsAffliateContactSvc = new MITSService<AffiliateContact>();
            mitsAffiliateMcService = new MITSService<AffiliateMasterContact>();
            entities = new MITSEntities();
            mitsLogger = new Logger(this.GetType());
        }

        private bool SendUserCredentials(AffiliateContact _aff, string password)
        {
            bool returnValue = false;
            try
            {
                if (ConfigHelper.SendUserCredentials == "true")
                {
                    Dictionary<object, object> _dictioanryInfo = new Dictionary<object, object>();
                    Dictionary<object, object> _dictioanryForgot = new Dictionary<object, object>();
                    StringBuilder message = new StringBuilder();
                    //message.Append("<html><body style='font-family:Calibri; font-size:11pt;'>Dear User,<br/><br/>");
                    message.Append(Utility.BodyStart());
                    message.Append(Utility.AddTextLine("Dear User,"));
                    message.Append(Utility.AddBreak(2));
                    message.Append(Utility.AddTextLine("Account Information:"));
                    message.Append(Utility.AddBreak(1));

                    _dictioanryInfo.Add("Username", _aff.UserName);
                    _dictioanryInfo.Add("Email", _aff.Email);
                    _dictioanryInfo.Add("Password", password);
                    message.Append(Utility.ConvertToHtmlTable(_dictioanryInfo));
                    message.Append(Utility.AddBreak(2));
                    message.Append(Utility.AddTextLine("In case of forgot password, use below information on forgot password screen:"));
                    message.Append(Utility.AddBreak(1));
                    _dictioanryForgot.Add("Security Question", _aff.SecurityQuestion.Question);
                    _dictioanryForgot.Add("Answer", _aff.Answer);
                    message.Append(Utility.ConvertToHtmlTable(_dictioanryForgot));
                    message.Append(Utility.AddBreak(1));
                    message.Append(Utility.AddTextLine("Thank you,"));
                    message.Append(Utility.AddBreak(1));
                    message.Append(Utility.AddTextLine("E-World Online"));
                    message.Append(Utility.BodyEnd());
                    if ((EmailHelper.SendMail("User Account Detail", message.ToString(), new string[] { _aff.Email }, null, new string[] { ConfigHelper.AdminEmail }) == true))
                    {
                        returnValue = true;
                    }
                    else
                        returnValue = false;
                }
                else if (ConfigHelper.SendUserCredentials == "false")
                {
                    Dictionary<object, object> _dictioanryInfo = new Dictionary<object, object>();
                    Dictionary<object, object> _dictioanryForgot = new Dictionary<object, object>();
                    StringBuilder message = new StringBuilder();
                    //message.Append("<html><body style='font-family:Calibri; font-size:11pt;'>Dear User,<br/><br/>");
                    message.Append(Utility.BodyStart());
                    message.Append(Utility.AddTextLine("Dear Admin,"));
                    message.Append(Utility.AddBreak(2));
                    message.Append(Utility.AddTextLine("Following user has been created:"));
                    message.Append(Utility.AddBreak(1));

                    _dictioanryInfo.Add("Username", _aff.UserName);
                    _dictioanryInfo.Add("Email", _aff.Email);
                    _dictioanryInfo.Add("Password", password);

                    message.Append(Utility.ConvertToHtmlTable(_dictioanryInfo));
                    message.Append(Utility.AddBreak(2));
                    message.Append(Utility.AddTextLine("In case of forgot password then use below information on forgot password screen:"));
                    message.Append(Utility.AddBreak(1));
                    _dictioanryForgot.Add("Security Question", _aff.SecurityQuestion.Question);
                    _dictioanryForgot.Add("Answer", _aff.Answer);
                    message.Append(Utility.ConvertToHtmlTable(_dictioanryForgot));
                    message.Append(Utility.AddBreak(1));
                    message.Append(Utility.AddTextLine("Thank you,"));
                    message.Append(Utility.AddBreak(1));
                    message.Append(Utility.AddTextLine("E-World Online"));
                    message.Append(Utility.BodyEnd());
                    if ((EmailHelper.SendMail("User Account Detail", message.ToString(), new string[] { ConfigHelper.AdminEmail }, null, null) == true))
                    {
                        returnValue = true;
                    }
                    else
                        returnValue = false;
                }
               
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to send email to: " + _aff.Email + "", ex);
            }
            return returnValue;
        }
        public AffiliateContact GetAffiliateContact(int id)
        {
            AffiliateContact _affContact = null;
            try
            {
                _affContact= mitsAffliateContactSvc.GetSingle(d => d.Id == id);
                //if (ConfigHelper.ComputePwdHash == "true")
                //{
                //    _affContact.Password = Utility.CreateHash(_affContact.Password);
                //}
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get an entity contact: "+id+"", ex);
            }
            return _affContact;
        }

        public IQueryable<AffiliateContact> GetAffiliateContactsPaged(int pageIndex, int pageSize, out int totalRecords, int affiliateId)
        {
            IQueryable<AffiliateContact> affiliateContactList = null;
            try
            {
                affiliateContactList = mitsAffliateContactSvc.MitsEntities.AffiliateContacts.Where(x => x.AffiliateId == affiliateId);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get entity contacts of "+affiliateId+"", ex);
            }
            totalRecords = affiliateContactList.Count();
            return affiliateContactList.OrderBy(e => e.FirstName).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public List<Message> Add(AffiliateContact _affiliateContactModel, string services)
        {
            List<Message> messages = new List<Message>();
            string unHasedPassword = string.Empty;
            try
            {
                AffiliateContact affiliateContact = mitsAffliateContactSvc.GetSingle(a => a.UserName.ToLower() == _affiliateContactModel.UserName.ToLower());
                if (affiliateContact != null)
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.UserNameExists));
                }
                affiliateContact = mitsAffliateContactSvc.GetSingle(a => a.Email.ToLower() == _affiliateContactModel.Email.ToLower());
                if (affiliateContact != null)
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.EmailExists));
                }
                if (messages.Count > 0)
                    return messages;

                State state = new MITSService<State>().GetSingle(t => t.Id == _affiliateContactModel.StateId);
                _affiliateContactModel.State = state;

                City city = new MITSService<City>().GetSingle(t => t.Id == _affiliateContactModel.CityId);
                _affiliateContactModel.City = city;

                if (_affiliateContactModel.SecurityQuestionId != 0)
                {
                    SecurityQuestion securityQuestion = new MITSService<SecurityQuestion>().GetSingle(t => t.Id == _affiliateContactModel.SecurityQuestionId);
                    _affiliateContactModel.SecurityQuestion = securityQuestion;
                }
                else
                {
                    _affiliateContactModel.SecurityQuestionId = null;
                    _affiliateContactModel.Answer = null;
                    _affiliateContactModel.SecurityQuestion = null;
                }
                
                unHasedPassword = _affiliateContactModel.Password;
                if (ConfigHelper.ComputePwdHash == "true")
                {
                    _affiliateContactModel.Password = Utility.CreateHash(_affiliateContactModel.Password);
                }

                mitsAffliateContactSvc.Add(_affiliateContactModel);
                if (!string.IsNullOrEmpty(services))
                {
                    string[] selectedServices = services.Split(',');
                    foreach (string service in selectedServices)
                    {
                        int serviceId = int.Parse(service);
                        ServiceType srv = mitsAffliateContactSvc.MitsEntities.ServiceTypes.Where(t => t.Id == serviceId).FirstOrDefault();
                        AffiliateContactService selectedService = new AffiliateContactService();
                        selectedService.ServiceType = srv;
                        selectedService.AffiliateContact = _affiliateContactModel;
                        selectedService.Active = true;
                        mitsAffliateContactSvc.MitsEntities.AffiliateContactServices.Add(selectedService);
                    }
                    mitsAffliateContactSvc.MitsEntities.SaveChanges();
                }
                AffiliateContact _aff = this.GetAffiliateContact(_affiliateContactModel.Id);
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.AffiliateContactId, _aff.Id);
                
                //Mail to user after successful creation
                bool success = this.SendUserCredentials(_affiliateContactModel, unHasedPassword);
                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                mitsLogger.Error("Failed to add entity contact: "+_affiliateContactModel.Id+"", ex);
            }
            return messages;
        }

        public List<Message> Update(AffiliateContact _affiliateContactModel, string services,bool passwordIsChanged)
        {
            List<Message> messages = new List<Message>();
            try
            {
                AffiliateContact affiliateContact = mitsAffliateContactSvc.GetSingle(a => a.UserName.ToLower() == _affiliateContactModel.UserName.ToLower() && a.Id != _affiliateContactModel.Id);
                if (affiliateContact != null)
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.UserNameExists));
                }
                affiliateContact = mitsAffliateContactSvc.GetSingle(a => a.Email.ToLower() == _affiliateContactModel.Email.ToLower() && a.Id != _affiliateContactModel.Id);
                if (affiliateContact != null)
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.EmailExists));
                }

                if (messages.Count > 0)
                    return messages;

                State state = mitsAffliateContactSvc.MitsEntities.States.Where(t => t.Id == _affiliateContactModel.StateId).FirstOrDefault();
                _affiliateContactModel.State = state;

                City city = mitsAffliateContactSvc.MitsEntities.Cities.Where(t => t.Id == _affiliateContactModel.CityId).FirstOrDefault();
                _affiliateContactModel.City = city;

                if (_affiliateContactModel.SecurityQuestionId != 0)
                {
                    SecurityQuestion securityQuestion = mitsAffliateContactSvc.MitsEntities.SecurityQuestions.Where(t => t.Id == _affiliateContactModel.SecurityQuestionId).FirstOrDefault();
                    _affiliateContactModel.SecurityQuestion = securityQuestion;
                }
                else
                {
                    _affiliateContactModel.SecurityQuestion = null;
                    _affiliateContactModel.SecurityQuestionId = null;
                    _affiliateContactModel.Answer = null;
                }

                if (ConfigHelper.ComputePwdHash == "true")
                {

                    //Check the password is changed                    
                    if (passwordIsChanged)
                    {
                        _affiliateContactModel.Password = Utility.CreateHash(_affiliateContactModel.Password);
                    }
                }
               
                mitsAffliateContactSvc.Save(_affiliateContactModel);

                //Add selected services to the context and save
                if (!string.IsNullOrEmpty(services))
                {
                    string[] selectedServices = services.Split(',');
                    IList<AffiliateContactService> existingServices = mitsAffliateContactSvc.MitsEntities.AffiliateContactServices.Where(x => x.AffiliateContactId == _affiliateContactModel.Id).ToList();


                    //foreach (AffiliateContactService existingSvc in existingServices)
                    //existingSvc.Active = false;

                    foreach (string selectedSvc in selectedServices)
                    {
                        bool newSvc = true;
                        foreach (AffiliateContactService existingSvc in existingServices)
                        {
                            if (existingSvc.ServiceType.Id.ToString() == selectedSvc)
                            {
                                if (existingSvc.Active == false)
                                {
                                    existingSvc.Active = true;
                                }
                                newSvc = false;
                                break;
                            }
                        }
                        if (newSvc == true)
                        {
                            int serviceId = int.Parse(selectedSvc);
                            ServiceType srv = mitsAffliateContactSvc.MitsEntities.ServiceTypes.Where(t => t.Id == serviceId).FirstOrDefault();
                            AffiliateContactService selectedService = new AffiliateContactService();
                            selectedService.ServiceType = srv;
                            selectedService.AffiliateContact = _affiliateContactModel;
                            selectedService.Active = true;

                            mitsAffliateContactSvc.MitsEntities.AffiliateContactServices.Add(selectedService);
                        }
                    }
                    foreach (AffiliateContactService existingSvc in existingServices)
                    {
                        bool isUnchecked = true;
                        foreach (string selectedSvc in selectedServices)
                        {
                            if (existingSvc.ServiceType.Id.ToString() == selectedSvc)
                            {
                                isUnchecked = false;
                            }
                        }
                        if (isUnchecked == true && existingSvc.Active == true)
                            existingSvc.Active = false;
                    }
                    mitsAffliateContactSvc.MitsEntities.SaveChanges();
                }
                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));  
            }
            catch (Exception ex)
            {
               
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                mitsLogger.Error("Failed to update entity contact: " + _affiliateContactModel.Id + "", ex);
            }
            return messages;
        }

        //public IQueryable<AffiliateContact> GetAffiliateContactsPaged(int pageIndex, int pageSize, out int totalRecords, int affiliateId)
        //{
        //    IQueryable<AffiliateContact> affiliateContactList = null;
        //    try
        //    {
        //        affiliateContactList = mitsAffliateContactSvc.MitsEntities.AffiliateContacts.Where(x => x.AffiliateId == affiliateId);
        //    }
        //    catch (Exception ex)
        //    {
        //        mitsLogger.Error("Failed to get entity contacts of " + affiliateId + "", ex);
        //    }
        //    totalRecords = affiliateContactList.Count();
        //    return affiliateContactList.OrderBy(e => e.FirstName).Skip(pageIndex * pageSize).Take(pageSize);

        //}


        public IQueryable<AffiliateMasterContact> GetAffiliateMasterContacts(int pageIndex, int pageSize, out int totalRecords, int contactId)
        {
            IQueryable<AffiliateMasterContact> affiliateContactList = null;
            try
            {
                affiliateContactList = mitsAffliateContactSvc.MitsEntities.AffiliateMasterContacts.Where(x => x.MasterAffiliateContactId == contactId);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get entity master contacts of " + contactId + "", ex);
            }
            totalRecords = affiliateContactList.Count();
            return affiliateContactList.OrderByDescending(e => e.DefaultUser).ThenBy(e => e.AffiliateContact1.UserName).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public bool AddMasterContact( AffiliateMasterContact masterContact)
        {
            Int32 id = 0;

            try
            {
                IQueryable<AffiliateMasterContact> affiliateContactList = null;
                affiliateContactList = mitsAffliateContactSvc.MitsEntities.AffiliateMasterContacts.Where(x => x.MasterAffiliateContactId == masterContact.MasterAffiliateContactId && x.DefaultUser == true);
                if (affiliateContactList == null || affiliateContactList.Count() == 0)
                    masterContact.DefaultUser = true;
                entities.AffiliateMasterContacts.Add(masterContact);
                entities.SaveChanges();
                id = masterContact.Id;

            }
            catch (Exception ex) {
                id = 0;
                mitsLogger.Error("Failed to add master contact", ex);
            }

            if (id == 0)
                return false;
            else
                return true;
        }

        public bool SetMasterContactDefault(Int32 masterContactId, Int32 id)
        {
            try
            {

                AffiliateMasterContact mc1 = mitsAffiliateMcService.GetSingle(x => x.MasterAffiliateContactId == masterContactId && x.DefaultUser == true);
                mc1.DefaultUser = false;
                mitsAffiliateMcService.Save(mc1);

                AffiliateMasterContact mc2 = mitsAffiliateMcService.GetSingle(x => x.Id == id);
                mc2.DefaultUser = true;
                mitsAffiliateMcService.Save(mc2);
                
                //var affiliateContactList = entities.AffiliateMasterContacts.Where(x => x.MasterAffiliateContactId == masterContactId && (x.Id == id || x.DefaultUser == true));

                //foreach( AffiliateMasterContact  mc in affiliateContactList )
                //{
                //    if (mc.DefaultUser == true)
                //        mc.DefaultUser = false;
                //    if (mc.Id == id)
                //        mc.DefaultUser = true;


                //    mitsAffiliateMcService.Save(mc);
                //}
                //mitsAffiliateMcService.MitsEntities.SaveChanges();

            }
            catch (Exception ex)
            {
                mitsLogger.Error("Unable to set default.", ex);
                return false;
            }

            return true;
        }

        public AffiliateContact GetDefaultAssociateContact(int id)
        {
            AffiliateContact _affContact = null;
            try
            {
                AffiliateMasterContact mc = mitsAffiliateMcService.GetSingle(x => x.MasterAffiliateContactId == id && x.DefaultUser == true);
                _affContact = mitsAffliateContactSvc.GetSingle(d => d.Id == mc.AssociateContactId);
                //if (ConfigHelper.ComputePwdHash == "true")
                //{
                //    _affContact.Password = Utility.CreateHash(_affContact.Password);
                //}
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get an entity contact: " + id + "", ex);
            }
            return _affContact;
        }

        public AffiliateMasterContact GetAffiliateMasterById(Int32 id)
        {
            return new MITSService<AffiliateMasterContact>().GetSingle(i => i.Id == id);
        }

        public bool DeleteAffiliateMaster(Int32 id)
        {
            try {
                AffiliateMasterContact mc = GetAffiliateMasterById(id);
                new MITSService<AffiliateMasterContact>().Delete(mc);
            }
            catch (Exception)
            {
                return false;
            }
            
            return true;
        }
        
    }
}
