﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System.Web.Mvc;
using Mits.Common;

namespace Mits.BLL
{
    //public class AffiliateService : ServiceBase<IAffiliateModel>, IAffiliateService
    public class AffiliateBO
    {
        MITSService<Affiliate> mitsAffliateSvc = null;
        Logger mitsLogger = null;
        public AffiliateBO()
        {
            mitsAffliateSvc = new MITSService<Affiliate>();
            mitsLogger = new Logger(this.GetType());
        }

        public Affiliate GetAffiliate(int id)
        {
            Affiliate _aff = null;
            try
            {
                _aff= mitsAffliateSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get an entity: "+id+"", ex);
            }
            return _aff;
        }
        public IList<AffiliateType> GetAffiliateType()
        {
            IList<AffiliateType> _affType;
            IList<AffiliateType> _unSortedList = new MITSService<AffiliateType>().GetAll();
            IEnumerable<AffiliateType> enumSorted = _unSortedList.OrderBy(f => f.Type);
            try
            {
                _affType = enumSorted.ToList();
            }
            catch (Exception ex)
            {
                _affType = _unSortedList;
            }
            AffiliateType _select = new AffiliateType();
            _select.Id = 0;
            _select.Type = "Select";
            _affType.Insert(0, _select);
            return _affType;
        }

        public List<Message> Add(Affiliate _affiliateModel, string services, int roleId)
        {
            List<Message> messages = new List<Message>();
            try
            {
                State state = new MITSService<State>().GetSingle(t => t.Id == _affiliateModel.StateId);
                _affiliateModel.State = state;

                City city = new MITSService<City>().GetSingle(t => t.Id == _affiliateModel.CityId);
                _affiliateModel.City = city;

                AffiliateType affiliateType = new MITSService<AffiliateType>().GetSingle(t => t.Id == _affiliateModel.AffiliateTypeId);
                _affiliateModel.AffiliateType = affiliateType;

                mitsAffliateSvc.Add(_affiliateModel);
                if (roleId != 0)
                {
                    AffiliateRole affiliateRole = new AffiliateRole();
                    affiliateRole.AffiliateId = _affiliateModel.Id;
                    affiliateRole.RoleId = roleId;
                    new MITSService<AffiliateRole>().Add(affiliateRole);
                }
                if (!string.IsNullOrEmpty(services))
                {
                    string[] selectedServices = services.Split(',');
                    foreach (string service in selectedServices)
                    {
                        int serviceId = int.Parse(service);
                        ServiceType srv = mitsAffliateSvc.MitsEntities.ServiceTypes.Where(t => t.Id == serviceId).FirstOrDefault();
                        AffiliateService selectedService = new AffiliateService();
                        selectedService.ServiceType = srv;
                        selectedService.Affiliate = _affiliateModel;
                        selectedService.Active = true;
                        mitsAffliateSvc.MitsEntities.AffiliateServices.Add(selectedService);
                    }
                    mitsAffliateSvc.MitsEntities.SaveChanges();
                    //messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
                }
                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                mitsLogger.Error("Failed to add an entity: "+_affiliateModel.Name+"", ex);
            }
            return messages;
        }

        public List<Message> Update(Affiliate _affiliateModel, string services, int roleId)
        {
            List<Message> messages = new List<Message>();
            try
            {
                State state = mitsAffliateSvc.MitsEntities.States.Where(t => t.Id == _affiliateModel.StateId).FirstOrDefault();
                _affiliateModel.State = state;

                City city = mitsAffliateSvc.MitsEntities.Cities.Where(t => t.Id == _affiliateModel.CityId).FirstOrDefault();
                _affiliateModel.City = city;

                AffiliateType affiliateType = mitsAffliateSvc.MitsEntities.AffiliateTypes.Where(t => t.Id == _affiliateModel.AffiliateTypeId).FirstOrDefault();
                _affiliateModel.AffiliateType = affiliateType;

                //Add selected services to the context and save
                if (!string.IsNullOrEmpty(services))
                {
                    string[] selectedServices = services.Split(',');
                    IList<AffiliateService> existingServices = mitsAffliateSvc.MitsEntities.AffiliateServices.Where(x => x.AffiliateId == _affiliateModel.Id).ToList();


                    //foreach (AffiliateService existingSvc in existingServices)
                    //    existingSvc.Active = false;

                    foreach (string selectedSvc in selectedServices)
                    {
                        bool newSvc = true;
                        foreach (AffiliateService existingSvc in existingServices)
                        {
                            if (existingSvc.ServiceType.Id.ToString() == selectedSvc)
                            {
                                if (existingSvc.Active == false)
                                {
                                    existingSvc.Active = true;
                                }
                                newSvc = false;
                                break;
                            }
                        }
                        if (newSvc == true)
                        {
                            int serviceId = int.Parse(selectedSvc);
                            ServiceType srv = mitsAffliateSvc.MitsEntities.ServiceTypes.Where(t => t.Id == serviceId).FirstOrDefault();
                            AffiliateService selectedService = new AffiliateService();
                            selectedService.ServiceType = srv;
                            selectedService.Affiliate = _affiliateModel;
                            selectedService.Active = true;

                            mitsAffliateSvc.MitsEntities.AffiliateServices.Add(selectedService);
                        }
                    }
                    foreach (AffiliateService existingSvc in existingServices)
                    {
                        bool isUnchecked = true;
                        foreach (string selectedSvc in selectedServices)
                        {
                            if (existingSvc.ServiceType.Id.ToString() == selectedSvc)
                            {
                                isUnchecked = false;
                            }
                        }
                        if (isUnchecked == true && existingSvc.Active == true)
                            existingSvc.Active = false;
                    }

                    mitsAffliateSvc.MitsEntities.SaveChanges();
                }
                MITSService<AffiliateRole> affiliateRoleService = new MITSService<AffiliateRole>();
                AffiliateRole affiliateRole = affiliateRoleService.GetSingle(x => x.AffiliateId == _affiliateModel.Id);
                if (affiliateRole == null)
                {
                    if (roleId != 0)
                    {
                        affiliateRole = new AffiliateRole() { AffiliateId = _affiliateModel.Id, RoleId = roleId };
                        affiliateRoleService.Add(affiliateRole);
                    }
                }
                else
                {
                    if (roleId != 0)
                    {
                        affiliateRole.RoleId = roleId;
                        affiliateRoleService.Save(affiliateRole);
                    }
                    else
                    {
                        affiliateRoleService.Delete(affiliateRole);
                    }
                }
                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));  
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                mitsLogger.Error("Failed to update entity: " + _affiliateModel.Name + "", ex);
            }
            return messages;
        }

        //public void Delete(Affiliate _affiliateModel)
        //{
        //    try
        //    {
        //        //Delete assoicated service, i.e. child entity first
        //        AffiliateService _affService = new MITSService<AffiliateService>().GetSingle(s => s.Id == _affiliateModel.ServiceTypeId);
        //        if (_affService != null)
        //            new MITSService<AffiliateService>().Delete(_affService);

        //        mitsAffliateSvc.Delete(_affiliateModel);



        //    }
        //    catch (Exception ex)
        //    {
        //        // this.LogError("Error getting Adding Affiliate", e);
        //    }
        //}

        public List<Affiliate> GetAffiliatesPaged(int pageIndex, int pageSize, string searchString,string entity, out int totalRecords)
        {

            
            //DocumentLibEntities obj = new DocumentLibEntities();
            //DateTime uploadDate;
           // bool isDate = DateTime.TryParse(searchString, out uploadDate);

            //IQueryable<Affiliate> affiliateList = mitsAffliateSvc.MitsEntities.Affiliates.Where(x => x.Name.Contains(searchString));
            IList<Affiliate> affiliateList = null;
            List<Affiliate> _affList = null;
            try
            {
                if (string.IsNullOrEmpty(searchString) && entity == "0")
                {
                    affiliateList = mitsAffliateSvc.GetAll(x=>x.AffiliateType!=null);
                    _affList = affiliateList.ToList();
                    _affList = _affList.OrderBy(f => f.Name).ToList();
                }
                else
                {
                    affiliateList = mitsAffliateSvc.GetAll(x=>x.AffiliateType!=null);
                    _affList = affiliateList.ToList();
                    _affList = _affList.OrderBy(f => f.Name).ToList();
                    if (string.IsNullOrEmpty(searchString) != true)
                    {
                        IQueryable<Affiliate> _affiliateList = mitsAffliateSvc.MitsEntities.Affiliates.Where(x => x.Name.Contains(searchString));
                        List<Affiliate> entityList = _affiliateList.ToList();
                        //IEnumerable<Affiliate> enumList = _affList.Where(f => (!string.IsNullOrEmpty(searchString) ? f.Name.Contains(searchString) : true));
                        //List<Affiliate> entityList = enumList.ToList();
                        if (entityList.Count > 0)
                        {
                            _affList.Clear();
                            _affList.AddRange(entityList);
                        }
                        else
                        {
                            _affList.Clear();
                        }
                    }
                    if (entity != "0")
                    {
                        int ent = int.Parse(entity);
                        IEnumerable<Affiliate> enumList = _affList.Where(x =>x.AffiliateTypeId==ent);
                        List<Affiliate> entityTypeList = enumList.ToList();
                        if (entityTypeList.Count > 0)
                        {
                            _affList.Clear();
                            _affList.AddRange(entityTypeList);
                        }
                        else
                        {
                            _affList.Clear();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get entity list of "+searchString+"", ex);
            }
            if (_affList != null)
            {
                totalRecords = _affList.Count();
                return _affList.OrderBy(e => e.Name).Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }
            totalRecords = 0;
            return _affList;
            
         }

        public List<Document> GetDocsByPDataId(int pdId)
        {

            ForeignTable _forgeignTable = mitsAffliateSvc.MitsEntities.ForeignTables.ToList()
                .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "Affiliate");

            int invoiceTableId = _forgeignTable != null ? _forgeignTable.Id : 0;

            List<DocumentRelation> _docRels = mitsAffliateSvc.MitsEntities.DocumentRelations
                .Where(i => i.ForeignTableID == invoiceTableId && i.ForeignID == pdId).ToList();


            List<Document> _docs = new List<Document>();
            foreach (DocumentRelation docrel in _docRels)
            {
                _docs.Add(mitsAffliateSvc.MitsEntities.Documents.FirstOrDefault(i => i.Id == docrel.DocumentID));
            }


            return _docs;
        }

        public int SaveAttachment(Document document)
        {
            try
            {
                DocumentType documentType = new DocumentType();
                String extension = System.IO.Path.GetExtension(document.UploadFileName);

                document.DocumentType = mitsAffliateSvc.MitsEntities.DocumentTypes.FirstOrDefault<DocumentType>(i => i.FileExtension == extension);
                mitsAffliateSvc.MitsEntities.Documents.Add(document);
                int affected = mitsAffliateSvc.MitsEntities.SaveChanges();

                return document.Id;
            }
            catch (Exception ex)
            {

            }
            return -1;
        }

        public bool SaveAttachmentRelation(DocumentRelation docRelation)
        {
            try
            {
                docRelation.ForeignTableID = mitsAffliateSvc.MitsEntities.ForeignTables.ToList()
                    .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "Affiliate").Id;

                docRelation.Document = mitsAffliateSvc.MitsEntities.Documents.ToList().FirstOrDefault<Document>(i => i.Id == docRelation.DocumentID);
                docRelation.ForeignTable = mitsAffliateSvc.MitsEntities.ForeignTables.ToList()
                    .FirstOrDefault<ForeignTable>(i => i.Id == docRelation.ForeignTableID);


                mitsAffliateSvc.MitsEntities.DocumentRelations.Add(docRelation);
                int affected = mitsAffliateSvc.MitsEntities.SaveChanges();

                return affected > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool DeleteDocument(int id) 
        {
            MITSService<Document> mitsDocumentSvc = new MITSService<Document>();
            MITSService<DocumentRelation> mitsDocumentRelationSvc = new MITSService<DocumentRelation>();

            foreach (DocumentRelation _documentRelation in mitsDocumentRelationSvc.GetAll().Where(X => X.DocumentID == id))
            {
                mitsDocumentRelationSvc.Delete(_documentRelation);
            }

            Document _document = mitsDocumentSvc.GetSingle(X => X.Id == id);
            mitsDocumentSvc.Delete(_document);
            return true;
        }

        public bool DeleteAffiliate(int id)
        {
            MITSService<Affiliate> mitsAffiliateSvc = new MITSService<Affiliate>();
            MITSService<AffiliateAccreditation> mitsAffiliateAccreditationSvc = new MITSService<AffiliateAccreditation>();
            foreach (AffiliateAccreditation _affiliateAccreditation in mitsAffiliateAccreditationSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsAffiliateAccreditationSvc.Delete(_affiliateAccreditation);
            }
            
            MITSService<AffiliateContact> mitsAffiliateContactSvc = new MITSService<AffiliateContact>();
            foreach (AffiliateContact _affiliateContact in mitsAffiliateContactSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsAffiliateContactSvc.Delete(_affiliateContact);
            }

            MITSService<AffiliateContractRate> mitsAffiliateContractRateSvc = new MITSService<AffiliateContractRate>();
            foreach (AffiliateContractRate _affiliateContractRate in mitsAffiliateContractRateSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsAffiliateContractRateSvc.Delete(_affiliateContractRate);
            }

            MITSService<AffiliateService> mitsAffiliateServiceSvc = new MITSService<AffiliateService>();
            foreach (AffiliateService _affiliateService in mitsAffiliateServiceSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsAffiliateServiceSvc.Delete(_affiliateService);
            }

            MITSService<AffiliateState> mitsAffiliateStateSvc = new MITSService<AffiliateState>();
            foreach (AffiliateState _affiliateState in mitsAffiliateStateSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsAffiliateStateSvc.Delete(_affiliateState);
            }

            MITSService<AffiliateTarget> mitsAffiliateTargetSvc = new MITSService<AffiliateTarget>();
            foreach (AffiliateTarget _affiliateTarget in mitsAffiliateTargetSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsAffiliateTargetSvc.Delete(_affiliateTarget);
            }

            MITSService<Invoice> mitsInvoiceSvc = new MITSService<Invoice>();
            foreach (Invoice _invoice in mitsInvoiceSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsInvoiceSvc.Delete(_invoice);
            }

            MITSService<MailBackDevice> mitsMailBackDeviceSvc = new MITSService<MailBackDevice>();
            foreach (MailBackDevice _mailBackDevice in mitsMailBackDeviceSvc.GetAll().Where(X => X.ManufacturerId == id))
            {
                mitsMailBackDeviceSvc.Delete(_mailBackDevice);
            }

            MITSService<ManufacturerPolicy> mitsManufacturerPolicySvc = new MITSService<ManufacturerPolicy>();
            foreach (ManufacturerPolicy _manufacturerPolicy in mitsManufacturerPolicySvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsManufacturerPolicySvc.Delete(_manufacturerPolicy);
            }

            MITSService<Payment> mitsPaymentSvc = new MITSService<Payment>();
            foreach (Payment _payment in mitsPaymentSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsPaymentSvc.Delete(_payment);
            }

            MITSService<RecyclerProductWeight> mitsRecyclerProductWeightSvc = new MITSService<RecyclerProductWeight>();
            foreach (RecyclerProductWeight _recyclerProductWeight in mitsRecyclerProductWeightSvc.GetAll().Where(X => X.RecyclerId == id))
            {
                mitsRecyclerProductWeightSvc.Delete(_recyclerProductWeight);
            }

            MITSService<ReminderGroupAffiliate> mitsReminderGroupAffiliateSvc = new MITSService<ReminderGroupAffiliate>();
            foreach (ReminderGroupAffiliate _reminderGroupAffiliate in mitsReminderGroupAffiliateSvc.GetAll().Where(X => X.AffiliateId == id))
            {
                mitsReminderGroupAffiliateSvc.Delete(_reminderGroupAffiliate);
            }
           
            Affiliate _document = mitsAffiliateSvc.GetSingle(X => X.Id == id);
            mitsAffiliateSvc.Delete(_document);
            return true;
        }
    }
}

