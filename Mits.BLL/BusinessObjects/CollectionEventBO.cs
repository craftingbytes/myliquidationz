﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;

namespace Mits.BLL.BusinessObjects
{
    public class CollectionEventBO
    {

        public List<CollectionEvent> GetCollectionEvents(int pageIndex, int pageSize, int? stateId, out int totalRecords)
        {
            try
            {
                MITSService<CollectionEvent> service = new MITSService<CollectionEvent>();
                var events = service.GetAll(x => (!stateId.HasValue || x.StateId == stateId)).OrderBy(x => x.State.Name);
                totalRecords = events.ToList().Count;
                return events.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
                totalRecords = 0;
                return new List<CollectionEvent>();
            }
        }

        public bool AddCollectionEvent(CollectionEvent collectionEvent)
        {
            try
            {
                new MITSService<CollectionEvent>().Add(collectionEvent);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public CollectionEvent GetCollectionEventById(int id)
        {
            try
            {
                return new MITSService<CollectionEvent>().GetSingle(x => x.Id == id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateCollectionEvent(CollectionEvent collectionEvent)
        {
            try
            {
                new MITSService<CollectionEvent>().Save(collectionEvent);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteCollectionEvent(int id)
        {
            try
            {
                var collectionEvent = GetCollectionEventById(id);
                if (collectionEvent != null)
                {
                    new MITSService<CollectionEvent>().Delete(collectionEvent);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }
    }
}
