﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class ClientAddressBO
    {
        private Logger mitsLogger = null;
        ISessionCookieValues _sessionValues = null;

        public ClientAddressBO(ISessionCookieValues sessionValues)
        {
            _sessionValues = sessionValues;
            mitsLogger = new Logger(this.GetType());
        }

        public IList<ClientAddress> GetClientAddresses(int pageIndex, int pageSize, int? affiliateId, out int totalRecords)
        {
            MITSService<ClientAddress> service = new MITSService<ClientAddress>();
            IList<ClientAddress> addresses = new List<ClientAddress>();
            int roleId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            try
            {
                if (roleId == (int)Constants.AffiliateContactRole.Administrator ||
                        roleId == (int)Constants.AffiliateContactRole.Finance ||
                        roleId == (int)Constants.AffiliateContactRole.Manager ||
                        roleId == (int)Constants.AffiliateContactRole.ExecutiveAssistant)
                {
                    if (affiliateId.HasValue)
                    {
                        addresses = service.GetAll(x => x.AffiliateId == affiliateId.Value).OrderBy(x => x.AffiliateId).ThenBy(x => x.CompanyName).ToList();
                    }
                    else
                    {
                        addresses = service.GetAll().OrderBy(x => x.AffiliateId).ThenBy(x => x.CompanyName).ToList();
                    }
                    totalRecords = addresses.Count;
                    addresses = addresses.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                    return addresses;
                }
                else if (roleId == (int)Constants.AffiliateContactRole.Processor)
                {
                    int linqAffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                    addresses = service.GetAll(x => x.AffiliateId == linqAffiliateId).OrderBy(x => x.CompanyName).ToList();
                    totalRecords = addresses.Count;
                    addresses = addresses.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                    return addresses;
                }
                totalRecords = 0;
                return addresses;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get client address list", ex);
                totalRecords = 0;
                return addresses;
            }
        }

        public ClientAddress GetClientAddressById(int id)
        {
            try
            {
                return new MITSService<ClientAddress>().GetSingle(x => x.Id == id);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get client address by Id: " + id, ex);
                return null;
            }
        }

        public bool AddClientAddress(ClientAddress clientAddress)
        {
            try
            {
                new MITSService<ClientAddress>().Add(clientAddress);
                PickUpAddress pickUp = new PickUpAddress();
                pickUp.ClientAddressId = clientAddress.Id;
                pickUp.CompanyName = clientAddress.CompanyName;
                pickUp.Address = clientAddress.Address;
                pickUp.StateId = clientAddress.StateId;
                pickUp.CityId = clientAddress.CityId;
                pickUp.Zip = clientAddress.Zip;
                pickUp.Phone = clientAddress.Phone;
                pickUp.CollectionMethodId = 3;
                new MITSService<PickUpAddress>().Add(pickUp);
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to add client address ", ex);
                return false;
            }
        }

        public bool UpdateClientAddress(ClientAddress clientAddress)
        {
            try
            {
                new MITSService<ClientAddress>().Save(clientAddress);
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to update client address ", ex);
                return false;
            }
        }

        public bool DeleteClientAddress(int id)
        {
            try
            {
                var client = GetClientAddressById(id);
                if (client != null)
                {
                    var service = new MITSService<PickUpAddress>();
                    List<PickUpAddress> pickUps = service.GetAll(x => x.ClientAddressId == id).ToList();
                    foreach (var pickUp in pickUps)
                    {
                        service.Delete(pickUp);
                    }
                    new MITSService<ClientAddress>().Delete(client);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to delete client address ", ex);
                return false;
            }
            return false;
        }

        public IList<PickUpAddress> GetPickUpAddresses(Int32 clientAddressId, Int32 pageIndex, Int32 pageSize, out Int32 totalRecords)
        {
            MITSService<PickUpAddress> service = new MITSService<PickUpAddress>();
            IList<PickUpAddress> addresses = new List<PickUpAddress>();
            try
            {
                addresses = service.GetAll(x => x.ClientAddressId == clientAddressId).OrderBy(x => x.CompanyName).ToList();
                totalRecords = addresses.Count;
                addresses = addresses.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                return addresses;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get pick up address list", ex);
                totalRecords = 0;
                return addresses;
            }
        }

        public PickUpAddress GetPickUpAddressById(int id)
        {
            try
            {
                return new MITSService<PickUpAddress>().GetSingle(x => x.Id == id);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get pick up address by Id: " + id, ex);
                return null;
            }
        }

        public bool AddPickUpAddress(PickUpAddress pickUpAddress)
        {
            try
            {
                new MITSService<PickUpAddress>().Add(pickUpAddress);
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to add pick up address ", ex);
                return false;
            }
        }

        public bool UpdatePickUpAddress(PickUpAddress pickUpAddress)
        {
            try
            {
                new MITSService<PickUpAddress>().Save(pickUpAddress);
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to update pick up address ", ex);
                return false;
            }
        }

        public bool DeletePickUpAddress(int id)
        {
            try
            {
                var pickUp = GetPickUpAddressById(id);
                if (pickUp != null)
                {
                    new MITSService<PickUpAddress>().Delete(pickUp);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to delete pick up address ", ex);
                return false;
            }
            return false;
        }

        public IList<State> GetSupportedStates(int affiliateId)
        {
            MITSEntities mitsEntities = new MITSEntities();
            var supportedStates = (from state in mitsEntities.States
                                   join acr in mitsEntities.AffiliateContractRates
                                   on state.Id equals acr.StateId
                                   where acr.AffiliateId == affiliateId
                                   group state by new { state.Id } into t
                                   select t.FirstOrDefault()).OrderBy(x => x.Name);

            return supportedStates.ToList();
        }
    }
}
