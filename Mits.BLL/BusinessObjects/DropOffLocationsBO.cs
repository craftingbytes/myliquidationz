﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System;
using Mits.Common;
using System.Text;
using System.Collections;

namespace Mits.BLL.BusinessObjects
{
    public class DropOffLocationsBO
    {
        int locationTypeOther = 9;
        MITSEntities mitsDataContext = null;
        Logger logger = null;
        MITSService<DropOffLocation> mitsDropOffSvc = null;
        public DropOffLocationsBO()
        {
            mitsDataContext = new MITSService<MailBack>().MitsEntities;
            mitsDropOffSvc = new MITSService<DropOffLocation>();
            logger = new Logger(this.GetType());
        }

        //for get La&Ln
        public List<DropOff_Location> GetAllLocationsForTemp()
        {

            List<DropOff_Location> list = mitsDataContext.DropOff_Location.Where(d => d.ZipCode != "0" && d.Longitude==null).ToList();
            return list;
        }

        public void SaveDropOffLocation(decimal lat, decimal lgt, string point)
        {
            DropOff_Location dol = mitsDataContext.DropOff_Location.Where(d => d.ZipCode == point).FirstOrDefault();
            dol.Latitude = lat;
            dol.Longitude = lgt;
            mitsDataContext.SaveChanges();
        }

        public List<DropOffLoc> GetNearestLocationsForAcer(decimal lat, decimal lgt, string city, string state, string prods, string mfgr)
        {
            List<NearAffiliate> _nearAffiliates = new List<NearAffiliate>();
            try
            {
                IList<Affiliate> _affiliate = mitsDataContext.Affiliates
                    .Where(c => c.Name.Contains("ACER America Corporation")).ToList();
                int ownerCompanyId = _affiliate[0].Id;//5778 is id of 'ACER America Corporation'
                IList<DropOffLocation> _dropOffLocations = mitsDataContext.DropOffLocations
                    .Where(c => c.State.Abbreviation == state && c.OwnerCompany == ownerCompanyId).ToList();
                foreach (DropOffLocation drl in _dropOffLocations)
                {
                    if (drl.Latitude == null || drl.Longitude == null)
                        continue;

                    NearAffiliate naff = new NearAffiliate();
                    decimal longitude, latitude = 0;
                    naff.Id = drl.Id;
                    naff.Name = drl.Name;
                    naff.Address = drl.Address1;
                    if (drl.City != null)
                        naff.City = drl.City.Name;
                    if (drl.State != null)
                        naff.State = drl.State.Name;
                    naff.Website = drl.WebSite;
                    naff.Phone = drl.Phone;
                    naff.Latitude = drl.Latitude.GetValueOrDefault();
                    naff.Longitude = drl.Longitude.GetValueOrDefault();
                    if (drl.Latitude != null)
                        latitude = Math.Abs(lat - drl.Latitude.Value);
                    else
                        latitude = Math.Abs(lat - 0);

                    if (drl.Longitude != null)
                        longitude = Math.Abs(lgt - drl.Longitude.Value);
                    else
                        longitude = Math.Abs(lgt - 0);

                    naff.DistanceMiles = Math.Round(Math.Sqrt(Math.Pow(69.1 * Convert.ToDouble(latitude), 2)
                               + Math.Pow(69.1 * Convert.ToDouble(longitude) * Math.Cos(Convert.ToDouble(lat) / 57.3), 2)), 1);

                    if (drl.DropOffLocationTypes != null && drl.DropOffLocationTypes.Count != 0)
                    {
                        naff.DropOffLocationTypes = drl.DropOffLocationTypes.ToList();//.First().LocationType;
                    }
                    else
                    {
                        naff.DropOffLocationTypes = new List<DropOffLocationType>();
                        naff.DropOffLocationTypes.Add(new DropOffLocationType() { LocationType = new LocationType() { Id = locationTypeOther } });
                    }

                    if (naff.DistanceMiles <= 100 && _nearAffiliates.Exists(x => x.Id == naff.Id) == false)
                        _nearAffiliates.Add(naff);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }

            return BuildLocationsList(_nearAffiliates,true);
        }
        public List<DropOffLoc> GetNearestLocationsForSony(decimal lat, decimal lgt, string city, string state, string prods, string mfgr)
        {
            List<NearAffiliate> _nearAffiliates = new List<NearAffiliate>();
            try
            {
                IList<Affiliate> _affiliate= mitsDataContext.Affiliates
                    .Where(c => c.Name.Contains("Sony Electronics")).ToList();
                int ownerCompanyId = _affiliate[0].Id;//5550 is id of 'Sony Electronics'
                IList<DropOffLocation> _dropOffLocations = mitsDataContext.DropOffLocations
                    .Include("DropOffLocationTypes")
                    .Include("DropOffLocationTypes.LocationType")
                    .Where(c => c.State.Abbreviation == state && c.OwnerCompany == ownerCompanyId).ToList();
                foreach (DropOffLocation drl in _dropOffLocations)
                {
                    if (drl.Latitude == null || drl.Longitude == null)
                        continue;
                    
                    NearAffiliate naff = new NearAffiliate();
                    decimal longitude, latitude = 0;
                    naff.Id = drl.Id;
                    naff.Name = drl.Name;
                    naff.Address = drl.Address1;
                    if(drl.City!=null)
                        naff.City = drl.City.Name;
                    if(drl.State!=null)
                        naff.State = drl.State.Name ;
                    naff.Website = drl.WebSite;
                    naff.Phone = drl.Phone;
                    naff.BusinessHours = drl.BusinessHours;
                    naff.Latitude = drl.Latitude.GetValueOrDefault();
                    naff.Longitude = drl.Longitude.GetValueOrDefault();
                    if (drl.Latitude != null)
                        latitude = Math.Abs(lat - drl.Latitude.Value);
                    else
                        latitude = Math.Abs(lat - 0);

                    if (drl.Longitude != null)
                        longitude = Math.Abs(lgt - drl.Longitude.Value);
                    else
                        longitude = Math.Abs(lgt - 0);

                    naff.DistanceMiles = Math.Round(Math.Sqrt(Math.Pow(69.1 * Convert.ToDouble(latitude), 2)
                               + Math.Pow(69.1 * Convert.ToDouble(longitude) * Math.Cos(Convert.ToDouble(lat) / 57.3), 2)), 1);

                    if (drl.DropOffLocationTypes != null && drl.DropOffLocationTypes.Count != 0)
                    {
                        naff.DropOffLocationTypes = drl.DropOffLocationTypes.ToList();//.First().LocationType;
                    }
                    else
                    {
                        naff.DropOffLocationTypes = new List<DropOffLocationType>();
                        naff.DropOffLocationTypes.Add(new DropOffLocationType() { LocationType = new LocationType() { Id = locationTypeOther } });
                    }

                    if (naff.DistanceMiles <= 100 && _nearAffiliates.Exists(x => x.Id == naff.Id) == false)
                        _nearAffiliates.Add(naff);
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message, ex);   
            }

            return BuildLocationsList(_nearAffiliates,true);   
        }

        public List<DropOffLoc> GetNearestLocations(decimal lat, decimal lgt, string city, string state, string prods, string mfgr)
        {
            return BuildLocationsList(GetNearestAffiliates(lat, lgt, city, state, prods, mfgr), true);

        }

        public List<DropOffLoc> GetNearestMichiganLocations(decimal lat, decimal lgt, string city, string state, string prods, string mfgr)
        {
            return BuildLocationsList(GetNearestMichiganAffiliates(lat, lgt, city, state, prods, mfgr), true);

        }

        public List<NearAffiliate> GetNearestMichiganAffiliates(decimal lat, decimal lgt, string city, string state, string prods, string mfgr)
        {
            List<NearAffiliate> _nearAffiliates = new List<NearAffiliate>();
            try
            {
                IList<DropOffLocation> _dropOffLocations = mitsDataContext.DropOffLocations
                    .Where(c => c.State.Abbreviation == "MI").ToList();
                foreach (DropOffLocation drl in _dropOffLocations)
                {
                    if (drl.Latitude == null || drl.Longitude == null)
                        continue;

                    NearAffiliate naff = new NearAffiliate();
                    decimal longitude, latitude = 0;
                    naff.Id = drl.Id;
                    naff.Name = drl.Name;
                    naff.Address = drl.Address1;
                    if (drl.City != null)
                        naff.City = drl.City.Name;
                    if (drl.State != null)
                        naff.State = drl.State.Name;
                    naff.Website = drl.WebSite;
                    naff.Phone = drl.Phone;
                    naff.Latitude = drl.Latitude.GetValueOrDefault();
                    naff.Longitude = drl.Longitude.GetValueOrDefault();
                    if (drl.Latitude != null)
                        latitude = Math.Abs(lat - drl.Latitude.Value);
                    else
                        latitude = Math.Abs(lat - 0);

                    if (drl.Longitude != null)
                        longitude = Math.Abs(lgt - drl.Longitude.Value);
                    else
                        longitude = Math.Abs(lgt - 0);

                    naff.DistanceMiles = Math.Round(Math.Sqrt(Math.Pow(69.1 * Convert.ToDouble(latitude), 2)
                               + Math.Pow(69.1 * Convert.ToDouble(longitude) * Math.Cos(Convert.ToDouble(lat) / 57.3), 2)), 1);

                    if (drl.DropOffLocationTypes != null && drl.DropOffLocationTypes.Count != 0)
                    {
                        naff.DropOffLocationTypes = drl.DropOffLocationTypes.ToList();//First().LocationType;
                    }
                    else
                    {
                        naff.DropOffLocationTypes = new List<DropOffLocationType>();
                        naff.DropOffLocationTypes.Add(new DropOffLocationType() { LocationType = new LocationType() { Id = locationTypeOther } });

                    }

                    if (naff.DistanceMiles <= 300 && _nearAffiliates.Exists(x => x.Id == naff.Id) == false)
                        _nearAffiliates.Add(naff);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return _nearAffiliates;

        }

        public List<NearAffiliate> GetNearestAffiliates(decimal lat, decimal lgt, string city, string state, string prods, string mfgr)
        {
            List<NearAffiliate> _nearAffiliates = new List<NearAffiliate>();
            try
            {
                //IList<AffiliateContractRate> _affiliateContractRates = null;

                //if (!string.IsNullOrEmpty(prods))
                //    _affiliateContractRates = mitsDataContext.AffiliateContractRates
                //      .Include("Affiliate").Include("ProductType").Include("ServiceType")
                //      .Where(c => c.State.Abbreviation == state
                //       && c.ServiceTypeId == 1//collection
                //       && prods.Contains(c.ProductType.Name)
                //       ).ToList();
                //else
                //    _affiliateContractRates = mitsDataContext.AffiliateContractRates
                //      .Include("Affiliate").Include("ProductType").Include("ServiceType")
                //      .Where(c => c.State.Abbreviation == state
                //       && c.ServiceTypeId == 1//collection
                //       //&& c.Affiliate.Notes.Contains("Acer")
                //       ).ToList();

                //foreach (AffiliateContractRate afr in _affiliateContractRates)
                //{
                //    NearAffiliate _aff = new NearAffiliate();
                //    if (afr.Affiliate != null)
                //    {
                //        decimal longitude, latitude = 0;
                //        _aff.Id = afr.Affiliate.Id;
                //        _aff.Name = afr.Affiliate.Name;
                //        _aff.Address = afr.Affiliate.Address1;
                //        if (afr.Affiliate.City != null)
                //            _aff.City = afr.Affiliate.City.Name;
                //        if (afr.Affiliate.State != null)
                //            _aff.State = afr.Affiliate.State.Name;

                //        _aff.Website = afr.Affiliate.WebSite;
                //        _aff.Phone = afr.Affiliate.Phone;

                //        _aff.Latitude = afr.Affiliate.Latitude.GetValueOrDefault();
                //        _aff.Longitude = afr.Affiliate.Longitude.GetValueOrDefault();

                //        if (afr.Affiliate.Latitude != null)
                //            latitude = Math.Abs(lat - afr.Affiliate.Latitude.Value);
                //        else
                //            latitude = Math.Abs(lat - 0);

                //        if (afr.Affiliate.Longitude != null)
                //            longitude = Math.Abs(lgt - afr.Affiliate.Longitude.Value);
                //        else
                //            longitude = Math.Abs(lgt - 0);

                //        _aff.HasAccrediation = afr.Affiliate.AffiliateAccreditations.Count > 0;

                //        _aff.DistanceMiles = Math.Round(Math.Sqrt(Math.Pow(69.1 * Convert.ToDouble(latitude), 2)
                //            + Math.Pow(69.1 * Convert.ToDouble(longitude) * Math.Cos(Convert.ToDouble(lat) / 57.3), 2)), 1);

                //        if (_aff.DistanceMiles <= 100 && _nearAffiliates.Exists(x => x.Id == _aff.Id) == false)
                //            _nearAffiliates.Add(_aff);
                //    }
                //} 

                IList<DropOffLocation> _dropOffLocations = mitsDataContext.DropOffLocations
                    .Where(c => c.State.Abbreviation == state).ToList();
                foreach (DropOffLocation drl in _dropOffLocations)
                {
                    if (drl.Latitude == null || drl.Longitude == null)
                        continue;

                    NearAffiliate naff = new NearAffiliate();
                    decimal longitude, latitude = 0;
                    naff.Id = drl.Id;
                    naff.Name = drl.Name;
                    naff.Address = drl.Address1;
                    if (drl.City != null)
                        naff.City = drl.City.Name;
                    if (drl.State != null)
                        naff.State = drl.State.Name;
                    naff.Website = drl.WebSite;
                    naff.Phone = drl.Phone;
                    naff.Latitude = drl.Latitude.GetValueOrDefault();
                    naff.Longitude = drl.Longitude.GetValueOrDefault();
                    if (drl.Latitude != null)
                        latitude = Math.Abs(lat - drl.Latitude.Value);
                    else
                        latitude = Math.Abs(lat - 0);

                    if (drl.Longitude != null)
                        longitude = Math.Abs(lgt - drl.Longitude.Value);
                    else
                        longitude = Math.Abs(lgt - 0);

                    naff.DistanceMiles = Math.Round(Math.Sqrt(Math.Pow(69.1 * Convert.ToDouble(latitude), 2)
                               + Math.Pow(69.1 * Convert.ToDouble(longitude) * Math.Cos(Convert.ToDouble(lat) / 57.3), 2)), 1);

                    if (drl.DropOffLocationTypes != null && drl.DropOffLocationTypes.Count != 0)
                    {
                        naff.DropOffLocationTypes = drl.DropOffLocationTypes.ToList();//First().LocationType;
                    }
                    else
                    {
                        naff.DropOffLocationTypes = new List<DropOffLocationType>();
                        naff.DropOffLocationTypes.Add(new DropOffLocationType() { LocationType = new LocationType() { Id = locationTypeOther } });

                    }

                    if (naff.DistanceMiles <= 100 && _nearAffiliates.Exists(x => x.Id == naff.Id) == false)
                        _nearAffiliates.Add(naff);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return _nearAffiliates;

        }

        private List<DropOffLoc> BuildLocationsList(List<NearAffiliate> affiliates, bool HasLocationType)
        {
            List<DropOffLoc> _dropOffLocList = new List<DropOffLoc>();

            if (affiliates != null)
            {
                IOrderedEnumerable<NearAffiliate> _nearAffiliatesOrdered;

                //if(HasLocationType)
                    _nearAffiliatesOrdered = affiliates.OrderBy(x => x.DropOffLocationTypes[0].LocationType.Id).ThenBy(x => x.DistanceMiles);
                //else
                    //_nearAffiliatesOrdered = affiliates.OrderBy(x => x.DistanceMiles);

                foreach (NearAffiliate aff in _nearAffiliatesOrdered)
                {
                    DropOffLoc dropOff = new DropOffLoc();

                    string location = string.Empty;

                    location = aff.Name + "<br/>";
                    location += aff.Address + " " + aff.City + "," + aff.State + " " + aff.Zip + "<br/>";
                    location += aff.Website + "<br/>";
                    location += aff.Phone + "<br/>";
                    if (aff.BusinessHours != null)
                    {
                        int len = 50;
                        if (aff.BusinessHours.Length < len)
                        {
                            location += aff.BusinessHours + "<br/>";
                        }
                        else
                        {
                            int startPosition = 0;
                            int residuaryLenght = aff.BusinessHours.Length;
                            while (residuaryLenght > 0)
                            {
                                if (residuaryLenght > len)
                                {
                                    location += aff.BusinessHours.Substring(startPosition, len) + "<br/>";
                                }
                                else
                                {
                                    location += aff.BusinessHours.Substring(startPosition) + "<br/>";
                                }
                                startPosition += len;
                                residuaryLenght -= len;
                            }
                        }
                    }
                    dropOff.Location = location;
                    dropOff.Distance = aff.DistanceMiles.ToString() + " miles";
                    
                    string addressParams = aff.Longitude.ToString() +","+ aff.Latitude.ToString();

                    dropOff.ViewOnMap = "<a style='color:#FF9900' href='#' onclick='javascript:showMapLocations(" + addressParams + ") '>View on Map</a>";
                    dropOff.Longitude = aff.Longitude.ToString();
                    dropOff.Latitude = aff.Latitude.ToString();
                    dropOff.Message = CheckAccreditation(aff.HasAccrediation);
                    if (aff.DropOffLocationTypes != null && aff.DropOffLocationTypes.Count != 0)
                    {
                        dropOff.LocationType = new string[aff.DropOffLocationTypes.Count];
                        for (int i =0;i<aff.DropOffLocationTypes.Count;i++)
                        {
                            dropOff.LocationType[i] = aff.DropOffLocationTypes[i].LocationType.Id.ToString();
                        }
                    }
                    _dropOffLocList.Add(dropOff);
                }
            }

            return _dropOffLocList;
        }

        public DropOffLocation GetDropOffLocation(int id)
        {
            DropOffLocation _drf = null;
            try
            {
                _drf =  mitsDataContext.DropOffLocations.Where(d => d.Id == id).First<DropOffLocation>();
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an DropOffLocation: " + id + "", ex);
            }
            return _drf;
        }
            

        public IList<DropOffLocation> GetDropOffLocations(int pageIndex,int pageSize,string name, out int totalRecords)
        {
            IList<DropOffLocation> locations = new List<DropOffLocation>();
            totalRecords = 0;
            try
            {
                if (name != null && name != "")
                {
                    locations = mitsDataContext.DropOffLocations.Where(d => d.Name.Contains(name)).ToList();
                }
                else
                {
                    locations = mitsDataContext.DropOffLocations.ToList();
                }
                if (locations.Count>0)
                {
                    IEnumerable<DropOffLocation> enumSorted = locations.OrderBy(f => f.Name);
                    try
                    {
                        locations = enumSorted.ToList();
                    }
                    catch 
                    { }

                    totalRecords = locations.Count();
                    return locations.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return locations;
        }

        public List<Message> Update(DropOffLocation dropoffModel, string devices,string locationtypes)
        {
            List<Message> messages = new List<Message>();
            try
            {
                if (messages.Count > 0)
                    return messages;

                State state = mitsDropOffSvc.MitsEntities.States.Where(t => t.Id == dropoffModel.StateId).FirstOrDefault();
                dropoffModel.State = state;

                City city = mitsDropOffSvc.MitsEntities.Cities.Where(t => t.Id == dropoffModel.CityId).FirstOrDefault();
                dropoffModel.City = city;

                mitsDropOffSvc.Save(dropoffModel);
                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));

                UpdateDropOffLocationDevices(devices, dropoffModel);
                UpdateDropOffLocationTypes(locationtypes, dropoffModel);
               

                mitsDropOffSvc.MitsEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                logger.Error("Failed to update DropOffLocation: " + dropoffModel.Name + "", ex);
            }
            return messages;
        }

        private void UpdateDropOffLocationDevices(string newdevices,DropOffLocation dropoffModel)
        {
            //update location-devices
            if (!string.IsNullOrEmpty(newdevices))
            {
                #region
                string[] selectedDevices = newdevices.Split(',');
                IList<DropOffLocationDevice> existingDevices = mitsDropOffSvc.MitsEntities.DropOffLocationDevices.Where(x => x.DropOffLocationId == dropoffModel.Id).ToList();

                //delete unchecked
                foreach (DropOffLocationDevice existingDev in existingDevices)
                {
                    bool isChecked = false;
                    foreach (string selectedDev in selectedDevices)
                    {
                        if (existingDev.DeviceId.ToString() == selectedDev)
                        {
                            isChecked = true;
                            break;
                        }
                    }
                    if (!isChecked)
                    {
                        mitsDropOffSvc.MitsEntities.DropOffLocationDevices.Remove(existingDev);
                    }
                }

                //add new checked
                foreach (string selectedDev in selectedDevices)
                {
                    bool isExisted = false;
                    foreach (DropOffLocationDevice existingDev in existingDevices)
                    {
                        if (existingDev.DeviceId.ToString() == selectedDev)
                        {
                            isExisted = true;
                            break;
                        }
                    }
                    if (!isExisted)
                    {
                        DropOffLocationDevice newLocationDevice = new DropOffLocationDevice();
                        newLocationDevice.DeviceId = Convert.ToInt32(selectedDev);
                        newLocationDevice.DropOffLocationId = dropoffModel.Id;
                        mitsDropOffSvc.MitsEntities.DropOffLocationDevices.Add(newLocationDevice);
                    }
                }
                #endregion
            }
            //end
        }
        private void UpdateDropOffLocationTypes(string newtypes, DropOffLocation dropoffModel)
        {
            //update location-types
            if (!string.IsNullOrEmpty(newtypes))
            {
                #region
                string[] selectedTypes = newtypes.Split(',');
                IList<DropOffLocationType> existingTypes = mitsDropOffSvc.MitsEntities.DropOffLocationTypes.Where(x => x.DropOffLocationId == dropoffModel.Id).ToList();

                //delete unchecked
                foreach (DropOffLocationType existingType in existingTypes)
                {
                    bool isSelected = false;
                    foreach (string selectedTyp in selectedTypes)
                    {
                        if (existingType.TypeId.ToString() == selectedTyp)
                        {
                            isSelected = true;
                            break;
                        }
                    }
                    if (!isSelected)
                    {
                        mitsDropOffSvc.MitsEntities.DropOffLocationTypes.Remove(existingType);
                    }
                }

                //add new checked
                foreach (string selectedTyp in selectedTypes)
                {
                    bool isExisted = false;
                    foreach (DropOffLocationType existingTyp in existingTypes)
                    {
                        if (existingTyp.TypeId.ToString() == selectedTyp)
                        {
                            isExisted = true;
                            break;
                        }
                    }
                    if (!isExisted)
                    {
                        DropOffLocationType newLocationType = new DropOffLocationType();
                        newLocationType.TypeId = Convert.ToInt32(selectedTyp);
                        newLocationType.DropOffLocationId = dropoffModel.Id;
                        mitsDropOffSvc.MitsEntities.DropOffLocationTypes.Add(newLocationType);
                    }
                }
                #endregion
            }
            //end
        }


        public List<Message> Add(DropOffLocation dropOffModel, string devices,string locationtypes)
        {
            List<Message> messages = new List<Message>();
            try
            {

                if (messages.Count > 0)
                    return messages;

                State state = new MITSService<State>().GetSingle(t => t.Id == dropOffModel.StateId);
                dropOffModel.State = state;

                City city = new MITSService<City>().GetSingle(t => t.Id == dropOffModel.CityId);
                dropOffModel.City = city;

                mitsDropOffSvc.Add(dropOffModel);

                if (!string.IsNullOrEmpty(locationtypes))
                {
                    string[] selectedTypes = locationtypes.Split(',');
                    foreach (string selectedTyp in selectedTypes)
                    {
                        DropOffLocationType newLocationType = new DropOffLocationType();
                        newLocationType.TypeId = Convert.ToInt32(selectedTyp);
                        newLocationType.DropOffLocationId = dropOffModel.Id;
                        mitsDropOffSvc.MitsEntities.DropOffLocationTypes.Add(newLocationType);
                    }
                }

                if (!string.IsNullOrEmpty(devices))
                {
                    string[] selectedDevices = devices.Split(',');
                    foreach (string selectedDev in selectedDevices)
                    {
                        DropOffLocationDevice newLocationDevice = new DropOffLocationDevice();
                        newLocationDevice.DeviceId = Convert.ToInt32(selectedDev);
                        newLocationDevice.DropOffLocationId = dropOffModel.Id;
                        mitsDropOffSvc.MitsEntities.DropOffLocationDevices.Add(newLocationDevice);
                    }
                }


                mitsDropOffSvc.MitsEntities.SaveChanges();

                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                logger.Error("Failed to add an DropOffLocation: " + dropOffModel.Name + "", ex);
            }
            return messages;
        }

        public bool DeleteDropOffLocation(int id)
        {
            bool success = false;
            try
            {
                MITSService<DropOffLocationDevice> dbDropOffLocationDevice = new MITSService<DropOffLocationDevice>();
                MITSService<DropOffLocationType> dbDropOffLocationType = new MITSService<DropOffLocationType>();
                DropOffLocation dropoff = mitsDropOffSvc.GetSingle(D => D.Id == id);
                IList<DropOffLocationDevice> list = dbDropOffLocationDevice.GetAll(X => X.DropOffLocationId == id);
                IList<DropOffLocationType> listType = dbDropOffLocationType.GetAll(X => X.DropOffLocationId == id);
                foreach (DropOffLocationDevice one in list)
                {
                    dbDropOffLocationDevice.Delete(one);
                }
                foreach (DropOffLocationType one in listType)
                {
                    dbDropOffLocationType.Delete(one);
                }
                mitsDropOffSvc.Delete(dropoff);
                success = true;
            }
            catch (Exception ex)
            {
                logger.Error("Failed delete DropOffLocation: " + id + "", ex);

            }

            return success;
        }

        private string CheckAccreditation(bool hasAccreditation)
        {
            String message = "";
            if (!hasAccreditation)
            {
                message += " <span class=\"fontRed\" title=\"Please note the accreditation disclaimer.\">*</span>";
                message = "</p><p><span class=\"fontRed\">*</span>This recycler/collector is provided for your information only. " +
                                   "No warranties or representations of the quality or prices at this facility is being made by MITS or their affiliates.  " +
                                   "It is recommended that you contact this recycler/collector for any additional information about recycling your product " +
                                   "prior to delivery.";

            }
            return message;
        }
    }
    public class NearAffiliate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string BusinessHours { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public bool HasAccrediation { get; set; }
        public Double DistanceMiles { get; set; }
        public IList<DropOffLocationType> DropOffLocationTypes { get; set; }
        
    }
    [Serializable]
    public class DropOffLoc
    {
        
        public string Location { get; set; }
        public string Distance { get; set; }
        public string ViewOnMap { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Message { get; set; }
        public string[] LocationType { get; set; }
   
    }
}
