﻿using Mits.Common;
using Mits.DAL.EntityModels;
using System.Collections.Generic;
using System.Text;
namespace Mits.BLL.BusinessObjects
{
    public class ResetPasswordBO
    {
        MITSService<AffiliateContact> mitsAffliateContactSvc = null;

        public ResetPasswordBO()
        {
            mitsAffliateContactSvc = new MITSService<AffiliateContact>();
        }

        public void SendPasswordResetEmail(string email,string answer, int questionId, string callbackUrl)
        {
            AffiliateContact affiliateContact = mitsAffliateContactSvc.GetSingle(t => t.Email.ToLower() == email.ToLower());
            if (affiliateContact == null ||
                affiliateContact.SecurityQuestionId != questionId ||
                affiliateContact.Answer.ToLower() != answer.ToLower())
            {
                throw new MessageException(MessageType.BusinessError, Constants.Messages.InformationExists);
            }

            Dictionary<object, object> _dictioanry = new Dictionary<object, object>();
            StringBuilder message = new StringBuilder();
            //message.Append("<html><body style='font-family:Calibri; font-size:11pt;'>Dear User,<br/><br/>");
            message.Append(Utility.BodyStart());
            message.Append(Utility.AddTextLine("Dear User,"));
            message.Append(Utility.AddBreak(2));
            message.Append(Utility.AddTextLine("You have requested a reset of your MITS account password."));
            message.Append(Utility.AddBreak(2));
            message.Append(Utility.AddTextLine("<b>Instructions:</b>  Please reset your password by <a href =\"" + callbackUrl + "\">clicking here</a>."));
            message.Append(Utility.AddBreak(2));
            message.Append(Utility.AddTextLine("Thank you,"));
            message.Append(Utility.AddBreak(1));
            message.Append(Utility.AddTextLine("E-World Online"));
            message.Append(Utility.AddBreak(2));
            message.Append(Utility.AddTextLine("Manufacturer's Interstate Takeback System (MITS)"));
            message.Append(Utility.AddBreak(1));
            message.Append(Utility.BodyEnd());

            if (!EmailHelper.SendMail("Request To Reset Account Password", message.ToString(), new string[] { affiliateContact.Email }, null, null))
            {
                throw new MessageException(MessageType.BusinessError, Constants.Messages.TryLater);
            }
        }
    }
}
