﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.Common;
namespace Mits.BLL.BusinessObjects
{
    public class AccountReceivableBO
    {
        MITSService<Payment> mitsReceivableSvc = null;
        Logger mitsLogger = null;
        public AccountReceivableBO()
        {
            mitsReceivableSvc = new MITSService<Payment>();
            mitsLogger = new Logger(this.GetType());
        }

        public IList<Affiliate> GetAllAffiliate()
        {
            IList<Affiliate> _affList = null;
            try
            {
               _affList= new MITSService<Affiliate>().GetAll().OrderBy(f => f.Name).ToList();
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get entity List", ex);
            }
            return _affList;
        }
        public IList<PaymentType> GetPaymentTypes()
        {
            IList<PaymentType> _paymentList = null;
            try
            {
                _paymentList= new MITSService<PaymentType>().GetAll();
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get payment types list", ex);
            }
            return _paymentList;
        }
        
        public Payment GetPaymentByID(Int32 id) 
        {
            Payment _payment = null;
            try
            {
                _payment = mitsReceivableSvc.GetSingle(p => p.Id == id);
                if (_payment != null)
                {
                    foreach (Invoice _invoice in GetAllUnPaidInvoices(_payment.AffiliateId.Value))
                    {
                        if (!_payment.PaymentDetails.Select(pd => pd.InvoiceId).Contains(_invoice.Id))
                        {
                            PaymentDetail _newPaymentDetail = new PaymentDetail();
                            _newPaymentDetail.InvoiceId = _invoice.Id;
                            _newPaymentDetail.Invoice = _invoice;
                            _payment.PaymentDetails.Add(_newPaymentDetail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get payment by Id "+id+"", ex);
            }
            return _payment;
        }

        public Payment CreateNewPayment(Int32 affiliateID)
        {
            Payment _payment = new Payment();
            try
            {
                _payment.AffiliateId = affiliateID;
                foreach (Invoice _invoice in GetAllUnPaidInvoices(_payment.AffiliateId.Value))
                {
                    PaymentDetail _paymentDetail = new PaymentDetail();
                    _paymentDetail.InvoiceId = _invoice.Id;
                    _paymentDetail.Invoice = _invoice;
                    _payment.PaymentDetails.Add(_paymentDetail);
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to create new payment of "+affiliateID+"", ex);
            }
            return _payment;
        }

        public bool AddPayment(Payment payment)
        {
            bool success = false;
            try
            {
                foreach (PaymentDetail _paymentDetail in payment.PaymentDetails.ToList())
                {
                    if (_paymentDetail.Id == 0 && (!_paymentDetail.PaidAmount.HasValue || _paymentDetail.PaidAmount.Value == 0))
                        mitsReceivableSvc.MitsEntities.PaymentDetails.Remove(_paymentDetail);
                }

                mitsReceivableSvc.Add(payment);
                success = true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to add payment "+payment.Id+"", ex);
            }
            return success;
        }
        public bool UpdatePayment(Payment payment)
        {
            bool success = false;
            try
            {
                foreach (PaymentDetail _paymentDetail in payment.PaymentDetails.ToList())
                {
                    if (_paymentDetail.Id == 0 && (!_paymentDetail.PaidAmount.HasValue || _paymentDetail.PaidAmount.Value == 0))
                        mitsReceivableSvc.MitsEntities.PaymentDetails.Remove(_paymentDetail);
                }
                mitsReceivableSvc.Save(payment);
                success = true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to update payment " + payment.Id + "", ex);
            }
            return success;
        }

        public List<Invoice> GetAllUnPaidInvoices(Int32 affiliateID)
        {
            List<Invoice> invoices = null;
            try
            {
                invoices = mitsReceivableSvc.MitsEntities.Invoices.Where(i => i.AffiliateId == affiliateID).Distinct().ToList();

                for (int i = 0; i < invoices.Count; )
                {
                    if (IsInvoiceNotFullyPaid(invoices[i].Id))
                        i++;
                    else
                        invoices.RemoveAt(i);
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get all unpaid Invoices of "+affiliateID+"", ex);
            }
            return invoices;
        }

        public bool IsInvoiceNotFullyPaid(Int32 invoiceID)
        {
            bool success = false;
            try
            {
               success= GetInvoiceAmount(invoiceID) > GetInvoicePaidAmount(invoiceID);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to check Invoice "+invoiceID+" not fully paid", ex);
            }
            return success;
        }
        public decimal GetInvoiceAmount(Int32 invoiceID) 
        {
            decimal totalAmount = 0;
            try
            {

                var Recs = from ir in mitsReceivableSvc.MitsEntities.InvoiceReconciliations
                           where ir.OEMInvoiceId == invoiceID 
                           select new { total = Math.Round((decimal)ir.Quantity) * ir.Rate };

                foreach (var item in Recs)
                {
                    totalAmount += item.total.Value;
                }

                IList<InvoiceAdjustment> adjustments = new MITSService<InvoiceAdjustment>().GetAll(x => x.InvoiceId == invoiceID);
                foreach (InvoiceAdjustment adjustment in adjustments)
                {
                    totalAmount += adjustment.Amount;
                }
              
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get Invoice " + invoiceID + " amount", ex);
            }
            return totalAmount;
        }
        public decimal GetInvoicePaidAmount(Int32 invoiceID)
        {
            decimal? _invoicePaidAmount = null;
            try
            {
                _invoicePaidAmount = mitsReceivableSvc.MitsEntities.PaymentDetails.Where(pd => pd.InvoiceId == invoiceID).Sum(pd => pd.PaidAmount);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get Invoice " + invoiceID + " paid amount", ex);
            }
            return _invoicePaidAmount.HasValue ? _invoicePaidAmount.Value : 0;
        }

        public IList<Affiliate> GetReceivables()
        {
            //Sorting List
            IList<Affiliate> sortedList=null;
            try
            {
                string _oem = Common.Constants.AffiliateType.OEM.ToString();
                IList<Affiliate> affiliates = new MITSService<Affiliate>().GetAll(x => x.AffiliateType.Type == _oem);
                IEnumerable<Affiliate> sortedEnum = affiliates.OrderBy(f => f.Name);
                try
                {
                    sortedList = sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    sortedList = affiliates;
                }

                Affiliate _selectAffiliate = new Affiliate();
                _selectAffiliate.Id = 0;
                _selectAffiliate.Name = "Select";
                sortedList.Insert(0, _selectAffiliate);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get OEMs List", ex);
            }
            return sortedList;
        }

        public List<Payment> GetReceivablesPaged(int pageIndex, int pageSize, string receivedVal, string searchDateTo, string searchDateFrom, out int totalRecords)
        {

            int _id = default(int);
            DateTime dateTo = default(DateTime);
            DateTime dateFrom = default(DateTime);
            string _oem = Common.Constants.AffiliateType.OEM.ToString();
            IList<Payment> receivableList = default(IList<Payment>);
            List<Payment> _receivable = default(List<Payment>);
            try
            {
                if ((string.IsNullOrEmpty(receivedVal) || receivedVal == "0")
                    && string.IsNullOrEmpty(searchDateTo) && string.IsNullOrEmpty(searchDateFrom))
                {
                    receivableList = mitsReceivableSvc.GetAll(x => x.Affiliate.AffiliateType.Type == _oem);
                    _receivable = receivableList.ToList();
                }
                else
                {

                    bool _isIdVal = int.TryParse(receivedVal, out _id);
                    bool isdatefrom = DateTime.TryParse(searchDateFrom, out dateFrom);
                    bool isdateto = DateTime.TryParse(searchDateTo, out dateTo);


                    receivableList = mitsReceivableSvc.GetAll(x => x.Affiliate.AffiliateType.Type == _oem);
                    _receivable = receivableList.ToList();
                    if (_isIdVal == true && receivedVal != "0")
                    {
                        IEnumerable<Payment> _receiveList = _receivable.Where(x => x.AffiliateId == _id);
                        List<Payment> _entityLst = _receiveList.ToList();
                        if (_receiveList.Count() > 0)
                        {
                            _receivable.Clear();
                            _receivable.AddRange(_entityLst);


                        }
                        else
                        {
                            _receivable.Clear();
                        }
                    }
                    if (isdatefrom == true)
                    {
                        IEnumerable<Payment> dateFromList = _receivable.Where(x => x.Date >= dateFrom);
                        List<Payment> dateFromLst = dateFromList.ToList();
                        if (dateFromList.Count() > 0)
                        {
                            _receivable.Clear();
                            _receivable.AddRange(dateFromLst);

                        }
                        else
                        {
                            _receivable.Clear();
                        }
                    }
                    if (isdateto == true)
                    {
                        IEnumerable<Payment> dateToList = _receivable.Where(x => x.Date <= dateTo);
                        List<Payment> dateToLst = dateToList.ToList();
                        if (dateToList.Count() > 0)
                        {
                            _receivable.Clear();
                            _receivable.AddRange(dateToLst);

                        }
                        else
                        {
                            _receivable.Clear();
                        }
                    }

                }
                
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get "+receivedVal+" Receivables", ex);
            }
            if (_receivable != null)
            {
                totalRecords = _receivable.Count();
                return _receivable.OrderBy(e => e.Affiliate.Name).Skip(pageIndex * pageSize).Take(pageSize).ToList();//change
            }
            totalRecords = 0;
            return _receivable;

        }

        public string DeletePayment(int paymentId)
        {
            string msg = string.Empty;
            try
            {
                IList<PaymentDetail> PaymentDetails =
                    new MITSService<PaymentDetail>().GetAll(x => x.PaymentID == paymentId).ToList();
                foreach (PaymentDetail Rec in PaymentDetails)
                {
                    new MITSService<PaymentDetail>().Delete(Rec);
                }
                mitsReceivableSvc.Delete(mitsReceivableSvc.GetSingle(x => x.Id == paymentId));
                msg = "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

            return msg;
        }
    }
  
}