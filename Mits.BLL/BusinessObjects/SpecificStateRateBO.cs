﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class SpecificStateRateBO
    {

        MITSService<ProcessingData> mitsProcessingDataSvc = null;
        MITSEntities mitsDataContext = null;

        Logger logger = null;

        public SpecificStateRateBO()
        {
            mitsProcessingDataSvc = new MITSService<ProcessingData>();
            mitsDataContext = mitsProcessingDataSvc.MitsEntities;

            logger = new Logger(this.GetType());
        }


        public IList<State> GetCurrentSpecificState()
        {
            IList<StateSpecificRate> _stateRates = mitsDataContext.StateSpecificRates.ToList<StateSpecificRate>();
            IList<State> _currentStates = new List<State>();

            foreach (StateSpecificRate specificRate in _stateRates)
            {
                if (!_currentStates.Contains(specificRate.State))
                    _currentStates.Add(specificRate.State);
            }

            IList<State> unSortedList = _currentStates;
            IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _currentStates = sortedEnum.ToList();
            }
            catch
            {
                _currentStates = unSortedList;
            }

            State _select = new State();
            _select.Id = 0;
            _select.Name = "Select";
            _currentStates.Insert(0, _select);
            return _currentStates;
          
        }

        public IList<StateSpecificRate> GetSpecificRates(int stateid)
        {

            IList<StateSpecificRate> specificrates = new List<StateSpecificRate>();
            
            try
            {
                if (stateid == null || stateid == 0)
                {
                    specificrates = mitsDataContext.StateSpecificRates.Distinct().ToList();
                    
                    
                }
                else
                {
                    specificrates = mitsDataContext.StateSpecificRates.Where(x => x.State.Id == stateid).Distinct().ToList();
                    
                    
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return specificrates;

        }

        public bool EditSpecificRate(StateSpecificRate entityToUpdate)
        {
            bool success = false;
            try
            {
                if (entityToUpdate.Id == 0)
                {
                    StateSpecificRate entity = new StateSpecificRate();
                    entity.StateId = entityToUpdate.StateId;
                    entity.Urban = entityToUpdate.Urban;
                    entity.Rural = entityToUpdate.Rural;
                    entity.Metro = entityToUpdate.Metro;
                    entity.NonMetro = entityToUpdate.NonMetro;

                    mitsDataContext.StateSpecificRates.Add(entity);
                }
                else
                {
                    StateSpecificRate entity = mitsDataContext.StateSpecificRates.Where(e => e.Id == entityToUpdate.Id).FirstOrDefault();
                    entity.Rural = entityToUpdate.Rural;
                    entity.Metro = entityToUpdate.Metro;
                    entity.NonMetro = entityToUpdate.NonMetro;
                    entity.StateId = entityToUpdate.StateId;
                    entity.Urban = entityToUpdate.Urban;
                }
                
                mitsDataContext.SaveChanges();
                success = true;
            }
            catch (Exception ex)
            {
                logger.Error("Failed Editing StateSpecificRate: " + entityToUpdate.Id.ToString(), ex);
            }
            return success;
        }

        public bool DeleteSpecificRate(int id)
        {
            bool success = false;
            try
            {

                StateSpecificRate entity = mitsDataContext.StateSpecificRates.Where(e => e.Id == id).FirstOrDefault();
                mitsDataContext.StateSpecificRates.Remove(entity);
                mitsDataContext.SaveChanges();

                success = true;
            }
            catch (Exception ex)
            {
                logger.Error("Failed delete document: " + id + "", ex);

            }

            return success;

        }

        public bool AddSpecificRate(StateSpecificRate entityToAdd)
        {
            bool success = false;
            try
            {

                StateSpecificRate entity = new StateSpecificRate();
                entity.StateId = entityToAdd.StateId;
                entity.Urban = entityToAdd.Urban;
                entity.Rural = entityToAdd.Rural;
                entity.Metro = entityToAdd.Metro;
                entity.NonMetro = entityToAdd.NonMetro;

                mitsDataContext.StateSpecificRates.Add(entity);
                mitsDataContext.SaveChanges();

                success = true;
            }
            catch (Exception ex)
            {
                logger.Error("Failed Adding StateSpecificRate: ", ex);

            }
            return success;
        }

        public IList<State> GetStates()
        {
            IList<State> states = null;
            try
            {
                IList<State> unSortedList = new MITSService<State>().GetAll();
                IEnumerable<State> enumSorted = unSortedList.OrderBy(f => f.Name);
                try
                {
                    states = enumSorted.ToList();
                }
                catch (Exception ex)
                {
                    states = unSortedList;
                }

                State tmpState = new State();
                tmpState.Id = -1;
                tmpState.Name = "Select";
                states.Insert(0, tmpState);
            }
            catch (Exception ex)
            {
                logger.Error("Failed Getting States", ex);

            }
            return states;

        }
      


    }
}
