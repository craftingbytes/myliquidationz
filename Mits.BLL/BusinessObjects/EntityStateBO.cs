﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using Mits.Common;
namespace Mits.BLL.BusinessObjects
{
    public class EntityStateBO
    {
        MITSRepository<AffiliateState> mitsStateRepo = new MITSRepository<AffiliateState>();
        public EntityStateBO()
        {
            
        }
        //Getting a list of all Entities

        public IList<Affiliate> GetAllEnity()
        {
            //Sorting List
            IList<Affiliate> _entityList;
            IList<Affiliate> unSortedList = new MITSService<Affiliate>().GetAll(x => x.AffiliateTypeId == 1 || x.AffiliateTypeId == 2);
            IEnumerable<Affiliate> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _entityList = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _entityList = unSortedList;
            }
            Affiliate select = new Affiliate();
            select.Id = 0;
            select.Name = "Select";
            _entityList.Insert(0, select);
            return _entityList;

        }

        //Getting a list of all States

        public IList<State> GetAllState()
        {
            //Sorting List
            IList<State> state;
            IList<State> unSortedList = new MITSService<State>().GetAll();
            IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                state = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                state = unSortedList;
            }
            return state;

        }
        public List<Message> Add(string entity, string state)
        {
            int _id = int.Parse(entity);
            bool _newEntry = default(bool);
            bool _sameState = default(bool);
            string[] _selectedStates;
            List<Message> message=new List<Message>();
            _selectedStates = state.Split(',');

            AffiliateState _entityIdCheck = new MITSService<AffiliateState>().GetSingle(t => t.AffiliateId == _id);

            //Check for new entry

            if (_entityIdCheck == null)
            {
                //new entry

                foreach (var States in _selectedStates)
                {
                    AffiliateState _entityStateObj = new AffiliateState();
                    _entityStateObj.AffiliateId = _id;
                    _entityStateObj.StateId = int.Parse(States);
                    _entityStateObj.Active = true;
                    new MITSService<AffiliateState>().Add(_entityStateObj);
                }

                message.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
                return message;
            }

            //Check for old entry
            else
            {
                //old entry need to update here, Get all existing states against this old entry

                MITSService<AffiliateState> _saveEntityState = new MITSService<AffiliateState>();
                IList<AffiliateState> _existingStates = new MITSService<AffiliateState>().GetAll(t => t.AffiliateId == _entityIdCheck.AffiliateId & t.Active == true);


                //Checks if the same policy is being submitted again then return, else continue 

                if (_existingStates.Count == _selectedStates.Length)
                {

                    foreach (var _selectState in _selectedStates)
                    {
                        _sameState = false;
                        foreach (AffiliateState _eState in _existingStates)
                        {
                            if (int.Parse(_selectState) == _eState.StateId)
                            {

                                _sameState = true;
                            }
                        }
                        if (_sameState == false)
                        {
                            break;
                        }
                    }
                }
                if (_sameState == true)
                {
                    message.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.RecordExist));
                    return message;
                }
                
                //check for new State entries

                IList<AffiliateState> _existingState = new MITSService<AffiliateState>().GetAll(t => t.AffiliateId == _entityIdCheck.AffiliateId);

                foreach (AffiliateState _state in _existingState)
                {
                    _state.Active = false;
                    _saveEntityState.Save(_state);
                }

                foreach (var _selectState in _selectedStates)
                {
                    _newEntry = true;
                    foreach (AffiliateState _sPol in _existingState)
                    {
                        if (int.Parse(_selectState) == _sPol.StateId)
                        {

                            _sPol.Active = true;
                            _saveEntityState.Save(_sPol);
                            _newEntry = false;
                        }
                    }
                    if (_newEntry == true)
                    {
                        AffiliateState _stateObj = new AffiliateState();
                        _stateObj.AffiliateId = _id;
                        _stateObj.StateId = int.Parse(_selectState);
                        _stateObj.Active = true;
                        new MITSService<AffiliateState>().Add(_stateObj);
                    }
                }
                message.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            return message;

        }
    }
}
