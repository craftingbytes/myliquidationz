﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System.Web.Mvc;
using Mits.Common;


namespace Mits.BLL.BusinessObjects
{
    public class LoginBO
    {
        MITSService<AffiliateContact> mitsAffliateContactSvc = null;

        public LoginBO()
        {
            mitsAffliateContactSvc = new MITSService<AffiliateContact>();
        }


        public AffiliateContactViewModel LoginAffiliateContact(string userName, string password)
        {
            if (ConfigHelper.ComputePwdHash == "true")
            {
                password = Utility.CreateHash(password, false);
            }

            AffiliateContactViewModel model=default(AffiliateContactViewModel);
            AffiliateContact affiliateContact = mitsAffliateContactSvc.GetSingle(d => (d.UserName == userName || d.Email == userName));
            if (affiliateContact == null)
                return null;
            else
            {
                if (affiliateContact.LoginTryCount == 0 || affiliateContact.LoginTryCount==null)
                {
                    if (affiliateContact.Password == password)
                    {
                        if (affiliateContact.Active == true)
                            model = new AffiliateContactViewModel(affiliateContact);
                        else
                        {
                            model = new AffiliateContactViewModel(affiliateContact);
                            model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.UserInactive));
                        }
                    }
                    else
                    {
                        if (affiliateContact.LoginTryCount == null)
                        {
                            affiliateContact.LoginTryCount = 0;
                            affiliateContact.LoginTryCount = affiliateContact.LoginTryCount + 1;
                            affiliateContact.LastLoginTime = DateTime.Now;
                            mitsAffliateContactSvc.Save(affiliateContact);
                            model = new AffiliateContactViewModel(affiliateContact);
                            model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.LoginTry(affiliateContact.LoginTryCount)));
                        }
                        else
                        {
                            affiliateContact.LoginTryCount = affiliateContact.LoginTryCount + 1;
                            affiliateContact.LastLoginTime = DateTime.Now;
                            mitsAffliateContactSvc.Save(affiliateContact);
                            model = new AffiliateContactViewModel(affiliateContact);
                            model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.LoginTry(affiliateContact.LoginTryCount)));
                        }
                    }
                    
                }
                else if (affiliateContact.LoginTryCount < ConfigHelper.LoginTryCount)
                {
                    if (affiliateContact.Password == password)
                    {
                        if (affiliateContact.Active == true)
                        {
                            affiliateContact.LoginTryCount = 0;
                            mitsAffliateContactSvc.Save(affiliateContact);
                            model = new AffiliateContactViewModel(affiliateContact);
                        }
                        else
                        {
                            model = new AffiliateContactViewModel(affiliateContact);
                            model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.UserInactive));
                        }
                    }
                    else
                    {
                        affiliateContact.LoginTryCount++;
                        mitsAffliateContactSvc.Save(affiliateContact);
                        model = new AffiliateContactViewModel(affiliateContact);
                        model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.LoginTry(affiliateContact.LoginTryCount)));
                    }
                }
                else if (affiliateContact.LoginTryCount >= ConfigHelper.LoginTryCount)
                {
                    DateTime _lastLoginTime = Convert.ToDateTime(affiliateContact.LastLoginTime);
                    TimeSpan _timeElapsed = DateTime.Now.TimeOfDay - _lastLoginTime.TimeOfDay;
                  
                    if (_timeElapsed.Minutes < ConfigHelper.LoginTryMinute && _timeElapsed.Hours==0 
                        && DateTime.Now.Day==_lastLoginTime.Day && DateTime.Now.Year==_lastLoginTime.Year)
                    {
                       
                        Dictionary<object, object> _dictioanry = new Dictionary<object, object>(); 
                        StringBuilder message = new StringBuilder();
                        //message.Append("<html><body style='font-family:Calibri; font-size:11pt;'>Dear User,<br/><br/>");
                        message.Append(Utility.BodyStart());
                        message.Append(Utility.AddBreak(2));
                        message.Append(Utility.AddTextLine("Dear Admin,"));
                        message.Append(Utility.AddBreak(2));
                        message.Append(Utility.AddTextLine("The account of the below user has been locked due to exceeding the maximum login try limit."));
                        message.Append(Utility.AddBreak(2));
                        _dictioanry.Add("Name", affiliateContact.FirstName + " " + affiliateContact.LastName);
                        _dictioanry.Add("UserName", affiliateContact.UserName);
                        message.Append(Utility.ConvertToHtmlTable(_dictioanry));
                        message.Append(Utility.AddBreak(2));
                        message.Append(Utility.AddTextLine("Manufacturer's Interstate Takeback System (MITS)"));
                        message.Append(Utility.AddBreak(2));
                        message.Append(Utility.BodyEnd());



                        if ((EmailHelper.SendMail("Account Locked", message.ToString(), new string[] { ConfigHelper.AdminEmail }, new string[] {affiliateContact.Email}, null) == true))
                        {
                            model = new AffiliateContactViewModel(affiliateContact);
                            model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.AccountLocked));
                        }
                        else
                        {
                            model = new AffiliateContactViewModel(affiliateContact);
                            model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.TryLater));
                        }
                    }
                    else
                    {
                       affiliateContact.LoginTryCount = 0;
                       mitsAffliateContactSvc.Save(affiliateContact);
                       if (affiliateContact.Password == password)
                       {
                           if (affiliateContact.Active == true)
                               model = new AffiliateContactViewModel(affiliateContact);
                           else
                           {
                               model = new AffiliateContactViewModel(affiliateContact);
                               model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.UserInactive));
                           }
                       }
                       else
                       {
                           affiliateContact.LoginTryCount++;
                           affiliateContact.LastLoginTime = DateTime.Now;
                           mitsAffliateContactSvc.Save(affiliateContact);
                           model = new AffiliateContactViewModel(affiliateContact);
                           model.Messages.Add(new Message(MessageType.Failure.ToString(), Constants.Messages.LoginTry(affiliateContact.LoginTryCount)));
                       }

                    }

                }
            }
            return model;
        }

        public Int32 AddAuditEvent(AuditEvent ae)
        {
            new MITSService<AuditEvent>().Add(ae);
            return ae.Id;
        }
    }
}
