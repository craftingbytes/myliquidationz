﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;

namespace Mits.BLL.BusinessObjects
{
   public class OEMStatePolicyBO
    {
         MITSRepository<StatePolicy> mitsStateRepo = new MITSRepository<StatePolicy>();

         public OEMStatePolicyBO()
        {
            
        }

        public IQueryable GetAllState()
        {
            return mitsStateRepo.GetAll() as IQueryable;
        }


        #region Add OEM Policies

        //Add Policies against a particular OEM and a State and send response back to view

        public Dictionary<string,int> Add(string oem, string state, string policies)
        {

            bool _newPolicyEntry = default(bool);
           
            string[] _selectedPolicies;
            int _oemId = int.Parse(oem);
            int _stateId = int.Parse(state);
            bool _samePolicy = default(bool);

            //Return multiple values via Dictionary 

            Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();
            _returnDictionaryValue["duplicate"] = 0;
            _returnDictionaryValue["selectedId"] = _oemId;

            _selectedPolicies = policies.Split(',');
            ManufacturerPolicy _check = new MITSService<ManufacturerPolicy>().GetSingle(t => t.StateId == _stateId & t.AffiliateId == _oemId );
          
           
            //Check for new entry

            if (_check == null)
            {
                //New Entry

                foreach (var Policies in _selectedPolicies)
                {
                    ManufacturerPolicy _manufacturerPolicyObj = new ManufacturerPolicy();
                    _manufacturerPolicyObj.AffiliateId = _oemId;
                    _manufacturerPolicyObj.StateId = _stateId;
                    _manufacturerPolicyObj.PolicyId = int.Parse(Policies);
                    _manufacturerPolicyObj.Active = true;
                    new MITSService<ManufacturerPolicy>().Add(_manufacturerPolicyObj);
                    _returnDictionaryValue["selectedId"] = _oemId;
                }

                return _returnDictionaryValue;
            }
            else
            {
                //old entry need to update here, Get all existing policies against this old entry
               
                MITSService<ManufacturerPolicy> _saveManufacturerPolicy = new MITSService<ManufacturerPolicy>();
                IList<ManufacturerPolicy> _existingPolicies = new MITSService<ManufacturerPolicy>().GetAll(t => t.StateId == _check.StateId & t.AffiliateId==_check.AffiliateId & t.Active==true );


                //Checks if the same policy is being submitted again then return else continue 

                if (_existingPolicies.Count == _selectedPolicies.Length)
                {

                    foreach (var _selectPol in _selectedPolicies)
                    {
                        _samePolicy = false;
                        foreach (ManufacturerPolicy _eManuPolicy in _existingPolicies)
                        {
                            if (int.Parse(_selectPol) == _eManuPolicy.PolicyId)
                            {

                                _samePolicy = true;
                            }
                        }
                        if (_samePolicy == false)
                        {
                            break;
                        }
                    }
                }
                if (_samePolicy == true)
                {
                    _returnDictionaryValue["duplicate"] = 1;
                    return _returnDictionaryValue;
                }
                
                
                
                //check for new policy entries

                IList<ManufacturerPolicy> _existingManufacturerPolicies = new MITSService<ManufacturerPolicy>().GetAll(t => t.StateId == _check.StateId & t.AffiliateId == _check.AffiliateId );
               
                foreach (ManufacturerPolicy policy in _existingManufacturerPolicies)
                {
                    policy.Active = false;
                    _saveManufacturerPolicy.Save(policy);
                }

                foreach (var _selectPol in _selectedPolicies)
                {
                    _newPolicyEntry = true;
                    foreach (ManufacturerPolicy _eManuPolicy in _existingManufacturerPolicies)
                    {
                        if (int.Parse(_selectPol) == _eManuPolicy.PolicyId)
                        {

                            _eManuPolicy.Active = true;
                            _saveManufacturerPolicy.Save(_eManuPolicy);
                            _newPolicyEntry = false;

                        }
                   }
                    if (_newPolicyEntry == true)
                    {
                        ManufacturerPolicy _manufacturerPolicyObj = new ManufacturerPolicy();
                        _manufacturerPolicyObj.AffiliateId = _oemId;
                        _manufacturerPolicyObj.StateId = _stateId;
                        _manufacturerPolicyObj.PolicyId = int.Parse(_selectPol);
                        _manufacturerPolicyObj.Active = true;
                        new MITSService<ManufacturerPolicy>().Add(_manufacturerPolicyObj);
                       

                   }
                }

              _returnDictionaryValue["selectedId"] = _oemId;
              return _returnDictionaryValue;
            }


        }

        #endregion




    }
}
