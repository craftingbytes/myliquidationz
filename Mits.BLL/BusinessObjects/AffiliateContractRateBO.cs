﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class AffiliateContractRateBO
    {
        MITSService<AffiliateContractRate> mitsAffiliateContractRateSvc = null;
        Logger mitsLogger = null;

        public AffiliateContractRateBO()
        {
            mitsAffiliateContractRateSvc = new MITSService<AffiliateContractRate>();
            mitsLogger = new Logger(this.GetType());
        }

        public List<AffiliateContractRate> GetContractRates(Int32 pageIndex, Int32 pageSize, out Int32 totalRecords, Int32 affiliateId)
        {
            try
            {
                MITSEntities mitsEntities = new MITSEntities();
                var result1 = from r in mitsEntities.AffiliateContractRates
                              where r.AffiliateId == affiliateId
                              group r by new { r.StateId, r.ServiceTypeId, r.RateStartDate, r.RateEndDate, r.PlanYear } into g
                              select new { StateId = g.Key.StateId, ServiceTypeId = g.Key.ServiceTypeId, RateStartDate = g.Key.RateStartDate, RateEndDate = g.Key.RateEndDate,g.Key.PlanYear };

                var result2 = from r in result1
                              join s in mitsEntities.States
                              on r.StateId equals s.Id
                              join c in mitsEntities.ServiceTypes
                              on r.ServiceTypeId equals c.Id
                              select new { State = s, ServiceType = c, RateStartDate = r.RateStartDate, RateEndDate = r.RateEndDate, r.PlanYear };

                totalRecords = result2.ToList().Count;
                var rates = result2.OrderBy(x => x.State.Name).ThenBy(x => x.RateStartDate).ThenBy(x => x.ServiceType.Name).Skip(pageIndex * pageSize).Take(pageSize).ToList();

                List<AffiliateContractRate> contractRates = new List<AffiliateContractRate>();

                int index = 1;
                foreach (var r in rates)
                {
                    var contractRate = new AffiliateContractRate();
                    contractRate.Id = index++;
                    contractRate.State = r.State;
                    contractRate.ServiceType = r.ServiceType;
                    contractRate.RateStartDate = r.RateStartDate;
                    contractRate.RateEndDate = r.RateEndDate;
                    contractRate.PlanYear = r.PlanYear;
                    contractRates.Add(contractRate);
                }
                return contractRates;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get entities contract rate list", ex);
                totalRecords = 0;
                return null;
            }
        }

        public List<AffiliateContractRate> GetContractRateDetails(Int32 pageIndex, Int32 pageSize, out Int32 totalRecords, Int32 affiliateId, string oper, int stateId, string startDate, string endDate, int serviceTypeId)
        {
            try
            {



                if (oper == "new")
                {
                    MITSService<StateProductType> service = new MITSService<StateProductType>();
                    var stateProductTypes = service.GetAll(x => x.StateId == stateId).OrderBy(x => x.ProductType.Name).ToList();
                    var newContractRates = new List<AffiliateContractRate>();
                    int index = 0;
                    foreach (var type in stateProductTypes)
                    {
                        AffiliateContractRate rate = new AffiliateContractRate();
                        rate.Id = index--;
                        rate.ProductType = type.ProductType;
                        newContractRates.Add(rate);
                    }
                    totalRecords = newContractRates.Count;
                    return newContractRates;
                }
                else if (oper == "edit")
                {
                    DateTime start = Convert.ToDateTime(startDate);
                    DateTime end = Convert.ToDateTime(endDate);
                    MITSService<AffiliateContractRate> service = new MITSService<AffiliateContractRate>();
                    
                    var rates = service.GetAll(x => x.AffiliateId.Value == affiliateId 
                        && x.StateId.Value == stateId 
                        && x.ServiceTypeId.Value == serviceTypeId
                        && x.RateStartDate.HasValue
                        && x.RateStartDate.Value == start
                        && x.RateEndDate.HasValue
                        && x.RateEndDate.Value == end).OrderBy(x => x.ProductType.Name).ToList();

                    var combinedRates = new List<AffiliateContractRate>();


                    MITSService<StateProductType> rateservice = new MITSService<StateProductType>();
                    var stateProductTypes = rateservice.GetAll(x => x.StateId == stateId).OrderBy(x => x.ProductType.Name).ToList();

                    var newtypes = from st in stateProductTypes
                                  where !rates.Any(x => x.ProductType == st.ProductType)
                                  select new { ProductType = st.ProductType};

                    foreach (AffiliateContractRate rate in rates)
                    {
                        combinedRates.Add(rate);
                    }
                    int index = 0;
                    foreach (var newtype in newtypes)
                    {
                            AffiliateContractRate newrate = new AffiliateContractRate();
                            newrate.Id = index--;
                            newrate.ProductType = newtype.ProductType;
                            combinedRates.Add(newrate);
                    }
                   

                    totalRecords = combinedRates.Count;
                    return combinedRates;
                }
                totalRecords = 0;
                return null;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get entities contract rate list", ex);
                totalRecords = 0;
                return null;
            }
        }

        public AffiliateContractRate GetAffiliateContractRateByID(Int32 id)
        {
            try
            {
                return mitsAffiliateContractRateSvc.GetSingle(a => a.Id == id);
            }
            catch (Exception ex) 
            {
                mitsLogger.Error("Failed to get all entity contract rate by Id: "+id+"", ex);
                return null;
            }
        }

        //using planyear logic
        //public AffiliateContractRate GetAffiliateContractRate(int affiliateId, int stateId, int productTypeId,DateTime invoiceDate)
        //{
        //    try
        //    {

        //        return mitsAffiliateContractRateSvc.GetSingle(a => a.AffiliateId == affiliateId && a.StateId == stateId && a.ServiceTypeId == 10 && a.ProductTypeId == productTypeId && invoiceDate >= a.RateStartDate && invoiceDate <= a.RateEndDate);
        //    }
        //    catch (Exception ex)
        //    {
        //        mitsLogger.Error("Failed to get all entity contract rate by AffiliateId: " + affiliateId + ",StateId:" + stateId, ex);
        //        return null;
        //    }
        //}

        public bool AddContractRates(List<AffiliateContractRate> rates)
        {
            try
            {
                MITSService<AffiliateContractRate> service = new MITSService<AffiliateContractRate>();
                foreach (var rate in rates)
                {
                    service.Add(rate);
                }
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to add entity contract rate", ex);
                return false;
            }
        }

        public bool UpdateContractRates(List<AffiliateContractRate> rates)
        {
            try
            {
                var service = new MITSService<AffiliateContractRate>();
                foreach (var newRate in rates)
                {
                    if (newRate.Id <= 0)
                    {
                        service.Add(newRate);
                    }
                    else
                    {
                        var oldRate = GetAffiliateContractRateByID(newRate.Id);
                        oldRate.RateStartDate = newRate.RateStartDate;
                        oldRate.RateEndDate = newRate.RateEndDate;
                        oldRate.QuantityTypeId = newRate.QuantityTypeId;
                        oldRate.ServiceTypeId = newRate.ServiceTypeId;
                        oldRate.Rate = newRate.Rate;
                        oldRate.PlanYear = newRate.PlanYear;
                        service.Save(oldRate);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to update entity contract rate", ex);
                return false;
            }
        }

        public bool DeleteContractRates(int affiliateId, int stateId, DateTime start, DateTime end, int serviceTypeId)
        {
            try
            {
                var service = new MITSService<AffiliateContractRate>();
                var rates = service.GetAll(x => x.AffiliateId == affiliateId
                    && x.StateId == stateId
                    && x.ServiceTypeId == serviceTypeId
                    && x.RateStartDate == start
                    && x.RateEndDate == end).ToList();
                foreach (var rate in rates)
                {
                    service.Delete(rate);
                }
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to delete entity contract rate", ex);
                return false;
            }
        }

        public bool IsExistContractRate(int id, int affiliateId, int stateId, DateTime start, DateTime end, int serviceTypeId)
        {
            MITSService<AffiliateContractRate> service = new MITSService<AffiliateContractRate>();
            if (id == 0)
            {
                var rates = service.GetAll(x => x.AffiliateId == affiliateId
                    && x.StateId == stateId
                    && x.ServiceTypeId == serviceTypeId
                    && (x.RateStartDate <= start && x.RateEndDate >= start || x.RateStartDate <= end && x.RateEndDate >= end));
                return rates.Count > 0;
            }
            else
            {
                var rate = service.GetSingle(x => x.Id == id);
                var rates = service.GetAll(x => x.AffiliateId == affiliateId
                    && x.StateId == stateId
                    && x.ServiceTypeId == serviceTypeId
                    && x.RateStartDate != rate.RateStartDate && x.RateEndDate != rate.RateEndDate
                    && (x.RateStartDate <= start && x.RateEndDate >= start || x.RateStartDate <= end && x.RateEndDate >= end));
                return rates.Count > 0;
            }
        }

        public IList<State> GetStates()
        {
            IList<State> _states = null;
            try
            {
                IList<State> unSortedList = mitsAffiliateContractRateSvc.MitsEntities.States.ToList();
                try
                {
                    IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
                    _states = sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _states = unSortedList;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get states list", ex);
            }
            return _states;
            
        }
        public IList<ProductType> GetProductTypes()
        {
            IList<ProductType> _productTypes = null;
            try
            {
                IList<ProductType> unSortedList = mitsAffiliateContractRateSvc.MitsEntities.ProductTypes.ToList();
                try
                {
                    IEnumerable<ProductType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                   _productTypes= sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _productTypes= unSortedList;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get product types list", ex);
            }
            return _productTypes;
        }
        public IList<ServiceType> GetServiceTypes()
        {
            IList<ServiceType> _serviceTypes = null;
            try
            {
                IList<ServiceType> unSortedList = mitsAffiliateContractRateSvc.MitsEntities.ServiceTypes.ToList();
                try
                {
                    IEnumerable<ServiceType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                    _serviceTypes=sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _serviceTypes=unSortedList;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get service types list", ex);
            }
            return _serviceTypes;
        }
        public IList<PriceUnitType> GetPriceUnitTypes()
        {
            IList<PriceUnitType> _priceUnits = null;
            try
            {
                IList<PriceUnitType> unSortedList = mitsAffiliateContractRateSvc.MitsEntities.PriceUnitTypes.ToList();
                try
                {
                    IEnumerable<PriceUnitType> sortedEnum = unSortedList.OrderBy(f => f.Type);
                    _priceUnits=sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _priceUnits= unSortedList;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get price unit types list", ex);
            }
            return _priceUnits;
        }
        public IList<QuantityType> GetQuantityTypes()
        {
            IList<QuantityType> _quantityTypes = null;
            try
            {
                IList<QuantityType> unSortedList = mitsAffiliateContractRateSvc.MitsEntities.QuantityTypes.ToList();
                try
                {
                    IEnumerable<QuantityType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                    _quantityTypes= sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _quantityTypes= unSortedList;
                }
            }
            catch(Exception ex)
            {
                mitsLogger.Error("Failed to get quantity types list", ex);
            }
            return _quantityTypes;
        }
    }
}
