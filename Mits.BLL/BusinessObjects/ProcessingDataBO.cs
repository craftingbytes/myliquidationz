﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System;
using Mits.Common;
using System.Text;

namespace Mits.BLL.BusinessObjects
{
    public class ProcessingDataBO
    {

        MITSService<ProcessingData> mitsProcessingDataSvc = null;
        MITSEntities mitsDataContext = null;
        MITSEntities entities = null;
        ISessionCookieValues _sessionValues = null;
        Logger logger = null;

        public ProcessingDataBO(ISessionCookieValues sessionValues)
        {
            _sessionValues = sessionValues;
            entities = new MITSEntities();
            mitsProcessingDataSvc = new MITSService<ProcessingData>();
            mitsDataContext = mitsProcessingDataSvc.MitsEntities;

            logger = new Logger(this.GetType());
        }


        public List<Document> GetDocsByPDataId(int pdId)
        {

            ForeignTable _forgeignTable = mitsDataContext.ForeignTables.ToList()
                .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "ProcessingData");

            int processDataTableId = _forgeignTable != null ? _forgeignTable.Id : 0;

            List<DocumentRelation> _docRels = mitsDataContext.DocumentRelations
                .Where(i => i.ForeignTableID == processDataTableId && i.ForeignID == pdId).ToList();


            List<Document> _docs = new List<Document>();
            foreach (DocumentRelation docrel in _docRels)
            {

                _docs.Add(mitsDataContext.Documents.FirstOrDefault(i => i.Id == docrel.DocumentID));
            }


            return _docs;
        }

        public ProcessingData GetProcessingDataByID(Int32 id)
        {
            return  mitsProcessingDataSvc.GetSingle(x => x.Id == id);
        }

        public ProcessingData GetProcessingDataByInvoiceID(Int32 id)
        {
            return mitsProcessingDataSvc.GetSingle(x => x.InvoiceId == id);
        }

        public int SaveAttachment(Document document)
        {
            try
            {
                DocumentType documentType = new DocumentType();
                String extension = System.IO.Path.GetExtension(document.UploadFileName);

                document.DocumentType = mitsDataContext.DocumentTypes.FirstOrDefault<DocumentType>(i => i.FileExtension == extension);
                mitsDataContext.Documents.Add(document);
                int affected = mitsDataContext.SaveChanges();

                return document.Id;
            }
            catch (Exception ex)
            {

            }
            return -1;
        }

        public bool DeleteProcessData(int Id,out string msg)
        {
            ProcessingData processdata = mitsDataContext.ProcessingDatas.Where(p => p.Id == Id).FirstOrDefault();
            Invoice invoice = mitsDataContext.Invoices.Where(i => i.Id == processdata.InvoiceId).FirstOrDefault();

            MITSService<InvoiceItem> mitsInvoiceItem = new MITSService<InvoiceItem>();
            MITSService<InvoiceItemReconciliation> invoiceItemRecon = new MITSService<InvoiceItemReconciliation>();


            MITSService<PaymentDetail> paymentDetail = new MITSService<PaymentDetail>();
            IEnumerable<PaymentDetail> payDetails = paymentDetail.GetAll().Where(X => X.InvoiceId == invoice.Id);
            
            if(payDetails.Count<PaymentDetail>() > 0)
            {
                msg = "The invoice of this processing data has been paid, it cannot be deleted!";
                return false;
            }
            

            foreach (InvoiceItem _invoiceItem in mitsInvoiceItem.GetAll().Where(X => X.InvoiceId == invoice.Id))
            {
                IEnumerable<InvoiceItemReconciliation> invoiceRecons = invoiceItemRecon.GetAll().Where(R => R.ReconcileFromInvoiceItemID == _invoiceItem.Id);
                if (invoiceRecons.Count<InvoiceItemReconciliation>() > 0)
                {
                    msg = "The Invoice of this processing data has reconciliation, it cannot be deleted!";
                    return false;
                }
                else
                {
                    mitsInvoiceItem.Delete(_invoiceItem);
                }
            }

            mitsDataContext.ProcessingDatas.Remove(processdata);
            mitsDataContext.Invoices.Remove(invoice);

            mitsDataContext.SaveChanges();
            msg = "Deleting succeed!";
            return true;

        }

        public bool SaveAttachmentRelation(DocumentRelation docRelation)
        {
            try
            {
                docRelation.ForeignTableID = mitsDataContext.ForeignTables.ToList()
                    .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "ProcessingData").Id;

                docRelation.Document = mitsDataContext.Documents.ToList().FirstOrDefault<Document>(i => i.Id == docRelation.DocumentID);
                docRelation.ForeignTable = mitsDataContext.ForeignTables.ToList()
                    .FirstOrDefault<ForeignTable>(i => i.Id == docRelation.ForeignTableID);


                mitsDataContext.DocumentRelations.Add(docRelation);
                int affected = mitsDataContext.SaveChanges();

                return affected > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public State GetRegion(string ZipStateProvince)
        {

            return mitsDataContext.States.FirstOrDefault<State>(i => i.Name == ZipStateProvince);

        }
        public State GetRegion(int regionId)
        {
            return mitsDataContext.States.Where(i => i.Id == regionId).FirstOrDefault();

        }
        public City GetCity(int cityId)
        {

            return mitsDataContext.Cities.FirstOrDefault<City>(i => i.Id == cityId);

        }
        public IList<CollectionMethod> GetCollectioneMethods()
        {

            //Sorting List
            IList<CollectionMethod> colMethods;
            IList<CollectionMethod> unSortedList = mitsDataContext.CollectionMethods.ToList();
            try
            {
                IEnumerable<CollectionMethod> sortedEnum = unSortedList.OrderBy(f => f.Method);
                colMethods = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
               colMethods=unSortedList;
            }
           
            CollectionMethod tmpColMethods = new CollectionMethod();
            tmpColMethods.Id = 0;
            tmpColMethods.Method = "";
            colMethods.Insert(0, tmpColMethods);

            return colMethods;

        }

        public IList<StateWeightCategory> GetStateWeightCategories(int stateid)
        {
            IList<StateWeightCategory> categories;
            categories = mitsDataContext.StateWeightCategories.Where(x => x.StateId == stateid).OrderBy(x => x.Name).ToList();
            StateWeightCategory category = new StateWeightCategory();
            category.Id = 0;
            category.Name = "Default";
            category.Ratio = 1;
            categories.Add(category);
            return categories.OrderBy(x => x.Id).ToList<StateWeightCategory>();
        }

        public IQueryable<InvoiceItem> GetInvoiceItems(int pageIndex, int pageSize, int invoiceId, out int totalRecords)
        {

            IQueryable<InvoiceItem> itemList = null;
            itemList = entities.InvoiceItems.Where(x => x.InvoiceId == invoiceId);
            totalRecords = itemList.Count<InvoiceItem>();
            return itemList.OrderBy(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public bool UpdateProcessingData(ProcessingData pd)
        {
            try
            {
                mitsProcessingDataSvc.Save(pd);
                int changed = mitsDataContext.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public Invoice GetInvoiceById(Int32 id)
        {
            return mitsDataContext.Invoices.FirstOrDefault(x => x.Id == id);
        }

        public InvoiceItem GetInvoiceItemById(Int32 id)
        {
            return mitsDataContext.InvoiceItems.FirstOrDefault(x => x.Id == id);
        }


        public bool UpdateInvoice(Invoice invoice)
        {
            try
            {
                new MITSService<Invoice>().Save(invoice);
            }
            catch (Exception) 
            {
                return false;
            }
            return true;
        }

        public IList<City> GetCities(int stateId)
        {

            //Sorting List
            IList<City> cities;
            IList<City> unSortedList1 = mitsDataContext.Cities.Where(x => x.StateId == stateId).ToList();
            try
            {
                IEnumerable<City> sortedEnum1 = unSortedList1.OrderBy(f => f.Name);
                cities = sortedEnum1.ToList();
            }
            catch (Exception ex)
            {
                cities = unSortedList1;
            }

            return cities;

        }

        public IQueryable<ProcessingData> GetPaged(int pageIndex, int pageSize, string searchString, string states,
            string beginDate, string endDate, int affiliateId, out int totalRecords)
        {

            int numricValue;

            if (string.IsNullOrEmpty(states) || states == "null")
            {
                states = string.Empty;
                 IList<State> statesList;
                IList<State> unSortedList = mitsDataContext.States.ToList();
                IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
                try
                {
                    statesList = sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    statesList = unSortedList;
                }
                for (int i = 0; i < statesList.Count; i++)
                {
                    if (i == statesList.Count - 1)
                        states += statesList[i].Name;
                    else
                        states += statesList[i].Name + ",";

                }

            }

            DateTime bDate = Convert.ToDateTime(beginDate);
            DateTime eDate = Convert.ToDateTime(endDate);
            bool isNumberic = int.TryParse(searchString, out numricValue);
            
            //Change by me for showing reports to E-World and Processor
            
            IQueryable<ProcessingData> processingList = null;
            string affiliateType = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateType);
            if (affiliateType == Constants.AffiliateType.Processor.ToString())
            {
                 processingList =
                processingList = mitsDataContext.ProcessingDatas.Include("Invoice").Where(x => (
                  !string.IsNullOrEmpty(searchString) ? x.Client.Contains(searchString) ||
                      x.RecievedBy.Contains(searchString) || x.ShipmentNo.Contains(searchString)
                   || (isNumberic ? x.Invoice.Id == numricValue || x.ShipmentNo.Contains(searchString) : false)
                       : true)

                       && ((!string.IsNullOrEmpty(beginDate) && !string.IsNullOrEmpty(endDate)) ? (x.ProcessingDate >= bDate && x.ProcessingDate <= eDate) : true)

                       && (!string.IsNullOrEmpty(x.State) ? states.Contains(x.State) : true)

                       && (x.Invoice.AffiliateId == affiliateId)

                      );
            }
            else if (affiliateType == Constants.AffiliateType.Eworld.ToString())
            {
                 processingList =
                processingList = mitsDataContext.ProcessingDatas.Include("Invoice").Where(x => (
                      !string.IsNullOrEmpty(searchString) ? x.Client.Contains(searchString) ||
                          x.RecievedBy.Contains(searchString) || x.ShipmentNo.Contains(searchString)
                       || (isNumberic ? x.Invoice.Id == numricValue || x.ShipmentNo.Contains(searchString) : false)
                           : true)

                           && ((!string.IsNullOrEmpty(beginDate) && !string.IsNullOrEmpty(endDate)) ? (x.ProcessingDate >= bDate && x.ProcessingDate <= eDate) : true)

                           && (!string.IsNullOrEmpty(x.State) ? states.Contains(x.State) : true)

                           //&& (x.Invoice.AffiliateId == affiliateId)

                          );

            }
            totalRecords = processingList.Count<ProcessingData>();
            return processingList.OrderByDescending(e => e.ProcessingDate).Skip(pageIndex * pageSize).Take(pageSize);



        }

        public bool EditProcessingData(int id, string shipmentNumber, string comments)
        {


            ProcessingData e2 = null;
            e2 = (from emp in mitsDataContext.ProcessingDatas
                  where emp.Id == id
                  select emp).FirstOrDefault();
            e2.ShipmentNo = shipmentNumber;//data from controller
            e2.Comments = comments;
            int affected = mitsDataContext.SaveChanges();

            return true;
        }

        public Affiliate GetAffiliateDetails(int affiliateId)
        {

            return mitsDataContext.Affiliates.Where(i => i.Id == affiliateId).FirstOrDefault(); //.OrderBy(e => e.AffiliateDetailTypeID)


        }
        public StateSpecificRate GetStateSpecificRate(string state)
        {
            return mitsDataContext.StateSpecificRates.FirstOrDefault(i => i.State.Abbreviation == state);
        }

        public bool Save(Invoice invoice, ProcessingData pdata, List<InvoiceItem> invoiceItems)
        {
            bool isSaved = true;
            try
            {


                mitsDataContext.Invoices.Add(invoice);

                // invoice = mitsDataContext.Invoices.FirstOrDefault(i => i.Id == invoice.Id);
                pdata.Invoice = invoice;

                mitsDataContext.ProcessingDatas.Add(pdata);
                foreach (var item in invoiceItems)
                {
                    item.Invoice = invoice;
                    
                    var rate = (from c in mitsDataContext.AffiliateContractRates
                               where c.AffiliateId == invoice.AffiliateId && c.StateId == invoice.StateId &&
                               c.ServiceTypeId == item.ServiceType.Id && c.ProductTypeId == item.ProductTypeId
                               && pdata.ProcessingDate > c.RateStartDate && pdata.ProcessingDate < c.RateEndDate
                               select c.Rate).FirstOrDefault();
                    item.Rate = Convert.ToDecimal(rate);
                    mitsDataContext.InvoiceItems.Add(item);
                }

                mitsDataContext.SaveChanges();
            }
            catch
            {
                isSaved = false;
            }
            return isSaved;
        }

        public bool Save(Invoice invoice, ProcessingData pdata)
        {
            bool isSaved = true;
            try
            {
                mitsDataContext.Invoices.Add(invoice);
                pdata.Invoice = invoice;
                mitsDataContext.ProcessingDatas.Add(pdata);
                mitsDataContext.SaveChanges();
            }
            catch
            {
                isSaved = false;
            }
            return isSaved;
        }

        public IEnumerable<ProductType> GetAllDevices()
        {
            return mitsDataContext.ProductTypes.Where(x => x.ConsumerSelectable == true).AsEnumerable<ProductType>(); //.OrderBy("ProductTypeName")

        }

        public IList<State> GetSupportedStates(int affiliateId)
        {
            IList<AffiliateContractRate> _affiliateContractRates = mitsDataContext.AffiliateContractRates.Where(c => c.AffiliateId == affiliateId).ToList();

            List<State> _affiliateSupportedStates = new List<State>();

            foreach (AffiliateContractRate afr in _affiliateContractRates)
            {
                if (_affiliateSupportedStates.Contains(afr.State) == false)
                    _affiliateSupportedStates.Add(afr.State);
            }
            
            //Sorting List
            IList<State> unSortedList=_affiliateSupportedStates;
            try
            {
                IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
                return sortedEnum.ToList();
            }
            catch (Exception ex)
            {
              return unSortedList;
            }

        }


        public IEnumerable<ProductType> GetSupportedProducts(int affiliateId , int stateId)
        {
            IList<AffiliateContractRate> _affiliateContractRates = mitsDataContext.AffiliateContractRates.Where(c => c.AffiliateId == affiliateId && c.StateId == stateId).ToList();

            List<ProductType> _affiliateSupportedProducts = new List<ProductType>();
            foreach (AffiliateContractRate afr in _affiliateContractRates)
            {
                if (afr.ProductTypeId != null)
                {
                    _affiliateSupportedProducts.Add(new MITSService<ProductType>().GetSingle(x => x.Id == afr.ProductTypeId.Value));
                }
            }
            
            //Sorting List
            IList<ProductType> unSortedList = _affiliateSupportedProducts;
            try
            {
                IEnumerable<ProductType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                return sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                return unSortedList;
            }
         

        }

        public IEnumerable<ProductType> GetSupportedProducts(int affiliateId, int stateId, int planYear)
        {
            IList<AffiliateContractRate> _affiliateContractRates = mitsDataContext.AffiliateContractRates.Where(c => c.AffiliateId == affiliateId && c.StateId == stateId && c.PlanYear == planYear).ToList();

            List<ProductType> _affiliateSupportedProducts = new List<ProductType>();
            foreach (AffiliateContractRate afr in _affiliateContractRates)
            {
                if (afr.ProductTypeId != null)
                {
                    _affiliateSupportedProducts.Add(new MITSService<ProductType>().GetSingle(x => x.Id == afr.ProductTypeId.Value));
                }
            }

            //Sorting List
            IList<ProductType> unSortedList = _affiliateSupportedProducts;
            try
            {
                IEnumerable<ProductType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                return sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                return unSortedList;
            }


        }

        public IList<ClientAddress> GetClientAddresses(int affiliateId)
        {
            return new MITSService<ClientAddress>().GetAll(x => x.AffiliateId == affiliateId).OrderBy(x => x.CompanyName).ToList();
        }

        public void SendInvoiceMail(string invoiceId)
        {
              String subject = "";
              StringBuilder message = new StringBuilder();

              try
              {
                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate _aff = new MITSService<Affiliate>().GetSingle(x => x.Id == affiliateId);

                  message.Append(Utility.BodyStart());

                  subject = "Invoice Verified: " + invoiceId;

                  message.Append(Utility.AddTextLine("Date: " + String.Format("{0:MM/dd/yy}", DateTime.Now)));
                  message.Append(Utility.AddBreak(2));
                  message.Append(Utility.AddTextLine("Invoice # " + invoiceId));
                  message.Append(Utility.AddBreak(2));
                  message.Append(Utility.AddTextLine("Company:" + _aff.Name));
                  message.Append(Utility.AddBreak(2));
                message.Append(Utility.AddTextLine("User:" + _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactName)));

                  message.Append(Utility.BodyEnd());

                  EmailHelper.SendMailAsync(subject, message.ToString(),
                      new string[] { ConfigHelper.InvoiceToEmail }, null,
                      new string[] { ConfigHelper.AdminEmail });

              }
              catch (Exception ex)
              {
                  //logger.Error("An invoice was creatd but there was an error attemping to email the notification. Invoice #" + invoiceId, ex);
                  logger.Error("An invoice was creatd but there was an error attemping to email the notification. Invoice #" + invoiceId + ex.Message, ex);
              }
                
            
        }
        //public bool Save(InvoiceEntity invoice, ProcessingDataEntity pdata, List<tInvoiceItemEntity> invoiceItems)
        //{
        //    ProcessingDataEntities entities = new ProcessingDataEntities();


        //    InvoiceDataContext mitsDataContext = new InvoiceDataContext();
        //    invoice.ProcessingDataEntities.Add(pdata);
        //    invoice.InvoiceItemEntities.AddRange(invoiceItems);

        //    mitsDataContext.InvoiceEntities.InsertOnSubmit(invoice);

        //    mitsDataContext.SubmitChanges();
        //    return true;

        //}
        //public IEnumerable<StateSupportedDevices> GetStateSpecificDevices(string state)
        //{
        //    return mitsDataContext.V_StateSupportedDevices.Where(i => i.StateProvinceAbbrev == state.ToUpper()).AsEnumerable<V_StateSupportedDevices>();

        //}

        public Int32 AddInvoiceItem(InvoiceItem invoiceItem)
        {
            Invoice invoice = GetInvoiceById(Convert.ToInt32(invoiceItem.InvoiceId));
            var rate = (    from c in mitsDataContext.AffiliateContractRates
                            where c.AffiliateId == invoice.AffiliateId 
                            && c.StateId == invoice.StateId 
                            && c.ServiceTypeId == invoiceItem.ServiceTypeId 
                            && c.ProductTypeId == invoiceItem.ProductTypeId
                            && invoice.PlanYear == c.PlanYear
                            select c.Rate).FirstOrDefault();
            invoiceItem.Rate = Convert.ToDecimal(rate);
            new MITSService<InvoiceItem>().Add(invoiceItem); 
            return invoiceItem.Id;
        }

        public bool UpdateInvoiceItem(InvoiceItem invoiceItem)
        {
            try
            {
                Invoice invoice = GetInvoiceById(Convert.ToInt32(invoiceItem.InvoiceId));
                var rate = (from c in mitsDataContext.AffiliateContractRates
                            where c.AffiliateId == invoice.AffiliateId
                            && c.StateId == invoice.StateId
                            && c.ServiceTypeId == invoiceItem.ServiceTypeId
                            && c.ProductTypeId == invoiceItem.ProductTypeId
                            && invoice.PlanYear == c.PlanYear
                            select c.Rate).FirstOrDefault();
                invoiceItem.Rate = Convert.ToDecimal(rate);
                new MITSService<InvoiceItem>().Save(invoiceItem);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool IsBelowTarget(Int32 invoiceId, decimal weight)
        {
            p_isBelowTarget_Result isBelowTarget = mitsDataContext.p_isBelowTarget(invoiceId, weight);
            return Convert.ToBoolean(isBelowTarget.Success);
        }

        public bool DeleteInvoiceItem(Int32 id)
        {
            try
            {
                new MITSService<InvoiceItem>().Delete(GetInvoiceItemById(id));
                return true;
            }
            catch (Exception)
            {
                return false;
            }

            
        }

        public string UpdateVerifyByInvoiceId(Int32 id)
        {
            string msg = string.Empty;
            try
            {
                Invoice currentInvoice = GetInvoiceById(id);

                if (currentInvoice.Verified == true)
                {

   
                    if (currentInvoice.Approved == true)
                    {
                        return "This Process Report is approved and cannot be unverified.";
                    }
                    else
                    {
                        currentInvoice.Verified = false;
                        new MITSService<Invoice>().Save(currentInvoice);
                        AuditEvent auditEvent = new AuditEvent();
                        auditEvent.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                        auditEvent.Entity = "Invoice";
                        auditEvent.AuditActionId = Convert.ToInt32(Mits.Common.Constants.ActionType.InvoiceUnverified);
                        auditEvent.EntityId = currentInvoice.Id ;
                        auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
                        auditEvent.TimeStamp = DateTime.Now;
                        //auditEvent.Note = "Unverified";
                        AddAuditEvent(auditEvent);
                        return "Success";
                    }

                }
                else
                {
                    currentInvoice.Verified = true;
                    new MITSService<Invoice>().Save(currentInvoice);
                    AuditEvent auditEvent = new AuditEvent();
                    auditEvent.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                    auditEvent.Entity = "Invoice";
                    auditEvent.AuditActionId = Convert.ToInt32(Mits.Common.Constants.ActionType.InvoiceVerified);
                    auditEvent.EntityId = currentInvoice.Id;
                    auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
                    auditEvent.TimeStamp = DateTime.Now;
                    //auditEvent.Note = "Verified";
                    AddAuditEvent(auditEvent);
                    SendInvoiceMail(id.ToString());

                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public Int32 AddAuditEvent(AuditEvent ae)
        {
            new MITSService<AuditEvent>().Add(ae);
            return ae.Id;
        }

    }
}

