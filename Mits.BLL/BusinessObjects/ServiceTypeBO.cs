﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;


namespace Mits.BLL.BusinessObjects
{
    public class ServiceTypeBO
    {
        MITSRepository<ServiceTypeGroupRelation> mitsServiceTypeRepo = new MITSRepository<ServiceTypeGroupRelation>();

        public ServiceTypeBO()
        {
            
        }

        //Getting all service types from ServiceTypeGroupRelation

        public IQueryable GetAllServiceType()
        {
            return mitsServiceTypeRepo.GetAll() as IQueryable;
        }


        //Getting a list of all service types from ServiceType    
  
        public IList<ServiceType> GetAll()
        {
            //Sorting List
            IList<ServiceType> _serviceTypeObj;
            IList<ServiceType> unSortedList = new MITSService<ServiceType>().GetAll();
            IEnumerable<ServiceType> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _serviceTypeObj = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _serviceTypeObj = unSortedList;
            }

            ServiceType _selectService = new ServiceType();
            _selectService.Id = 0;
            _selectService.Name = "Select";
            _serviceTypeObj.Insert(0, _selectService);
            return _serviceTypeObj;
        }

       


        //Checking If service type already exists        
        
        public bool Exists(string name)
        {
            IList<ServiceType> _serviceTypeObj = new MITSService<ServiceType>().GetAll();
            bool _existsFlag = false;
            for (var i = 0; i < _serviceTypeObj.Count; i++)
            {
                if ((name.Equals(_serviceTypeObj[i].Name, StringComparison.OrdinalIgnoreCase)))
                    _existsFlag = true;
            }
            return _existsFlag;

        }

        //Checking If Description already exists

        public bool DescExists(string name,string desc)
        {
            IList<ServiceType> _serviceTypeGroupObj = new MITSService<ServiceType>().GetAll();
            bool _existsFlag = false;
            for (var i = 0; i < _serviceTypeGroupObj.Count; i++)
            {
                if ((name.Equals(_serviceTypeGroupObj[i].Name, StringComparison.OrdinalIgnoreCase)))
                {
                    if ((desc.Equals(_serviceTypeGroupObj[i].Description, StringComparison.OrdinalIgnoreCase)))
                        _existsFlag = true;
                }
            }
            return _existsFlag;

        }


        //Returning Id of existing Service Type       

        public int ReturnId(string name)
        {
            IList<ServiceType> _serviceList = new MITSService<ServiceType>().GetAll();
            int _returnValue = 0;
            for (var i = 0; i < _serviceList.Count; i++)
            {
                if ((name.Equals(_serviceList[i].Name, StringComparison.OrdinalIgnoreCase)))
                    _returnValue = _serviceList[i].Id;
            }
            return _returnValue;

        }

        //Adding info in ServiceTypeGroupRelation

        public void AddServiceTypeGroupRelation(string[] _selectedGroups, int id)
        {
            foreach (var Groups in _selectedGroups)
            {
                ServiceTypeGroupRelation _serviceTypeGroupRelationObj = new ServiceTypeGroupRelation();
                _serviceTypeGroupRelationObj.ServiceTypeId = id;
                _serviceTypeGroupRelationObj.ServiceTypeGroupId = int.Parse(Groups);
                _serviceTypeGroupRelationObj.Active = true;
                new MITSService<ServiceTypeGroupRelation>().Add(_serviceTypeGroupRelationObj);

            }


        }



        #region Add Service Type and Associated Groups


        //Add service type and associated groups to ServiceTypeGroupRelation and return mulitple values via Dictionary

        public Dictionary<string, int> Add(string servicetype, string associatedGroups, string name, string desc)
        {
            
            int _id = int.Parse(servicetype);
            bool _samePolicy = default(bool);
            int _returnValue = default(int);
            
            //Dictionary Object to hold the return values
            
            Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();
            _returnDictionaryValue["duplicate"] = 0;
            _returnDictionaryValue["selectedId"] = _id;
            
            
            string[] _selectedGroups = associatedGroups.Split(',');

            ServiceTypeGroupRelation _check = new MITSService<ServiceTypeGroupRelation>().GetSingle(t => t.ServiceTypeId == _id);
            
            //If Data is directly entered without selecting from the dropdown

            if (_id == 0)
            {

                //New Entry of Service Type

                if ((this.Exists(name))!=true)
                {    
                    ServiceType _serviceTypeObj = new ServiceType();
                    _serviceTypeObj.Name = name;
                    _serviceTypeObj.Description = desc;
                    new MITSService<ServiceType>().Add(_serviceTypeObj);

                    //New Entry of Assocaited Groups for a new Service Type

                    if (_check == null)
                    {

                        this.AddServiceTypeGroupRelation(_selectedGroups, _serviceTypeObj.Id);
                    }

                    _returnDictionaryValue["selectedId"] = _serviceTypeObj.Id;
                    return _returnDictionaryValue;
                }
                
                //Old Entry of Service Type with description change 
    
                else if ((this.DescExists(name,desc)) != true)
                {
                    _returnValue = this.ReturnId(name);
                    ServiceType _serviceTypeObj = new MITSService<ServiceType>().GetSingle(x => x.Id == _returnValue);
                    _serviceTypeObj.Name = name;
                    _serviceTypeObj.Description = desc;
                    new MITSService<ServiceType>().Save(_serviceTypeObj);

                    //New Entry of associated groups for an old service type with only change in description

                    if (_check == null)
                    {

                        this.AddServiceTypeGroupRelation(_selectedGroups, _serviceTypeObj.Id);

                    }
                    _returnDictionaryValue["selectedId"] = _serviceTypeObj.Id;
                    return _returnDictionaryValue;
                }
                
                //No change in ServiceType is made 

                else
                {
                   
                    
                    _returnDictionaryValue["duplicate"] = 1;
                    return _returnDictionaryValue;
                }

            }

            //Old Entry of Service Type
            
            else
            {

               

                ServiceType _serviceTypeObj = new MITSService<ServiceType>().GetSingle(x => x.Id == _id);

                //If change is made in old entry then save changes

                if (!(_serviceTypeObj.Name.Equals(name, StringComparison.OrdinalIgnoreCase) && _serviceTypeObj.Description.Equals(desc)))
                {


                    _serviceTypeObj.Name = name;
                    _serviceTypeObj.Description = desc;
                    new MITSService<ServiceType>().Save(_serviceTypeObj);

                    //New Entry of Assocaited Groups for an old Service Type

                    if (_check == null)
                    {


                        this.AddServiceTypeGroupRelation(_selectedGroups, _serviceTypeObj.Id);

                        _returnDictionaryValue["selectedId"] = _serviceTypeObj.Id;
                        return _returnDictionaryValue;
                    }

                    //Old Entry of associated groups for an old Service Type

                    else
                    {
                      

                        MITSService<ServiceTypeGroupRelation> _saveServiceTypeGrouprelation = new MITSService<ServiceTypeGroupRelation>();
                        IList<ServiceTypeGroupRelation> _existingGrouprelations = new MITSService<ServiceTypeGroupRelation>().GetAll(t => t.ServiceTypeId == _check.ServiceTypeId & t.Active==true);

                        //Checks if the same policy is being submitted again then return else continue 

                        if (_existingGrouprelations.Count == _selectedGroups.Length)
                        {

                            foreach (var _selectPol in _selectedGroups)
                            {
                                _samePolicy = false;
                                foreach (ServiceTypeGroupRelation _eGroupRel in _existingGrouprelations)
                                {
                                    if (int.Parse(_selectPol) == _eGroupRel.ServiceTypeGroupId)
                                    {

                                        _samePolicy = true;
                                    }
                                }
                                if (_samePolicy == false)
                                {
                                    break;
                                }
                            }
                        }
                        if (_samePolicy == true)
                        {
                            _returnDictionaryValue["duplicate"] = 1;
                            return _returnDictionaryValue;
                        }

                        //Making All associated groups inactive against this particular Service Type



                        IList<ServiceTypeGroupRelation> _existingServiceTypeGrouprelations = new MITSService<ServiceTypeGroupRelation>().GetAll(t => t.ServiceTypeId == _check.ServiceTypeId);

                        foreach (ServiceTypeGroupRelation groupRelation in _existingServiceTypeGrouprelations)
                        {
                            groupRelation.Active = false;
                            _saveServiceTypeGrouprelation.Save(groupRelation);
                        }


                        //Saving old entries of associated groups by making them active
 
                        foreach (var sGroup in _selectedGroups)
                        {
                            bool newEntry = true;
                            foreach (ServiceTypeGroupRelation _gRelation in _existingServiceTypeGrouprelations)
                            {
                                if (int.Parse(sGroup) == _gRelation.ServiceTypeGroupId)
                                {

                                    _gRelation.Active = true;
                                    _saveServiceTypeGrouprelation.Save(_gRelation);
                                    newEntry = false;



                                }
                            }
                            //Check for new associated group entries and then add them

                            if (newEntry == true)
                            {
                                ServiceTypeGroupRelation _serviceTypeGroupRelationObj = new ServiceTypeGroupRelation();
                                _serviceTypeGroupRelationObj.ServiceTypeId = _id;
                                _serviceTypeGroupRelationObj.ServiceTypeGroupId = int.Parse(sGroup);
                                _serviceTypeGroupRelationObj.Active = true;
                                new MITSService<ServiceTypeGroupRelation>().Add(_serviceTypeGroupRelationObj);


                            }
                        }

                        _returnDictionaryValue["selectedId"] =_id;
                        return _returnDictionaryValue;
                    }
                }

                //If no change is made in Service Type then only check for associated groups

                else
                {
                    //New Entry of Assocaited Groups for an old Service Type

                    if (_check == null)
                    {
                        this.AddServiceTypeGroupRelation(_selectedGroups, _serviceTypeObj.Id);

                        _returnDictionaryValue["selectedId"] = _serviceTypeObj.Id;
                        return _returnDictionaryValue;

                    }

                    //Old Entry of associated groups for an old Service Type

                    else
                    {
                        

                        MITSService<ServiceTypeGroupRelation> _saveServiceTypeGrouprelation = new MITSService<ServiceTypeGroupRelation>();
                        IList<ServiceTypeGroupRelation> _existingGrouprelations = new MITSService<ServiceTypeGroupRelation>().GetAll(t => t.ServiceTypeId == _check.ServiceTypeId & t.Active == true);


                        //Checks if the same policy is being submitted again then return else continue 

                        if (_existingGrouprelations.Count == _selectedGroups.Length)
                        {

                            foreach (var _selectPol in _selectedGroups)
                            {
                                _samePolicy = false;
                                foreach (ServiceTypeGroupRelation _eManuPolicy in _existingGrouprelations)
                                {
                                    if (int.Parse(_selectPol) == _eManuPolicy.ServiceTypeGroupId)
                                    {

                                        _samePolicy = true;
                                    }
                                }
                                if (_samePolicy == false)
                                {
                                    break;
                                }
                            }
                        }
                        if (_samePolicy == true)
                        {
                            _returnDictionaryValue["duplicate"] = 1;
                            return _returnDictionaryValue;
                        }
                        
                        
                        //Making All associated groups inactive against this particular Service Type

                        IList<ServiceTypeGroupRelation> _existingServiceTypeGrouprelations = new MITSService<ServiceTypeGroupRelation>().GetAll(t => t.ServiceTypeId == _check.ServiceTypeId);

                        foreach (ServiceTypeGroupRelation groupRelation in _existingServiceTypeGrouprelations)
                        {
                            groupRelation.Active = false;
                            _saveServiceTypeGrouprelation.Save(groupRelation);
                        }

                        //Saving old entries of associated groups by making them active

                        foreach (var sGroup in _selectedGroups)
                        {
                            bool newEntry = true;
                            foreach (ServiceTypeGroupRelation _gRelation in _existingServiceTypeGrouprelations)
                            {
                                if (int.Parse(sGroup) == _gRelation.ServiceTypeGroupId)
                                {

                                    _gRelation.Active = true;
                                    _saveServiceTypeGrouprelation.Save(_gRelation);
                                    newEntry = false;



                                }
                            }

                            //Check for new associated group entries and then add them

                            if (newEntry == true)
                            {

                                ServiceTypeGroupRelation _serviceTypeGroupRelationObj = new ServiceTypeGroupRelation();
                                _serviceTypeGroupRelationObj.ServiceTypeId = _id;
                                _serviceTypeGroupRelationObj.ServiceTypeGroupId = int.Parse(sGroup);
                                _serviceTypeGroupRelationObj.Active = true;
                                new MITSService<ServiceTypeGroupRelation>().Add(_serviceTypeGroupRelationObj);

                            }
                        }

                        _returnDictionaryValue["selectedId"] = _id;
                        return _returnDictionaryValue;

                    }

                }
            }


        }
        #endregion

    }
}
