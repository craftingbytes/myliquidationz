﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class AuditReportBO
    {

        public List<AuditReport> GetAuditReports(int? stateId, string facilityName, string planyear,  int pageIndex, int pageSize, out int totalRecords)
        {
            MITSEntities mitsEntities = new MITSEntities();
            try
            {
                //DateTime _fromDate = default(DateTime);
                //DateTime _toDate = default(DateTime);
                //if (!string.IsNullOrEmpty(fromDate))
                //{
                //    _fromDate = DateTime.Parse(fromDate);
                //}
                //if (!string.IsNullOrEmpty(toDate))
                //{
                //    _toDate = DateTime.Parse(toDate);
                //}

                int _planyear = 0;
                int.TryParse(planyear, out _planyear);


                var auditReports = (from a in mitsEntities.AuditReports
                                    where (!stateId.HasValue || a.StateId == stateId.Value)
                                    && (string.IsNullOrEmpty(facilityName) || !string.IsNullOrEmpty(a.FacilityName) && a.FacilityName.IndexOf(facilityName) > -1)
                                    && (_planyear == 0 || a.PlanYear == _planyear)
                                    //&& (string.IsNullOrEmpty(fromDate) || a.AuditDate.HasValue && a.AuditDate.Value.CompareTo(_fromDate) >= 0)
                                    //&& (string.IsNullOrEmpty(toDate) || a.AuditDate.HasValue && a.AuditDate.Value.CompareTo(_toDate) <= 0)
                                    select a);

                //if (affiliateId.HasValue)
                //{
                //    auditReports = (from a in auditReports
                //                    where
                //                    (from s in mitsEntities.AffiliateTargets
                //                     where s.Weight.HasValue
                //                     && s.Weight.Value > 0
                //                     && s.StateId == a.StateId
                //                     && s.AffiliateId.Value == affiliateId
                //                     select s.StateId.Value
                //                     ).Contains(a.StateId.Value)
                //                    select a
                //                    );
                //}
                var results = auditReports.OrderByDescending(x => x.PlanYear).ThenBy(x => x.State.Name).ToList();
                totalRecords = results.Count();
                results = results.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                return results;
            }
            catch (Exception ex)
            {
                totalRecords = 0;
                return new List<AuditReport>();
            }
        }

        public int AddAuditReport(AuditReport auditReport, List<Document> documents)
        {
            try
            {
                MITSEntities mitsEntities = new MITSEntities();

                AuditReport _auditReport = mitsEntities.AuditReports.FirstOrDefault(x => x.FacilityName == auditReport.FacilityName && x.PlanYear == auditReport.PlanYear && x.StateId == auditReport.StateId);
                if (_auditReport == null)
                {
                    AuditReport _auditReportExisting = mitsEntities.AuditReports.FirstOrDefault(x => x.FacilityName == auditReport.FacilityName && x.PlanYear == auditReport.PlanYear && x.StateId != auditReport.StateId);

                    if (_auditReportExisting == null)
                    {
                        mitsEntities.AuditReports.Add(auditReport);
                        mitsEntities.SaveChanges();
                    }
                    else 
                    {
                        mitsEntities.p_AddAuditReportState(_auditReportExisting.Id, auditReport.StateId);
                        _auditReport = mitsEntities.AuditReports.FirstOrDefault(x => x.FacilityName == auditReport.FacilityName && x.PlanYear == auditReport.PlanYear && x.StateId == auditReport.StateId);
                        auditReport.Id = _auditReport.Id;
                    }
                }

                
                int reportId = auditReport.Id;

                int tableId = mitsEntities.ForeignTables.First<ForeignTable>(i => i.ForeignTableName == "AuditReport").Id;
                foreach (Document doc in documents)
                {
                    String extension = System.IO.Path.GetExtension(doc.UploadFileName);
                    doc.DocumentType = mitsEntities.DocumentTypes.FirstOrDefault<DocumentType>(i => i.FileExtension == extension);
                    //mitsEntities.Documents.Add(doc);
                    //mitsEntities.SaveChanges();
                    //int documentId = doc.Id;

                    DocumentRelation docRelation = new DocumentRelation();
                    docRelation.Document = doc;
                    docRelation.ForeignID = reportId;
                    docRelation.ForeignTableID = tableId;
                    mitsEntities.DocumentRelations.Add(docRelation);
                    mitsEntities.SaveChanges();
                    mitsEntities.p_AddDocumentToAuditReport(docRelation.Id);
                }
                //var auditReports = from a in mitsEntities.AuditReports
                //                   where    a.FacilityName == auditReport.FacilityName
                //                            && a.PlanYear ==  auditReport.PlanYear
                //                            && a.StateId != auditReport.StateId
                //                   select a;
                //foreach (AuditReport report in auditReports)
                //{
                //    mitsEntities.p_AddAuditReportState(reportId, report.StateId);
                //}

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //return new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError);
            }
            return auditReport.Id;// new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved);
        }

        public List<Document> GetDocuments(int auditReportId)
        {
            List<Document> documents = new List<Document>();

            MITSEntities mitsEntities = new MITSEntities();

            documents = (from d in mitsEntities.Documents
                         join r in mitsEntities.DocumentRelations
                         on d.Id equals r.DocumentID
                         join f in mitsEntities.ForeignTables
                         on r.ForeignTableID equals f.Id
                         where f.ForeignTableName == "AuditReport"
                         && r.ForeignID == auditReportId
                         select d).ToList();

            return documents.OrderBy(x => x.UploadFileName).ToList();
        }

        public Message AddDocument(int auditReportId, Document doc)
        {
            try
            {
                MITSEntities mitsEntities = new MITSEntities();
                int tableId = mitsEntities.ForeignTables.First<ForeignTable>(i => i.ForeignTableName == "AuditReport").Id;
                String extension = System.IO.Path.GetExtension(doc.UploadFileName);
                doc.DocumentType = mitsEntities.DocumentTypes.FirstOrDefault<DocumentType>(i => i.FileExtension == extension);

                DocumentRelation docRelation = new DocumentRelation();
                docRelation.Document = doc;
                docRelation.ForeignID = auditReportId;
                docRelation.ForeignTableID = tableId;
                mitsEntities.DocumentRelations.Add(docRelation);
                mitsEntities.SaveChanges();
                mitsEntities.p_AddDocumentToAuditReport(docRelation.Id);
                mitsEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                return new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError);
            }
            return new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved);
        }

        public AuditReport GetAuditReportById(int id)
        {
            MITSService<AuditReport> auditReportService = new MITSService<AuditReport>();
            return auditReportService.GetSingle(x => x.Id == id);
        }

        public bool UpdateAuditReport(AuditReport auditReport)
        {
            try
            {
                new MITSService<AuditReport>().Save(auditReport);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteAuditReport(int auditReportId)
        {
            try
            {
                var auditReport = GetAuditReportById(auditReportId);
                if (auditReport == null)
                {
                    return false;
                }

                MITSService<DocumentRelation> mitsDocumentRelationSvc = new MITSService<DocumentRelation>();
                foreach (DocumentRelation _documentRelation in mitsDocumentRelationSvc.GetAll().Where(X => X.ForeignID == auditReportId && X.ForeignTableID == 7))
                {
                    mitsDocumentRelationSvc.Delete(_documentRelation);
                }

                new MITSService<AuditReport>().Delete(auditReport);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
