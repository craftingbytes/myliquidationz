﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class ContractsBO
    {
        Logger mitsLogger = null;
        MITSService<Affiliate> mitsAffiliate=null;
        ISessionCookieValues _sessionValues = null;

        public ContractsBO(ISessionCookieValues sessionValues)
        {
            _sessionValues = sessionValues;
            mitsAffiliate =new MITSService<Affiliate>();
            mitsLogger = new Logger(this.GetType());

        }
        public List<Document> GetPaged(int pageIndex, int pageSize,out int totalRecords)
        {

            totalRecords = 0;

            try
            {
                int _oemId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                ForeignTable _forgeignTable = mitsAffiliate.MitsEntities.ForeignTables.ToList()
               .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "Affiliate");
                
                int _affiliateTableId = _forgeignTable != null ? _forgeignTable.Id : 0;

                List<DocumentRelation> _docRels = mitsAffiliate.MitsEntities.DocumentRelations
               .Where(i => i.ForeignTableID == _affiliateTableId && i.ForeignID == _oemId).ToList();

                List<Document> _docs = new List<Document>();
                foreach (DocumentRelation docrel in _docRels)
                {
                    _docs.Add(mitsAffiliate.MitsEntities.Documents.FirstOrDefault(i => i.Id == docrel.DocumentID));
                }
                //List<DocumentLib> _docLib = new List<DocumentLib>();
                //foreach (Document doc in _docs)
                //{
                //    _docLib.Add(mitsAffiliate.MitsEntities.DocumentLibs.FirstOrDefault(i => i.Id == doc.DocumentTypeId));
                //}

                totalRecords = _docs.Count;
                return _docs;
                
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed viewing documents", ex);
            }
            return null;
        }
    }
}
