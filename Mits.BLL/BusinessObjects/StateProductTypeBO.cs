﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;

namespace Mits.BLL.BusinessObjects
{
    public class StateProductTypeBO
    {

        public List<StateProductType> GetStateProductTypes(int pageIndex, int pageSize, int? stateId, out int totalRecords)
        {
            try
            {
                MITSService<StateProductType> service = new MITSService<StateProductType>();
                var events = service.GetAll(x => (!stateId.HasValue || x.StateId == stateId)).OrderBy(x => x.State.Name).ThenBy(x => x.ProductType.Name);
                totalRecords = events.ToList().Count;
                return events.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
                totalRecords = 0;
                return new List<StateProductType>();
            }
        }

        public bool IsExistsStateProductType(StateProductType stateProductType)
        {
            if (stateProductType.Id > 0)
            {
                var types = new MITSService<StateProductType>().GetAll(x => x.StateId == stateProductType.StateId 
                    && x.ProductTypeId == stateProductType.ProductTypeId
                    && x.Id != stateProductType.Id);
                return types.Count > 0;
            }
            else
            {
                var types = new MITSService<StateProductType>().GetAll(x => x.StateId == stateProductType.StateId && x.ProductTypeId == stateProductType.ProductTypeId);
                return types.Count > 0;
            }
        }

        public bool AddStateProductType(StateProductType stateProductType)
        {
            try
            {
                new MITSService<StateProductType>().Add(stateProductType);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public StateProductType GetStateProductTypeById(int id)
        {
            try
            {
                return new MITSService<StateProductType>().GetSingle(x => x.Id == id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateStateProductType(StateProductType stateProductType)
        {
            try
            {
                new MITSService<StateProductType>().Save(stateProductType);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteStateProductType(int id)
        {
            try
            {
                var stateProductType = GetStateProductTypeById(id);
                if (stateProductType != null)
                {
                    new MITSService<StateProductType>().Delete(stateProductType);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }
    }
}
