﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class PlanYearBO
    {

        MITSService<ProcessingData> mitsProcessingDataSvc = null;
        MITSEntities mitsDataContext = null;

        Logger logger = null;

        public PlanYearBO()
        {
            mitsProcessingDataSvc = new MITSService<ProcessingData>();
            mitsDataContext = mitsProcessingDataSvc.MitsEntities;

            logger = new Logger(this.GetType());
        }


        public IList<State> GetStates()
        {
            IList<State> states = null;
            try
            {
                IList<State> unSortedList = new MITSService<State>().GetAll();
                IEnumerable<State> enumSorted = unSortedList.OrderBy(f => f.Name);
                try
                {
                    states = enumSorted.ToList();
                }
                catch (Exception ex)
                {
                    states = unSortedList;
                }

                //State tmpState = new State();
                //tmpState.Id = 0;
                //tmpState.Name = "All State";
                //states.Insert(0, tmpState);
            }
            catch (Exception ex)
            {
                logger.Error("Failed Getting States", ex);

            }
            return states;

        }


        public IList<PlanYear> GetPlanYears(int pageIndex, int pageSize, int stateid, out int totalRecords)
        {
            IList<PlanYear> planyears = new List<PlanYear>();
            totalRecords = 0;

            try
            {
                if (stateid == null || stateid == 0)
                {
                    planyears = mitsDataContext.PlanYears.ToList();
                }
                else
                {
                    planyears = mitsDataContext.PlanYears.Where(x => x.State.Id == stateid).Distinct().ToList();
                }

                if (planyears != null)
                {
                    totalRecords = planyears.Count();
                    return planyears.OrderBy(p => p.Year).Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return planyears;
        }

        public bool DeletePlanYear(int id)
        {
            bool success = false;
            try
            {

                PlanYear entity = mitsDataContext.PlanYears.Where(e => e.Id == id).FirstOrDefault();
                mitsDataContext.PlanYears.Remove(entity);
                mitsDataContext.SaveChanges();

                success = true;
            }
            catch (Exception ex)
            {
                logger.Error("Failed delete PlanYear: " + id + "", ex);

            }

            return success;

        }

        public bool EditPlanYear(PlanYear entityToUpdate)
        {
            bool success = false;
            try
            {
                if (entityToUpdate.Id == 0)
                {
                    
                    PlanYear entity = new PlanYear();
                    
                    entity.StateId = entityToUpdate.StateId;
                    entity.Year = entityToUpdate.Year;
                    entity.StartDate = entityToUpdate.StartDate;
                    entity.EndDate = entityToUpdate.EndDate;
                    mitsDataContext.PlanYears.Add(entity);
                }
                else
                {
                    int id = entityToUpdate.Id;
                    PlanYear entity = mitsDataContext.PlanYears.Where(e => e.Id == id).FirstOrDefault();
                    entity.Year = entityToUpdate.Year;
                    entity.StartDate = entityToUpdate.StartDate;
                    entity.EndDate = entityToUpdate.EndDate;
                    entity.StateId = entityToUpdate.StateId;
                }

                mitsDataContext.SaveChanges();
                success = true;
            }
            catch (Exception ex)
            {
                logger.Error("Failed Editing PlanYear: " + entityToUpdate.Id.ToString(), ex);
            }
            return success;
        }




        


    }



}
