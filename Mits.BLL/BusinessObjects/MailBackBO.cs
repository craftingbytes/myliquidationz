﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System;
using Mits.Common;
using System.Text;
using System.Collections;
using Newtonsoft.Json;

namespace Mits.BLL.BusinessObjects
{
    public class MailBackBO
    {
        Logger logger = null;
        MITSEntities mitsDataContext = null;
        ISessionCookieValues _sessionValues = null;

        public MailBackBO(ISessionCookieValues sessionValues)
        {
            _sessionValues = sessionValues;
            mitsDataContext = new MITSService<MailBack>().MitsEntities;
            logger = new Logger(this.GetType());
        }
        public IList<State> GetStates()
        {
            IList<State> _states = new MITSService<State>().GetAll();
            State _tmpState = new State();
            _tmpState.Id = -1;
            _tmpState.Name = "All";
            _states.Insert(0, _tmpState);

            return _states;
        }
        public IList<Affiliate> GetAllOEMs()
        {
            return new MITSService<Affiliate>().GetAll(x=>x.AffiliateType.Type == "OEM");
        }

        public IList<ProductType> GetStateDevices(string state)
        {
            IList<AffiliateContractRate> _affiliateContractRates = null;

            if (!string.IsNullOrEmpty(state))
                _affiliateContractRates = mitsDataContext.AffiliateContractRates.Where(x => x.State.Abbreviation == state).ToList();


            List<ProductType> _stateSupportedProducts = new List<ProductType>();
            foreach (AffiliateContractRate afr in _affiliateContractRates)
            {
                if (!_stateSupportedProducts.Contains(afr.ProductType))
                {
                    ProductType product = new MITSService<ProductType>().GetSingle(x => x.Id == afr.ProductTypeId);
                    if (product != null)
                        _stateSupportedProducts.Add(product);
                }
            }

            return _stateSupportedProducts;

        }

        public bool AddMailBack(int qty, string size, int deviceTypeId, int mfgId, string model,
            bool needBoxes, int boxesQuantity, string boxSize, string mailbackAddress, string weight, string dimension)
        {
            MailBack entity = new MailBack();
            entity.MailBackAddress = mailbackAddress;
           

            MailBackDevice deviceEntity = new MailBackDevice();

            deviceEntity.ProductTypeId = deviceTypeId;
            deviceEntity.ManufacturerId = mfgId;
            deviceEntity.Model = model;
            deviceEntity.NeedBoxes = needBoxes;
            deviceEntity.Quantity = qty;
            deviceEntity.Size = size;
            deviceEntity.MailBack = entity;

            entity.MailBackDevices.Add(deviceEntity);


            mitsDataContext.MailBacks.Add(entity);
           
           int afffected = mitsDataContext.SaveChanges();

            return true;
        }

        public ProductType GetProductByTypeId(int typeId)
        {

            return mitsDataContext.ProductTypes.FirstOrDefault<ProductType>(i => i.Id == typeId);
        }
         public bool SendMailBack()
        {
            String subject = "Client Requested for MailBack";
            bool needboxes = false;
            bool _success = false;

            DataTable devices = MailBackDevices;

            StringBuilder message = new StringBuilder();
             message.Append(Utility.BodyStart());
             message.Append(Utility.AddTextLine("Dear Admin,"));
             message.Append(Utility.AddBreak(2));
            
            // message.Append("<html><body style='font-family:Calibri; font-size:11pt;'>Dear Admin,<br/><br/>");
             message.Append(Utility.AddTextLine((ConsumerDetails.FirstName+" "+ConsumerDetails.LastName).Trim() + 
                " has requested for MailBack services for the following devices."));
             message.Append(Utility.AddBreak(2));
           
            //message.Append((ConsumerDetails.FirstName + " " + ConsumerDetails.LastName).Trim() +
             //   " has requested for MailBack services for the following devices.<br/><br/>");

            for (int i = 0; i < devices.Rows.Count; i++)
            {
                Dictionary<object, object> _devices = new Dictionary<object, object>();

                DataRow row = devices.Rows[i];

                _devices.Add("Devices #", (i + 1).ToString());
                _devices.Add("Type", row["TypeName"]);
                _devices.Add("Manufacturer", row["ManufacturerName"]);
                _devices.Add("Model", row["Model"]);
                _devices.Add("Quantity", row["Quantity"]);
                _devices.Add("Weight", row["Weight"]);
                _devices.Add("Dimension", row["Dimension"]);
                _devices.Add("Size of device", row["Size"]);

                if (Convert.ToString(row["NeedBox"]).Equals("Yes"))
                {
                    _devices.Add("NeedBox", "He Need box(s) for the above Device");
                    needboxes = true;
                }
                else
                {
                    _devices.Add("NeedBox", "No");
                }

                message.Append(Utility.ConvertToHtmlTable(_devices));
            }
            
             message.Append(Utility.AddTextLine("The contact information is below."));
            //message.Append("The contact information is below.");

            Dictionary<object,object>  _contactInfo = new Dictionary<object,object>();

            _contactInfo.Add("First Name", ConsumerDetails.FirstName);
            _contactInfo.Add("Last Name", ConsumerDetails.LastName);
            _contactInfo.Add("Address", ConsumerDetails.Address);
            _contactInfo.Add("City", ConsumerDetails.City);
            _contactInfo.Add("State/Province", ConsumerDetails.State);
            _contactInfo.Add("Zip/Postal Code", ConsumerDetails.Zip);
            _contactInfo.Add("Phone Number", ConsumerDetails.Phone);
            _contactInfo.Add("Email", ConsumerDetails.Email);
            //_contactInfo.Add("", "");
            _contactInfo.Add("IP Address", System.Web.HttpContext.Current.Request.UserHostAddress);

            message.Append(Utility.ConvertToHtmlTable(_contactInfo));


            string _mailbackAddress ="Name:"+ ConsumerDetails.FirstName + " " + ConsumerDetails.LastName;
            _mailbackAddress += "Address:" + ConsumerDetails.Address + " " + ConsumerDetails.City + "," + ConsumerDetails.State + " " + ConsumerDetails.Zip;
            _mailbackAddress += "Email:" + ConsumerDetails.Email;

            //message.Append("<br/><br/>Manufacturer's Interstate Takeback System (MITS)<br/><br/>");
             message.Append(Utility.AddBreak(2));
    
             message.Append(Utility.AddTextLine("Manufacturer's Interstate Takeback System (MITS)"));
             message.Append(Utility.AddBreak(2));
             message.Append(Utility.BodyEnd());

            if (needboxes)
            {
                subject += " with Boxes";
            }
            try
            {
                _success = EmailHelper.SendMail(subject, message.ToString(),new string[] { ConfigHelper.MailBackToEmail },new string[] { ConfigHelper.MailBackCCEmail }, null);

                if (_success)
                {
                    foreach (DataRow row in MailBackDevices.Rows)
                    {

                        AddMailBack(
                            Convert.ToInt32(row["Quantity"]),
                            Convert.ToString(row["Size"]),
                            Convert.ToInt32(row["Type"]),
                            Convert.ToInt32(row["Manufacturer"]),
                            Convert.ToString(row["Model"]),
                            Convert.ToString(row["NeedBox"]).ToLower() == "yes" ? true : false,
                            Convert.ToString(row["BoxesQuantity"]) == "" ? 0 : Convert.ToInt32(row["BoxesQuantity"]),
                            Convert.ToString(row["BoxSize"]), 
                            _mailbackAddress,
                            Convert.ToString(row["Weight"]),
                            Convert.ToString(row["Dimension"]));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("A mailback was created but an error occured." + ex.Message , ex);
                _success = false;
            }

            return _success;

        }
         public DataTable MailBackDevices
        {
            get
            {
                DataTable obj = null;
                string json = _sessionValues.GetSessionValue(Constants.SessionParameters.MailBackDevices);
                if (json != "-1")
                {
                    obj = (DataTable)JsonConvert.DeserializeObject(Uri.UnescapeDataString(json));
                }
                return obj;
            }
            set
            {
                string json = JsonConvert.SerializeObject(value);
                if (!string.IsNullOrWhiteSpace(json))
                {
                    _sessionValues.SetSessionValue(Constants.SessionParameters.MailBackDevices, Uri.EscapeDataString(json));
                }
            }
        }
         public string ConsumerProductTypes
         {
             get { return _sessionValues.GetSessionValue(Constants.SessionParameters.ConsumerProductTypes); }
             set { _sessionValues.SetSessionValue(Constants.SessionParameters.ConsumerProductTypes, value); }
         }
         public string ConsumerProductManufacturerID
         {
            get { return _sessionValues.GetSessionValue(Constants.SessionParameters.ConsumerProductManufacturerID); }
            set { _sessionValues.SetSessionValue(Constants.SessionParameters.ConsumerProductManufacturerID, value); }
         }

         public Consumer ConsumerDetails
         {
            get
            {
                Consumer obj = null;
                string json = _sessionValues.GetSessionValue(Constants.SessionParameters.ConsumerDetails);
                if (json != "-1")
                {
                    obj = (Consumer)JsonConvert.DeserializeObject(Uri.UnescapeDataString(json));
                }
                return obj;
            }
            set
            {
                string json = JsonConvert.SerializeObject(value);
                if (!string.IsNullOrWhiteSpace(json))
                {
                    _sessionValues.SetSessionValue(Constants.SessionParameters.ConsumerDetails, Uri.EscapeDataString(json));
                }
            }
         }
     
    }

    public class Consumer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
