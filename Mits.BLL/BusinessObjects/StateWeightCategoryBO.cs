﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class StateWeightCategoryBO
    {
        MITSService<StateWeightCategory> mitsStateWeightCategorySvc = null;
        MITSEntities mitsDataContext = null;
        Logger logger = null;

        public StateWeightCategoryBO()
        {
            mitsStateWeightCategorySvc = new MITSService<StateWeightCategory>();
            mitsDataContext = mitsStateWeightCategorySvc.MitsEntities;

            logger = new Logger(this.GetType());
        }

        public IList<StateWeightCategory> GetStateWeightCategories(int stateid)
        {

            IList<StateWeightCategory> categories = new List<StateWeightCategory>();

            try
            {
                if (stateid == null || stateid == 0)
                {
                    categories = mitsDataContext.StateWeightCategories.Distinct().ToList();


                }
                else
                {
                    categories = mitsDataContext.StateWeightCategories.Where(x => x.State.Id == stateid).Distinct().ToList();


                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return categories;

        }

        public IList<State> GetStates()
        {
            IList<State> states = null;
            try
            {
                IList<State> unSortedList = new MITSService<State>().GetAll();
                IEnumerable<State> enumSorted = unSortedList.OrderBy(f => f.Name);
                try
                {
                    states = enumSorted.ToList();
                }
                catch (Exception ex)
                {
                    states = unSortedList;
                }

            }
            catch (Exception ex)
            {
                logger.Error("Failed Getting States", ex);

            }
            return states;

        }

        public IList<State> GetAvailableStates()
        {
            IList<StateWeightCategory> _stateCategories = mitsDataContext.StateWeightCategories.ToList<StateWeightCategory>();
            IList<State> _currentStates = new List<State>();

            foreach (StateWeightCategory category in _stateCategories)
            {
                if (!_currentStates.Contains(category.State))
                    _currentStates.Add(category.State);
            }

            IList<State> unSortedList = _currentStates;
            IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _currentStates = sortedEnum.ToList();
            }
            catch
            {
                _currentStates = unSortedList;
            }

            State _select = new State();
            _select.Id = 0;
            _select.Name = "Select";
            _currentStates.Insert(0, _select);
            return _currentStates;

        }
    }
}
