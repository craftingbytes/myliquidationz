﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using System.Data.Objects.DataClasses;

namespace Mits.BLL.BusinessObjects
{
    public class EventBO
    {

        MITSService<Event> mitsEventSvc = null;
        MITSEntities mitsDataContext = null;

        public EventBO()
        {
            mitsEventSvc = new MITSService<Event>();

            mitsDataContext = mitsEventSvc.MitsEntities;
        }


        public IList<State> GetStates()
        {
            IList<State> states;
            IList<State> unSortedList = new MITSService<State>().GetAll();
            IEnumerable<State> enumSorted = unSortedList.OrderBy(f => f.Name);
            try
            {
                states = enumSorted.ToList();
            }
            catch (Exception ex)
            {
                states = unSortedList;
            }

            State tmpState = new State();
            tmpState.Id = -1;
            tmpState.Name = "All";
            states.Insert(0, tmpState);

            return states;
        }

        public Reminder GetReminder(int remiderId)
        {


            return mitsDataContext.Reminders.FirstOrDefault(r => r.Id == remiderId);


        }

        public List<String> GetReminderAttatchments(int remiderId)
        {


            return mitsDataContext.ReminderAttachments.Include("Reminder")
                .Where(r => r.Reminder.Id == remiderId).Select(r => r.AttachmentName).ToList();


        }

        public void CloneRemindersInfo(int newEventId, int oldEventId)
        {
            // clone groups

            var groups = mitsDataContext.ReminderGroups.Include("Event").Include("ReminderGroupAffiliates").Where(g => g.EventId == oldEventId).ToList();

            ReminderGroup newGroup;
            EntityCollection<Affiliate> affiliates = new EntityCollection<Affiliate>();
            foreach (var group in groups)
            {
                newGroup = new ReminderGroup();
                var affList = group.ReminderGroupAffiliates.ToList();

                foreach (var item in affList)
                {
                    newGroup.ReminderGroupAffiliates.Add(item);
                }

                // newGroup.Affiliate.Add = affiliates;
                newGroup.Event = mitsDataContext.Events.FirstOrDefault(e => e.Id == newEventId);

                mitsDataContext.ReminderGroups.Add(newGroup);
                mitsDataContext.SaveChanges();

                var reminders = mitsDataContext.Reminders.Include("ReminderGroup").Include("ReminderStatu").
                    Include("Period").Where(r => r.ReminderGroupId == group.Id).ToList();

                Reminder rem;
                foreach (var reminder in reminders)
                {
                    rem = new Reminder();
                    rem.Emails = reminder.Emails;
                    rem.Number = reminder.Number;
                    rem.Period = reminder.Period;
                    // rem.ReminderAttachment = reminder.ReminderAttachment;
                    rem.ReminderGroup = mitsDataContext.ReminderGroups.FirstOrDefault(g => g.Id == newGroup.Id);
                    rem.ReminderStatu = mitsDataContext.ReminderStatus.FirstOrDefault(s => s.Id == reminder.ReminderStatu.Id);
                    rem.Title = reminder.Title;

                    mitsDataContext.Reminders.Add(rem);
                    mitsDataContext.SaveChanges();
                }


            }


        }

        public int UpdateRemider(Reminder rem)
        {

            mitsDataContext.SaveChanges();
            return rem.Id;
        }

        public List<EventType> GetEventType()
        {


            return mitsDataContext.EventTypes.OrderBy(e => e.Type).ToList();


        }

        public List<State> GetStateList()
        {
            return mitsDataContext.States.ToList();

        }


        public List<RecurrenceType> GetRecurrenceType()
        {


            return mitsDataContext.RecurrenceTypes.ToList();


        }
        public List<ReminderStatu> GetReminderStatus()
        {


            return mitsDataContext.ReminderStatus.Where(x => x.Status != "Completed").ToList();


        }

        public List<Period> GetReminderPeriod()
        {


            return mitsDataContext.Periods.ToList();


        }
        public List<Affiliate> GetAffiliates()
        {


            var aff = mitsDataContext.Affiliates;


            return aff.ToList();


        }

        public List<string> GetAffiliateEmails(int groupid)
        {

            List<string> emails = new List<string>();
            var group = mitsDataContext.ReminderGroups.Include("ReminderGroupAffiliates").Where(a => a.Id == groupid).FirstOrDefault();

            var affiliates = group.ReminderGroupAffiliates.ToList();

            foreach (var aff in affiliates)
            {
                if (aff.Affiliate.AffiliateContacts != null)
                {
                    foreach (AffiliateContact contact in aff.Affiliate.AffiliateContacts)
                    {
                        emails.Add(contact.Email);
                    }
                }

            }

            return emails;


        }

        public List<Event> GetTemplateEvents()
        {


            var list = mitsDataContext.Events.Where(e => e.IsTemplate == true);



            return list.ToList();


        }

        public bool IsEventTemplateDuplicate(string name)
        {


            if (mitsDataContext.Events.Where(e => e.TemplateName == name && e.IsTemplate == true).Count() > 0)
                return true;


            return false;


        }

        public Event GetEvent(int id)
        {

            return mitsDataContext.Events.Include("State").Include("EventType").Include("ReminderGroups").FirstOrDefault(e => e.Id == id);





        }

        public List<Affiliate> GetAffiliates(int affiliateType)
        {

            var aff = mitsDataContext.Affiliates.Where(a => a.AffiliateTypeId == affiliateType);
            return aff.ToList();

        }


        public void SaveFileData(int reminderId, string filePath, string fileName)
        {

            ReminderAttachment attachment = new ReminderAttachment();
            attachment.AttachmentName = fileName;
            attachment.AttachmentPath = filePath;
            attachment.Reminder = mitsDataContext.Reminders.FirstOrDefault(r => r.Id == reminderId);

            mitsDataContext.ReminderAttachments.Add(attachment);
            mitsDataContext.SaveChanges();

        }
        public int UpdateChanges(Event eventObj)
        {
            mitsDataContext.SaveChanges();
            return eventObj.Id;
        }



        public int SaveEvent(Event eventObj)
        {

            mitsDataContext.Events.Add(eventObj);
            mitsDataContext.SaveChanges();
            return eventObj.Id;
        }

        public List<Event> SearchEvents(string searchText, string states, DateTime start, DateTime end)
        {
            if (string.IsNullOrEmpty(states) || states == "null")
            {
                states = string.Empty;
                List<State> statesList = GetStateList();

                for (int i = 0; i < statesList.Count; i++)
                {
                    if (i == statesList.Count - 1)
                        states += statesList[i].Abbreviation;
                    else
                        states += statesList[i].Abbreviation + ",";

                }

            }

            List<Event> eventDetailList = mitsDataContext.Events.Where<Event>(x => (!x.IsTemplate.Value) && (!string.IsNullOrEmpty(searchText) ? x.Title.Contains(searchText)
                        || x.EventType.Type.Contains(searchText) || x.Description.Contains(searchText) : true)
                            && states.Contains(x.State.Abbreviation) && (x.DateBegin > start ||
                                x.DateBegin < end)).ToList();


            List<Event> recurringEventList = new List<Event>();
            foreach (Event eventDetail in eventDetailList)
            {
                if (eventDetail.DateEnd == null)
                    eventDetail.DateEnd = DateTime.Now.Date.AddYears(20);

                if (eventDetail.SingleRecurrenceValue != null)
                {

                    Event singleRecurrenceEventDetail = CloneEventDetail(eventDetail);

                    singleRecurrenceEventDetail.DateBegin = singleRecurrenceEventDetail.SingleRecurrenceValue.Value;
                    singleRecurrenceEventDetail.DateEnd = singleRecurrenceEventDetail.SingleRecurrenceValue.Value;
                    recurringEventList.Add(singleRecurrenceEventDetail);
                }
                else if (eventDetail.RecurrenceIntervalForDay != null && eventDetail.DateEnd != null)
                {
                    DateTime dailyEventDate = eventDetail.DateBegin.Value.AddDays(eventDetail.RecurrenceIntervalForDay.Value);
                    while (dailyEventDate <= eventDetail.DateEnd.Value)
                    {
                        Event dailyEvent = CloneEventDetail(eventDetail);
                        dailyEvent.DateBegin = dailyEventDate;
                        dailyEvent.DateEnd = dailyEventDate;
                        recurringEventList.Add(dailyEvent);
                        dailyEventDate = dailyEventDate.AddDays(eventDetail.RecurrenceIntervalForDay.Value);
                    }
                }
                else if (eventDetail.RecurrenceIntervalForMonth != null && eventDetail.DateEnd != null)
                {
                    DateTime monthlyEventDate = new DateTime(eventDetail.DateBegin.Value.Year, eventDetail.DateBegin.Value.Month + 1, 1);
                    monthlyEventDate = monthlyEventDate.AddDays(eventDetail.RecurrenceIntervalForMonth.Value - 1);
                    while (monthlyEventDate <= eventDetail.DateEnd.Value)
                    {
                        Event monthlyEvent = CloneEventDetail(eventDetail);
                        monthlyEvent.DateBegin = monthlyEventDate;
                        monthlyEvent.DateEnd = monthlyEventDate;
                        recurringEventList.Add(monthlyEvent);
                        monthlyEventDate = monthlyEventDate.AddMonths(1);
                    }


                }
                else if (eventDetail.RecurrenceIntervalQuaterly != null && eventDetail.DateEnd != null)
                {
                    DateTime nextDate = DateTime.Now;


                    DateTime quarterlyEventDate; // = AddQuarter(eventDetail.DateBegin.Value, eventDetail.RecurrenceIntervalQuaterly.Value, ref nextDate);

                    quarterlyEventDate = eventDetail.DateBegin.Value.AddMonths(3) ; //nextDate;

                    while (quarterlyEventDate <= eventDetail.DateEnd)
                    {
                        Event quarterlyEvent = CloneEventDetail(eventDetail);

                        //quarterlyEventDate = AddQuarter(quarterlyEventDate, eventDetail.RecurrenceIntervalQuaterly.Value, ref nextDate);
                        quarterlyEvent.DateBegin = quarterlyEventDate;
                        quarterlyEvent.DateEnd = quarterlyEventDate;
                        recurringEventList.Add(quarterlyEvent);
                        quarterlyEventDate = quarterlyEventDate.AddMonths(3); //nextDate;
                    }
                }
                else if (eventDetail.RecurrenceIntervalYearly != null && eventDetail.DateEnd != null)
                {
                    DateTime anualEventDate = eventDetail.RecurrenceIntervalYearly.Value.AddYears(1);

                    while (anualEventDate <= eventDetail.DateEnd)
                    {

                        Event anualEventDetail = CloneEventDetail(eventDetail);

                        anualEventDetail.DateBegin = anualEventDate;
                        anualEventDetail.DateEnd = anualEventDate;
                        recurringEventList.Add(anualEventDetail);
                        anualEventDate = anualEventDate.AddYears(1);
                    }
                }

            }


            return eventDetailList.Union<Event>(recurringEventList).ToList();

        }

        private DateTime AddQuarter(DateTime eventDate, int quarterDay, ref DateTime nextEventDate)
        {

            if (eventDate.Month <= 3)
            {
                eventDate = new DateTime(eventDate.Year, 1, quarterDay);
                nextEventDate = new DateTime(eventDate.Year, 4, quarterDay);
            }
            else if (eventDate.Month > 3 && eventDate.Month <= 6)
            {
                eventDate = new DateTime(eventDate.Year, 4, quarterDay);
                nextEventDate = new DateTime(eventDate.Year, 7, quarterDay);

            }
            else if (eventDate.Month > 6 && eventDate.Month <= 9)
            {
                eventDate = new DateTime(eventDate.Year, 7, quarterDay);
                nextEventDate = new DateTime(eventDate.Year, 10, quarterDay);
            }
            else if (eventDate.Month > 9 && eventDate.Month <= 12)
            {
                eventDate = new DateTime(eventDate.Year, 10, quarterDay);
                nextEventDate = new DateTime(eventDate.Year + 1, 1, quarterDay);

            }
            return eventDate;

        }
        private Event CloneEventDetail(Event eventDetail)
        {

            Event clonedEventDetail = new Event();

            clonedEventDetail.ActionDate = eventDetail.ActionDate;
            clonedEventDetail.DateBegin = eventDetail.DateBegin;
            clonedEventDetail.DateEnd = eventDetail.DateEnd;
            //clonedEventDetail.EntityKey = eventDetail.EntityKey;
            clonedEventDetail.Description = eventDetail.Description;
            clonedEventDetail.Id = eventDetail.Id;
            clonedEventDetail.Title = eventDetail.Title;
            clonedEventDetail.EventType = new EventType();
            clonedEventDetail.EventType.Id = eventDetail.EventType.Id;
            clonedEventDetail.EventType.Type = eventDetail.EventType.Type;
            clonedEventDetail.RecurrenceIntervalForDay = eventDetail.RecurrenceIntervalForDay;
            clonedEventDetail.RecurrenceIntervalForMonth = eventDetail.RecurrenceIntervalForMonth;
            clonedEventDetail.RecurrenceIntervalQuaterly = eventDetail.RecurrenceIntervalQuaterly;
            clonedEventDetail.RecurrenceIntervalYearly = eventDetail.RecurrenceIntervalYearly;
            clonedEventDetail.SingleRecurrenceValue = eventDetail.SingleRecurrenceValue;
            clonedEventDetail.State = new State();
            clonedEventDetail.State.Id = eventDetail.State.Id;
            clonedEventDetail.State.Abbreviation = eventDetail.State.Abbreviation;
            clonedEventDetail.State.Name = eventDetail.State.Name;
                



            return clonedEventDetail;


        }
        public List<Reminder> GetReminderByGroup(int groupId)
        {


            List<Reminder> list = mitsDataContext.Reminders.Include("Period").Include("ReminderStatu").Include("ReminderGroup")
                .Include("ReminderAttachments").Where(r => r.ReminderGroupId == groupId && r.ReminderStatu.Status != "Completed").ToList();


            return list;


        }

        public List<Reminder> GetReminderByEvent(int eventId, int pageIndex, int pageSize, string searchString, out int totalRecords)
        {

            ReminderGroup group = mitsDataContext.ReminderGroups.FirstOrDefault(x => x.EventId == eventId);
            if (group == null)
            {
                totalRecords = 0;
                return null;
            }

            totalRecords = mitsDataContext.Reminders.Include("Period").Include("ReminderStatu").Include("ReminderGroup")
                .Include("ReminderAttachments").Where(r => r.ReminderGroupId == group.Id && r.ReminderStatu.Status != "Completed").Count();

            List<Reminder> list = mitsDataContext.Reminders.Include("Period").Include("ReminderStatu").Include("ReminderGroup")
                .Include("ReminderAttachments").Where(r => r.ReminderGroupId == group.Id && r.ReminderStatu.Status != "Completed").OrderBy(g => g.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();


            return list;


        }

        public List<GroupEntity> GetReminderGroups(int eventId, int pageIndex, int pageSize, string searchString, out int totalRecords)
        {

            totalRecords = mitsDataContext.ReminderGroups.Where(r => r.EventId == eventId).Count();


            var groupList =
                mitsDataContext.ReminderGroups.Include("ReminderGroupAffiliates").Include("Event").Where(r => r.EventId == eventId).OrderBy(g => g.Id)
                .Skip(pageIndex * pageSize).Take(pageSize).ToList();


            List<GroupEntity> entityList = new List<GroupEntity>();


            GroupEntity entity;
            StringBuilder ids = new StringBuilder();
            StringBuilder names = new StringBuilder();
            StringBuilder emails = new StringBuilder();

            // TODO: N+1 queries problem (this could take minutes depending on the size of the collection)
            for (int i = 0; i < groupList.Count; i++)
            {
                entity = new GroupEntity();

                var collection = mitsDataContext.Entry(groupList[i]).Collection(s => s.ReminderGroupAffiliates);
                if (!collection.IsLoaded)
                {
                    collection.Load();
                }

                var affList = groupList[i].ReminderGroupAffiliates.ToList();
                names.Remove(0, names.Length);
                ids.Remove(0, ids.Length);
                emails.Remove(0, ids.Length);
                foreach (var aff in affList)
                {
                    ids.Append(aff.AffiliateId.ToString() + ",");
                    names.Append(aff.Affiliate.Name + ";");
                    //  emails.Append(mitsDataContext.AffiliateContactDetails.Include("Affiliate").FirstOrDefault(c=>c.Affiliate.AffiliateId== aff.AffiliateId).EMail+ ";");
                }
                entity.AffiliateIds = ids.ToString().TrimEnd(',');
                entity.AffiliateNames = names.ToString().TrimEnd(';');
                entity.ReminderGroupId = groupList[i].Id;

                entityList.Add(entity);

            }


            return entityList;


        }
        public int SaveGroupReminder(int eventId, string[] affiliates)
        {

            ReminderGroup grp = new ReminderGroup();

            grp.RowId = 2;
            grp.Event = mitsDataContext.Events.FirstOrDefault(e => e.Id == eventId);

            mitsDataContext.ReminderGroups.Add(grp);

            mitsDataContext.SaveChanges();

            
            var affList = new System.Data.Objects.DataClasses.EntityCollection<Affiliate>();
            ReminderGroupAffiliate rgaff =null;
            int id = 0;
            foreach (var item in affiliates)
            {
                if (item != "All" && item != "0" && item != "-1" && item != "-2" && item != "-3" && item != "-4" && item != "-5")
                {
                    rgaff =new ReminderGroupAffiliate();

                    id = Convert.ToInt32(item);
                    rgaff.Affiliate = mitsDataContext.Affiliates.FirstOrDefault(e => e.Id == id);
                    rgaff.ReminderGroup = grp;

                    mitsDataContext.ReminderGroupAffiliates.Add(rgaff);
                }
            }

            mitsDataContext.SaveChanges();

            return grp.Id;


        }

        public void DeleteEvent(int eventId)
        {
            var groups = mitsDataContext.ReminderGroups.Include("Event").Where(g => g.EventId == eventId).ToList();

            foreach (var group in groups)
            {
                mitsDataContext.ReminderGroups.Remove(group);
            }

            mitsDataContext.SaveChanges();

            var evnt = mitsDataContext.Events.FirstOrDefault(e => e.Id == eventId);
            mitsDataContext.Events.Remove(evnt);
            mitsDataContext.SaveChanges();

        }

        public void DeleteReminderGroup(int groupId)
        {
            var reminders = mitsDataContext.Reminders.Where(r => r.ReminderGroupId == groupId).ToList();
            var groupAffiliates = mitsDataContext.ReminderGroupAffiliates.Where(g => g.ReminderGroupId == groupId).ToList();
            var group = mitsDataContext.ReminderGroups.Include("Reminders").FirstOrDefault(e => e.Id == groupId);


            foreach (var reminder in reminders)
            {
                mitsDataContext.Reminders.Remove(reminder);
            }
            mitsDataContext.SaveChanges();

            foreach (var groupAffiliate in groupAffiliates)
            {
                mitsDataContext.ReminderGroupAffiliates.Remove(groupAffiliate);
            }
            mitsDataContext.SaveChanges();
            
            mitsDataContext.ReminderGroups.Remove(group);
            mitsDataContext.SaveChanges();

        }
        public int SaveReminder(int eventId, string number, int period, string title, int status, bool showOnCal)
        {

            ReminderGroup group = mitsDataContext.ReminderGroups.FirstOrDefault(x => x.EventId == eventId);
            if (group == null) {
                group = new ReminderGroup();
                group.EventId = eventId;
                group.RowId = 1;
                mitsDataContext.ReminderGroups.Add(group);
                mitsDataContext.SaveChanges();
            }


            Reminder reminder = new Reminder();
            reminder.ReminderGroup = mitsDataContext.ReminderGroups.FirstOrDefault(e => e.Id == group.Id);
            reminder.ReminderStatu = mitsDataContext.ReminderStatus.FirstOrDefault(e => e.Id == status);
            reminder.Period = mitsDataContext.Periods.FirstOrDefault(e => e.Id == period);
            reminder.Number = number;
            reminder.Title = title;
            reminder.ShowOnCalendar = showOnCal;
            reminder.Emails = "";
            mitsDataContext.Reminders.Add(reminder);
            mitsDataContext.SaveChanges();

            return reminder.Id;

        }

        public void SaveReminderAttachment(int reminderId, ReminderAttachment reminder)
        {
            reminder.Reminder = mitsDataContext.Reminders.FirstOrDefault(e => e.Id == reminderId);
            mitsDataContext.ReminderAttachments.Add(reminder);
            mitsDataContext.SaveChanges();

        }




        public int UpdateEvent(Event eventObj)
        {


            var updateEvent = mitsDataContext.Events.FirstOrDefault(e => e.Id == eventObj.Id);

            updateEvent.Title = eventObj.Title;
            updateEvent.Description = eventObj.Description;
            updateEvent.ActionDate = eventObj.ActionDate;
            updateEvent.DateBegin = eventObj.DateBegin;
            updateEvent.DateEnd = eventObj.DateEnd;
            updateEvent.EventType = eventObj.EventType;
            //updateEvent.EventTypeReference = eventObj.EventTypeReference;
            updateEvent.RecurrenceIntervalForDay = eventObj.RecurrenceIntervalForDay;
            updateEvent.RecurrenceIntervalForMonth = eventObj.RecurrenceIntervalForMonth;
            updateEvent.RecurrenceIntervalQuaterly = eventObj.RecurrenceIntervalQuaterly;
            updateEvent.RecurrenceIntervalYearly = eventObj.RecurrenceIntervalYearly;
            updateEvent.RecurrenceType = eventObj.RecurrenceType;

            //  updateEvent.ReminderGroup = eventObj.ReminderGroup;

            mitsDataContext.SaveChanges();

            return updateEvent.Id;

        }
        public class GroupEntity
        {
            public int ReminderGroupId { get; set; }

            public string AffiliateIds { get; set; }
            public string AffiliateNames { get; set; }
        }

    }
}
