﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    
    public class StateGuidelineBO
    {
        Logger mitsLogger = null;

        public StateGuidelineBO()
        {
            mitsLogger = new Logger(this.GetType());
        }

        // Gets StateGuideline by stateId
        public StateGuideline GetStateGuidelineByStateId(int stateId)
        {
            StateGuideline guideline = null;
            try
            {
                guideline = new MITSService<StateGuideline>().GetSingle(s => s.StateId == stateId);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get state guideline by stateId " + stateId + "", ex);
            }
            return guideline;
        }

        public bool SaveStateGuideline(StateGuideline guideline)
        {
            bool isSaved = true;
            try
            {
                StateGuideline updateGuideline = new MITSService<StateGuideline>().GetSingle(x => x.StateId == guideline.StateId);
                if (updateGuideline == null)
                {
                    StateGuideline newGuideline = new StateGuideline();
                    newGuideline.StateId = guideline.StateId;
                    newGuideline.GoverningBody = string.IsNullOrEmpty(guideline.GoverningBody) ? string.Empty : guideline.GoverningBody;
                    newGuideline.Website = string.IsNullOrEmpty(guideline.Website) ? string.Empty : guideline.Website;
                    newGuideline.Updates = string.IsNullOrEmpty(guideline.Updates) ? string.Empty : guideline.Updates;
                    newGuideline.OEMObligation = string.IsNullOrEmpty(guideline.OEMObligation) ? string.Empty : guideline.OEMObligation;
                    newGuideline.CoveredDevices = string.IsNullOrEmpty(guideline.CoveredDevices) ? string.Empty : guideline.CoveredDevices;
                    newGuideline.RegulationChange = string.IsNullOrEmpty(guideline.RegulationChange) ? string.Empty : guideline.RegulationChange;
                    newGuideline.RegulationType = string.IsNullOrEmpty(guideline.RegulationType) ? string.Empty : guideline.RegulationType;
                    newGuideline.LawState = guideline.LawState;
                    newGuideline.UpdateDate = DateTime.Now;
                    new MITSService<StateGuideline>().Add(newGuideline);
                }
                else
                {
                    updateGuideline.GoverningBody = string.IsNullOrEmpty(guideline.GoverningBody) ? string.Empty : guideline.GoverningBody;
                    updateGuideline.Website = string.IsNullOrEmpty(guideline.Website) ? string.Empty : guideline.Website;
                    updateGuideline.Updates = string.IsNullOrEmpty(guideline.Updates) ? string.Empty : guideline.Updates;
                    updateGuideline.OEMObligation = string.IsNullOrEmpty(guideline.OEMObligation) ? string.Empty : guideline.OEMObligation;
                    updateGuideline.CoveredDevices = string.IsNullOrEmpty(guideline.CoveredDevices) ? string.Empty : guideline.CoveredDevices;
                    updateGuideline.RegulationChange = string.IsNullOrEmpty(guideline.RegulationChange) ? string.Empty : guideline.RegulationChange;
                    updateGuideline.RegulationType = string.IsNullOrEmpty(guideline.RegulationType) ? string.Empty : guideline.RegulationType;
                    updateGuideline.LawState = guideline.LawState;
                    updateGuideline.UpdateDate = DateTime.Now;
                    new MITSService<StateGuideline>().Save(updateGuideline);
                }
                MapXMLHandler.CreateCustomerMapXMLFile();
            }
            catch
            {
                isSaved = false;
            }
            return isSaved;
        }

        public IList<StateGuideline_Contact> GetContactsByStateId(int stateId)
        {
            var contacts = new MITSService<StateGuideline_Contact>().GetAll(s => s.StateId == stateId);
            return contacts;
        }

        public bool SaveContact(StateGuideline_Contact contact)
        {
            bool isSaved = true;
            try
            {
                StateGuideline_Contact updateContact = new MITSService<StateGuideline_Contact>().GetSingle(x => x.Id == contact.Id);
                if (updateContact == null)
                {
                    StateGuideline_Contact newContact = new StateGuideline_Contact();
                    newContact.StateId = contact.StateId;
                    newContact.Name = string.IsNullOrEmpty(contact.Name) ? string.Empty : contact.Name;
                    newContact.Phone = string.IsNullOrEmpty(contact.Phone) ? string.Empty : contact.Phone;
                    newContact.EMail = string.IsNullOrEmpty(contact.EMail) ? string.Empty : contact.EMail;
                    new MITSService<StateGuideline_Contact>().Add(newContact);
                    contact.Id = newContact.Id;
                }
                else
                {
                    updateContact.Name = string.IsNullOrEmpty(contact.Name) ? string.Empty : contact.Name;
                    updateContact.Phone = string.IsNullOrEmpty(contact.Phone) ? string.Empty : contact.Phone;
                    updateContact.EMail = string.IsNullOrEmpty(contact.EMail) ? string.Empty : contact.EMail;
                    new MITSService<StateGuideline_Contact>().Save(updateContact);
                    contact.Id = updateContact.Id;
                }
                UpdateModifyDate(contact.StateId);
            }
            catch
            {
                isSaved = false;
            }
            return isSaved;
        }

        public bool DeleteContact(int id)
        {
            bool isDeleted = true;
            try
            {
                StateGuideline_Contact delContact = new MITSService<StateGuideline_Contact>().GetSingle(x => x.Id == id);
                if (delContact != null)
                {
                    new MITSService<StateGuideline_Contact>().Delete(delContact);
                    UpdateModifyDate(delContact.StateId);
                }
            }
            catch
            {
                isDeleted = false;
            }
            return isDeleted;
        }

        public IList<StateGuideline_DeadlineDate> GetDeadlineDatesByStateId(int stateId)
        {
            var deadlineDates = new MITSService<StateGuideline_DeadlineDate>().GetAll(s => s.StateId == stateId);
            return deadlineDates;
        }

        public bool SaveDeadlineDate(StateGuideline_DeadlineDate deadlineDate)
        {
            bool isSaved = true;
            try
            {
                StateGuideline_DeadlineDate updateDeadlineDate = new MITSService<StateGuideline_DeadlineDate>().GetSingle(x => x.Id == deadlineDate.Id);
                if (updateDeadlineDate == null)
                {
                    StateGuideline_DeadlineDate newDeadlineDate = new StateGuideline_DeadlineDate();
                    newDeadlineDate.StateId = deadlineDate.StateId;
                    newDeadlineDate.Name = string.IsNullOrEmpty(deadlineDate.Name) ? string.Empty : deadlineDate.Name;
                    newDeadlineDate.Date = string.IsNullOrEmpty(deadlineDate.Date) ? string.Empty : deadlineDate.Date;
                    new MITSService<StateGuideline_DeadlineDate>().Add(newDeadlineDate);
                    deadlineDate.Id = newDeadlineDate.Id;
                }
                else
                {
                    updateDeadlineDate.Name = string.IsNullOrEmpty(deadlineDate.Name) ? string.Empty : deadlineDate.Name;
                    updateDeadlineDate.Date = string.IsNullOrEmpty(deadlineDate.Date) ? string.Empty : deadlineDate.Date;
                    new MITSService<StateGuideline_DeadlineDate>().Save(updateDeadlineDate);
                    deadlineDate.Id = updateDeadlineDate.Id;
                }
                UpdateModifyDate(deadlineDate.StateId);
            }
            catch
            {
                isSaved = false;
            }
            return isSaved;
        }

        public bool DeleteDeadlineDate(int id)
        {
            bool isDeleted = true;
            try
            {
                StateGuideline_DeadlineDate delDeadlineDate = new MITSService<StateGuideline_DeadlineDate>().GetSingle(x => x.Id == id);
                if (delDeadlineDate != null)
                {
                    new MITSService<StateGuideline_DeadlineDate>().Delete(delDeadlineDate);
                    UpdateModifyDate(delDeadlineDate.StateId);
                }
            }
            catch
            {
                isDeleted = false;
            }
            return isDeleted;
        }

        public IList<StateGuideline_Link> GetLinksByStateId(int stateId)
        {
            var links = new MITSService<StateGuideline_Link>().GetAll(s => s.StateId == stateId);
            return links;
        }

        public bool SaveLink(StateGuideline_Link link)
        {
            bool isSaved = true;
            try
            {
                StateGuideline_Link updateLink = new MITSService<StateGuideline_Link>().GetSingle(x => x.Id == link.Id);
                if (updateLink == null)
                {
                    StateGuideline_Link newLink = new StateGuideline_Link();
                    newLink.StateId = link.StateId;
                    newLink.Name = string.IsNullOrEmpty(link.Name) ? string.Empty : link.Name;
                    newLink.Link = string.IsNullOrEmpty(link.Link) ? string.Empty : link.Link;
                    new MITSService<StateGuideline_Link>().Add(newLink);
                    link.Id = newLink.Id;
                }
                else
                {
                    updateLink.Name = string.IsNullOrEmpty(link.Name) ? string.Empty : link.Name;
                    updateLink.Link = string.IsNullOrEmpty(link.Link) ? string.Empty : link.Link;
                    new MITSService<StateGuideline_Link>().Save(updateLink);
                    link.Id = updateLink.Id;
                }
                UpdateModifyDate(link.StateId);
            }
            catch
            {
                isSaved = false;
            }
            return isSaved;
        }

        public bool DeleteLink(int id)
        {
            bool isDeleted = true;
            try
            {
                StateGuideline_Link delLink = new MITSService<StateGuideline_Link>().GetSingle(x => x.Id == id);
                if (delLink != null)
                {
                    new MITSService<StateGuideline_Link>().Delete(delLink);
                    UpdateModifyDate(delLink.StateId);
                }
            }
            catch
            {
                isDeleted = false;
            }
            return isDeleted;
        }

        private void UpdateModifyDate(int stateId)
        {
            var guideline = GetStateGuidelineByStateId(stateId);
            if (guideline == null)
            {
                guideline = new StateGuideline();
                guideline.StateId = stateId;
                guideline.UpdateDate = DateTime.Now;
                new MITSService<StateGuideline>().Add(guideline);
            }
            else
            {
                guideline.UpdateDate = DateTime.Now;
                new MITSService<StateGuideline>().Save(guideline);
            }            
        }
    }
}
