﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class DocumentLibBO
    {

        MITSService<DocumentLib> mitsDocumentLibSvc = null;

        Logger mitsLogger = null;

        public DocumentLibBO()
        {
            mitsDocumentLibSvc = new MITSService<DocumentLib>();

            mitsLogger = new Logger(this.GetType());

        }

        public void AddDocumentLib(DocumentLib entity)
        {
            try
            {
                mitsDocumentLibSvc.Add(entity);
            }
            catch(Exception ex)
            {
                mitsLogger.Error("Failed Adding dcoument: "+entity.Id+"", ex);
            }

        }

        public IQueryable<DocumentLib> GetPaged(string sidx, string sord, int pageIndex, int pageSize, string searchString, string states,int planyear, out int totalRecords)
        {
            totalRecords = 0;
            try
            {
                if (string.IsNullOrEmpty(states))
                {
                    states = string.Empty;
                    IList<State> statesList = new MITSService<State>().GetAll();

                    for (int i = 0; i < statesList.Count; i++)
                    {
                        if (i == statesList.Count - 1)
                            states += statesList[i].Name;
                        else
                            states += statesList[i].Name + ",";
                    }
                }

                //DateTime uploadDate;
                //bool isDate = DateTime.TryParse(searchString, out uploadDate);


                IQueryable<DocumentLib> documentLibList = mitsDocumentLibSvc.GetQueryable(x =>
                    (

                    (!string.IsNullOrEmpty(searchString) ? x.Title.Contains(searchString)
                    || x.Description.Contains(searchString)
                    || x.Tags.Contains(searchString)
                    //|| (isDate ? x.UploadDate == uploadDate : false)
                    : true)
                        //&& (!string.IsNullOrEmpty(x.StateId.ToString()) ? states.Contains(x.StateId.ToString()) : true)
                    
                    && (x.StateId != null ? states.Contains(x.State.Name) : true)
                    && (planyear != 0 ? x.PlanYear == planyear: true)

                    )

                    );

                if (documentLibList != null)
                {
                    totalRecords = documentLibList.Count();
                    if (sidx == "State")
                    {
                        if (sord == "asc")
                        {
                            documentLibList = documentLibList.OrderBy(x => x.State.Name);
                        }
                        else
                        {
                            documentLibList = documentLibList.OrderByDescending(x => x.State.Name);
                        }
                    }
                    else if (sidx == "Title")
                    {
                        if (sord == "asc")
                        {
                            documentLibList = documentLibList.OrderBy(x => x.Title);
                        }
                        else
                        {
                            documentLibList = documentLibList.OrderByDescending(x => x.Title);
                        }
                    }
                    else if (sidx == "Year")
                    {
                        if (sord == "asc")
                        {
                            documentLibList = documentLibList.OrderBy(x => x.PlanYear);
                        }
                        else
                        {
                            documentLibList = documentLibList.OrderByDescending(x => x.PlanYear);
                        }
                    }
                    else
                    {
                        documentLibList = documentLibList.OrderByDescending(x => x.PlanYear);
                    }
                    return documentLibList.Skip(pageIndex * pageSize).Take(pageSize);
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed Searching documents: "+searchString+"", ex);
            }
            return null;
        }


        public bool EditDocumentLib(DocumentLib entityToUpdate)
        {
            bool success = false;
            try
            {
                DocumentLib entity = mitsDocumentLibSvc.GetSingle(e => e.Id == entityToUpdate.Id);

                entity.Description = entityToUpdate.Description;

                entity.Title = entityToUpdate.Title;

                entity.Tags = entityToUpdate.Tags;

                entity.StateId = entityToUpdate.StateId;

                entity.UploadDate = entityToUpdate.UploadDate;
                entity.StateId= entityToUpdate.StateId;
                if (entityToUpdate.PlanYear != 0)
                    entity.PlanYear = entityToUpdate.PlanYear;

                mitsDocumentLibSvc.Save(entity);

                success = true;

            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed Editing document: "+entityToUpdate.Id+"", ex);
            }

            return success;
        }
        public bool DeleteDocumentLib(int id)
        {
            bool success = false;
            try
            {

                DocumentLib entity = mitsDocumentLibSvc.GetSingle(e => e.Id == id);
                mitsDocumentLibSvc.Delete(entity);

                success = true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed delete document: "+id+"", ex);

            }

            return success;



        }
        public bool AddDocumentLib(string title, string tags, string description, string uploadDate, string fileName, string State, string PlanYear)
        {
            bool success = false;
            try
            {

                DocumentLib entity = new DocumentLib();
                entity.Title = title;
                entity.Tags = tags;
                entity.Description = description;
                entity.AttachmentPath = fileName;
                if (!string.IsNullOrEmpty(uploadDate))
                {
                    entity.UploadDate = Convert.ToDateTime(uploadDate);
                }

                if (Convert.ToInt32(PlanYear) != 0)
                {
                    entity.PlanYear = Convert.ToInt32(PlanYear);
                }
                entity.State = new MITSService<State>().GetSingle(x => x.Name == State);

                mitsDocumentLibSvc.Add(entity);

                success = true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed Adding document: "+title+"", ex);

            }
            return success;
        }
        public IList<State> GetStates()
        {
            IList<State> states = null;
            try
            {
                IList<State> unSortedList = new MITSService<State>().GetAll();
                IEnumerable<State> enumSorted = unSortedList.OrderBy(f => f.Name);
                try
                {
                    states = enumSorted.ToList();
                }
                catch (Exception ex)
                {
                    states = unSortedList;
                }

                State tmpState = new State();
                tmpState.Id = -1;
                tmpState.Name = "All";
                states.Insert(0, tmpState);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed Editing document", ex);

            }
            return states;

        }
         
    }
}
