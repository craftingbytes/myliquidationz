﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System.Web.Mvc;
using Mits.Common;
namespace Mits.BLL.BusinessObjects
{
   public class InvoiceBO
    {


       MITSService<Invoice> mitsInvoiceSvc = null;
       Logger mitsLogger = null;
       MITSEntities mitsDataContext = null;
       public InvoiceBO()
        {
            mitsInvoiceSvc = new MITSService<Invoice>();
            mitsDataContext = mitsInvoiceSvc.MitsEntities;
            mitsLogger = new Logger();
        }

       //Getting a list of States

       public IList<State> GetStates()
       {
           //Sorting List
           IList<State> sortedList;
           IList<State> states = new MITSService<State>().GetAll();
           IEnumerable<State> sortedEnum = states.OrderBy(f => f.Name);
           try
           {
               sortedList = sortedEnum.ToList();
           }
           catch (Exception ex)
           {
               sortedList = states;
           }

           State _selectStates = new State();
           _selectStates.Id = 0;
           _selectStates.Name = "Select";
           sortedList.Insert(0, _selectStates);
           return sortedList;
       }

       public IList<State> GetSupportedStates(int affiliateId)
       {
           IList<AffiliateContractRate> _affiliateContractRates = mitsDataContext.AffiliateContractRates.Where(c => c.AffiliateId == affiliateId).ToList();

           List<State> _affiliateSupportedStates = new List<State>();

           foreach (AffiliateContractRate afr in _affiliateContractRates)
           {
               if (_affiliateSupportedStates.Contains(afr.State) == false)
                   _affiliateSupportedStates.Add(afr.State);
           }

           //Sorting List
           IList<State> unSortedList = _affiliateSupportedStates;
           try
           {
               IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
               return sortedEnum.ToList();
           }
           catch (Exception ex)
           {
               return unSortedList;
           }

       }
       public State GetRegion(int regionId)
       {
           return mitsDataContext.States.Where(i => i.Id == regionId).FirstOrDefault();
       }
       public City GetCity(int cityId)
       {
           return mitsDataContext.Cities.FirstOrDefault<City>(i => i.Id == cityId);

       }

       public IList<ProductType> GetProductTypes()
       {
           IList<ProductType> unSortedList = mitsInvoiceSvc.MitsEntities.ProductTypes.ToList();
           try
           {
               IEnumerable<ProductType> sortedEnum = unSortedList.OrderBy(f => f.Name);
               return sortedEnum.ToList();
           }
           catch (Exception ex)
           {
               return unSortedList;
           }
       }
       public IList<ServiceType> GetServiceTypes()
       {
           IList<ServiceType> unSortedList = mitsInvoiceSvc.MitsEntities.ServiceTypes.ToList();
           try
           {
               IEnumerable<ServiceType> sortedEnum = unSortedList.OrderBy(f => f.Name);
               return sortedEnum.ToList();
           }
           catch (Exception ex)
           {
               return unSortedList;
           }
       }
       public IList<PriceUnitType> GetPriceUnitTypes()
       {
           IList<PriceUnitType> unSortedList = mitsInvoiceSvc.MitsEntities.PriceUnitTypes.ToList();
           try
           {
               IEnumerable<PriceUnitType> sortedEnum = unSortedList.OrderBy(f => f.Type);
               return sortedEnum.ToList();
           }
           catch (Exception ex)
           {
               return unSortedList;
           }
       }
       public IList<QuantityType> GetQuantityTypes()
       {
           IList<QuantityType> unSortedList = mitsInvoiceSvc.MitsEntities.QuantityTypes.ToList();
           try
           {
               IEnumerable<QuantityType> sortedEnum = unSortedList.OrderBy(f => f.Name);
               return sortedEnum.ToList();
           }
           catch (Exception ex)
           {
               return unSortedList;
           }
       }
       public IList<Affiliate> GetAffiliate(bool isOEM)
       {
           //Sorting List
           IList<Affiliate> sortedList;
           string _oem = Mits.Common.Constants.AffiliateType.OEM.ToString();
           //IList<Affiliate> affiliates = new MITSService<Affiliate>().GetAll(x => x.AffiliateType.Type == _oem || x.AffiliateType.Id == 2);
           IList<Affiliate> affiliates;
           if (isOEM)
               affiliates = new MITSService<Affiliate>().GetAll(x => x.AffiliateType.Type == _oem);
           else
               affiliates = new MITSService<Affiliate>().GetAll(x => x.AffiliateType.Id == 2);

           IEnumerable<Affiliate> sortedEnum = affiliates.OrderBy(f => f.Name);
           try
           {
               sortedList = sortedEnum.ToList();
           }
           catch (Exception ex)
           {
               sortedList = affiliates;
           }

           Affiliate _selectAffiliates = new Affiliate();
           _selectAffiliates.Id = 0;
           _selectAffiliates.Name = "Select";
           sortedList.Insert(0, _selectAffiliates);
           return sortedList;
       }
       public decimal GetInvoiceAmount(Int32 invoiceID,int oemid,DateTime invoiceDate)
       {
           decimal totalAmount = 0;
           try
           {
               Invoice _invoice = mitsInvoiceSvc.MitsEntities.Invoices.Where(i => i.Id == invoiceID).FirstOrDefault();
               if (_invoice.Affiliate.AffiliateTypeId == 1)
               {
                   var Recs = from ir in mitsInvoiceSvc.MitsEntities.InvoiceReconciliations
                              where ir.OEMInvoiceId == invoiceID
                              select new { total = Math.Round((decimal)ir.Quantity) * ir.Rate };

                   foreach (var item in Recs)
                   {
                       totalAmount += item.total.Value;
                   }
    
               }
               else if (_invoice.Affiliate.AffiliateTypeId == 2)
               {
                   IList<InvoiceItem> Items = new MITSService<InvoiceItem>().GetAll(x => x.InvoiceId == invoiceID);
                   foreach (InvoiceItem item in Items)
                   {
                       var qual = item.Quantity.HasValue ? item.Quantity.Value : 0;
                       totalAmount += (qual * (item.Rate.HasValue ? item.Rate.Value : 0));
                   } 
               }

               IList<InvoiceAdjustment> adjustments = new MITSService<InvoiceAdjustment>().GetAll(x => x.InvoiceId == invoiceID);
               foreach (InvoiceAdjustment adjustment in adjustments)
               {
                   totalAmount += adjustment.Amount;
               }

           }
           catch (Exception ex)
           {
               mitsLogger.Error("Failed to get Invoice " + invoiceID + " amount", ex);
           }
           return totalAmount;
       }
       public List<Invoice> GetInvoicesPaged(int pageIndex, int pageSize, string searchEntity,string searchInvoice,string searchReceivable, string searchState,string searchDateTo,string searchDateFrom,  out int totalRecords,int affiliateType)
       {


           //DocumentLibEntities obj = new DocumentLibEntities();
           //DateTime uploadDate;
           // bool isDate = DateTime.TryParse(searchString, out uploadDate);

           //IQueryable<Affiliate> affiliateList = mitsAffliateSvc.MitsEntities.Affiliates.Where(x => x.Name.Contains(searchString));
           int _id=default(int);
           int _invoiceId=default(int);
           //bool _isReceivable=default(bool);
           int _state=default(int);
           DateTime dateTo=default(DateTime);
           DateTime dateFrom=default(DateTime);
           
           IList<Invoice> invoiceList = default(IList<Invoice>);
            List<Invoice> _invoice =default(List<Invoice>);
          if ((string.IsNullOrEmpty(searchEntity) || searchEntity=="0") && string.IsNullOrEmpty(searchInvoice) 
              //&& string.IsNullOrEmpty(searchReceivable) 
              && (string.IsNullOrEmpty(searchState) || searchState=="0") 
              && string.IsNullOrEmpty(searchDateTo) && string.IsNullOrEmpty(searchDateFrom))
          {
              invoiceList =mitsInvoiceSvc.GetAll(x=>x.Affiliate.AffiliateTypeId==affiliateType);
              _invoice = invoiceList.ToList(); 
          }
          else
          {
                 
                 bool _isIdVal = int.TryParse(searchEntity,out _id);
                 bool _isInvoiceId = int.TryParse(searchInvoice, out _invoiceId);
                 bool _isState = int.TryParse(searchState, out _state);
                 bool isdatefrom = DateTime.TryParse(searchDateFrom, out dateFrom);
                 bool isdateto = DateTime.TryParse (searchDateTo, out dateTo);
                 //_isReceivable = bool.Parse(searchReceivable);

                 invoiceList = mitsInvoiceSvc.GetAll(x => x.Affiliate.AffiliateTypeId == affiliateType);
                 _invoice = invoiceList.ToList(); 
                if (_isIdVal == true && searchEntity != "0")
                 {
                     IEnumerable<Invoice> entityList = _invoice.Where(x => x.AffiliateId == _id);
                    List<Invoice> _entityLst = entityList.ToList();
                     if (entityList.Count() > 0)
                     {
                         _invoice.Clear();
                         _invoice.AddRange(_entityLst);
                        
                        
                     }
                     else
                     {
                         _invoice.Clear();
                     }
                 }
                 if (_isInvoiceId == true)
                 {
                     IEnumerable<Invoice> invoiceIdList = _invoice.Where(x => x.Id == _invoiceId);
                     List<Invoice> _invoiceIdLst = invoiceIdList.ToList();
                     if (invoiceIdList.Count() > 0)
                     {
                         _invoice.Clear();
                         _invoice.AddRange(_invoiceIdLst);

                     }
                     else
                     {
                         _invoice.Clear();
                     }
                 }
                 if (_isState == true && searchState != "0")
                 {
                     IEnumerable<Invoice> stateList = _invoice.Where(x => x.StateId == _state);
                     List<Invoice> _stateLst = stateList.ToList();
                     if (stateList.Count() > 0)
                     {
                         _invoice.Clear();
                         _invoice.AddRange(_stateLst);

                     }
                     else
                     {
                         _invoice.Clear();
                     }
                 }
                 //if (_isReceivable == true)
                 //{
                 //    IEnumerable<Invoice> receiveList = _invoice.Where(x => x.Receivable == _isReceivable);
                 //    List<Invoice> _receiveLst = receiveList.ToList();
                 //    if (receiveList.Count() > 0)
                 //    {
                 //        _invoice.Clear();
                 //        _invoice.AddRange(_receiveLst);

                 //    }
                 //    else
                 //    {
                 //        _invoice.Clear();
                 //    }
                    
                 //}
                 if (isdatefrom && isdateto)
                 {
                     IEnumerable<Invoice> dateFromList = _invoice.Where(x => x.InvoiceDate >= dateFrom && x.InvoiceDate <= dateTo);
                     List<Invoice> dateFromLst = dateFromList.ToList();
                     if (dateFromList.Count() > 0)
                     {
                         _invoice.Clear();
                         _invoice.AddRange(dateFromLst);

                     }
                     else
                     {
                         _invoice.Clear();
                     }
                 }
                 else if (isdatefrom == true)
                 {
                     IEnumerable<Invoice> dateFromList = _invoice.Where(x => x.InvoiceDate >= dateFrom);
                     List<Invoice> dateFromLst = dateFromList.ToList();
                     if (dateFromList.Count() > 0)
                     {
                         _invoice.Clear();
                         _invoice.AddRange(dateFromLst);

                     }
                     else
                     {
                         _invoice.Clear();
                     }
                 }
                 else if (isdateto == true)
                 {
                     IEnumerable<Invoice> dateToList = _invoice.Where(x => x.InvoiceDate <= dateTo);
                     List<Invoice> dateToLst = dateToList.ToList();
                     if (dateToList.Count() > 0)
                     {
                         _invoice.Clear();
                         _invoice.AddRange(dateToLst);

                     }
                     else
                     {
                         _invoice.Clear();
                     }
                 }
     
          }
          if (_invoice != null)
          {
              totalRecords = _invoice.Count();
              return _invoice.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();//change
          }
          totalRecords = 0;
          return _invoice;


       } 
              
       public Invoice GetInvoiceByID(Int32 id) 
       {
           return mitsInvoiceSvc.GetSingle(x => x.Id == id);
       }
       public bool AddInvoice(Invoice invoice)
       {
           mitsInvoiceSvc.Add(invoice);
           return true;
       }
       public bool UpdateInvoice(Invoice invoice)
       {
           mitsInvoiceSvc.Save(invoice);
           return true;
       }
       
       public InvoiceItem GetInvoiceItemByID(Int32 id)
       {
           return new MITSService<InvoiceItem>().GetSingle(i => i.Id == id);
       }
       public bool AddInvoiceItem(InvoiceItem invoiceItem)
       {
           new MITSService<InvoiceItem>().Add(invoiceItem);
           return true;
       }
       public bool UpdateInvoiceItem(InvoiceItem invoiceItem)
       {
           new MITSService<InvoiceItem>().Save(invoiceItem);
           return true;
       }
       public bool DeleteInvoiceItem(Int32 id)
       {
           new MITSService<InvoiceItem>().Delete(GetInvoiceItemByID(id));
           return true;
       }

       public ProcessingData GetProcessingDataByInvoiceId(Int32 invoiceId)
       {
           return new MITSService<ProcessingData>().GetSingle(i => i.InvoiceId == invoiceId);
       }
       public bool UpdateProcessingData(ProcessingData processingData)
       {
           new MITSService<ProcessingData>().Save(processingData);
           return true;
       }
       public bool AddProcessingData(ProcessingData processingData)
       {
           new MITSService<ProcessingData>().Add(processingData);
           return true;
       }

       public int  GetDocCountByPDataId(int pdId)
       {

           ForeignTable _forgeignTable = mitsDataContext.ForeignTables.ToList()
               .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "ProcessingData");

           int processDataTableId = _forgeignTable != null ? _forgeignTable.Id : 0;

           List<DocumentRelation> _docRels = mitsDataContext.DocumentRelations
               .Where(i => i.ForeignTableID == processDataTableId && i.ForeignID == pdId).ToList();


           //List<Document> _docs = new List<Document>();
           //foreach (DocumentRelation docrel in _docRels)
           //{

           //    _docs.Add(mitsDataContext.Documents.FirstOrDefault(i => i.Id == docrel.DocumentID));
           //}


           return _docRels.Count();
       }

       public List<Document> GetDocsByPDataId(int pdId)
       {

           ForeignTable _forgeignTable = mitsInvoiceSvc.MitsEntities.ForeignTables.ToList()
               .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "Invoice");

           int invoiceTableId = _forgeignTable != null ? _forgeignTable.Id : 0;

           List<DocumentRelation> _docRels = mitsInvoiceSvc.MitsEntities.DocumentRelations
               .Where(i => i.ForeignTableID == invoiceTableId && i.ForeignID == pdId).ToList();


           List<Document> _docs = new List<Document>();
           foreach (DocumentRelation docrel in _docRels)
           {
               _docs.Add(mitsInvoiceSvc.MitsEntities.Documents.FirstOrDefault(i => i.Id == docrel.DocumentID));
           }


           return _docs;
       }

       public int SaveAttachment(Document document)
       {
           try
           {
               DocumentType documentType = new DocumentType();
               String extension = System.IO.Path.GetExtension(document.UploadFileName);

               document.DocumentType = mitsInvoiceSvc.MitsEntities.DocumentTypes.FirstOrDefault<DocumentType>(i => i.FileExtension == extension);
               mitsInvoiceSvc.MitsEntities.Documents.Add(document);
               int affected = mitsInvoiceSvc.MitsEntities.SaveChanges();

               return document.Id;
           }
           catch (Exception ex)
           {

           }
           return -1;
       }

       public bool SaveAttachmentRelation(DocumentRelation docRelation)
       {
           try
           {
               docRelation.ForeignTableID = mitsInvoiceSvc.MitsEntities.ForeignTables.ToList()
                   .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "Invoice").Id;

               docRelation.Document = mitsInvoiceSvc.MitsEntities.Documents.ToList().FirstOrDefault<Document>(i => i.Id == docRelation.DocumentID);
               docRelation.ForeignTable = mitsInvoiceSvc.MitsEntities.ForeignTables.ToList()
                   .FirstOrDefault<ForeignTable>(i => i.Id == docRelation.ForeignTableID);


               mitsInvoiceSvc.MitsEntities.DocumentRelations.Add(docRelation);
               int affected = mitsInvoiceSvc.MitsEntities.SaveChanges();

               return affected > 0;
           }
           catch (Exception ex)
           {

           }
           return false;
       }

       public string DeleteOEMInvoiceByInvoiceId(Int32 id )
       {
           string msg = string.Empty;
           try
           {
               Invoice currentInvoice = GetInvoiceByID(id);
               var paidItems = from pi in mitsInvoiceSvc.MitsEntities.PaymentDetails
                               where pi.InvoiceId == id
                               select pi.Id;
               if (paidItems.Count() > 0)
                   return "There are payments associated to this invoice";
               
               ReconciliationBO _reconcilliationBO = new ReconciliationBO();

               _reconcilliationBO.DeleteRecocilation(id);
               DeleteAdjustments(id);
               mitsInvoiceSvc.Delete(currentInvoice);
               msg = "Success";
           }
           catch (Exception ex)
           {
               return ex.Message;
           }
           return msg;
       }

       public void DeleteAdjustments(int invoiceId)
       {
           IList<InvoiceAdjustment> adjustments =
               new MITSService<InvoiceAdjustment>().GetAll(x => x.InvoiceId == invoiceId).ToList();
           
           foreach (InvoiceAdjustment adjustment in adjustments)
           {
               new MITSService<InvoiceAdjustment>().Delete(adjustment);
           }
       }

       public string UpdateApprovalByInvoiceId(Int32 id)
       {
           string msg = string.Empty;
           try
           {
               Invoice currentInvoice = GetInvoiceByID(id);

               if (currentInvoice.Approved == true)
               {

                   var reconciledItems = from ii in mitsInvoiceSvc.MitsEntities.InvoiceItems
                                         join ir in mitsInvoiceSvc.MitsEntities.InvoiceReconciliations on ii.Id equals ir.ProcesserInvoiceItemId
                                         where ii.InvoiceId == id
                                         select ii.Id;
                   var payments = from p in mitsInvoiceSvc.MitsEntities.PaymentDetails
                                  where p.InvoiceId == id
                                  select p.Id;

                   if (reconciledItems.Count() > 0 || payments.Count() > 0 )
                   {
                       return "There are items already reconciled to this invoice or payments made on this invoice.";
                   }
                   else
                   {
                       currentInvoice.Approved = false;
                       mitsInvoiceSvc.Save(currentInvoice);
                       return "Success";
                   }        

               }
               else
               {
                   currentInvoice.Approved = true;
                   currentInvoice.InvoiceDate = DateTime.Now;
                   mitsInvoiceSvc.Save(currentInvoice);
                   return "Success";
               }
           }
           catch (Exception ex)
           {
               return ex.Message;
           }

       }

       public string AddInvoiceAdjustment(Int32 InvoiceId, string Description, decimal Amount)
       {
           string msg = string.Empty;
           try
           {
               MITSEntities db = new MITSEntities();
               InvoiceAdjustment newAdjustment = new InvoiceAdjustment();
               newAdjustment.InvoiceId = InvoiceId;
               newAdjustment.Description = Description;
               newAdjustment.Amount = Amount;
               newAdjustment.AdjustmentDate = DateTime.Now; ;
               db.InvoiceAdjustments.Add(newAdjustment);
               db.SaveChanges();
               return "Success";
           }
           catch (Exception ex)
           {
               return ex.Message;
           }
       }
    }
}
