﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class AffiliateTargetBO
    {
         MITSService<AffiliateTarget> mitsAffiliateTargetSvc = null;
         Logger mitsLogger = null;   
        public AffiliateTargetBO()
        {
            mitsAffiliateTargetSvc = new MITSService<AffiliateTarget>();
            mitsLogger = new Logger(this.GetType());
        }

        public IList<AffiliateTarget> GetAllAffiliateTarget(Int32 pageIndex, Int32 pageSize, out Int32 totalRecords, Int32 affiliateId)
        {
            try
            {
                totalRecords = mitsAffiliateTargetSvc.GetAll().Where(at=>at.AffiliateId == affiliateId).ToList().Count;
                return mitsAffiliateTargetSvc.GetAll().Where(at => at.AffiliateId == affiliateId).OrderBy(x => x.State.Name).ThenBy(x => x.StartDate).ToList().Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex) 
            {
                mitsLogger.Error("Failed to get entity target list", ex);
                totalRecords = 0;
                return null;
            }
        }
        public AffiliateTarget GetAffiliateTargetByID(Int32 id)
        {
            try
            {
                return mitsAffiliateTargetSvc.GetSingle(a => a.Id == id);
            }
            catch (Exception ex) 
            {
                mitsLogger.Error("Failed to get entity targer by Id: " + id + "", ex);
                return null;
            }
        }

        public bool AddAffiliateTarget(AffiliateTarget affiliateTarget)
        {
            try
            {
                mitsAffiliateTargetSvc.Add(affiliateTarget);
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to add entity target: " + affiliateTarget.Id + "", ex);
                return false;
            }
        }
        public bool UpdateAffiliateTarget(AffiliateTarget affiliateTarget)
        {
            try
            {
                mitsAffiliateTargetSvc.Save(affiliateTarget);
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to update entity target: " + affiliateTarget.Id + "", ex);
                return false;
            }
        }

        public bool HasAffiliateContrateRate(AffiliateTarget affiliateTarget)
        {
            try
            {
                var service = new MITSService<AffiliateContractRate>();
                var rates = service.GetAll(x => x.AffiliateId == affiliateTarget.AffiliateId
                    && x.StateId == affiliateTarget.StateId
                    && x.ServiceTypeId == affiliateTarget.ServiceTypeId
                    && affiliateTarget.StartDate >= x.RateStartDate
                    && affiliateTarget.StartDate <= x.RateEndDate
                    && affiliateTarget.EndDate >= x.RateStartDate
                    && affiliateTarget.EndDate <= x.RateEndDate);
                return rates.Count > 0;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to check existing entity contract rate", ex);
                return false;
            }
        }

        public bool IsAlreadyExist(AffiliateTarget affiliateTarget)
        {
            try
            {
                return mitsAffiliateTargetSvc.GetAll().Where(ac =>
                    ac.AffiliateId == affiliateTarget.AffiliateId
                    && ((ac.StartDate <= affiliateTarget.StartDate && ac.EndDate >= affiliateTarget.StartDate) || (ac.StartDate <= affiliateTarget.EndDate && ac.EndDate >= affiliateTarget.EndDate))
                    && ac.StateId == affiliateTarget.StateId
                    && ac.PolicyId == affiliateTarget.PolicyId
                    && ac.ServiceTypeId == affiliateTarget.ServiceTypeId
                    && ac.QuantityTypeId == affiliateTarget.QuantityTypeId
                    //&& ac.Weight == affiliateTarget.Weight
                    && ac.Id != affiliateTarget.Id
                    ).Count() > 0;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to check existing entity target: " + affiliateTarget.Id + "", ex);
                return false;
            }
        }
        public bool DeleteAffiliateTarget(Int32 id)
        {
            try
            {
                AffiliateTarget _affiliateTarget = GetAffiliateTargetByID(id);
                if (_affiliateTarget != null)
                {
                    RemoveAllProducts(_affiliateTarget);
                    mitsAffiliateTargetSvc.Delete(_affiliateTarget);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex) 
            {
                mitsLogger.Error("Failed to delete entity target: " + id + "", ex);
                return false;
            }
        }

        public bool RemoveAllProducts(AffiliateTarget affiliateTarget) 
        {
            try
            {
                MITSService<AffiliateTargetProduct> _mitsAffiliateTargetProduct = new MITSService<AffiliateTargetProduct>();
                while (affiliateTarget.AffiliateTargetProducts.Count > 0)
                {
                    _mitsAffiliateTargetProduct.Delete(affiliateTarget.AffiliateTargetProducts.ToList()[0]);
                }
                return true;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to remove all products: " + affiliateTarget.Id + "", ex);
                return false;
            }
        }

        public bool AssignProducts(AffiliateTarget affiliateTarget,List<Int32> productIDs ) 
        {
            try
            {
                RemoveAllProducts(affiliateTarget);
                foreach (Int32 pID in productIDs)
                {
                    affiliateTarget.AffiliateTargetProducts.Add(new AffiliateTargetProduct() { AffiliateTargetId = affiliateTarget.Id, ProductTypeId = pID });
                }
                return UpdateAffiliateTarget(affiliateTarget);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to assign product to entity target: " + affiliateTarget.Id + "", ex);
                return false;
            }
        }

        public IList<State> GetStates()
        {
            IList<State> _states = null;
            try
            {
                IList<State> unSortedList = mitsAffiliateTargetSvc.MitsEntities.States.ToList();
                try
                {
                    IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
                    _states=sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _states=unSortedList;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get states list", ex);
            }
            return _states;
        }
        public IList<Policy> GetPolicies()
        {
            IList<Policy> _policies = null;
            try
            {
                IList<Policy> unSortedList = mitsAffiliateTargetSvc.MitsEntities.Policies.ToList();
                try
                {
                    IEnumerable<Policy> sortedEnum = unSortedList.OrderBy(f => f.Name);
                    _policies= sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _policies= unSortedList;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get policies list", ex);
            }
            return _policies;
        }
        public IList<ServiceType> GetServiceTypes()
        {
            IList<ServiceType> _services = null;
            try
            {
                IList<ServiceType> unSortedList = mitsAffiliateTargetSvc.MitsEntities.ServiceTypes.ToList();
                try
                {
                    IEnumerable<ServiceType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                    _services=sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _services=unSortedList;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get service types list", ex);
            }
            return _services;
        }
        public IList<QuantityType> GetQuantityTypes()
        {
            IList<QuantityType> _quantites = null;
            try
            {
                IList<QuantityType> unSortedList = mitsAffiliateTargetSvc.MitsEntities.QuantityTypes.ToList();
                try
                {
                    IEnumerable<QuantityType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                    _quantites= sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _quantites= unSortedList;
                }
              }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get quantity types list", ex);
            }
            return _quantites;
        }
        public IList<ProductType> GetProductTypes()
        {
            IList<ProductType> _products = null;
            try
            {
                IList<ProductType> unSortedList = mitsAffiliateTargetSvc.MitsEntities.ProductTypes.ToList();
                try
                {
                    IEnumerable<ProductType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                    _products= sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    _products= unSortedList;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get product types list", ex);
            }
            return _products;
        }

        public IList<ProductType> GetProductTypesByTargetId(int targetId)
        {
            var target = GetAffiliateTargetByID(targetId);
            var service = new MITSService<AffiliateContractRate>();
            var rates = service.GetAll(x => x.AffiliateId == target.AffiliateId
                    && x.StateId == target.StateId
                    && x.ServiceTypeId == target.ServiceTypeId
                    && target.StartDate >= x.RateStartDate
                    && target.StartDate <= x.RateEndDate
                    && target.EndDate >= x.RateStartDate
                    && target.EndDate <= x.RateEndDate);
            IList<ProductType> types = new List<ProductType>();
            foreach (var product in target.AffiliateTargetProducts)
            {
                if (!types.Contains(product.ProductType))
                {
                    types.Add(product.ProductType);
                }
            }
            foreach (var rate in rates)
            {
                if (!types.Contains(rate.ProductType))
                {
                    types.Add(rate.ProductType);
                }
            }
            return types.OrderBy(x => x.Name).ToList();
        }
    }
}
