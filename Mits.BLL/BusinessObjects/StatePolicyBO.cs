﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;

namespace Mits.BLL.BusinessObjects
{
    public class StatePolicyBO
    {
        MITSRepository<StatePolicy> mitsStateRepo = new MITSRepository<StatePolicy>();

        public StatePolicyBO()
        {
            
        }

        public IQueryable GetAllState()
        {
            return mitsStateRepo.GetAll() as IQueryable;
        }


        //Getting a list of all States

        public IList<State> GetAll()
        {
            //Sorting List
             IList<State> _stateList;
            IList<State> unSortedList = new MITSService<State>().GetAll();
            IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _stateList = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _stateList = unSortedList;
            }
            State select = new State();
            select.Id = 0;
            select.Name = "Select";
            _stateList.Insert(0, select);
            return _stateList;

        }

        //Getting a list of all Policies

        public IList<Policy> GetAllPolicy()
        {
            //Sorting List
             IList<Policy> policy ;
            IList<Policy> unSortedList = new MITSService<Policy>().GetAll();
            IEnumerable<Policy> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                policy = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                policy = unSortedList;
            }
            return policy;

        }

        //Add Policies against a particular state and return a Dictionary Object

        public Dictionary<string,int> Add(string state, string policies)
        {
            int _id = int.Parse(state);
            bool _newEntry = default(bool);
            bool _samePolicy = default(bool);
            string[] _selectedPolicies;
            _selectedPolicies = policies.Split(',');


            //Return multiple values via Dictionary object

            Dictionary<string, int> _returnDictionaryValue = new Dictionary<string, int>();
            _returnDictionaryValue["selectedId"] = _id;
            _returnDictionaryValue["duplicate"] = 0;



            StatePolicy _stateIdCheck = new MITSService<StatePolicy>().GetSingle(t => t.StateId == _id);
           
            //Check for new entry

            if (_stateIdCheck == null)
            {
                //new entry

                foreach (var Policies in _selectedPolicies)
                {
                    StatePolicy _statePolicyObj = new StatePolicy();
                    _statePolicyObj.StateId = _id;
                    _statePolicyObj.PolicyId = int.Parse(Policies);
                    _statePolicyObj.Active = true;
                    new MITSService<StatePolicy>().Add(_statePolicyObj);
                    _returnDictionaryValue["selectedId"] = _statePolicyObj.StateId;
                }
                return _returnDictionaryValue;
            }
            
            //Check for old entry
            else
            {
                //old entry need to update here, Get all existing policies against this old entry
                
                MITSService<StatePolicy> _saveStatePolicy = new MITSService<StatePolicy>();
                IList<StatePolicy> _existingPolicies = new MITSService<StatePolicy>().GetAll(t => t.StateId == _stateIdCheck.StateId & t.Active==true );

                
                //Checks if the same policy is being submitted again then return, else continue 

                if (_existingPolicies.Count == _selectedPolicies.Length)
                {

                    foreach (var _selectPol in _selectedPolicies)
                    {
                        _samePolicy = false;
                        foreach (StatePolicy _eStatePolicy in _existingPolicies)
                        {
                            if (int.Parse(_selectPol) == _eStatePolicy.PolicyId)
                            {

                                _samePolicy = true;
                            }
                        }
                        if (_samePolicy == false)
                        {
                            break;
                        }
                    }
                }
                if (_samePolicy == true)
                {
                    _returnDictionaryValue["duplicate"] = 1;
                    return _returnDictionaryValue;
                }

                
                //check for new policy entries

                IList<StatePolicy> _existingStatePolicies = new MITSService<StatePolicy>().GetAll(t => t.StateId == _stateIdCheck.StateId);

                foreach (StatePolicy policy in _existingStatePolicies)
                {
                    policy.Active = false;
                    _saveStatePolicy.Save(policy);
                }

                foreach (var _pol in _selectedPolicies)
                {
                    _newEntry = true;
                    foreach (StatePolicy _sPol in _existingStatePolicies)
                    {
                        if (int.Parse(_pol) == _sPol.PolicyId)
                        {

                            _sPol.Active = true;
                            _saveStatePolicy.Save(_sPol);
                            _newEntry = false;
                            _returnDictionaryValue["selectedId"] = _sPol.StateId;
                           
                           
                        }
                    }
                    if (_newEntry == true)
                    {
                        StatePolicy _statePolicyObj = new StatePolicy();
                        _statePolicyObj.StateId = _id;
                        _statePolicyObj.PolicyId = int.Parse(_pol);
                        _statePolicyObj.Active = true;
                        new MITSService<StatePolicy>().Add(_statePolicyObj);
                        _returnDictionaryValue["selectedId"] = _statePolicyObj.StateId;
                       
                    }
                }
                return _returnDictionaryValue;
            }


        }

       


    }
}
