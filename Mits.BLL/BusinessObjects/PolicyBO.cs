﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System.Collections;
using Mits.Common;
namespace Mits.BLL.BusinessObjects
{
    public class PolicyBO
    {
        MITSRepository<Policy> mitsPolicyRepo=null;
        Logger mitsLogger = null;
        
        public PolicyBO()
        {
            mitsPolicyRepo = new MITSRepository<Policy>();
            mitsLogger = new Logger(this.GetType());
        }

        //Getting a list of Policies
  
        public IList<Policy> GetAll()
        {
            //Sorting List
            IList<Policy> sortedList = null;
            try
            {
                IList<Policy> _policies = new MITSService<Policy>().GetAll();
                IEnumerable<Policy> sortedEnum = _policies.OrderBy(f => f.Name);
                try
                {
                    sortedList = sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    sortedList = _policies;
                }

                Policy _selectPolicy = new Policy();
                _selectPolicy.Id = 0;
                _selectPolicy.Name = "Select";
                sortedList.Insert(0, _selectPolicy);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get Policy list", ex);
            }
            return sortedList;
        }

        //Checking If policy already exists        

        public bool Exists(string name)
        {
            bool _existsFlag = false;
            try
            {
                IList<Policy> policyList = new MITSService<Policy>().GetAll();
                for (var i = 0; i < policyList.Count; i++)
                {
                    if ((name.Equals(policyList[i].Name, StringComparison.OrdinalIgnoreCase)))
                        _existsFlag = true;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to check for same policy name: "+name+"", ex);
            }
            return _existsFlag;

        }

        //Checking If Description already exists

        public bool DescExists(string desc)
        {
            bool _existsFlag = false;
            try
            {
                IList<Policy> _policyObj = new MITSService<Policy>().GetAll();
                for (var i = 0; i < _policyObj.Count; i++)
                {
                    if ((desc.Equals(_policyObj[i].Description)))
                        _existsFlag = true;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to check for same description: "+desc+" against a policy", ex);
            }
            return _existsFlag;

        }

        //Returning Id of existing Policy       

        public int ReturnId(string name)
        {
            int _returnValue = 0;
            try
            {
                IList<Policy> policyList = new MITSService<Policy>().GetAll();
                for (var i = 0; i < policyList.Count; i++)
                {
                    if ((name.Equals(policyList[i].Name, StringComparison.OrdinalIgnoreCase)))
                        _returnValue = policyList[i].Id;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to return Id of existing policy: "+name+"", ex);
            }
            return _returnValue;

        }

        //Add Policy detail in Policy and return multiple values via Dictionary object

        public Dictionary<string, int> Add(string policy, string name, string desc)
        {
            int _id = int.Parse(policy);
            int _returnValue = default(int);
            
            Dictionary<string, int> _retrunDictionaryValue = new Dictionary<string, int>();
            _retrunDictionaryValue["duplicate"] = 0;
            _retrunDictionaryValue["selectedId"] = _id;
            try
            {
                if (_id == 0)
                {
                    //New Entry of Policy by entering info in text boxes

                    if ((this.Exists(name)) != true)
                    {

                        Policy _policy = new Policy();
                        _policy.Name = name;
                        _policy.Description = desc;
                        new MITSService<Policy>().Add(_policy);
                        _retrunDictionaryValue["selectedId"] = _policy.Id;


                    }

                    //Old Entry by entering info in text boxes

                    else if ((this.DescExists(desc) != true))
                    {
                        _returnValue = this.ReturnId(name);
                        Policy _policy = new MITSService<Policy>().GetSingle(x => x.Id == _returnValue);
                        _policy.Name = name;
                        _policy.Description = desc;
                        new MITSService<Policy>().Save(_policy);
                        _retrunDictionaryValue["selectedId"] = _policy.Id;
                    }


                    else
                    {
                        _retrunDictionaryValue["duplicate"] = 1;

                    }
                }

               // Old Entry of Accreditation Type
                else
                {
                    Policy _policy = new MITSService<Policy>().GetSingle(x => x.Id == _id);

                    if (_policy.Description == null && desc != null)
                    {
                        _policy.Name = name;
                        _policy.Description = desc;
                        new MITSService<Policy>().Save(_policy);
                        _retrunDictionaryValue["selectedId"] = _policy.Id;
                    }

                    else if (!(_policy.Name.Equals(name, StringComparison.OrdinalIgnoreCase) && _policy.Description.Equals(desc)))
                    {
                        _policy.Name = name;
                        _policy.Description = desc;
                        new MITSService<Policy>().Save(_policy);
                        _retrunDictionaryValue["selectedId"] = _policy.Id;
                    }
                    else
                    {
                        _retrunDictionaryValue["duplicate"] = 1;

                    }

                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to add Policy: " + _id + "", ex);
            }
            return _retrunDictionaryValue;

        }
    }
}
