﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Mits.BLL.ViewModels;
using Mits.BLL.Interfaces;
using Mits.DAL.EntityModels;
using Mits.DAL;
using Mits.DAL.Interfaces;
using System.Data;
using System.Collections;
using Mits.Common;

namespace Mits.BLL.BusinessObjects
{
    public class AccreditationBO
    {
        
        MITSRepository<AccreditationType> mitsAccrediationRepo=null;
        Logger mitsLogger = null;
        public AccreditationBO()
        {
            mitsAccrediationRepo = new MITSRepository<AccreditationType>();
            mitsLogger = new Logger(this.GetType());
        }

        public IQueryable GetAllAccreditation()
        {
            IQueryable _accList = null;
            try
            {
                _accList= mitsAccrediationRepo.GetAll() as IQueryable;
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get accreditation list", ex);
            }
            return _accList;
        }

        //Getting a list of Accreditations
  
        public IList<AccreditationType> GetAll()
        {
            //Sorting List
            IList<AccreditationType> sortedList=null;
            try
            {
                IList<AccreditationType> accreditations = new MITSService<AccreditationType>().GetAll();
                IEnumerable<AccreditationType> sortedEnum = accreditations.OrderBy(f => f.Name);
                try
                {
                    sortedList = sortedEnum.ToList();
                }
                catch (Exception ex)
                {
                    sortedList = accreditations;
                }

                AccreditationType _selectAccreditation = new AccreditationType();
                _selectAccreditation.Id = 0;
                _selectAccreditation.Name = "Select";
                sortedList.Insert(0, _selectAccreditation);
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to get accrediation types list", ex);
            }
            return sortedList;
        }

        //Checking If accreditation already exists        

        public bool Exists(string name)
        {
            bool _existsFlag = false;
            try
            {
                IList<AccreditationType> accreditationList = new MITSService<AccreditationType>().GetAll();
                for (var i = 0; i < accreditationList.Count; i++)
                {
                    if ((name.Equals(accreditationList[i].Name, StringComparison.OrdinalIgnoreCase)))
                        _existsFlag = true;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to check for same accrediation name: "+name+"", ex);
            }
            return _existsFlag;

        }

        //Checking If Description already exists

        public bool DescExists(string desc)
        {
            bool _existsFlag = false;
            try
            {
                IList<AccreditationType> _accreditationObj = new MITSService<AccreditationType>().GetAll();
                for (var i = 0; i < _accreditationObj.Count; i++)
                {
                    if ((desc.Equals(_accreditationObj[i].Description)))
                        _existsFlag = true;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to check for same description: "+desc+" against an accrediation", ex);
            }
            return _existsFlag;

        }


        //Returning Id of existing Accreditation       

        public int ReturnId(string name)
        {
            int _returnValue = 0;
            try
            {
                IList<AccreditationType> accreditationList = new MITSService<AccreditationType>().GetAll();
                for (var i = 0; i < accreditationList.Count; i++)
                {
                    if ((name.Equals(accreditationList[i].Name, StringComparison.OrdinalIgnoreCase)))
                        _returnValue = accreditationList[i].Id;
                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to return Id of existing accreditation: "+name+"", ex);
            }
            return _returnValue;

        }

        //Add Accreditation detail in AccreditationType and return multiple values via Dictionary object

        public Dictionary<string, int> Add(string accreditation, string name, string desc)
        {
            int _id = int.Parse(accreditation);
            int _returnValue = default(int);
            
            Dictionary<string, int> _retrunDictionaryValue = new Dictionary<string, int>();
            _retrunDictionaryValue["duplicate"] = 0;
            _retrunDictionaryValue["selectedId"] = _id;
            try
            {
                if (_id == 0)
                {
                    //New Entry of Accreditation Type by entering info in text boxes

                    if ((this.Exists(name)) != true)
                    {

                        AccreditationType _accreditation = new AccreditationType();
                        _accreditation.Name = name;
                        _accreditation.Description = desc;
                        new MITSService<AccreditationType>().Add(_accreditation);
                        _retrunDictionaryValue["selectedId"] = _accreditation.Id;


                    }

                    //Old Entry by entering info in text boxes

                    else if ((this.DescExists(desc) != true))
                    {
                        _returnValue = this.ReturnId(name);
                        AccreditationType _accreditation = new MITSService<AccreditationType>().GetSingle(x => x.Id == _returnValue);
                        _accreditation.Name = name;
                        _accreditation.Description = desc;
                        new MITSService<AccreditationType>().Save(_accreditation);
                        _retrunDictionaryValue["selectedId"] = _accreditation.Id;
                    }


                    else
                    {
                        _retrunDictionaryValue["duplicate"] = 1;

                    }
                }

               // Old Entry of Accreditation Type
                else
                {
                    AccreditationType _accreditation = new MITSService<AccreditationType>().GetSingle(x => x.Id == _id);

                    if (_accreditation.Description == null && desc != null)
                    {
                        _accreditation.Name = name;
                        _accreditation.Description = desc;
                        new MITSService<AccreditationType>().Save(_accreditation);
                        _retrunDictionaryValue["selectedId"] = _accreditation.Id;
                    }

                    else if (!(_accreditation.Name.Equals(name, StringComparison.OrdinalIgnoreCase) && _accreditation.Description.Equals(desc)))
                    {
                        _accreditation.Name = name;
                        _accreditation.Description = desc;
                        new MITSService<AccreditationType>().Save(_accreditation);
                        _retrunDictionaryValue["selectedId"] = _accreditation.Id;
                    }
                    else
                    {
                        _retrunDictionaryValue["duplicate"] = 1;

                    }

                }
            }
            catch (Exception ex)
            {
                mitsLogger.Error("Failed to add Accreditation type: "+accreditation+"", ex);
            }
            return _retrunDictionaryValue;

        }
    }
}
