﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;
namespace Mits.BLL.ViewModels
{
    public class ProductTypeViewModel
    {

        public SelectList ProductType { get; private set; }
        public SelectList SiteImageId { get; private set; }
        public SelectList WeightQuantityType { get; private set; }
        public SelectList ContainerType { get; private set; }

       

        public ProductTypeViewModel()
      {

          //Getting a list of all Product type from ProductType and binding it with the selectlist
          IList<ProductType> _productList;
          IList<ProductType> unSortedList = new MITSService<ProductType>().GetAll();
          IEnumerable<ProductType> sortedEnum = unSortedList.OrderBy(f => f.Name);
          try
          {
              _productList = sortedEnum.ToList();
          }
          catch(Exception ex)
          {
              _productList = unSortedList;
          }

          ProductType _selectProduct = new ProductType();
          _selectProduct.Id = 0;
          _selectProduct.Name = "Select";
          _productList.Insert(0, _selectProduct);
          ProductType = new SelectList(_productList, "Id", "Name");

          //Getting a list of all Site Image Id from SiteImage and binding it with the selectlist
            IList<SiteImage> _imageList; 
          IList<SiteImage> unSortedList1 = new MITSService<SiteImage>().GetAll();
          IEnumerable<SiteImage> sortedEnum1 = unSortedList1.OrderBy(f => f.Name);
          try
          {
              _imageList = sortedEnum1.ToList();
          }
          catch (Exception ex)
          {
              _imageList = unSortedList1;
          }

          SiteImage _selectImage = new SiteImage();
          _selectImage.Id = 0;
          _selectImage.Name = "Select";
          _imageList.Insert(0, _selectImage);
          SiteImageId = new SelectList(_imageList, "Id", "Name");

          //Getting a list of all Weight quantity type from QuantityType and binding it with the selectlist
          IList<QuantityType> _weightList;
          IList<QuantityType> unSortedList2 = new MITSService<QuantityType>().GetAll(t => t.QuantityTypeGroupId == 1);
          IEnumerable<QuantityType> sortedEnum2 = unSortedList2.OrderBy(f => f.Name);
          try
          {
              _weightList = sortedEnum2.ToList();
          }
          catch (Exception ex)
          {
              _weightList = unSortedList2;
          }

          QuantityType _selectWeight = new QuantityType();
          _selectWeight.Id = 0;
          _selectWeight.Name = "Select";
          _weightList.Insert(0, _selectWeight);
          WeightQuantityType = new SelectList(_weightList, "Id", "Name");

          //Getting a list of all Container type from QuantityType and binding it with the selectlist
          IList<QuantityType> _containerList;
          IList<QuantityType> unSortedList3 = new MITSService<QuantityType>().GetAll(t => t.QuantityTypeGroupId == 4);
          IEnumerable<QuantityType> sortedEnum3 = unSortedList3.OrderBy(f => f.Name);
          try
          {
              _containerList = sortedEnum3.ToList();
          }
          catch (Exception ex)
          {
              _containerList = unSortedList3;
          }

          QuantityType _selectContainer = new QuantityType();
          _selectContainer.Id = 0;
          _selectContainer.Name = "Select";
          _containerList.Insert(0, _selectContainer);
          ContainerType = new SelectList(_containerList, "Id", "Name");
         
          
          

      }




    }
}
