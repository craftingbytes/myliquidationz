﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
namespace Mits.BLL.ViewModels
{
    public class StatePolicyViewModel
    {
        public SelectList States { get; private set; }
        public IList<Policy> Policies { get; private set; }


        public StatePolicyViewModel(State _state)
      
        {
          
         //Get a list of all states and bind them to selectlist
            IList<State> _stateList;
            IList<State> unSortedList = new MITSService<State>().GetAll();
          IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
          try
          {
              _stateList = sortedEnum.ToList();
          }
          catch (Exception ex)
          {
              _stateList = unSortedList;
          }
          State selectState = new State();
          selectState.Id = 0;
          selectState.Name = "Select";
          _stateList.Insert(0, selectState);
          States = new SelectList(_stateList, "Id", "Name",_state.Id);


          //Get a list of all policies  

          MITSService<Policy> unSortedList1 = new MITSService<Policy>();
          IEnumerable<Policy> sortedEnum1 = (unSortedList1.GetAll()).OrderBy(f => f.Name);
          try
          {
              Policies = sortedEnum1.ToList();
          }
          catch (Exception ex)
          {
              Policies = unSortedList1.GetAll();
          }
          
          
          

      }


    }
}
