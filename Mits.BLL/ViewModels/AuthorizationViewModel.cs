﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Mits.DAL.EntityModels;

namespace Mits.BLL.ViewModels
{
    public class AuthorizationViewModel
    {
        public SelectList Roles { get; private set; }

        public String Message;

        public AuthorizationViewModel()
        {
            Roles = new SelectList(new MITSService<Role>().GetAll(), "Id", "RoleName");
        }
    }
}
