﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.ViewModels
{
    public class AffiliateContactViewModel
    {
        public AffiliateContact AffiliateContact { get; private set; }
        public SelectList States { get; private set; }
        public SelectList Cities { get; private set; }
        public SelectList AffiliateTypes { get; private set; }
        public SelectList SecurityQuestions { get; private set; }
        public SelectList Role { get; private set; }
        public IList<ServiceType> ServiceTypes { get; private set; }
        public IList<ServiceType> AffiliateServices { get; private set; }
        public IList<AffiliateContactService> AffiliateContactServices { get; private set; }

        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }


        public AffiliateContactViewModel(AffiliateContact affiliateContact)
        {
            AffiliateContact = affiliateContact;
            AffiliateBO affiliateBO = new AffiliateBO();

            //Sorting list
            IList<State> _stateList;
            IList<State> unSortedList = new MITSService<State>().GetAll();
            IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _stateList = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _stateList = unSortedList;
            }

            State selectState = new State();
            selectState.Id = 0;
            selectState.Name = "Select";
            _stateList.Insert(0, selectState);
            States = new SelectList(_stateList, "Id", "Name", affiliateContact.StateId);

            IList<City> unSortedList1 = new List<City> { };
            if (affiliateContact.CityId != null)
            {
                int? stateId = affiliateContact.StateId;
                unSortedList1 = new MITSService<City>().GetAll(x => x.StateId == stateId);
            }
            //Sorting list
            IList<City> _cityList;
            IEnumerable<City> sortedEnum1 = unSortedList1.OrderBy(f => f.Name);
            try
            {
                _cityList = sortedEnum1.ToList();
            }
            catch (Exception ex)
            {
                _cityList = unSortedList1;
            }
            Cities = new SelectList(_cityList, "Id", "Name", affiliateContact.CityId);


            //Sorting list
            IList<SecurityQuestion> _securityQuestionList;
            IList<SecurityQuestion> unSortedList2 = new MITSService<SecurityQuestion>().GetAll();
            IEnumerable<SecurityQuestion> sortedEnum2 = unSortedList2.OrderBy(f => f.Question);
            try
            {
                _securityQuestionList = sortedEnum2.ToList();
            }
            catch (Exception ex)
            {
                _securityQuestionList = unSortedList2;
            }

            SecurityQuestion selectSecurityQuestion = new SecurityQuestion();
            selectSecurityQuestion.Id = 0;
            selectSecurityQuestion.Question = "Select";
            _securityQuestionList.Insert(0, selectSecurityQuestion);
            SecurityQuestions = new SelectList(_securityQuestionList, "Id", "Question", affiliateContact.SecurityQuestionId);

            //Sorting list
            var roleService = new MITSService<Role>();
            var affiliateRoleService = new MITSService<AffiliateRole>();
            var affiliateRole = affiliateRoleService.GetSingle(x => x.AffiliateId == AffiliateContact.Affiliate.Id);
            List<Role> _roleList = new List<Role>();
            _roleList = roleService.GetAll(x => x.AffiliateTypeId == AffiliateContact.Affiliate.AffiliateTypeId).ToList();
            //if (affiliateRole == null)
            //{
            //    _roleList = roleService.GetAll(x => x.AffiliateTypeId == AffiliateContact.Affiliate.AffiliateTypeId).ToList();
            //}
            //else
            //{
            //    _roleList.Add(affiliateRole.Role);
            //}
            _roleList.AddRange(roleService.GetAll(x => x.CreateByAffiliateId == AffiliateContact.Affiliate.Id).ToList());
            Role selectRole = new Role();
            selectRole.Id = 0;
            selectRole.RoleName = "Select";
            _roleList.Insert(0, selectRole);
            Role = new SelectList(_roleList, "Id", "RoleName", affiliateContact.RoleId);

            //Sorting list
            MITSService<ServiceType> _serviceTypeRepo = new MITSService<ServiceType>();
            IList<ServiceType> unSortedList3 = _serviceTypeRepo.GetAll();
            IEnumerable<ServiceType> sortedEnum3 = unSortedList3.OrderBy(f => f.Name);
            try
            {
                ServiceTypes = sortedEnum3.ToList();
            }
            catch (Exception ex)
            {
                ServiceTypes = unSortedList3;
            }

            //Sorting list 
            IList<AffiliateService> affSrvs = affiliateContact.Affiliate.AffiliateServices.Where(x => x.Active).ToList();
            IList<ServiceType> unSortedList5 = new List<ServiceType>();
            foreach (AffiliateService affSvc in affSrvs)
                unSortedList5.Add(affSvc.ServiceType);

            IEnumerable<ServiceType> sortedEnum5 = unSortedList5.OrderBy(f => f.Name);
            try
            {
                AffiliateServices = sortedEnum5.ToList();
            }
            catch (Exception ex)
            {
                AffiliateServices = unSortedList5;
            }

            //Sorting list 
            IList<AffiliateContactService> unSortedList4 = affiliateContact.AffiliateContactServices.Where(x => x.Active).ToList();
            IEnumerable<AffiliateContactService> sortedEnum4 = unSortedList4.OrderBy(f => f.ServiceType);

            try
            {
                AffiliateContactServices = sortedEnum4.ToList();
            }
            catch (Exception ex)
            {
                AffiliateContactServices = unSortedList4;
            }
        }
    }
}
