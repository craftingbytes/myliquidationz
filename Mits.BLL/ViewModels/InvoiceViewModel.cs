﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;

namespace Mits.BLL.ViewModels
{
    public class InvoiceViewModel
    {
        ISessionCookieValues _sessionValues = null;
        public Invoice Invoice { get; set; }
        public SelectList Affiliates { get; private set; }
        public SelectList States { get; private set; }
        public SelectList PlayYears { get; private set; }
        public String Message { get; set; }
        public List<Document> Documents { get; set; }
        public ProcessingData ProcessingData { get; private set; }

        public InvoiceViewModel(ISessionCookieValues sessionValues, Invoice invoice)
        {
            _sessionValues = sessionValues;
            Invoice = invoice;
            if (invoice.Id > 0)
                Affiliates = new SelectList(new MITSService<Affiliate>().GetAll().OrderBy(af => af.Name), "id", "Name", Invoice.AffiliateId);
            else
            {
                int affiliateTypeId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                if (affiliateTypeId == (int)Common.Constants.AffiliateType.OEM || affiliateTypeId == (int)Common.Constants.AffiliateType.Processor)
                {
                    Affiliates = new SelectList(new MITSService<Affiliate>().GetAll().Where(a => a.Id == affiliateId), "id", "Name");
                }
                else
                {
                    Affiliates = new SelectList(new MITSService<Affiliate>().GetAll().Where(a => a.AffiliateTypeId == (int)Common.Constants.AffiliateType.OEM || a.AffiliateTypeId == (int)Common.Constants.AffiliateType.Processor).OrderBy(af => af.Name), "id", "Name", Invoice.AffiliateId);
                }
            }
            States = new SelectList(new MITSService<State>().GetAll(), "id", "Name", Invoice.StateId);

            //Get processingdata entity model
            ProcessingData = new MITSService<ProcessingData>().GetSingle(i => i.InvoiceId == Invoice.Id);
           
            List<SelectListItem> planYearItems = new List<SelectListItem>();
            planYearItems.Add(new SelectListItem() { Text = "2010", Value = "2010" });
            planYearItems.Add(new SelectListItem() { Text = "2011", Value = "2011" });
            planYearItems.Add(new SelectListItem() { Text = "2012", Value = "2012" });
            planYearItems.Add(new SelectListItem() { Text = "2013", Value = "2013" });
            planYearItems.Add(new SelectListItem() { Text = "2014", Value = "2014" });
            PlayYears = new SelectList(planYearItems,"Value","Text");

            Documents = new InvoiceBO().GetDocsByPDataId(invoice.Id); 
        }
    }
}
