﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;

namespace Mits.BLL.ViewModels
{
    public class EntityStateViewModel
    {
        public SelectList Entity { get; private set; }
        public IList<State> States { get; private set; }
        public IList<AffiliateState> EntityStates { get; private set; }

        public EntityStateViewModel(Affiliate _aff)
      
        {
          
         //Get a list of all entities and bind them to selectlist
            IList<Affiliate> _affList;
            IList<Affiliate> unSortedList = new MITSService<Affiliate>().GetAll(x => x.AffiliateTypeId == 1 || x.AffiliateTypeId == 2);
            IEnumerable<Affiliate> sortedEnum = unSortedList.OrderBy(f => f.Name);
          try
          {
              _affList = sortedEnum.ToList();
          }
          catch (Exception ex)
          {
              _affList = unSortedList;
          }
          Affiliate selectEntity = new Affiliate();
          selectEntity.Id = 0;
          selectEntity.Name = "Select";
          _affList.Insert(0, selectEntity);
          Entity = new SelectList(_affList, "Id", "Name",_aff.Id);

          //Get a list of all States  

          MITSService<State> unSorted = new MITSService<State>();
          IEnumerable<State> sorted = (unSorted.GetAll()).OrderBy(f => f.Name);
          try
          {
              States = sorted.ToList();
          }
          catch (Exception ex)
          {
              States = unSorted.GetAll();
          }

          MITSService<AffiliateState> mitsAffiliate = new MITSService<AffiliateState>();
          IList<AffiliateState> sortedList1 = mitsAffiliate.GetAll(x => x.AffiliateId == _aff.Id && x.Active == true);
          IEnumerable<AffiliateState> sortedEnum4 = sortedList1.OrderBy(f => f.State.Name);
          try
          {
              EntityStates = sortedEnum4.ToList();
          }
          catch (Exception ex)
          {
              EntityStates = sortedList1;
          }
          
          

      }
    }
}
