﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.ViewModels
{
    public class ReconciliationViewModel
    {

        public IList<Affiliate> Entities { get; private set; }

        public ReconciliationViewModel()
        {

            string type = Constants.AffiliateType.OEM.ToString();
            IList<Affiliate> sortedData;
            IList<Affiliate> unsortedData = new MITSService<Affiliate>().GetAll(x => x.AffiliateType != null && x.AffiliateType.Type == type);
            IEnumerable<Affiliate> enumList = unsortedData.OrderBy(f => f.Name);
            try
            {
                sortedData = enumList.ToList();
            }
            catch (Exception ex)
            {
                sortedData = unsortedData;
            }
            //Affiliate select = new Affiliate();
            //select.Id = 0;
            //select.Name = "Select";
            //sortedData.Insert(0, select);

            Entities = sortedData;
        }
    }
}
