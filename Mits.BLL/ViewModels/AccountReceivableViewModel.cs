﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;
using System.Web.Mvc;
using Mits.BLL.BusinessObjects;

namespace Mits.BLL.ViewModels
{
    public class AccountReceivableViewModel
    {
        public Payment Payment { get; set; }
        public SelectList Affiliates { get; private set; }
        public SelectList PaymentTypes { get; private set; }
        private AccountReceivableBO accountReceivableBO;
        public String Message { get; set; }
        public AccountReceivableViewModel(Payment payment) 
        {
            accountReceivableBO=new AccountReceivableBO();
            Payment = payment;
            Affiliates = new SelectList(accountReceivableBO.GetAllAffiliate().Where(a => a.AffiliateTypeId == Mits.Common.Constants.AffiliateType.OEM.GetHashCode()), "id", "Name", Payment.AffiliateId);
            PaymentTypes = new SelectList(accountReceivableBO.GetPaymentTypes(), "id", "Name", Payment.PaymentTypeID);
        }
    }
}
