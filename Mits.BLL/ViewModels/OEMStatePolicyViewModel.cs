﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;

namespace Mits.BLL.ViewModels
{
   public class OEMStatePolicyViewModel
    {
        public SelectList OEM { get; private set; }
        public SelectList States { get; private set; }
        public IList<Policy> Policies { get; private set; }


        public OEMStatePolicyViewModel()
      {
          
//Getting All OEM from Affiliate
          IList<Affiliate> _affiliateList;
          AffiliateType _affiliate=new MITSService<AffiliateType>().GetSingle(t=>t.Type=="OEM");
          IList<Affiliate> unSortedList = new MITSService<Affiliate>().GetAll(t => t.AffiliateTypeId == _affiliate.Id);
          IEnumerable<Affiliate> sortedEnum = unSortedList.OrderBy(f => f.Name);
          try
          {
              _affiliateList = sortedEnum.ToList();
          }
          catch (Exception ex)
          {
              _affiliateList = unSortedList;
          }
          Affiliate selectOEM = new Affiliate();
          selectOEM.Id = 0;
          selectOEM.Name = "Select";
          _affiliateList.Insert(0, selectOEM);
          OEM = new SelectList(_affiliateList, "Id", "Name");



//Getting All States from State  
          IList<State> _stateList;
          IList<State> unSortedList1 = new MITSService<State>().GetAll();
          IEnumerable<State> sortedEnum1 = unSortedList1.OrderBy(f => f.Name);
          try
          {
              _stateList = sortedEnum1.ToList();
          }
          catch (Exception ex)
          {
              _stateList = unSortedList1;
          }
          State selectState = new State();
          selectState.Id = 0;
          selectState.Name = "Select";
          _stateList.Insert(0, selectState);
          States = new SelectList(_stateList, "Id", "Name");

//Getting All Policies from Policies
          MITSService<Policy> _policyRepo = new MITSService<Policy>();
          IEnumerable<Policy> sortedEnum2 = (_policyRepo.GetAll()).OrderBy(f => f.Name);
          try
          {
              Policies = sortedEnum2.ToList();
          }
          catch (Exception ex)
          {
              Policies = _policyRepo.GetAll();
          }
          
          
          

      }


    }



    }

