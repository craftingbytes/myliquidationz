﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;
using Mits.Common;

namespace Mits.BLL.ViewModels
{
    
    public class ProcessingDataViewModel
    {
        public ProcessingData ProcessingData { get; private set; }
        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }
        public ProcessingDataViewModel(ProcessingData processingData)
        {
            ProcessingData = processingData;
        }
    }
}
