﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mits.DAL.EntityModels;

namespace Mits.BLL.ViewModels
{
    public class StateGuidelinesViewModel
    {
        public State State { get; set; }
        public StateGuideline StateGuideline { get; private set; }
        public IList<StateGuideline_Link> Links { get; private set; }
        public IList<StateGuideline_Contact> Contacts { get; private set; }
        public IList<StateGuideline_DeadlineDate> DeadineDates { get; private set; }

        public StateGuidelinesViewModel(State state)
        {
            State = state;
            StateGuideline = new MITSService<StateGuideline>().GetSingle(s => s.StateId == state.Id);
            if (StateGuideline == null)
            {
                StateGuideline = new StateGuideline();
            }
            StateGuideline.GoverningBody = String.IsNullOrEmpty(StateGuideline.GoverningBody) ? StateGuideline.GoverningBody : StateGuideline.GoverningBody.Replace("\n", "<br/>");
            StateGuideline.CoveredDevices = String.IsNullOrEmpty(StateGuideline.CoveredDevices) ? StateGuideline.CoveredDevices : StateGuideline.CoveredDevices.Replace("\n", "<br/>");
            StateGuideline.OEMObligation = String.IsNullOrEmpty(StateGuideline.OEMObligation) ? StateGuideline.OEMObligation : StateGuideline.OEMObligation.Replace("\n", "<br/>");
            StateGuideline.Updates = String.IsNullOrEmpty(StateGuideline.Updates) ? StateGuideline.Updates : StateGuideline.Updates.Replace("\n", "<br/>");
            StateGuideline.Website = String.IsNullOrEmpty(StateGuideline.Website) ? StateGuideline.Website : StateGuideline.Website.Replace("\n", "<br/>");
            StateGuideline.RegulationChange = String.IsNullOrEmpty(StateGuideline.RegulationChange) ? StateGuideline.RegulationChange : StateGuideline.RegulationChange.Replace("\n", "<br/>");
            StateGuideline.RegulationType = String.IsNullOrEmpty(StateGuideline.RegulationType) ? StateGuideline.RegulationType : StateGuideline.RegulationType.Replace("\n", "<br/>");
            if (!string.IsNullOrEmpty(StateGuideline.Website) && !(StateGuideline.Website.ToLower().StartsWith("http://") || StateGuideline.Website.ToLower().StartsWith("https://")))
            {
                StateGuideline.Website = "http://" + StateGuideline.Website;
            }
            Links = new MITSService<StateGuideline_Link>().GetAll(s => s.StateId == state.Id);
            Contacts = new MITSService<StateGuideline_Contact>().GetAll(s => s.StateId == state.Id);
            DeadineDates = new MITSService<StateGuideline_DeadlineDate>().GetAll(s => s.StateId == state.Id);
        }

    }
}
