﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.ViewModels
{
    public class AffiliateViewModel
    {
      ISessionCookieValues _sessionValues = null;
      public Affiliate Affiliate { get; private set; }
      public SelectList States { get; private set; }
      public SelectList Cities { get; private set; }
      public SelectList AffiliateTypes { get; private set; }
      public SelectList AffiliateRoles { get; private set; }
      public SelectList PaymentTerms { get; private set; }
      public IList<ServiceType> ServiceTypes { get; private set; }
      public IList<AffiliateService> AffiliateServices { get; private set; }
      public List<Document> Documents { get; set; }
      private List<Message> messages = new List<Message>();
      public List<Message> Messages
      {
          get { return messages; }
          set { messages = value; }
      }

      public AffiliateViewModel(ISessionCookieValues sessionValues, Affiliate affiliate)
      {
          _sessionValues = sessionValues;
          Affiliate = affiliate;
          AffiliateBO affiliateBO = new AffiliateBO();
    
          //Sorting list
           IList<State> _stateList;
          IList<State> unSortedList = new MITSService<State>().GetAll();
          IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
          try
          {
              _stateList = sortedEnum.ToList();
          }
          catch (Exception ex)
          {
              _stateList = unSortedList;
          }
        
         
          State selectState = new State();
          selectState.Id = 0;
          selectState.Name = "Select";
          _stateList.Insert(0, selectState);

          States = new SelectList(_stateList, "Id", "Name", affiliate.StateId);

          IList<City> unSortedList1 = new List<City> { };
          //if (affiliate.CityId != null)
          //{
              int? stateId = affiliate.StateId;
              unSortedList1 = new MITSService<City>().GetAll(x => x.StateId == stateId);
          //}
          //Sorting list
          IList<City> _cityList;
          IEnumerable<City> sortedEnum1 = unSortedList1.OrderBy(f => f.Name);
          try
          {
              _cityList = sortedEnum1.ToList();
          }
          catch (Exception ex)
          {
              _cityList = unSortedList1;
          }
          City _city = new City();
          _city.Id = 0;
          _city.Name = "Select";
          _cityList.Insert(0, _city);

          Cities = new SelectList(_cityList, "Id", "Name", affiliate.CityId);

          //Sorting list
          IList<AffiliateType> _affiliateTypeList;
          IList<AffiliateType> unSortedList2 = new MITSService<AffiliateType>().GetAll();
          IEnumerable<AffiliateType> sortedEnum2 = unSortedList2.OrderBy(f => f.Type);
          try
          {
              _affiliateTypeList = sortedEnum2.ToList();
          }
          catch (Exception ex)
          {
              _affiliateTypeList = unSortedList2;
          }
        

          AffiliateType selectAffiliateType = new AffiliateType();
          selectAffiliateType.Id = 0;
          selectAffiliateType.Type = "Select";
          _affiliateTypeList.Insert(0, selectAffiliateType);

          AffiliateTypes = new SelectList(_affiliateTypeList, "Id", "Type", affiliate.AffiliateTypeId);

          IList<PaymentTerm> _pamentTermList = new MITSService<PaymentTerm>().GetAll().OrderBy(x => x.Id).ToList();
          PaymentTerm selectPaymentTerm = new PaymentTerm();
          selectPaymentTerm.Id = 0;
          selectPaymentTerm.Name = "Select";
          _pamentTermList.Insert(0, selectPaymentTerm);

          PaymentTerms = new SelectList(_pamentTermList, "Id", "Name", affiliate.PaymentTermId != null ? affiliate.PaymentTermId : 0);

          //Sorting list
          MITSService<ServiceType> _serviceTypeRepo = new MITSService<ServiceType>();
          IList<ServiceType> sortedList = _serviceTypeRepo.GetAll();
          IEnumerable<ServiceType> sortedEnum3 = sortedList.OrderBy(f => f.Name);
          try
          {
              ServiceTypes = sortedEnum3.ToList();
          }
          catch (Exception ex)
          {
              ServiceTypes = sortedList;
          }
         

          //Sorting list
          MITSService<AffiliateService> _affiliateServiceSvc = new MITSService<AffiliateService>();
          IList<AffiliateService> sortedList1 = _affiliateServiceSvc.GetAll(x => x.AffiliateId == affiliate.Id && x.Active == true);
          IEnumerable<AffiliateService> sortedEnum4 = sortedList1.OrderBy(f => f.ServiceType);
          try
          {
              AffiliateServices = sortedEnum4.ToList();
          }
          catch (Exception ex)
          {
              AffiliateServices = sortedList1;
          }
          Documents = affiliateBO.GetDocsByPDataId(affiliate.Id);

          var roleService = new MITSService<Role>();
          List<Role> roles = roleService.GetAll(x => x.AffiliateTypeId == affiliate.AffiliateTypeId).ToList();
            int linqAffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
            roles.AddRange(roleService.GetAll(x => x.CreateByAffiliateId == linqAffiliateId).ToList());
          roles.Insert(0, new Role() { Id = 0, RoleName = "Select" });
          var affiliateRoleService = new MITSService<AffiliateRole>();
          AffiliateRole affiliateRole = affiliateRoleService.GetSingle(x => x.AffiliateId == affiliate.Id);
          if (affiliateRole == null)
          {
              AffiliateRoles = new SelectList(roles, "Id", "RoleName");
          }
          else
          {
              AffiliateRoles = new SelectList(roles, "Id", "RoleName", affiliateRole.RoleId);
          }
      }

    }

}