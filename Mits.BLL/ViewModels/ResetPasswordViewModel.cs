﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.ViewModels
{
    public class ResetPasswordViewModel
    {
        public SelectList SecurityQuestion { get; private set; }
        public AffiliateContact affiliateContact { get; private set; }
        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }
        public ResetPasswordViewModel(AffiliateContact _affiliateContact)
        {
             affiliateContact = _affiliateContact;
            //Sorting list
            IList<SecurityQuestion> _securityQuestionList;
            IList<SecurityQuestion> unSortedList2 = new MITSService<SecurityQuestion>().GetAll();
            IEnumerable<SecurityQuestion> sortedEnum2 = unSortedList2.OrderBy(f => f.Question);
            try
            {
                _securityQuestionList = sortedEnum2.ToList();
            }
            catch (Exception ex)
            {
                _securityQuestionList = unSortedList2;
            }


            SecurityQuestion selectSecurityQuestion = new SecurityQuestion();
            selectSecurityQuestion.Id = 0;
            selectSecurityQuestion.Question = "Select";
            _securityQuestionList.Insert(0, selectSecurityQuestion);
            SecurityQuestion = new SelectList(_securityQuestionList, "Id", "Question",affiliateContact.SecurityQuestionId);
        }
    }
}
