﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;

namespace Mits.BLL.ViewModels
{
    public class AccountPayableViewModel
    {
        public Payment Payment { get; set; }
        public SelectList Affiliates { get; private set; }
        public SelectList PaymentTypes { get; private set; }
        private AccountPayableBO accountPayableBO;
        public String Message { get; set; }
        public AccountPayableViewModel(Payment payment)
        {
            accountPayableBO = new AccountPayableBO();
            Payment = payment;
            Affiliates = new SelectList(accountPayableBO.GetAllAffiliate().Where(a => a.AffiliateTypeId == Mits.Common.Constants.AffiliateType.Processor.GetHashCode()), "id", "Name", Payment.AffiliateId);
            PaymentTypes = new SelectList(accountPayableBO.GetPaymentTypes(), "id", "Name", Payment.PaymentTypeID);
        }
    }
}
