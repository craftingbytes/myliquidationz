﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;

namespace Mits.BLL.ViewModels
{
    public class AffiliateAccreditationViewModel
    {

        public SelectList Affiliates{ get; private set; }
        public IList<AccreditationType> Accreditations { get; private set; }

        public AffiliateAccreditationViewModel()
      {
          
          
         //Getting a list of affiliates and binding them to selectlist of affiliates

          IList<Affiliate> _affiliateList;
          IList<Affiliate> unSortedList = new MITSService<Affiliate>().GetAll();
          IEnumerable<Affiliate> sortedEnum = unSortedList.OrderBy(f => f.Name);
          try
          {
              _affiliateList = sortedEnum.ToList();
          }
          catch (Exception ex)
          {
              _affiliateList = unSortedList;
          }

          Affiliate selectAffiliate = new Affiliate();
          selectAffiliate.Id = 0;
          selectAffiliate.Name = "Select";
          _affiliateList.Insert(0, selectAffiliate);
          Affiliates = new SelectList(_affiliateList, "Id", "Name");


          //Getting a list of accreditations

          MITSService<AccreditationType> unSortedList1 = new MITSService<AccreditationType>();
          IEnumerable<AccreditationType> sortedEnum1 = (unSortedList1.GetAll()).OrderBy(f => f.Name);
          try
          {
              Accreditations = sortedEnum1.ToList();
          }
          catch (Exception ex)
          {
              Accreditations = unSortedList1.GetAll(); 
          }
         
          
          

      }

    }
}
