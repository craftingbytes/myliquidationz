﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.BLL.BusinessObjects;

namespace Mits.BLL.ViewModels
{
    public class ServiceTypeViewModel
    {
        public SelectList ServiceTypes{ get; private set; }
        public IList<ServiceTypeGroup> Groups { get; private set; }

        public ServiceTypeViewModel()
      {

        //Getting a list of all service type from ServiceType and binding it with the selectlist
          IList<ServiceType> _serviceList ;
          IList<ServiceType> unSortedList = new MITSService<ServiceType>().GetAll();
          IEnumerable<ServiceType> sortedEnum = unSortedList.OrderBy(f => f.Name);
          try
          {
              _serviceList = sortedEnum.ToList();
          }
          catch (Exception ex)
          {
              _serviceList = unSortedList;
          }

          ServiceType selectService = new ServiceType();
          selectService.Id = 0;
          selectService.Name = "Select";
          _serviceList.Insert(0, selectService);
          ServiceTypes = new SelectList(_serviceList, "Id", "Name");

          //Getting a list of all service type groups from ServiceTypeGroup

          MITSService<ServiceTypeGroup> _serviceTyepGroupRepo = new MITSService<ServiceTypeGroup>();
          IEnumerable<ServiceTypeGroup> sortedEnum1 = (_serviceTyepGroupRepo.GetAll()).OrderBy(f => f.Name);
          try
          {
              Groups = sortedEnum1.ToList();
          }
          catch (Exception ex)
          {
              Groups = _serviceTyepGroupRepo.GetAll();
          }

      }




    }
}
