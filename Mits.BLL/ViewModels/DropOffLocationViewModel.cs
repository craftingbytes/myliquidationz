﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Mits.BLL.Interfaces;
using System.Web.Mvc;
using Mits.DAL.EntityModels;
using Mits.Common;

namespace Mits.BLL.ViewModels
{
    public class DropOffLocationViewModel
    {
        public DropOffLocation DropOffLoc { get; private set; }
        public SelectList States { get; private set; }
        public SelectList Cities { get; private set; }
        public SelectList Levels { get; private set; }
        public IList<LocationType> LocationTypes { get; private set; }
        public SelectList SelectedLevels { get; private set; }
        public IList<DropOffDevice> DropOffDevices { get; private set; }
        public IList<DropOffLocationDevice> DropOfflocationDevices { get; private set; }
        public IList<DropOffLocationType> DropOffLocationTypes { get; private set; }
        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }

        public DropOffLocationViewModel(DropOffLocation dropoffloc)
        {
            DropOffLoc = dropoffloc;

            //Sorting list
            IList<State> _stateList;
            IList<State> unSortedList = new MITSService<State>().GetAll();
            IEnumerable<State> sortedEnum = unSortedList.OrderBy(f => f.Name);
            try
            {
                _stateList = sortedEnum.ToList();
            }
            catch (Exception ex)
            {
                _stateList = unSortedList;
            }


            State selectState = new State();
            selectState.Id = 0;
            selectState.Name = "Select";
            _stateList.Insert(0, selectState);

            States = new SelectList(_stateList, "Id", "Name", dropoffloc.StateId);

            IList<City> unSortedList1 = new List<City> { };

            int? stateId = dropoffloc.StateId;
            unSortedList1 = new MITSService<City>().GetAll(x => x.StateId == stateId);

            IList<City> _cityList;
            IEnumerable<City> sortedEnum1 = unSortedList1.OrderBy(f => f.Name);
            try
            {
                _cityList = sortedEnum1.ToList();
            }
            catch (Exception ex)
            {
                _cityList = unSortedList1;
            }
            City _city = new City();
            _city.Id = 0;
            _city.Name = "Select";
            _cityList.Insert(0, _city);

            Cities = new SelectList(_cityList, "Id", "Name", dropoffloc.CityId);

            MITSService<DropOffLocationType> dropOffLocationType = new MITSService<DropOffLocationType>();
            DropOffLocationTypes = dropOffLocationType.GetAll(x => x.DropOffLocationId == DropOffLoc.Id);

            IList<LocationType> selectedTypes = new List<LocationType>();
            foreach (DropOffLocationType t in DropOffLocationTypes)
            {
                selectedTypes.Add(t.LocationType);
            }
            

            IList<LocationType> unSortedtypeList = new List<LocationType> { };
            unSortedtypeList = new MITSService<LocationType>().GetAll();
            LocationTypes = unSortedtypeList;
            IList<LocationType> typeList;
            IEnumerable<LocationType> sortedTypeEnum = unSortedtypeList.OrderBy(l => l.Id);
            try
            {
                typeList = sortedTypeEnum.ToList();
            }
            catch
            {
                typeList = unSortedtypeList;
            }
            LocationType type = new LocationType();
            type.Id = 0; type.LevelName = "Select";
            typeList.Insert(0, type);
            Levels = new SelectList(typeList, "Id", "LevelName", selectedTypes);

            //Sorting list
            DropOffDevices = new MITSService<DropOffDevice>().GetAll();

            MITSService<DropOffLocationDevice> dropOffLocationDevice = new MITSService<DropOffLocationDevice>();
            DropOfflocationDevices = dropOffLocationDevice.GetAll(x => x.DropOffLocationId == DropOffLoc.Id);

            
           

        }

    }

}