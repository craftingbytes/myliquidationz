﻿var lastSel;
var inEditMode = false;
var rootPath = window.location.protocol + "//" + window.location.host;

$(function () {

    InitGrid();

    $("#btnAdd").button().click(function () {
        //jQuery("#jqGrid").jqGrid('editGridRow',"new",{height:280,reloadAfterSubmit:false});

        addRow();
    });

    $("#btnSubmit").button().click(function () {

        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;


        var isFocused = false;
        isFocused = false;

        if (!isValid) {
            return false;
        }


        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');
        //if (!isAllSaved()) {
        //    return false;
        //}
        if (inEditMode) {
            alert('There is an item opened in edit mode. Please save or cancel it first.');
            return false;
        }
        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    $("form").attr("action", rootPath + "/BulkShipment/Edit");
                    //compileGridData();
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });

    $('input[id$=txtReceivedDate]').datepicker({});
    //$('input[id$=txtReceivedDate]').datepicker('setDate', new Date());
    $('input[id$=txtShippededDate]').datepicker({});

});

function InitGrid() {
    $("#jqGrid").jqGrid({
        //data: mydata,
        datatype: 'json',
        mtype: 'GET',
        url: rootPath + '/BulkShipment/GetBulkShipmentItems?BulkShipmentID=' + currentBulkShipmentID,
        height: 150,

        colNames: [
                    'BulkShipmentItemID',
                    'Store #', 'ItemId',
                    'Description',
                    'Condition',
                    'Comment',
                    'Qty',
                    'CED',
                    'Disposition',
                    'ItemsBulkShipmentId',
                    'Pull',
                    'Actions'
                    ],
        colModel: [
                    { name: 'BulkShipmentItemID', index: 'BulkShipmentItemID', editable: true, width: 85, hidden: true },
   		            { name: 'StoreNumber', index: 'StoreNumber', editable: true, width: 40, sortable: false, editrules: { integer: true, required: true }, align: 'right'
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { parseStoreNumber() }} ] }
                    },
                    { name: 'ItemId', index: 'ItemId', editable: true, width: 200, sortable: false, editrules: { required: true}  
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { getDescription() }} ] }
                    },
   		            { name: 'Description', index: 'Description', editable: true, width: 200, sortable: false, editrules: { required: true} },
                    { name: 'Condition', index: 'Condition', editable: true, width: 100, sortable: false, editrules: { required: true} 
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { getCondition() }} ] }
                    },
                    { name: 'ReceivedCondition2', index: 'ReceivedCondition2', editable: true, width: 200, sortable: false, editrules: { required: false }, editoptions: { dataEvents: [{ type: 'change', fn: function (e) { getSaleCondition(e) } }]} },
                    { name: 'Qty', index: 'Qty', editable: true, width: 50, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'CoveredDevice', index: 'CoveredDevice', editable:true, width: 30, edittype:'checkbox', formatter: "checkbox"
                            , editoptions: { value: "true:false" }, hidden: true
                    },
                    { name: 'Disposition', index: 'Disposition', editable: true, width: 100, sortable: false, editrules: { required: true} },
                    { name: 'ItemsBulkShipmentId', index: 'ItemsBulkShipmentId', editable: true, width: 20, hidden: true },
                    { name: 'Pulled', index: 'Pulled', editable: true, width: 50, sortable: false, editrules: { integer: true, required: true }, editoptions: { defaultValue:"0"} },
                    {name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],
        rowNum: 25,
        rowList: [25, 50, 100, 200],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        //editurl: rootPath + '/Shipment/AddProductItem',
        editurl: rootPath + '/BulkShipment/EditBulkShipmentItem',
        width: 925,
        height: 300,
        afterInsertRow: function (rowid, aData) {
            //be = "<input id='" + rowid + "_edRow' style='height:22px;padding:0' type='button' value='Edit' onclick=\"editRow('" + rowid + "');\" title='Edit Record' />";
            //se = "<input id='" + rowid + "_savRow' style='height:22px;padding:0' type='button' value='Save' onclick=\"saveRow('" + rowid + "');\" title='Save Record' />";
            //ce = "<input id='" + rowid + "_canRow' style='height:22px;padding:0' type='button' value='Cancel' onclick=\"restoreRow('" + rowid + "');\" title='Cancel Edit' />";
            //$("#jqGrid").jqGrid('setRowData', rowid, { Action: be + se + ce });
            //$("input[type=button]").button();
        },

        loadComplete: function () {
            //$('#Description').change(popacct); 
        }
    });

}


function addRow()
{
    jQuery("#jqGrid").jqGrid('editGridRow', 'new', {
        height: 280,
        recreateForm: true,
        reloadAfterSubmit: false,
        addCaption: "Add Record",
        editCaption: "Edit Record",
        bSubmit: "Add",
        bCancel: "Done",
        bClose: "Close",
        saveData: "Data has been changed! Save changes?",
        bYes: "Yes",
        bNo: "No",
        bExit: "Done",
        modal: true,
        addedrow: 'last',
        closeAfterEdit: false,
        url: rootPath + '/BulkShipment/EditBulkShipmentItem',
        afterSubmit: function (response, postdata) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            return [result.success, result.message, result.bulkshipmentId]
        },
        afterShowForm: function (formid) {
            //setPalletNumber();
        },
        afterComplete: function (response, postdata, formid) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            $("#jqGrid").jqGrid("setCell", result.bulkshipmentId, 'Disposition', result.disposition);
            //setStoreNumber();
            return true;
        },
        viewPagerButtons: false //,

    });
}

function parseStoreNumber() {
    storeNumber = $('#StoreNumber').val();
    if (storeNumber.length > 4)
        storeNumber = storeNumber.substring(0,4);
    storeNumber = storeNumber.replace(/^[0]+/g,"");
    $('#StoreNumber').val(storeNumber);
    $('#ItemsBulkShipmentId').val($('#BulkShipmentId').val());
    return [true,'']; 
}

function parseBarCode() {
    storeNumber = $('#StoreNumber').val();
    var chunks = storeNumber.split(' ');
    alert(chunks.length.toString());
    if (chunks.length > 1)
    {
        $('#StoreNumber').val(chunks[0]);
        $('#ItemId').val(chunks[1]);
    }
    getDescription();
    return [true,'']; 
}

function getDescription() {
    //var itemId = $('#ItemId').val();
    var itemId = $('#ItemId').val();
    if (itemId.length > 6 || isNaN(itemId)) {
        itemId = "Misc";
        $('#ItemId').val("Misc");
    }
   
    $.ajax({
        url: "/Shipment/GetProductDescription",
        dataType: 'json',
        type: "GET",
        data: ({ itemId: itemId }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.description == "")
            {
                $('#Description').val("");
                $('#Description').focus();    
            }
            else
            {
                $('#Description').val(result.description);
                $('#Disposition').val(result.disposition);
                if (result.coveredDevice)
                    $("#CoveredDevice").attr('checked', true);
                else
                    $("#CoveredDevice").attr('checked', false);
                //$('#CoveredDevice').val(result.coveredDevice);
                $('#Condition').focus(); 
            }
        }
    });

    return [true,'']; 
}

function EditRow(rowId) {
    if (rowId && rowId != lastSel) {
        //CancelRow(lastSel);
        lastSel = rowId;
    }
    //$('#ED_ActionMenu_' + rowId).css({ display: "none" });
    //$('#SC_ActionMenu_' + rowId).css({ display: "block" });
    //$('#jqGrid').editRow(rowId);
    //jQuery("#jqGrid").editGridRow("rowId",);
    jQuery("#jqGrid").jqGrid('editGridRow', rowId, {
                height: 280,
                recreateForm: true,
                reloadAfterSubmit: false, 
                addCaption: "Add Record",
		        editCaption: "Edit Record",
		        bSubmit: "Save",
		        bCancel: "Cancel",
		        bClose: "Close",
		        saveData: "Data has been changed! Save changes?",
		        bYes : "Yes",
		        bNo : "No",
		        bExit: "Cancel",
                modal: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                afterSubmit: function (response, postdata) {
                    var json = response.responseText;
                    var result = eval("(" + json + ")");
                    return [result.success, result.message]
                },
                onClose: function (response, postdata) { 
                                CancelRow(rowId);
                            },
                afterComplete: function (response, postdata, formid) {
                    var json = response.responseText;
                    var result = eval("(" + json + ")");
                    $("#jqGrid").jqGrid("setCell", rowId, 'Disposition', result.disposition);
                    SaveRow(rowId);
                    //addRow();
                }

               });
    inEditMode = true;
}

function SaveRow(rowId) {
    //$('#jqGrid').saveRow(rowId, AfterSaveRow);

    if ($("#jqGrid").saveRow(rowId, false, 'clientArray')) {
        var newData = $("#jqGrid").getRowData(rowId);
        if (parseInt(newData["Qty"]).toString() == 'NaN') {
            return;
        }
        else {
            inEditMode = false;
            //$('#ED_ActionMenu_' + lastSel).css({ display: "block" });
            //$('#SC_ActionMenu_' + lastSel).css({ display: "none" });
        }
    }
}

function CancelRow(rowId) {
    //$('#ED_ActionMenu_' + rowId).css({ display: "block" });
    //$('#SC_ActionMenu_' + rowId).css({ display: "none" });
    //$('#jqGrid').restoreRow(rowId);

    //$.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.ItemId == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
        lastSel = lastSel - 1;
    }
    inEditMode = false;
}

function DeleteRow(rowId) {
    //var rowData = $("#jqGrid").getRowData(rowId);
    $.ajax({
        url: "/BulkShipment/DeleteBulkShipmentItem",
        dataType: 'json',
        type: "POST",
        data: ({ bulkShipmentItemId: rowId}),
        async: false,
        cache: false,
        success: function (result) {
            jQuery("#jqGrid").jqGrid('delRowData', rowId);
        }
    });
    //jQuery("#jqGrid").jqGrid('delRowData', rowId);
    //$('#jqGrid').delGridRow(rowId, { delData: { BulkShipmentItemID: $("#jqGrid").getRowData(rowId).Id} });
}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "";
    actionMenuHtml += "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    //actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}

function compileGridData() {
    //get total rows in you grid
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    var lineItems = '';
    var rows = jQuery("#jqGrid").jqGrid('getRowData');
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        if (row.BulkShipmentItemID == "")
            lineItems =  'BulkShipmentItemID:0';
        else
            lineItems =  'BulkShipmentItemID:' + row.BulkShipmentItemID;
        lineItems += ',StoreNumber:' + row.StoreNumber;
        lineItems += ',ItemId:' + row.ItemId;
        lineItems += ',Description:' + row.Description;
        lineItems += ',Qty:' + row.Qty;
        lineItems += ',Condition:' + row.Condition;
        lineItems += ',ReceivedCondition2:' + row.ReceivedCondition2;
        lineItems += ',CoveredDevice:' + row.CoveredDevice;
        lineItems += ',Disposition:' + row.Disposition;
        $('<input type="hidden" />').attr('name', 'Item_' + (i + 1) ).attr('value', lineItems).appendTo('#hiddenDivId');
    }
}

function setStoreNumber() {
    var rowCount = $("#jqGrid").getGridParam('reccount');
    if (rowCount == 0)
    {
        //$('#PalletUnitNumber').val(1);
    }
    else 
    {
        var rowData = $("#jqGrid").getRowData(rowCount);
        var storeNumber = rowData.StoreNumber;
        $('#StoreNumber').val(storeNumber);
    }
}
         
function getCondition() {
    var condition = $('#Condition').val();
    if (condition == "Like New" ||
        condition == "Used" ||
        condition == "Repairable" ||
        condition == "Scratch and Dent" ||
        condition == "Damaged(parts)" ||
        condition == "Destroyed" ||
        condition == "Opened/Damaged box" ||
        condition == "Untested" ||
        condition == "Incomplete" ||
        condition == "Display Unit" ||
        condition == "Pulled from floor" ||
        condition == "Other")
    {
        //do nothing
    } 
    else if (condition == "1")
         $('#Condition').val("Like New");
    else if (condition == "2")
         $('#Condition').val("Used");
    else if (condition == "3")
         $('#Condition').val("Repairable");
    else if (condition == "4")
         $('#Condition').val("Scratch and Dent");
    else if (condition == "5")
         $('#Condition').val("Damaged(parts)");
    else if (condition == "6")
         $('#Condition').val("Destroyed");
    else if (condition == "7")
         $('#Condition').val("Opened/Damaged box");
    else if (condition == "8")
         $('#Condition').val("Untested");
    else if (condition == "9")
         $('#Condition').val("Incomplete");
    else if (condition == "10")
         $('#Condition').val("Display Unit");
    else if (condition == "11")
         $('#Condition').val("Pulled from floor");
    else if (condition == "99")
         $('#Condition').val("Other");
    else
        $('#Condition').val("Other");
}

function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            if (elem.name == "ddlPickupState")
                $("select#ddlPickupCity").clearSelect();
            else
                $("select#ddlShipToCity").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        if (elem.name == "ddlPickupState")
                            $("select#ddlPickupCity").fillSelect(result.data);
                        else
                            $("select#ddlShipToCity").fillSelect(result.data);

                    }
                }
            });
        }
    }
}

function getSaleCondition(e) {
    condition = $(e.target).val();
    if (condition == "1")
        $(e.target).val("Tested OK.");
    else if (condition == "2")
        $(e.target).val("Out of box.");
    else if (condition == "3")
        $(e.target).val("In box.");
    else if (condition == "4")
        $(e.target).val("Parts Only.");

}