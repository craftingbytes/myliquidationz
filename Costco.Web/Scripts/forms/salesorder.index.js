﻿var affiliateId;
var affiliateType = $("#hfAffiliateType").val();
var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    $("button, input:submit, input:button").button();
    affiliateId = $("#hdAffiliateId").val();
    $("#txtBeginDate").datepicker();
    //$('#txtBeginDate').datepicker('setDate', new Date());
    $("#txtEngingDate").datepicker();
    //$('#txtEngingDate').datepicker('setDate', new Date());

    jQuery("#jqGrid").jqGrid({
        url: "/SalesOrder/GetSalesOrderList",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id', 'Buyer', 'ASR', 'Created', 'Sold Date', 'Ship Date', 'Available', 'Actions'],
        colModel: [
                    { name: 'SalesOrderId', index: 'SalesOrderId', width: 40, align: 'Left', sortable: false },
                    { name: 'SoldToName', index: 'SoldToName', width: 120, editable: false, sortable: false, align: 'Left', hidden: false, formatter: buyerFmatter },
                    { name: 'SoldBy', index: 'SoldBy', width: 120, align: 'Left', sortable: false, hidden: true },
   		            { name: 'Created', index: 'Created', width: 80, align: 'center', sortable: false},
   		            { name: 'SoldDate', index: 'SoldDate', width: 80, editable: false, sortable: false, align: 'center' },
                    { name: 'ShipDate', index: 'ShipDate', width: 80, align: 'Left', sortable: false, formatter: shippingFmatter },
                    { name: 'Available', index: 'Available', width: 40, align: 'Left', hidden:true, sortable: false },
                    { name: 'Actions', index: 'Actions', width: 70, editable: false, sortable: false, align: 'center', formatter: actionFmatter }
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240



    });

    $("#jqGrid").jqGrid('navGrid', '#pager2',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    //if (affiliateType != 1 && affiliateType != 5 && affiliateType !=2) {
    //    jQuery("#jqGrid").jqGrid('hideCol', ["Actions"]);
    //}

    $("#btnSearch").click(function () {
        Search();
    });

    if (affiliateType == 5) //recycler
        $("#btnAdd").show();
    else
        $("#btnAdd").hide();

    $('input[id$=txtShipDate]').datepicker({});
    $('input[id$=txtShipDate]').datepicker('setDate', new Date());

    $('#ddlBuyerList').change(function () {
        FillAddressData();
    });

});

function Search() {
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEngingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);

    var inComplete = $('#cbIncomplete').attr('checked'); 

    var beginId = $("#txtBeginId").val();
    var endId = $("#txtEndId").val();
    jQuery("#jqGrid").setGridParam({ postData: { BeginDate: strBeginingDate, EndDate: strEndingDate, BeginId: beginId, EndId: endId, Incomplete: inComplete }, page: 1 }).trigger("reloadGrid");

}

$('input[id$=btnAdd]').click(function () {
    $("#dialog-choice").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                $("form").attr("action", rootPath + "/SalesOrder/Create");
                //compileGridData();
                $("form").submit();
                //$(this).dialog("close");
            }
        }
    });
});


function actionFmatter(cellvalue, options, rowObject) {
    var salesOrderId = rowObject[0];
    var available = rowObject[6];
    if (available == undefined)
        return cellvalue;
    var cell = "<div>";
    var url = "return showReport('../reports/SalesOrderItemReport.aspx?SalesOrderId=" + salesOrderId + "', 'SalesOrderItemReport');";
    cell += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Sales Order Report'  title='Sales Order Report'  style='border:none;'/></a>";
    if (available != true) {
        cell += "&nbsp;&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:editSalesOrder(" + salesOrderId + ")' href='#' ><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit'  style='border:none;'/></a>";
    }
    cell += "</div>";
    return cell;
}

function editSalesOrder(salesOrderId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + "/SalesOrder/Edit?Id=" + salesOrderId;
    window.location.href = url;
}

function shippingFmatter(cellvalue, options, rowObject) {
    if (cellvalue == "" || cellvalue == "Not Shipped") {
        var url = "javascript:openShipDialog(" + options.rowId + ")";
        return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#' onclick='" + url + "' >Not Shipped</a>";
    } else if (cellvalue != "Not Shipped") {
        var url = "javascript:openRevertDialog(" + options.rowId + ")";
        return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#' onclick='" + url + "' >" + cellvalue + "</a>";
    }
    else
        return cellvalue;

}
function openShipDialog(rowId) {
    var rowData = $("#jqGrid").getRowData(rowId);
    var salesOrderId = rowData["SalesOrderId"];
    var available = rowData["Available"];
    $("#dialog-ship").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                var shipdate = $("#txtShipDate").val();
                $.ajax({
                    url: "/SalesOrder/UpdateShipDate",
                    dataType: 'json',
                    type: "POST",
                    data: ({ salesOrderId: salesOrderId, shipDate: $("#txtShipDate").val() }),
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result.success == true) {
                            rowData["ShipDate"] = result.shipDate;
                            rowData["Available"] = available;
                            $("#jqGrid").jqGrid('setRowData', rowId, rowData);
                        }
                        else {
                            alert(result.message);
                        }
                        //jQuery("#jqGrid").jqGrid('delRowData', rowId);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An error Occurred");
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}
function openRevertDialog(rowId) {
    var rowData = $("#jqGrid").getRowData(rowId);
    var salesOrderId = rowData["SalesOrderId"];
    var available = rowData["Available"];
    $("#dialog-revert").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                $.ajax({
                    url: "/SalesOrder/RevertShipDate",
                    dataType: 'json',
                    type: "POST",
                    data: ({ salesOrderId: salesOrderId }),
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result.success == true) {
                            rowData["ShipDate"] = "Not Shipped";
                            $("#jqGrid").jqGrid('setRowData', rowId, rowData);
                        }
                        else {
                            alert(result.message);
                        }
                        //jQuery("#jqGrid").jqGrid('delRowData', rowId);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An error Occurred");
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}
function buyerFmatter(cellvalue, options, rowObject) {
    if (affiliateId == 533 && (cellvalue == "" || cellvalue == null)) {
        var url = "javascript:openBuyerDialog(" + options.rowId + ")";
        return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#' onclick='" + url + "' >SetBuyer</a>";
    } else if (cellvalue == null)
        return "";
    else
        return cellvalue;

}
function openBuyerDialog(rowId) {
    var rowData = $("#jqGrid").getRowData(rowId);
    $('#ddlBuyerList').val(0);
    FillAddressData();
    $("#dialog-buyer").dialog({
        modal: true,
        width: 500,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                if ($("#ddlBuyerList").val() == 0) {
                    alert("Buyer is required.!");
                    return;
                }

                if ($.trim($("#SoldToName").val()) == "") {
                    alert("Buyer name is required!");
                    return;
                }

                if ($.trim($("#SoldToAddress").val()) == "") {
                    alert("Address is required!");
                    return;
                }

                if ($("#ddlBuyerState").val() == 0) {
                    alert("State is required.!");
                    return;
                }

                if ($("#ddlBuyerCity").val() == 0) {
                    alert("City is required.!");
                    return;
                }

                if ($.trim($("#SoldToZip ").val()) == "") {
                    alert("Zip is required!");
                    return;
                }


                $.ajax({
                    url: "/SalesOrder/SetBuyer",
                    dataType: 'json',
                    type: "POST",
                    data: ({
                            salesOrderId: rowId,
                            SoldToAffiliateId: $("#ddlBuyerList").val(),
                            SoldToName: $("#SoldToName ").val(),
                            SoldToAddress: $("#SoldToAddress ").val(),
                            SoldToState: $("#ddlBuyerState").val(),
                            SoldToCity: $("#ddlBuyerCity").val(),
                            SoldToZip: $("#SoldToZip ").val()
                    }),
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result.success == true) {
                            rowData["SoldToName"] = result.buyerName;
                            $("#jqGrid").jqGrid('setRowData', rowId, rowData);
                        }
                        else {
                            alert(result.message);
                        }
                        //jQuery("#jqGrid").jqGrid('delRowData', rowId);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An error Occurred");
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}

function FillAddressData() {
    var soldToId = $('#ddlBuyerList').val();
    $('#SoldToAffiliateId').val(soldToId);
    $("#SoldToName").val($("#ddlBuyerList").find("option:selected").text());
    if (soldToId == 0) {
        $("#SoldToName").val("");
        $("#SoldToName").prop("readonly", true);
        $('#SoldToAddress').val("");
        $("#SoldToAddress").prop("readonly", true);
        if ($('#ddlBuyerState').get(0).options[0].selected != true) {
            $('#ddlBuyerState').get(0).options[0].selected = true;
            $("#ddlBuyerState").trigger("change");
        }
        $('#ddlBuyerCity').val(0);
        $('#SoldToZip').val("");

    }
    else if (soldToId == 999999) {
        $("#SoldToName").val("");
        $("#SoldToName").prop("readonly", false);
        $('#SoldToAddress').val("");
        $("#SoldToAddress").prop("readonly", false);
        if ($('#ddlBuyerState').get(0).options[0].selected != true) {
            $('#ddlBuyerState').get(0).options[0].selected = true;
            $("#ddlBuyerState").trigger("change");
        }
        $('#ddlBuyerCity').val(0);
        $('#SoldToZip').val("");

    } else {
        $("#SoldToName").prop("readonly", true);
        //$('#SoldToAffiliateId').val(soldToId);
        $.ajax({
            url: '/Shipment/GetShipToAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: soldToId },
            async: false,
            success: function (result) {
                $('#SoldToAddress').val(result.address);
                $("#SoldToAddress").prop("readonly", true);
                if ($('#ddlBuyerState').val() != result.data) {
                    $('#ddlBuyerState').val(result.state);
                    $('#ddlBuyerState').trigger('change');
                    $('#SoldToStateId').val(result.state);
                }
                $('#ddlBuyerCity').val(result.city);
                $('#SoldToCityId').val(result.city);
                $('#SoldToZip').val(result.zip);
            }
        });
    }
}
function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            if (elem.name == "ddlPickupState")
                $("select#ddlPickupCity").clearSelect();
            else
                $("select#ddlShipToCity").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#ddlBuyerCity").fillSelect(result.data);
                    }
                }
            });
        }
    }
}

function SetStateCity(elem) {
    $('#SoldToStateId').val($('#ddlBuyerState').val());
    $('#SoldToCityId').val($('#ddlBuyerCity').val());

}