﻿var affiliateType = $("#hfAffiliateType").val();
var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    $("button, input:submit, input:button").button();


    $("#txtBeginDate").datepicker();
    //$('#txtBeginDate').datepicker('setDate', new Date());
    $("#txtEngingDate").datepicker();
    //$('#txtEngingDate').datepicker('setDate', new Date());



    jQuery("#jqGrid").jqGrid({
        url: "/SalesOrder/GetSaleOrderEworldList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id', 'ASR', 'ASR', 'Created', 'SoldDate', 'Sold To', 'SoldToAffiliateId', 'Items', 'Amount', 'Invoiced', 'Actions'],
        colModel: [
                    { name: 'SalesOrderId', index: 'SalesOrderId', width: 40, align: 'center', sortable: false },
                    { name: 'SoldBy', index: 'SoldBy', width: 120, align: 'Left', sortable: false, hidden: true },
                    { name: 'ASR', index: 'ASR', width: 120, align: 'Left', sortable: false, hidden: false },
   		            { name: 'Created', index: 'Created', width: 80, align: 'center', sortable: false },
                    { name: 'SoldDate', index: 'SoldDate', width: 80, align: 'center', sortable: false },
                    { name: 'SoldTo', index: 'SoldTo', width: 120, align: 'Left', sortable: false, hidden: false },
                    { name: 'SoldToAffiliateId', index: 'SoldToAffiliateId', width: 120, align: 'Left', sortable: false, hidden: true },
   		            { name: 'Items', index: 'Items', width: 80, editable: false, sortable: false, align: 'center', formatter: reportFmatter },
                    { name: 'Amount', index: 'Amount', width: 80, align: 'right', editable: true, hidden: false, sortable: false, formatter: 'number', formatoptions: { decimalPlaces: 2} },
                    { name: 'Invoiced', index: 'Invoiced', sortable: false, width: 30, align: 'center', formatter: InvoicedCheckBoxFormatter },
                    { name: 'Edit', index: 'Edit', width: 80, align: 'right', editable: false, sortable: false, align: 'center', formatter: fmatter }
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240



    });

    $("#jqGrid").jqGrid('navGrid', '#pager2',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    $("#btnSearch").click(function () {
        Search();
    });


});

function Search() {
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEngingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);
    var beginId = $("#txtBeginId").val();
    var endId = $("#txtEndId").val();

    var invoiceStatus = $("#ddlInvoiceStatus").val();
    jQuery("#jqGrid").setGridParam({ postData: { BeginDate: strBeginingDate, EndDate: strEndingDate, BeginId: beginId, EndId: endId, InvoiceStatus: invoiceStatus }, page: 1 }).trigger("reloadGrid");

}

function reportFmatter(cellvalue, options, rowObject) {
    var salesOrderId = rowObject[0];
    var cell = "<div>";
    var url = "return showReport('../reports/SalesOrderItemReport.aspx?SalesOrderId=" + salesOrderId + "', 'SalesOrderItemReport');";
    cell += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Sales Order Report'  title='Sales Order Report'  style='border:none;'/></a>";
    cell += "</div>";
    return cell;
}

function fmatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var cell = "";
    if (cellvalue == false)
        cell = "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:unsellSalesOrder(" + rowid + ")' href='#' >Unsell</a>";
    else
        cell = "&nbsp;";
    return cell;
}

function unsellSalesOrder(salesOrderId) {
    $.ajax({
        url: '/SalesOrder/UnsellSalesOrder',
        dataType: 'json',
        type: "POST",
        data: "id=" + salesOrderId,
        async: false,
        success: function (result) {
            if (result.success) {
                //alert("Success:SalesOrder Updated");
                //reformatActionMenu(rowId, invoiceid, result.invoiced, result.invoicedate);
                $("#jqGrid").trigger("reloadGrid");
            }
            else {
                alert("Unsell Failed:" + result.message);
                chkbox.checked = !(chkbox.checked);
            }
        }

    });

}

function InvoicedCheckBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    //var salesOrderId = rowObject[0];
    if (cellvalue == 1)
        return "<input type='checkbox' Name='Approved' id='invoicedCheck" + rowid + "' onclick=\"UpdateInvoiced(" + rowid + "," + rowid + ")\"  checked />";
    else
        return "<input type='checkbox' Name='Approved' id='invoicedCheck" + rowid + "' onclick=\"UpdateInvoiced(" + rowid + "," + rowid + ")\"  />";
}

function UpdateInvoiced(rowId, salesOrderId) {
    var chkbox = document.getElementById("invoicedCheck" + rowId);
    var rowData = $("#jqGrid").getRowData(rowId);
    $.ajax({
        url: '/SalesOrder/UpdateInvoiced',
        dataType: 'json',
        type: "POST",
        data: "id=" + salesOrderId,
        async: false,
        success: function (result) {
            if (result.success) {
                rowData["Edit"] = chkbox.checked;
                rowData["Invoiced"] = chkbox.checked;
                $("#jqGrid").jqGrid('setRowData', rowId, rowData);
                //alert("Success:SalesOrder Updated");
                //reformatActionMenu(rowId, invoiceid, result.invoiced, result.invoicedate);

            }
            else {
                alert("Update Failed:" + result.message);
                chkbox.checked = !(chkbox.checked);
            }
        }

    });
}

