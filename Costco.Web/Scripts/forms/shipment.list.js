﻿var affiliateType = $("#hfAffiliateType").val();

$(document).ready(function () {
    //$("#warehouses").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 120, width: 125 });
    $("button, input:submit, input:button").button();

    var myDate = new Date();
    var month = myDate.getMonth();
    var day = myDate.getDay();
    var year = myDate.getFullYear() - 1;
    myDate = month + "/" + day + "/" + year;

    $("#txtBeginDate").datepicker();
    $("#txtEngingDate").datepicker();

    var data = eval('(' + $("#Data").val() + ')');

    var Warehouses = '';
    if (data.Warehouses) {
        Warehouses = data.Warehouses;
        $("#txtWarehouse").val(data.Warehouses);
    }

    var BeginDate = '';
    if (data.BeginDate) {
        BeginDate = data.BeginDate;
        $("#txtBeginDate").val(data.BeginDate);
    }
    var EndDate = '';
    if (data.EndDate) {
        EndDate = data.EndDate;
        $("#txtEndingDate").val(data.EndDate);
    }
    var BeginId = '';
    if (data.BeginId) {
        BeginId = data.BeginId;
        $("#txtBeginId").val(data.BeginId);
    }
    var EndId = '';
    if (data.EndId) {
        EndId = data.EndId;
        $("#txtEndId").val(data.EndId);
    }

    var page = 1;
    var rows = 10;
    if (data.page) {
        page = data.page;
    }
    if (data.rows) {
        rows = data.rows;
    }


    jQuery("#jqGrid").jqGrid({
        url: "GetShippingDataList",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['ShipmentId', 'Warehouse', 'ASR', 'Shipping Date', 'Received Date', 'Shp. Rpt/Lbl/BOL', '', '', 'ReadyToShip', 'InvoiceId', 'Complete', 'Complete', 'Actions'],
        colModel: [
                    { name: 'ShipmentId', index: 'ShipmentId', width: 40, align: 'Left', sortable: false },
                    { name: 'Warehouse', index: 'Warehouse', width: 80, align: 'Left', sortable: false },
                    { name: 'ASR', index: 'ASR', width: 120, align: 'Left', sortable: false },
   		            { name: 'ShipmentDate', index: 'ShipmentDate', width: 80, align: 'Left', sortable: false, formatter: shippingFmatter },
   		            { name: 'ReceivedDate', index: 'ReceivedDate', width: 80, editable: false, sortable: false, align: 'center', formatter: receivingFmatter },
   		            { name: 'ShippingData', index: 'ShipmentId', width: 80, editable: false, sortable: false, align: 'center', formatter: reportShippingFmatter },
                    { name: 'DownloadFile', index: 'ShipmentId', width: 30, editable: false, sortable: false, align: 'center' },
   		            { name: 'AttachFile', index: 'ShipmentId', width: 30, editable: false, sortable: false, align: 'center' },
                    { name: 'ReadyToShip', index: 'ReadyToShip', width: 40, align: 'Left', sortable: false, hidden: true },
                    { name: 'InvoiceId', index: 'InvoiceId', width: 40, align: 'Left', sortable: false, hidden: true },
                    { name: 'Complete', index: 'Complete', width: 40, align: 'Left', sortable: false, hidden: true },
                    { name: 'CompleteEdit', index: 'CompleteEdit', width: 50, editable: false, sortable: false, align: 'center', formatter: CompleteCheckBoxFormatter, hidden: (affiliateType == 5 ? false : true) },
                    { name: 'Actions', index: 'Actions', width: 50, editable: false, sortable: false, align: 'center', formatter: actionFmatter },
   	              ],
        rowNum: rows,
        page: page,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        postData: { Warehouses: Warehouses, BeginDate: BeginDate, EndDate: EndDate, BeginId: BeginId, EndId: EndId }


    });

    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    //if (affiliateType != 1 && affiliateType != 5 && affiliateType !=2) {
    //    jQuery("#jqGrid").jqGrid('hideCol', ["Actions"]);
    //}

    $("#btnSearch").click(function () {
        Search();
    });

    if (affiliateType == 4) {
        $("#btnAdd").show();
        $("#txtWarehouse").hide();
    }
    else {
        $("#btnAdd").hide();
        $("#txtWarehouse").show();
    }


    $('input[id$=txtShipDate]').datepicker({});
    $('input[id$=txtShipDate]').datepicker('setDate', new Date());



});

function Search() {
    var warehouse = '';
    //warehouse = $("#warehouses option:selected").text();
    warehouse = $("#txtWarehouse").val();
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEndingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);

    var beginId = $("#txtBeginId").val();
    var endId = $("#txtEndId").val();

    jQuery("#jqGrid").setGridParam({ postData: { Warehouses: warehouse, BeginDate: strBeginingDate, EndDate: strEndingDate, BeginId: beginId, EndId: endId }, page: 1 }).trigger("reloadGrid");

}

$('input[id$=btnAdd]').click(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/Shipment/Index';
    window.location.href = url;
});

function receivingFmatter(cellvalue, options, rowObject) {
    if (cellvalue == "") {
        return "&nbsp;";
    }
    else {
        var ShipmentId = rowObject["ShipmentId"];
        var complete = rowObject[10];
        if (ShipmentId == null)
            ShipmentId = rowObject[5];
        var url = "return showReport('../reports/ReceivingReport.aspx?ShipmentId=" + ShipmentId + "', 'ReceivingReport');";
        var cellstring = "<div>" + cellvalue + " <a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#'  onclick=\"" + url + "\"><img src='../../Content/images/report.png'  alt='Receiving Report'  title='Receiving Report'  style='border:none;'/></a>";
        var urlCert = "return showReport('/Reports/GenericReport.aspx?ReportName=CertificateOfRecycling&DisplayName=CertificateOfRecycling&ShipmentId=" + ShipmentId + "','CertificateOfRecycling');";
        cellstring += " <a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#'  onclick=\"" + urlCert + "\"><img src='../../Content/images/report.png'  alt='Certificate Of Recycling'  title='Certificate Of Recycling'  style='border:none;'/></a>";
        cellstring += "</div>";
        return cellstring;
    }

}


function shippingFmatter(cellvalue, options, rowObject) {
    var isShipped = rowObject["ReadyToShip"];
    if (isShipped == null)
        isShipped = rowObject[8];

    if (isShipped == true && affiliateType == 1) {
        var url = "javascript:openRevertDialog(" + options.rowId + ")";
        return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#' onclick='" + url + "' >" + cellvalue + "</a>";
    }
    else if (isShipped == true) {
        return cellvalue;
    }
    else if (isShipped == false && affiliateType == 4) {
        var url = "javascript:openShipDialog(" + options.rowId + ")";
        return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#' onclick='" + url + "' >Not Shipped</a>";
    }
    else {
        return "Not Shipped";
    }
}
function openShipDialog(rowId) {
    var rowData = $("#jqGrid").getRowData(rowId);
    var shipmentId = rowData["ShipmentId"];
    $("#dialog-ship").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                var shipdate = $("#txtShipDate").val();
                $.ajax({
                    url: "/Shipment/UpdateReadyToShip",
                    dataType: 'json',
                    type: "POST",
                    data: ({ shipmentId: shipmentId, shipmentDate: $("#txtShipDate").val() }),
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result.success == true) {
                            rowData["ShipmentDate"] = result.shipmentDate;
                            rowData["ReadyToShip"] = true;
                            rowData["Actions"] = "";
                            $("#jqGrid").jqGrid('setRowData', rowId, rowData );
                        }
                        else {
                            alert(result.message);
                        }
                        //jQuery("#jqGrid").jqGrid('delRowData', rowId);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An error Occurred");
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}

function openRevertDialog(rowId) {
    var rowData = $("#jqGrid").getRowData(rowId);
    var shipmentId = rowData["ShipmentId"];
    $("#dialog-revert").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                $.ajax({
                    url: "/Shipment/RevertReadyToShip",
                    dataType: 'json',
                    type: "POST",
                    data: ({ shipmentId: shipmentId }),
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result.success == true) {
                            rowData["ShipmentDate"] = "Not Shipped";
                            rowData["ReadyToShip"] = false;
                            $("#jqGrid").jqGrid('setRowData', rowId, rowData);
                        }
                        else {
                            alert(result.message);
                        }
                        //jQuery("#jqGrid").jqGrid('delRowData', rowId);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An error Occurred");
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}


function reportShippingFmatter(cellvalue, options, rowObject) {
    var isShipped = rowObject["ReadyToShip"];
    if (isShipped == null)
        isShipped = rowObject[8];

    var ShipmentId = rowObject["ShipmentId"];
    if (ShipmentId == null)
        ShipmentId = rowObject[5];

    var url = "return showReport('../reports/ShippingReport.aspx?ShipmentId=" + ShipmentId + "', 'ShippingReport');"
    var urlLabel = "return showReport('../reports/ShippingLabel.aspx?ShipmentId=" + ShipmentId + "', 'ShippingLabel');"
    var urlBOL = "return showReport('../reports/BillOfLaiding.aspx?ShipmentId=" + ShipmentId + "', 'BillOfLaiding');"

    if (affiliateType == 4 && isShipped == false) {
        url = "javascript:alert('Status is currently \&#34;Not Shipped\&#34; \\n Please click \&#34;Not Shipped\&#34; under the \&#34;Shipping Date\&#34; column.');";
        urlLabel = url;
        urlBOL = url;
    }

    var cell  = "<div>";
        cell += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Shipping Report'  title='Shipping Report'  style='border:none;'/></a>"
        cell += "  <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + urlLabel + "\" href='#' ><img src='../../Content/images/report.png'  alt='Shipping Label'  title='Shipping Label'  style='border:none;'/></a>"
        cell += "  <a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + urlBOL + "\" href='#' ><img src='../../Content/images/report.png'  alt='Bill of Laiding'  title='Bill of Laiding'  style='border:none;'/></a>"
        cell += "</div>";
    return cell;
}

function actionFmatter(cellvalue, options, rowObject) {

    var ShipmentId = rowObject[0];
    var isShipped = rowObject[8];
    var receivedDate = rowObject[4];
    var isComplete = rowObject[10];

    if (ShipmentId == undefined)
        return cellvalue;

    if (isComplete == true)
        return "&nbsp;";

    var Warehouses = $("#jqGrid").getGridParam("postData").Warehouses;
    var BeginDate = $("#jqGrid").getGridParam("postData").BeginDate;
    var EndDate = $("#jqGrid").getGridParam("postData").EndDate;
    var BeginId = $("#jqGrid").getGridParam("postData").BeginId;
    var EndId = $("#jqGrid").getGridParam("postData").EndId;
    var page = $("#jqGrid").getGridParam("page");
    var rows = $("#jqGrid").getGridParam("rowNum");


    if ((affiliateType == 5  || affiliateType == 2) && isShipped==true) {
        var url = '/Shipment/Edit?Id=' + ShipmentId;
        url = url + '&data=Warehouses:"' + Warehouses + '",BeginDate:"' + BeginDate + '",EndDate:"' + EndDate + '",BeginId:"' + BeginId + '",EndId:"' + EndId + '",page:"' + page + '",rows:"' + rows + '"';
        var content = "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='" + url + "' >Receiving</a>";
        return content;
    }

    if (affiliateType == 1 ) {
        var url = '/Shipment/Edit?Id=' + ShipmentId;
        url = url + '&data=Warehouses:"' + Warehouses + '",BeginDate:"' + BeginDate + '",EndDate:"' + EndDate + '",BeginId:"' + BeginId + '",EndId:"' + EndId + '",page:"' + page + '",rows:"' + rows + '"';
        var content = "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='" + url + "' >View</a>";
        if (affiliateType == 1) {
            var url2 = "javascript:openDeleteDialog(" + ShipmentId + ")";
            content += "&nbsp; <a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='" + url2 + "' >| Delete </a>";

        }
        return content;
    }

    else if (affiliateType == 4 && receivedDate == "" && isShipped == false) {
        var url = '/Shipment/EditShipper?Id=' + ShipmentId;
        url = url + '&data=Warehouses:"' + Warehouses + '",BeginDate:"' + BeginDate + '",EndDate:"' + EndDate + '",BeginId:"' + BeginId + '",EndId:"' + EndId + '",page:"' + page + '",rows:"' + rows + '"';
        return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='" + url + "' >Edit</a>";
    }
    else {
        return "&nbsp;";
    }
}

function openFileDialog(dataProcessReportId) {
    var w = window.open("../../Upload.aspx?processDataId=" + dataProcessReportId + "&attachfile=true", "Window1", "height=300,width=450,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");
}

function SaveAttachment(fileName, processDataId) {
    var rootPath = window.location.protocol + "//" + window.location.host;

    $.ajax({
        url: rootPath + '/Shipment/SaveAttachment',
        dataType: 'text',
        type: "POST",
        data: ({ fileName: fileName, processDataId: processDataId }),
        async: false,
        success: function (result) {
            $("#jqGrid").trigger("reloadGrid");
        }
    });
}

function GetAttachments(processDataId) {
    lastProcessDataId = processDataId;
    var rootPath = window.location.protocol + "//" + window.location.host;
    var docsHtml = "<ul>"

    $.ajax({
        url: rootPath + '/Shipment/GetAttachments',
        dataType: 'json',
        type: "POST",
        data: ({ processDataId: processDataId }),
        async: false,
        success: function (result) {
            docsHtml += "<table width='100%'>";
            $.each(result.rows, function (index, entity) {
                docsHtml += "<tr><td width='100%'><a href=javascript:Download('" + entity.UploadFileName + "'); style='color:#FF9900;text-decoration:none;_text-decoration:none;'>" + entity.UploadFileName + "</a></td>";
                docsHtml += "</tr>";
                //docsHtml += "<td width='15%'><img src='../../Content/images/icon/delete.png' alt='Remove attachment' style='cursor:hand;' title='Remove attachment' id='btnDel' border='0' onclick='javascript:DeleteAttachment(" + entity.DocumentID + ");' /></td></tr>";
            });

            docsHtml += "</table>"

        }
    });

    $("#dialogFiles").html(docsHtml);

    $("#dialogFiles").dialog({
        modal: false,
        width: 400,
        resizable: false,
        position: 'center'
    }
                );

}

function Download(fileName) {
    fileName = fileName.replace('#', '<~>');
    if (fileName != "-" && fileName != "") {
        $.ajax({
            url: '/FileUpload/IsFileExist?FileName=' + fileName,
            dataType: 'json',
            type: 'POST',
            //data: ({}),
            async: false,
            success: function (isFileExist) {
                if (isFileExist) {
                    window.location = "/FileUpload/Download?FileName=" + fileName;
                }
                else {
                    alert('This file does not exist.'); //data.toString()
                }
            }
        });
    }

}


function CompleteCheckBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var complete = cellvalue;
    var isShipped = rowObject[8];
    if (rowObject[9] > 0) {
        return "<input type='checkbox' Name='Approved' id='completeCheck" + rowid + "'  checked DISABLED />";
    }
    if (isShipped == false)
        return "&nbsp;";

    if (complete == 1) {
        return "<input type='checkbox' Name='Approved' id='completeCheck" + rowid + "' onclick=\"UpdateCompleteCheck(" + rowid  + ")\"  checked />";
    }
    else
        return "<input type='checkbox' Name='Approved' id='completeCheck" + rowid + "' onclick=\"UpdateCompleteCheck(" + rowid + ")\"  />";
}


function UpdateCompleteCheck(rowId) {
    var chkbox = document.getElementById("completeCheck" + rowId);
    $.ajax({
        url: '/Shipment/UpdateComplete',
        dataType: 'json',
        type: "POST",
        data: "id=" + rowId,
        async: false,
        success: function (result) {
            if (result.success) {
                alert("Success:Shipment Updated");
                reformatAction(rowId, result.complete);

            }
            else {
                alert("Update Failed:" + result.message);
                chkbox.checked = !(chkbox.checked);
            }
        }

    });
}


function reformatAction(rowId, complete) {
    var isComplete = complete;
    var shipmentId = rowId;
    var formattedAction;
    if (isComplete == true)
        formattedAction = "&nbsp;";
    else {
        var url = '/Shipment/Edit?Id=' + shipmentId;
        formattedAction = "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='" + url + "' >Receiving</a>";
    }
    $("#jqGrid").jqGrid("setCell", rowId, 'Actions', formattedAction);

}


function openDeleteDialog(shipmentId) {
    $("#dialog-delete").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                $.ajax({
                    url: "/Shipment/DeleteShipment",
                    dataType: 'json',
                    type: "POST",
                    data: ({ ShipmentId: shipmentId }),
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result.success == true) {
                            $('#jqGrid').trigger("reloadGrid");
                        }
                        else {
                            alert(result.message);
                        }
                        //jQuery("#jqGrid").jqGrid('delRowData', rowId);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An error Occurred");
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}