﻿//var to store last selected row id
var lastSel;
var inEditMode = false;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;
var inEditMode = false;
var scrollPosition = 0;
var affiliateType = $("#hfAffiliateType").val();
var affiliateId = $("#hfAffiliateId").val();

$(document).ready(function () {
    affiliateType = $("#hfAffiliateType").val();
    affiliateId = $("#hfAffiliateId").val();
    jQuery("#jqGrid").jqGrid({
        url: "GetShipmentItems",
        datatype: 'json',
        mtype: 'GET',
        height: 150,

        colNames: ['ShipmentItemID', 'Pallet #', 'ItemId', 'Description', 'lbs', 'Qty', 'Condition', 'Dispostion', 'Qty', 'Condition', 'Comment', 'CED', 'Pull', 'Price', 'Description', 'Actions'],
        colModel: [
                    { name: 'ShipmentItemID', index: 'ShipmentItemID', editable: true, width: 60, hidden: true },
   		            { name: 'PalletUnitNumber', index: 'PalletUnitNumber', editable: false, width: 30, sortable: false, editrules: { integer: true, required: true }, align: 'right' },
                    { name: 'ItemId', index: 'ItemId', editable: false, width: 50, sortable: false, editrules: { required: true} },
   		            { name: 'Description', index: 'Description', editable: false, width: 100, sortable: false, editrules: { required: true} },
                    { name: 'Weight', index: 'Weight', editable: false, width: 30, sortable: false, editrules: { required: true, number: true }, align: 'right' },
   		            { name: 'Qty', index: 'Qty', editable: false, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },

                    { name: 'Condition', index: 'Condition', editable: false, width: 50, sortable: false, editrules: { required: true} },
                    { name: 'Disposition', index: 'Disposition', editable: false, width: 50, sortable: false, editrules: { required: true} },
                    { name: 'QtyReceived', index: 'QtyReceived', editable: true, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
        //{ name: 'ConditionReceived', index: 'ConditionReceived', editable: true, width: 150, sortable: false, editrules: { required: true} },
                    {name: 'ConditionReceived', index: 'ConditionReceived', width: 50, editable: true, sortable: false, editrules: { required: true }, edittype: "select" },
                    { name: 'ConditionReceived2', index: 'ConditionReceived2', editable: true, width: 80, sortable: false, editrules: { required: false }, editoptions: { dataEvents: [{ type: 'change', fn: function (e) { getCondition(e) } }]} },
   		            { name: 'CoveredDevice', index: 'CoveredDevice', editable: true, width: 30, edittype: 'checkbox', formatter: "checkbox", editoptions: { value: "true:false" }, hidden: true },
                    { name: 'Pulled', index: 'Pulled', editable: true, width: 30, sortable: false, editrules: { integer: true, required: true }, align: 'right' },
                    { name: 'Price', index: 'Price', hidden: (affiliateId == 533 ? false : true), editable: true, width: 50, sortable: false, editrules: { required: false, number: true }, align: 'right' },
                    { name: 'DefaultDescription', hidden: (affiliateId == 533 ? false : true), index: 'DefaultDescription', editable: true, width: 100, sortable: false, editrules: { required: false } },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'left', width: 70, editable: false }

   	            ],
        rowNum: 100000,
        rowList: [10, 25, 50, 100],
        //pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "/Shipment/EditShipmentItem",
        //afterInsertRow: function(rowid, rowdata){
        //    if (rowdata.ItemId == 'Misc')
        //    {
                
        //        $('#jqGrid').jqGrid('setCell', rowid, 'Price', '', '', { editable: true });
        //        $('#jqGrid').jqGrid('setCell', rowid, 'DefaultDescription', '', '', 'not-editable-cell');
        //    }

        //},
        gridComplete: function () {
            LoadDropdownsData();
            sumWeight();
        },
        loadComplete: function () {
            //var ids = $('#jqGrid').jqGrid('getDataIDs');
            //for (var i=0;i<ids.length;i++) {
            //    var id=ids[i];
            //    if ($('#jqGrid').jqGrid('getCell', id, 'ItemId') == 'Misc') {
            //        $('#jqGrid').jqGrid('setCell', id, 'Price', '', 'not-editable-cell');
            //    }
            //}
            jQuery("#jqGrid").closest(".ui-jqgrid-bdiv").scrollTop(scrollPosition);
        }


    });

    //$("#jqGrid").jqGrid('navGrid', '#pager',
    //            { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;


        var isFocused = false;
        isFocused = false;

        //if (isValid) {
        //    var d = new Date($("#txtReceivedDate").val());
        //    var now = new Date();
        //    now.setHours(0, 0, 0, 0);
        //    var flag = d < now;
        //    if (flag) {
        //        $("#validationSummary").html("<ul><li>Receiving Date cannot be in the past.</li></ul>");
        //        isValid = false;
        //    }
        //    else
        //        isValid = true;
        //}

        if (!isValid) {
            return false;
        }


        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');
        //if (!isAllSaved()) {
        //    return false;
        //}
        if (inEditMode) {
            alert('There is an item opened in edit mode. Please save or cancel it first.');
            return false;
        }
        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    $("form").attr("action", rootPath + "/Shipment/Edit");
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });

    $('input[id$=txtReceivedDate]').datepicker({});
    //$('input[id$=txtReceivedDate]').datepicker('setDate', new Date());

    $("#dlgSales").dialog({
        modal: true,
        width: 300,
        height: 240,
        //zIndex: 500,
        resizable: false,
        position: 'center',
        autoOpen: false,
        show: 'fold',
        hide: 'fold',
        Ok: function () {
            $(this).dialog('close');
        }
    });




    $("#dlgAddSalesBtnCancel").click(function () {
        $('#dlgSales').dialog('close');
    });

    $("#dlgAddSalesBtnSave").click(function () {
        AddSalesItem();
    });

    if (affiliateType  == 1) {
        $("#ddlRecyclerList").show();
        $("#btnRecylerInfo").show();
    }
    else {
        $("#ddlRecyclerList").hide();
        $("#btnRecylerInfo").hide();
    }

    $('#ddlRecyclerList').change(function () {
        FillShipToAddressData();
    })

    $("#btnRecylerInfo").click(function () {
        SaveRecyclerInfo();
    });

});

function ActionMenuFormatter(cellvalue, options, rowdata) {

    if (affiliateType == 1 || affiliateType == 2)
        return "";
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "' style='float:left;'><img src='/Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='/Content/images/icon/copy.png' style='cursor:hand;'  alt='Copy'  title='Copy' onclick='javascript:CopyRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>"
    actionMenuHtml += "</div>";

    //if eworld and receivedqty minus pulled >= 1 and disposition = salvage
    if ($('#txtRecyclerName').val() == "E-World Recyclers" && rowdata[8] - rowdata[12] >= 1 && rowdata[7] == "Salvage")
        actionMenuHtml += "<div id='SUB_ActionMenu_" + options.rowId + "' style='float:left;'><img src='/Content/images/icon/pricing2.png' style='cursor:hand;'  alt='Sell'  title='Sell' onclick='javascript:OpenSalesDiag(\"" + options.rowId + "\")' style='border:none;' class='edit'/></div>";
    else
        actionMenuHtml += "<div id='SUB_ActionMenu_" + options.rowId + "' style='float:left;display:none;'><img src='/Content/images/icon/pricing2.png' style='cursor:hand;'  alt='Sell'  title='Sell' onclick='javascript:OpenSalesDiag(\"" + options.rowId + "\")' style='border:none;' class='edit'/></div>";    
    
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='/Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='/Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";

        
    return actionMenuHtml;
}

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SUB_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    var ret = $('#jqGrid').jqGrid('getRowData', rowId);
    if (ret.ItemId == 'Misc' || ret.ItemId.length < 2 || ret.ItemId.length > 6) {
        $('#jqGrid').setColProp('Price', { editable: false });
        $('#jqGrid').setColProp('DefaultDescription', { editable: false });
    }
    else {
        $('#jqGrid').setColProp('Price', { editable: true });
        $('#jqGrid').setColProp('DefaultDescription', { editable: true });
    }
    $('#jqGrid').editRow(rowId);
}

function CopyRow(rowId) {
    lastSel = rowId;
    var rowdata = $('#jqGrid').getRowData(rowId);
    var condition = rowdata['Condition'];
    var condtiontype = 99;
    switch(condition)
    {
        case "Used":
            condtiontype = 2;
            break;
        case "Like New":
            condtiontype = 1;
            break;
        case "Opened/damaged box":
            condtiontype = 7;
            break;
        case "Incomplete":
            condtiontype = 9;
            break;
        case "Damaged":
            condtiontype = 5;
            break;
        default:
            condtiontype = 99;
    }
    //$('#jqGrid').editRow(rowId);

    $("#jqGrid").setRowData(rowId, { QtyReceived: rowdata['Qty'], ConditionReceived: condtiontype });
    $('#jqGrid').editRow(rowId);
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
    return false;
}



function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(response) {
    var json = response.responseText;
    var result = eval("(" + json + ")");

    reformatAction(lastSel);
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    //scrollPosition = jQuery("#jqGrid").closest(".ui-jqgrid-bdiv").scrollTop();
    //$('#jqGrid').trigger("reloadGrid");
    if (result.status == false)
        alert(result.message);
    return result.status;
    
}

function CancelRow(rowId) {
    reformatAction(rowId);
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}


function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            $("select#" + elem.name).clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        if (elem.name == "ddlPickupState")
                            $("select#ddlPickupCity").fillSelect(result.data);
                        else
                            $("select#ddlShipToCity").fillSelect(result.data);

                    }
                }
            });
        }
    }
}


function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}

function FillShipToAddressData() {
    var reccyclerId = $('#ddlRecyclerList').val();
    $("#txtRecyclerName").val($("#ddlRecyclerList").find("option:selected").text());
    if (reccyclerId == 0) {
        $('#txtShipToAddress').val("");
        if ($('#ddlShipToState').get(0).options[0].selected != true) {
            $('#ddlShipToState').get(0).options[0].selected = true;
            $("#ddlShipToState").trigger("change");
        }
        $('#ddlShipToCity').val(0);
        $('#txtShipToZip').val("");
        $('#txtShipToPhone').val("");

    } else {
        $.ajax({
            url: '/Shipment/GetShipToAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: reccyclerId },
            async: false,
            success: function (result) {
                $('#txtShipToAddress').val(result.address);
                if ($('#ddlShipToState').val() != result.state) {
                    $('#ddlShipToState').val(result.state);
                    $("#ddlShipToState").trigger("change");
                }
                $('#ddlShipToCity').val(result.city);
                $('#txtShipToZip').val(result.zip);
            }
        });
    }
}

function LoadDropdownsData() {
    $.ajax({
        url: '/Shipment/GetDropDownsData',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('ConditionReceived', { editoptions: { value: result.receivedConditions} });
        }
    });

}

function sumWeight() {
    var totalweight = 0;
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    var rows = jQuery("#jqGrid").jqGrid('getRowData');
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        totalweight += parseFloat(row.Weight) * parseFloat(row.Qty);
    }
    $("#txtTotalWeight").val(totalweight);
}

function getCondition(e) {
    condition = $(e.target).val();
    if (condition == "1")
        $(e.target).val("Tested OK.");
    else if (condition == "2")
        $(e.target).val("Out of box.");
    else if (condition == "3")
        $(e.target).val("In box.");
    else if (condition == "4")
        $(e.target).val("Parts Only.");

}

function reformatAction(rowId) {
    var rowdata = $('#jqGrid').getRowData(rowId);
    var disposition = rowdata['Disposition'];
    var qtyReceived = $("#" + rowId + "_QtyReceived").val();
    var pulled = $("#" + rowId + "_Pulled").val();
    if ($('#txtRecyclerName').val() == "E-World Recyclers" && qtyReceived - pulled >= 1 && disposition == "Salvage")
        $('#SUB_ActionMenu_' + lastSel).css({ display: "block" });
}


function OpenSalesDiag(rowId) {
    var rowdata = $('#jqGrid').getRowData(rowId);
    var disposition = rowdata['Disposition'];
    $('#currentShipmentItem').val(rowdata['ShipmentItemID']);
    $('#txtItemDescription').val(rowdata['Description']);
    $('#txtItemItemId').val(rowdata['ItemId']);
    $('#txtItemDescription').val(rowdata['Description']);
    
    var qtyReceived = rowdata['QtyReceived'];
    var pulled = rowdata['Pulled'];
    $('#txtItemQuantity').val(qtyReceived - pulled);
    $("#dlgSales").dialog('open');
}


function AddSalesItem() {
    shipmentId = $('#ShipmentId').val();
    shipmentItemId = $('#currentShipmentItem').val();
    description = $('#txtItemDescription').val();
    itemId = $('#txtItemItemId').val();
    description = $('#txtItemDescription').val();
    quantity = $('#txtItemQuantity').val();
    salesOrderId = $('#txtSalesOrderId').val();

    $.ajax({
        url: '/SalesOrder/AddSaleOrderShipmentItem',
        dataType: 'json',
        type: "POST",
        data: ({ SalesOrderId: salesOrderId, ShipmentId: shipmentId, ShipmentItemId: shipmentItemId, Quantity:quantity  }),
        async: false,
        success: function (result) {
            if (result.success == true) {
                $('#dlgSales').dialog('close');
                //jQuery("#jqGrid").trigger("reloadGrid");
            } else {
                alert(result.message);
            }
        }
    });
}

function SaveRecyclerInfo() {
    var shipmentId = $('#ShipmentId').val();
    var shipToAffiliateId = $('#ddlRecyclerList').val();
    var shipToName = $("#txtRecyclerName").val();
    var shipToAddress = $('#txtShipToAddress').val();
    var shipToZip = $('#txtShipToZip').val();
    var shipToCityId = $('#ddlShipToCity').val();
    var shipToStateId = $('#ddlShipToState').val();


    $.ajax({
        url: '/Shipment/SaveRecyclerInfo',
        dataType: 'json',
        type: "POST",
        data: ({ shipmentId: shipmentId, shipToAffiliateId: shipToAffiliateId, shipToName: shipToName, shipToAddress: shipToAddress, shipToZip: shipToZip, shipToCityId: shipToCityId, shipToStateId: shipToStateId }),
        async: false,
        success: function (result) {
            if (result.success == true) {
                alert("Recycler Info Saved");
            } else {
                alert(result.message);
            }
        }
    });
}