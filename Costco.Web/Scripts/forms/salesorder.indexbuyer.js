﻿var affiliateType = $("#hfAffiliateType").val();
var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    $("button, input:submit, input:button").button();


    $("#txtBeginDate").datepicker();
    //$('#txtBeginDate').datepicker('setDate', new Date());
    $("#txtEngingDate").datepicker();
    //$('#txtEngingDate').datepicker('setDate', new Date());

    jQuery("#jqGrid").jqGrid({
        url: "/SalesOrder/GetSaleOrdersBuyerList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id', 'ASR','Created','Sold', 'Items','State','Pallets', 'BuyerId', 'Amount','Bid'],
        colModel: [
                    { name: 'SalesOrderId', index: 'SalesOrderId', width: 40, align: 'center', sortable: false },
                    { name: 'SoldBy', index: 'SoldBy', width: 120, align: 'Left', sortable: false, hidden: true  },
   		            { name: 'Created', index: 'Created', width: 80, align: 'center', sortable: false },
                    { name: 'SoldDate', index: 'SoldDate', width: 80, align: 'center', sortable: false },
   		            { name: 'Items', index: 'Items', width: 80, editable: false, sortable: false, align: 'center', formatter: reportFmatter },
                    { name: 'State', index: 'State', width: 80, align: 'center', sortable: false },
                    { name: 'Pallets', index: 'Pallets', width: 80, align: 'center', sortable: false },
   		            { name: 'BuyerId', index: 'BuyerId', width: 80, editable: false, sortable: false, align: 'center', hidden:true },
                    { name: 'Amount', index: 'Amount', width: 80, align: 'right', editable: true, hidden: false, sortable: false, formatter: 'number', formatoptions: { decimalPlaces: 2} },
                    { name: 'Bid', index: 'Bid', width: 80, align: 'right', editable: false, sortable: false, align: 'center', formatter: bidFmatter }
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "/SalesOrder/AddBid"



    });

    $("#jqGrid").jqGrid('navGrid', '#pager2',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    $("#btnSearch").click(function () {
        Search();
    });


});

function Search() {
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEngingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);
    var beginId = $("#txtBeginId").val();
    var endId = $("#txtEndId").val();
    var auctionType = $("#ddlAuctionType").val();
    jQuery("#jqGrid").setGridParam({ postData: { BeginDate: strBeginingDate, EndDate: strEndingDate, BeginId: beginId, EndId: endId, AuctionType: auctionType }, page: 1 }).trigger("reloadGrid");

}

function reportFmatter(cellvalue, options, rowObject) {
    var salesOrderId = rowObject[0];
    var cell = "<div>";
    var url = "return showReport('../reports/SalesOrderItemReport.aspx?SalesOrderId=" + salesOrderId + "', 'SalesOrderItemReport');";
    cell += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Sales Order Report'  title='Sales Order Report'  style='border:none;'/></a>";
    cell += "</div>";
    return cell;
}

function bidFmatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    if (cellvalue == 'Open') {
        var cell = "<div id='ED_ActionMenu_" + options.rowId + "'>";
        cell += "&nbsp;&nbsp;<img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit Bid'  title='Edit Bid' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>";
        cell += "</div>";
        cell += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>";
        cell += "&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
        return cell;
    }
    else
        return cellvalue;
}
var lastSel;
function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(response) {
    var json = response.responseText;
    var result = eval("(" + json + ")");
    if (result.status == false)
        alert(result.message);
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    $('#jqGrid').trigger("reloadGrid")
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}
