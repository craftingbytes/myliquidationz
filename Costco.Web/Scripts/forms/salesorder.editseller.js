﻿var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    $('input[id$=SoldDate]').datepicker({});
    $('#ddlBuyerList').change(function () {
        FillAddressData();
    })

    //jQuery("#jqGrid").jqGrid({
    //    url: "/SalesOrder/GetBids",
    //    datatype: 'json',
    //    mtype: 'GET',
    //    colNames: ['Id','Buyer_Id', 'Buyer_Name', 'City', 'State', 'Zip', 'Count', 'Amount'],
    //    colModel: [
    //                { name: 'SalesOrderId', index: 'SalesOrderId', width: 40, align: 'Left', sortable: false, hidden: true },
    //                { name: 'Buyer_Id', index: 'Buyer_Id', width: 40, align: 'Left', sortable: false, hidden: true },
    //                { name: 'Buyer_Name', index: 'Buyer_Name', width:120, editable: false, sortable: false, align: 'Left' },
    //                { name: 'Buyer_City', index: 'Buyer_City', width: 120, editable: false, sortable: false, align: 'Left' },
   	//	            { name: 'Buyer_State', index: 'Buyer_State', width: 40, editable: false, sortable: false, align: 'Center' },
   	//	            { name: 'Buyer_Zip', index: 'Buyer_Zip', width: 80, editable: false, sortable: false, align: 'Center' },
    //                { name: 'Bid_Count', index: 'Bid_Count', width: 40, align: 'Center', sortable: false},
    //                { name: 'Amount', index: 'Amount', width: 80, align: 'right', editable: true, hidden: false, sortable: false, formatter: 'number', formatoptions: { decimalPlaces: 2} }
   	//              ],
    //    rowNum: 10000,
    //    rowList: [10, 25, 50, 100],
    //    //pager: jQuery('#pager'),
    //    viewrecords: true,
    //    rownumbers: true,
    //    width: 910,
    //    height: 240
    //});

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;


        var isFocused = false;
        isFocused = false;

        if (!isValid) {
            return false;
        }


        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');

        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    //$("form").attr("action", rootPath + "/SalesOrder/EditSeller");
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });
});



function FillAddressData() {
    var soldToId = $('#ddlBuyerList').val();
    $("#SoldToName").val($("#ddlBuyerList").find("option:selected").text());
    if (soldToId == 0) {
        $('#SoldToAddress').val("");
        if ($('#ddlBuyerState').get(0).options[0].selected != true) {
            $('#ddlBuyerState').get(0).options[0].selected = true;
            $("#ddlBuyerState").trigger("change");
        }
        $('#ddlBuyerCity').val(0);
        $('#SoldToZip').val("");

    } else {
        $('#SoldToAffiliateId').val(soldToId);
        $.ajax({
            url: '/Shipment/GetShipToAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: soldToId },
            async: false,
            success: function (result) {
                $('#SoldToAddress').val(result.address);
                if ($('#ddlBuyerState').val() != result.data) {
                    $('#ddlBuyerState').val(result.state);
                    $('#ddlBuyerState').trigger('change');
                    $('#SoldToStateId').val(result.state);
                }
                $('#ddlBuyerCity').val(result.city);
                $('#SoldToCityId').val(result.city);
                $('#SoldToZip').val(result.zip);
            }
        });
    }
}
function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            if (elem.name == "ddlPickupState")
                $("select#ddlPickupCity").clearSelect();
            else
                $("select#ddlShipToCity").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                            $("select#ddlBuyerCity").fillSelect(result.data);
                    }
                }
            });
        }
    }
}
function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}