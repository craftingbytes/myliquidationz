﻿//var rootPath = window.location.protocol + "//" + window.location.host;

$(document).ready(function () {
    

    jQuery("#jqGrid").jqGrid({
        url: "/ReportList/GetReportList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id_m', 'Group 1', '', 'Group 2', '', 'Group 3', '', 'All', ''],
        colModel: [
                    { name: 'ReportId', index: 'ReportId', width: 10, align: 'Left', sortable: false, hidden: true },
                    { name: 'Caption_1', index: 'Caption_1', width: 150, align: 'Left', editable: false, sortable: false },
                    { name: 'ActionGroup1Nav', index: 'ActionGroup1Nav', sortable: false, formatter: myformatterNav1, title: true, align: 'center', width: 30, editable: false },
                    { name: 'Caption_2', index: 'Caption_2', width: 150, align: 'Left', editable: false, sortable: false },
                    { name: 'ActionGroup1Nav2', index: 'ActionGroup1Nav2', sortable: false, formatter: myformatterNav1, title: true, align: 'center', width: 30, editable: false },
                    { name: 'Caption_3', index: 'Caption_3', width: 150, align: 'Left', editable: false, sortable: false },
                    { name: 'ActionGroup1Nav3', index: 'ActionGroup1Nav3', sortable: false, formatter: myformatterNav1, title: true, align: 'center', width: 30, editable: false },
                    { name: 'Caption_m', index: 'Caption_m', width: 150, align: 'Left', editable: false, sortable: false },
                    { name: 'ActionMenuAdd', index: 'ActionMenuAdd', sortable: false, formatter: myformatterAdd, title: true, align: 'center', width: 60, editable: false }
                   
        ],
        rowNum: 10000,
        //rowList: [10, 25, 50, 100],
        //pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 930,
        height: 500,
        editurl: "PerformAction",
        afterSubmitCell: function (serverStatus, aPostData) {

        },
        gridComplete: function () {
        }

    });


    $("#btnEdit").click(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + '/ReportList/Index';
        window.location.href = url;
    });

});

function myformatterNav1(cellvalue, options, rowObject) {
    if (cellvalue > 0) {
        var url = "<div>";
        url += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:UpdateDisplayOrder(" + cellvalue + ", 1)' href='#' ><img src='/Content/images/icon/sm/Arrow-mini-up-48.png'  alt='Add to Group 1'  title='Add to Group 1'  style='border:none;'/></a>";
        url += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:UpdateDisplayOrder(" + cellvalue + ", 0)' href='#' ><img src='/Content/images/icon/sm/Arrow-mini-down-48.png'  alt='Add to Group 2'  title='Add to Group 2'  style='border:none;'/></a>";
        url += "<div>";
        return url;
    } else {
        return "&nbsp;";
    }



 
}

function myformatterAdd(cellvalue, options, rowObject) {
    var ReportId = rowObject[0];

    if (cellvalue == 0) {
        var url = "<div>";
        url += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:AddReportToGroup(" + ReportId + ", 1)' href='#' ><img src='/Content/images/icon/sm/plus.gif'  alt='Add to Group 1'  title='Add to Group 1'  style='border:none;'/></a>";
        url += "&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:AddReportToGroup(" + ReportId + ", 2)' href='#' ><img src='/Content/images/icon/sm/plus.gif'  alt='Add to Group 2'  title='Add to Group 2'  style='border:none;'/></a>";
        url += "&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:AddReportToGroup(" + ReportId + ", 3)' href='#' ><img src='/Content/images/icon/sm/plus.gif'  alt='Add to Group 3'  title='Add to Group 3'  style='border:none;'/></a>";
        url += "<div>";
        return url;
    } else {
        return "<div><a href='javascript:DeleteReportFromGroup(" + ReportId + ")'><img src='../../Content/images/icon/delete.png'  alt='Delete'  title='Delete Report from Group'  style='border:none;'/> </a>"
    }




}


function AddReportToGroup(reportId, groupId) {
    $.ajax({
        url: "/ReportList/AddReportToGroup",
        dataType: 'json',
        type: "POST",
        data: ({ ReportId: reportId, GroupId: groupId }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");
            }
            else
                alert(result.message);

        }
    });
}

function DeleteReportFromGroup(reportId) {
    $.ajax({
        url: "/ReportList/DeleteReportFromGroup",
        dataType: 'json',
        type: "POST",
        data: ({ ReportId: reportId}),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");
            }
            else
                alert(result.message);

        }
    });
}

function UpdateDisplayOrder(userReportId, direction) {
    $.ajax({
        url: "/ReportList/UpdateDisplayOrder",
        dataType: 'json',
        type: "POST",
        data: ({ UserReportId: userReportId, Direction: direction }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");
            }
            else
                alert(result.message);

        }
    });
}

//function EditList() {
//    if ($("#btnEdit").val() == "Edit") {
//        $("#btnEdit").val("Done");
//        $('#jqGrid').jqGrid('setColProp', 'ActionGroup1Nav', { hidden: false });
//        $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");
//    }
//    else {
//        $("#btnEdit").val("Edit");
//        $('#jqGrid').jqGrid('setColProp', 'ActionGroup1Nav', { hidden: true });
//        $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");
//    }
//}