﻿var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {

    $("#Months").dropdownchecklist({ firstItemChecksAll: false, maxDropHeight: 120, width: 125 });


    jQuery("#jqGrid").jqGrid({
        url: "/Invoice/GetShipmentsList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id', 'ASR', 'Store', 'Date', 'Weight','Action'],
        colModel: [
                    { name: 'ShipmentId', index: 'ShipmentId', width: 25, align: 'Center', sortable: false, hidden: false },
                    { name: 'ASR', index: 'ASR', width: 80, align: 'Left', sortable: false },
                    { name: 'Store', index: 'Store', width: 25, align: 'Center', sortable: false },
   		            { name: 'ReceivedDate', index: 'ReceivedDate', width: 25, align: 'center', sortable: false },
                    { name: 'Weight', index: 'Weight', width: 25, editable: false, sortable: false, align: 'right' },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 25, editable: false }
   	              ],
        rowNum: 100000,
        rowList: [10, 25, 50, 100],
        //pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240
    });


    $("#btnSearch").click(function () {
        Search();
    });

    $("#btnSubmit").button().click(function () {
        $("#btnSubmit").hide();
        //$("form").attr("action", rootPath + "/ProcessingData/Create");
        if (compileGridData()) {
            $("form").submit();
        }
        else {
            $("#btnSubmit").show();
        }
    });
});

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var url = "javascript:DeleteRow(\"" + options.rowId + "\")";
    return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#' onclick='" + url + "' >Remove</a>";
    //    return "<div>" + cellvalue + " <a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='#'  onclick=\"" + url + "\"><img src='../../Content/images/report.png'  alt='Receiving Report'  title='Receiving Report'  style='border:none;'/></a></div>";
    //return "<img src='/Content/images/icon/cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
}

function DeleteRow(rowId) {
    jQuery("#jqGrid").jqGrid('delRowData', rowId);
}


function Search() {
    var year = $("#Year").val();
    //var monthlist = $("#Months option:selected").val();
    var monthlist = $.map($("#Months option:selected"), function (el, i) {
        return $(el).val();
    });
    var months = monthlist.join(", ")
    jQuery("#jqGrid").setGridParam({ postData: { Year: year, Months: months}, page: 1 }).trigger("reloadGrid");

}

function compileGridData() {
    var shipmentIds = $('#jqGrid').jqGrid('getDataIDs');
    var shipmentList = shipmentIds.join(", ");

    /*
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    if (numberOfRecords <= 0)
        return false;
    var lineItems = '';
    for (i = 1; i <= numberOfRecords; i++) {
        var rowData = $("#jqGrid").getRowData(i);

        if (rowData['ShipmentId'] != undefined) {
            lineItems = 'ShipmentId:' + rowData['ShipmentId'];
            $('<input type="hidden" />').attr('name', 'Item_' + i).attr('value', lineItems).appendTo('#hiddenDivId');
        }
    }
    */
    $('<input type="hidden" />').attr('name', 'shipmentList').attr('value', shipmentList).appendTo('#hiddenDivId');
    return true;
    
}


