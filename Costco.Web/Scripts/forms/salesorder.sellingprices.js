﻿var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {

    var colMap = {};
    jQuery("#jqGrid").jqGrid({
        url: "/SalesOrder/GetSellingPricesList",
        datatype: 'json',
        mtype: 'GET',
        scrollrows: true,
        colNames: ['Id', 'SalesOrderId', 'Type', 'Shipment', 'ItemId', 'Description', 'Retail', 'Price', 'Qty','Cost', 'Total','Edit'],
        colModel: [
                    { name: 'SalesOrderItemId', index: 'SalesOrderItemId', editable: true, width: 25, align: 'Left', sortable: false, hidden: true },
                    { name: 'SalesOrderId', index: 'SalesOrderId', width: 25, align: 'Left', sortable: false, hidden: true },
                    { name: 'TypeId', index: 'TypeId', width: 25, align: 'Left', sortable: false, hidden: true, editable:true },
                    { name: 'ShipmentId', index: 'ShipmentId', width: 25, align: 'Left', sortable: false },
   		            { name: 'ItemId', index: 'ItemId', width: 25, align: 'center', sortable: false },
   		            { name: 'Description', index: 'Description', width: 80, editable:false, sortable: false, align: 'Left' },
                    { name: 'Retail', index: 'Retail', width: 30, align: 'Right', editable: false, sortable: false, editrules: { number: true, required: true } },
                    { name: 'Price', index: 'Price', width: 30, align: 'Right', editable: true, sortable: false, editrules: { number: true, required: true } },
                    { name: 'Qty', index: 'Qty', editable: false, width: 20, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Cost', index: 'Cost', width: 30, align: 'Right', editable: false, sortable: false, editrules: { number: true, required: true }
                    },
                    { name: 'Total', index: 'Total', width: 30, align: 'Right', editable: false, sortable: false, editrules: { number: true, required: true } },
                    {
                        name: 'Actions', index: 'Actions', width: 30, editable: false, sortable: false, align: 'center', formatter: ItemFormatter, cellattr: function (rowId, tv, rawObject, cm, rdata) {
                            var _profit = Number(rdata['Total']) - Number(rdata['Cost']);
                            if (_profit < 0) {
                                return ' style="background-color:#FFCCCC"';
                            } else {
                                return ' style="background-color:#FFFFFF"';
                            }
                        }
                    }
        ],
        rowNum: 25,
        rowList: [10, 25, 50, 100, 200],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "/SalesOrder/EditItemSellingPrice",
        loadComplete: function () {

        }

    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });



    $("#btnApplyPct").button().click(function () {


        $("#validationSummary").html("");
        //var isValid = ValidateForm(); //true;


        var isFocused = false;
        isFocused = false;

        //if (!isValid) {
        //    return false;
        //}


        $("#dialog-change").removeClass('ui-icon ui-icon-circle-check');

        $("#dialog-change").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    //$("form").attr("action", rootPath + "/SalesOrder/EditSeller");
                    changeInitialPct();
                    $(this).dialog("close");
                }
            },
            beforeClose: function (event, ui) {
                $("#txtPct").val( $("#hdInitialPct").val());
            }
        });


    });

    $("#btnSell").click(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + "/SalesOrder/Sell?Id=" + $("#hdSalesOrderId").val();
        window.location.href = url;
        //openSellDialog();
    });


    $("#btnAddMore").click(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + "/SalesOrder/Edit?Id=" + $("#hdSalesOrderId").val();
        window.location.href = url;
        //openSellDialog();
    });


    $("#btnViewReport").click(function () {
        showReport('/reports/SalesOrderItemReport.aspx?SalesOrderId=' + $("#hdSalesOrderId").val(), 'SalesOrderItemReport');
    });



});

function ItemFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;


    var cell = "<div id='ED_ActionMenu_" + options.rowId + "'>";
    cell += "&nbsp;&nbsp;<img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/></div>";
    cell += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>";
    cell += "&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return cell;

}


var lastSel;
function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(response) {
    var json = response.responseText;
    var result = eval("(" + json + ")");
    if (result.status == false)
        alert(result.message);
    else {
        $("#SoldAmount").val(result.newAmount);
        $("#jqGrid").jqGrid("setCell", result.rowId, 'Total', result.newTotal);

        var rowData = $("#jqGrid").getRowData(result.rowId);
            
        if ((rowData.Cost * rowData.Qty) > result.newTotal)
            $("#jqGrid").jqGrid("setCell", result.rowId, 'Actions', '', { "background-color": "#FFCCCC" });
        else     
            $("#jqGrid").jqGrid("setCell", result.rowId, 'Actions', '', { "background-color": "#FFFFFF" });

    }

    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    return result.status;
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}


function changeInitialPct() {
    var currentpct = $("#hdInitialPct").val();
    var newpct = $("#txtPct").val();
    var salesOrderId = $("#hdSalesOrderId").val();
    $.ajax({
        url: "/SalesOrder/ChangeInitialPercentage",
        dataType: 'json',
        type: "POST",
        data: ({ salesOrderId: salesOrderId, newPct: newpct }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                $("#SoldAmount").val(result.soldAmount);
                $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");
                $("#hdInitialPct").val(result.newPct);
            }
            else {
                $("#txtPct").val(currentpct);
                alert(result.message);
            }

        }
    });
}

