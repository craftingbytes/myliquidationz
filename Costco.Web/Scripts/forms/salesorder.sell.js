﻿var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    $('input[id$=SoldDate]').datepicker({});


    $('#ddlBuyerList').change(function () {
        FillAddressData();
    });

    $("#btnCancel").button().click(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var isSold = $('#IsSold').val();
        if (isSold == "True")
        {
            var url = rootPath + "/SalesOrder/Index";
            window.location.href = url;
        }
        else
        {
            var url = rootPath + "/SalesOrder/Edit?Id=" + $("#Id").val();
            window.location.href = url;

        }
    });

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;


        var isFocused = false;
        isFocused = false;

        if (!isValid) {
            return false;
        }


        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');

        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    //$("form").attr("action", rootPath + "/SalesOrder/EditSeller");
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });
});



function FillAddressData() {
    var soldToId = $('#ddlBuyerList').val();
    $('#SoldToAffiliateId').val(soldToId);
    $("#SoldToName").val($("#ddlBuyerList").find("option:selected").text());
    if (soldToId == 0) {
        $("#SoldToName").val("");
        $("#SoldToName").prop("readonly", true);
        $('#SoldToAddress').val("");
        $("#SoldToAddress").prop("readonly", true);
        if ($('#ddlBuyerState').get(0).options[0].selected != true) {
            $('#ddlBuyerState').get(0).options[0].selected = true;
            $("#ddlBuyerState").trigger("change");
        }
        $('#ddlBuyerCity').val(0);
        $('#SoldToZip').val("");

    }
    else if (soldToId == 999999) {
        $("#SoldToName").val("");
        $("#SoldToName").prop("readonly", false);
        $('#SoldToAddress').val("");
        $("#SoldToAddress").prop("readonly", false);
        if ($('#ddlBuyerState').get(0).options[0].selected != true) {
            $('#ddlBuyerState').get(0).options[0].selected = true;
            $("#ddlBuyerState").trigger("change");
        }
        $('#ddlBuyerCity').val(0);
        $('#SoldToZip').val("");

    } else {
        $("#SoldToName").prop("readonly", true);
        //$('#SoldToAffiliateId').val(soldToId);
        $.ajax({
            url: '/Shipment/GetShipToAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: soldToId },
            async: false,
            success: function (result) {
                $('#SoldToAddress').val(result.address);
                if ($('#ddlBuyerState').val() != result.data) {
                    $('#ddlBuyerState').val(result.state);
                    $('#ddlBuyerState').trigger('change');
                    $('#SoldToStateId').val(result.state);
                }
                $('#ddlBuyerCity').val(result.city);
                $('#SoldToCityId').val(result.city);
                $('#SoldToZip').val(result.zip);
            }
        });
    }
}
function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            if (elem.name == "ddlPickupState")
                $("select#ddlPickupCity").clearSelect();
            else
                $("select#ddlShipToCity").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                            $("select#ddlBuyerCity").fillSelect(result.data);
                    }
                }
            });
        }
    }
}

function SetStateCity(elem) {
    $('#SoldToStateId').val($('#ddlBuyerState').val());
    $('#SoldToCityId').val($('#ddlBuyerCity').val());

}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}