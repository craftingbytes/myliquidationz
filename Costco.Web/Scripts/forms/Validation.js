﻿function SubmitForm(controllerAction) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var isValid = ValidateForm();
    if (isValid) {
        document.forms[0].action = rootPath + controllerAction;
        document.forms[0].submit();
    }
   
    return false;
}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var type;
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                type = oRequired.getAttribute('type');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || (oRequired.value == "0" && type!="text")) {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"
                           

                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }


                var compareWith = oRequired.getAttribute('comparewith');
                if (bValidated && compareWith != null && $('#' + compareWith) != null && $.trim($('#' + compareWith).val()) != '' && $.trim(oRequired.value) != '') //if match failed
                {
                    var compareOperator = oRequired.getAttribute('compareoperator');
                    var compareType = oRequired.getAttribute('comparetype');
                    if (compareType == 'date') {
                        var d1 = new Date($.trim(oRequired.value));
                        var d2 = new Date($('#' + compareWith).val());
                        if (compareOperator == 'greater') {
                            bValidated = (d1 > d2);
                        }
                        else if (compareOperator == 'greaterorequal') {
                            bValidated = (d1 >= d2);
                        } else if (compareOperator == 'lesser') {
                            bValidated = (d1 < d2);
                        } else if (compareOperator == 'lesserorequal') {
                            bValidated = (d1 <= d2);
                        }
                    }
                    if (!bValidated) {
                        
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('comparemessage') + "</li>";
                    }
                    
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}
