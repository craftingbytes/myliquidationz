﻿//var to store last selected row id
var lastSel;
var inEditMode = false;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;
var inEditMode = false;
var store = "";

$(function () {
    store = $('#txtAffiliateName').val();
    InitGrid();

    $('#ddlRecyclerList').change(function () {
        FillShipToAddressData();
    })

    $("#btnAdd").button().click(function () {
        //jQuery("#jqGrid").jqGrid('editGridRow',"new",{height:280,reloadAfterSubmit:false});

        addRow();
    });

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;


        var isFocused = false;
        isFocused = false;

        if (!isValid) {
            return false;
        }


        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');
        //if (!isAllSaved()) {
        //    return false;
        //}
        if (inEditMode) {
            alert('There is an item opened in edit mode. Please save or cancel it first.');
            return false;
        }
        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    $("form").attr("action", rootPath + "/Shipment/EditShipper");
                    //compileGridData();
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });

    $('input[id$=txtShipDate]').datepicker({});
    //$('input[id$=txtShipDate]').datepicker('setDate', new Date());

    $("#ddlRecyclerList").hide();
    $("#ddlShipToState").hide();
    $("#txtShipToState").val($("#ddlShipToState :selected").text());
    $("#ddlShipToCity").hide();
    $("#txtShipToCity").val($("#ddlShipToCity :selected").text());
    $("#ddlCarrierList").hide();
    $("#txtCarrier").val($("#ddlCarrierList :selected").text());


});

function InitGrid() {
    $("#jqGrid").jqGrid({
        //data: mydata,
        datatype: 'json',
        mtype: 'GET',
        url: rootPath + '/Shipment/GetShipmentItemsShipper',
        height: 150,



        colNames: [ 'ShipmentItemID','Store','Pallet #', 'ItemId', 'Description', 'lbs', 'Qty', 'Condition','Electronics','Disposition',,'Price',, 'Actions'],
        colModel: [
                    { name: 'ShipmentItemID', index: 'ShipmentItemID', editable: true, width: 85, hidden: true },
                    { name: 'Store', index: 'Store', editable: true, width: 80, sortable: false, editrules: { required: false, integer: true }, align: 'right', hidden: (store == 'Costco 1998' || store == 'Costco 1997' ? false : true) },
   		            { name: 'PalletUnitNumber', index: 'PalletUnitNumber', editable: true, width: 40, sortable: false, editrules: { integer: true, required: true }, align: 'right' },
                    { name: 'ItemId', index: 'ItemId', editable: true, width: 200, sortable: false, editrules: { required: true}  
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { getDescription() }} ] }
                    },
   		            { name: 'Description', index: 'Description', editable: true, width: 200, sortable: false, editrules: { required: true} },
                    { name: 'Weight', index: 'Weight', editable: true, width: 100, sortable: false, editrules: { required: true, number: true }, align: 'right' },
   		            { name: 'Qty', index: 'Qty', editable: true, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    {name: 'Condition', index: 'Condition', editable: true, width: 200, editrules: { required: true }, sortable: false, edittype: 'select', formatter: 'select', hidden:true },
                    { name: 'CoveredDevice', index: 'CoveredDevice', editable:true, width: 30, editrules: { required: true }, edittype: 'select', formatter: 'select'},
                    { name: 'DispositionDisplay', index: 'DispositionDisplay', editable: false, width: 200, editrules: { required: true }, sortable: false, edittype: 'select', formatter: 'select' },
                    { name: 'ItemsShipmentId', index: 'ItemsShipmentId', editable: true, width: 85, hidden: true },
                    { name: 'Price', index: 'Price', editable: (store == 'Costco 1998' || store == 'Costco 1997' ? true : false), width: 80, sortable: false, editrules: { required: false, number: true }, align: 'right', hidden: (store == 'Costco 1998' || store == 'Costco 1997' ? false : true) },
                    { name: 'Disposition', index: 'Disposition', editable: true, width: 10, hidden: true, value:'Salvage' },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],

        rowNum: 100000,
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        //editurl: rootPath + '/Shipment/AddProductItem',
        editurl: rootPath + '/Shipment/EditShipmentItemShipper',
        width: 925,
        height: 300,
        afterInsertRow: function (rowid, aData) {
            be = "<input id='" + rowid + "_edRow' style='height:22px;padding:0' type='button' value='Edit' onclick=\"editRow('" + rowid + "');\" title='Edit Record' />";
            se = "<input id='" + rowid + "_savRow' style='height:22px;padding:0' type='button' value='Save' onclick=\"saveRow('" + rowid + "');\" title='Save Record' />";
            ce = "<input id='" + rowid + "_canRow' style='height:22px;padding:0' type='button' value='Cancel' onclick=\"restoreRow('" + rowid + "');\" title='Cancel Edit' />";
            $("#jqGrid").jqGrid('setRowData', rowid, { Action: be + se + ce });
            $("input[type=button]").button();
            //sumWeight();
        },

        loadComplete: function () {
            //$('#Description').change(popacct); 
            sumWeight();
        }
    });
    loadDropdowns();
}

function loadDropdowns() {
    $('#jqGrid').setColProp('Condition', { editoptions: { value: "Used:Used;Like New:Like New;Damaged:Damaged;Opened/damaged box:Opened/damaged box;Incomplete:Incomplete" } });

    $('#jqGrid').setColProp('CoveredDevice', { editoptions: { value: ":Select;true:Y;false:N" } });

    //$('#jqGrid').setColProp('Condition', { editoptions: { value: "Used:Used;Like New:Like New;Damaged:Damaged;Opened/damaged box:Opened/damaged box;Incomplete:Incomplete" } });

    if ($('#txtAffiliateName').val() == "Costco 1998")
        $('#jqGrid').setColProp('DispositionDisplay', { editoptions: { value: "Recycle:Recycle;Salvage:Salvage"} });
    else
        $('#jqGrid').setColProp('DispositionDisplay', { editoptions: { value: "Salvage:Salvage;Recycle:Recycle" } });

}

function addRow()
{
    jQuery("#jqGrid").jqGrid('editGridRow', 'new', {
        height: 320,
        recreateForm: true,
        reloadAfterSubmit: false,
        addCaption: "Add Record",
        editCaption: "Edit Record",
        bSubmit: "Add",
        bCancel: "Done",
        bClose: "Close",
        saveData: "Data has been changed! Save changes?",
        bYes: "Yes",
        bNo: "No",
        bExit: "Cancel",
        modal: true,
        addedrow: 'last',
        closeAfterEdit: false,
        url: rootPath + '/Shipment/EditShipmentItemShipper',
        afterSubmit: function (response, postdata) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            //if (result.success)
            //    $('#BulkShipmentItemID').val(result.bulkshipmentId);
            //alert(result.success);
            return [result.success, result.message, result.shipmentId]
        },
        afterShowForm: function (formid) {
            setPalletNumber();
        },
        afterComplete: function (response, postdata, formid) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            $("#jqGrid").jqGrid("setCell", result.shipmentId, 'DispositionDisplay', result.disposition);
            setPalletNumber();
            sumWeight();
            return true;
        },
        viewPagerButtons: false
    });

}

function setPalletNumber() {
    var rowCount = $("#jqGrid").getGridParam('reccount');
    if (rowCount == 0)
    {
        $('#PalletUnitNumber').val(1);
    }
    else 
    {
        var rows = jQuery("#jqGrid").jqGrid('getRowData');
        var rowData = rows[rowCount - 1];
        var palletNumber = rowData.PalletUnitNumber;
        $('#PalletUnitNumber').val(palletNumber);
    }
    $('#ItemsShipmentId').val($('#ShipmentId').val());
    $('#Qty').val(1);
    $('#Disposition').val("Salvage");
    
}

function getDescription() { 
    var itemId =  $('#ItemId').val();
    if (itemId.length > 6 || isNaN(itemId) || itemId.length < 2)
    {
        itemId = "Misc";
        $('#ItemId').val("Misc");
    }
   
    
    $.ajax({
        url: "/Shipment/GetProductDescription",
        dataType: 'json',
        type: "GET",
        data: ({ itemId: itemId }),
        async: false,
        cache: false,
        success: function (result) {
            $('#Description').val(result.description);
            if (result.weight == 0)
                $('#Weight').val('');
            else
                $('#Weight').val(result.weight);
            if (result.coveredDevice == true)
                $('#CoveredDevice').val("true");
            else if (result.coveredDevice == false)
                $('#CoveredDevice').val("false");
            //$('#Disposition').val("Salvage");
            $('#Price').val(result.price);

        }
    });

    return [true,'']; 
}
function EditRow(rowId) {
    if (rowId && rowId != lastSel) {
        //CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    //$('#jqGrid').editRow(rowId);
    //jQuery("#jqGrid").editGridRow("rowId",);
    $("#jqGrid").jqGrid("setCell", rowId, 'Disposition', "Salvage");
    jQuery("#jqGrid").jqGrid('editGridRow', rowId, {
        height: 320,
        recreateForm: true,
        reloadAfterSubmit: false,
        addCaption: "Add Record",
        editCaption: "Edit Record",
        bSubmit: "Save",
        bCancel: "Cancel",
        bClose: "Close",
        saveData: "Data has been changed! Save changes?",
        bYes: "Yes",
        bNo: "No",
        bExit: "Cancel",
        modal: true,
        closeAfterEdit: true,
        viewPagerButtons: false,
        afterSubmit: function (response, postdata) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            // $("#jqGrid").setRowData(rowId, { Disposition: "Salvage" });
            //$('#Disposition').val("Salvage");
            var newData = $("#jqGrid").getRowData(rowId);
            //$('#Disposition').val("Salvage");
            //if (result.success)
            //    $('#BulkShipmentItemID').val(result.bulkshipmentId);
            //alert(result.success);
            return [result.success, result.message]
        },
        onClose: function (response, postdata) {
            
            CancelRow(rowId);
        },
        afterComplete: function (response, postdata, formid) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            $("#jqGrid").jqGrid("setCell", rowId, 'DispositionDisplay', result.disposition);
            SaveRow(rowId);
        }

    });
    inEditMode = true;
    sumWeight();
}

function SaveRow(rowId) {
    //$('#jqGrid').saveRow(rowId, AfterSaveRow);

    //if ($("#jqGrid").saveRow(rowId, false, 'clientArray')) {
    //    var newData = $("#jqGrid").getRowData(rowId);
    //    if (parseInt(newData["Qty"]).toString() == 'NaN') {
    //        return;
    //    }
    //    else {
            inEditMode = false;
            $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
            $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    //    }
    //}
    sumWeight();
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    //$('#jqGrid').restoreRow(rowId);

    //$.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.ItemId == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
        lastSel = lastSel - 1;
    }
    inEditMode = false;
}

function DeleteRow(rowId){
    //jQuery("#jqGrid").jqGrid('delRowData', rowId);
    //var rowData = $("#jqGrid").getRowData(rowId);
    $.ajax({
        url: "/Shipment/DeleteShipmentItem",
        dataType: 'json',
        type: "POST",
        data: ({ shipmentItemId: rowId }),
        async: false,
        cache: false,
        success: function (result) {
            jQuery("#jqGrid").jqGrid('delRowData', rowId);
        }
    });
    sumWeight();
}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            if (elem.name == "ddlPickupState")
                $("select#ddlPickupCity").clearSelect();
            else
                $("select#ddlShipToCity").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        if (elem.name == "ddlPickupState")
                            $("select#ddlPickupCity").fillSelect(result.data);
                        else
                            $("select#ddlShipToCity").fillSelect(result.data);

                    }
                }
            });
        }
    }
}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}

function FillShipToAddressData() {
    var reccyclerId = $('#ddlRecyclerList').val();
    $("#txtRecyclerName").val($("#ddlRecyclerList").find("option:selected").text());
    if (reccyclerId == 0) {
        $('#txtShipToAddress').val("");
        if ($('#ddlShipToState').get(0).options[0].selected != true) {
            $('#ddlShipToState').get(0).options[0].selected = true;
            $("#ddlShipToState").trigger("change");
        }
        $('#ddlShipToCity').val(0);
        $('#txtShipToZip').val("");
        $('#txtShipToPhone').val("");

    } else {
        $.ajax({
            url: '/Shipment/GetShipToAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: reccyclerId },
            async: false,
            success: function (result) {
                $('#txtShipToAddress').val(result.address);
                if ($('#ddlShipToState').val() != result.data) {
                    $('#ddlShipToState').val(result.state);
                    $("#ddlShipToState").trigger("change");
                }
                $('#ddlShipToCity').val(result.city);
                $('#txtShipToZip').val(result.zip);
            }
        });
    }
}

function compileGridData() {
    //get total rows in you grid
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    var lineItems = '';

     
    var rows = jQuery("#jqGrid").jqGrid('getRowData');
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        if (row.ShipmentItemID == "")
            lineItems =  'ShipmentItemID:0';
        else
            lineItems =  'ShipmentItemID:' + row.ShipmentItemID;
        lineItems += ',PalletUnitNumber:' + row.PalletUnitNumber;
        lineItems += ',ItemId:' + row.ItemId;
        lineItems += ',Description:' + row.Description ;
        lineItems += ',Qty:' + row.Qty;
        lineItems += ',Condition:' + row.Condition;
        lineItems += ',CoveredDevice:' + row.CoveredDevice;
        lineItems += ',Disposition:' + row.Disposition;
        $('<input type="hidden" />').attr('name', 'Item_' + (i + 1) ).attr('value', lineItems).appendTo('#hiddenDivId');
    }
}

function sumWeight() {
    var totalweight = 0;
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    var rows = jQuery("#jqGrid").jqGrid('getRowData');
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        totalweight += parseFloat(row.Weight) * parseFloat(row.Qty);
    }
    $("#txtTotalWeight").val(totalweight);
}