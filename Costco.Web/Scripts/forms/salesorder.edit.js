﻿//var affiliateType = $("#hfAffiliateType").val();
var rootPath = window.location.protocol + "//" + window.location.host;
var currentPosition;
var affiliateId;
$(document).ready(function () {
    currentPosition = 0;
    affiliateId = $("#hdAffiliateId").val();
    //$("button, input:submit, input:button").button();



    jQuery("#jqGrid").jqGrid({
        url: "/SalesOrder/GetSalesItemList",
        datatype: 'json',
        mtype: 'GET',
        scrollrows: true,
        colNames: ['Id', 'SalesOrderId', 'Type', 'Shipment', 'ItemId', 'Description', 'Qty', 'TypeId', 'Disposition', 'Comments', 'SaleComments', 'Price','Total', 'Actions'],
        colModel: [
                    { name: 'SalesOrderItemId', index: 'SalesOrderItemId', editable: true, width: 25, align: 'Left', sortable: false, hidden: true },
                    { name: 'SalesOrderId', index: 'SalesOrderId', width: 25, align: 'Left', sortable: false, hidden: true },
                    { name: 'Type', index: 'Type', width: 25, align: 'Left', sortable: false, hidden: false },
                    { name: 'ShipmentId', index: 'ShipmentId', width: 25, align: 'Left', sortable: false },
   		            { name: 'ItemId', index: 'ItemId', width: 25, align: 'center', sortable: false },
   		            { name: 'Description', index: 'Description', width: 80, editable: (affiliateId == 533 ? true : false), sortable: false, align: 'Left' },
                    { name: 'Qty', index: 'Qty', editable: true, width: 20, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'TypeId', index: 'TypeId', width: 40, align: 'Left', editable: true, sortable: false, hidden: true },
                    { name: 'Disposition', index: 'Disposition', width: 30, editable: false, sortable: false, align: 'Left' },
                    { name: 'Comments', index: 'Comments', width: 60, editable: false, sortable: false, align: 'Left' },
                    { name: 'SaleComment', index: 'SaleComment', width: 80, editable: true, sortable: false, align: 'Left', editoptions: { dataEvents: [{ type: 'change', fn: function (e) { getCondition(e) } }]} },
                    { name: 'Price', index: 'Price', width: 30, align: 'Right', editable: true, sortable: false, editrules: { number: true, required: true } },
                    { name: 'Total', index: 'Total', width: 30, align: 'Right', editable: false, sortable: false, editrules: { number: true, required: true } },
                    { name: 'Actions', index: 'Actions', width: 70, editable: false, sortable: false, align: 'center', formatter: SalesItemFormatter }
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100,200],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "/SalesOrder/EditSalesOrderItem",
        loadComplete: function () {
            setFocus(currentPosition);
        }
    });

    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    $("button, input:submit, input:button").button();

    $("#txtBeginDate").datepicker();
    $("#txtEndDate").datepicker();

    var data = eval('(' + $("#Data").val() + ')');
    var BeginId = '';
    if (data.BeginId) {
        BeginId = data.BeginId;
        $("#txtBeginId").val(data.BeginId);
    }
    else {
        $("#txtBeginId").val(BeginId);
    }
    var EndId = '';
    if (data.EndId) {
        EndId = data.EndId;
        $("#txtEndId").val(data.EndId);
    }
    else {
        $("#txtEndId").val(EndId);
    }
    var BeginDate = '';
    if (data.BeginDate) {
        BeginDate = data.BeginDate;
        $("#txtBeginDate").val(data.BeginDate);
    }
    var EndDate = '';
    if (data.EndDate) {
        EndDate = data.EndDate;
        $("#txtEndDate").val(data.EndDate);
    }

    var shipmentType = 'All';
    if (data.ShipmentType) {
        shipmentType = data.ShipmentType;
        $("#shipmentType").val(data.ShipmentType);
    }


    var page = 1;
    var rows = 10;
    if (data.page) {
        page = data.page;
    }
    if (data.rows) {
        rows = data.rows;
    }


    jQuery("#jqGrid2").jqGrid({
        url: "/SalesOrder/GetShipmentsList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Type', 'Id', 'Received Date', 'Reports', 'Actions'],
        colModel: [
                    { name: 'Type', index: 'Type', width: 25, align: 'Left', sortable: false, hidden: false },
                    { name: 'Shipment', index: 'Shipment', width: 40, align: 'Left', sortable: false },
   		            { name: 'ReceivedDate', index: 'ReceivedDate', width: 80, align: 'center', sortable: false },
                    { name: 'ShippingData', index: 'ShipmentId', width: 80, editable: false, sortable: false, align: 'center', formatter: reportShippingFmatter },
        //{ name: 'Actions', index: 'Actions', width: 70, editable: false, sortable: false, align: 'center'}   //, formatter: actionFmatter },
                    {name: 'Add', index: 'Add', sortable: false, width: 30, align: 'center', formatter: AddShipmentFormatter }
   	              ],
        rowNum: rows,
        page: page,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager2'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        postData: { BeginDate: BeginDate, EndDate: EndDate, BeginId: BeginId, EndId: EndId, ShipmentType: shipmentType }
    });



    $("#jqGrid2").jqGrid('navGrid', '#pager2',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });


    $("#btnSearch").click(function () {
        Search();
    });

    $("#btnSell").click(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + "/SalesOrder/Sell?Id=" + $("#hdSalesOrderId").val();
        window.location.href = url;
        //openSellDialog();
    });


    $("#btnSetPrices").click(function () {
        var rootPath = window.location.protocol + "//" + window.location.host;
        var url = rootPath + "/SalesOrder/SellingPrices?Id=" + $("#hdSalesOrderId").val();
        window.location.href = url;
        //openSellDialog();
    });


    $("#btnViewReport").click(function () {
        showReport('/reports/SalesOrderItemReport.aspx?SalesOrderId=' +  $("#hdSalesOrderId").val() , 'SalesOrderItemReport');
    });

    $("#btnSetWeight").click(function () {
        openWeightDialog();
    });


    jQuery("#jqgridShipmentItems").jqGrid({
        url: "/SalesOrder/GetShipmentItemsToPull",
        datatype: 'json',
        mtype: 'GET',
        colNames: [
                    'Shipment',
                    'ShipmentItemID',
                    'ItemId',
                    'Description',
                    'Dispostion',
                    'Condition',
                    'Comment',
                    'CED',
                    'Qty',
                    'Available',
                    'Pulled',
                    'Actions'
        ],
        colModel: [
                    { name: 'ShipmentId', index: 'ShipmentId', editable: false, width: 40, hidden: false },
                    { name: 'ShipmentItemID', index: 'ShipmentItemID', editable: true, width: 60, hidden: true },
                    { name: 'ItemId', index: 'ItemId', editable: false, width: 40, sortable: false, editrules: { required: true } },
   		            { name: 'Description', index: 'Description', editable: false, width: 125, sortable: false, editrules: { required: true } },
                    { name: 'Disposition', index: 'Disposition', editable: false, width: 60, sortable: false, editrules: { required: true } },
        //{ name: 'QtyReceived', index: 'QtyReceived', editable: false, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'ConditionReceived', index: 'ConditionReceived', editable: false, width: 75, sortable: false, editrules: { required: true } },
                    { name: 'ConditionReceived2', index: 'ConditionReceived2', editable: false, width: 200, sortable: false, editrules: { required: false }, hidden:true},
   		            { name: 'CoveredDevice', index: 'CoveredDevice', editable: false, width: 30, edittype: 'checkbox', formatter: "checkbox", editoptions: { value: "true:false" }, hidden:true },
                    { name: 'QtyReceived', index: 'QtyReceived', editable: false, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Available', index: 'Available', editable: false, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Pulled', index: 'Pulled', editable: true, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Add', index: 'Add', sortable: false, width: 30, align: 'center', formatter: AddShipmentItemFormatter }
        ],
        rowNum: 10000,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pagerItems'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "/PullShipmentItem/EditShipmentItem"
    });

    jQuery("#jqgridBulkItems").jqGrid({
        url: "/SalesOrder/GetBulkShipmentItemsToPull",
        datatype: 'json',
        mtype: 'GET',
        colNames: [
                    'Shipment',
                    'ShipmentItemID',
                    'ItemId',
                    'Description',
                    'Dispostion',
                    'Condition',
                    'Comment',
                    'CED',
                    'Qty',
                    'Available',
                    'Pulled',
                    'Actions'
        ],
        colModel: [
                    { name: 'ShipmentId', index: 'ShipmentId', editable: false, width: 40, hidden: false },
                    { name: 'ShipmentItemID', index: 'ShipmentItemID', editable: true, width: 60, hidden: true },
                    { name: 'ItemId', index: 'ItemId', editable: false, width: 40, sortable: false, editrules: { required: true } },
   		            { name: 'Description', index: 'Description', editable: false, width: 125, sortable: false, editrules: { required: true } },
                    { name: 'Disposition', index: 'Disposition', editable: false, width: 60, sortable: false, editrules: { required: true } },
        //{ name: 'QtyReceived', index: 'QtyReceived', editable: false, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'ConditionReceived', index: 'ConditionReceived', editable: false, width: 75, sortable: false, editrules: { required: true } },
                    { name: 'ConditionReceived2', index: 'ConditionReceived2', editable: false, width: 200, sortable: false, editrules: { required: false }, hidden: true },
   		            { name: 'CoveredDevice', index: 'CoveredDevice', editable: false, width: 30, edittype: 'checkbox', formatter: "checkbox", editoptions: { value: "true:false" }, hidden: true },
                    { name: 'QtyReceived', index: 'QtyReceived', editable: false, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Available', index: 'Available', editable: false, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Pulled', index: 'Pulled', editable: true, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Add', index: 'Add', sortable: false, width: 30, align: 'center', formatter: AddBulkShipmentItemFormatter }

        ],
        rowNum: 10000,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pagerItems'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "/PullShipmentItem/EditShipmentItem"
    });

    jQuery("#jqgridSSOItems").jqGrid({
        url: "/SalesOrder/GetSSOrderItemsToPull",
        datatype: 'json',
        mtype: 'GET',
        colNames: [
                    'SSOrder',
                    'SSOItemId',
                    'ItemId',
                    'Description',
                    //'Dispostion',
                    //'Condition',
                    //'Comment',
                    //'CED',
                    'Qty',
                    'Available',
                    'Pulled',
                    'Actions'
        ],
        colModel: [
                    { name: 'SSOrderId', index: 'SSOrderId', editable: false, width: 40, hidden: false },
                    { name: 'SSOItemId', index: 'SSOItemId', editable: true, width: 60, hidden: true },
                    { name: 'ItemId', index: 'ItemId', editable: false, width: 40, sortable: false, editrules: { required: true } },
   		            { name: 'Description', index: 'Description', editable: false, width: 125, sortable: false, editrules: { required: true } },
                    //{ name: 'Disposition', index: 'Disposition', editable: false, width: 60, sortable: false, editrules: { required: true } },
        //{ name: 'QtyReceived', index: 'QtyReceived', editable: false, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    //{ name: 'ConditionReceived', index: 'ConditionReceived', editable: false, width: 75, sortable: false, editrules: { required: true } },
                    //{ name: 'ConditionReceived2', index: 'ConditionReceived2', editable: false, width: 200, sortable: false, editrules: { required: false }, hidden: true },
   		            //{ name: 'CoveredDevice', index: 'CoveredDevice', editable: false, width: 30, edittype: 'checkbox', formatter: "checkbox", editoptions: { value: "true:false" }, hidden: true },
                    { name: 'QtyReceived', index: 'QtyReceived', editable: false, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Available', index: 'Available', editable: false, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Pulled', index: 'Pulled', editable: true, width: 30, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Add', index: 'Add', sortable: false, width: 30, align: 'center', formatter: AddSSOItemFormatter }

        ],
        rowNum: 10000,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pagerItems'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "/PullShipmentItem/EditShipmentItem"
    });

}); 

function Search() {
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEndDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);

    var beginId = $("#txtBeginId").val();
    var endId = $("#txtEndId").val();

    var shipmentType = $("#shipmentType").val();

    jQuery("#jqGrid2").setGridParam({ postData: { BeginDate: strBeginingDate, EndDate: strEndingDate, BeginId: beginId, EndId: endId, ShipmentType: shipmentType}, page: 1 }).trigger("reloadGrid");

}

function openSellDialog() {
    $("#dialog-sell").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                var minimumValue = $("#txtMinValue").val();
                if (parseInt(minimumValue).toString() == 'NaN') {
                    alert("A valid number is required for Estimated Minimum Value");
                    return;
                }

                var salesOrderId = $("#SalesOrderId").val();
                $.ajax({
                    url: "/SalesOrder/ReadyToSell",
                    dataType: 'json',
                    type: "POST",
                    data: ({ salesOrderId: salesOrderId, minimumValue: minimumValue }),
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result.success == true) {
                            //$(this).dialog("close");
                            var url2 = rootPath + "/SalesOrder/Index";
                            window.location.href = url2;
                        }
                        else {
                            alert(result.message);
                        }
                        //jQuery("#jqGrid").jqGrid('delRowData', rowId);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An error Occurred");
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}

function isInt(value)
{
    var er = /^[0-9]+$/;

    return ( er.test(value) ) ? true : false;
}

function SalesItemFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var salesOrderItemId = rowObject[0];
    var salesOrderId = rowObject[1];
    var shipmentId = rowObject[3];
    var typeId = rowObject[2];

    var cell = "<div id='ED_ActionMenu_" + options.rowId + "'>";
    cell += "&nbsp;&nbsp;<img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>";
    cell += "&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:deleteSalesOrderItem(" + rowid + "," + salesOrderItemId + ",\"" + typeId + "\")' class='delete'/>";
    cell += "&nbsp;&nbsp;<img src='../../Content/images/icon/delete.png' style='cursor:hand;' alt='Delete Shipment'  title='Delete Shipment'  style='border:none;' onclick='javascript:deleteShipment(" + rowid + "," + salesOrderId + "," + shipmentId + ",\"" + typeId + "\")' class='delete'/></div>";
    cell    += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>";
    cell    += "&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return cell;

}

var lastSel;
function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });

    var itemId = $("#jqGrid").jqGrid('getCell', rowId, 'ItemId').toString();
    var price = $("#jqGrid").jqGrid('getCell', rowId, 'Price');
    if (price <= 0 || affiliateId == 533 || itemId == 'Misc') {
        $('#jqGrid').jqGrid('setColProp', 'Price', { editable: true });
    } else {
        $('#jqGrid').jqGrid('setColProp', 'Price', { editable: false });
    }
    $('#jqGrid').editRow(rowId);
}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(response) {
    var json = response.responseText;
    var result = eval("(" + json + ")");
    //if (result.status == false)
    //    alert(result.message);
    if (result.status == true) {
        var rowData = $("#jqgridBulkItems").getRowData(lastSel);
        rowData["Total"] = result.total;
        $("#jqGrid").jqGrid('setRowData', lastSel, rowData);
    }
    else {
        alert(result.message);
    }
    
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    //$('#jqGrid').trigger("reloadGrid")
    return result.status;
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function deleteSalesOrderItem(rowId, salesOrderItemId, typeId) {
    currentPosition = $("#jqGrid").getInd(rowId) - 1;
    //alert("Row " + currentPosition);
    $.ajax({
        url: "/SalesOrder/DeleteSalesOrderItem",
        dataType: 'json',
        type: "POST",
        data: ({ salesOrderItemId: salesOrderItemId, typeId: typeId }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                jQuery("#jqGrid").jqGrid('delRowData', rowId);
                
                $("#jqGrid").trigger("reloadGrid");
                //$("#jqGrid2").setGridParam({ page: 1 }).trigger("reloadGrid");
            }

        }
    });
}

function deleteShipment(rowId, salesOrderId, shipmentId, typeId) {
    currentPosition = 0;
    $.ajax({
        url: "/SalesOrder/DeleteSalesOrderItemsShipment",
        dataType: 'json',
        type: "POST",
        data: ({ salesOrderId: salesOrderId, shipmentId: shipmentId,typeId:typeId }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                jQuery("#jqGrid").jqGrid('delRowData', rowId);
                $("#jqGrid").trigger("reloadGrid");
                $("#jqGrid2").setGridParam({ page: 1 }).trigger("reloadGrid");
            }

        }
    });
}

function reportShippingFmatter(cellvalue, options, rowObject) {
    var typeId = rowObject[0];
    var ShipmentId = rowObject[1];
    var url = "";
    if (typeId == "S")
        url = "return showReport('/reports/ReceivingReport.aspx?ShipmentId=" + ShipmentId + "', 'ReceivingReport');";
    else if (typeId == "B")
        url = "return showReport('/reports/BulkReceivingReport.aspx?ShipmentId=" + ShipmentId + "', 'BulkReceivingReport');";
    else if (typeId == "C")
        url = "return showReport('/Reports/GenericReport.aspx?ReportName=SalvageSalesOrderReport&DisplayName=SalvageSalesOrderReport&SalesOrderId=" + ShipmentId + "', 'SalvageSalesOrderReport');";
    var cell = "<div>";
    cell += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Shipping Report'  title='Shipping Report'  style='border:none;'/></a>";
    cell += "</div>";
    return cell;
}

function AddShipmentFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var typeId = rowObject[0];
    var shipmentId = rowObject[1];
    var salesOrderId = $("#SalesOrderId").val();
    var cell = "<div>";
        
    if (typeId == 'S' || typeId == 'B')
        cell +="<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:editPulledItems(" + rowid + "," + salesOrderId + "," + shipmentId +",\"" + typeId + "\")' href='#' ><img src='../../Content/images/icon/edit.png'  alt='Pull Items'  title='Pull Items'  style='border:none;'/></a>";
    
    if (typeId == 'S')
        cell += "&nbsp;&nbsp;&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:openShipmentItems(" + rowid + "," + shipmentId + ")' href='#' ><img src='../../Content/images/icon/sm/plus1.png'  alt='Add Individual Items'  title='Add Individual Items'  style='border:none;'/></a>";
    else if (typeId == 'C')
        cell += "&nbsp;&nbsp;&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:openSSOItems(" + rowid + "," + shipmentId + ")' href='#' ><img src='../../Content/images/icon/sm/plus1.png'  alt='Add Individual Items'  title='Add Individual Items'  style='border:none;'/></a>";
    else 
        cell += "&nbsp;&nbsp;&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:openBulkItems(" + rowid + "," + shipmentId + ")' href='#' ><img src='../../Content/images/icon/sm/plus1.png'  alt='Add Individual Items'  title='Add Individual Items'  style='border:none;'/></a>";

    cell += "&nbsp;&nbsp;&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:addSalesOrderItem(" + rowid + "," + shipmentId + ",\"" + typeId + "\")' href='#' ><img src='../../Content/images/icon/sm/plus2.png'  alt='Add Entire Shipment '  title='Add Entire Shipment'  style='border:none;'/></a>";

    cell += "</div>";
    return cell;
}

function addSalesOrderItem(rowId, shipmentId, typeId) {
    $.ajax({
        url: "/SalesOrder/AddSalesOrderItems",
        dataType: 'json',
        type: "POST",
        data: ({ shipmentId: shipmentId, typeId: typeId }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                jQuery("#jqGrid2").jqGrid('delRowData', rowId);
                $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");
            }
            else
                alert(result.message);

        }
    });
}
function editPulledItems(rowId, salesOrderId, shipmentId,typeId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    //PullShipmentItem/Index?salesOrderId=" + salesOrderId + "&shipmentId=" + shipmentId + "
    var url = "";
    if (typeId == "S")
        url = rootPath + "/PullShipmentItem/Index?salesOrderId=" + salesOrderId + "&shipmentId=" + shipmentId;
    else if (typeId == "B")
        url = rootPath + "/PullShipmentItem/BulkIndex?salesOrderId=" + salesOrderId + "&shipmentId=" + shipmentId;
    var BeginDate = $("#jqGrid2").getGridParam("postData").BeginDate;
    var EndDate = $("#jqGrid2").getGridParam("postData").EndDate;
    var BeginId = $("#jqGrid2").getGridParam("postData").BeginId;
    var EndId = $("#jqGrid2").getGridParam("postData").EndId;
    var ShipmentType = $("#jqGrid2").getGridParam("postData").ShipmentType;
    var page = $("#jqGrid2").getGridParam("page");
    var rows = $("#jqGrid2").getGridParam("rowNum");


    url = url + '&data=BeginDate:"' + BeginDate + '",EndDate:"' + EndDate + '",BeginId:"' + BeginId + '",EndId:"' + EndId + '",ShipmentType:"' + ShipmentType + '",page:"' + page + '",rows:"' + rows + '"';
    window.location.href = url;
}

function getCondition(e) {
    condition = $(e.target).val();
    if (condition == "1")
        $(e.target).val("Tested OK.");
    else if (condition == "2")
        $(e.target).val("Out of box.");
    else if (condition == "3")
        $(e.target).val("In box.");
    else if (condition == "4")
        $(e.target).val("Parts Only.");

}

function openWeightDialog() {
    $("#dialog-weights").dialog({
        modal: true,
        width: 500,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                var pallets = $("#TotalPallets").val();
                if (parseInt(pallets).toString() == 'NaN') {
                    alert("A valid number is required for Pallets!");
                    return;
                }
                var weight = $("#TotalWeight").val();
                if (parseInt(weight).toString() == 'NaN') {
                    alert("A valid number is required for Weight!");
                    return;
                }


                var salesOrderId = $("#SalesOrderId").val();
                var comments = $("#Comment").val();
                $.ajax({
                    url: "/SalesOrder/UpdateWeightPallets",
                    dataType: 'json',
                    type: "POST",
                    data: ({ salesOrderId: salesOrderId, pallets: pallets, weight: weight, comments: comments }),
                    async: false,
                    cache: false,
                    success: function (result) {
                        if (result.success == true) {
                            alert("Weight and Pallets set.");
                        }
                        else {
                            alert(result.message);
                            $("#TotalPallets").val("");
                            $("#TotalWeight").val("");
                            $("#Comment").val("");
                        }
                        //jQuery("#jqGrid").jqGrid('delRowData', rowId);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An error Occurred");
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}

function setFocus(current_row) {
    ids = $("#jqGrid").jqGrid("getDataIDs");
    if (current_row <= 0)
        return;
    else if (ids && ids.length >= current_row) {
        $("#jqGrid").setSelection(ids[current_row]);
        //jQuery("#jqGrid").closest(".ui-jqgrid-bdiv").scrollTop(ids[current_row]);
    }
    else if (ids) {
        $("#jqGrid").setSelection(ids[ids.length - 1]);
    }



}


function openShipmentItems(shipmentId) {
    jQuery("#jqgridShipmentItems").setGridParam({ postData: { shipmentId: shipmentId }, page: 1 }).trigger("reloadGrid");
    $("#dialog-ShipmentItems").dialog({
        modal: true,
        width: 910,
        resizable: false,
        position: { my: 'center', at: 'center', of: window },
        buttons: {
            "Close": function () {
                window.location = '#';
                $(this).dialog("close");
            }
        },
        beforeClose: function (event, ui) {
            var BeginDate = $("#jqGrid2").getGridParam("postData").BeginDate;
            var EndDate = $("#jqGrid2").getGridParam("postData").EndDate;
            var BeginId = $("#jqGrid2").getGridParam("postData").BeginId;
            var EndId = $("#jqGrid2").getGridParam("postData").EndId;
            var ShipmentType = $("#jqGrid2").getGridParam("postData").ShipmentType;
            var page = $("#jqGrid2").getGridParam("page");
            var rows2 = $("#jqGrid2").getGridParam("rowNum");
            jQuery("#jqGrid2").setGridParam({ postData: { BeginDate: BeginDate, EndDate: EndDate, BeginId: BeginId, EndId: EndId, ShipmentType: ShipmentType }, page: page, rowNum: rows2 }).trigger("reloadGrid");

            var rows = $("#jqGrid").getGridParam("rowNum");
            $("#jqGrid").setGridParam({ page: 1}).trigger("reloadGrid");
            
 
        }
    });
}

function openBulkItems(shipmentId) {
    jQuery("#jqgridBulkItems").setGridParam({ postData: { shipmentId: shipmentId }, page: 1 }).trigger("reloadGrid");
    $("#dialog-BulkItems").dialog({
        modal: true,
        width: 910,
        resizable: false,
        position: { my: 'center', at: 'center', of: window },
        buttons: {
            "Close": function () {
                window.location = '#';
                $(this).dialog("close");
            }
        },
        beforeClose: function (event, ui) {
            var BeginDate = $("#jqGrid2").getGridParam("postData").BeginDate;
            var EndDate = $("#jqGrid2").getGridParam("postData").EndDate;
            var BeginId = $("#jqGrid2").getGridParam("postData").BeginId;
            var EndId = $("#jqGrid2").getGridParam("postData").EndId;
            var ShipmentType = $("#jqGrid2").getGridParam("postData").ShipmentType;
            var page = $("#jqGrid2").getGridParam("page");
            var rows2 = $("#jqGrid2").getGridParam("rowNum");
            jQuery("#jqGrid2").setGridParam({ postData: { BeginDate: BeginDate, EndDate: EndDate, BeginId: BeginId, EndId: EndId, ShipmentType: ShipmentType }, page: page, rowNum: rows2 }).trigger("reloadGrid");

            var rows = $("#jqGrid").getGridParam("rowNum");
            $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");


        }
    });
}

function openSSOItems(ssOrderId) {
    jQuery("#jqgridSSOItems").setGridParam({ postData: { ssOrderId: ssOrderId }, page: 1 }).trigger("reloadGrid");
    $("#dialog-SSOrderItems").dialog({
        modal: true,
        width: 910,
        resizable: false,
        position: { my: 'center', at: 'center', of: window },
        buttons: {
            "Close": function () {
                window.location = '#';
                $(this).dialog("close");
            }
        },
        beforeClose: function (event, ui) {
            var BeginDate = $("#jqGrid2").getGridParam("postData").BeginDate;
            var EndDate = $("#jqGrid2").getGridParam("postData").EndDate;
            var BeginId = $("#jqGrid2").getGridParam("postData").BeginId;
            var EndId = $("#jqGrid2").getGridParam("postData").EndId;
            var ShipmentType = $("#jqGrid2").getGridParam("postData").ShipmentType;
            var page = $("#jqGrid2").getGridParam("page");
            var rows2 = $("#jqGrid2").getGridParam("rowNum");
            jQuery("#jqGrid2").setGridParam({ postData: { BeginDate: BeginDate, EndDate: EndDate, BeginId: BeginId, EndId: EndId, ShipmentType: ShipmentType }, page: page, rowNum: rows2 }).trigger("reloadGrid");

            var rows = $("#jqGrid").getGridParam("rowNum");
            $("#jqGrid").setGridParam({ page: 1 }).trigger("reloadGrid");


        }
    });
}


function AddShipmentItemFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var cell = "<div>";
    cell += "&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:addShipmentItem(" + rowid + ", false)' href='#' ><img src='../../Content/images/icon/sm/plus1.png'  alt='Add One'  title='Add One'  style='border:none;'/></a>";
    cell += "&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:addShipmentItem(" + rowid + ", true)' href='#' ><img src='../../Content/images/icon/sm/plus2.png'  alt='Add Entire Qty'  title='Add Entire Qty'  style='border:none;'/></a>";
    cell += "</div>";
    return cell;
}

function addShipmentItem(rowId, allItems) {
    var rowData = $("#jqgridShipmentItems").getRowData(rowId);
    $.ajax({
        url: "/SalesOrder/AddSalesOrderItem",
        dataType: 'json',
        type: "POST",
        data: ({ shipmentItemId: rowId, AllItems: allItems }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                if (result.available > 0) {
                    rowData["QtyReceived"] = result.qty;
                    rowData["Available"] = result.available;
                    rowData["Pulled"] = result.pulled;
                    $("#jqgridShipmentItems").jqGrid('setRowData', rowId, rowData);
                } else {
                    jQuery("#jqgridShipmentItems").jqGrid('delRowData', rowId);
                }

            }
            else
                alert(result.message);

        }
    });
}

function AddBulkShipmentItemFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var cell = "<div>";
    cell += "&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:addBulkShipmentItem(" + rowid + ", false)' href='#' ><img src='../../Content/images/icon/sm/plus1.png'  alt='Add One'  title='Add One'  style='border:none;'/></a>";
    cell += "&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:addBulkShipmentItem(" + rowid + ", true)' href='#' ><img src='../../Content/images/icon/sm/plus2.png'  alt='Add Entire Qty'  title='Add Entire Qty'  style='border:none;'/></a>";
    cell += "</div>";
    return cell;
}

function addBulkShipmentItem(rowId, allItems) {
    var rowData = $("#jqgridBulkItems").getRowData(rowId);
    $.ajax({
        url: "/SalesOrder/AddSalesOrderBulkItem",
        dataType: 'json',
        type: "POST",
        data: ({ shipmentItemId: rowId, AllItems: allItems }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                if (result.available > 0) {
                    rowData["QtyReceived"] = result.qty;
                    rowData["Available"] = result.available;
                    rowData["Pulled"] = result.pulled;
                    $("#jqgridBulkItems").jqGrid('setRowData', rowId, rowData);
                } else {
                    jQuery("#jqgridBulkItems").jqGrid('delRowData', rowId);
                }

            }
            else
                alert(result.message);

        }
    });
}

function AddSSOItemFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var cell = "<div>";
    cell += "&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:addSSOItem(" + rowid + ", false)' href='#' ><img src='../../Content/images/icon/sm/plus1.png'  alt='Add One'  title='Add One'  style='border:none;'/></a>";
    cell += "&nbsp;<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:addSSOItem(" + rowid + ", true)' href='#' ><img src='../../Content/images/icon/sm/plus2.png'  alt='Add Entire Qty'  title='Add Entire Qty'  style='border:none;'/></a>";
    cell += "</div>";
    return cell;
}

function addSSOItem(rowId, allItems) {
    var rowData = $("#jqgridSSOItems").getRowData(rowId);
    $.ajax({
        url: "/SalesOrder/AddSalesOrderSSOItem",
        dataType: 'json',
        type: "POST",
        data: ({ shipmentItemId: rowId, AllItems: allItems }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.status == true) {
                if (result.available > 0) {
                    rowData["QtyReceived"] = result.qty;
                    rowData["Available"] = result.available;
                    rowData["Pulled"] = result.pulled;
                    $("#jqgridSSOItems").jqGrid('setRowData', rowId, rowData);
                } else {
                    jQuery("#jqgridSSOItems").jqGrid('delRowData', rowId);
                }

            }
            else
                alert(result.message);

        }
    });
}
