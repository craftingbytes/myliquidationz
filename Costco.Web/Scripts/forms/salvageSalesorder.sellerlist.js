﻿var affiliateType = $("#hfAffiliateType").val();
var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    $("button, input:submit, input:button").button();


    $("#txtBeginDate").datepicker();
    $("#txtEngingDate").datepicker();



    jQuery("#jqGrid").jqGrid({
        url: "/SalvageSales/GetSalvageSalesOrderList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id', 'Created', 'SoldDate', 'Sold To', 'SoldToAffiliateId',  'Amount' ,'Action'],
        colModel: [
                    { name: 'SalvageSalersOrderId', index: 'SalvageSalersOrderId', width: 40, align: 'center', sortable: false },
   		            { name: 'Created', index: 'Created', width: 80, align: 'center', sortable: false },
                    { name: 'SoldDate', index: 'SoldDate', width: 80, align: 'center', sortable: false },
                    { name: 'SoldTo', index: 'SoldTo', width: 120, align: 'Left', sortable: false, hidden: false },
                    { name: 'SoldToAffiliateId', index: 'SoldToAffiliateId', width: 120, align: 'Left', sortable: false, hidden: true },
                    { name: 'Amount', index: 'Amount', width: 80, align: 'right', editable: true, hidden: false, sortable: false, formatter: 'number', formatoptions: { decimalPlaces: 2} },
                    { name: 'Edit', index: 'Edit', width: 80, align: 'right', editable: false, sortable: false, align: 'center', formatter: fmatter }
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240



    });

    $("#jqGrid").jqGrid('navGrid', '#pager2',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    $("#btnSearch").click(function () {
        Search();
    });


});

function Search() {
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEngingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);
    var beginId = $("#txtBeginId").val();
    var endId = $("#txtEndId").val();

    jQuery("#jqGrid").setGridParam({ postData: { BeginDate: strBeginingDate, EndDate: strEndingDate, BeginId: beginId, EndId: endId }, page: 1 }).trigger("reloadGrid");

}

//function reportFmatter(cellvalue, options, rowObject) {
//    var salesOrderId = rowObject[0];
//    var cell = "<div>";
//    var url = "return showReport('../reports/SalesOrderItemReport.aspx?SalesOrderId=" + salesOrderId + "', 'SalesOrderItemReport');";
//    cell += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Sales Order Report'  title='Sales Order Report'  style='border:none;'/></a>";
//    cell += "</div>";
//    return cell;
//}

function fmatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var cell = "<div>";
    if (cellvalue == false)
        cell = "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:editSalesOrder(" + rowid + ")' href='#' ><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit'  style='border:none;'/></a>";
    //if (cellvalue == "Open")
    //    cell = "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:editSalesOrder(" + rowid + ")' href='#' ><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit'  style='border:none;'/></a>";
    //else
    //    cell = "&nbsp;";
    var url = "return showReport('/Reports/GenericReport.aspx?ReportName=SalvageSalesOrderReport&DisplayName=SalvageSalesOrderReport&SalesOrderId=" + rowid + "', 'SalvageSalesOrderReport');";
    cell += "&nbsp;" + "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='/Content/images/report.png'  alt='Sales Order Report'  title='Sales Order Report'  style='border:none;'/></a>";
    cell += "</div>"
    return cell;
}

function editSalesOrder(salesOrderId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + "/SalvageSales/EditSeller?Id=" + salesOrderId;
    window.location.href = url;
}




$('input[id$=btnAdd]').click(function () {
    $("#dialog-choice").dialog({
        modal: true,
        width: 400,
        resizable: false,
        position: 'center',
        buttons: {
            "No": function () {
                window.location = '#';
                $(this).dialog("close");
            },
            "Yes": function () {
                $("form").attr("action", rootPath + "/SalvageSales/Create");
                $("form").submit();
                $(this).dialog("close");
            }
        }
    });
});

