﻿var rootPath = window.location.protocol + "//" + window.location.host;
var affiliateType = $("#hfAffiliateType").val();
$(document).ready(function () {

    $("#txtBeginDate").datepicker();
    //$('#txtBeginDate').datepicker('setDate', new Date());
    $("#txtEngingDate").datepicker();

    $("#txtUpdateDate").datepicker();

    jQuery("#jqGrid").jqGrid({
        url: "/Invoice/GetCostcoBillsList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id', 'Name', 'Date', 'Weight', 'Total', 'Action', 'Notes'],
        colModel: [
                    { name: 'InvoiceId', index: 'InvoiceId', width: 25, align: 'Center', sortable: false, hidden: false },
                    { name: 'Name', index: 'Name', width: 80, align: 'Left', sortable: false },
   		            { name: 'InvoiceDate', index: 'InvoiceDate', width: 25, align: 'center', sortable: false },
                    { name: 'Weight', index: 'Weight', width: 25, editable: false, sortable: false, align: 'right' },
                    { name: 'Total', index: 'Total', width: 25, editable: false, sortable: false, align: 'right' },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 25, editable: false },
                    { name: 'Notes', index: 'Notes', width: 25, align: 'Center', sortable: false, hidden: true }
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240
    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    $("#btnSearch").click(function () {
        Search();
    });


    $("#dlgAdjustment").dialog({
        modal: true,
        width: 800,
        height: 240,
        zIndex: 500,
        resizable: false,
        position: 'center',
        autoOpen: false,
        show: 'fold',
        hide: 'fold',
        Ok: function () {
            $(this).dialog('close');
        }
    });

    $("#dlgAdjBtnCancel").click(function () {
        $('#dlgAdjustment').dialog('close'); ;
        $("#txtAdjDesc").val('');
        $("#txtAdjAmount").val('');

    });

    $("#dlgAdjBtnSave").click(function () {
        AddAdjustment();
    });



    $("#dlgUpdate").dialog({
        modal: true,
        width: 600,
        height: 400,
        zIndex: 500,
        resizable: false,
        position: 'center',
        autoOpen: false,
        show: 'fold',
        hide: 'fold',
        Ok: function () {
            $(this).dialog('close');
        }
    });

    $("#dlgUpdateBtnCancel").click(function () {
        $('#dlgUpdate').dialog('close'); ;
        $("#txtUpdateDate").val('');
        $("#txtUpdateNotes").val('');

    });

    $("#dlgUpdateBtnSave").click(function () {
        UpdateInvoice();
    });

    if (affiliateType == 1) {
        $("#btnAdd").show();
    }
    else {
        $("#btnAdd").hide();
    }
});

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var notes = rowdata[6];
    var invoicedate = rowdata[2];
    var url = "return showReport('/Reports/GenericReport.aspx?ReportName=InvoiceCostcoBill&DisplayName=InvoiceCostcoBill&InvoiceId=" + options.rowId + "', 'InvoiceCostcoBill');";
    var url2 = "return showReport('/Reports/GenericReport.aspx?ReportName=InvoiceCostcoBillDetail&DisplayName=InvoiceCostcoBillDetail&InvoiceId=" + options.rowId + "', 'InvoiceCostcoBillDetail');";
    //var url = "return showReport('/Reports/InvoiceCostcoBill.aspx?InvoiceID=" + options.rowId + "', 'InvoiceCostcoBill');";
    var cellstring = "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='/Content/images/report.png'  alt='Invoice'  title='Invoice'  style='border:none;'/></a>&nbsp;&nbsp;&nbsp;";
    cellstring += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url2 + "\" href='#' ><img src='/Content/images/report.png'  alt='Detail'  title='Detail'  style='border:none;'/></a>&nbsp;&nbsp;&nbsp;";
    if (affiliateType == 1) {
        cellstring += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:displayUpdate(\"" + options.rowId + "\",\"" + notes + "\",\"" + invoicedate + "\" )' href='#' ><img src='/Content/images/icon/edit.png'  alt='Edit'  title='Edit'  style='border:none;'/></a>&nbsp;&nbsp;&nbsp;";
        cellstring += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:displayAddAdjustment(\"" + options.rowId + "\")' href='#' ><img src='/Content/images/icon/sm/plus.gif'  alt='Add Adjustment'  title='Add Adjustment'  style='border:none;'/></a>";
    }
    cellstring += "</div>";
    return cellstring;
}

function DeleteRow(rowId) {
    jQuery("#jqGrid").jqGrid('delRowData', rowId);
}


function Search() {
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEngingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);

    var beginId = $("#txtBeginId").val();
    var endId = $("#txtEndId").val();

    jQuery("#jqGrid").setGridParam({ postData: { BeginDate: strBeginingDate, EndDate: strEndingDate, BeginId: beginId, EndId: endId }, page: 1 }).trigger("reloadGrid");

}

$('input[id$=btnAdd]').click(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/Invoice/Create';
    window.location.href = url;
});

function displayAddAdjustment(invoiceNumber) {
    $("#currentInvoice").val(invoiceNumber);
    $("#txtAdjDesc").val('');
    $("#txtAdjAmount").val('');
    $("#dlgAdjustment").dialog('open');
}


function AddAdjustment() {
    if (!ValidateAdjDetailInputs()) {
        return;
    }
    var invoiceId = $("#currentInvoice").val();
    var adjDesc = $("#txtAdjustmentDesc").val();
    var adjAmount = $("#txtAdjustmentAmount").val();
    var fAdjAmount = parseFloat(adjAmount);
    $.ajax({
        url: '/Invoice/AddAdustment',
        dataType: 'json',
        type: "POST",
        data: ({ InvoiceId: invoiceId, Description: adjDesc, Amount: fAdjAmount }),
        async: false,
        success: function (result) {
            if (result.message == "AccessDenied") {
                var rootPath = window.location.protocol + "//" + window.location.host;
                var url = rootPath + '/Home/AccessDenied';
                window.location.href = url;
            }
            else if (result.status == "True") {
                if (result.message != '') {
                    alert(result.message);
                    $('#dlgAdjustment').dialog('close');
                    $("#txtAdjustmentDesc").val('');
                    $("#txtAdjustmentAmount").val('');
                }
                jQuery("#jqGrid").trigger("reloadGrid");
            } else {
                alert(result.message);
            }
        }
    });
}

function ValidateAdjDetailInputs() {
    var invoiceId = $("#currentInvoice").val();
    var collectorAdjDesc = $("#txtAdjustmentDesc").val();
    var adjAmount = $("#txtAdjustmentAmount").val();
    var fAdjAmount = parseFloat(adjAmount);
    var errIndex = 0;
    var errMsg = '';
    if (collectorAdjDesc < 1) {
        errIndex++;
        errMsg += errIndex + ". Description is required.\n";
    }
    if (isNaN(fAdjAmount)) {
        errIndex++;
        errMsg += errIndex + ". Amount not a valid Quantity.\n";
    }
    if (errMsg != '') {
        alert(errMsg);
        return false;
    }
    return true;
}

function displayUpdate(invoiceNumber, notes, invoicedate) {
    $("#currentInvoice2").val(invoiceNumber);
    $("#txtUpdateDate").val(invoicedate);
    $("#txtUpdateNotes").val(notes);
    $("#dlgUpdate").dialog('open');
}

function UpdateInvoice() {
    var invoiceId = $("#currentInvoice2").val();
    var updateNotes = $("#txtUpdateNotes").val();
    var updateDate = $("#txtUpdateDate").val();
    $.ajax({
        url: '/Invoice/UpdateInvoice',
        dataType: 'json',
        type: "POST",
        data: ({ InvoiceId: invoiceId, Notes: updateNotes, InvoiceDate: updateDate }),
        async: false,
        success: function (result) {
            if (result.message == "AccessDenied") {
                var rootPath = window.location.protocol + "//" + window.location.host;
                var url = rootPath + '/Home/AccessDenied';
                window.location.href = url;
            }
            else if (result.success == true) {
                alert("Invoice updated");
                $('#dlgUpdate').dialog('close');
                $("#txtUpdateDate").val('');
                $("#txtUpdateNotes").val('');
                jQuery("#jqGrid").trigger("reloadGrid");
            } else {
                alert(result.message);
            }
        }
    });
}