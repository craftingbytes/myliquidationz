﻿//var to store last selected row id
var lastSel;
var inEditMode = false;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;
var inEditMode = false;


$(function () {

    //InitGrid();

    $('#ddlRecyclerList').change(function () {
        FillShipToAddressData();
    })

    $("#btnAdd").button().click(function () {
        //jQuery("#jqGrid").jqGrid('editGridRow',"new",{height:280,reloadAfterSubmit:false});

        addRow();
    });

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;


        var isFocused = false;
        isFocused = false;

        if (!isValid) {
            return false;
        }


        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');
        if (inEditMode) {
            alert('There is an item opened in edit mode. Please save or cancel it first.');
            return false;
        }
        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    $("form").attr("action", rootPath + "/Shipment/Create");
                    //compileGridData();
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });

    $('input[id$=txtShipDate]').datepicker({});
    $('input[id$=txtShipDate]').datepicker('setDate', new Date());


    if ($("#txtAffiliateName").val() == "Costco 1998" || $("#txtAffiliateName").val() == "Costco 1997") {
        $("#ddlRecyclerList").show();
    }
    else {
        $("#ddlRecyclerList").hide();
    }
    $("#ddlShipToState").hide();
    $("#txtShipToState").val($("#ddlShipToState :selected").text());
    $("#ddlShipToCity").hide();
    $("#txtShipToCity").val($("#ddlShipToCity :selected").text());
    $("#ddlCarrierList").hide();
    $("#txtCarrier").val($("#ddlCarrierList :selected").text());
     
});

function InitGrid() {
    $("#jqGrid").jqGrid({
        //data: mydata,
        datatype: 'json',
        mtype: 'GET',
        url: rootPath + '/Shipment/InitItemGrid',
        height: 150,



        colNames: ['Pallet #', 'ItemId', 'Description', 'lbs', 'Qty', 'Condition', , 'Disposition', 'Actions'],
        colModel: [
   		            { name: 'PalletUnitNumber', index: 'PalletUnitNumber', editable: true, width: 40, sortable: false, editrules: { integer: true, required: true }, align: 'right' },
                    { name: 'ItemId', index: 'ItemId', editable: true, width: 200, sortable: false, editrules: { required: true }
                            , editoptions: { dataEvents: [{ type: 'change', fn: function (e) { getDescription() } }] }
                    },
   		            { name: 'Description', index: 'Description', editable: true, width: 200, sortable: false, editrules: { required: true} },
                    { name: 'Weight', index: 'Weight', editable: true, width: 100, sortable: false, editrules: { required: true, number: true }, align: 'right' },
   		            { name: 'Qty', index: 'Qty', editable: true, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Condition', index: 'Condition', editable: true, width: 200, editrules: { required: true }, sortable: false, edittype: 'select', formatter: 'select' },
                    { name: 'CoveredDevice', index: 'CoveredDevice', editable: true, width: 30, edittype: 'checkbox', formatter: "checkbox", hidden: true
                            , editoptions: { value: "true:false" }
                    },
                    { name: 'Disposition', index: 'Disposition', editable: true, width: 200, editrules: { required: true }, sortable: false, edittype: 'select', formatter: 'select' },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],
        rowNum: -1,
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        editurl: rootPath + '/Shipment/AddProductItem',
        width: 925,
        height: 300,
        afterInsertRow: function (rowid, aData) {
            be = "<input id='" + rowid + "_edRow' style='height:22px;padding:0' type='button' value='Edit' onclick=\"editRow('" + rowid + "');\" title='Edit Record' />";
            se = "<input id='" + rowid + "_savRow' style='height:22px;padding:0' type='button' value='Save' onclick=\"saveRow('" + rowid + "');\" title='Save Record' />";
            ce = "<input id='" + rowid + "_canRow' style='height:22px;padding:0' type='button' value='Cancel' onclick=\"restoreRow('" + rowid + "');\" title='Cancel Edit' />";
            $("#jqGrid").jqGrid('setRowData', rowid, { Action: be + se + ce });
            $("input[type=button]").button();
            sumWeight();
        },

        loadComplete: function () {
            //$('#jqGrid').setColProp('Condition', { editoptions: { value: "1:Like New;2:Used;3:Damaged;"} });
            //$('#Description').change(popacct); 
        }
    });
    loadDropdowns();
}

function loadDropdowns() {
    $('#jqGrid').setColProp('Condition', { editoptions: { value: "Like New:Like New;Used:Used;Damaged:Damaged;Opened/damaged box:Opened/damaged box;Incomplete:Incomplete"} });
    $('#jqGrid').setColProp('Disposition', { editoptions: { value: "Recycle:Recycle;Salvage:Salvage"} });

}

function addRow()
{
    jQuery("#jqGrid").jqGrid('editGridRow', 'new', {
            height: 280,
            recreateForm: true,
            reloadAfterSubmit: false, 
            addCaption: "Add Record",
		    editCaption: "Edit Record",
		    bSubmit: "Add",
		    bCancel: "Done",
		    bClose: "Close",
		    saveData: "Data has been changed! Save changes?",
		    bYes : "Yes",
		    bNo : "No",
		    bExit: "Done",
            modal: true,
            addedrow: 'last',
            closeAfterEdit: false,
            afterShowForm: function (formid){
                setPalletNumber();
            },
            afterComplete: function(response, postdata,formid){
                setPalletNumber();
                return true;
            },
            viewPagerButtons: false
    });
}

function setPalletNumber() {
    var rowCount = $("#jqGrid").getGridParam('reccount');
    if (rowCount == 0)
    {
        $('#PalletUnitNumber').val(1);
    }
    else 
    {
        var rowData = $("#jqGrid").getRowData(rowCount);
        var palletNumber = rowData.PalletUnitNumber;
        $('#PalletUnitNumber').val(palletNumber);
    }

    
}

function getDescription() { 
    //alert('popacct');
    var itemId =  $('#ItemId').val();
    //$('#Description').val(stuff);
    $.ajax({
        url: "/Shipment/GetProductDescription",
        dataType: 'json',
        type: "GET",
        data: ({ itemId: itemId }),
        async: false,
        cache: false,
        success: function (result) {
            $('#Description').val(result.description);
            $('#Weight').val(result.weight);
            if (result.coveredDevice)
                $("#CoveredDevice").attr('checked', true);
            else
                $("#CoveredDevice").attr('checked', false);
        }
    });

    //alert($("#CoveredDevice").val());
    return [true,'']; 
}
function EditRow(rowId) {
    if (rowId && rowId != lastSel) {
        //CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    //$('#jqGrid').editRow(rowId);
    //jQuery("#jqGrid").editGridRow("rowId",);
    jQuery("#jqGrid").jqGrid('editGridRow', rowId, {
                recreateForm: true,
                height: 280,
                reloadAfterSubmit: false, 
                addCaption: "Add Record",
		        editCaption: "Edit Record",
		        bSubmit: "Save",
		        bCancel: "Cancel",
		        bClose: "Close",
		        saveData: "Data has been changed! Save changes?",
		        bYes : "Yes",
		        bNo : "No",
		        bExit: "Cancel",
                modal: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                onClose: function (response, postdata) { 
                                CancelRow(rowId);
                            },
                afterComplete: function() {
                    SaveRow(rowId);
                    //addRow();
                }

               });
    inEditMode = true;
    sumWeight();
}

function SaveRow(rowId) {
    //$('#jqGrid').saveRow(rowId, AfterSaveRow);

    if ($("#jqGrid").saveRow(rowId, false, 'clientArray')) {
        var newData = $("#jqGrid").getRowData(rowId);
        if (parseInt(newData["Qty"]).toString() == 'NaN') {
            return;
        }
        else {
            inEditMode = false;
            $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
            $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
        }
    }
    sumWeight();
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    //$('#jqGrid').restoreRow(rowId);

    $.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.ItemId == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
        lastSel = lastSel - 1;
    }
    inEditMode = false;
}

function DeleteRow(rowId){
    jQuery("#jqGrid").jqGrid('delRowData', rowId);
    sumWeight();
}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}



function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            if (elem.name == "ddlPickupState")
                $("select#ddlPickupCity").clearSelect();
            else
                $("select#ddlShipToCity").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        if (elem.name == "ddlPickupState")
                            $("select#ddlPickupCity").fillSelect(result.data);
                        else
                            $("select#ddlShipToCity").fillSelect(result.data);

                    }
                }
            });
        }
    }
}

function sumWeight(){

    var totalweight = 0;
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    for (i = 1; i <= numberOfRecords; i++) {
        var rowData = $("#jqGrid").getRowData(i);
        totalweight += parseFloat(rowData['Weight']) * parseFloat(rowData['Qty']);
    }
    $("#txtTotalWeight").val(totalweight);
}
function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}

function FillShipToAddressData() {
    var reccyclerId = $('#ddlRecyclerList').val();
    $("#txtRecyclerName").val($("#ddlRecyclerList").find("option:selected").text());
    if ($("#ddlRecyclerList").find("option:selected").text() == "Donations") {
        $("#txtRecyclerName").prop("readonly", false);
        $('#txtShipToAddress').prop("readonly", false);
        $('#txtShipToZip').prop("readonly", false);
        $("#txtShipToState").hide();
        $("#txtShipToCity").hide();
        $('#ddlShipToCity').show();
        $("#ddlShipToState").show();
    }
    else {
        $("#txtRecyclerName").prop("readonly", true);
        $('#txtShipToAddress').prop("readonly", true);
        $('#txtShipToZip').prop("readonly", true);
        $("#txtShipToState").show();
        $("#txtShipToCity").show();
        $('#ddlShipToCity').hide();
        $("#ddlShipToState").hide();
    }
    if (reccyclerId == 0) {
        $('#txtShipToAddress').val("");
        if ($('#ddlShipToState').get(0).options[0].selected != true) {
            $('#ddlShipToState').get(0).options[0].selected = true;
            $("#ddlShipToState").trigger("change");
        }
        $('#ddlShipToCity').val(0);
        $('#txtShipToZip').val("");
        $('#txtShipToPhone').val("");

    } else {
        $.ajax({
            url: '/Shipment/GetShipToAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: reccyclerId },
            async: false,
            success: function (result) {
                $('#txtShipToAddress').val(result.address);
                if ($('#ddlShipToState').val() != result.data) {
                    $('#ddlShipToState').val(result.state);
                    $("#ddlShipToState").trigger("change");
                    $("#txtShipToState").val($("#ddlShipToState").find("option:selected").text());
                }
                $('#ddlShipToCity').val(result.city);
                $("#txtShipToCity").val($("#ddlShipToCity").find("option:selected").text());
                $('#txtShipToZip').val(result.zip);
            }
        });
    }
}

function compileGridData() {
    //get total rows in you grid
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    var lineItems = '';
    for (i = 1; i <= numberOfRecords; i++) {

        var rowData = $("#jqGrid").getRowData(i);
        lineItems = 'PalletUnitNumber:' + rowData['PalletUnitNumber'];
        lineItems += ',ItemId:' + rowData['ItemId'];
        lineItems += ',Description:' + rowData['Description'];
        lineItems += ',Qty:' + rowData['Qty'];
        lineItems += ',Condition:' + rowData['Condition'];
        lineItems += ',CoveredDevice:' + rowData['CoveredDevice'];
        lineItems += ',Disposition:' + rowData['Disposition'];
        lineItems += ',Weight:' + rowData['Weight'];
        $('<input type="hidden" />').attr('name', 'Item_' + i).attr('value', lineItems).appendTo('#hiddenDivId');
    }
}