﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;


    $("button, input:submit, input:button").button();

    jQuery("#jqGrid").jqGrid({
        url: "/AffiliateContact/GetAffiliateContacts",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id', 'First Name', 'Last Name', 'Phone', 'Actions'],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
   		{ name: 'First Name', index: 'First Name', width: 85, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
        { name: 'Last Name', index: 'Last Name', width: 85, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Phone', index: 'Phone', width: 130, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125} },
        {name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: myformatter, title: true, align: 'center', width: 35, editable: false },
        //{ name: 'ActionMenu', index: 'ActionMenu', sortable: false, title: true, align: 'center', width: 35, editable: false },


   	],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 925,
        height: 70,
        editurl: "PerformAction"


    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();


});

$('select[id$=StateId]').change(function (elem) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    state = $("select#StateId").val();
    if (state == "0") {
        $("select#CityId").clearSelect();
    }
    else {
        var rootPath = window.location.protocol + "//" + window.location.host;
        $.ajax({
            url: rootPath + '/Affiliate/GetCities/',
            dataType: 'json',
            type: "POST",
            data: ({ state: state }),
            async: false,
            success: function (result) {
                if (result.data) {
                    $("select#CityId").fillSelect(result.data);
                }
            }
        });
    }

});

$("#Password").focus(function (event) {
    $("#Password").select();
})

$("#ConfirmPassword").focus(function (event) {
    $("#ConfirmPassword").select();
})

$('input[id$=btnClear]').click(function () {

    $(this.form).find(':input').each(function () {
        switch (this.type) {

            case 'select-multiple':
            case 'select-one':
                $(this).val(0);
                break;
            case 'password':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
                if (this.name != 'Active')
                    this.checked = false;
                break;
        }
    });
    var state = $("select#StateId").val();
    if (state == "0") {
        $("select#CityId").clearSelect();
    }
    $("#validationSummary").html("");

});

$('input[id$=btnAdd]').click(function () {

    $(this.form).find(':input').each(function () {
        switch (this.type) {

            case 'select-multiple':
            case 'select-one':
                $(this).val(0);
                break;
            case 'password':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
                if (this.name != 'Active')
                    this.checked = false;
                break;
        }
    });
    var state = $("select#StateId").val();
    if (state == "0") {
        $("select#CityId").clearSelect();
    }
    $("#validationSummary").html("");
    //var affiliateId = <% = Session["AffiliateId"]; %>; 

    //alert(affiliateId);
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/AffiliateContact/Clear' + "?data=" + $('#data').val(); ;
    window.location.href = url;

});

$('input[id$=btnSubmit]').click(function () {

    for (var iForm = 0; iForm < document.forms.length; iForm++) {//look through all the forms
        return ValidateSingleFormEntry(iForm);
    }

});



function Edit(affiliateContactId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    if (affiliateContactId != "-") {
        window.location.href = rootPath + '/AffiliateContact/Edit/' + affiliateContactId + "?data=" + $('#data').val();
    }

}

function myformatter(cellvalue, options, rowObject) {
    return "<div><a href='javascript:Edit(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit' style='border:none;'/> </a></div>";
}

function Cancel() {
    var url = '/Affiliate/Index/';
    window.location.href = url + "?data=" + $('#data').val();
}


function ValidateSingleFormEntry(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var type;
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                type = oRequired.getAttribute('type');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || (oRequired.value == "0" && type != "text")) {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"
                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;
                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }

        if ($("input#Password").val().toLowerCase() != $("input#ConfirmPassword").val().toLowerCase()) {
            bValidated = false;
            dynamicHTML += "<li>Password and Confirm Password should be same.</li>";
        }
        if ($("input#Password").val().length < 6 || $("input#ConfirmPassword").val().length < 6) {
            bValidated = false;
            dynamicHTML += "<li>Password must be at least 6 characters long.</li>";
        }
        dynamicHTML += "</ul>";
        //alert(dynamicHTML);
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }

}