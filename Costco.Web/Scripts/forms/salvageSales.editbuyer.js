﻿//var to store last selected row id
var lastSel;
var inEditMode = false;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;
var inEditMode = false;
var store = "";
var validationstring = "";

$(function () {
    $('#txtSoldAmount').val(round($('#txtSoldAmount').val(), 2));
    $('#txtRetailValue').val(round($('#txtRetailValue').val(), 2));

    $('input[id$=txtReceivedDate]').datepicker({});

    InitGrid();

    $("#btnUpdate").click(function () {
        openReceiveDialog();
    });




});

function InitGrid() {
    $("#jqGrid").jqGrid({
        datatype: 'json',
        mtype: 'GET',
        url: rootPath + '/SalvageSales/GetSalvageItemsBuyer',
        height: 150,

        colNames: ['SalvageItemID', 'Pallet #', 'ItemId', 'Description','dumb' , 'Retail', 'Price', 'Qty', 'Total','Missing', 'Actions'],
        colModel: [
                    { name: 'SalvageItemID', index: 'SalvageItemID', editable: true, width: 85, hidden: true },
   		            { name: 'PalletUnitNumber', index: 'PalletUnitNumber', editable: false, width: 40, sortable: false, editrules: { integer: true, required: true }, align: 'right' },
                    { name: 'ItemId', index: 'ItemId', editable: false, width: 200, sortable: false, editrules: { required: true}  
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { getDescription() }} ] }
                    },
   		            { name: 'Description', index: 'Description', editable: false, width: 200, sortable: false, editrules: { required: true} },
                    { name: 'SalesOrderId', index: 'SalesOrderId', editable: true, width: 85, hidden: true },
                    { name: 'Retail', index: 'Retail', editable: false, width: 80, sortable: false, editrules: { required: false }, align: 'right', hidden: false, editoptions: { dataEvents: [{ type: 'change', fn: function (e) { calculateSellingPrice() } }] } },
                    { name: 'Price', index: 'Price', editable: false, width: 80, sortable: false, editrules: { required: false }, align: 'right', hidden: false },
                    { name: 'Qty', index: 'Qty', editable: false, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Total', index: 'Total', editable: false, width: 80, sortable: false, editrules: { required: false, number: true }, align: 'right', hidden: false },
                    { name: 'Missing', index: 'Missing', editable: true, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],

        rowNum: 100000,
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        editurl: rootPath + '/SalvageSales/EditSalvageItemBuyer',
        width: 925,
        height: 300,

        loadComplete: function () {
        }
    });
}

function EditRow(rowId) {
    if (rowId && rowId != lastSel) {
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);

    inEditMode = true;
}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(response) {
    var json = response.responseText;
    var result = eval("(" + json + ")");
    //if (result.status == false)
    //    alert(result.message);
    if (result.success != true) {
        alert(result.message);
    }

    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    //$('#jqGrid').trigger("reloadGrid")
    return result.success;
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
    //$.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);

    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.ItemId == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
        lastSel = lastSel - 1;
    }
    inEditMode = false;
}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    //var action = "<div><img src='/Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;</div>";
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='/Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='/Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            validationstring = dynamicHTML;
        }
        return bValidated;
    }
}

function ValidateSale() {
    var bValidated = true;
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";
    if ($('#txtSoldBy').val() == '') {
        bValidated = false;
        dynamicHTML += "<li>Sold by is a required field.</li>"
    }
    if ($('#txtSoldDate').val() == '') {
        bValidated = false;
        dynamicHTML += "<li>Sold date is a required field.</li>"
    }

    dynamicHTML += "</ul>";
    if (bValidated == false) {
        validationstring = dynamicHTML;
    }
    return bValidated;
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}


function openReceiveDialog() {
    $("#dialog-receive").dialog({
        //appendTo: "form",
        modal: true,
        width: 350,
        resizable: false,
        position: 'center',
        buttons: {
            "Cancel": function () {
                $(this).dialog("close");
            },
            "Set": function () {
                $("#validationSummary").html("");
                //var isValid = ValidateForm(); //true;


                var isFocused = false;
                isFocused = false;

                //if (!isValid) {
                //    $("#validationSummary").html(validationstring);
                //    return false;
                //}

                var ssId = $('#SSOrderId').val();
                var receivedBy = $('#txtReceivedBy').val();
                var receivedDate = $('#txtReceivedDate').val();

                $.ajax({
                    url: '/SalvageSales/Receive',
                    dataType: 'json',
                    type: "POST",
                    data: { id: ssId, receivedBy: receivedBy, receivedDate: receivedDate },
                    async: false,
                    success: function (result) {
                        if (result.status == false) {
                            alert(result.message);
                            return false;
                        } else {
                            $('#tdReceivedBy').text('Received By: ' + receivedBy);
                            window.location.href = "/SalvageSales/BuyerList";
                        }
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}




