﻿var affiliateType = $("#hfAffiliateType").val();
var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    $("button, input:submit, input:button").button();


    jQuery("#jqGrid").jqGrid({
        url: "/Product/GetProductList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['ItemId', 'Description', 'Weight', 'Price', 'Default', 'Type','Count'],
        colModel: [
                    { name: 'ItemId', index: 'ItemId', width: 80, align: 'Left', editable: false, sortable: false },
                    { name: 'Description', index: 'Description', width: 160, align: 'Left', editable: true, sortable: false, editrules: {required: true} },
   		            { name: 'Weight', index: 'Weight', width: 80, align: 'Center', editable: true, sortable: false, editrules: { number: true, required: true} },
   		            { name: 'Price', index: 'Price', width: 80, align: 'Right', editable: true, sortable: false, editrules: { number: true, required: true }, hidden: (affiliateType == 1 ? false : (affiliateType == 5 ? false : true)) },
                    { name: 'Disposition', index: 'Disposition', width: 55, editable: false, sortable: false, editrules: { required: false }, edittype: 'select' },
                    { name: 'Type', index: 'Type', width: 145,  editable: true, sortable: false, editrules: { required: false }, edittype: 'select' },
                    { name: 'Count', index: 'Count', width: 80, align: 'Right', editable: false, sortable: false }
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "PerformAction",

        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: "/Product/UpdateProductCell",
        afterInsertRow: function (rowid, rowdata, rowelem) {

        },
        afterSubmitCell: function (serverStatus, aPostData) {
            //var response = serverStatus.responseText;
            //alert(response);
            //return [false, response, ""];

            var json = serverStatus.responseText;
            var result = eval("(" + json + ")");
            //$('#TotalWeight').val(result.totalWeight);
            return [result.success, result.message, ""];
        },
        gridComplete: function () {
            LoadDropdownsData();
        },
        postData: { SearchString: "", ProductTypeId: 99, PriceNotSet: false, WeightNotSet: false }
        //afterSaveCell: function (rowid, cellname, value, iRow, iCol) {
        //    AddPrice(rowid, cellname, value, iRow, iCol);
        //    //alert('saved');
        //}
    });

    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });


    $("#btnSearch").click(function () {
        Search();
    });


});

function Search() {
    var searchString = $("#txtSearchString").val();
    var productTypeId = $("#ddlProductType").val()
    var priceNotSet = $("#PriceSet").prop('checked');
    var weightNotSet = $("#WeightSet").prop('checked');
    jQuery("#jqGrid").setGridParam({ postData: { SearchString: searchString, ProductTypeId: productTypeId, PriceNotSet: priceNotSet, WeightNotSet: weightNotSet }, page: 1 }).trigger("reloadGrid");
}

function AddPrice(rowid, cellname, value, iRow, iCol) {
    $.ajax({
        url: '/Product/AddPrice',
        dataType: 'json',
        type: "POST",
        data: ({ ItemId: rowid, reference: value }),
        async: false,
        success: function (result) {
            if (result.success) {
                chkbox.checked = true;
                alert("Success:Invoice Updated");
            }
            else {
                alert("Update Failed:" + result.message);
                return false;
            }
        }

    });
    
    //alert("hello");

}


function LoadDropdownsData() {
    $.ajax({
        url: '/Product/GetProductTypes',
        dataType: 'json',
        type: "POST",
        data: {},
        async: false,
        success: function (result) {
            $('#jqGrid').setColProp('Type', { editoptions: { value: result.productTypes} });
        }
    });
    $('#jqGrid').setColProp('Disposition', { editoptions: { value: "Not Set:Not Set;Recycle:Recycle;Salvage:Salvage"} });

}

