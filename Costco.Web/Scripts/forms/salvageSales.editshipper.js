﻿//var to store last selected row id
var lastSel;
var inEditMode = false;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;
var inEditMode = false;
var store = "";
var validationstring = "";

$(function () {
    $('#txtSoldAmount').val(round($('#txtSoldAmount').val(), 2));
    $('#txtRetailValue').val(round($('#txtRetailValue').val(), 2));

    $('input[id$=txtSoldDate]').datepicker({});

    InitGrid();

    $("#btnAdd").button().click(function () {
        addRow();
    });

    $("#btnChangeBuyer").click(function () {
        openBuyerDialog();
    });

    $("#btnSell").click(function () {
        openSellDialog();
    });

    $('#ddlBuyerList').change(function () {
        FillAddressData();
    });


});

function InitGrid() {
    $("#jqGrid").jqGrid({
        //data: mydata,
        datatype: 'json',
        mtype: 'GET',
        url: rootPath + '/SalvageSales/GetSalvageItemsShipper',
        height: 150,



        colNames: ['SalvageItemID', 'Pallet #', 'ItemId', 'Description','dumb' , 'Retail', 'Price', 'Qty', 'Total', 'Actions'],
        colModel: [
                    { name: 'SalvageItemID', index: 'SalvageItemID', editable: true, width: 85, hidden: true },
   		            { name: 'PalletUnitNumber', index: 'PalletUnitNumber', editable: true, width: 40, sortable: false, editrules: { integer: true, required: true }, align: 'right' },
                    { name: 'ItemId', index: 'ItemId', editable: true, width: 200, sortable: false, editrules: { required: true}  
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { getDescription() }} ] }
                    },
   		            { name: 'Description', index: 'Description', editable: true, width: 200, sortable: false, editrules: { required: true} },
                    { name: 'SalesOrderId', index: 'SalesOrderId', editable: true, width: 85, hidden: true },
                    { name: 'Retail', index: 'Retail', editable: true, width: 80, sortable: false, editrules: { required: false }, align: 'right', hidden: false, editoptions: { dataEvents: [ {type: 'change', fn: function(e) { calculateSellingPrice() }}] } },
                    { name: 'Price', index: 'Price', editable: true, width: 80, sortable: false, editrules: { required: false }, align: 'right', hidden: false },
                    { name: 'Qty', index: 'Qty', editable: true, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Total', index: 'Total', editable: false, width: 80, sortable: false, editrules: { required: false, number: true }, align: 'right', hidden: false },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],

        rowNum: 100000,
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        //editurl: rootPath + '/Shipment/AddProductItem',
        editurl: rootPath + '/SalvageSales/EditSalvageItemShipper',
        width: 925,
        height: 300,
        //afterInsertRow: function (rowid, aData) {
        //    be = "<input id='" + rowid + "_edRow' style='height:22px;padding:0' type='button' value='Edit' onclick=\"editRow('" + rowid + "');\" title='Edit Record' />";
        //    se = "<input id='" + rowid + "_savRow' style='height:22px;padding:0' type='button' value='Save' onclick=\"saveRow('" + rowid + "');\" title='Save Record' />";
        //    ce = "<input id='" + rowid + "_canRow' style='height:22px;padding:0' type='button' value='Cancel' onclick=\"restoreRow('" + rowid + "');\" title='Cancel Edit' />";
        //    $("#jqGrid").jqGrid('setRowData', rowid, { Action: be + se + ce });
        //    $("input[type=button]").button();
            //sumWeight();
        //},

        loadComplete: function () {
            //$('#Description').change(popacct); 
            //sumWeight();
        }
    });
    //loadDropdowns();
}

function addRow()
{
    jQuery("#jqGrid").jqGrid('editGridRow', 'new', {
        height: 320,
        recreateForm: true,
        reloadAfterSubmit: false,
        addCaption: "Add Record",
        editCaption: "Edit Record",
        bSubmit: "Add",
        bCancel: "Done",
        bClose: "Close",
        saveData: "Data has been changed! Save changes?",
        bYes: "Yes",
        bNo: "No",
        bExit: "Cancel",
        modal: true,
        addedrow: 'last',
        closeAfterEdit: false,
        url: rootPath + '/SalvageSales/EditSalvageItemShipper',
        afterSubmit: function (response, postdata) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            return [result.success, result.message, result.ssItemId];
        },
        afterShowForm: function (formid) {
            setPalletNumber();
        },
        afterComplete: function (response, postdata, formid) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            setTotal(result.ssItemId);
            $('#txtRetailValue').val(round(result.retailAmount,2));
            $('#txtSoldAmount').val(round(result.soldAmount,2));
            setPalletNumber();
            return true;
        },
        viewPagerButtons: false
    });

}

function setPalletNumber() {
    var rowCount = $("#jqGrid").getGridParam('reccount');
    if (rowCount == 0)
    {
        $('#PalletUnitNumber').val(1);
    }
    else 
    {
        var rows = jQuery("#jqGrid").jqGrid('getRowData');
        var rowData = rows[rowCount - 1];
        var palletNumber = rowData.PalletUnitNumber;
        $('#PalletUnitNumber').val(palletNumber);
    }
    $('#SalesOrderId').val($('#SSOrderId').val());
    $('#Qty').val(1);
    
}

function getDescription() { 
    var itemId =  $('#ItemId').val();
    if (itemId.length > 6 || isNaN(itemId) || itemId.length < 2)
    {
        itemId = "Misc";
        $('#ItemId').val("Misc");
    }
   
    
    $.ajax({
        url: "/Shipment/GetProductDescription",
        dataType: 'json',
        type: "GET",
        data: ({ itemId: itemId }),
        async: false,
        cache: false,
        success: function (result) {
            $('#Description').val(result.description);
            $('#Retail').val(result.price);
            $('#Price').val(round(result.price * ($('#txtPct').val() * .01), 2));

        }
    });

    return [true,'']; 
}
function EditRow(rowId) {
    if (rowId && rowId != lastSel) {
        //CancelRow(lastSel);
        lastSel = rowId;
    }
    //$('#ED_ActionMenu_' + rowId).css({ display: "none" });
    //$('#SC_ActionMenu_' + rowId).css({ display: "block" });
    jQuery("#jqGrid").jqGrid('editGridRow', rowId, {
        height: 320,
        recreateForm: true,
        reloadAfterSubmit: false,
        addCaption: "Add Record",
        editCaption: "Edit Record",
        bSubmit: "Save",
        bCancel: "Cancel",
        bClose: "Close",
        saveData: "Data has been changed! Save changes?",
        bYes: "Yes",
        bNo: "No",
        bExit: "Cancel",
        modal: true,
        closeAfterEdit: true,
        viewPagerButtons: false,
        afterSubmit: function (response, postdata) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            //var newData = $("#jqGrid").getRowData(rowId);
            return [result.success, result.message]
        },
        onClose: function (response, postdata) {
            
            CancelRow(rowId);
        },
        afterComplete: function (response, postdata, formid) {
            var json = response.responseText;
            var result = eval("(" + json + ")");
            setTotal(rowId);
            $('#txtRetailValue').val(round(result.retailAmount,2));
            $('#txtSoldAmount').val(round(result.soldAmount,2));
            SaveRow(rowId);
        }

    });
    inEditMode = true;
}

function SaveRow(rowId) {

    inEditMode = false;
    //$('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    //$('#SC_ActionMenu_' + lastSel).css({ display: "none" });

    //sumWeight();
}

function CancelRow(rowId) {
    //$('#ED_ActionMenu_' + rowId).css({ display: "block" });
    //$('#SC_ActionMenu_' + rowId).css({ display: "none" });
    //$('#jqGrid').restoreRow(rowId);
    //$.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);

    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.ItemId == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
        lastSel = lastSel - 1;
    }
    inEditMode = false;
}

function DeleteRow(rowId){
    $.ajax({
        url: "/SalvageSales/DeleteSalvageSalesItem",
        dataType: 'json',
        type: "POST",
        data: ({ SalvageItemID: rowId }),
        async: false,
        cache: false,
        success: function (result) {
            jQuery("#jqGrid").jqGrid('delRowData', rowId);
            $('#txtRetailValue').val(round(result.retailAmount,2));
            $('#txtSoldAmount').val(round(result.soldAmount,2));
        }
    });
}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var action = "<div><img src='/Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='/Content/images/icon/Cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    //var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='/Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    //actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='/Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return action;
}

function calculateSellingPrice() {
    var retailprice = $('#Retail').val();
    var sellingprice = round(retailprice * ($('#txtPct').val() * .01), 2);
    $('#Price').val(sellingprice);
    //$('#Total').val(sellingprice * $('#Qty').val());
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

function setTotal(rowid) {
    var qty = $('#jqGrid').getCell(rowid, 'Qty');
    var sellingprice = $('#jqGrid').getCell(rowid, 'Price');
    $("#jqGrid").jqGrid("setCell", rowid, 'Total', round(qty * sellingprice, 2));
}

function openBuyerDialog() {
    $("#dialog-buyer").dialog({
        appendTo: "form",
        modal: true,
        width: 500,
        resizable: false,
        position: 'center',
        buttons: {
            "Cancel": function () {
                $('#ddlBuyerList').val(0);
                $('#SoldToName').val("");
                $('#SoldToAddress').val("");
                $('#ddlBuyerState').val(0);
                $('#ddlBuyerCity').val(0);
                $('#SoldToZip').val("");
                window.location = '#';
                $(this).dialog("close");
            },
            "Set/Change": function () {
                $("#validationSummary").html("");
                var isValid = ValidateForm(); //true;


                var isFocused = false;
                isFocused = false;

                if (!isValid) {
                    $("#validationSummary").html(validationstring);
                    return false;
                }

                var ssId = $('#SSOrderId').val();
                var soldToId = $('#ddlBuyerList').val();
                var soldToName = $('#SoldToName').val();
                var soldToAddress = $('#SoldToAddress').val();
                var soldToStateId = $('#ddlBuyerState').val();
                var soldToCityId = $('#ddlBuyerCity').val();
                var soldToZip = $('#SoldToZip').val();

                $.ajax({
                    url: '/SalvageSales/SetBuyer',
                    dataType: 'json',
                    type: "POST",
                    data: { id: ssId, soldToId: soldToId, soldToName: soldToName, soldToAddress: soldToAddress, soldToStateId: soldToStateId, soldToCityId: soldToCityId, soldToZip: soldToZip },
                    async: false,
                    success: function (result) {
                        if (result.status == false) {
                            alert("result.message")
                            return false;
                        } else {
                            $('#tdCompany').text($('#SoldToName').val());
                            $('#tdAddress').text($('#SoldToAddress').val());
                            $('#tdStateCityZip').text( $('#ddlBuyerCity :selected').text() + ", " +$('#ddlBuyerState :selected').text() + " " + $('#SoldToZip').val());
                            //var soldToCityId = $('#ddlBuyerCity').val();
                            //var soldToZip = $('#SoldToZip').val();
                            $('#ddlBuyerList').val(0);
                            $('#SoldToName').val("");
                            $('#SoldToAddress').val("");
                            $('#ddlBuyerState').val(0);
                            $('#ddlBuyerCity').val(0);
                            $('#SoldToZip').val("");
                            
                        }
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}

function openBuyerDialog() {
    $("#dialog-buyer").dialog({
        appendTo: "form",
        modal: true,
        width: 500,
        resizable: false,
        position: 'center',
        buttons: {
            "Cancel": function () {
                $("#validationSummary").html("");
                $('#ddlBuyerList').val(0);
                $('#SoldToName').val("");
                $('#SoldToAddress').val("");
                $('#ddlBuyerState').val(0);
                $('#ddlBuyerCity').val(0);
                $('#SoldToZip').val("");
                window.location = '#';
                $(this).dialog("close");
            },
            "Set/Change": function () {
                $("#validationSummary").html("");
                var isValid = ValidateForm(); //true;


                var isFocused = false;
                isFocused = false;

                if (!isValid) {
                    $("#validationSummary").html(validationstring);
                    return false;
                }

                var ssId = $('#SSOrderId').val();
                var soldToId = $('#ddlBuyerList').val();
                var soldToName = $('#SoldToName').val();
                var soldToAddress = $('#SoldToAddress').val();
                var soldToStateId = $('#ddlBuyerState').val();
                var soldToCityId = $('#ddlBuyerCity').val();
                var soldToZip = $('#SoldToZip').val();

                $.ajax({
                    url: '/SalvageSales/SetBuyer',
                    dataType: 'json',
                    type: "POST",
                    data: { id: ssId, soldToId: soldToId, soldToName: soldToName, soldToAddress: soldToAddress, soldToStateId: soldToStateId, soldToCityId: soldToCityId, soldToZip: soldToZip },
                    async: false,
                    success: function (result) {
                        if (result.status == false) {
                            return false;
                        } else {
                            $('#hdSoldToId').val(soldToId);
                            $('#tdCompany').text($('#SoldToName').val());
                            $('#tdAddress').text($('#SoldToAddress').val());
                            $('#tdStateCityZip').text( $('#ddlBuyerCity :selected').text() + ", " +$('#ddlBuyerState :selected').text() + " " + $('#SoldToZip').val());
                            //var soldToCityId = $('#ddlBuyerCity').val();
                            //var soldToZip = $('#SoldToZip').val();
                            $('#ddlBuyerList').val(0);
                            $('#SoldToName').val("");
                            $('#SoldToAddress').val("");
                            $('#ddlBuyerState').val(0);
                            $('#ddlBuyerCity').val(0);
                            $('#SoldToZip').val("");
                            
                        }
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}

function openSellDialog() {
    var soldToId = $('#hdSoldToId').val();
    if (soldToId == null || soldToId == 0 || soldToId == 999999)
    {
        alert("Buyer must be set before selling.");
        return;
    }
    $("#dialog-sell").dialog({
        //appendTo: "form",
        modal: true,
        width: 350,
        resizable: false,
        position: 'center',
        buttons: {
            "Cancel": function () {
                $("#validationSummary2").html("");
                $('#txtSoldBy').val("");
                $('#txtSoldDate').val("");
                $(this).dialog("close");
            },
            "Set/Change": function () {
                $("#validationSummary2").html("");
                var isValid = ValidateSale(); //true;


                var isFocused = false;
                isFocused = false;

                if (!isValid) {
                    $("#validationSummary2").html(validationstring);
                    return false;
                }

                var ssId = $('#SSOrderId').val();
                var soldBy = $('#txtSoldBy').val();
                var soldDate = $('#txtSoldDate').val();

                $.ajax({
                    url: '/SalvageSales/Sell',
                    dataType: 'json',
                    type: "POST",
                    data: { id: ssId, soldBy: soldBy, soldDate: soldDate},
                    async: false,
                    success: function (result) {
                        if (result.status == false) {
                            alert("result.message");
                            return false;
                        } else {
                            
                            window.location.href = "/SalvageSales/SellerList";
                        }
                    }
                });
                $(this).dialog("close");
            }
        }
    });
}
function FillAddressData() {
    var soldToId = $('#ddlBuyerList').val();
    $('#SoldToAffiliateId').val(soldToId);
    $("#SoldToName").val($("#ddlBuyerList").find("option:selected").text());
    if (soldToId == 0) {
        $("#SoldToName").val("");
        $("#SoldToName").prop("readonly", true);
        $('#SoldToAddress').val("");
        $("#SoldToAddress").prop("readonly", true);
        if ($('#ddlBuyerState').get(0).options[0].selected != true) {
            $('#ddlBuyerState').get(0).options[0].selected = true;
            $("#ddlBuyerState").trigger("change");
        }
        $('#ddlBuyerCity').val(0);
        $('#SoldToZip').val("");

    }
    else if (soldToId == 999999) {
        $("#SoldToName").val("");
        $("#SoldToName").prop("readonly", false);
        $('#SoldToAddress').val("");
        $("#SoldToAddress").prop("readonly", false);
        if ($('#ddlBuyerState').get(0).options[0].selected != true) {
            $('#ddlBuyerState').get(0).options[0].selected = true;
            $("#ddlBuyerState").trigger("change");
        }
        $('#ddlBuyerCity').val(0);
        $('#SoldToZip').val("");

    } else {
        $("#SoldToName").prop("readonly", true);
        //$('#SoldToAffiliateId').val(soldToId);
        $.ajax({
            url: '/Shipment/GetShipToAddress/',
            dataType: 'json',
            type: "POST",
            data: { id: soldToId },
            async: false,
            success: function (result) {
                $('#SoldToAddress').val(result.address);
                if ($('#ddlBuyerState').val() != result.data) {
                    $('#ddlBuyerState').val(result.state);
                    $('#ddlBuyerState').trigger('change');
                    //$('#SoldToStateId').val(result.state);
                }
                $('#ddlBuyerCity').val(result.city);
                //$('#SoldToCityId').val(result.city);
                $('#SoldToZip').val(result.zip);
            }
        });
    }
}
function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            if (elem.name == "ddlPickupState")
                $("select#ddlPickupCity").clearSelect();
            else
                $("select#ddlShipToCity").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#ddlBuyerCity").fillSelect(result.data);
                    }
                }
            });
        }
    }
}
function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            validationstring = dynamicHTML;
        }
        return bValidated;
    }
}

function ValidateSale() {
    var bValidated = true;
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";
    if ($('#txtSoldBy').val() == '') {
        bValidated = false;
        dynamicHTML += "<li>Sold by is a required field.</li>"
    }
    if ($('#txtSoldDate').val() == '') {
        bValidated = false;
        dynamicHTML += "<li>Sold date is a required field.</li>"
    }

    dynamicHTML += "</ul>";
    if (bValidated == false) {
        validationstring = dynamicHTML;
    }
    return bValidated;
}






