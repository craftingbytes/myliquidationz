﻿
var inEditMode = false;
var lastSel;
var affiliateType = $("#hfAffiliateType").val();
$(document).ready(function () {
    $("button, input:submit, input:button").button();

    var currentSalesOrderID = $("#hfCurrentSalesOrderID").val();
    var rootPath = window.location.protocol + "//" + window.location.host;

    jQuery("#jqGrid").jqGrid({
        url: "/PullShipmentItem/GetShipmentItemsToPull",
        datatype: 'json',
        mtype: 'GET',
        colNames: [
                    'ShipmentId',
                    'ShipmentItemID',
                    'ItemId',
                    'Description',
                    'Dispostion',
                    'Condition',
                    'Comment',
                    'CED',
                    'Qty',
                    'Available',
                    'Pulled',
                    'Actions'
                  ],
        colModel: [
                    { name: 'ShipmentId', index: 'ShipmentId', editable: false, width: 85, hidden: false },
                    { name: 'ShipmentItemID', index: 'ShipmentItemID', editable: true, width: 85, hidden: true },
                    { name: 'ItemId', index: 'ItemId', editable: false, width: 80, sortable: false, editrules: { required: true} },
   		            { name: 'Description', index: 'Description', editable: false, width: 200, sortable: false, editrules: { required: true} },
                    { name: 'Disposition', index: 'Disposition', editable: false, width: 100, sortable: false, editrules: { required: true} },
        //{ name: 'QtyReceived', index: 'QtyReceived', editable: false, width: 100, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    {name: 'ConditionReceived', index: 'ConditionReceived', editable: false, sortable: false, editrules: { required: true} },
                    { name: 'ConditionReceived2', index: 'ConditionReceived2', editable: false, width: 200, sortable: false, editrules: { required: false} },
   		            { name: 'CoveredDevice', index: 'CoveredDevice', editable: false, width: 30, edittype: 'checkbox', formatter: "checkbox", editoptions: { value: "true:false"} },
                    { name: 'QtyReceived', index: 'QtyReceived', editable: false, width: 50, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Available', index: 'Available', editable: false, width: 50, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'Pulled', index: 'Pulled', editable: true, width: 50, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],
        rowNum: 50,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240,
        editurl: "/PullShipmentItem/EditShipmentItem"
    });


    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });


    $("#btnSearch").click(function () {
        Search();
    });

    $("#btnBack").click(function () {
        var url = rootPath + '/SalesOrder/Edit?id=' + currentSalesOrderID;
        window.location.href = url + "&data=" + $('#data').val();
    });

    if (currentSalesOrderID == 0) {
        $("#btnSearch").show();
        $("#btnBack").hide();
    }
    else {
        $("#btnSearch").hide();
        $("#btnBack").show();
    }

});

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "";
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;</div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function EditRow(rowId) {
    if (rowId && rowId !== lastSel) {
        CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    $('#jqGrid').editRow(rowId);
}

function SaveRow(rowId) {
    $('#jqGrid').saveRow(rowId, AfterSaveRow);
}

function AfterSaveRow(response) {
    var json = response.responseText;
    var result = eval("(" + json + ")");
    if (result.status == false)
        alert(result.message);
    $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
    $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
    $('#jqGrid').trigger("reloadGrid")
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    $('#jqGrid').restoreRow(rowId);
}

function Search() {
    $("#validationSummary").html("");
    var isValid = ValidateForm(); //true;
    if (!isValid) {
        return false;
    }
    var shipmentId = $("#txtShipmentId").val();
    jQuery("#jqGrid").setGridParam({ postData: { shipmentId: shipmentId}, page: 1 }).trigger("reloadGrid");
}


function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}