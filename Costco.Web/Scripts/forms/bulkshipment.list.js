﻿var affiliateType = $("#hfAffiliateType").val();

$(document).ready(function () {

    $("button, input:submit, input:button").button();

    var myDate = new Date();
    var month = myDate.getMonth();
    var day = myDate.getDay();
    var year = myDate.getFullYear() - 1;
    myDate = month + "/" + day + "/" + year;

    $("#txtBeginDate").datepicker();
    $('#txtBeginDate').datepicker('setDate', new Date());
    $("#txtEngingDate").datepicker();
    $('#txtEngingDate').datepicker('setDate', new Date());

    jQuery("#jqGrid").jqGrid({
        url: "GetShippingDataList",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['ShipmentId', 'From', 'Recycler', 'Shipped', 'Received', 'Shipping Report', '', '', 'Actions'],
        colModel: [
                    { name: 'ShipmentId', index: 'ShipmentId', width: 40, align: 'Left', sortable: false },
                    { name: 'From', index: 'From', width: 120, align: 'Left', sortable: false },
                    { name: 'Recycler', index: 'Recycler', width: 120, align: 'Left', sortable: false },
                    { name: 'ShippedDate', index: 'ShippedDate', width: 80, sortable: false },
   		            { name: 'ReceivedDate', index: 'ReceivedDate', width: 80, sortable: false },
   		            { name: 'ShippingData', index: 'ShipmentId', width: 80, editable: false, sortable: false, align: 'center', formatter: reportShippingFmatter },
                    { name: 'DownloadFile', index: 'ShipmentId', width: 30, editable: false, sortable: false, align: 'center' },
   		            { name: 'AttachFile', index: 'ShipmentId', width: 30, editable: false, sortable: false, align: 'center' },
                    { name: 'Actions', index: 'Actions', width: 70, editable: false, sortable: false, align: 'center', formatter: actionFmatter },
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240



    });

    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    //if (affiliateType != 1 && affiliateType != 5 && affiliateType !=2) {
    //    jQuery("#jqGrid").jqGrid('hideCol', ["Actions"]);
    //}

    $("#btnSearch").click(function () {
        Search();

    });

});

function Search() {
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEngingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);

    jQuery("#jqGrid").setGridParam({ postData: {  BeginDate: strBeginingDate, EndDate: strEndingDate }, page: 1 }).trigger("reloadGrid");

}

function reportShippingFmatter(cellvalue, options, rowObject) {
    var ShipmentId = rowObject[5];
    var url = "return showReport('../reports/BulkReceivingReport.aspx?ShipmentId=" + ShipmentId + "', 'BulkReceivingReport');";
    var cell = "<div>";
    cell += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Shipping Report'  title='Bulk Receiving Report'  style='border:none;'/></a>";
    cell += "</div>";
    return cell;
}

function actionFmatter(cellvalue, options, rowObject) {
    if (affiliateType == 1 || affiliateType == 5) {
        var url = '/BulkShipment/Edit?Id=' + cellvalue;
        return "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' href='" + url + "' >Edit</a>";
    }
    else {
        return "";
    }
}

function openFileDialog(dataProcessReportId) {
    var w = window.open("../../Upload.aspx?processDataId=" + dataProcessReportId + "&attachfile=true", "Window1", "height=300,width=450,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");
}

function SaveAttachment(fileName, processDataId) {
    var rootPath = window.location.protocol + "//" + window.location.host;

    $.ajax({
        url: rootPath + '/BulkShipment/SaveAttachment',
        dataType: 'text',
        type: "POST",
        data: ({ fileName: fileName, processDataId: processDataId }),
        async: false,
        success: function (result) {
            $("#jqGrid").trigger("reloadGrid");
        }
    });
}

function GetAttachments(processDataId) {
    lastProcessDataId = processDataId;
    var rootPath = window.location.protocol + "//" + window.location.host;
    var docsHtml = "<ul>"

    $.ajax({
        url: rootPath + '/BulkShipment/GetAttachments',
        dataType: 'json',
        type: "POST",
        data: ({ processDataId: processDataId }),
        async: false,
        success: function (result) {
            docsHtml += "<table width='100%'>";
            $.each(result.rows, function (index, entity) {
                docsHtml += "<tr><td width='100%'><a href=javascript:Download('" + entity.UploadFileName + "'); style='color:#FF9900;text-decoration:none;_text-decoration:none;'>" + entity.UploadFileName + "</a></td>";
                docsHtml += "</tr>";
                //docsHtml += "<td width='15%'><img src='../../Content/images/icon/delete.png' alt='Remove attachment' style='cursor:hand;' title='Remove attachment' id='btnDel' border='0' onclick='javascript:DeleteAttachment(" + entity.DocumentID + ");' /></td></tr>";
            });

            docsHtml += "</table>"

        }
    });

    $("#dialogFiles").html(docsHtml);

    $("#dialogFiles").dialog({
        modal: false,
        width: 400,
        resizable: false,
        position: 'center'
    }
                );

}

function Download(fileName) {
    fileName = fileName.replace('#', '<~>');
    if (fileName != "-" && fileName != "") {
        $.ajax({
            url: '/FileUpload/IsFileExist?FileName=' + fileName,
            dataType: 'json',
            type: 'POST',
            //data: ({}),
            async: false,
            success: function (isFileExist) {
                if (isFileExist) {
                    window.location = "/FileUpload/Download?FileName=" + fileName;
                }
                else {
                    alert('This file does not exist.'); //data.toString()
                }
            }
        });
    }

}