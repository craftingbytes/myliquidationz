﻿$(document).ready(function () {

    var rootPath = window.location.protocol + "//" + window.location.host;
        var searchVal = '';
    var data = eval('(' + $("#Data").val() + ')');
    if (data.name) {
        searchVal = data.name;
        $("#txtSearch").val(data.name);
    
    }
    var entity = '0';
    if (data.type) {
        entity = data.type;
        $("#EntityTypeId").val(data.type);
    }
    var page = 1;
    var rows = 50;
    if (data.page) {
        page = data.page;
    }
    if (data.rows) {
        rows = data.rows;
    }

    jQuery("#jqGrid").jqGrid({
        url: "/Affiliate/GetAffiliates",
        datatype: 'json',
        mtype: 'GET',

        colNames: ['Id', 'Entity Name', 'Entity Type', 'Address', 'State', 'Phone', 'City', 'Actions'],
        colModel: [
   		{ name: 'Id', index: 'Id', width: 85, hidden: true },
   		{ name: 'Entity Name', index: 'Entity Name', width: 130, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
        { name: 'Entity Type', index: 'Entity Type', width: 75, editable: false, sortable: false },
   		{ name: 'Address', index: 'Address', width: 130, editable: false, sortable: false },
   		{ name: 'State', index: 'State', width: 65, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
   		{ name: 'Phone', index: 'Phone', width: 60, editable: false, sortable: false, editrules: { required: true }, editoptions: { size: 30, maxlength: 125} },
        { name: 'City', index: 'City', width: 65, editable: false, sortable: false, hidden:true, editrules: { required: true }, editoptions: { size: 30, maxlength: 100} },
        { name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: myformatter, title: true, align: 'center', width: 80, editable: false }

   	],
        rowNum: rows,
        page: page,
        rowList: [10, 25, 50, 100],
        pager: "#pager",
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 230,
        editurl: "/Affiliate/PerformAction",
        postData: {searchString: searchVal, searchEntity: entity }
        //afterInsertRow: function (rowid, rowdata, rowelem) {
        //    $('#' + rowid).contextMenu('MenuJqGrid', eventsMenu);
        //}
    });
    $("#jqGrid").jqGrid('navGrid', '#pager',
                { edit: false, add: false, del: false, search: false, refresh: true });

    $(".ui-paging-info", '#pager_left').remove();

});

function myformatter(cellvalue, options, rowObject) {
    return "<div>"
        + "<a href='javascript:editEntity(" + rowObject[0] + ")'><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit Entity'  style='border:none;'/></a>"
        + "<a href='javascript:editUsers(" + rowObject[0] + ")'><img src='../../Content/images/icon/user.png'  alt='Edit'  title='Add/Edit Entity Users'  style='border:none;'/></a>"
        + "<a href='javascript:deleteEntity(" + rowObject[0] + ")'><img src='../../Content/images/icon/delete.png'  alt='Edit'  title='Delete Entity' style='border:none;'/></a>"
        + "</div>";
}

$("#btnSearch").click(function () {
    var searchVal = $("#txtSearch").val();
    var entity = $("#EntityTypeId").val();          
    jQuery("#jqGrid").setGridParam({ postData: { searchString: searchVal, searchEntity: entity }, page: 1 }).trigger("reloadGrid");
});

$('input[id$=btnAdd]').click(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/Affiliate/Create/';
    window.location.href = url;
});

function editEntity(t) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/Affiliate/Edit/' + t;
    var name = $("#jqGrid").getGridParam("postData").searchString;
    var type = $("#jqGrid").getGridParam("postData").searchEntity;
    var page = $("#jqGrid").getGridParam("page");
    var rows = $("#jqGrid").getGridParam("rowNum");
    url = url + '?data=name:"' + name + '",type:"' + type + '",page:"' + page + '",rows:"' + rows + '"';
    window.location.href = url;
}

function editUsers(t) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + '/AffiliateContact/Index/' + t;
    var name = $("#jqGrid").getGridParam("postData").searchString;
    var type = $("#jqGrid").getGridParam("postData").searchEntity;
    var page = $("#jqGrid").getGridParam("page");
    var rows = $("#jqGrid").getGridParam("rowNum");
    url = url + '?data=name:"' + name + '",type:"' + type + '",page:"' + page + '",rows:"' + rows + '"';
    window.location.href = url;
}