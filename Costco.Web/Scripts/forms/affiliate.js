﻿function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var type;
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                type = oRequired.getAttribute('type');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || (oRequired.value == "0" && type != "text")) {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }

        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }

}

function LongLatByZip() {
    var zip = $("input[id$=Zip]").val();
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': ', ' + zip }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var geoResults = results[0];
            var point = geoResults.geometry.location;

            latitude = point.lat();
            longitude = point.lng();

            $("input[id$=Longitude]").val(longitude);
            $("input[id$=Latitude]").val(latitude);

            document.forms[0].submit();

            //keep in some hdden field

        } else {
            //InValidZipCode();
            //alert("invalid zip");
        }
    });
}

function SetWareHouseVisibity() {
    affiliateType = $("select#AffiliateTypeId").val();
    if (affiliateType == "4") {
        $("#RowParentId").show();
        $("#RowDefaultRecycler").show();
        $("#RowDefaultCarrier").show();
        $("#RowWarehoseNumber").show();
    }
    else if (affiliateType == "7") {
        $("#RowParentId").show();
        $("#RowDefaultRecycler").hide();
        $("#RowDefaultCarrier").hide();
        $("#RowWarehoseNumber").hide();
    }
    else {
        $("#RowParentId").hide();
        $("#RowDefaultRecycler").hide();
        $("#RowDefaultCarrier").hide();
        $("#RowWarehoseNumber").hide();
    }
}

$(document).ready(function () {
    var rootPath = window.location.protocol + "//" + window.location.host;

    if ($("#hidMess").text() == "true") {

        $("#saved").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "OK": function () {
                    var url = rootPath + '/Affiliate/Index/';
                    window.location.href = url;
                }
            }
        });
        return false;
    }

    $('select[id$=StateId]').change(function (elem) {

        state = $("select#StateId").val();
        if (state == "0") {
            $("select#CityId").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities/',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#CityId").fillSelect(result.data);
                    }
                }
            });
        }

    });

    $('select[id$=AffiliateTypeId]').change(function (elem) {

        affiliateType = $("select#AffiliateTypeId").val();
        if (affiliateType == "0") {
            $("select#ParentId").clearSelect();
            $("select#DefaultRecycler").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetAffiliateParents/',
                dataType: 'json',
                type: "POST",
                data: ({ affiliateType: affiliateType }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#ParentId").fillSelect(result.data);
                    }
                }
            });

            $.ajax({
                url: rootPath + '/Affiliate/GetRecyclers/',
                dataType: 'json',
                type: "POST",
                data: ({ affiliateType: affiliateType }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#DefaultRecycler").fillSelect(result.data);
                    }
                }
            });

            $.ajax({
                url: rootPath + '/Affiliate/GetCarriers/',
                dataType: 'json',
                type: "POST",
                data: ({ affiliateType: affiliateType }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        $("select#DefaultCarrier").fillSelect(result.data);
                    }
                }
            });
        }
        if (affiliateType == "4") {
            $("#RowParentId").show();
            $("#RowDefaultRecycler").show();
            $("#RowDefaultCarrier").show();
            $("#RowWarehoseNumber").show();
        }
        else if (affiliateType == "7") {
            $("#RowParentId").show();
            $("#RowDefaultRecycler").hide();
            $("#RowDefaultCarrier").hide();
            $("#RowWarehoseNumber").hide();
        }
        else {
            $("#RowParentId").hide();
            $("#RowDefaultRecycler").hide();
            $("#RowDefaultCarrier").hide();
            $("#RowWarehoseNumber").hide();
        }

    });

    $('input[id$=btnClear]').click(function () {

        $(this.form).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                    $(this).val(0);
                    break;
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                    break;
            }
        });
        var state = $("select#StateId").val();
        if (state == "0") {
            $("select#CityId").clearSelect();
        }
        $("#validationSummary").html("");
    });

    $('#btnCancel').click(function () {
        var url = rootPath + '/Affiliate/Index/';
        window.location.href = url;

    });

    $('input[id$=btnSubmit]').click(function () {

        var isValid = ValidateForm(document.forms[0]);
        if (isValid) {
            LongLatByZip();
        }
        else {
            return false;
        }
    });

    SetWareHouseVisibity();
});