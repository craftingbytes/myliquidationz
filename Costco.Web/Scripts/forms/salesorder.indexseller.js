﻿var affiliateType = $("#hfAffiliateType").val();
var rootPath = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    $("button, input:submit, input:button").button();


    $("#txtBeginDate").datepicker();
    //$('#txtBeginDate').datepicker('setDate', new Date());
    $("#txtEngingDate").datepicker();
    //$('#txtEngingDate').datepicker('setDate', new Date());



    jQuery("#jqGrid").jqGrid({
        url: "/SalesOrder/GetSaleOrdersSellerList",
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Id', 'ASR','Created', 'SoldDate', 'Sold To', 'SoldToAffiliateId', 'Items', 'Count', 'Amount', 'Invoiced' ,'Edit'],
        colModel: [
                    { name: 'SalesOrderId', index: 'SalesOrderId', width: 40, align: 'center', sortable: false },
                    { name: 'SoldBy', index: 'SoldBy', width: 120, align: 'Left', sortable: false, hidden: false  },
   		            { name: 'Created', index: 'Created', width: 80, align: 'center', sortable: false },
                    { name: 'SoldDate', index: 'SoldDate', width: 80, align: 'center', sortable: false },
                    { name: 'SoldTo', index: 'SoldTo', width: 120, align: 'Left', sortable: false, hidden: false },
                    { name: 'SoldToAffiliateId', index: 'SoldToAffiliateId', width: 120, align: 'Left', sortable: false, hidden: true },
   		            { name: 'Items', index: 'Items', width: 80, editable: false, sortable: false, align: 'center', formatter: reportFmatter },
   		            { name: 'BidCount', index: 'BidCount', width: 80, editable: false, sortable: false, align: 'center', hidden:true },
                    { name: 'Amount', index: 'Amount', width: 80, align: 'right', editable: true, hidden: false, sortable: false, formatter: 'number', formatoptions: { decimalPlaces: 2} },
                    { name: 'Invoiced', index: 'Invoiced', sortable: false, width: 30, align: 'center', formatter: InvoicedCheckBoxFormatter },
                    { name: 'Edit', index: 'Edit', width: 80, align: 'right', editable: false, sortable: false, align: 'center', formatter: fmatter }
   	              ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: jQuery('#pager'),
        viewrecords: true,
        rownumbers: true,
        width: 910,
        height: 240



    });

    $("#jqGrid").jqGrid('navGrid', '#pager2',
                { edit: false, add: false, del: false, search: false, refresh: true }, {}, { closeOnEscape: true, reloadAfterSubmit: true, closeAfterAdd: true, left: 450, top: 450, width: 400 });

    $("#btnSearch").click(function () {
        Search();
    });


});

function Search() {
    var strBeginingDate = $("#txtBeginDate").val();
    var strEndingDate = $("#txtEngingDate").val();

    var beginingDate = new Date(strBeginingDate);
    var endingDate = new Date(strEndingDate);
    var beginId = $("#txtBeginId").val();
    var endId = $("#txtEndId").val();

    var auctionType = $("#ddlAuctionType").val();
    jQuery("#jqGrid").setGridParam({ postData: { BeginDate: strBeginingDate, EndDate: strEndingDate, BeginId: beginId, EndId: endId, AuctionType: auctionType }, page: 1 }).trigger("reloadGrid");

}

function reportFmatter(cellvalue, options, rowObject) {
    var salesOrderId = rowObject[0];
    var cell = "<div>";
    var url = "return showReport('../reports/SalesOrderItemReport.aspx?SalesOrderId=" + salesOrderId + "', 'SalesOrderItemReport');";
    cell += "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + url + "\" href='#' ><img src='../../Content/images/report.png'  alt='Sales Order Report'  title='Sales Order Report'  style='border:none;'/></a>";
    cell += "</div>";
    return cell;
}

function fmatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var cell = "";
    if (cellvalue == "Open")
        cell = "<div><a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick='javascript:editSalesOrder(" + rowid + ")' href='#' ><img src='../../Content/images/icon/edit.png'  alt='Edit'  title='Edit'  style='border:none;'/></a>";
    else
        cell = "&nbsp;";
    return cell;
}

function editSalesOrder(salesOrderId) {
    var rootPath = window.location.protocol + "//" + window.location.host;
    var url = rootPath + "/SalesOrder/EditSeller?Id=" + salesOrderId;
    window.location.href = url;
}

function InvoicedCheckBoxFormatter(cellvalue, options, rowObject) {
    var rowid = options.rowId;
    var salesOrderId = rowObject[0];
    if (cellvalue == 1)
        return "<input type='checkbox' Name='Approved' id='invoicedCheck" + rowid + "' onclick=\"UpdateInvoiced(" + rowid + "," + salesOrderId + ")\"  checked />";
    else
        return "<input type='checkbox' Name='Approved' id='invoicedCheck" + rowid + "' onclick=\"UpdateInvoiced(" + rowid + "," + salesOrderId + ")\"  />";
}

function UpdateInvoiced(rowId, salesOrderId) {
    var chkbox = document.getElementById("invoicedCheck" + rowId);
    $.ajax({
        url: '/SalesOrder/UpdateInvoiced',
        dataType: 'json',
        type: "POST",
        data: "id=" + salesOrderId,
        async: false,
        success: function (result) {
            if (result.success) {
                //alert("Success:SalesOrder Updated");
                //reformatActionMenu(rowId, invoiceid, result.invoiced, result.invoicedate);

            }
            else {
                alert("Update Failed:" + result.message);
                chkbox.checked = !(chkbox.checked);
            }
        }

    });
}

