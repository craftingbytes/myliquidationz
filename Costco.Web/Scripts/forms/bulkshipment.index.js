﻿var lastSel;
var inEditMode = false;

var rootPath = window.location.protocol + "//" + window.location.host;
var deviceList;
var inEditMode = false;

$(function () {

    //InitGrid();


    $("#btnAdd").button().click(function () {
        addRow();
    });

    $("#btnSubmit").button().click(function () {


        $("#validationSummary").html("");
        var isValid = ValidateForm(); //true;


        var isFocused = false;
        isFocused = false;

        if (!isValid) {
            return false;
        }


        $("#dialog-choice").removeClass('ui-icon ui-icon-circle-check');
        if (inEditMode) {
            alert('There is an item opened in edit mode. Please save or cancel it first.');
            return false;
        }
        $("#dialog-choice").dialog({
            modal: true,
            width: 400,
            resizable: false,
            position: 'center',
            buttons: {
                "No": function () {
                    window.location = '#';
                    $(this).dialog("close");
                },
                "Yes": function () {
                    $("form").attr("action", rootPath + "/BulkShipment/Create");
                    //compileGridData();
                    $("form").submit();
                    $(this).dialog("close");
                }
            }
        });


    });

    $('input[id$=txtReceivedDate]').datepicker({});
    $('input[id$=txtReceivedDate]').datepicker('setDate', new Date());

    $('input[id$=txtShippededDate]').datepicker({});
    $('input[id$=txtShippededDate]').datepicker('setDate', new Date());
});

function InitGrid() {
    $("#jqGrid").jqGrid({
        //data: mydata,
        datatype: 'json',
        mtype: 'GET',
        url: rootPath + '/BulkShipment/InitItemGrid',
        height: 150,



        colNames: ['Store #', 'ItemId', 'Description', 'Condition','Comment', 'Qty','CED', 'Disposition', 'Actions'],
        colModel: [
   		            { name: 'StoreNumber', index: 'StoreNumber', editable: true, width: 40, sortable: false, editrules: { integer: true, required: true }, align: 'right'
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { parseStoreNumber() }} ] }
                    },
                    { name: 'ItemId', index: 'ItemId', editable: true, width: 200, sortable: false, editrules: { required: true}  
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { getDescription() }} ] }
                    },
   		            { name: 'Description', index: 'Description', editable: true, width: 200, sortable: false, editrules: { required: true} },
                    { name: 'Condition', index: 'Condition', editable: true, width: 200, sortable: false, editrules: { required: true} 
                            , editoptions: { dataEvents: [ {type: 'change', fn: function(e) { getCondition() }} ] }
                    },
                    { name: 'ReceivedCondition2', index: 'ReceivedCondition2', editable: true, width: 200, sortable: false, editrules: { required: false} },
                    { name: 'Qty', index: 'Qty', editable: true, width: 50, sortable: false, editrules: { required: true, integer: true }, align: 'right' },
                    { name: 'CoveredDevice', index: 'CoveredDevice', editable:true, width: 30, edittype:'checkbox', formatter: "checkbox"
                            , editoptions: { value:"true:false" }
                    },
                    { name: 'Disposition', index: 'Disposition', editable: true, width: 100, sortable: false, editrules: { required: true} },
                    {name: 'ActionMenu', index: 'ActionMenu', sortable: false, formatter: ActionMenuFormatter, title: true, align: 'center', width: 70, editable: false }

   	            ],
        rowNum: -1,
        viewrecords: true,
        rownumbers: true,
        footerrow: true,
        pgbuttons: true,
        editurl: rootPath + '/Shipment/AddProductItem',
        width: 925,
        height: 300,
        afterInsertRow: function (rowid, aData) {
            be = "<input id='" + rowid + "_edRow' style='height:22px;padding:0' type='button' value='Edit' onclick=\"editRow('" + rowid + "');\" title='Edit Record' />";
            se = "<input id='" + rowid + "_savRow' style='height:22px;padding:0' type='button' value='Save' onclick=\"saveRow('" + rowid + "');\" title='Save Record' />";
            ce = "<input id='" + rowid + "_canRow' style='height:22px;padding:0' type='button' value='Cancel' onclick=\"restoreRow('" + rowid + "');\" title='Cancel Edit' />";
            $("#jqGrid").jqGrid('setRowData', rowid, { Action: be + se + ce });
            $("input[type=button]").button();
        },

        loadComplete: function () {
            //$('#Description').change(popacct); 
        }
    });

}

function addRow()
{
    jQuery("#jqGrid").jqGrid('editGridRow', 'new', {
            height: 280,
            recreateForm: true,
            reloadAfterSubmit: false, 
            addCaption: "Add Record",
		    editCaption: "Edit Record",
		    bSubmit: "Add",
		    bCancel: "Done",
		    bClose: "Close",
		    saveData: "Data has been changed! Save changes?",
		    bYes : "Yes",
		    bNo : "No",
		    bExit: "Done",
            modal: true,
            addedrow: 'last',
            closeAfterEdit: false,
            afterShowForm: function (formid){
                setDisposition();
            },
            afterComplete: function(response, postdata,formid){
                setDisposition();
                return true;
            },
            viewPagerButtons: false //,

    });
}

function setDisposition() {
    var rowCount = $("#jqGrid").getGridParam('reccount');
    if (rowCount == 0) {
        $('#Disposition').val('');
    }
    else {
        var rowData = $("#jqGrid").getRowData(rowCount);
        var disposition = rowData.Disposition;
        $('#Disposition').val(disposition);
    }


}


function parseStoreNumber() {
    storeNumber = $('#StoreNumber').val();
    if (storeNumber.length > 4)
        storeNumber = storeNumber.substring(0,4);
    storeNumber = storeNumber.replace(/^[0]+/g,"");
    $('#StoreNumber').val(storeNumber);
    return [true,'']; 
}

function parseBarCode() {
    storeNumber = $('#StoreNumber').val();
    var chunks = storeNumber.split(' ');
    alert(chunks.length.toString());
    if (chunks.length > 1)
    {
        $('#StoreNumber').val(chunks[0]);
        $('#ItemId').val(chunks[1]);
    }
    getDescription();
    return [true,'']; 
}

function getDescription() { 
    var itemId =  $('#ItemId').val();
    $.ajax({
        url: "/Shipment/GetProductDescription",
        dataType: 'json',
        type: "GET",
        data: ({ itemId: itemId }),
        async: false,
        cache: false,
        success: function (result) {
            if (result.description == "")
            {
                $('#Description').val("");
                $('#Description').focus();    
            }
            else
            {
                $('#Description').val(result.description);
                if (result.coveredDevice)
                    $("#CoveredDevice").attr('checked', true);
                else
                    $("#CoveredDevice").attr('checked', false);
                //$('#CoveredDevice').val(result.coveredDevice);
                $('#Condition').focus(); 
            }
        }
    });

    return [true,'']; 
}

function EditRow(rowId) {
    if (rowId && rowId != lastSel) {
        //CancelRow(lastSel);
        lastSel = rowId;
    }
    $('#ED_ActionMenu_' + rowId).css({ display: "none" });
    $('#SC_ActionMenu_' + rowId).css({ display: "block" });
    //$('#jqGrid').editRow(rowId);
    //jQuery("#jqGrid").editGridRow("rowId",);
    jQuery("#jqGrid").jqGrid('editGridRow', rowId, {
                height: 280,
                recreateForm: true,    
                reloadAfterSubmit: false, 
                addCaption: "Add Record",
		        editCaption: "Edit Record",
		        bSubmit: "Save",
		        bCancel: "Cancel",
		        bClose: "Close",
		        saveData: "Data has been changed! Save changes?",
		        bYes : "Yes",
		        bNo : "No",
		        bExit: "Cancel",
                modal: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                onClose: function (response, postdata) { 
                                CancelRow(rowId);
                            },
                afterComplete: function() {
                    SaveRow(rowId);
                    //addRow();
                }

               });
    inEditMode = true;
}

function SaveRow(rowId) {
    //$('#jqGrid').saveRow(rowId, AfterSaveRow);

    if ($("#jqGrid").saveRow(rowId, false, 'clientArray')) {
        var newData = $("#jqGrid").getRowData(rowId);
        if (parseInt(newData["Qty"]).toString() == 'NaN') {
            return;
        }
        else {
            inEditMode = false;
            $('#ED_ActionMenu_' + lastSel).css({ display: "block" });
            $('#SC_ActionMenu_' + lastSel).css({ display: "none" });
        }
    }
}

function CancelRow(rowId) {
    $('#ED_ActionMenu_' + rowId).css({ display: "block" });
    $('#SC_ActionMenu_' + rowId).css({ display: "none" });
    //$('#jqGrid').restoreRow(rowId);

    $.fn.fmatter.rowactions(rowId, 'jqGrid', 'cancel', false);
    var rowData = $("#jqGrid").getRowData(rowId);
    if (rowData.ItemId == "") {
        jQuery("#jqGrid").jqGrid('delRowData', rowId);
        lastSel = lastSel - 1;
    }
    inEditMode = false;
}

function DeleteRow(rowId){
    jQuery("#jqGrid").jqGrid('delRowData', rowId);
}

function ActionMenuFormatter(cellvalue, options, rowdata) {
    var actionMenuHtml = "<div id='ED_ActionMenu_" + options.rowId + "'><img src='../../Content/images/icon/edit.png' style='cursor:hand;'  alt='Edit'  title='Edit' onclick='javascript:EditRow(\"" + options.rowId + "\")' style='border:none;' class='edit'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Delete'  title='Delete'  style='border:none;' onclick='javascript:DeleteRow(\"" + options.rowId + "\")'/></div>";
    actionMenuHtml += "<div id='SC_ActionMenu_" + options.rowId + "' style='display:none;'><img src='../../Content/images/icon/save.png' style='cursor:hand;' alt='Save'  title='Save' onclick='javascript:SaveRow(\"" + options.rowId + "\")' style='border:none;'/>&nbsp;&nbsp;<img src='../../Content/images/icon/Cancel.png' style='cursor:hand;' alt='Cancel'  title='Cancel'  style='border:none;' onclick='javascript:CancelRow(\"" + options.rowId + "\")'/></div>";
    return actionMenuHtml;
}

function ValidateForm(form) {
    var oForm;
    if (!form) form = 0; //default to first form
    switch (typeof form) {
        case 'object':
            oForm = form;
            break;
        case 'number':
            oForm = document.forms[form];
            break;
        default:
            oForm = document.forms[0];
            break
    }
    var dynamicHTML = "";
    dynamicHTML += "<ul class=ValidationSummary>";

    var oRequired;
    var sRequired = '';
    var bValidated = true;
    if (oForm) {
        for (var iControl = 0; iControl < oForm.length; iControl++) {
            oRequired = oForm[iControl];

            if (oRequired) {

                sRequired = oRequired.getAttribute('required');
                if (sRequired) {//is it required
                    try {
                        if (!oRequired.value || $.trim(oRequired.value) == '' || oRequired.value == "0") {//is it empty
                            bValidated = false;
                            oRequired.style.backgroundColor = '#FFEEEE';
                            dynamicHTML += "<li>" + sRequired + "</li>"


                        } else {
                            oRequired.style.backgroundColor = '#FFFFFF';
                        }
                    } catch (err) {
                        bValidated = false;

                    }
                }
                var regularExpAtt = oRequired.getAttribute('regexp');
                if (regularExpAtt) {
                    var reg = regularExpAtt;
                    if ($.trim(oRequired.value) != '' && oRequired.value.search(reg) == -1) //if match failed
                    {
                        bValidated = false;
                        oRequired.style.backgroundColor = '#FFEEEE';
                        dynamicHTML += "<li>" + oRequired.getAttribute('regexpmesg') + "</li>";
                    }
                }
            }
        }
        dynamicHTML += "</ul>";
        if (bValidated == false) {
            $("#validationSummary").html(dynamicHTML);
        }
        return bValidated;
    }
}

function compileGridData() {
    //get total rows in you grid
    var numberOfRecords = $("#jqGrid").getGridParam("records");
    var lineItems = '';
    for (i = 1; i <= numberOfRecords; i++) {

        var rowData = $("#jqGrid").getRowData(i);
        lineItems = 'StoreNumber:' + rowData['StoreNumber'];
        lineItems += ',ItemId:' + rowData['ItemId'];
        lineItems += ',Description:' + rowData['Description'];
        lineItems += ',Qty:' + rowData['Qty'];
        lineItems += ',Condition:' + rowData['Condition'];
        lineItems += ',ReceivedCondition2:' + rowData['ReceivedCondition2'];
        lineItems += ',CoveredDevice:' + rowData['CoveredDevice'];
        lineItems += ',Disposition:' + rowData['Disposition'];
        $('<input type="hidden" />').attr('name', 'Item_' + i).attr('value', lineItems).appendTo('#hiddenDivId');
    }
}

function setStoreNumber() {
    var rowCount = $("#jqGrid").getGridParam('reccount');
    if (rowCount == 0)
    {
        //$('#PalletUnitNumber').val(1);
    }
    else 
    {
        var rowData = $("#jqGrid").getRowData(rowCount);
        var storeNumber = rowData.StoreNumber;
        $('#StoreNumber').val(storeNumber);
    }
}
         
function getCondition() {
    var condition = $('#Condition').val();
    if (condition == "Like New" ||
        condition == "Used" ||
        condition == "Repairable" ||
        condition == "Scratch and Dent" ||
        condition == "Damaged(parts)" ||
        condition == "Destroyed" ||
        condition == "Opened/Damaged box" ||
        condition == "Untested" ||
        condition == "Incomplete" ||
        condition == "Display Unit" ||
        condition == "Pulled from floor" ||
        condition == "Other")
    {
        //do nothing
    } 
    else if (condition == "1")
         $('#Condition').val("Like New");
    else if (condition == "2")
         $('#Condition').val("Used");
    else if (condition == "3")
         $('#Condition').val("Repairable");
    else if (condition == "4")
         $('#Condition').val("Scratch and Dent");
    else if (condition == "5")
         $('#Condition').val("Damaged(parts)");
    else if (condition == "6")
         $('#Condition').val("Destroyed");
    else if (condition == "7")
         $('#Condition').val("Opened/Damaged box");
    else if (condition == "8")
         $('#Condition').val("Untested");
    else if (condition == "9")
        $('#Condition').val("Incomplete");
    else if (condition == "10")
        $('#Condition').val("Display Unit");
    else if (condition == "11")
        $('#Condition').val("Pulled from floor");
    else if (condition == "99")
         $('#Condition').val("Other");
    else
        $('#Condition').val("Other");
}

function GetCities(elem) {
    if (elem != null && elem.name != null) {
        state = $("select#" + elem.name).val();
        if (state == "0") {
            if (elem.name == "ddlPickupState")
                $("select#ddlPickupCity").clearSelect();
            else
                $("select#ddlShipToCity").clearSelect();
        }
        else {
            $.ajax({
                url: rootPath + '/Affiliate/GetCities',
                dataType: 'json',
                type: "POST",
                data: ({ state: state }),
                async: false,
                success: function (result) {
                    if (result.data) {
                        if (elem.name == "ddlPickupState")
                            $("select#ddlPickupCity").fillSelect(result.data);
                        else
                            $("select#ddlShipToCity").fillSelect(result.data);

                    }
                }
            });
        }
    }
}