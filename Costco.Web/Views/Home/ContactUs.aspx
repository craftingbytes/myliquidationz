﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master"  Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>
<div class="rs">
                    <h2 class="title">Contact us</h2>
                        <div id="content">
                        <div class="post">
                          
                     
              <div class="entry">
                            <div style="padding-top:5px; width:40%; float:left">
                                <h3>
                                    E-WORLD ONLINE</h3>
                                1390 Engineer St<br />
                                Vista, Ca 92081<br /><br />
                                Office: (760) 599-0888<br />
                                Fax: (760) 599-0840
                                <br />
                                Email: <a href="mailto:lyle@eworldrecyclers.com">lyle@eworldrecyclers.com</a><br />
                                <br />
                                Toll free:<span style="color: #0072BC; font-size: medium; font-weight: bold">877-342-6756</span>
                            </div>    
            </div> 
</div> 
</asp:Content>

