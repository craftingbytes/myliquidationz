﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

   
    <script type="text/javascript">
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        // open hidden layer
        function mopen(id) {
            // cancel close timer
            mcancelclosetime();

            // close old layer
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';

            // get new layer and show it
            ddmenuitem = document.getElementById(id);
            ddmenuitem.style.visibility = 'visible';

        }
        // close showed layer
        function mclose() {
            if (ddmenuitem) ddmenuitem.style.visibility = 'hidden';
        }

        // go close timer
        function mclosetime() {
            closetimer = window.setTimeout(mclose, timeout);
        }

        // cancel close timer
        function mcancelclosetime() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        // close layer when click-out
        document.onclick = mclose; 

    </script>
               
       
                    <div id="content">
                        <div class="post">
                            <h2 class="title">
                                Costco RTV shipped through E-World Recyclers FAQ’s</h2>
                            <div class="entry">
                                <div class="Section1">
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            What can we ship through this program?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            All electronics(anything that has a cord or runs on batteries), furniture and equipment can be shipped.<span style='mso-spacerun: yes'> </span>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            What is not acceptable?<o:p></o:p></span></u>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Food, books, clothing, liquids, oils, hazardous material, light bulbs or batteries.<o:p></o:p></span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            What if there is a questionable item?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Please contact Lyle De Stigter – 760-599-0888 ext 205 or <a href="mailto:info@e-worldonline.com">lyle@eworldrecyclers.com</a> .<span style='mso-spacerun: yes'> </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>

                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            How should the material be prepared?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Do not break, scratch, paint or otherwise deface or damage the item. Items can be shipped the same way they were returned from the customer unless there are oils or liquids in the items which will need to be separated prior to shipping. If the items are in retail boxes send them that way don’t un-box. .<span style='mso-spacerun: yes'> </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>

                                     <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            What if an item is already broken?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            Try to keep all of the parts together either in a box or taped together.<span style='mso-spacerun: yes'> </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>

                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <u><span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            How should a pallet be loaded?<o:p></o:p></span></u></p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            If using a Gaylord/Pallet box place heavy, large and bulky items at the bottom, medium or lighter items on top of the larger ones. Small items, if loose, should be placed into boxes or bags and put on the top of the pallets to prevent them falling through to the bottom of the box/pallet and possibly breaking or falling out of the bottom.. Once the pallet is ready to ship wrap tightly bottom to top with stretch film with a packing list between the layers of film.
If the item is too large to go into a box then use twine to strap to a pallet then stretch film tightly with a packing list between the layers of film.
LCD TVs are to be handled carefully to prevent breaking. Do not put them in the bottom of a box. Do not stack items on top of them. Whenever possible keep them on the top of the pallet with the screen protected
.<span style='mso-spacerun: yes'> </span>
                                            <o:p></o:p>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style='vertical-align: middle'>
                                        <span style='font-size: 11.0pt; font-family: "Calisto MT","serif"; mso-bidi-font-family: "Lucida Sans Unicode"'>
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                    </p>

                                </div>
                            </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;</div>
                    </div>
                    <!-- end #content -->
                    <!-- end #sidebar -->
                    <div style="clear: both;">
                        &nbsp;</div>
      
        <!-- end #page -->
  

</asp:Content>

