﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />-->
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Search Shipments</h1>
        <table width="100%" style="margin-top: 20px;">
            <tr>
                <td align="center">
                    <fieldset style="width: 80%">
                    <legend><b>Search Criteria</b></legend>
                        <table style="margin-top:10px">
                            <tr>
                                <td align="right">
                                    Beginning Date:
                                </td>
                                <td align="left">
                                    <input type="hidden" id="Data" name="Data" value='<%=ViewData["Data"] %>'/>
                                    <input type="text" id="txtBeginDate" class="date-pick" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Ending Date:
                                </td>
                                <td>
                                    <input type="text" id="txtEndingDate" class="date-pick" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Warehouse:
                                </td>
                                <td>
                                    <input type="text" id="txtWarehouse" style="width:80px;" />
                                   
                                </td>
                            </tr>
                            <tr>
                                
                                <td align="right">
                                    Beginning Id:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginId" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Ending Id:
                                </td>
                                 <td align="left">
                                    <input type="text" id="txtEndId" style="width:80px;" />
                                </td>
                                <td colspan="2" align="right">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" style="width:90px;" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <input type="button" id="btnAdd" name="btnAdd" value="Add Shipment"  class="ui-button ui-widget ui-state-default ui-corner-all" />
        <br />
        <table id="jqGrid">
        </table>
        
        <div id="pager">
        </div>
        </div>
            <div id="dialog" title="Invalid Dates!" style="display: none;">
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">Begining Date cannot be bigger than Ending Date.</span>
        </div>

        <div id="dialogFiles" title="Attached files" style="display: none;">
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">
                <ul id="ulFiles">
           
                </ul>
                </span>
        </div>
        <div id="dialog-ship" title="Confirm Status Change!" style="display: none;">
         <label for="txtShipDate">Shipment Date</label>   
         <input id="txtShipDate" name="txtShipDate" req="true" required="Shipping Date is required." style="width:100px"/>
         <br />
         <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">
                <br />You are about to change the status to Shipped.
                <br />You will not be able to edit the shipment afterwards.
                <br />
                Do you want to proceed ?</span>
        </p>
        </div>

        <div id="dialog-revert" title="Confirm Status Change!" style="display: none;">
         <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">
                <br />You are about to revert the shipping status to "Not Shiped"
                Do you want to proceed ?</span>
        </p>
        </div>
        <div id="dialog-delete" title="Confirm Deletion!" style="display: none;">
         <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">
                <br />You are about to permanently delete this shipment.
                Do you want to proceed ?</span>
        </p>
        </div>
    <input id="hfAffiliateType" name="hfIsOem"  type="hidden" />
    <script type="text/javascript">
        $("#hfAffiliateType").val('<%=ViewData["AffiliateType"] %>');
    </script>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/shipment.list.js?version=1.12")%>' type="text/javascript"></script>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/ui.dropdownchecklist.js")%>' type="text/javascript"></script>-->
</asp:Content>

