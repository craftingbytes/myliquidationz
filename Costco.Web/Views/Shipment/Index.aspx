﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <h1 class="heading">Shipping Form</h1>
    <table cellpadding="3" width="100%" border="0">
        <tr>
            <td style="padding-left: 130px;" colspan="2">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
                <div id="divDocuments" title="Download Attachments">
                    <ul id="ulAttachment" style="display:none;">
                        <li style="height:20px;">
                        <a href="javascript:Download($('#txtUploadedFileName').val());" style="color:#FF9900;text-decoration:none;_text-decoration:none;">
                            <img src='../../Content/images/d4.PNG' alt='Open selected file' title='Open selected file' id='btnDel' border="0"/>
                            : <font style="font-weight: bold;vertical-align:top;"> <label id="lblFileName"></label></font>
                            </a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <td style = "width: 45%">
                <fieldset style="float: left; height: 220px; width: 95%;">
                    <legend style="padding: 5px;"><b>Pickup Address</b></legend>
                    <table cellpadding="3" style="width: 100%">
                        <tr>
                            <td align="right" style="width: 15%">
                                <label for="txtAddress">
                                    <font color='red'>*</font><b>Company :</b>
                                </label>
                            </td>
                            <td  style="width: 35%">
                                <input type="text" id="txtAffiliateName" name="txtAffiliateName" value="<%=ViewData["txtAffiliateName"] %>" req="true" required="Company Name is required." style="width:150px" READONLY  />
                            </td>
                            <td style="width: 15%">&nbsp;</td>
                            <td style="width: 35%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label for="txtAddress">
                                     <font color='red'>*</font><b>Address :</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtPickupAddress" name="txtPickupAddress" value="<%=ViewData["txtPickupAddress"] %>" req="true" required="Pickup Address is required." style="width:150px" READONLY/>
                            </td>
                            <td align="right">
                                <label for="txtState">
                                     <font color='red'>*</font><b>State :</b>
                                </label>
                            </td>
                            <td>
                                <%= Html.DropDownList("ddlPickupState", (SelectList)ViewData["PickupState"], new { style = "width:120px", onchange = "GetCities(this);", required = "Pickup State is required." })%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label for="txtCity">
                                     <font color='red'>*</font><b>City :</b>
                                </label>
                            </td>
                            <td>
                                <%= Html.DropDownList("ddlPickupCity", (SelectList)ViewData["PickupCity"], new { style = "width:120px", required = "Pickup City is required." })%>
                            </td>
                            <td align="right">
                                <label for="txtZip">
                                    <b>Zip:</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtPickupZip" name="txtPickupZip" value="<%=ViewData["txtPickupZip"] %>" maxlength="5" regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                                    regexpmesg="Invalid Zip format, Please try xxxxx."  style="width:100px" READONLY />
                            </td>
                        </tr>     
                    </table>
                </fieldset>
            </td>
            <td style = "width: 45%">
                <fieldset style="float: left; height: 220px; width: 95%;">
                    <legend style="padding: 5px;"><b>Shipping Info</b></legend>
                    <table cellpadding="3" style="width: 100%">
                        <tr>
                            <td align="right" style="width: 15% ">
                                <label>
                                     <font color='red'>*</font><b>Shipped By:</b>
                                </label>
                            </td>
                            <td style="width: 35% ">
                                <input type="text" id="txtShippedBy" name="txtShippedBy"  req="true" required="Shipped By is required." style="width:150px" />
                            </td>
                            <td align="right" style="width: 15% ">
                                <label for="txtShipDate">
                                     <font color='red'>*</font><b>Shipping Date:</b>
                                </label>
                            </td>
                            <td style="width: 35% ">
                                <input type="text" id="txtShipDate" name="txtShipDate" req="true" required="Shipping Date is required." style="width:100px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label>
                                     <font color='red'>*</font><b><b>Total Weight:</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtTotalWeight" name="txtTotalWeight" maxlength="10" regexp="^[0-9]\d*(\.\d+)?$" value="0"
                                    regexpmesg="Invalid value. Enter valid number for weight."  style="width:100px"  disabled = "disabled"/>
                                
                            </td>
                            <td align="right">
                                <label>
                                     <font color='red'>*</font><b><b>Carrier:</b>
                                </label>
                            </td>
                            <td>
                                <%= Html.DropDownList("ddlCarrierList", (SelectList)ViewData["CarrierList"], new { style = "width:104px", required = "Carrier is required." })%>
                                <input type="text" id="txtCarrier" name="txtCarrier" READONLY />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label>
                                     <b>Comments:</b>
                                </label>
                            </td>
                            <td colspan=3 >
                                <textarea id="txtShipComments" name="txtShipComments" rows="5" style="width: 300px;" cols="20"></textarea>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style = "width: 45%">
                <fieldset style="float: left; height: 220px; width: 95%;">
                    <legend style="padding: 5px;"><b>Ship To Address</b></legend>
                    <table cellpadding="3" style="width: 100%">
                        <tr>
                            <td align="right" style="width: 15%">
                                <label for="txtRecyclerName">
                                    <font color='red'>*</font><b>Company :</b>
                                </label>
                            </td>
                            <td colspan="3" >
                                <%= Html.DropDownList("ddlRecyclerList", (SelectList)ViewData["RecyclerList"], new { style = "width:104px", required = "Recycler is required." })%>
                                <input type="text" id="txtRecyclerName" name="txtRecyclerName" value="<%=ViewData["txtRecyclerName"] %>" req="true" required="Recycler Name is required." style="width:150px" READONLY />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label for="txtShipToAddress">
                                     <font color='red'>*</font><b>Address :</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtShipToAddress" name="txtShipToAddress" value="<%=ViewData["txtShipToAddress"] %>" req="true" required="Ship to Address is required." style="width:150px" READONLY />
                            </td>
                            <td align="right">
                                <label for="txtState">
                                     <font color='red'>*</font><b>State :</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtShipToState" name="txtShipToState" READONLY />
                                <%= Html.DropDownList("ddlShipToState", (SelectList)ViewData["ShipToState"], new { style = "width:104px", onchange = "GetCities(this);", required = "Ship to State is required." })%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label for="txtCity">
                                     <font color='red'>*</font><b>City :</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtShipToCity" name="txtShipToCity" READONLY />
                                <%= Html.DropDownList("ddlShipToCity", (SelectList)ViewData["ShipToCity"], new { style = "width:104px", required = "Ship to City is required."  })%>
                            </td>
                            <td align="right">
                                <label for="txtZip">
                                    <b>Zip:</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtShipToZip" name="txtShipToZip" value="<%=ViewData["txtShipToZip"] %>" maxlength="5" regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                                    regexpmesg="Invalid Zip format, Please try xxxxx."  style="width:100px" READONLY />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <div style="width: 95%; margin-left:5px; ">
        <br />
        <!--<input type="button" id="btnAdd" value="Add" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        <br />-->
        <table style="width: 95%; margin-left:5px; ">
            <tr>
                <td align="center">
                    <label for="btnSubmit">
                        <b>Fill in pickup and shipping info and click Create to start.</b>
                        <br />
                    </label>
                    <input type="button" id="btnSubmit" value="Create" class="fr ui-button ui-widget ui-state-default ui-corner-all" />
                </td>
            </tr>
        </table>
        <div class="cb">
        </div>
        <br />
    </div>
    <div id="hiddenDivId">
    </div>
    <div id="dialog-choice" title="Confirm Submit!" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">You are about to create a new shipment.<br />
                <br />
                Do you want to proceed ?</span>
        </p>
    </div>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/shipment.index.js?version=1.4")%>' type="text/javascript"></script>
</asp:Content>

