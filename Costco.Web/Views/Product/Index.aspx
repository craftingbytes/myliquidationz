﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />-->
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Products</h1>
        <table width="100%" style="margin-top: 20px;">
        <tr>
                <td align="center">
                    <br />
                    <fieldset style="width: 90%">
                    <legend><b>Search Criteria</b></legend>
                        <table style="margin-top:10px">
                            <tr>
                                <td align="right">
                                    <label>
                                         <b>ItemId/Desc:</b>
                                    </label>
                                </td>
                                <td>
                                    <input type="text" id="txtSearchString" class="date-pick" style="width:150px;" />
                                </td>
                                <td align="right">
                                    <label>
                                         <b>Type:</b>
                                    </label>
                                </td>
                                <td>
                                    <%= Html.DropDownList("ddlProductType", (SelectList)ViewData["ProductTypeList"], new { style = "width:104px" })%>
                                </td>
                                <% if((ViewData["AffiliateType"] as int?) != 2) 
                                    {%>
                                        <td align="right">
                                        <label>
                                             <b>Price Not Set:</b>
                                        </label>
                                        </td>
                                        <td align="right">
                                            <input type="checkbox" name="PriceSet" id="PriceSet"   />
                                        </td>
                                    <%}%>
                                
                                <td align="right">
                                    <label>
                                         <b>Weight Not Set:</b>
                                    </label>
                                </td>
                                <td align="right">
                                    <input type="checkbox" name="WeightSet" id="WeightSet"   />
                                </td>
                                <td align="right" style="padding-top: 0px; padding-left: 10px;">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                                </td>
                                
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        </div>
        <div id="dialog-choice" title="Confirm Submit!" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">You are about to create a new Sales Order.<br />
                <br />
                Do you want to proceed ?</span>
        </p>
        </div>
    <input id="hfAffiliateType" name="hfAffiliateType"  type="hidden" />
    <script type="text/javascript">
        $("#hfAffiliateType").val('<%=ViewData["AffiliateType"] %>');
    </script>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/product.index.js")%>' type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content3" runat="server" contentplaceholderid="head2">

</asp:Content>


