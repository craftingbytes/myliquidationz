﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />-->
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Pull Shipment Items</h1>
        <table width="100%" style="margin-top: 20px;">
            <tr>
            <td style="padding-left: 50px;" colspan="2">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
                <input id="hfCurrentSalesOrderID" name="hfCurrentSalesOrderID"  type="hidden" value="<%=ViewData["CurrentSalesOrderID"] %>" />
                <input type="hidden" id="data" name="data" value='<%= ViewData["Data"] %>' />
            </td>
            </tr>
            <tr>
                <td align="center">
                    <br />
                    <fieldset style="width: 80%">
                    <legend><b>Search Criteria</b></legend>
                        <table style="margin-top:10px">
                            <tr>
                                <td align="right">
                                    Shimpent Id:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtShipmentId" name="txtShipmentId" maxlength="10" regexp="^[0-9]\d*(\.\d+)?$"  value="<%=ViewData["CurrentShipmentID"] %>"
                                    regexpmesg="Invalid value. Enter valid number for ShipmentId."  style="width:100px" req="true"  required="Shipment Id is required."/>
                                </td>
                                <td></td>
                                <td align="left" style=" padding-left: 10px;">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                                    <input type="button" id="btnBack" name="btnBack" value="Back To Sales Order"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        </div>
    
    <!--<script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/pullshipmentitem.js")%>' type="text/javascript"></script>
</asp:Content>

