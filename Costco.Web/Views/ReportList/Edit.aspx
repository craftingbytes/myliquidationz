﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head2" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <table>
            <tr>
                <td><h1 class="heading">Report List</h1> </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td><input type="button" id="btnEdit" name="btnEdit" value="Done"  class="ui-button ui-widget ui-state-default ui-corner-all" /></td>
            </tr>
        </table>
        <br />
        <table id="jqGrid">
        </table>
    </div>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/reportlist.edit.js?version=1.15")%>' type="text/javascript"></script>
</asp:Content>
