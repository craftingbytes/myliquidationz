﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<Costco.BLL.ViewModels.LoginViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <table cellspacing="0" cellpadding="50" border="0" width="100%" style="text-align:left;">
        <tr>
            <td valign="top" style="padding-right:30px;" width="50%">
                <p style="color:#605F5B;font-size:11pt;">
                </p>
            </td>
            <td valign="top" width="50%" align="center">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2" class="loginTab">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table cellpadding="5" cellspacing="0" class="loginBox" width="100%" border="0">
                                <tr>
                                    <td colspan="2" height="37px" style="color:Red; text-align:left;">
                                        <div id="validationSummary" class="ValidationSummary">
                                            <% if (Model.Messages.Count > 0)
                                               {%><ul class="ValidationSummary">
                                       <%foreach (var message in Model.Messages)
                                         {%>
                                       <li>
                                           <%=message.Text %></li>
                                       <%} %>
                                   </ul>
                                            <%} %>
                                        </div>
                                    </td>
                                </tr>
                                <tr valign="bottom">
                                    <td align="right" style="color:#052C55;font-weight:bold;">
                                        User Name / E-mail:
                                    </td>
                                    <td align="left">
                                        <input type="text" id="UserName" name="UserName" value="<%= Model.UserName%>" style="width: 200px;"
                                            maxlength="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="color:#052C55;font-weight:bold;">
                                        Password:
                                    </td>
                                    <td align="left">
                                        <input type="password" id="Password" name="Password" value="<%= Model.Password%>"
                                            style="width: 200px;" maxlength="50" />
                                   
                                    </td>
                                    
                                </tr>
                                <tr valign="top">
                                    <td>
                                       
                                    </td>
                                    <td align="left">
                                    <input type="submit" style="background-image:url('../../Content/images/Login/btn_login.png');width:107px;height:35px;background-color:Transparent;border:none;" value="" />
                                    </td> 
                                </tr>
                                <tr>
                                <td>
                                </td>
                                <td align="left"> 
                                    <!--<a href="../ForgotPassword/Index" style="color:#FF9900;text-decoration:none;_text-decoration:none;">Forgot your Password?</a>-->
                                    </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="loginLeftBottom">
                        </td>
                        <td class="loginRightBottom">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            //checkVersion();
            //checkFireFox();
        });
        function getInternetExplorerVersion()
            // Returns the version of Internet Explorer or a -1
            // (indicating the use of another browser).
        {
            var rv = -1; // Return value assumes failure.
            if (navigator.appName == 'Microsoft Internet Explorer') {
                var ua = navigator.userAgent;
                var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                if (re.exec(ua) != null)
                    rv = parseFloat(RegExp.$1);
            }
            return rv;
        }
        function checkVersion() {
            var msg = "You're not using Internet Explorer.";
            var ver = getInternetExplorerVersion();
            var site2 = "<% =ConfigurationManager.AppSettings["MVC2Site"] %>";
            if (ver > -1 && ver < 7.0) {
                window.location.href = site2;
            }
            //else
            //alert(msg + " version:" + ver);
        }
        function checkFireFox() {
            //alert("check");
            //var msg = "You're not using Internet Explorer.";
            //alert(navigator.appName);
            if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))
                window.location.href = "/FireFoxSupport.html";
        }
    </script>
</asp:Content>
