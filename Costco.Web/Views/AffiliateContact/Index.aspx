﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<Costco.BLL.ViewModels.AffiliateContactViewModel>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
    <table style="padding:0;">
        <tr>
            <td colspan="2" style="padding-top: 20px;">
                <h3 style="color: Gray; padding-left: 20px;">
                    <%=ViewData["Name"] %> Users </h3>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-left: 20px; padding-top: 6px;">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-top: 10px;">
                <h3 style="color: Gray; padding-left: 20px;">
                    Add/Edit User</h3>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-left: 160px; padding-top: 10px;">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                    <% if (Model.Messages.Count > 0)
                       {%><ul class="ValidationSummary">
                           <%foreach (var message in Model.Messages)
                             {%>
                           <li>
                               <%=message.Text %></li>
                           <%} %>
                       </ul>
                    <%} %>
                </div>
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                <span style='color: red'>*</span>First Name :
            </td>
            <td>
                <input type="text" id="FirstName" name="FirstName" value="<%= Model.AffiliateContact.FirstName%>"
                    style="width: 200px;" maxlength="50" required="First Name is required." />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                <span style='color: red'>*</span>Last Name :
            </td>
            <td>
                <input type="text" id="LastName" name="LastName" value="<%= Model.AffiliateContact.LastName%>"
                    style="width: 200px;" maxlength="50" required="Last Name is required." />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                Address 1 :
            </td>
            <td>
                <input type="text" id="Address1" name="Address1" value="<%= Model.AffiliateContact.Address1%>"
                    style="width: 200px;" maxlength="100" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                Address 2 :
            </td>
            <td>
                <input type="text" id="Address2" name="Address2" value="<%= Model.AffiliateContact.Address2%>"
                    style="width: 200px;" maxlength="100" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                State :
            </td>
            <td>
                <%= Html.DropDownList("StateId", Model.States, new { style = "width:205px" })%>
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                </span>City :
            </td>
            <td>
                <%= Html.DropDownList("CityId", Model.Cities, new { style = "width:205px" })%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                Zip :
            </td>
            <td>
                <input type="text" id="Zip" name="Zip" value="<%= Model.AffiliateContact.Zip%>" style="width: 200px;"
                    maxlength="10" regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                    regexpmesg="Invalid Zip format, Please try xxxxx or xxxxx-xxxx." />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right" align="right" valign="top" style="padding-top:3px;">
                Phone :
            </td>
            <td>
                <input type="text" id="Phone" name="Phone" value="<%= Model.AffiliateContact.Phone%>"
                    style="width: 200px;" maxlength="20"  />
                    <%--  regexp="\(?\b[0-9]{3}\)?\b[-. ]?[0-9]{3}[-. ]?[0-9]{4}"
                    regexpmesg="Invalid Phone format, Please try (xxx)xxx-xxxx or xxx-xxx-xxxx."
                    <br />
                    <label style=" font-size:smaller; font-family:Verdana">Format: (xxx)xxx-xxxx or xxx-xxx-xxxx</label>--%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right" align="right" valign="top" style="padding-top:3px;">
                Fax :
            </td>
            <td>
                <input type="text" id="Fax" name="Fax" value="<%= Model.AffiliateContact.Fax%>" style="width: 200px;"
                    maxlength="20"  />
                    <%--regexp="\(?\b[0-9]{3}\)?\b[-. ]?[0-9]{3}[-. ]?[0-9]{4}"
                    regexpmesg="Invalid Fax format, Please try (xxx)xxx-xxxx or xxx-xxx-xxxx."
                    <br />
                    <label style=" font-size:smaller; font-family:Verdana">Format: (xxx)xxx-xxxx or xxx-xxx-xxxx</label>--%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                <span style='color: red'>*</span>User Name :
            </td>
            <td>
                <input type="text" id="UserName" name="UserName" value="<%= Model.AffiliateContact.UserName%>"
                    required="User Name is required." style="width: 200px;" maxlength="50" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 55px;">
            <td align="right" valign="top" style="padding-top:10px;">
                Email :
            </td>
            <td  colspan="2">
                <input type="text" id="Email" name="Email" value="<%= Model.AffiliateContact.Email%>"
                    style="width: 200px;" maxlength="50" regexp="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    regexpmesg="Invalid Email format, Please try hello@mydomain.com." />
                        <br />
                    <label style=" font-size:smaller; font-family:Verdana">Format: hello@mydomain.com</label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
            <tr style="height: 30px;">
            <td align="right">
                <span style='color: red'>*</span>Role :
            </td>
            <td>
                <%= Html.DropDownList("RoleId", Model.Roles, new { required = "Role is required.", style = "width:205px" })%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                <span style='color: red'>*</span>Password :
            </td>
            <td>
                <input type="password" id="Password" name="Password" value="<%= Model.AffiliateContact.Password%>" required="Password is required." style="width: 200px;" maxlength="14" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                <span style='color: red'>*</span>Confirm Password :
            </td>
            <td>
                <input type="password" id="ConfirmPassword" name="ConfirmPassword" value="<%= Model.AffiliateContact.Password%>" required="Confirm Password is required." style="width: 200px;" maxlength="14" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                Security Question :
            </td>
            <td>
                 <%= Html.DropDownList("SecurityQuestionId", Model.SecurityQuestions, new { style = "width:342px;text-align:left;z-index:auto;"})%>
            </td>
            <td>
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                Answer :
            </td>
            <td>
                <input type="text" id="Answer" name="Answer" value="<%= Model.AffiliateContact.Answer%>"
                    style="width: 335px;" maxlength="50" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 30px;">
            <td align="right">
                Active :
            </td>
            <td>
                <%if (Model.AffiliateContact.Active == true || Model.AffiliateContact.Active == null)
                    {%>
                <%= Html.CheckBox("Active", true)%>
                <%}
                    else
                    {%>
                <%= Html.CheckBox("Active", false)%>
                <%} %>
                <!--<%= Html.CheckBox("Active", true)%> -->
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr align="left">
            <td>
            </td>
            <td align="left" style="padding-left: 10px; padding-top: 20px;">
            <input id="btnSubmit" name="btnSubmit" type="submit" value="Submit" class="ui-button ui-widget ui-state-default ui-corner-all" />
            &nbsp;
            <input type="button" id="btnClear" name="btnClear" value="Clear" class="ui-button ui-widget ui-state-default ui-corner-all" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" id="btnAdd" name="btnAdd" value="Add User" class="ui-button ui-widget ui-state-default ui-corner-all" />
            <input type="hidden" id="data" name="data" value='<%= ViewData["Data"] %>' />
            <input type="button" id="btnCancel" name="btnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="Cancel();"/>
            </td>
        </tr>
    </table>
    <script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/affiliatecontact.index.js")%>' type="text/javascript"></script>
</asp:Content>
