﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <div class="admin">
    <h1 class="heading">Search Entity</h1>
     
    <table cellspacing="0" width="70%" style="margin-top: 5px">
       <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
            <fieldset style="height: 150px; width: 350px;">
        <legend style="padding: 5px;"><b>Search Criteria</b></legend>
                    <table style="margin-top:10px">
                       
                        <tr>
                        <td align="right">
                                Entity Name:
                            </td>
                            <td align="left">
                                <input type="text" id="txtSearch" size="30" style="width: 200px;"  />                                
                            </td>
                           </tr>
                            <tr>
                            <td align="right">
                                Entity Type:
                            </td>
                            <td align="left"> 
                                <%= Html.DropDownList("EntityTypeId", ViewData["AffiliateTypes"] as SelectList, new { style = "width:205px" })%>
                            </td>
                            </tr>
                            <tr>
                            <td>
                            </td>
                            <td align="left">
                                <input type="button" id="btnSearch" name="btnSearch" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                        </tr>
                    </table>
                     </fieldset>            
            </td>
        </tr>
       <tr>
        <td style="padding-left:20px;padding-top:6px;">
            <input type="hidden" id="Data" name="Data" value='<%=ViewData["Data"] %>'/>
          <%if ((ViewData["AccessRights"] as Costco.BLL.Authorization.AccessRights) != null && (ViewData["AccessRights"] as Costco.BLL.Authorization.AccessRights).Add.HasValue && (ViewData["AccessRights"] as Costco.BLL.Authorization.AccessRights).Add.Value)
            { %>    
            <input type="button" id="btnAdd" name="btnAdd" value="Create Entity" class="ui-button ui-widget ui-state-default ui-corner-all"/>
            <%}%>
        </td>
        </tr>
        <tr>
            <td style="padding-top:3px;">
                <div id="divSearch">
                </div>
                <table id="jqGrid">
                </table>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table>
    </div>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.contextmenu.r2.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/affiliate.index.js")%>' type="text/javascript"></script>
</asp:Content>




