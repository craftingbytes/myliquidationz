﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />-->
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Search Bulk Shipments</h1>
        <table width="100%" style="margin-top: 20px;">
            <tr>
                <td align="center">
                    <br />
                    <fieldset style="width: 80%">
                    <legend><b>Search Criteria</b></legend>
                        <table style="margin-top:10px">
                            <tr>
                                <td align="right">
                                    Beginning Date:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginDate" class="date-pick" style="width:140px;" />
                                </td>
                                <td align="right">
                                    Ending Date:
                                </td>
                                <td>
                                    <input type="text" id="txtEngingDate" class="date-pick" style="width:140px;" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="2" align="right" style="padding-top: 10px; padding-left: 10px;">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        </div>
            <div id="dialog" title="Invalid Dates!" style="display: none;">
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">Begining Date cannot be bigger than Ending Date.</span>
        </div>

        <div id="dialogFiles" title="Attached files" style="display: none;">
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">
                <ul id="ulFiles">
           
                </ul>
                </span>
        </div>
    <input id="hfAffiliateType" name="hfIsOem"  type="hidden" />
    <script type="text/javascript">
        $("#hfAffiliateType").val('<%=ViewData["AffiliateType"] %>');
    </script>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/bulkshipment.list.js")%>' type="text/javascript"></script>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/ui.dropdownchecklist.js")%>' type="text/javascript"></script>-->
</asp:Content>

