﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />-->
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Costco Bills</h1>
        <table width="100%" style="margin-top: 20px;">
        <tr>
                <td align="center">
                    <br />
                    <fieldset style="width: 80%">
                    <legend><b>Search Criteria</b></legend>
                        <table style="margin-top:10px">
                            <tr>
                                <td align="right">
                                    Beginning Date:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginDate" class="date-pick" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Ending Date:
                                </td>
                                <td>
                                    <input type="text" id="txtEngingDate" class="date-pick" style="width:80px;" />
                                </td>

                                <td align="right">
                                    Beginning Id:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginId" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Ending Id:
                                </td>
                                 <td align="left">
                                    <input type="text" id="txtEndId" style="width:80px;" />
                                </td>
                                <td colspan="2" align="right">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" style="width:90px;" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <table width="100%" style="margin-top: 10px;">
            <tr><td align="left"><input type="button" id="btnAdd" value="New Bill" class="fr ui-button ui-widget ui-state-default ui-corner-all" /></td></tr>
        </table>
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        
        </div>
    <div id="dlgAdjustment" title="Add Invoice Adjustment" style="z-index:500">
        <table cellspacing="0" cellpadding="5" width="100%">
            <tr>
                <td style="padding-left: 20px;">
                    <div style="float:left; width:51%;">
                        <table cellspacing="0" cellpadding="5">
                            <tr>
                                <td align="center" >
                                    &nbsp;
                                </td>
                                <td align="center" >
                                    Description
                                </td>
                                <td align="center" >
                                    Amount
                                </td>
                            </tr>
                            <tr>
                                <td align="right" nowrap>
                                    <font color="red">*</font>Invoice Adjustment :
                                </td>
                                <td>
                                    <input type="text" id="txtAdjustmentDesc" name="txtAdjustmentDesc" size="100" style="width: 300px;"  />
                                    <input type="hidden" id="currentInvoice" name="currentInvoice" value = "0"/>
                                </td>
                                <td>
                                    <input type="text" id="txtAdjustmentAmount" name="txtAdjustmentAmount" size="100" style="width: 100px;"  />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px; padding-top: 20px;" align="center">
                    <table style="padding: 0px; border:0">
                        <tr>
                            <td align="right">
                                <input type="button" id="dlgAdjBtnCancel" name="dlgAdjBtnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                            <td align="right">
                                <input type="button" id="dlgAdjBtnSave" name="dlgBtnSave" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="dlgUpdate" title="Edit Invoice" style="z-index:500">
        <table cellspacing="0" cellpadding="5" style="width: 62%">
            <tr>
                <td style="padding-left: 10px;">
                    <div style="float:left; width:100%; height: 142px;">
                        <table cellspacing="0" cellpadding="5">
                            <tr>
                                <td align="right" nowrap>
                                    InvoiceDate:
                                </td>
                                <td>
                                    <input type="text" id="txtUpdateDate" name="txtUpdateDate" size="100" style="width: 100px;"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" nowrap>
                                    Notes :
                                </td>
                                <td>
                                    <textarea id="txtUpdateNotes" name="txtUpdateNotes"  rows="5" 
                                        style="width: 546px;" ></textarea>
                                    <input type="hidden" id="currentInvoice2" name="currentInvoice2" value = "0"/>
                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px; padding-top: 20px;" align="center">
                    <table style="padding: 0px; border:0">
                        <tr>
                            <td align="right">
                                <input type="button" id="dlgUpdateBtnCancel" name="dlgUpdateBtnCancel" value="Cancel" class="ui-button ui-widget ui-state-default ui-corner-all" />
                            </td>
                            <td align="right">
                                <input type="button" id="dlgUpdateBtnSave" name="dlgUpdateBtnSave" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="hiddenDivId">
    </div>
    <input id="hfAffiliateType" name="hfAffiliateType"  type="hidden" />
    <script type="text/javascript">
        $("#hfAffiliateType").val('<%=ViewData["AffiliateType"] %>');
    </script>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/ui.dropdownchecklist.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/invoice.costcobills.js")%>' type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" runat="server" contentplaceholderid="head2">


</asp:Content>


