﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />-->
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Generate Bill to Costco</h1>
        <table width="100%" style="margin-top: 20px;">
        <tr>
                <td align="center">
                    <br />
                    <fieldset style="width: 80%">
                    <legend><b>Search Criteria</b></legend>
                        <table style="margin-top:10px">
                            <tr>
                                <td align="right">
                                    Year :
                                </td>
                                <td>
                                    <select id="Year" style="width:173px;">
                                       <option value="0">Select</option>
                                       <option value="2012">2012</option>
                                       <option value="2013">2013</option>
                                       <option value="2014">2014</option>
                                    </select>
                                </td>
                                <td align="right">
                                    Month :
                                </td>
                                <td>
                                    <select id="Months" style="width:173px;" multiple="multiple">
                                       <option value="1">January</option>
                                       <option value="2">February</option>
                                       <option value="3">March</option>
                                       <option value="4">April</option>
                                       <option value="5">May</option>
                                       <option value="6">June</option>
                                       <option value="7">July</option>
                                       <option value="8">August</option>
                                       <option value="9">September</option>
                                       <option value="10">October</option>
                                       <option value="11">November</option>
                                       <option value="12">December</option>
                                    </select>
                                </td>
      
                                <td align="right" style="padding-top: 0px; padding-left: 10px;">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <table id="jqGrid">
        </table>
        <table width="100%" style="margin-top: 10px;">
            <tr><td align="center"><input type="button" id="btnSubmit" value="Generate Bill" class="fr ui-button ui-widget ui-state-default ui-corner-all" /></td></tr>
        </table>
        </div>
    <div style="width: 95%; margin-left:8px; ">
    
    </div>
    <div id="hiddenDivId">
    </div>
    <input id="hfAffiliateType" name="hfAffiliateType"  type="hidden" />
    <script type="text/javascript">
        $("#hfAffiliateType").val('<%=ViewData["AffiliateType"] %>');
    </script>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/ui.dropdownchecklist.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/invoice.create.js")%>' type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" runat="server" contentplaceholderid="head2">


</asp:Content>


