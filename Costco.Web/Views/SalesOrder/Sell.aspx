﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<Costco.BLL.ViewModels.SalesOrderViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
        <h1 class="heading">Sales Order Id: <%=Model.SalesOrder.Id%> </h1>
    <table cellpadding="3" width="100%" border="0">
        <tr>
            <td style="padding-left: 130px;" colspan="2">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
            </td>
        </tr>
        <tr>
             <td style = "width: 45%" valign="top">
                <fieldset style="float: left; height: 140px; width: 95%;">
                    <legend style="padding: 5px;"><b>ASR Address</b></legend>
                    <table cellpadding="3" style="width: 100%">
                        <tr>
                            <td align="right" style="width: 15%">
                                <label for="txtASRName">
                                    <b>Company :</b>
                                </label>
                            </td">
                            <td  colspan=3>
                                <input type="text" id="txtASRName" name="txtASRName" value="<%=Model.SalesOrder.Affiliate.Name%>" style="width:300px" READONLY />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 15%">
                                <label for="txtASRAddress">
                                     <b>Address :</b>
                                </label>
                            </td>
                            <td style="width: 35%">
                                <input type="text" id="txtASRAddress" name="txtASRAddress" value="<%=Model.SalesOrder.Affiliate.Address1%>"  style="width:120px" READONLY />
                            </td>
                            <td align="right">
                                <label for="txtASRState">
                                     <font color='red'>*</font><b>State :</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtASRState" name="txtASRState" value="<%=Model.SalesOrder.Affiliate.State.Abbreviation%>"  style="width:120px" READONLY />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label for="txtASRCity">
                                     <font color='red'>*</font><b>City :</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtASRCity" name="txtASRCity" value="<%=Model.SalesOrder.Affiliate.City.Name%>"  style="width:120px" READONLY />
                            </td>
                            <td align="right">
                                <label for="txtASRZip">
                                    <b>Zip:</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="txtASRZip" name="txtASRZip" value="<%=Model.SalesOrder.Affiliate.Zip%>"  style="width:120px" READONLY />
                            </td>
                        </tr>     
                    </table>
                </fieldset>
            </td>
            <td style = "width: 45%" rowspan=2 valign="top">
                <fieldset style="float: left; height: 286px; width: 95%;">
                    <legend style="padding: 5px;"><b>Sales Info</b></legend>
                    <table cellpadding="3" style="width: 100%; height: 256px;">
                        <tr>
                            <td align="right" style="width: 30%">
                                <label for="txtTotalPallets">
                                    <font color='red'>*</font><b>Total Pallets :</b>
                                </label>
                            </td>
                            <td  style="width: 20%">
                                <input type="text" id="TotalPallets" name="TotalPallets" value="<%=Model.SalesOrder.TotalPallets %>" maxlength="10" regexp="^[0-9]+[0-9]*$" 
                                    regexpmesg="Invalid value. Enter whole number for weight."  style="width:100px; text-align: right" req="true" required="Total Weight is Required" />
                            </td>
                            <td align="right">
                                <label>
                                     <font color='red'>*</font><b>Total Weight:</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="TotalWeight" name="TotalWeight" value="<%=Model.SalesOrder.TotalWeight %>" maxlength="10" regexp="^[0-9]+[0-9]*$" 
                                    regexpmesg="Invalid value. Enter whole number for weight."  style="width:100px; text-align: right" req="true" required="Total Weight is Required" />                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 30% ">
                                <label for="txtSalesDate">
                                     <font color='red'>*</font><b>Sales Date:</b>
                                </label>
                            </td>
                            <td style="width: 20% ">
                                <input type="text" id="SoldDate" name="SoldDate" value="<%= Model.SalesOrder.SoldDate.HasValue ? Model.SalesOrder.SoldDate.Value.ToShortDateString() : "" %>" req="true" required="Sales Date is required." style="width:100px" />
                            </td>
                            <td align="right" style="width: 30% ">
                                <label for="txtRetailValue">
                                    <b>Retail Value:</b>
                                </label>
                            </td>
                            <td style="width: 20% ">
                                <input type="text" id="txtRetailValue" name="txtRetailValue" value="<%=ViewData["RetailValue"]%>" style="width:100px; text-align: right" READONLY />
                            </td>          
                        </tr>
                        <tr>
                            <td align="right" style="width: 30%">
                                <label for="txtPct">
                                    <b>Initial %:</b>
                                </label>
                            </td>
                            <td  style="width: 20%">
                                <input type="text" id="txtPct" name="txtPct" value="<%=Model.SalesOrder.InitialPercentage.HasValue ? Model.SalesOrder.InitialPercentage.Value.ToString("0.00") : "" %>" maxlength="10"  style="width:100px; text-align: right" req="true"  READONLY />
                                <%--<input type="text" id="txtPrice" name="txtPrice" value="<%=ViewData["Price"]%>" maxlength="10"  style="width:100px; text-align: right" req="true"  READONLY />--%>
                            </td>
                            <td align="right">
                                <label>
                                     </font><b>Sales Amount:</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="SoldAmount" name="SoldAmount" READONLY value="<%=Model.SalesOrder.SoldAmount.HasValue ? Model.SalesOrder.SoldAmount.Value.ToString("0.00") : "" %>" maxlength="10" regexp="^[0-9]\d*(\.\d+)?$"  
                                    regexpmesg="Invalid value. Enter money value for amount."  style="width:100px; text-align: right" req="true" required="Sales Amount is Required" />                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label>
                                     <b>Comments:</b>
                                </label>
                            </td>
                            <td colspan=3 >                              
                                <textarea id="Comment" name="Comment" rows="5" style="width: 358px;"><%=Model.SalesOrder.Comment %></textarea>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </fieldset>
            </td>
           
        </tr>
        <tr>
            <td style = "width: 45%" valign="top">
                <fieldset style="float: left; height: 140px; width: 95%;">
                    <legend style="padding: 5px;"><b>Buyer&#39;s Address</b></legend>
                    <table cellpadding="3" style="width: 100%">
                        <tr>
                            <td align="right" style="width: 15%">
                                <label for="txtRecyclerName">
                                    <font color='red'>*</font><b>Company :</b>
                                </label>
                            </td">
                            <td colspan="3">
                                <%= Html.DropDownList("ddlBuyerList", (SelectList)ViewData["BuyerList"], new { style = "width:120px", required = "Buyer is required." })%>
                                <input type="text" id="SoldToName" name="SoldToName" value="<%=Model.SalesOrder.SoldToName%>" req="true" required="Recycler Name is required." style="width:200px" />
                            </td>
                            
                        </tr>
                        <tr>
                            <td align="right">
                                <label for="txtAddress">
                                     <font color='red'>*</font><b>Address :</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="SoldToAddress" name="SoldToAddress" value="<%=Model.SalesOrder.SoldToAddress%>" req="true" required="Pickup Address is required." style="width:120px" />
                            </td>
                            <td align="right">
                                <label for="txtState">
                                     <font color='red'>*</font><b>State :</b>
                                </label>
                            </td>
                            <td>
                                <%= Html.DropDownList("ddlBuyerState", (SelectList)ViewData["BuyerState"], new { style = "width:120px", onchange = "GetCities(this);", required = "Buyer State is required." })%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label for="txtCity">
                                     <font color='red'>*</font><b>City :</b>
                                </label>
                            </td>
                            <td>
                                <%= Html.DropDownList("ddlBuyerCity", (SelectList)ViewData["ddlBuyerCity"], new { style = "width:120px", onchange = "SetStateCity(this);", required = "Buyer City is required." })%>
                            </td>
                            <td align="right">
                                <label for="txtZip">
                                    <b>Zip:</b>
                                </label>
                            </td>
                            <td>
                                <input type="text" id="SoldToZip" name="SoldToZip" value="<%=Model.SalesOrder.SoldToZip%>" maxlength="5" regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                                    regexpmesg="Invalid Zip format, Please try xxxxx."  style="width:100px"  />
                            </td>
                        </tr>     
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="1" align="right">
                <input type="button" id="btnCancel" value="Cancel" class="fr ui-button ui-widget ui-state-default ui-corner-all" />&nbsp;&nbsp;
                <input type="hidden" id ="IsSold" value="<%=ViewData["IsSold"] %>" name = "IsSold" />
            </td>
            <td colspan="1" align="left">
                &nbsp;&nbsp;
                <input type="button" id="btnSubmit" value="Submit" class="fr ui-button ui-widget ui-state-default ui-corner-all" />
                <input type="hidden" id ="SoldToAffiliateId" value="<%=Model.SalesOrder.SoldToAffiliateId %>" name = "SoldToAffiliateId" />
                <input type="hidden" id ="Id" value = "<%=Model.SalesOrder.Id %>" name = "Id" />
                <input type="hidden" id ="SoldToStateId" value="<%=Model.SalesOrder.SoldToStateId %>" name = "SoldToStateId" />
                <input type="hidden" id ="SoldToCityId" value="<%=Model.SalesOrder.SoldToCityId %>" name = "SoldToCityId" />
                <input type="hidden" id ="AffiliateId" value="<%=Model.SalesOrder.AffiliateId %>" name = "AffiliateId" />       
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table id="jqGrid"></table>
            </td>
        </tr>
    </table>
    
    <div id="dialog-choice" title="Confirm Submit!" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">You are about to save the sales data permanently.<br />
                <br />
                Do you want to proceed ?</span>
        </p>
    </div>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/salesorder.sell.js?version=1.3")%>' type="text/javascript"></script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">
</asp:Content>
