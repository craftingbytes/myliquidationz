﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<Costco.BLL.ViewModels.SalesOrderViewModel>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />-->
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <div class="admin">
        <table>
        <tr>
            <td>
                <h1 class="heading">Sales Order Id: <%=Model.SalesOrder.Id%> </h1>
                <input type="hidden" id="hdSalesOrderId" name="hdSalesOrderId"  value="<%=Model.SalesOrder.Id%>" />
                <input type="hidden" id="hdAffiliateId" name="hdAffiliateId" value="<%=Model.SalesOrder.AffiliateId %>" />
            </td>
            <td style="width:50px">
            </td>
            <td align="center">
                <input type="button" id="btnSetWeight" name="btnSetWeight" value="Set Weight/Pallets"  class="ui-button ui-widget ui-state-default ui-corner-all" />
            </td>
            <td align="center">
                <input type="button" id="btnSetPrices" name="btnSetPrices" value="Set Selling Prices"  class="ui-button ui-widget ui-state-default ui-corner-all" />
            </td>
            <td align="center">
                <input type="button" id="btnSell" name="btnSell" value="Ready To Sell"  class="ui-button ui-widget ui-state-default ui-corner-all" />
            </td>
        </tr>
        </table>

        <input type="hidden" id="Data" name="Data" value='<%=ViewData["Data"] %>'/>
        <span style="color: Blue;"></span>
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        <table id="tblShipments" style="margin-top:10px">
                <tr>
                    <td align="center">
                        <fieldset style="width:90%">
                        <legend><b>Available Shipments</b></legend>
                        <table>
                            <tr>
                                <td align="right">
                                    Type:
                                </td>
                                <td>
                                    <select id="shipmentType" name="shipmentType" style="width:104px">
                                    <option value="All">All</option>
                                    <option value="S">Shipment</option>
                                    <option value="B">Bulk</option>
                                    <option value="C">Salvage Sale</option>
                                </select>
                                </td>
                                <td align="right">
                                    Beginning Id:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginId" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Ending Id:
                                </td>
                                <td>
                                    <input type="text" id="txtEndId" class="date-pick" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Beginning Date:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginDate" class="date-pick" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Ending Date:
                                </td>
                                <td>
                                    <input type="text" id="txtEndDate" class="date-pick" style="width:80px;" />
                                </td>
                                <td align="right">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td align="center">
                        <table id="jqGrid2"></table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div id="pager2">
                    </div>
                    </td>
                </tr>
            </table>
        <div id="dialog-sell" title="Confirm Status Change!" style="display: none;">
         <table>
            <tr>
                <td colspan="2">
                    <div id="validationSummary" style="width: 200px;" class="ValidationSummary"></div>
                </td>
            </tr>
            <tr>
            <td><label for="txtMinValue">Estimated Minimum Value</label></td>   
            <td><input id="txtMinValue" name="txtMinValue" req="true" required="Minimum Value Required." style="width:100px"/></td>
            </tr>
         </table>
         
         
         <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">
                <br />You are about to make this order available to sell.  
                <br />If you continue you will no longer be able to edit this order.
                <br />Do you want to proceed?</span>
        </p>
        </div>
        <div id="dialog-weights" title="Set Pallets and Weights" style="display: none;">
         <table>
            <tr>
                <td colspan="2">
                    <div id="Div2" style="width: 200px;" class="ValidationSummary"></div>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="txtTotalPallets">
                        <font color='red'>*</font><b>Total Pallets :</b>
                    </label>
                </td>
                <td><input type="text" id="TotalPallets" name="TotalPallets" value="<%=Model.SalesOrder.TotalPallets %>" maxlength="10" regexp="^[0-9]+[0-9]*$" 
                                        regexpmesg="Invalid value. Enter whole number for weight."  style="width:100px" req="true" required="Total Weight is Required" />

                </td>
                <td align="right">
                    <label>
                            <font color='red'>*</font><b>Total Weight:</b>
                    </label>
                </td>
                <td>
                    <input type="text" id="TotalWeight" name="TotalWeight" value="<%=Model.SalesOrder.TotalWeight %>" maxlength="10" regexp="^[0-9]+[0-9]*$" 
                        regexpmesg="Invalid value. Enter whole number for weight."  style="width:100px" req="true" required="Total Weight is Required" />
                                
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label>
                            <b>Comments:</b>
                    </label>
                </td>
                <td colspan=3 >                              
                    <textarea id="Comment" name="Comment" rows="5" style="width: 300px;" cols="20"><%=Model.SalesOrder.Comment %></textarea>
                </td>
            </tr>
         </table>
        </div>
        <div id="dialog-ShipmentItems" title="Add shipment items" style="display: none;">
            <table id="jqgridShipmentItems">
            </table>
        </div>
        <div id="dialog-BulkItems" title="Add shipment items" style="display: none;">
            <table id="jqgridBulkItems">
            </table>
        </div>
        <div id="dialog-SSOrderItems" title="Add Salvage Sales items" style="display: none;">
            <table id="jqgridSSOItems">
            </table>
        </div>
    </div>
    <input type="hidden" id="SalesOrderId" name="SalesOrderId" value="<%=Model.SalesOrder.Id%>" />

    <!--<script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/salesorder.edit.js?version=1.14")%>' type="text/javascript"></script>
</asp:Content>
