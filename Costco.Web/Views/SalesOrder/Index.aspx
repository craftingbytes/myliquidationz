﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />-->
    <!--<link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Sales Orders</h1>
        <table width="100%" style="margin-top: 20px;">
        <tr>
                <td align="center">
                    <br />
                    <fieldset style="width: 90%">
                    <legend><b>Search Criteria</b></legend>
                        <table style="margin-top:10px">
                            <tr>
                                <td align="right">
                                    <!--Incomplete:-->
                                </td>
                                <td align="left">
                                    <input type="hidden" id="cbIncomplete" name="cbIncomplete"/>
                                </td>
                                
                                <td align="right">
                                    Beginning Id:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginId" style="width:50px;" />
                                </td>
                                <td align="right">
                                    Ending Id:
                                </td>
                                <td>
                                    <input type="text" id="txtEndId" style="width:50px;" />
                                </td>
                                <td align="right">
                                    Beginning Date:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginDate" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Ending Date:
                                </td>
                                <td>
                                    <input type="text" id="txtEngingDate" class="date-pick" style="width:80px;" />
                                </td>
                                <td align="right" style="padding-top: 0px; padding-left: 10px;">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <input type="button" id="btnAdd" name="btnAdd" value="Create New Sales Order "  class="ui-button ui-widget ui-state-default ui-corner-all" />
        <br />
        <br />
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        </div>
        <div id="dialog-choice" title="Confirm Submit!" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">You are about to create a new Sales Order.<br />
                <br />
                Do you want to proceed ?</span>
        </p>
        </div>
        <div id="dialog-ship" title="Confirm Status Change!" style="display: none;">
         <label for="txtShipDate">Shipment Date</label>   
         <input id="txtShipDate" name="txtShipDate" req="true" required="Shipping Date is required." style="width:100px"/>
         <br />
         <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">
                <br />You are about to change the status to Shipped.
                <br />
                Do you want to proceed ?</span>
         </p>
       </div>
       <div id="dialog-revert" title="Confirm Status Change!" style="display: none;">
         <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">
                <br />You are about to revert the shipping status to "Not Shiped"
                Do you want to proceed ?</span>
        </p>
        </div>
        <div id="dialog-buyer" title="Buyer Info" style="display: none;">
            <fieldset style="float: left; height: 140px; width: 95%;">
                <legend style="padding: 5px;"><b>Buyer&#39;s Address</b></legend>
                <table cellpadding="3" style="width: 100%">
                    <tr>
                        <td align="right" style="width: 15%">
                            <label for="txtRecyclerName">
                                <font color='red'>*</font><b>Company :</b>
                            </label>
                        </td">
                        <td colspan="3">
                            <%= Html.DropDownList("ddlBuyerList", (SelectList)ViewData["BuyerList"], new { style = "width:120px", required = "Buyer is required." })%>
                            <input type="text" id="SoldToName" name="SoldToName" value="" req="true" required="Recycler Name is required." style="width:200px" />
                        </td>
                            
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="txtAddress">
                                    <font color='red'>*</font><b>Address :</b>
                            </label>
                        </td>
                        <td>
                            <input type="text" id="SoldToAddress" name="SoldToAddress" value="" req="true" required="Pickup Address is required." style="width:120px" />
                        </td>
                        <td align="right">
                            <label for="txtState">
                                    <font color='red'>*</font><b>State :</b>
                            </label>
                        </td>
                        <td>
                            <%= Html.DropDownList("ddlBuyerState", (SelectList)ViewData["BuyerState"], new { style = "width:120px", onchange = "GetCities(this);", required = "Buyer State is required." })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="txtCity">
                                    <font color='red'>*</font><b>City :</b>
                            </label>
                        </td>
                        <td>
                            <%= Html.DropDownList("ddlBuyerCity", (SelectList)ViewData["ddlBuyerCity"], new { style = "width:120px", onchange = "SetStateCity(this);", required = "Buyer City is required." })%>
                        </td>
                        <td align="right">
                            <label for="txtZip">
                                <b>Zip:</b>
                            </label>
                        </td>
                        <td>
                            <input type="text" id="SoldToZip" name="SoldToZip" value="" maxlength="5" regexp="^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$"
                                regexpmesg="Invalid Zip format, Please try xxxxx."  style="width:100px"  />
                        </td>
                    </tr>     
                </table>
                </fieldset>
            </div>
    <input id="hfAffiliateType" name="hfAffiliateType"  type="hidden" />
    <input type="hidden" id="hdAffiliateId" name="hdAffiliateId" value="<%=ViewData["AffiliateId"] %>" />
    <script type="text/javascript">
        $("#hfAffiliateType").val('<%=ViewData["AffiliateType"] %>');
    </script>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/salesorder.index.js?version=1.3")%>' type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content3" runat="server" contentplaceholderid="head2">

</asp:Content>


