﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<Costco.BLL.ViewModels.SalesOrderViewModel>" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {

    }
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style type="text/css">
        .myAltRowClass { background-color: #DCFFFF; background-image: none; }
    </style>
    <div class="admin">
    <h1 class="heading">Sales Order Id: <%=Model.SalesOrder.Id%> </h1>
    <table cellpadding="3" width="100%" border="0">
        <tr>
            <td style="padding-left: 130px;" colspan="2">
                <div id="validationSummary" style="width: 500px;" class="ValidationSummary">
                </div>
            </td>
        </tr>
        <tr>
            <td  align="center" colspan="3" valign="top">
                <fieldset style="width: 90%">
                    <legend style="padding: 5px;"><b>Sales Info</b></legend>
                    <table cellpadding="3" style="width: 100%;">
                        <tr>
                            <td align="right" >
                              
                                <label for="txtRetailValue">
                                    <b>Retail Value:</b>
                                </label>
                            </td>
                            <td align="left">
                                <input type="text" id="txtRetailValue" name="txtRetailValue" value="<%=ViewData["RetailValue"]%>" style="width:100px; text-align: right" READONLY />
                            </td> 
                            <td align="right">
                                <label for="txtTotalPallets">
                                    <b>Initial Percentage</b>
                                </label>
                            </td>
                            <td  align="left">
                                <input type="text" id="txtPct" name="txtPct" value="<%=Model.SalesOrder.InitialPercentage.HasValue ? Model.SalesOrder.InitialPercentage.Value.ToString("0.00") : "0.00" %>"  maxlength="10"  style="width:100px; text-align: right" req="true"  />
                            </td>
                            <td>
                                <input type="button" id="btnApplyPct" value="Change %" class="fr ui-button ui-widget ui-state-default ui-corner-all" />&nbsp;
                            </td>
                            <td align="right">
                                <label>
                                     </font><b>Sales Amount:</b>
                                </label>
                            </td>
                            <td align="left">
                                <input type="text" id="SoldAmount" name="SoldAmount" READONLY value="<%=Model.SalesOrder.SoldAmount.HasValue ? Model.SalesOrder.SoldAmount.Value.ToString("0.00") : "0.00" %>" maxlength="10" regexp="^[0-9]\d*(\.\d+)?$"  
                                    regexpmesg="Invalid value. Enter money value for amount."  style="width:100px; text-align: right" req="true" required="Sales Amount is Required" />                                
                            </td>
                            

                        </tr>

                    </table>
                </fieldset>
            </td>
        </tr>

        <tr>
            <td colspan="1" align="left">

            </td>
            <td colspan ="1" align="center">
                <input type="button" id="btnAddMore" value="Add More Items" class="fr ui-button ui-widget ui-state-default ui-corner-all" />&nbsp;
                <input type="button" id="btnSell" value="Ready to Sell" class="fr ui-button ui-widget ui-state-default ui-corner-all" />&nbsp;
                <input type="button" id="btnViewReport" value="View Report" class="fr ui-button ui-widget ui-state-default ui-corner-all" />&nbsp;
            </td>
            <td colspan="1" align="left">
                &nbsp;&nbsp;
                
                <input type="hidden" id ="SoldToAffiliateId" value="<%=Model.SalesOrder.SoldToAffiliateId %>" name = "SoldToAffiliateId" />
                <input type="hidden" id ="hdSalesOrderId" value = "<%=Model.SalesOrder.Id %>" name = "hdSalesOrderId" />
                <input type="hidden" id ="hdInitialPct" value ="<%=ViewData["InitialPercentage"]%>" name="hdInitialPct" />

                <input type="hidden" id ="AffiliateId" value="<%=Model.SalesOrder.AffiliateId %>" name = "AffiliateId" />       
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table id="jqGrid"></table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <div id="pager">
        
                </div>

            </td>
        </tr>
    </table>
    </div>
    <div id="dialog-change" title="Change Initial Percentage!" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
            <span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
                font-size: 14px; color: #464646;">You are about to reset <b>all</b> prices to a new percentage.<br />
                <br />
                Do you want to proceed ?</span>
        </p>
    </div>
    <script src='<%=ResolveClientUrl("~/Scripts/forms/salesorder.sellingprices.js?version=1.1")%>' type="text/javascript"></script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head2" runat="server">



</asp:Content>
