﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Costco.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Content/css/ui.dropdownchecklist.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/css/default.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="admin">
        <h1 class="heading">Auctions</h1>
        <table width="100%" style="margin-top: 20px;">
        <tr>
                <td align="center">
                    <br />
                    <fieldset style="width: 98%">
                    <legend><b>Search Criteria</b></legend>
                        <table style="margin-top:10px">
                            <tr>
                                <td>
                                    <select id="ddlAuctionType" name="ddlAuctionType">
                                        <option value="AllOpen">All Open Auctions</option>
                                        <option value="All">All Auctions</option>
                                        <option value="Open">My Current Auctions</option>
                                        <option value="Won">My Auctions Won</option>
                                        <option value="Lost">My Auctions Lost</option>
                                    </select>
                                </td>
                                <td align="right">
                                    Beginning Id:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginId" style="width:50px;" />
                                </td>
                                <td align="right">
                                    Ending Id:
                                </td>
                                <td>
                                    <input type="text" id="txtEndId" style="width:50px;" />
                                </td>
                                <td align="right">
                                    Beginning Date:
                                </td>
                                <td align="left">
                                    <input type="text" id="txtBeginDate" style="width:80px;" />
                                </td>
                                <td align="right">
                                    Ending Date:
                                </td>
                                <td>
                                    <input type="text" id="txtEngingDate" class="date-pick" style="width:80px;" />
                                </td>
                                <td align="right" style="padding-top: 0px; padding-left: 10px;">
                                    <input type="button" id="btnSearch" name="btnSearch" value="Search"  class="ui-button ui-widget ui-state-default ui-corner-all" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                <br />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <span style="color: Blue;"></span>
        <br />
        <br />
        <table id="jqGrid">
        </table>
        <div id="pager">
        </div>
        </div>
    <input id="hfAffiliateType" name="hfAffiliateType"  type="hidden" />
    <script type="text/javascript">
        $("#hfAffiliateType").val('<%=ViewData["AffiliateType"] %>');
    </script>
    <!--<script src='<%=ResolveClientUrl("~/Scripts/forms/Validation.js")%>' type="text/javascript"></script>-->
    <!--<script src='<%=ResolveClientUrl("~/Scripts/js/jquery.fillSelect.js")%>' type="text/javascript"></script>-->
    <script src='<%=ResolveClientUrl("~/Scripts/forms/salesorder.indexbuyer.js")%>' type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content3" runat="server" contentplaceholderid="head2">

</asp:Content>


