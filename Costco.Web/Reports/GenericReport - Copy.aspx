﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenericReport.aspx.cs" Inherits="Costco.Web.Reports.GenericReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" style="height: 100%">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/cupertino/jquery-ui-1.10.2.custom.css")%>" />
    <script src='<%=ResolveClientUrl("~/Scripts/jquery-1.11.0.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/jquery-ui-1.10.2.custom.min.js")%>' type="text/javascript"></script>
</head>
<body style="height:100%">
    <form id="form1" runat="server" style="height: 100%">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <rsweb:ReportViewer ID="rptviewer" runat="server" Width="100%"  height="1930px" >
    </rsweb:ReportViewer>

       
    </form>
    <script type="text/javascript">  
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(showDatePicker);
              $(document).ready(function() {

                      $(function() {
                              showDatePicker();
                          });
                  });
                
              function showDatePicker() {
              
                  var parameterRow = $("#ParametersRowrptviewer");
                      var innerTable = $(parameterRow).find("table").find("table");
                      var span = innerTable.find("span:contains('Start Date')");
                      if (span) {
                              var innerRow = $(span).parent().parent();
                              var innerCell = innerRow.find("td").eq(1);
                              var textFrom = innerCell.find("input[type=text]");
                              innerCell = innerRow.find("td").eq(4);
                              var textTo = innerCell.find("input[type=text]");
                  
                              $(textFrom).datepicker({
                                      
                                      dateFormat: 'mm/dd/yyyy',
                                      changeMonth: true,
                                      numberOfMonths: 1,
                                      onClose: function(selectedDate) {
                                              $(textTo).datepicker();
                                          }
                              });
                          $(textFrom).focus(function(e) {
                                  //e.preventDefault();
                                  $(textFrom).datepicker("show");
                              });
                          $(textTo).datepicker({
                                  
                                  dateFormat: 'mm/dd/yyyy',
                                  changeMonth: true,
                                  numberOfMonths: 1,
                                  onClose: function(selectedDate) {
                                          $(textFrom).datepicker();
                                      }
                          });
                      $(textTo).focus(function() {
                              $(textTo).datepicker("show");
                          });
                  }
              }
        </script>
</body>
</html>


