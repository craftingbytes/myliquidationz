﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Costco.Common;
using Costco.BLL.Authorization;
using Costco.BLL;
using Costco.DAL.EntityModels;
using System.Web.Mvc;

namespace Costco.Web.Reports
{
    public partial class GenericReportWithDates : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;
        public GenericReportWithDates()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                rptviewer.Reset();


                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new CostcoService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                String reportName = Request["ReportName"];
                String displayName = Request["DisplayName"];


                rptviewer.ShowPrintButton = false;
                rptviewer.Reset();
                rptviewer.ProcessingMode = ProcessingMode.Remote;


                if (displayName == null)
                    rptviewer.ServerReport.DisplayName = reportName;
                else
                    rptviewer.ServerReport.DisplayName = displayName;

                ServerReportHelper.GetLoginInfo("/" + reportName, rptviewer.ServerReport);

                List<ReportParameter> newReportParams = new List<ReportParameter>();

                ReportParameterInfoCollection reportParams = rptviewer.ServerReport.GetParameters();
                foreach (ReportParameterInfo reportParam in reportParams)
                { 
                    
                    //do something
                    string parametername = reportParam.Name;
                    if (reportParam.State == ParameterState.MissingValidValue)
                    {
                        string paramName = reportParam.Name;
                        if (paramName == "AffiliateId")
                        {
                            ReportParameter newParam = new ReportParameter("AffiliateId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                            newReportParams.Add(newParam);
                        }
                        else
                        {
                            string requestParam = Request[paramName];
                            ReportParameter newParam = new ReportParameter(paramName, requestParam);
                            newReportParams.Add(newParam);
                        }                 
                    }
                }

                rptviewer.ServerReport.SetParameters(newReportParams);
                rptviewer.ServerReport.Refresh();

            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DatePickers.Value = string.Join(",", (GetDateParameters()));
        }

        public  List<string> GetDateParameters()
        {
            List<string> datepickers = new List<string>();
            // I'm assuming report view control id as reportViewer
            foreach (ReportParameterInfo info in rptviewer.ServerReport.GetParameters())
            {
                if (info.DataType == ParameterDataType.DateTime)
                {
                    datepickers.Add( string.Format("[{0}]", info.Prompt));
                }


                //if (info.Name == "Blank")
                //{
                //    blankHidden.Value = info.Values[0];
                //}
                //if (info.Name == "StartDateHidden")
                //{
                //    startDateHidden.Value = info.Values[0];
                //}

                //if (info.Name == "EndDateHidden")
                //{
                //    endDateHidden.Value = info.Values[0];
                //}
            }
            return datepickers;
        }
    }
}