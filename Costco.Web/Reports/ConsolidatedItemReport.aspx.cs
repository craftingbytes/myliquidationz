﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Costco.Common;
using Costco.BLL.Authorization;
using Costco.BLL;
using Costco.DAL.EntityModels;
using System.Web.Mvc;

namespace Costco.Web.Reports
{
    public partial class ConsolidatedItemReport : System.Web.UI.Page
    {
        private ISessionCookieValues _sessionValues = null;
        public ConsolidatedItemReport()
        {
            _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

                rptviewer.Reset();

                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new CostcoService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                ReportParameter rptParam = new ReportParameter("AffiliateId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));



                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(rptParam);

                rptviewer.ShowPrintButton = false;
                rptviewer.Reset();
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.SizeToReportContent = true;

                ServerReportHelper.GetLoginInfo("/ConsolidatedItemReport", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}