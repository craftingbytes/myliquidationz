﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenericReportWithDates.aspx.cs" Inherits="Costco.Web.Reports.GenericReportWithDates" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" style="height: 100%">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/cupertino/jquery-ui-1.10.2.custom.css")%>" />
    <script src='<%=ResolveClientUrl("~/Scripts/jquery-1.11.0.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/jquery-ui-1.10.2.custom.min.js")%>' type="text/javascript"></script>
   
</head>
<body style="height:100%">
    <form id="form1" runat="server" style="height: 100%">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="DatePickers" runat="server" />
    <asp:HiddenField ID="startDateHidden" runat="server" />
    <asp:HiddenField ID="endDateHidden" runat="server" />
    <asp:HiddenField ID="blankHidden" runat="server" />
    <rsweb:ReportViewer ID="rptviewer" runat="server" Width="100%"  height="1930px" >
    </rsweb:ReportViewer>

       
    </form>
     <script type="text/javascript">

         Sys.WebForms.PageRequestManager.getInstance().add_endRequest(applicationInitHandler);
         $(document).ready(function () {
             //alert("ready");
             //initHidden();
             //fixParameters();
         });

         //if ($.browser.chrome) {
         //function applicationHandler() {
         //    fixParameters();
         //}
         //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(fixParameters);

         //function initHidden() {
         //    var parameterRow = $("#ParametersRowrptviewer");
         //    var innerTable = $(parameterRow).find("table").find("table");
         //    //var startDateHidden = innerTable.find("span:contains('StartDateHidden')").parent("td").next("td").find("input[type=text]") ; //.parent().parent().find("td").eq(1).find("input[type=text]");
         //    var startDateText = innerTable.find("span:contains('Start Date')").parent("td").next("td").find("input[type=text]"); //.parent().parent().find("td").eq(1).find("input[type=text]");
         //    //var endDateHidden = innerTable.find("span:contains('EndDateHidden')").parent("td").next("td").find("input[type=text]"); // .parent().parent().find("td").eq(1).find("input[type=text]");
         //    //var endDateText = innerTable.find("span:contains('End Date')").parent("td").next("td").find("input[type=text]"); //.parent().parent().find("td").eq(1).find("input[type=text]");
         //    //var _startDate = $("#startDateHidden").val();
         //    //var _endDate = $("#endDateHidden").val();
         //    //startDateHidden.hide();
         //    //var _startDate = $("#startDateHidden").val().split(" ");
         //    //var _endDate = $("#endDateHidden").val().split(" ");
         //    //startDateText.val(_startDate[0]);
         //    //endDateText.val(_endDate[0]);
         //    var blankTR = innerTable.find("span:contains('Blank1')").parent("td").parent("tr");
         //    //blankTR.hide();
         //    var blankValueA = innerTable.find("span:contains('Blank1')").parent("td").next("td").find("input[type=text]"); //.parent().parent().find("td").eq(1).find("input[type=text]");
         //    var blankValueB = innerTable.find("span:contains('Blank2')").parent("td").next("td").find("input[type=text]"); //.parent().parent().find("td").eq(1).find("input[type=text]");
             
         //    blankValueA.val("A");
         //    blankValueB.val("B");
         //    blankValueB.change();
             
         //    var startDateValue = startDateText.val();

         //    //startDateText.change(function () {
         //    //    alert("Change detected!");
         //    //    $("#startDateHidden").val($(this).val());
         //    //});


         //    //currentForm.submit();

         //}
         function applicationInitHandler() {
             //alert("app handler");
             fixParameters();
         }
         function fixParameters() {
             var parameterRow = $("#ParametersRowrptviewer");
             var innerTable = $(parameterRow).find("table").find("table");
             //var blankTR = innerTable.find("span:contains('Blank1')").parent("td").parent("tr");
             //blankTR.hide();
             var startDateText = innerTable.find("span:contains('Start Date')").parent("td").next("td").find("input[type=text]");
             var endDateText = innerTable.find("span:contains('End Date')").parent("td").next("td").find("input[type=text]");
             var currentBrowser = getBrowser();
             var browserName = currentBrowser[0];
             if (browserName == "Chrome" || browserName == "Safari")
             {
                 
                 if (startDateText.attr("data-calendar") == "true")
                 {
                     alert("stuff");
                 } else{
                 
                    startDateText.datepicker({
                        showOn: "button"
                                , buttonImage: '/Reserved.ReportViewerWebControl.axd?OpType=Resource&Name=Microsoft.Reporting.WebForms.calendar.gif'
                                , buttonImageOnly: true
                     }).attr("data-calendar", "true");
                 }

                 if (endDateText.attr("data-calendar") == "true") {
                     alert("stuff2");
                 } else {

                     endDateText.datepicker({
                         showOn: "button"
                                 , buttonImage: '/Reserved.ReportViewerWebControl.axd?OpType=Resource&Name=Microsoft.Reporting.WebForms.calendar.gif'
                                 , buttonImageOnly: true
                     }).attr("data-calendar", "true");

                     endDateText.change(function () { startDateText.change();});
                 }
                 //$($(":hidden[id*='DatePickers']").val().split(",")).each(function (i, item) {
                 //    var h = $("table[id*='ParametersGrid'] span").filter(function (i) {
                 //        var v = "[" + $(this).text() + "]";
                 //        return (v != null && item != "" && v.indexOf(item) >= 0);                   
                 //    }).parent("td").next("td").find("input").datepicker({
                 //        showOn: "button"
                 //          , buttonImage: '/Reserved.ReportViewerWebControl.axd?OpType=Resource&Name=Microsoft.Reporting.WebForms.calendar.gif'
                 //          , buttonImageOnly: true
                 //    });
                 //});
                 //$($(":hidden[id*='DatePickers']").val().split(",")).each(function (i, item) {
                 //    var h = $("table[id*='ParametersGrid'] span").filter(function (i) {
                 //        var v = "[" + $(this).text() + "]";
                 //        return (v != null && item != "" && v.indexOf(item) >= 0);
                 //    }).parent("td").next("td").find("input").parent().children("input").each(function () {
                 //        $(this).val($(this).val().substring(0, 10));
                 //    });
                 //});
             }
         }

         function getBrowser() {
             var navigatorObj = navigator.appName,
                 userAgentObj = navigator.userAgent,
                 matchVersion;
             var match = userAgentObj.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*(\.?\d+(\.\d+)*)/i);
             if (match && (matchVersion = userAgentObj.match(/version\/([\.\d]+)/i)) !== null) match[2] = matchVersion[1];
             //mobile
             if (navigator.userAgent.match(/iPhone|Android|webOS|iPad/i)) {
                 return match ? [match[1], match[2], mobile] : [navigatorObj, navigator.appVersion, mobile];
             }
             // web browser
             return match ? [match[1], match[2]] : [navigatorObj, navigator.appVersion, '-?'];
         };


    </script>
</body>
</html>
