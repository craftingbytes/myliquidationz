﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Costco.Common;

namespace Costco.Web.Reports
{
    public partial class ShippingLabel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReportParameter rptParam = new ReportParameter("ShipmentId", Request["ShipmentId"]);

               

                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(rptParam);

                rptviewer.ShowPrintButton = false;
                rptviewer.Reset();
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "Shipping Label";
                rptviewer.SizeToReportContent = true;

                ServerReportHelper.GetLoginInfo("/ShippingLabel", rptviewer.ServerReport);

                rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}