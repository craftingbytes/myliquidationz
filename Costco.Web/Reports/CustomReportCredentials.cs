﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Reporting.WebForms;
using System.Net;
using System.Security.Principal;

namespace Costco.Web.Reports
{
    public class CustomReportCredentials : IReportServerCredentials
    {


        private string _userName;

        private string _password;

        private string _domain;

        public CustomReportCredentials(string userName, string password, string domain)
        {

            _userName = userName;

            _password = password;


            _domain = domain;
        }

        #region IReportServerCredentials Members

        bool IReportServerCredentials.GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
        {

            userName = _userName;

            password = _password;

            authority = _domain;
            authCookie = null;

            return false;
        }

        WindowsIdentity IReportServerCredentials.ImpersonationUser
        {
            get { return null; }
        }

        ICredentials IReportServerCredentials.NetworkCredentials
        {
            get { return new NetworkCredential(_userName, _password, _domain); }
        }

        #endregion
    }
}