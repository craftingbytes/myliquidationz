﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenericReport.aspx.cs" Inherits="Costco.Web.Reports.GenericReport" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" style="height: 100%">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/Content/css/cupertino/jquery-ui-1.10.2.custom.css")%>" />
    <script src='<%=ResolveClientUrl("~/Scripts/jquery-1.11.0.min.js")%>' type="text/javascript"></script>
    <script src='<%=ResolveClientUrl("~/Scripts/jquery-ui-1.10.2.custom.min.js")%>' type="text/javascript"></script>
<%--    <STYLE type="text/css">
      html, body, form  
            {  
                margin: 0;  
                padding: 0;  
                height: 100%;  
                overflow: hidden;  
                font-family: Verdana, Tahoma, Arial;  
                font-size: small;  
            } 
       .spinner {
	    position: fixed;
	    top: 50%;
	    left: 50%;
	    margin-left: -50px; /* half width of the spinner gif */
	    margin-top: -50px; /* half height of the spinner gif */
	    text-align:center;
	    z-index:1234;
	    overflow: auto;
	    width: 150px; /* width of the spinner gif */
	    height: 62px; /*height of the spinner gif +2px to fix IE8 issue */
	    background-color:#E1E1D7; 
	    border:1px solid black;
    }

    .HighlightDiv img{
    background-color: transparent; 
    border-top-width: 1px; 
    border-right-width: 1px; 
    border-bottom-width: 1px; 
    border-left-width: 1px; 
    border-top-color: transparent; 
    border-right-color: transparent; 
    border-bottom-color: transparent; 
    border-left-color: transparent; 
    border-top-style: solid; 
    border-right-style: solid; 
    border-bottom-style: solid; 
    border-left-style: solid; 
    cursor: default; 
    }

    .HighlightDiv:hover img{
    background-color: #DDEEF7; 
    border-top-width: 1px; 
    border-right-width: 1px; 
    border-bottom-width: 1px; 
    border-left-width: 1px; 
    border-top-color: #336699; 
    border-right-color: #336699; 
    border-bottom-color: #336699; 
    border-left-color: #336699; 
    border-top-style: solid; 
    border-right-style: solid; 
    border-bottom-style: solid; 
    border-left-style: solid; 
    cursor:pointer; 
    }
 </STYLE>--%>
    
</head>
<body style="height:100%">
    <form id="form1" runat="server" style="height: 100%">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="divMain">
        <rsweb:ReportViewer ID="rptviewer" runat="server" Width="100%"  height="100%" >
        </rsweb:ReportViewer>
        <%--<iframe id="frmPrint" name="frmPrint" runat="server" style = "display:none"></iframe>--%>
    </div>
    <%--<div id="spinner" class="spinner" style="display:none;">
    <table align="center" valign="middle" style="height:100%;width:100%">
    <tr>
    <td><img id="img-spinner" src="/Reserved.ReportViewerWebControl.axd?OpType=Resource&Name=Microsoft.Reporting.WebForms.Icons.SpinningWheel.gif" alt="Printing"/></td>
    <td><span style="font-family:Verdana; font-weight:bold;font-size:10pt;width:86px;">Printing...</span></td>
    </tr>
    </table>
    </div>
       --%>
    </form>
    <script type="text/javascript">

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(applicationInitHandler);
        $(document).ready(function () {
            
            //$("#spinner").bind("ajaxSend", function () {
            //    $(this).show();
            //}).bind("ajaxStop", function () {
            //    $(this).hide();
            //}).bind("ajaxError", function () {
            //    $(this).hide();
            //});

        });

        function applicationInitHandler() {
            fixParameters();
            //showPrintButton();
        }
        function fixParameters() {
            var parameterRow = $("#ParametersRowrptviewer");
            var innerTable = $(parameterRow).find("table").find("table");
            var startDateText = innerTable.find("span:contains('Start Date')").parent("td").next("td").find("input[type=text]");
            var endDateText = innerTable.find("span:contains('End Date')").parent("td").next("td").find("input[type=text]");
            var currentBrowser = getBrowser();
            var browserName = currentBrowser[0];
            if (browserName == "Chrome" || browserName == "Safari") {

                if (startDateText.attr("data-calendar") == "true") {
                    //alert("stuff");
                } else {

                    startDateText.datepicker({
                        showOn: "button"
                                , buttonImage: '/Reserved.ReportViewerWebControl.axd?OpType=Resource&Name=Microsoft.Reporting.WebForms.calendar.gif'
                                , buttonImageOnly: true
                    }).attr("data-calendar", "true");
                }

                if (endDateText.attr("data-calendar") == "true") {
                    //alert("stuff2");
                } else {

                    endDateText.datepicker({
                        showOn: "button"
                                , buttonImage: '/Reserved.ReportViewerWebControl.axd?OpType=Resource&Name=Microsoft.Reporting.WebForms.calendar.gif'
                                , buttonImageOnly: true
                    }).attr("data-calendar", "true");

                    endDateText.change(function () { startDateText.change(); });
                }
            }
        }

        function getBrowser() {
            var navigatorObj = navigator.appName,
                userAgentObj = navigator.userAgent,
                matchVersion;
            var match = userAgentObj.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*(\.?\d+(\.\d+)*)/i);
            if (match && (matchVersion = userAgentObj.match(/version\/([\.\d]+)/i)) !== null) match[2] = matchVersion[1];
            //mobile
            if (navigator.userAgent.match(/iPhone|Android|webOS|iPad/i)) {
                return match ? [match[1], match[2], mobile] : [navigatorObj, navigator.appVersion, mobile];
            }
            // web browser
            return match ? [match[1], match[2]] : [navigatorObj, navigator.appVersion, '-?'];
        }

        function ServerCallSucceeded(result, context) {
            var iFrameURL = "<%=iFrameURL%>";
            window.frames['frmPrint'].document.location.href = iFrameURL;
            window.frames['frmPrint'].focus();
            var timeout = window.setTimeout("window.frames[\"frmPrint\"].focus();window.frames[\"frmPrint\"].print();", 500);
            window.setTimeout("ServerCallAfterPrint(this)", 2000);
        }

        function ServerCallSucceededAfterPrint(result, context) {
        }

        function ServerCallFailed(result, context) {
        }

        function ServerCallBeforePrint(btn) {
            $('#spinner').show();
            var context = new Object();
            //example of passing multiple args
            context.flag = new Array('Today', 'Tomorrow');
            //This "CallServerMethodBeforePrint" function is created from code behind and will exist in the final rendering of the page
            CallServerMethodBeforePrint(context.flag, context);
        }

        function ServerCallAfterPrint(btn) {
            var context = new Object();
            //example of passing multiple args
            context.flag = new Array('Today', 'Tomorrow');
            //This "CallServerAfterPrint" function is created from code behind and will exist in the final rendering of the page
            CallServerAfterPrint(context.flag, context);
            $('#spinner').hide();
        }

        function printPDF(btn) {
            ServerCallBeforePrint(btn);
        }

        function showPrintButton() {
            var table = $("table[title='Refresh']");
            var parentTable = $(table).parents('table');
            var parentDiv = $(parentTable).parents('div').parents('div').first();
            var btnPrint = $("<input type='button' id='btnPrint' name='btnPrint' value='Print' style=\"font-family:Verdana;font-size:8pt;width:86px\"/>");
            //var btnClose = $("<input type='button' id='btnClose' name='btnClose'value='Close' style=\"font-family:Verdana;font-size:8pt;width:86px\"/>");
            btnPrint.click(function () {
                printPDF(this);
            });
            //btnClose.click(function () {
            //    window.close();
            //});
            
            //if (parentDiv.find("input[value='Print']").length == 0) 
            if ($("div[id='customDiv']").length == 0) {
                parentDiv.append('<table cellpadding="0" cellspacing="0" toolbarspacer="true" style="display:inline-block;width:6px;"><tbody><tr><td></td></tr></tbody></table>');
                parentDiv.append('<div id="customDiv" class=" " style="display:inline-block;font-family:Verdana;font-size:8pt;vertical-align:inherit;"><table cellpadding="0" cellspacing="0"><tbody><tr><td><span style="cursor:pointer;" class="HighlightDiv" onclick="javascript:printPDF(this);" ><img src="/Reserved.ReportViewerWebControl.axd?OpType=Resource&Name=Microsoft.Reporting.WebForms.Icons.Print.gif" alt="Print Report" title="Print Report" width="18px" height="18px" style="margin-top:4px"/></span></td></tr></tbody></table></div>');
                //parentDiv.append('<table cellpadding="0" cellspacing="0" toolbarspacer="true" style="display:inline-block;width:10px;"><tbody><tr><td></td></tr></tbody></table>');
                //parentDiv.append('<div id="customDiv" class=" " style="display:inline-block;font-family:Verdana;font-size:8pt;vertical-align:inherit;"><table cellpadding="0" cellspacing="0" style="display:inline;"><tbody><tr><td><span style="cursor:pointer;" class="HighlightDiv" onclick="javascript:window.close();"><img src="../Images/cross-circle-frame.png" alt="Close Report" title="Close Report" width="18px" height="18px" style="margin-top:4px"/></span></td></tr></tbody></table></div>');
            }
        }

        function cfnReportsViewer_ViewReport(selectedTreeKeyGuidValue, ReportName, VenueExamCounter) {
            var windowWidth = 1000;
            var windowHeight = 800;
            var left = (screen.width / 2) - (windowWidth / 2);
            var top = (screen.height / 2) - (windowHeight / 2);
            var myForm = document.getElementById("frmReportViewer");
            if (myForm) {
                myForm.target = "PopupReport";
            }
            $("#hfAccessObjectGuid").val(selectedTreeKeyGuidValue);
            $("#hfReportName").val(ReportName);
            $("#hfVenueExamCounter").val(VenueExamCounter);
            var thePopup = window.open("about:blank", "PopupReport", 'scrollbars=yes,status=yes,toolbar=yes,menubar=no,location=no,resizable=no,fullscreen=yes, width=' + windowWidth + ', height=' + windowHeight + ', top=' + top + ', left=' + left);
            window.setTimeout(document.getElementById("frmReportViewer").submit(), 500);
            return false;
        }
    </script>
</body>
</html>
