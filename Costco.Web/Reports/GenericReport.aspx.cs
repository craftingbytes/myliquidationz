﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Costco.Common;
using Costco.BLL.Authorization;
using Costco.BLL;
using Costco.DAL.EntityModels;
using System.IO;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Web.Mvc;

namespace Costco.Web.Reports
{
    public partial class GenericReport : System.Web.UI.Page, ICallbackEventHandler
    {
        private string _reportName;
        private string _sessionPDFFileName;
        protected string iFrameURL;

        private ISessionCookieValues _sessionValues = null;
        public GenericReport()
        {
            _sessionValues = (ISessionCookieValues) DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _reportName = "ReportName";
            _sessionPDFFileName = Session.SessionID.ToString() + ".pdf";
            //Attach pdf to the iframe
            string url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath + "Uploads/" + _sessionPDFFileName;
            iFrameURL = url;
            RegisterClientsCallBackReference();
            RegisterClientCallBackAfterPrint();
            this.Page.Title = _reportName;

            if (!IsPostBack)
            {
                

                rptviewer.Reset();

                int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                Affiliate aff = new CostcoService<Affiliate>().GetSingle(a => a.Id == affiliateId);

                if (aff == null)
                {

                    Response.RedirectToRoute("AccessDenied", "Home");
                    return;
                }

                String reportName = Request["ReportName"];
                String displayName = Request["DisplayName"];


                rptviewer.ShowPrintButton = false;
                rptviewer.Reset();
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.SizeToReportContent = true;
                //rptviewer.AsyncRendering = true;

                if (displayName == null)
                    rptviewer.ServerReport.DisplayName = reportName;
                else
                    rptviewer.ServerReport.DisplayName = displayName;

                ServerReportHelper.GetLoginInfo("/" + reportName, rptviewer.ServerReport);
                
                List<ReportParameter> newReportParams = new List<ReportParameter>();

                ReportParameterInfoCollection reportParams = rptviewer.ServerReport.GetParameters();
                foreach (ReportParameterInfo reportParam in reportParams)
                { 
                    
                    //do something
                    string parametername = reportParam.Name;
                    if (reportParam.State == ParameterState.MissingValidValue)
                    {
                        string paramName = reportParam.Name;
                        if (paramName == "AffiliateId")
                        {
                            ReportParameter newParam = new ReportParameter("AffiliateId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                            newReportParams.Add(newParam);
                        }
                        else
                        {
                            string requestParam = Request[paramName];
                            ReportParameter newParam = new ReportParameter(paramName, requestParam);
                            newReportParams.Add(newParam);
                        }                 
                    }
                }

                rptviewer.ServerReport.SetParameters(newReportParams);
                rptviewer.ServerReport.Refresh();



                //ReportParameter affParam = new ReportParameter("AffiliateId", _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateId));
                //ReportParameter rptParam = new ReportParameter("SalesOrderId", Request["SalesOrderId"]);
                //List<ReportParameter> reportParams = new List<ReportParameter>();
                //reportParams.Add(affParam);
                //reportParams.Add(rptParam);
                //rptviewer.ServerReport.SetParameters(reportParams);

                //rptviewer.ServerReport.Refresh();
            }
        }


        protected void ShowPrintButton()
        {

            //string script = "<SCRIPT LANGUAGE='JavaScript'> ";
            //script += "showPrintButton()";
            //script += "</SCRIPT>";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowStatus", "javascript:showPrintButton();", true);

        }

        protected void SavePDF()
        {
            try
            {



                string _reportPath = ConfigurationManager.AppSettings["FolderPath"];
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;
                rptviewer.LocalReport.ReportPath = _reportPath + _reportName;

                byte[] bytes = rptviewer.ServerReport.Render("PDF", "", out mimeType, out encoding, out extension, out streamids, out warnings);
                //save the pdf byte to the folder
                if (!File.Exists(Server.MapPath("../Uploads/" + _sessionPDFFileName)))
                {
                    using (StreamWriter sw = new StreamWriter(File.Create(Server.MapPath("../Uploads/" + _sessionPDFFileName))))
                    {
                        sw.Write("");
                    }
                }
                FileStream fs = new FileStream(Server.MapPath("../Uploads/" + _sessionPDFFileName), FileMode.Open);
                byte[] data = new byte[fs.Length];
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                String error = e.Message;
            }
        }

        protected void rrvREXReport_ReportRefresh(object sender, CancelEventArgs e)
        {
            //ShowPrintButton();
        }


        public string AjaxCall(string name)
        {
            //var ajaxcall = Request.Form["ajaxcall"];

            if (name != null)
            {
                if (File.Exists(Server.MapPath("../Uploads/" + _sessionPDFFileName)))
                {
                    //frmPrint.Attributes["src"] = "";
                    File.Delete(Server.MapPath("../Uploads/" + _sessionPDFFileName));
                }
                else
                {
                    SavePDF();
                }
            }
            return name;
        }

        string returnValue;

        string ICallbackEventHandler.GetCallbackResult()
        {
            return returnValue;
        }

        void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
        {
            returnValue = AjaxCall(eventArgument);
        }

        private void RegisterClientsCallBackReference()
        {

            String myClientsCallBack = Page.ClientScript.GetCallbackEventReference(this, "arg", "ServerCallSucceeded", "context", "ServerCallFailed", true);
            //Could also call this wtihout the callback succeeded or failed methods:
            String myCompleteClientFunction = @"function CallServerMethodBeforePrint(arg, context)
                                      { " +
                                            myClientsCallBack + @"; 
                                      }";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "TheScriptToCallServer", myCompleteClientFunction, true);

        }

        private void RegisterClientCallBackAfterPrint()
        {
            String myClientAfterPrintCallBack = Page.ClientScript.GetCallbackEventReference(this, "arg", "ServerCallSucceededAfterPrint", "context", "ServerCallFailed", true);

            String myAfterPrintCompleteClientFunction = @"function CallServerAfterPrint(arg, context)
                                      { " +
                                            myClientAfterPrintCallBack + @"; 
                                      }";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "TheScriptToCallServerAfterPrint", myAfterPrintCompleteClientFunction, true);
        }
    }
}