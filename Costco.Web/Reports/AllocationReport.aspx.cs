﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Costco.Common;

namespace Costco.Web.Reports
{
    public partial class AllocationReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ReportParameter rptParam = new ReportParameter("RecyclerId", "CompuCycle");
                //List <ReportParameter> reportParams = new List<ReportParameter>();
                //reportParams.Add(rptParam);

                rptviewer.ShowPrintButton = false;
                rptviewer.Reset();
                rptviewer.ProcessingMode = ProcessingMode.Remote;
                rptviewer.ServerReport.DisplayName = "AllocationReport";
                rptviewer.SizeToReportContent = true;

                ServerReportHelper.GetLoginInfo("/AllocationReport", rptviewer.ServerReport);

                //rptviewer.ServerReport.SetParameters(reportParams);

                rptviewer.ServerReport.Refresh();
            }
        }
    }
}