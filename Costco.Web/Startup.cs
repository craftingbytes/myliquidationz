﻿using System;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Extensions.DependencyInjection;
using System.Web.Mvc;
using Costco.Common;
using System.Linq;

[assembly: OwinStartup(typeof(Costco.Web.Startup))]

namespace Costco.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                CookieHttpOnly = true,
#if !DEBUG
                CookieSecure = CookieSecureOption.Always,
#endif
            });

            var services = new ServiceCollection();
            ConfigureServices(services);

            DependencyResolver.SetResolver(new DefaultDependencyResolver(services.BuildServiceProvider()));
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped(typeof(ISessionCookieValues), typeof(SessionCookieValues));

            //This will find all of the public types that implement the IController interface or end with the word “Controller”
            //in the class name that aren’t abstract or an open generic type and add cause them to be added to the services collection.
            services.AddControllersAsServices(typeof(Startup).Assembly.GetExportedTypes()
               .Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition)
               .Where(t => typeof(IController).IsAssignableFrom(t)
                  || t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)));
        }
    }
}
