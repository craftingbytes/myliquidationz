﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Costco.Common;
using System.IO;

namespace Costco.Web.Controllers
{
    public class FileUploadController : CostcoApplicationController
    {
        //
        // GET: /FileUpload/

        public FileUploadController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult FileExists(FormCollection forms)
        {
            Dictionary<string, string> fileArray = new Dictionary<string, string>();
            foreach (string key in forms.AllKeys)
            {
                if (key != "folder")
                {
                    string targetDirectory = Path.Combine(Request.PhysicalApplicationPath, @"uploads\");
                    string targetFilePath = Path.Combine(targetDirectory, forms[key]);
                    if (System.IO.File.Exists(targetFilePath))
                        fileArray.Add(key, forms[key]);
                }
            }

            //return Json(fileArray);
            if (fileArray.Count > 0)
                return Json(1);
            else
                return Json(0);
        }

        public JsonResult IsFileExist(string fileName)
        {
            if (fileName != "-")
            {
                fileName = fileName.Replace("<~>", "#");
                fileName = fileName.Replace("�", " ");
                string virtualPath = "~/Uploads/" + fileName.Replace(" ", " ");
                string serverPath = ControllerContext.HttpContext.Server.MapPath(virtualPath);
                if (System.IO.File.Exists(serverPath))
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }
            }
            else
            {
                return null;
            }
        }

        public DownloadResult Download(string fileName)
        {
            //string fileName = Convert.ToString(Request.QueryString["FileName"]);

            if (fileName != "-")
            {
                fileName = fileName.Replace("<~>", "#");
                fileName = fileName.Replace("�", " ");
                string virtualPath = "~/Uploads/" + fileName.Replace(" ", " ");
                string serverPath = ControllerContext.HttpContext.Server.MapPath(virtualPath);
                return new DownloadResult { VirtualPath = virtualPath, FileDownloadName = fileName };
            }
            else
            {
                return null;
            }
        }
    }

    public class DownloadResult : ActionResult
    {

        public DownloadResult()
        {
        }

        public DownloadResult(string virtualPath)
        {
            this.VirtualPath = virtualPath;
        }
        public string VirtualPath
        {
            get;
            set;
        }

        public string FileDownloadName
        {
            get;
            set;
        }

        public override void ExecuteResult(ControllerContext context)
        {

            if (!String.IsNullOrEmpty(FileDownloadName))
            {

                string ext = Path.GetExtension(FileDownloadName);
                string type = "";

                switch (ext.ToLower())
                {

                    case ".htm":
                    case ".html":
                    case ".log":
                        type = "text/HTML";
                        break;
                    case ".txt":
                        type = "text/plain";
                        break;
                    case ".doc":
                        type = "application/ms-word";
                        break;
                    case ".tiff":

                    case ".tif":
                        type = "image/tiff";
                        break;
                    case ".asf":
                        type = "video/x-ms-asf";
                        break;
                    case ".avi":
                        type = "video/avi";
                        break;
                    case ".zip":
                        type = "application/zip";
                        break;
                    case ".xls":
                    case ".csv":
                        type = "application/vnd.ms-excel";
                        break;
                    case ".gif":
                        type = "image/gif";
                        break;
                    case ".jpg":
                    case "jpeg":
                        type = "image/jpeg";
                        break;
                    case ".bmp":
                        type = "image/bmp";
                        break;
                    case ".wav":
                        type = "audio/wav";
                        break;
                    case ".mp3":
                        type = "audio/mpeg3";
                        break;
                    case ".mpg":
                    case "mpeg":
                        type = "video/mpeg";
                        break;
                    case ".rtf":
                        type = "application/rtf";
                        break;
                    case ".asp":
                        type = "text/asp";
                        break;
                    case ".pdf":
                        type = "application/pdf";
                        break;
                    case ".fdf":
                        type = "application/vnd.fdf";
                        break;
                    case ".ppt":
                        type = "application/mspowerpoint";
                        break;
                    case ".dwg":
                        type = "image/vnd.dwg";
                        break;
                    case ".msg":
                        type = "application/msoutlook";
                        break;
                    case ".xml":
                    case ".sdxl":
                        type = "application/xml";
                        break;
                    case ".xdp":
                        type = "application/vnd.adobe.xdp+xml";
                        break;
                    default:
                        type = "application/octet-stream";
                        break;
                }
                context.HttpContext.Response.AppendHeader("content-disposition", "attachment; filename=" + HttpContext.Current.Server.UrlEncode(FileDownloadName));
                context.HttpContext.Response.ContentType = type;
                string serverPath = context.HttpContext.Server.MapPath(this.VirtualPath);
                {
                    context.HttpContext.Response.WriteFile(serverPath);
                }

            }


        }
    }
}
