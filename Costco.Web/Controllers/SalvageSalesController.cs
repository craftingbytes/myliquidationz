﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL.BusinessObjects;
using Costco.BLL.Authorization;
using Costco.Common;
using Costco.DAL.EntityModels;
using Costco.BLL.ViewModels;
using Costco.BLL;

namespace Costco.Web.Controllers
{
    public class SalvageSalesController : CostcoApplicationController
    {

        private SalvageSalesBO _ssOrderBO;
        private ProductBO _productBO;

        private AccessRights AccessRightsSalvageSalesOrdersSell
        {
            get
            {
                // Authorization authorization = new Authorization();              SalesOrdersSeller
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "SalvageSalesOrderSell");

            }
        }

        private Int32 CurrentSalvageSalesOrderId
        {
            set
            {
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.CurrentSalvageSalesOrderId, value);
            }
            get
            {
                return _sessionValues.GetSessionIntValue(Constants.SessionParameters.CurrentSalvageSalesOrderId);
            }
        }

        public SalvageSalesController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            _ssOrderBO = new SalvageSalesBO();
            _productBO = new ProductBO(_sessionValues);
        }

        //
        // GET: /SalvageSales/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SellerList()
        {
            return View();
        }



        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            if (AccessRightsSalvageSalesOrdersSell == null || AccessRightsSalvageSalesOrdersSell.Add.HasValue == false
                    || AccessRightsSalvageSalesOrdersSell.Add.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            Int32 ssOrderId = 0;
            try
            {
                SalvageSalesOrder ssOrder = new SalvageSalesOrder();
                ssOrder.AffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                ssOrder.DateCreated = DateTime.Now;



                ssOrderId = _ssOrderBO.SaveSalvageSalesOrder(ssOrder);
                if (ssOrderId == 0)
                    throw new Exception();

            }
            catch (Exception)
            {

                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("EditSeller", new { id = ssOrderId });
        }

        [HttpGet]
        public ActionResult GetSalvageSalesOrderList(string sidx, string sord, int page, int rows, string BeginDate, string EndDate, string BeginId, string EndId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _ssOrderBO.GetPaged(pageIndex, pageSize, BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), BeginId, EndId, out totalRecords).AsEnumerable<SalvageSalesOrder>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            //'Id', 'Created', 'SoldDate', 'Sold To', 'SoldToAffiliateId',  'Amount'
                            m.Id,
                            String.Format("{0:MM/dd/yyyy}",m.DateCreated),
                            String.Format("{0:MM/dd/yyyy}",m.DateSold),
                            m.SoldToName,
                            m.SoldToAffiliateId,
                            m.SoldAmount,
                            m.DateSold == null ? false : true
                            
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        public ActionResult EditSeller(Int32 id)
        {
            if (AccessRightsSalvageSalesOrdersSell == null || AccessRightsSalvageSalesOrdersSell.Update.HasValue == false
                    || AccessRightsSalvageSalesOrdersSell.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CurrentSalvageSalesOrderId = id;

            SalvageSalesOrder salesOrder = _ssOrderBO.GetSalvageSalesOrder(CurrentSalvageSalesOrderId);
            if (salesOrder == null)
            {
                return RedirectToAction("Error", "Home");
            }

            if (salesOrder.DateSold != null) {
                return RedirectToAction("SellerList");
            }
            ViewData["Data"] = "{" + Request["data"] + "}";
            SalvageSalesOrderViewModel model = new SalvageSalesOrderViewModel(salesOrder);


            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            if (salesOrder.SoldToStateId != null)
                ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name", salesOrder.SoldToStateId);
            else
                ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name");

            List<City> cityList = new List<City>();
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            if (salesOrder.SoldToCityId != null)
                ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name", salesOrder.SoldToCityId);
            else
                ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name");

            var buyerList = new CostcoService<Affiliate>().GetAll(x => (x.AffiliateTypeId == 7 && x.ParentId == salesOrder.AffiliateId) || x.Id == 533).ToList();
            buyerList.Insert(buyerList.Count, new Affiliate() { Id = 999999, Name = "New Buyer" });
            buyerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            if (salesOrder.SoldToAffiliateId != null)
                ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name", salesOrder.SoldToAffiliateId);
            else
                ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name");


            return View(model);
        }


        [HttpGet]
        public ActionResult GetSalvageItemsShipper(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _ssOrderBO.GetSSItems(pageIndex, pageSize, CurrentSalvageSalesOrderId, out totalRecords).AsEnumerable<SalvageSalesOrderItem>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            //[ 'ShipmentItemID','Pallet #', 'ItemId', 'Description', 'Qty', ,'Price', 'Actions'],
                            m.Id,
                            m.PalletUnitNumber,
                            m.ItemId,
                            m.Description,
                            m.SalvageSalesOrderId,
                            m.RetailValue,
                            m.SellingPrice,
                            m.Quantity
                            ,m.Quantity * m.SellingPrice
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult EditSalvageItemShipper(FormCollection col)
        {
            if (AccessRightsSalvageSalesOrdersSell == null || AccessRightsSalvageSalesOrdersSell.Update.HasValue == false
                    || AccessRightsSalvageSalesOrdersSell.Update.Value == false)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", shipmentId = 0 }, JsonRequestBehavior.AllowGet);
            }
            SalvageSalesOrderItem salvageItem = null;
            Int64 ssItemId = 0;
            bool saved = false;
            decimal[] soldTotals;
            try
            {
                if (col.AllKeys.Contains("oper"))
                {
                    decimal retailValue = 0;
                    if (col["Retail"] != null)
                    {
                        string strRetail = col["Retail"];
                        decimal result;
                        if (decimal.TryParse(strRetail, out result))
                            retailValue = result;
                    }
                    decimal sellingPrice = 0;
                    if (col["Price"] != null)
                    {
                        string strPrice = col["Price"];
                        decimal result;
                        if (decimal.TryParse(strPrice, out result))
                            sellingPrice = result;
                    }

                    if (col["oper"] == "add")
                    {
                        salvageItem = new SalvageSalesOrderItem();
                        salvageItem.PalletUnitNumber = col["PalletUnitNumber"];
                        salvageItem.ItemId = col["ItemId"];

                        
                        string storenumber = col["Store"];
                        if (storenumber != null && storenumber.Trim() != "")
                            salvageItem.Store = Convert.ToInt32(storenumber);

                        salvageItem.SalvageSalesOrderId = Convert.ToInt32(col["SalesOrderId"]);

                        salvageItem.Description = col["Description"];

                        salvageItem.Quantity = Convert.ToInt32(col["Qty"]);
                        salvageItem.RetailValue = retailValue;
                        salvageItem.SellingPrice = sellingPrice;
                        ssItemId = _ssOrderBO.AddSalvageSalesOrderItem(salvageItem);
                    }
                    else if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                    {
                        //int currentShipmentItemID = Convert.ToInt32(col["id"]);
                        salvageItem = _ssOrderBO.GetSalvageSalesOrderItem(Convert.ToInt64(col["id"]));
                        if (salvageItem != null)
                        {
                            string storenumber = col["Store"];
                            if (storenumber != null && storenumber.Trim() != "")
                                salvageItem.Store = Convert.ToInt32(storenumber);
                            salvageItem.PalletUnitNumber = col["PalletUnitNumber"];
                            salvageItem.ItemId = col["ItemId"];
                            salvageItem.Description = col["Description"];
                            salvageItem.Quantity = Convert.ToInt32(col["Qty"]);
                            salvageItem.RetailValue = retailValue;
                            salvageItem.SellingPrice = sellingPrice;
                            saved = _ssOrderBO.UpdateSalvageSalesOrderItem(salvageItem);
                        }
                    }
                    Product product = new Product();
                    product.ItemId = col["ItemId"];
                    product.Description = col["Description"];
                    //product.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
                    //product.Weight = Convert.ToDecimal(col["Weight"]);
                    _productBO.AddProduct(product);


                    if (salvageItem.ItemId.ToString().ToUpper() != "MISC" && salvageItem.ItemId.ToString().ToUpper() != "UNKNOWN")
                    {
                        v_MaxProductPrice price = _productBO.GetProductPriceByItemId(salvageItem.ItemId);
                        if (price.Price != retailValue && retailValue != 0)
                        {
                            ProductPrice productPrice = new ProductPrice();
                            productPrice.ItemId = salvageItem.ItemId;
                            productPrice.Price = retailValue;
                            productPrice.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                            productPrice.TimeStamp = DateTime.Now;
                            _productBO.AddProductPrice(productPrice);
                        }
                    }

                }
                soldTotals = calculateSoldAmount();
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", ssItemId = 0 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, ssItemId = ssItemId, soldAmount = soldTotals[0], retailAmount = soldTotals[1] }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteSalvageSalesItem(Int64 SalvageItemID)
        {
            try
            {
                _ssOrderBO.DeleteSalvageSalesOrderItem(SalvageItemID);
                decimal[] soldTotals = calculateSoldAmount();
                return Json(new { status = true, soldAmount = soldTotals[0], retailAmount = soldTotals[1] }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }

        private decimal[] calculateSoldAmount() {
            decimal[] totals = new decimal[2];
            totals[0] = 0;
            totals[1] = 0;

            CostcoEntities db = new CostcoEntities();
            decimal soldAmount;
            decimal retailAmount;
            db.p_UpdateSalvageSalesOrderSoldAmount(CurrentSalvageSalesOrderId, out soldAmount, out retailAmount);

            //return Json(new { status = true }, JsonRequestBehavior.AllowGet);
            totals[0] = soldAmount;
            totals[1] = retailAmount;
            return totals;
        }


        [HttpPost]
        public ActionResult SetBuyer(Int32 id, int soldToId, string soldToName, string soldToAddress, int soldToStateId, int soldToCityId, string soldToZip)
        {
            if (AccessRightsSalvageSalesOrdersSell == null || AccessRightsSalvageSalesOrdersSell.Update.HasValue == false
                    || AccessRightsSalvageSalesOrdersSell.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            SalvageSalesOrder ssOrder = _ssOrderBO.GetSalvageSalesOrder(id);
            try
            {
                if (soldToId == 999999)
                {
                    Affiliate buyer = new Affiliate();
                    buyer.Address1 = soldToAddress;
                    buyer.StateId = soldToStateId;
                    buyer.CityId = soldToCityId;
                    buyer.Zip = soldToZip;
                    buyer.Name = soldToName;
                    buyer.ParentId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                    buyer.AffiliateTypeId = 7;
                    int newBuyerId = _ssOrderBO.AddBuyer(buyer);
                    ssOrder.SoldToAffiliateId = newBuyerId;
                }
                else
                {
                    ssOrder.SoldToAffiliateId = soldToId;
                }

                ssOrder.SoldToAddress = soldToAddress;
                ssOrder.SoldToStateId = soldToStateId;
                ssOrder.SoldToCityId = soldToCityId;
                ssOrder.SoldToZip = soldToZip;
                ssOrder.SoldToName = soldToName;
                _ssOrderBO.UpdateSalvageSalesOrder(ssOrder);
            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to save Salvage Sales Order." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Sell(Int32 id, string soldBy, string soldDate)
        {
            if (AccessRightsSalvageSalesOrdersSell == null || AccessRightsSalvageSalesOrdersSell.Update.HasValue == false
                    || AccessRightsSalvageSalesOrdersSell.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            SalvageSalesOrder ssOrder = _ssOrderBO.GetSalvageSalesOrder(id);
            try{
                ssOrder.SoldBy = soldBy;
                ssOrder.DateSold = Convert.ToDateTime(soldDate);
                _ssOrderBO.UpdateSalvageSalesOrder(ssOrder);
            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to save Salvage Sales Order." }, JsonRequestBehavior.AllowGet);
            }
            //return RedirectToAction("SellerList");
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BuyerList()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetSalvageSalesOrderBuyerList(string sidx, string sord, int page, int rows, string BeginDate, string EndDate, string BeginId, string EndId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _ssOrderBO.GetPagedBuyer(pageIndex, pageSize, BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), BeginId, EndId, out totalRecords).AsEnumerable<V_SalvagSalesOrderBought>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        //['Id', 'SoldDate', 'Store', 'SoldToAffiliateId',  'Amount' ,'Action'],
                        id = m.Id,
                        cell = new object[] {
                            //'Id', 'Created', 'SoldDate', 'Sold To', 'SoldToAffiliateId',  'Amount'
                            m.Id,
                            String.Format("{0:MM/dd/yyyy}",m.DateSold),
                            m.SellerName,
                            String.Format("{0:MM/dd/yyyy}",m.ReceivedDate),
                            _ssOrderBO.GetMissing(m.Id),
                            m.SoldToAffiliateId,
                            m.SoldAmount
                            
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        public ActionResult EditBuyer(Int32 id)
        {
            //if (AccessRightsSalvageSalesOrdersSell == null || AccessRightsSalvageSalesOrdersSell.Update.HasValue == false
            //        || AccessRightsSalvageSalesOrdersSell.Update.Value == false)
            //{
            //    return RedirectToAction("AccessDenied", "Home");
            //}
            CurrentSalvageSalesOrderId = id;

            SalvageSalesOrder salesOrder = _ssOrderBO.GetSalvageSalesOrder(CurrentSalvageSalesOrderId);
            if (salesOrder == null)
            {
                return RedirectToAction("Error", "Home");
            }

            //if (salesOrder.DateSold != null)
            //{
            //    return RedirectToAction("SellerList");
            //}
            ViewData["Data"] = "{" + Request["data"] + "}";
            SalvageSalesOrderViewModel model = new SalvageSalesOrderViewModel(salesOrder);


            //var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            //stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            //if (salesOrder.SoldToStateId != null)
            //    ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name", salesOrder.SoldToStateId);
            //else
            //    ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name");

            //List<City> cityList = new List<City>();
            //cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            //if (salesOrder.SoldToCityId != null)
            //    ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name", salesOrder.SoldToCityId);
            //else
            //    ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name");

            //var buyerList = new CostcoService<Affiliate>().GetAll(x => (x.AffiliateTypeId == 7 && x.ParentId == salesOrder.AffiliateId) || x.Id == 533).ToList();
            //buyerList.Insert(buyerList.Count, new Affiliate() { Id = 999999, Name = "New Buyer" });
            //buyerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            //if (salesOrder.SoldToAffiliateId != null)
            //    ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name", salesOrder.SoldToAffiliateId);
            //else
            //    ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name");


            return View(model);
        }

        [HttpGet]
        public ActionResult GetSalvageItemsBuyer(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _ssOrderBO.GetSSItems(pageIndex, pageSize, CurrentSalvageSalesOrderId, out totalRecords).AsEnumerable<SalvageSalesOrderItem>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            //[ 'ShipmentItemID','Pallet #', 'ItemId', 'Description', 'Qty', ,'Price', 'Actions'],
                            m.Id,
                            m.PalletUnitNumber,
                            m.ItemId,
                            m.Description,
                            m.SalvageSalesOrderId,
                            m.RetailValue,
                            m.SellingPrice,
                            m.Quantity
                            ,m.Quantity * m.SellingPrice,
                            m.Missing == null ? 0 : m.Missing
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult EditSalvageItemBuyer(FormCollection col)
        {
            //if (AccessRightsSalvageSalesOrdersSell == null || AccessRightsSalvageSalesOrdersSell.Update.HasValue == false
            //        || AccessRightsSalvageSalesOrdersSell.Update.Value == false)
            //{
            //    return Json(new { success = false, message = "An error occurred, try logging out and back in.", shipmentId = 0 }, JsonRequestBehavior.AllowGet);
            //}
            bool saved = false;
            SalvageSalesOrderItem salvageItem = null;
            try
            {
                if (col.AllKeys.Contains("oper"))
                {

                    if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                    {
                        //int currentShipmentItemID = Convert.ToInt32(col["id"]);
                        salvageItem = _ssOrderBO.GetSalvageSalesOrderItem(Convert.ToInt64(col["id"]));
                        if (salvageItem != null)
                        {
                            salvageItem.Missing = Convert.ToInt32(col["Missing"]);
                            saved = _ssOrderBO.UpdateSalvageSalesOrderItem(salvageItem);
                            if (saved == false)
                                throw new Exception("Error updating item.");
                        }
                    }


                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", ssItemId = 0 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult Receive(Int32 id, string receivedBy, string receivedDate)
        {
            //if (AccessRightsSalvageSalesOrdersSell == null || AccessRightsSalvageSalesOrdersSell.Update.HasValue == false
            //        || AccessRightsSalvageSalesOrdersSell.Update.Value == false)
            //{
            //    return RedirectToAction("AccessDenied", "Home");
            //}
            SalvageSalesOrder ssOrder = _ssOrderBO.GetSalvageSalesOrder(id);
            try
            {
                ssOrder.ReceivedBy = receivedBy;
                ssOrder.ReceivedDate = Convert.ToDateTime(receivedDate);
                _ssOrderBO.UpdateSalvageSalesOrder(ssOrder);
            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to save Salvage Sales Order." }, JsonRequestBehavior.AllowGet);
            }
            //return RedirectToAction("SellerList");
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}