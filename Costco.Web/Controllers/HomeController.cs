﻿using System.Web.Mvc;
using Costco.Common;

namespace Costco.Web.Controllers
{
    [HandleError]
    public class HomeController : CostcoApplicationController
    {
        public HomeController(ISessionCookieValues sessionValues)
           : base(sessionValues)
        {
        }

        public virtual ActionResult Index()
        {
            int typeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            int roleid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
            if (typeid == 4 && roleid == 10 )
                return RedirectToAction("SellerList", "SalvageSales");
            else if (typeid == 4)
                return RedirectToAction("List", "Shipment");
            else if (typeid == 1)
                return RedirectToAction("Index", "Affiliate");
            else if (typeid == 5 && roleid == 7)
                return RedirectToAction("Index", "SalesOrder");
            else if (typeid == 5)
                return RedirectToAction("List", "Shipment");
            else if (typeid == 2)
                return RedirectToAction("List", "Shipment");
            else if (typeid == 3)
                return RedirectToAction("List", "Shipment");
            else if (typeid == 7)
                return RedirectToAction("IndexBuyer", "SalesOrder");
            else if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId) == -1)
                return RedirectToAction("Login", "Account");
            else
                return View();
        }

        public virtual ActionResult About()
        {
            return View();
        }

        public ActionResult AccessDenied()
        {
            if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId) == -1)
                return RedirectToAction("Login", "Account");
            else 
                return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult FAQ()
        {
            return View();
        }

    }
}
