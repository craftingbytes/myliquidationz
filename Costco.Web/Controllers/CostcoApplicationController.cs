﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Costco.Common;
using Costco.BLL.Authorization;

namespace Costco.Web.Controllers
{
    public class CostcoApplicationController : Controller
    {
        protected ISessionCookieValues _sessionValues = null;
        //
        // GET: /CostcoApplication/
        public CostcoApplicationController(ISessionCookieValues sessionValues)
        {
            _sessionValues = sessionValues;
            ViewData["MenuHTML"] = GetMenusHtml();
            ViewData["AffiliateContactName"] = _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactName);
        }

        public virtual string RootPath
        {
            get
            {
                if (System.Web.HttpContext.Current.Request.Url.IsDefaultPort)
                    return System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Host + "/";
                else
                    return System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Host + ":" + System.Web.HttpContext.Current.Request.Url.Port.ToString() + "/";
            }
        }


        public virtual string AppVirtualPath
        {
            get
            {
                return System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.HttpContext.Current.Request.ApplicationPath;
            }
        }

        private string GetMenusHtml()
        {
            string _menuHtml = "<div id=\"menu\">";
            _menuHtml += "<ul class=\"sf-menu\">";
            List<Menu> _menus = null;
            //_menus = new Menu().GetMenus("Public");

            _menus = Authorization.GetAuthorizationMenu(_sessionValues).MenuItems;
            int counter = 1;
            foreach (Menu _menu in _menus)
            {
                string cssClass = "line";
                if (counter == 1)
                    cssClass = "first line";
                else if (counter == _menus.Count)
                    cssClass = "one";
                else
                    cssClass = "line";

                //if (counter == _menus.Count && (String.IsNullOrEmpty(SessionParameters.AffiliateRoleName) || SessionParameters.AffiliateType == "-1"))
                //cssClass = "currentLast";

                if (_menu.OnClick != null && _menu.OnClick != "")
                    _menuHtml += "<li name='parentMenu' class=\"" + cssClass + "\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href='#' onclick=\"" + _menu.OnClick + "\">" + _menu.Caption + "</a>";

                else if (_menu.URL == null || _menu.URL == "" || _menu.URL == "#")
                    _menuHtml += "<li name='parentMenu' class=\"" + cssClass + "\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=#>" + _menu.Caption + "</a>";

                else if (_menu.Name == "LogIn")
                    _menuHtml += "<li name='parentMenu' class=\"" + cssClass + "\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=\"" + RootPath + _menu.URL + "\" target=\"_blank\">" + _menu.Caption + "</a>";
                else
                    _menuHtml += "<li name='parentMenu' class=\"" + cssClass + "\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=\"" + RootPath + _menu.URL + "\">" + _menu.Caption + "</a>";


                _menuHtml += GetChildMenusHtml(_menu);
                if (_menu.MenuItems.Count == 0)
                    _menuHtml += "</div></li>";
                else
                    _menuHtml += "</div><div style='float:left;padding:11px 5px;'><img src='" + AppVirtualPath + "Content/Images/Menus/arrow.png'></div></li>";
                counter++;
            }
            return _menuHtml += "</ul></div>";
        }

        private string GetChildMenusHtml(Menu _parent)
        {
            string _menuHTML = "";
            if (_parent.MenuItems.Count > 0)
                _menuHTML += "<ul>";
            foreach (Menu _menu in _parent.MenuItems)
            {
                if (_menu.OnClick != null && _menu.OnClick != "")
                    _menuHTML += "<li name='parentMenu' class=\"current\"><div style='float:left;padding:10px 0px 0px 0px;display:none;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href='#' onclick=\"" + _menu.OnClick + "\">" + _menu.Caption + "</a>";

                else if (_menu.URL == null || _menu.URL == "" || _menu.URL == "#")
                    _menuHTML += "<li class=\"current\"><div style='float:left;padding:10px 0px 0px 0px;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=#>" + _menu.Caption + "</a>";
                else
                    _menuHTML += "<li class=\"current\"><div style='float:left;padding:10px 0px 0px 0px;'><img src='" + AppVirtualPath + "Content/Images/Menus/bullet.png'></div><div style='float:left'><a href=\"" + RootPath + _menu.URL + "\">" + _menu.Caption + "</a>";
                _menuHTML += GetChildMenusHtml(_menu);
                if (_menu.MenuItems.Count == 0)
                    _menuHTML += "</div></li>";
                else
                    _menuHTML += "</div><div style='float:left;padding:13px 5px;'><img src='" + AppVirtualPath + "Content/Images/Menus/arrow.png'></div></li>";
            }
            if (_parent.MenuItems.Count > 0)
                _menuHTML += "</ul>";
            return _menuHTML;
        }

        public ActionResult GetReportsRights(string reportName)
        {
            // string reportName = "";
            AccessRights _accessRights = null;
            _accessRights = Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), reportName);
            return Json((_accessRights != null && _accessRights.Read.HasValue && _accessRights.Read.Value));
        }

    }
}
