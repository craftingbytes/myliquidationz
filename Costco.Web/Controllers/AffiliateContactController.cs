﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL.BusinessObjects;
using Costco.BLL.ViewModels;
using Costco.DAL.EntityModels;
using Costco.Common;
using Costco.BLL.Authorization;

namespace Costco.Web.Controllers
{
    public class AffiliateContactController : CostcoApplicationController
    {
        private AffiliateContactBO _affiliateContactBO;

        private AccessRights AccessRightsAffiliateContact
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "Contact");
            }
        }
        public AffiliateContactController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            ViewData["AccessRights"] = AccessRightsAffiliateContact;
            _affiliateContactBO = new AffiliateContactBO(_sessionValues);
        }

        public ActionResult Index(int? id)
        {
            int affiliateId = -1;
            if (id.HasValue)
                affiliateId = (int)id;
            if (AccessRightsAffiliateContact != null && AccessRightsAffiliateContact.Read.HasValue == true && AccessRightsAffiliateContact.Read == true)
            {
                AffiliateBO affiliateBO = new AffiliateBO();
                if (affiliateId == -1)
                    affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId);
                else
                    _sessionValues.SetSessionValue<int>(Constants.SessionParameters.SelectedAffiliateId, affiliateId);

                Affiliate affiliate = affiliateBO.GetAffiliate(affiliateId);
                if (affiliate == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                AffiliateContact _affContact = new AffiliateContact();
                _affContact.Affiliate = affiliate;
                ViewData["Name"] = affiliate.Name;

                ViewData["Data"] = Request["data"];
                return View(new AffiliateContactViewModel(_affContact));
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetAffiliateContacts(string sidx, string sord, int page, int rows)
        {
            int affiliateId = 0;
            int selectedid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId);
            if (selectedid != -1)
                affiliateId = selectedid;

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = _affiliateContactBO.GetAffiliateContactsPaged(pageIndex, pageSize, out totalRecords, affiliateId).AsEnumerable<AffiliateContact>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                           m.Id,
                            m.FirstName,
                            m.LastName,
                            m.Phone,                            
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }

        public ActionResult Clear()
        {
            return RedirectToAction("Index", new { id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId), data = Request["data"] });
        }


        [HttpPost]
        public ActionResult Index(AffiliateContact affiliateContact)
        {
            try
            {
                if (AccessRightsAffiliateContact != null && AccessRightsAffiliateContact.Add.HasValue == true && AccessRightsAffiliateContact.Add == true)
                {
                    AffiliateBO affiliateBO = new AffiliateBO();
                    int affiliateId = 0;
                    int selectedid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId);
                    if (selectedid != -1)
                        affiliateId = selectedid;

                    Affiliate affiliate = affiliateBO.GetAffiliate(affiliateId);
                    affiliateContact.Affiliate = affiliate;
                    AffiliateContactViewModel model = new AffiliateContactViewModel(affiliateContact);
                    string services = Request["selectedObjects"];
                    List<Message> messages = _affiliateContactBO.Add(affiliateContact);
                    ViewData["Name"] = affiliate.Name;


                    AffiliateContact _affContact = new AffiliateContact();
                    _affContact = _affiliateContactBO.GetAffiliateContact(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId));
                    AffiliateContactViewModel updatedModel = new AffiliateContactViewModel(_affContact);
                    updatedModel.Messages = messages;
                    ViewData["Data"] = Request["data"];
                    return View("Index", updatedModel);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Home");
                }

            }
            catch (Exception)
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            AffiliateContact _affContact = _affiliateContactBO.GetAffiliateContact(id);
            if (_affContact == null)
            {
                return RedirectToAction("Error", "Home");
            }
            AffiliateBO _affiliateBO = new AffiliateBO();
            Affiliate _affiliate = _affiliateBO.GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId));
            ViewData["Data"] = Request["data"];
            return View("Index", new AffiliateContactViewModel(_affContact));

        }


        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            AffiliateContact affiliateContactModel = _affiliateContactBO.GetAffiliateContact(id);
            try
            {
                bool passwordIsChanged = true;
                AffiliateContactViewModel model = new AffiliateContactViewModel(affiliateContactModel);
                string OldPassword;
                OldPassword = affiliateContactModel.Password;
                TryUpdateModel(affiliateContactModel);
                if (affiliateContactModel.Password.Equals(OldPassword))
                {
                    passwordIsChanged = false;
                }

                string services = Request["selectedObjects"];
                List<Message> messages = _affiliateContactBO.Update(affiliateContactModel, services, passwordIsChanged);

                AffiliateContactViewModel updatedModel = new AffiliateContactViewModel(affiliateContactModel);
                updatedModel.Messages = messages;
                AffiliateBO _affiliateBO = new AffiliateBO();
                Affiliate _affiliate = _affiliateBO.GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.SelectedAffiliateId));

                ViewData["Data"] = Request["data"];
                return View("Index", updatedModel);
            }
            catch (Exception)
            {
                return RedirectToAction("Edit", new { id = affiliateContactModel.Id, success = false, data = Request["data"] });
            }
        }

    }
}
