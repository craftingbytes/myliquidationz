﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Costco.DAL.EntityModels;
using Costco.BLL.BusinessObjects;
using Costco.Common;
using Costco.BLL.Authorization;
using Costco.BLL.ViewModels;
using Costco.BLL;

namespace Costco.Web.Controllers
{
    public class AffiliateController : CostcoApplicationController
    {
        private AffiliateBO _affiliateBO;

        private AccessRights AccessRightsAffiliateSearch
        {
            get
            {
                //Authorization.Authorization authorization = new Authorization.Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "AffiliateSearch");
            }
        }

        private AccessRights AccessRightsAffiliate
        {
            get
            {
                //Authorization.Authorization authorization = new Authorization.Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "Affiliate");
            }
        }

        public AffiliateController(ISessionCookieValues sessionValues)
            :base(sessionValues)
        {
            ViewData["AccessRights"] = Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "AffiliateSearch");
                _affiliateBO = new AffiliateBO();
        }

        public ActionResult Index()
        {
            if (AccessRightsAffiliateSearch.Read == true)
            {
                var affiliateTypeList = new CostcoService<AffiliateType>().GetAll().OrderBy(f => f.Type).ToList();
                affiliateTypeList.Insert(0, new AffiliateType() {Id=0, Type="Select"});
                ViewData["Data"] = "{" + Request["data"] + "}";
                ViewData["AffiliateTypes"] = new SelectList(affiliateTypeList, "Id", "Type");
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetAffiliates(int page, int rows, string searchString, string searchEntity)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            int entityType = Convert.ToInt32(searchEntity);
            var data = _affiliateBO.GetAffiliatesPaged(pageIndex, pageSize, searchString, entityType, out totalRecords).AsEnumerable<Affiliate>();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data

                    select new
                    {
                        id = m.Id,
                        cell = new object[] { 
                           m.Id,
                            m.Name,
                            m.AffiliateType!=null ?m.AffiliateType.Type:"",
                            m.Address1,
                            m.State!=null ?m.State.Name:"",
                            m.Phone,
                            m.City!=null ?m.City.Name:""
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int? saved)
        {
            if (AccessRightsAffiliate != null && AccessRightsAffiliate.Add == true)
            {

                Affiliate _aff = new Affiliate();
                if (saved.HasValue)
                {
                    ViewData["saved"] = "true";
                }
                //var affiliateTypeList = new CostcoService<AffiliateType>().GetAll().OrderBy(f => f.Type).ToList();
                //affiliateTypeList.Insert(0, new AffiliateType() { Id = 0, Type = "Select" });
                //ViewData["AffiliateTypes"] = new SelectList(affiliateTypeList, "Id", "Type");

                //var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
                //stateList.Insert(0, new State() { Id = 0, Name = "Select" });
                //ViewData["States"] = new SelectList(stateList, "Id", "Name");

                //List<City> cityList = new List<City>();
                //cityList.Insert(0, new City() { Id = 0 , Name = "Select"});
                //ViewData["Cities"] = new SelectList(cityList, "Id", "Name");


                return View(new AffiliateViewModel(_aff));
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpPost]
        public ActionResult Create(Affiliate affiliate)
        {
            AffiliateViewModel model = new AffiliateViewModel(affiliate);    
            List<Message> messages = _affiliateBO.Add(affiliate);
            model.Messages = messages;
            return RedirectToAction("Index");
        }

        public ActionResult GetCities(string state)
        {
            int stateId = int.Parse(state);
            IList<City> data;
            var data1 = new CostcoService<City>().GetAll(x => x.StateId == stateId);
            IEnumerable<City> enumList = data1.OrderBy(f => f.Name);
            try
            {
                data = enumList.ToList();
            }
            catch (Exception)
            {
                data = data1;
            }

            City _city = new City();
            _city.Id = 0;
            _city.Name = "Select";
            data.Insert(0, _city);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name

                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetAffiliateParents(string affiliateType)
        {
            int affiliateTypeId = int.Parse(affiliateType);
            IList<Affiliate> data;

            if (affiliateTypeId == 4) //costcoWarehouse
            {
                var data1 = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 3); //costco distribution
                IEnumerable<Affiliate> enumList = data1.OrderBy(f => f.Name);
                try
                {
                    data = enumList.ToList();
                }
                catch (Exception)
                {
                    data = data1;
                }
            }
            else if (affiliateTypeId == 7) //buyer
            {
                var data1 = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 5); //costco distribution
                IEnumerable<Affiliate> enumList = data1.OrderBy(f => f.Name);
                try
                {
                    data = enumList.ToList();
                }
                catch (Exception)
                {
                    data = data1;
                }
            }
            else
            {
                data = new List<Affiliate>();
            }

            Affiliate _affiliate = new Affiliate();
            _affiliate.Id = 0;
            _affiliate.Name = "Select";
            data.Insert(0, _affiliate);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name

                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetRecyclers(string affiliateType)
        {
            int affiliateTypeId = int.Parse(affiliateType);
            IList<Affiliate> data;

            if (affiliateTypeId == 4 || affiliateTypeId == 3) //costcoWarehouse && distribution
            {
                var data1 = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 5); //costco recyclers
                IEnumerable<Affiliate> enumList = data1.OrderBy(f => f.Name);
                try
                {
                    data = enumList.ToList();
                }
                catch (Exception)
                {
                    data = data1;
                }
            }
            else
            {
                data = new List<Affiliate>();
            }

            Affiliate _affiliate = new Affiliate();
            _affiliate.Id = 0;
            _affiliate.Name = "Select";
            data.Insert(0, _affiliate);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name

                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult GetCarriers(string affiliateType)
        {
            int affiliateTypeId = int.Parse(affiliateType);
            IList<Affiliate> data;

            if (affiliateTypeId == 4 || affiliateTypeId == 3) //costcoWarehouse && distribution
            {
                var data1 = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 6); //costco recyclers
                IEnumerable<Affiliate> enumList = data1.OrderBy(f => f.Name);
                try
                {
                    data = enumList.ToList();
                }
                catch (Exception)
                {
                    data = data1;
                }
            }
            else
            {
                data = new List<Affiliate>();
            }

            Affiliate _affiliate = new Affiliate();
            _affiliate.Id = 0;
            _affiliate.Name = "Select";
            data.Insert(0, _affiliate);

            var jsonData = new
            {
                data = (
                    from m in data
                    select new
                    {
                        Value = m.Id,
                        Text = m.Name

                    }).ToArray()
            };
            return Json(jsonData);
        }

        public ActionResult Edit(int id)
        {
            Affiliate affiliate = _affiliateBO.GetAffiliate(id);
            if (affiliate == null)
            {
                return RedirectToAction("Error", "Home");
            }
            AffiliateViewModel model = new AffiliateViewModel(affiliate);
            ViewData["Name"] = affiliate.Name;
            ViewData["Data"] = Request["data"];
            //var affiliateTypeList = new CostcoService<AffiliateType>().GetAll().OrderBy(f => f.Type).ToList();
            //affiliateTypeList.Insert(0, new AffiliateType() { Id = 0, Type = "Select" });
            //ViewData["AffiliateTypes"] = new SelectList(affiliateTypeList, "Id", "Type", affiliate.AffiliateTypeId);

            //var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            //stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            //ViewData["States"] = new SelectList(stateList, "Id","Name", affiliate.StateId);

            //List<City> cityList = new List<City>();
            //cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            //ViewData["Cities"] = new SelectList(cityList, "Id", "Name");
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection form)
        {
            try
            {
                Affiliate affiliateModel = _affiliateBO.GetAffiliate(id);
                AffiliateViewModel model = new AffiliateViewModel(affiliateModel);
                TryUpdateModel(affiliateModel);
                string services = Request["selectedObjects"];
                int roleId = Convert.ToInt32(Request["affiliateRoleId"]);
                List<Message> messages = _affiliateBO.Update(affiliateModel);


                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Edit", id);
            }
        }

        [HttpGet]
        public ActionResult Map(int? id)
        {

                //MapXMLHandler.CreateMapXMLFile(SessionParameters.SelectedAffiliateId, new MITSService<AffiliateTarget>().GetAll(X => X.AffiliateId == SessionParameters.SelectedAffiliateId).ToList());
                //ViewData["MapOwnerName"] = SelectedAffiliate.Name;
                //ViewData["Data"] = Request["data"];
                return View();

        }

        public DownloadResult Download(string fileName)
        {
            //string fileName = Convert.ToString(Request.QueryString["FileName"]);

            if (fileName != "-")
            {
                fileName = fileName.Replace("<~>", "#");
                string virtualPath = "~/Uploads/" + fileName.Replace(" ", " ");
                string serverPath = ControllerContext.HttpContext.Server.MapPath(virtualPath);
                return new DownloadResult { VirtualPath = virtualPath, FileDownloadName = fileName };
            }
            else
            {
                return null;
            }
        }
    }
}
