﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL;
using Costco.BLL.BusinessObjects;
using Costco.BLL.ViewModels;
using Costco.DAL.EntityModels;
using Costco.Common;
using Costco.BLL.Authorization;
using System.Web.Script.Serialization;

namespace Costco.Web.Controllers
{
    public class BulkShipmentController : CostcoApplicationController
    {
        private BulkShipmentBO _bulkShipmentBO;
        private AccessRights AccessRightsBulkShipmentCreate
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "BulkShipmentCreate");
            }
        }

        private AccessRights AccessRightsBulkShipmentList
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "BulkShipmentList");
            }
        }

        public BulkShipmentController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            ViewData["AccessRights"] = AccessRightsBulkShipmentCreate;
            _bulkShipmentBO = new BulkShipmentBO(_sessionValues);
        }

        public ActionResult Index()
        {
            if (AccessRightsBulkShipmentCreate == null || AccessRightsBulkShipmentCreate.Add.HasValue == false
                    || AccessRightsBulkShipmentCreate.Add.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            Affiliate affiliateData = _bulkShipmentBO.GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));
            BulkShipment _bulkShipment = new BulkShipment();
            BulkShipmentViewModel model = new BulkShipmentViewModel(_bulkShipment);
            model.ShipToAffiliate = affiliateData;

            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            ViewData["PickupState"] = new SelectList(stateList, "Id", "Name");

            List<City> cityList = new List<City>();
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            ViewData["PickupCity"] = new SelectList(cityList, "Id", "Name");


            _bulkShipment.ShipToAffiliateId = affiliateData.Id;
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewData["CurrentBulkShipmentID"] = id;


            BulkShipment _bulkShipment = _bulkShipmentBO.GetBulkShipment(id);
            Affiliate affiliateData = _bulkShipmentBO.GetAffiliate(_bulkShipment.ShipToAffiliateId);

            if (_bulkShipment == null)
            {
                return RedirectToAction("Error", "Home");
            }
            BulkShipmentViewModel model = new BulkShipmentViewModel(_bulkShipment);
            model.ShipToAffiliate = affiliateData;

            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            ViewData["PickupState"] = new SelectList(stateList, "Id", "Name", _bulkShipment.PickupStateId);

            List<City> cityList = new List<City>();
            if (_bulkShipment.PickupStateId > 0)
            {
                var data1 = new CostcoService<City>().GetAll(x => x.StateId == _bulkShipment.PickupStateId);
                cityList = data1.OrderBy(f => f.Name).ToList();
            }
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            ViewData["PickupCity"] = new SelectList(cityList, "Id", "Name", _bulkShipment.PickupCityId);
            return View(model);
        }

        [HttpGet]
        public ActionResult InitItemGrid()
        {
            //List<LineItem> items = null;
            return Content("{rows:[]}");
        }

        [HttpGet]
        public ActionResult GetBulkShipmentItems(string sidx, string sord, int page, int rows, int bulkShipmentId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _bulkShipmentBO.GetBulkShipmentItems(pageIndex, pageSize, bulkShipmentId, out totalRecords).AsEnumerable<BulkShipmentItem>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.Store, 
                            m.ItemId,
                            m.Description,
                            m.ConditionType.Condition,
                            m.ReceivedCondition2,
                            m.Qty,
                            m.CoveredDevice,
                            m.Disposition,
                            m.BulkShipmentId,
                            m.Pulled
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private List<BulkShipmentItem> FillShipmentItems(FormCollection form)
        {
            List<BulkShipmentItem> shipmentItems = new List<BulkShipmentItem>();
            BulkShipmentItem item = null;
            JavaScriptSerializer JSS = new JavaScriptSerializer();

            string key = string.Empty;
            string value = string.Empty;

            for (int i = 1; i <= form.AllKeys.Length; i++)
            {
                key = "Item_" + i.ToString();
                if (form[key] != null)
                {
                    value = form[key];
                    string[] jsonCollection = value.Split(new char[] { ',' });
                    item = new BulkShipmentItem();

                    foreach (var jsonItem in jsonCollection)
                    {
                        string[] clientitem = jsonItem.Split(new char[] { ':' });
                        switch (clientitem[0])
                        {
                            case "BulkShipmentItemID":
                                item.Id = Convert.ToInt32(clientitem[1]);
                                break;
                            case "StoreNumber":
                                item.Store = Convert.ToInt32(clientitem[1]);
                                break;
                            case "ItemId":
                                item.ItemId = clientitem[1];
                                break;
                            case "Description":
                                item.Description = clientitem[1];
                                break;
                            case "Qty":
                                item.Qty = Convert.ToInt32(clientitem[1]);
                                break;
                            case "Condition":
                                ConditionType condition = _bulkShipmentBO.GetCondtionTypeByCondition(clientitem[1]);
                                if (condition != null)
                                    item.ReceivedCondition = condition.Id;
                                else
                                    item.ReceivedCondition =  99; // default to other
                                break;
                            case "ReceivedCondition2":
                                item.ReceivedCondition2 = clientitem[1];
                                break;
                            case "CoveredDevice":
                                item.CoveredDevice = Convert.ToBoolean(clientitem[1]);
                                break;
                            case "Disposition":
                                item.Disposition = clientitem[1];
                                break;
                        }
                    }

                    shipmentItems.Add(item);

                }
            }
            return shipmentItems;
        }

        [HttpGet]
        public ActionResult List()
        {

            if (AccessRightsBulkShipmentList != null && AccessRightsBulkShipmentList.Read.HasValue
                    && AccessRightsBulkShipmentList.Read.Value)
            {
                ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetShippingDataList(string sidx, string sord, int page, int rows, bool _search, string BeginDate, string EndDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _bulkShipmentBO.GetPaged(pageIndex, pageSize, BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), out totalRecords).AsEnumerable<BulkShipment>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            ((m.City==null)?"":m.City.Name) + ", " + ((m.State==null)?"":m.State.Abbreviation),
                            m.Affiliate.Name,
                            String.Format("{0:MM/dd/yyyy}",m.PickupDate),
                            String.Format("{0:MM/dd/yyyy}",m.ReceivedDate),
                            m.Id,
                            GetDocsListHtml(m.Id),
                            GetDialogueHtml(m.Id),
                            m.Id
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            if (AccessRightsBulkShipmentCreate == null || AccessRightsBulkShipmentCreate.Add.HasValue == false
                    || AccessRightsBulkShipmentCreate.Add.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            Int32 bulkShipmentId = 0;
            
            try
            {
                BulkShipment shipment = new BulkShipment();
                shipment.ReceivedDate = Convert.ToDateTime(form["txtReceivedDate"]);
                shipment.ShipToAffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

                shipment.ReceivingComments = form["txtReceivedComments"];
                shipment.ReceivedBy = form["txtReceivedBy"];
                shipment.ShipmentTotalWeight = Convert.ToInt32(form["txtTotalWeight"]);
                shipment.PickupCompanyName = form["txtPickupCompanyName"];
                shipment.PickupAddress = form["txtPickupAddress"];
                shipment.PickupStateId = Convert.ToInt32(form["ddlPickupState"]);
                shipment.PickupCityId = Convert.ToInt32(form["ddlPickupCity"]);
                shipment.PickupZip = form["txtPickupZip"];
                shipment.PickupDate = Convert.ToDateTime(form["txtShippededDate"]);

                //List<BulkShipmentItem> shipmentItems = FillShipmentItems(form);

                bulkShipmentId = _bulkShipmentBO.Save(shipment);

                if (bulkShipmentId == 0)
                    throw new Exception();
            }
            catch (Exception)
            {

                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction("Edit", new { id = bulkShipmentId });
        }

        [HttpPost]
        public ActionResult Edit(FormCollection form)
        {
            if (AccessRightsBulkShipmentList == null || AccessRightsBulkShipmentList.Update.HasValue == false
                    || AccessRightsBulkShipmentList.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            try
            {
                BulkShipment shipment = _bulkShipmentBO.GetBulkShipment(Convert.ToInt32(form["BulkShipmentId"]));
                shipment.ReceivedDate = Convert.ToDateTime(form["txtReceivedDate"]);
                shipment.ShipToAffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

                shipment.ReceivingComments = form["txtReceivedComments"];
                shipment.ReceivedBy = form["txtReceivedBy"];
                shipment.ShipmentTotalWeight = Convert.ToInt32(form["txtTotalWeight"]);
                shipment.PickupCompanyName = form["txtPickupCompanyName"];
                shipment.PickupAddress = form["txtPickupAddress"];
                shipment.PickupStateId = Convert.ToInt32(form["ddlPickupState"]);
                shipment.PickupCityId = Convert.ToInt32(form["ddlPickupCity"]);
                shipment.PickupZip = form["txtPickupZip"];
                shipment.PickupDate = Convert.ToDateTime(form["txtShippededDate"]);
                int payableWeight = 0;
                if (Int32.TryParse(form["txtPayableWeight"], out payableWeight))
                    shipment.PayableWeight = payableWeight;


                bool saved = _bulkShipmentBO.Update(shipment);
                if (!saved)
                    throw new Exception();

                //List<BulkShipmentItem> shipmentItems = FillShipmentItems(form);
                //saved = _bulkShipmentBO.UpdateBulkShipmentItems(shipment, shipmentItems);
                //if (!saved)
                //    throw new Exception();

            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("List");
        }

        private string GetDocsListHtml(int pDataId)
        {
            List<Document> _docs = _bulkShipmentBO.GetDocsByPDataId(pDataId);

            string _markup = string.Empty;

            if (_docs.Count > 0)
            {
                _markup = "<div onclick='GetAttachments(" + pDataId + ")'><img src='../Content/images/icon/sm/paperclip.gif' title='View attachments'></img></div>";
            }

            return _markup;
        }

        private string GetDialogueHtml(int pDataId)
        {

            return "<div onclick='javascript:openFileDialog(" + pDataId + ")'><img src='../Content/images/icon/sm/attach_file.gif' title='Attach File' /></div>";
        }

        public ActionResult SaveAttachment(string fileName, string processDataId)
        {
            try
            {
                string path = Server.MapPath("~/Uploads/" + fileName);
                String saveLocation = Server.MapPath("~/Uploads/") + Guid.NewGuid();
                Document doc = new Document();

                doc.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                doc.ServerFileName = saveLocation;
                doc.UploadFileName = fileName;
                doc.UploadDate = System.DateTime.Now;

                if (!string.IsNullOrEmpty(processDataId))
                {
                    DocumentRelation docRel = new DocumentRelation();
                    docRel.DocumentID = _bulkShipmentBO.SaveAttachment(doc);
                    docRel.ForeignID = Convert.ToInt32(processDataId);
                    _bulkShipmentBO.SaveAttachmentRelation(docRel);
                }

                return Json("True", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
            //return null;
        }

        public ActionResult GetAttachments(string processDataId)
        {
            List<Document> _docs = _bulkShipmentBO.GetDocsByPDataId(Convert.ToInt32(processDataId));

            var jsonData = new
            {
                rows = (
                    from d in _docs
                    select new
                    {
                        DocumentID = d.Id,
                        //UploadFileName = d.UploadFileName.ToString()
                        UploadFileName = d.UploadFileName.Replace(" ", "&nbsp;")
                    }).ToList()

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditBulkShipmentItem(FormCollection col)
        {           
            if (AccessRightsBulkShipmentList == null || AccessRightsBulkShipmentList.Update.HasValue == false
                    || AccessRightsBulkShipmentList.Update.Value == false)
            {
                return Json(new { success = false, message="An error occurred, try logging out and back in.", bulkshipmentId=0 }, JsonRequestBehavior.AllowGet);
            }
            Int32 bulkshipmentId = 0;
            BulkShipmentItem item = null;
            try
            {
                if (col.AllKeys.Contains("oper"))
                {
                    if (col["oper"] == "add")
                    {
                        item = new BulkShipmentItem();

                        item.BulkShipmentId = Convert.ToInt32(col["ItemsBulkShipmentId"]);
                        item.Store = Convert.ToInt32(col["StoreNumber"]);
                        item.ItemId = col["ItemId"];
                        item.Description = col["Description"];
                        ConditionType condition = _bulkShipmentBO.GetCondtionTypeByCondition(col["Condition"]);
                        if (condition != null)
                            item.ReceivedCondition = condition.Id;
                        else
                            item.ReceivedCondition = 99; // default to other
                        item.ReceivedCondition2 = col["ReceivedCondition2"];
                        //item.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
                        item.Qty = Convert.ToInt32(col["Qty"]);
                        //item.Disposition = col["Disposition"];
                        item.Disposition = GetDisposition(col["ItemId"], col["Disposition"]);
                        item.Pulled = Convert.ToInt32(col["Pulled"]);
                        if (item.Pulled > item.Qty || item.Pulled < 0)
                            return Json(new { success = false, message = "Pulled Qty cannot be more than Received Qty.", bulkshipmentId = bulkshipmentId }, JsonRequestBehavior.AllowGet);
                        bulkshipmentId =_bulkShipmentBO.AddBulkShipmentItem(item);
                    }
                    else if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                    {
                        item = _bulkShipmentBO.GetBulkShipmentItemByID(Convert.ToInt32(col["id"]));
                        item.Store = Convert.ToInt32(col["StoreNumber"]);
                        item.ItemId = col["ItemId"];
                        item.Description = col["Description"];
                        ConditionType condition = _bulkShipmentBO.GetCondtionTypeByCondition(col["Condition"]);
                        if (condition != null)
                            item.ReceivedCondition = condition.Id;
                        else
                            item.ReceivedCondition = 99; // default to other
                        item.ReceivedCondition2 = col["ReceivedCondition2"];
                        //item.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
                        item.Qty = Convert.ToInt32(col["Qty"]);
                        //item.Disposition = col["Disposition"];
                        item.Disposition = GetDisposition(col["ItemId"], col["Disposition"]);
                        item.Pulled = Convert.ToInt32(col["Pulled"]);
                        if (item.Pulled > item.Qty || item.Pulled < 0)
                            return Json(new { success = false, message = "Pulled Qty cannot be more than Received Qty.", bulkshipmentId = bulkshipmentId }, JsonRequestBehavior.AllowGet);
                        _bulkShipmentBO.UpdateBulkShipmentItem(item);
                    }
                    Product product = new Product();
                    product.ItemId = col["ItemId"];
                    product.Description = col["Description"];
                    //product.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
                    _bulkShipmentBO.AddProduct(product);
                }
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", bulkshipmentId = 0 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, message = "An error occurred, try logging out and back in.", bulkshipmentId = bulkshipmentId, disposition = item.Disposition }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteBulkShipmentItem(int bulkShipmentItemId)
        {
            try
            {
                _bulkShipmentBO.DeleteBulkShipmentItem(bulkShipmentItemId);
                return Json(new { status = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }

        }

        private string GetDisposition(string itemid, string disposition)
        {
            string productDisposition = disposition;
            ProductDisposition pd = new CostcoService<ProductDisposition>().GetSingle(x => x.ItemId == itemid);
            if (pd != null)
                productDisposition = pd.Disposition;

            return productDisposition;
        }
    }
}
