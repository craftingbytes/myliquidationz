﻿using System;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL.BusinessObjects;
using Costco.DAL.EntityModels;
using Costco.Common;
using Costco.BLL.Authorization;

namespace Costco.Web.Controllers
{
    public class PullShipmentItemController : CostcoApplicationController
    {
        //
        // GET: /PullShipmentItem/
        private ShipmentBO _shipmentBO;
        private BulkShipmentBO _bulkShipmentBO;

        private Int32 CurrentSalesOrderID
        {
            set
            {
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.CurrentSalesOrderID, value);
            }
            get
            {
                return _sessionValues.GetSessionIntValue(Constants.SessionParameters.CurrentSalesOrderID);
            }
        }

        private Int32 CurrentShipmentID
        {
            set
            {
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.CurrentShipmentID, value);
            }
            get
            {
                return _sessionValues.GetSessionIntValue(Constants.SessionParameters.CurrentShipmentID);
            }
        }

        private AccessRights AccessRightsPullShipmentItem
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "PullShipmentItem");
            }
        }
        public PullShipmentItemController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            _shipmentBO = new ShipmentBO(_sessionValues);
            _bulkShipmentBO = new BulkShipmentBO(_sessionValues);
        }

        public ActionResult Index(Int32 salesOrderId=0, Int32 shipmentId=0)
        {

            if (AccessRightsPullShipmentItem == null || AccessRightsPullShipmentItem.Read.HasValue == false
                    || AccessRightsPullShipmentItem.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CurrentSalesOrderID = salesOrderId;
            CurrentShipmentID = shipmentId;

            ViewData["CurrentSalesOrderID"] = CurrentSalesOrderID;
            ViewData["CurrentShipmentID"] = CurrentShipmentID;
            ViewData["Data"] = Request["data"];
            return View();
        }


        public ActionResult BulkIndex(Int32 salesOrderId = 0, Int32 shipmentId = 0)
        {

            if (AccessRightsPullShipmentItem == null || AccessRightsPullShipmentItem.Read.HasValue == false
                    || AccessRightsPullShipmentItem.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CurrentSalesOrderID = salesOrderId;
            CurrentShipmentID = shipmentId;

            ViewData["CurrentSalesOrderID"] = CurrentSalesOrderID;
            ViewData["CurrentShipmentID"] = CurrentShipmentID;
            ViewData["Data"] = Request["data"];
            return View();
        }


        [HttpGet]
        public ActionResult GetShipmentItemsToPull(string sidx, string sord, int page, int rows, Int32 shipmentId=0)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            Int32 _shipmetId;
            if (CurrentShipmentID == 0)
                _shipmetId = shipmentId;
            else
                _shipmetId = CurrentShipmentID;

            Shipment shipment = _shipmentBO.GetShipment(_shipmetId);

            int affiliateTypeId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

            if (shipment == null)
                return Content("{rows:[]}");

            if (affiliateTypeId == 5 && affiliateId != shipment.ShipToAffiliateId )
                return Content("{rows:[]}");

            CostcoEntities db = new CostcoEntities();
            IQueryable<V_ShipmentInventory> inventory = db.V_ShipmentInventory.Where(x => x.ShipmentId == _shipmetId);
            inventory.OrderBy(e => e.ShipmentItemId).Skip(pageIndex * pageSize).Take(pageSize);

            totalRecords = inventory.Count<V_ShipmentInventory>();
            var data = inventory.AsEnumerable<V_ShipmentInventory>();



            //var data = _shipmentBO.GetShipmentItems(pageIndex, pageSize, _shipmetId, out totalRecords).AsEnumerable<ShipmentItem>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.ShipmentItemId,
                        cell = new object[] {
                            m.ShipmentId,
                            m.ShipmentItemId,
                            m.ItemId,
                            m.Description,
                            //m.Disposition,
                            m.ShipmentItem.Disposition,
                            //m.ReceivedQuantity - m.Pulled ,
                            //m.ConditionType !=null ? m.ConditionType.Condition : "",
                            m.ShipmentItem.ConditionType != null ? m.ShipmentItem.ConditionType.Condition : "",
                            //m.ReceivedCondition2,
                            m.ShipmentItem.ReceivedCondition2,
                            //m.CoveredDevice,
                            m.ShipmentItem.CoveredDevice,
                            m.Quantity,
                            m.Available,
                            m.Pulled
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult GetBulkShipmentItemsToPull(string sidx, string sord, int page, int rows, Int32 shipmentId = 0)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            Int32 _shipmetId;
            if (CurrentShipmentID == 0)
                _shipmetId = shipmentId;
            else
                _shipmetId = CurrentShipmentID;

            BulkShipment shipment = _bulkShipmentBO.GetBulkShipment(_shipmetId);

            int affiliateTypeId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

            if (shipment == null)
                return Content("{rows:[]}");

            if (affiliateTypeId == 5 && affiliateId != shipment.ShipToAffiliateId)
                return Content("{rows:[]}");

            CostcoEntities db = new CostcoEntities();
            IQueryable<V_BulkShipmentInventory> inventory = db.V_BulkShipmentInventory.Where(x => x.ShipmentId == _shipmetId);
            inventory.OrderBy(e => e.ShipmentItemId).Skip(pageIndex * pageSize).Take(pageSize);

            totalRecords = inventory.Count<V_BulkShipmentInventory>();
            var data = inventory.AsEnumerable<V_BulkShipmentInventory>();



            //var data = _shipmentBO.GetShipmentItems(pageIndex, pageSize, _shipmetId, out totalRecords).AsEnumerable<ShipmentItem>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.ShipmentItemId,
                        cell = new object[] {
                            m.ShipmentId,
                            m.ShipmentItemId,
                            m.ItemId,
                            m.Description,
                            m.ShipmentItem.Disposition,
                            m.ShipmentItem.ConditionType != null ? m.ShipmentItem.ConditionType.Condition : "",
                            m.ShipmentItem.ReceivedCondition2,
                            m.ShipmentItem.CoveredDevice,
                            m.Quantity,
                            m.Available,
                            m.Pulled
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult EditShipmentItem(FormCollection col)
        {
            if (AccessRightsPullShipmentItem == null || AccessRightsPullShipmentItem.Update.HasValue == false
                    || AccessRightsPullShipmentItem.Update.Value == false)
            {
                return Json(new { status = false, message="You do not have privileges to pull items." }, JsonRequestBehavior.AllowGet);
            }

            bool saved = false;
            if (col.AllKeys.Contains("oper"))
            {
                if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    int currentShipmentItemID = Convert.ToInt32(col["ShipmentItemID"]);
                    //int debug1 = Convert.ToInt32(col["QtyReceived"]);
                    //int debug2 = Convert.ToInt32(col["ConditionReceived"]);
                    ShipmentItem shipmentItem = _shipmentBO.GetShipmentItem(currentShipmentItemID);

                    CostcoEntities db = new CostcoEntities();
                    V_ShipmentInventory inventoryItem = db.V_ShipmentInventory.FirstOrDefault(x => x.ShipmentItemId == shipmentItem.Id);

                    if (shipmentItem != null)
                    {
                        int quantityOnHand = (int)inventoryItem.Quantity;
                        int pull = Convert.ToInt32(col["Pulled"]);


                        if (pull < 0)
                            return Json(new { status = false, message = "Total Pull amount cannot be less than zero." }, JsonRequestBehavior.AllowGet);

                        if (quantityOnHand - pull >= 0  )
                            shipmentItem.Pulled = pull;
                        else
                            return Json(new { status = false, message="Pull amount greater than quantity on hand." }, JsonRequestBehavior.AllowGet);
                        saved = _shipmentBO.UpdateItem(shipmentItem);
                    }
                }
            }
            return Json(new { status = saved }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult EditBulkShipmentItem(FormCollection col)
        {
            if (AccessRightsPullShipmentItem == null || AccessRightsPullShipmentItem.Update.HasValue == false
                    || AccessRightsPullShipmentItem.Update.Value == false)
            {
                return Json(new { status = false, message = "You do not have privileges to pull items." }, JsonRequestBehavior.AllowGet);
            }

            bool saved = false;
            if (col.AllKeys.Contains("oper"))
            {
                if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    int currentShipmentItemID = Convert.ToInt32(col["ShipmentItemID"]);
                    //int debug1 = Convert.ToInt32(col["QtyReceived"]);
                    //int debug2 = Convert.ToInt32(col["ConditionReceived"]);
                    BulkShipmentItem shipmentItem = _bulkShipmentBO.GetBulkShipmentItemByID(currentShipmentItemID);

                    CostcoEntities db = new CostcoEntities();
                    V_BulkShipmentInventory inventoryItem = db.V_BulkShipmentInventory.FirstOrDefault(x => x.ShipmentItemId == shipmentItem.Id);

                    if (shipmentItem != null)
                    {
                        int quantityOnHand = (int)inventoryItem.Quantity;
                        int pull = Convert.ToInt32(col["Pulled"]);


                        if ( pull < 0)
                            return Json(new { status = false, message = "Total Pull amount cannot be less than zero." }, JsonRequestBehavior.AllowGet);

                        if (quantityOnHand - pull >= 0)
                            shipmentItem.Pulled =  pull;
                        else
                            return Json(new { status = false, message = "Pull amount greater than quantity on hand." }, JsonRequestBehavior.AllowGet);
                        saved = _bulkShipmentBO.UpdateBulkShipmentItem(shipmentItem);
                    }
                }
            }
            return Json(new { status = saved }, JsonRequestBehavior.AllowGet);
        }

    }


}
