﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL;
using Costco.BLL.BusinessObjects;
using Costco.BLL.ViewModels;
using Costco.DAL.EntityModels;
using Costco.Common;
using Costco.BLL.Authorization;

namespace Costco.Web.Controllers
{
    public class SalesOrderController : CostcoApplicationController
    {
        //
        // GET: /SalesOrder/
        private SalesOrderBO _salesOrderBO;
        //private ShipmentBO _shipmentBO;

        private ProductBO _productBO;

        private ShipmentBO _shipmentBO;

        private SalvageSalesBO _ssOrderBO;

        private BulkShipmentBO _bulkShipmentBO;

        private AccessRights AccessRightsSalesOrdersASR
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "SalesOrdersASR");
            }
        }

        private AccessRights AccessRightsSalesOrdersBuyer
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "SalesOrdersBuyer");
            }
        }

        private AccessRights AccessRightsSalesOrdersSeller
        {
            get
            {
                // Authorization authorization = new Authorization();              SalesOrdersSeller
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "SalesOrdersSeller");
            }
        }

        private AccessRights AccessRightsSalesOrdersSell
        {
            get
            {
                // Authorization authorization = new Authorization();              SalesOrdersSeller
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "SalesOrderSell");
                                                                                   
            }
        }

        public SalesOrderController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            _salesOrderBO = new SalesOrderBO(_sessionValues);
            //_shipmentBO = new ShipmentBO();
            _productBO = new ProductBO(_sessionValues);
            _shipmentBO = new ShipmentBO(_sessionValues);
            _bulkShipmentBO = new BulkShipmentBO(_sessionValues);
            _ssOrderBO = new SalvageSalesBO();
        }

        private Int32 CurrentSalesOrderID
        {
            set
            {
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.CurrentSalesOrderID, value);
            }
            get
            {
                return _sessionValues.GetSessionIntValue(Constants.SessionParameters.CurrentSalesOrderID);
            }
        }

        #region ASR
        public ActionResult Index()
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Read.HasValue == false
                    || AccessRightsSalesOrdersASR.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            var buyerList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 7 && x.ParentId == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId)).ToList();
            buyerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            buyerList.Insert(buyerList.Count, new Affiliate() { Id = 999999, Name = "New Buyer" });
            ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name");


            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name");

            List<City> cityList = new List<City>();
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name");



            ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            ViewData["AffiliateId"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
            return View();
        }

        public ActionResult Edit(Int32 id)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            CurrentSalesOrderID = id;
            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);
            if (salesOrder == null || salesOrder.Available == true)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewData["Data"] = "{" + Request["data"] + "}";
            SalesOrderViewModel model = new SalesOrderViewModel(salesOrder);


            return View(model);
        }

        [HttpGet]
        public ActionResult GetSalesOrderList(string sidx, string sord, int page, int rows, string BeginDate, string EndDate, string BeginId, string EndId, bool Incomplete=false)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _salesOrderBO.GetPaged(pageIndex, pageSize, BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), BeginId, EndId, Incomplete, null, out totalRecords).AsEnumerable<SalesOrder>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.SoldToName,
                            m.Affiliate.Name,
                            String.Format("{0:MM/dd/yyyy}",m.Created),                            
                            String.Format("{0:MM/dd/yyyy}",m.SoldDate),
                            String.Format("{0:MM/dd/yyyy}",m.ShipDate),
                            m.Available
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetSalesItemList(string sidx, string sord, int page, int rows)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = _salesOrderBO.GetSalesOrderItemsAll(pageIndex, pageSize, CurrentSalesOrderID, out totalRecords).AsEnumerable<V_SalesOrderItemAll>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.SalesOrderId,
                            m.Type,
                            m.ShipmentId,
                            m.ItemId,
                            m.Description,
                            m.Quantity,
                            m.Type,
                            m.Disposition,
                            m.ReceivedCondition2,
                            m.SaleComment,
                            m.Price,
                            (m.Price * m.Quantity)
                            //String.Format("{0:MM/dd/yyyy}",m.Shipment.ReceivedDate),
                            //m.Shipment.Affiliate.Name
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetShipmentsList(int page, int rows, string BeginDate, string EndDate, string BeginId, string EndId, string ShipmentType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _salesOrderBO.GetAvailabeShipmentsToSell(pageIndex, pageSize, BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), BeginId, EndId, ShipmentType, out totalRecords).AsEnumerable<V_ShipmentAvailableToSellAll>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Type,
                            m.Id,
                            String.Format("{0:MM/dd/yyyy}",m.ReceivedDate),
                            m.Id
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Add.HasValue == false
                    || AccessRightsSalesOrdersASR.Add.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            Int32 salseOrderId = 0;
            try
            {
                SalesOrder salesOrder = new SalesOrder();
                salesOrder.AffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                salesOrder.Created = DateTime.Now;



                salseOrderId = _salesOrderBO.Save(salesOrder);
                if (salseOrderId == 0)
                    throw new Exception();

            }
            catch (Exception)
            {

                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("Edit", new { id = salseOrderId });
        }

        [HttpPost]
        public ActionResult AddSalesOrderItems(Int32 shipmentId, string typeId)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }

            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);

            if (typeId == "B")
            {
                try
                {
                    CostcoEntities db = new CostcoEntities();
                    IList<V_BulkShipmentInventory> shipmentinventory = db.V_BulkShipmentInventory.Where(x => x.ShipmentId == shipmentId && x.Available > 0).ToList();
                    foreach (V_BulkShipmentInventory item in shipmentinventory)
                    {
                        Int32 salseOrderItemId = 0;
                        SalesOrderBulkItem orderItem = new SalesOrderBulkItem();

                        orderItem.SalesOrderId = CurrentSalesOrderID;
                        orderItem.BulkShipmentId = shipmentId;
                        orderItem.BulkShipmentItemId = item.ShipmentItemId;
                        orderItem.Quantity = (int)item.Available;
                        
                        v_MaxProductPrice pp = _productBO.GetProductPriceByItemId(item.ItemId);
                        orderItem.Price = pp == null ? 0 : pp.Price;

                        Product product = _productBO.GetProduct(item.ItemId);
                        if (product != null && product.ItemId.Trim().ToUpper() != "MISC" && product.ItemId.Trim().Length <= 6 && product.ItemId.Trim().Length > 1)
                            orderItem.Description = product.Description;
                        else
                            orderItem.Description = item.Description;


                        BulkShipmentItem shipmentItem = _bulkShipmentBO.GetBulkShipmentItemByID(item.ShipmentItemId);
                        if (shipmentItem.ReceivedCondition2 == "Tested OK." ||
                            shipmentItem.ReceivedCondition2 == "Out of box." ||
                            shipmentItem.ReceivedCondition2 == "In box." ||
                            shipmentItem.ReceivedCondition2 == "Parts Only.")
                        {
                            orderItem.Comment = shipmentItem.ReceivedCondition2;
                        }

                        if (salesOrder.InitialPercentage != null && orderItem.Price > 0)
                        {
                            decimal pct = (decimal)salesOrder.InitialPercentage;
                            pct = Math.Round(pct * (decimal).01, 4);
                            orderItem.SellingPrice = Math.Round((pct * (decimal)orderItem.Price), 2);
                        }


                        salseOrderItemId = _salesOrderBO.AddSalesOrderBulkItem(orderItem);
                        if (salseOrderItemId == 0)
                            throw new Exception();
                    }
                    
                    if (salesOrder.InitialPercentage != null)
                        _salesOrderBO.UpdateSoldAmount(salesOrder.Id);

                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to add shipment." }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (typeId == "S")
            {
                try
                {
                    CostcoEntities db = new CostcoEntities();
                    IList<V_ShipmentInventory> shipmentinventory = db.V_ShipmentInventory.Where(x => x.ShipmentId == shipmentId && x.Available > 0).ToList();

                    foreach (V_ShipmentInventory item in shipmentinventory)
                    {
                        Int32 salseOrderItemId = 0;
                        SalesOrderItem orderItem = new SalesOrderItem();

                        orderItem.SalesOrderId = CurrentSalesOrderID;
                        orderItem.ShipmentId = shipmentId;
                        orderItem.ShipmentItemId = item.ShipmentItemId;
                        orderItem.Quantity = (int)item.Available;
                            
                        v_MaxProductPrice pp = _productBO.GetProductPriceByItemId(item.ItemId);
                        orderItem.Price = pp == null ? 0 : pp.Price;

                        Product product = _productBO.GetProduct(item.ItemId);

                        if (product != null && product.ItemId.Trim().ToUpper() != "MISC" && product.ItemId.Trim().Length <= 6 && product.ItemId.Trim().Length > 1)
                            orderItem.Description = product.Description;
                        else
                            orderItem.Description = item.Description;


                        ShipmentItem shipmentItem = _shipmentBO.GetShipmentItem(item.ShipmentItemId);
                        if (shipmentItem.ReceivedCondition2 == "Tested OK." ||
                            shipmentItem.ReceivedCondition2 == "Out of box." ||
                            shipmentItem.ReceivedCondition2 == "In box." ||
                            shipmentItem.ReceivedCondition2 == "Parts Only.")
                        {
                            orderItem.Comment = shipmentItem.ReceivedCondition2;
                        }

                        if (salesOrder.InitialPercentage != null && orderItem.Price > 0)
                        {
                            decimal pct = (decimal)salesOrder.InitialPercentage;
                            pct = Math.Round(pct * (decimal).01, 4);
                            orderItem.SellingPrice = Math.Round((pct * (decimal)orderItem.Price), 2);
                        }

                        salseOrderItemId = _salesOrderBO.AddSalesOrderItem(orderItem);
                        if (salseOrderItemId == 0)
                            throw new Exception();
                    }

                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to add shipment." }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (typeId == "C")
            {
                try
                {
                    CostcoEntities db = new CostcoEntities();
                    IList<V_SSOrderInventory> ssoinventory = db.V_SSOrderInventory.Where(x => x.SSOrderId == shipmentId && x.Available > 0).ToList();

                    foreach (V_SSOrderInventory item in ssoinventory)
                    {
                        Int32 salseOrderItemId = 0;
                        SalesOrderSSOItem orderItem = new SalesOrderSSOItem();

                        orderItem.SalesOrderId = CurrentSalesOrderID;
                        orderItem.SSOId = shipmentId;
                        orderItem.SSOItemId = item.SSOrderItemId;
                        orderItem.Quantity = (int)item.Available;

                        v_MaxProductPrice pp = _productBO.GetProductPriceByItemId(item.ItemId);
                        orderItem.Price = pp == null ? 0 : pp.Price;

                        Product product = _productBO.GetProduct(item.ItemId);

                        if (product != null && product.ItemId.Trim().ToUpper() != "MISC" && product.ItemId.Trim().Length <= 6 && product.ItemId.Trim().Length > 1)
                            orderItem.Description = product.Description;
                        else
                            orderItem.Description = item.Description;



                        if (salesOrder.InitialPercentage != null && orderItem.Price > 0)
                        {
                            decimal pct = (decimal)salesOrder.InitialPercentage;
                            pct = Math.Round(pct * (decimal).01, 4);
                            orderItem.SellingPrice = Math.Round((pct * (decimal)orderItem.Price), 2);
                        }

                        salseOrderItemId = _salesOrderBO.AddSalesOrderSSOItems(orderItem);
                        if (salseOrderItemId == 0)
                            throw new Exception();
                    }

                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to add shipment." }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSalesOrderItem(Int32 salesOrderItemId, string typeId)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Delete.HasValue == false
                    || AccessRightsSalesOrdersASR.Delete.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }

            if (typeId == "B")
            {
                try
                {
                    SalesOrderBulkItem item = _salesOrderBO.GetSalesOrderBulkItem(salesOrderItemId);
                    if(_salesOrderBO.DeleteSalesOrderBulkItem(salesOrderItemId) == false)
                        throw new Exception();

                    if  (item.SellingPrice != null || item.SellingPrice > 0)
                        _salesOrderBO.UpdateSoldAmount(item.SalesOrderId);
                    
                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to delete item." }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (typeId == "S")
            {
                try
                {
                    SalesOrderItem item = _salesOrderBO.GetSalesOrderItem(salesOrderItemId);
                    if (_salesOrderBO.DeleteSalesOrderItem(salesOrderItemId) == false)
                        throw new Exception();
                    if (item.SellingPrice != null || item.SellingPrice > 0)
                        _salesOrderBO.UpdateSoldAmount(item.SalesOrderId);
                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to delete item." }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (typeId == "C")
            {
                try
                {
                    SalesOrderSSOItem item = _salesOrderBO.GetSalesOrderSSOItem(salesOrderItemId);

                    if (_salesOrderBO.DeleteSalesOrderSSOItem(salesOrderItemId) == false)
                        throw new Exception();
                    if (item.SellingPrice != null || item.SellingPrice > 0)
                        _salesOrderBO.UpdateSoldAmount(item.SalesOrderId);
                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to delete item." }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSalesOrderItemsShipment(Int32 salesOrderId, Int32 shipmentId, string typeId)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }
            if (typeId == "B")
            {
                try
                {
                    CostcoEntities db = new CostcoEntities();
                    IList<SalesOrderBulkItem> items = db.SalesOrderBulkItems.Where(x => x.SalesOrderId == salesOrderId && x.BulkShipmentId == shipmentId).ToList();

                    foreach (SalesOrderBulkItem saleOrderItem in items)
                    {
                        if (_salesOrderBO.DeleteSalesOrderBulkItem(saleOrderItem.Id) == false)
                            throw new Exception();
                    }

                    _salesOrderBO.UpdateSoldAmount(salesOrderId);
                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to delete shipment." }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (typeId == "S")
            {
                try
                {
                    CostcoEntities db = new CostcoEntities();
                    IList<SalesOrderItem> items = db.SalesOrderItems.Where(x => x.SalesOrderId == salesOrderId && x.ShipmentId == shipmentId).ToList();

                    foreach (SalesOrderItem saleOrderItem in items)
                    {
                        if (_salesOrderBO.DeleteSalesOrderItem(saleOrderItem.Id) == false)
                            throw new Exception();
                    }
                    _salesOrderBO.UpdateSoldAmount(salesOrderId);
                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to delete shipment." }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (typeId == "C")
            {
                try
                {
                    CostcoEntities db = new CostcoEntities();
                    IList<SalesOrderSSOItem> items = db.SalesOrderSSOItems.Where(x => x.SalesOrderId == salesOrderId && x.SSOId == shipmentId).ToList();

                    foreach (SalesOrderSSOItem saleOrderItem in items)
                    {
                        if (_salesOrderBO.DeleteSalesOrderSSOItem(saleOrderItem.Id) == false)
                            throw new Exception();
                    }

                    _salesOrderBO.UpdateSoldAmount(salesOrderId);
                }
                catch (Exception)
                {
                    return Json(new { status = false, message = "Unable to delete shipment." }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditSalesOrderItem(FormCollection col)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }
            bool saved = false;
            decimal total = 0;
            if (col.AllKeys.Contains("oper"))
            {
                if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    if (col["TypeId"] == "B")
                    {
                        int currentSalesOrderItemId = Convert.ToInt32(col["SalesOrderItemId"]);
                        SalesOrderBulkItem item = _salesOrderBO.GetSalesOrderBulkItem(currentSalesOrderItemId);
                        if (item != null)
                        {
                            int newQuantity = Convert.ToInt32(col["Qty"]);
                            CostcoEntities db = new CostcoEntities();
                            V_BulkShipmentInventory inventoryItem = db.V_BulkShipmentInventory.FirstOrDefault(x => x.ShipmentItemId == item.BulkShipmentItemId);

                            if (inventoryItem == null)
                                return Json(new { status = false, message = "An error occurred while trying to update." }, JsonRequestBehavior.AllowGet);

                            if (newQuantity > item.Quantity + inventoryItem.Available)
                                return Json(new { status = false, message = "New quantity is greater than quantity on hand." }, JsonRequestBehavior.AllowGet);
                            if (newQuantity <= 0)
                                return Json(new { status = false, message = "New quantity can not be less than or equal to zero." }, JsonRequestBehavior.AllowGet);

                            string itemid = item.BulkShipmentItem.ItemId.Trim();
                            int intItemId = 0;
                            decimal newPrice = Convert.ToDecimal(col["Price"]);

                            if ((itemid.Length == 6 || itemid.Length == 5) && int.TryParse(itemid, out intItemId) && newPrice != item.Price)
                            {
                                ProductPrice pp = new ProductPrice();
                                pp.ItemId = itemid;
                                pp.Price = Convert.ToDecimal(col["Price"]);
                                pp.TimeStamp = DateTime.Now;
                                pp.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                                int ppId = _productBO.AddProductPrice(pp);
                            }
                            if (item.SellingPrice != null && item.Price != newPrice)
                            {
                                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(item.SalesOrderId);
                                if (salesOrder.InitialPercentage != null)
                                {
                                    decimal pct = (decimal)salesOrder.InitialPercentage;
                                    pct = Math.Round(pct * (decimal).01, 4);
                                    item.SellingPrice = Math.Round((pct * newPrice), 2);
                                }
                            }
                            item.Price = newPrice;
                            item.Price = newPrice;


                            string newDescription = col["Description"];
                            if ((itemid.Length == 6 || itemid.Length == 5) && int.TryParse(itemid, out intItemId) && newDescription != item.Description && newDescription != null)
                            {
                                Product product = new CostcoService<Product>().GetSingle(x => x.ItemId == itemid);
                                product.Description = newDescription;
                                _productBO.UpdateProduct(product);

                                ProductDescription pd = new ProductDescription();
                                pd.ItemId = itemid;
                                pd.Description = newDescription;
                                pd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                                pd.Timestamp = DateTime.Now;
                                int pdId = _productBO.AddProductDescription(pd);

                                item.Description = newDescription;

                            }
                            else if (newDescription != item.Description && newDescription != null)
                            {
                                item.Description = newDescription;
                            }


                            item.Comment = col["SaleComment"];
                            int oldQuantity = item.Quantity;
                            item.Quantity = newQuantity;
                            saved = _salesOrderBO.UpdateSalesOrderBulkItem(item);

                            total = (decimal)item.Quantity * (decimal)item.Price;
                            //if (oldQuantity != newQuantity && (item.SellingPrice != null || item.SellingPrice > 0))
                            _salesOrderBO.UpdateSoldAmount(item.SalesOrderId);

                            
                            
                        }
                        
                    }
                    else if (col["TypeId"] == "S")
                    {
                        int currentSalesOrderItemId = Convert.ToInt32(col["SalesOrderItemId"]);
                        SalesOrderItem item = _salesOrderBO.GetSalesOrderItem(currentSalesOrderItemId);
                        if (item != null)
                        {
                            int newQuantity = Convert.ToInt32(col["Qty"]);
                            CostcoEntities db = new CostcoEntities();
                            V_ShipmentInventory inventoryItem = db.V_ShipmentInventory.FirstOrDefault(x => x.ShipmentItemId == item.ShipmentItemId);

                            if (inventoryItem == null)
                                return Json(new { status = false, message = "An error occurred while trying to update." }, JsonRequestBehavior.AllowGet);

                            if (newQuantity > item.Quantity + inventoryItem.Available)
                                return Json(new { status = false, message = "New quantity is greater than quantity on hand." }, JsonRequestBehavior.AllowGet);
                            if (newQuantity <= 0)
                                return Json(new { status = false, message = "New quantity can not be less than or equal to zero." }, JsonRequestBehavior.AllowGet);

                            string itemid = item.ShipmentItem.ItemId.Trim();
                            int intItemId = 0;
                            decimal newPrice =  Convert.ToDecimal(col["Price"]);

                            if ((itemid.Length == 6 || itemid.Length == 5) && int.TryParse(itemid, out intItemId) && newPrice != item.Price)
                            {
                                ProductPrice pp = new ProductPrice();
                                pp.ItemId = itemid;
                                pp.Price = Convert.ToDecimal(col["Price"]);
                                pp.TimeStamp = DateTime.Now;
                                pp.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                                int ppId = _productBO.AddProductPrice(pp);
                                
                                
                            }

                            if (item.SellingPrice != null && item.Price != newPrice)
                            {
                                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(item.SalesOrderId);
                                if (salesOrder.InitialPercentage != null)
                                {
                                    decimal pct = (decimal)salesOrder.InitialPercentage;
                                    pct = Math.Round(pct * (decimal).01, 4);
                                    item.SellingPrice = Math.Round((pct  * newPrice),2);
                                }
                            }
                            item.Price = newPrice;


                            string newDescription = col["Description"];

                            if ((itemid.Length == 6 || itemid.Length == 5) && int.TryParse(itemid, out intItemId) && newDescription != item.Description && newDescription != null)
                            {
                                Product product = new CostcoService<Product>().GetSingle(x => x.ItemId == itemid);
                                product.Description = newDescription;
                                _productBO.UpdateProduct(product);

                                ProductDescription pd = new ProductDescription();
                                pd.ItemId = itemid;
                                pd.Description = newDescription;
                                pd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                                pd.Timestamp = DateTime.Now;
                                int pdId = _productBO.AddProductDescription(pd);

                                item.Description = newDescription;

                            }
                            else if (newDescription != item.Description && newDescription != null)
                            {
                                item.Description = newDescription;
                            }


                            item.Comment = col["SaleComment"];

                            int oldQuantity = item.Quantity;
                            item.Quantity = newQuantity;
                            saved = _salesOrderBO.UpdateSalesOrderItem(item);
                            total = (decimal)item.Quantity * (decimal)item.Price;
                            //if (oldQuantity != newQuantity && (item.SellingPrice != null || item.SellingPrice > 0))
                            _salesOrderBO.UpdateSoldAmount(item.SalesOrderId);
                        }
                    }
                    else if (col["TypeId"] == "C")
                    {
                        int currentSalesOrderItemId = Convert.ToInt32(col["SalesOrderItemId"]);
                        SalesOrderSSOItem item = _salesOrderBO.GetSalesOrderSSOItem(currentSalesOrderItemId);
                        if (item != null)
                        {
                            int newQuantity = Convert.ToInt32(col["Qty"]);
                            CostcoEntities db = new CostcoEntities();
                            V_SSOrderInventory inventoryItem = db.V_SSOrderInventory.FirstOrDefault(x => x.SSOrderItemId == item.SSOItemId);

                            if (inventoryItem == null)
                                return Json(new { status = false, message = "An error occurred while trying to update." }, JsonRequestBehavior.AllowGet);

                            if (newQuantity > item.Quantity + inventoryItem.Available)
                                return Json(new { status = false, message = "New quantity is greater than quantity on hand." }, JsonRequestBehavior.AllowGet);
                            if (newQuantity <= 0)
                                return Json(new { status = false, message = "New quantity can not be less than or equal to zero." }, JsonRequestBehavior.AllowGet);

                            string itemid = item.SalvageSalesOrderItem.ItemId.Trim();
                            int intItemId = 0;
                            decimal newPrice = Convert.ToDecimal(col["Price"]);

                            if ((itemid.Length == 6 || itemid.Length == 5) && int.TryParse(itemid, out intItemId) && newPrice != item.Price)
                            {
                                ProductPrice pp = new ProductPrice();
                                pp.ItemId = itemid;
                                pp.Price = Convert.ToDecimal(col["Price"]);
                                pp.TimeStamp = DateTime.Now;
                                pp.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                                int ppId = _productBO.AddProductPrice(pp);


                            }

                            if (item.SellingPrice != null && item.Price != newPrice)
                            {
                                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(item.SalesOrderId);
                                if (salesOrder.InitialPercentage != null)
                                {
                                    decimal pct = (decimal)salesOrder.InitialPercentage;
                                    pct = Math.Round(pct * (decimal).01, 4);
                                    item.SellingPrice = Math.Round((pct * newPrice), 2);
                                }
                            }
                            item.Price = newPrice;


                            string newDescription = col["Description"];

                            if ((itemid.Length == 6 || itemid.Length == 5) && int.TryParse(itemid, out intItemId) && newDescription != item.Description && newDescription != null)
                            {
                                Product product = new CostcoService<Product>().GetSingle(x => x.ItemId == itemid);
                                product.Description = newDescription;
                                _productBO.UpdateProduct(product);

                                ProductDescription pd = new ProductDescription();
                                pd.ItemId = itemid;
                                pd.Description = newDescription;
                                pd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                                pd.Timestamp = DateTime.Now;
                                int pdId = _productBO.AddProductDescription(pd);

                                item.Description = newDescription;

                            }
                            else if (newDescription != item.Description && newDescription != null)
                            {
                                item.Description = newDescription;
                            }


                            item.Comment = col["SaleComment"];

                            int oldQuantity = item.Quantity;
                            item.Quantity = newQuantity;
                            saved = _salesOrderBO.UpdateSalesOrderSSOItem(item);
                            total = (decimal)item.Quantity * (decimal)item.Price;
                            //if (oldQuantity != newQuantity && (item.SellingPrice != null || item.SellingPrice > 0))
                            _salesOrderBO.UpdateSoldAmount(item.SalesOrderId);
                        }
                    }
                }
            }
            return Json(new { status = saved, total = total }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ReadyToSell(int salesOrderId, decimal minimumValue)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { success = false, message = "Access denied." }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(salesOrderId);
                if (salesOrder == null)
                    throw (new Exception("Sales order does not exist"));
                salesOrder.Available = true;
                salesOrder.MinValue = minimumValue;
                bool saved = _salesOrderBO.Update(salesOrder);

                if (!saved)
                    throw new Exception("An error occurred saving sales order!");

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateShipDate(int salesOrderId, DateTime shipDate)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(salesOrderId);
                if (salesOrder == null)
                    throw (new Exception("SalesOrder does not exist"));

                salesOrder.ShipDate = shipDate;
                bool saved = _salesOrderBO.Update(salesOrder);

                if (!saved)
                    throw new Exception("An error occurred saving salesOrder!");

                return Json(new { success = true, shipDate = String.Format("{0:MM/dd/yyyy}", salesOrder.ShipDate) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }




        [HttpPost]
        public JsonResult RevertShipDate(int salesOrderId)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(salesOrderId);
                if (salesOrder == null)
                    throw (new Exception("SalesOrder does not exist"));



                DateTime? nullDate = null;
                salesOrder.ShipDate = nullDate;
                bool saved = _salesOrderBO.Update(salesOrder);

                if (!saved)
                    throw new Exception("An error occurred saving salesOrder!");

                return Json(new { success = true, shipDate = String.Format("{0:MM/dd/yyyy}", salesOrder.ShipDate) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateWeightPallets(int salesOrderId, int pallets, int weight, string comments)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { success = false, message = "Access denied." }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(salesOrderId);
                if (salesOrder == null)
                    throw (new Exception("Sales order does not exist"));

                salesOrder.TotalPallets = pallets;
                salesOrder.TotalWeight = weight;
                salesOrder.Comment = comments;
                bool saved = _salesOrderBO.Update(salesOrder);

                if (!saved)
                    throw new Exception("An error occurred saving sales order!");

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Seller/E-World

        public ActionResult IndexSeller()
        {
            if (AccessRightsSalesOrdersSeller == null || AccessRightsSalesOrdersSeller.Read.HasValue == false
                    || AccessRightsSalesOrdersSeller.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            return View();
        }

        [HttpGet]
        public ActionResult GetSaleOrdersSellerList(string sidx, string sord, int page, int rows, string BeginDate, string EndDate, string BeginId, string EndId, string AuctionType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            if (AuctionType == null)
                AuctionType = "AllOpen";

            var data = _salesOrderBO.GetPaged(pageIndex, pageSize, BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), BeginId, EndId, false,  AuctionType, out totalRecords).AsEnumerable<SalesOrder>();
            var data2 = _salesOrderBO.GetBids(BeginDate, EndDate).AsEnumerable<V_MaxBid>();


            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from s in data
                    join b in data2 on s.Id equals b.SalesOrderId into gj
                    from subset in gj.DefaultIfEmpty()

                    select new
                    {
                        id = s.Id,
                        cell = new object[] {
                            s.Id,
                            s.AffiliateId,
                            String.Format("{0:MM/dd/yyyy}",s.Created),
                            String.Format("{0:MM/dd/yyyy}",s.SoldDate),
                            s.SoldToName,
                            s.SoldToAffiliateId,
                            s.Id,
                            subset == null ? 0:subset.BidCount,
                            s.SoldDate == null ? (subset == null ? 0:subset.Amount) : s.SoldAmount,
                            s.Invoiced,
                            s.SoldDate == null ? "Open":" "
                            
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }


        public ActionResult EditSeller(Int32 id)
        {
            if (AccessRightsSalesOrdersSeller == null || AccessRightsSalesOrdersSeller.Update.HasValue == false
                    || AccessRightsSalesOrdersSeller.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CurrentSalesOrderID = id;
            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);
            if (salesOrder == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewData["Data"] = "{" + Request["data"] + "}";
            SalesOrderViewModel model = new SalesOrderViewModel(salesOrder);

            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            if (salesOrder.SoldToStateId != null)
                ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name", salesOrder.SoldToStateId);
            else
                ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name");

            List<City> cityList = new List<City>();
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            if (salesOrder.SoldToCityId != null)
                ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name", salesOrder.SoldToCityId);
            else
                ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name");

            var buyerList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 7).ToList();
            buyerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            if (salesOrder.SoldToAffiliateId != null)
                ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name", salesOrder.SoldToAffiliateId);
            else
                ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name");

            return View(model);
        }

        [HttpPost]
        public ActionResult EditSeller(SalesOrder salesOrder)
        {
            if (AccessRightsSalesOrdersSeller == null || AccessRightsSalesOrdersSeller.Update.HasValue == false
                 || AccessRightsSalesOrdersSeller.Update.Value == false)
            {   
                return RedirectToAction("AccessDenied", "Home");
            }
            CurrentSalesOrderID = salesOrder.Id;
            SalesOrder order = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);
            if (order == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                try
                {
                    order.SoldDate = salesOrder.SoldDate;
                    order.SoldAmount = salesOrder.SoldAmount;
                    order.TotalWeight = salesOrder.TotalWeight;
                    order.TotalPallets = salesOrder.TotalPallets;
                    order.Comment = salesOrder.Comment;

                    if (salesOrder.SoldToAffiliateId == 0)
                        order.SoldToAffiliateId = null;
                    else
                        order.SoldToAffiliateId = salesOrder.SoldToAffiliateId;

                    if (salesOrder.SoldToStateId == 0)
                        order.SoldToStateId = null;
                    else
                        order.SoldToStateId = salesOrder.SoldToStateId;

                    if (salesOrder.SoldToCityId == 0)
                        order.SoldToCityId = null;
                    else
                        order.SoldToCityId = salesOrder.SoldToCityId;

                    order.SoldToName = salesOrder.SoldToName;
                    order.SoldToAddress = salesOrder.SoldToAddress;
                    order.SoldToZip = salesOrder.SoldToZip;
                    _salesOrderBO.Update(order);
                }
                catch (Exception)
                {
                    return RedirectToAction("Error", "Home");
                }


            }
            return RedirectToAction("IndexSeller");
        }

        [HttpGet]
        public ActionResult GetBids(string sidx, string sord, int page, int rows)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = _salesOrderBO.GetBids(pageIndex, pageSize, CurrentSalesOrderID, out totalRecords).AsEnumerable<V_MaxAffiliateBid>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.SalesOrderId,
                        cell = new object[] {
                            m.SalesOrderId,
                            m.Buyer_Id,
                            m.Buyer_Name,
                            m.Buyer_City,
                            m.Buyer_State,
                            m.Buyer_Zip,
                            m.BidCount,
                            m.Amount
                           
                            //String.Format("{0:MM/dd/yyyy}",m.Shipment.ReceivedDate),
                            //m.Shipment.Affiliate.Name
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateInvoiced(Int32 id)
        {
            string msg = String.Empty;
            if (AccessRightsSalesOrdersSeller == null || AccessRightsSalesOrdersSeller.Update.HasValue == false
                    || AccessRightsSalesOrdersSeller.Update.Value == false)
            {
                msg = "Access Denied";
                return Json(new { success = false, message = msg });
            }

            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(id);
            if (salesOrder == null)
            {
                msg = "An error occurred";
                return Json(new { success = false, message = msg });
            }
 
            bool currentInvoiced = salesOrder.Invoiced ;
            salesOrder.Invoiced = !(currentInvoiced);


            if (_salesOrderBO.Update(salesOrder))
                return Json(new { success = true, message = msg });
            else
                return Json(new { success = false, message = "An error occurred." });

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnsellSalesOrder(Int32 id)
        {
            string msg = String.Empty;
            if (AccessRightsSalesOrdersSeller == null || AccessRightsSalesOrdersSeller.Update.HasValue == false
                    || AccessRightsSalesOrdersSeller.Update.Value == false)
            {
                msg = "Access Denied";
                return Json(new { success = false, message = msg });
            }

            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(id);
            if (salesOrder == null)
            {
                msg = "An error occurred";
                return Json(new { success = false, message = msg });
            }

            salesOrder.Available = false;
            DateTime? nullDate = null;
            salesOrder.SoldDate = nullDate;
            salesOrder.SoldToAffiliateId = null;
            salesOrder.SoldToName = null;
            salesOrder.SoldToCityId = null;
            salesOrder.SoldToStateId = null;
            salesOrder.SoldToAddress = null;
            salesOrder.SoldToZip = null;


            if (_salesOrderBO.Update(salesOrder))
                return Json(new { success = true, message = msg });
            else
                return Json(new { success = false, message = "An error occurred." });

        }

        #endregion

        #region EWorldAdmin

        public ActionResult IndexEworld()
        {
            if (AccessRightsSalesOrdersSeller == null || AccessRightsSalesOrdersSeller.Read.HasValue == false
                    || AccessRightsSalesOrdersSeller.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            return View();
        }

        [HttpGet]
        public ActionResult GetSaleOrderEworldList(string sidx, string sord, int page, int rows, string BeginDate, string EndDate, string BeginId, string EndId, string InvoiceStatus)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            if (InvoiceStatus == null)
                InvoiceStatus = "NotInvoiced";

            var data = _salesOrderBO.GetPagedEworld(pageIndex, pageSize, BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), BeginId, EndId, InvoiceStatus, out totalRecords).AsEnumerable<SalesOrder>();
  


            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages, 
                page = page,
                records = totalRecords,
                rows = (
                    from s in data


                    select new
                    {
                        id = s.Id,
                        cell = new object[] {
                            s.Id,
                            s.AffiliateId,
                            s.Affiliate.Name,
                            String.Format("{0:MM/dd/yyyy}",s.Created),
                            String.Format("{0:MM/dd/yyyy}",s.SoldDate),
                            s.SoldToName,
                            s.SoldToAffiliateId,
                            s.Id,
                            s.SoldDate == null ? 0 : s.SoldAmount,
                            s.Invoiced,
                            s.Invoiced
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        #endregion


        #region buyer

        public ActionResult IndexBuyer()
        {
            if (AccessRightsSalesOrdersBuyer == null || AccessRightsSalesOrdersBuyer.Read.HasValue == false
                    || AccessRightsSalesOrdersBuyer.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            return View();
        }

        [HttpGet]
        public ActionResult GetSaleOrdersBuyerList(string sidx, string sord, int page, int rows, string BeginDate, string EndDate, string BeginId, string EndId, string AuctionType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            if (AuctionType == null)
                AuctionType = "AllOpen";

            var data = _salesOrderBO.GetPaged(pageIndex, pageSize, BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), BeginId, EndId, false, AuctionType, out totalRecords).AsEnumerable<SalesOrder>();
            

            if (AuctionType == "Open" || AuctionType == "Won" || AuctionType == "Lost")
            {
                var data2 = _salesOrderBO.GetBids(BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), AuctionType).AsEnumerable<V_MaxAffiliateBid>();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = (
                        from b in data2 
                        join s in data on b.SalesOrderId equals s.Id
                        select new
                        {
                            id = s.Id,
                            cell = new object[] {
                            s.Id,
                            s.AffiliateId,
                            String.Format("{0:MM/dd/yyyy}",s.Created),
                            String.Format("{0:MM/dd/yyyy}",s.SoldDate),
                            s.Id,
                            s.Affiliate.State.Abbreviation,
                            s.TotalPallets,
                            b.Buyer_Id,
                            b.Amount,
                            b.Status                       
                        }
                        }).ToArray()
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data2 = _salesOrderBO.GetBids(BeginDate, EndDate, _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId), null).AsEnumerable<V_MaxAffiliateBid>();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var jsonData = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = (
                        from s in data
                        join b in data2 on s.Id equals b.SalesOrderId into gj
                        from subset in gj.DefaultIfEmpty()

                        select new
                        {
                            id = s.Id,
                            cell = new object[] {
                            s.Id,
                            s.AffiliateId,
                            String.Format("{0:MM/dd/yyyy}",s.Created),
                            String.Format("{0:MM/dd/yyyy}",s.SoldDate),
                            s.Id,
                            s.Affiliate.State.Abbreviation,
                            s.TotalPallets,
                            subset == null ? 0:subset.Buyer_Id,
                            subset == null ? 0:subset.Amount,
                            s.SoldDate == null ? "Open": s.SoldToAffiliateId == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) ? "Won" : "Lost"                        
                        }
                        }).ToArray()
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddBid(FormCollection col)
        {
            if (AccessRightsSalesOrdersBuyer == null || AccessRightsSalesOrdersBuyer.Update.HasValue == false
                    || AccessRightsSalesOrdersBuyer.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                CostcoEntities db = new CostcoEntities();
                decimal newBidAmount = Convert.ToDecimal(col["Amount"]);
                int buyerId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                int salesOrderId = Convert.ToInt32(col["id"]);

                V_MaxAffiliateBid oldBid = db.V_MaxAffiliateBid.FirstOrDefault(x => x.Buyer_Id == buyerId && x.SalesOrderId == salesOrderId);

                if (oldBid != null && (oldBid.Amount >= newBidAmount || newBidAmount <= 0))
                    return Json(new { status = false, message = "Bid must be higher than previous bid." }, JsonRequestBehavior.AllowGet);


                Int32 bidId = 0;
                Bid bid = new Bid();
                bid.AffiliateId = buyerId;
                bid.SalesOrderId = salesOrderId;
                bid.Amount = newBidAmount;
                bid.Created = DateTime.Now;

                bidId = _salesOrderBO.AddBid(bid);
                if (bidId == 0)
                    throw new Exception();

            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to add bid." }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region ASRReceiving

        [HttpPost]
        public JsonResult AddSaleOrderShipmentItem(FormCollection col)
        {

            string message = "";
            bool success = false;
            int salesOrderId = 0;
            SalesOrder salesOrder = null;

            if (Int32.TryParse(col["SalesOrderId"], out salesOrderId))
                salesOrder = _salesOrderBO.GetSalesOrder(salesOrderId);
            if (salesOrder == null || salesOrder.AffiliateId != _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId))
                return Json(new { success = false, message = "Invalid Sales Order" }, JsonRequestBehavior.AllowGet);

            if (salesOrder.Available)
                return Json(new { success = false, message = "Cannot add to closed Sales Order" }, JsonRequestBehavior.AllowGet);

            int quantity = 0;

            success = Int32.TryParse(col["Quantity"], out quantity);
            if (success == false || quantity == 0)
                return Json(new { success = false, message = "Invalid Quantity" }, JsonRequestBehavior.AllowGet);

            int shipmentItemId = Convert.ToInt32(col["ShipmentItemId"]);


            CostcoEntities db = new CostcoEntities();
            V_ShipmentInventory inventoryItem = db.V_ShipmentInventory.FirstOrDefault(x => x.ShipmentItemId == shipmentItemId);

            if (inventoryItem == null)
                return Json(new { success = false, message = "An error occurred while trying to add." }, JsonRequestBehavior.AllowGet);

            if (quantity > inventoryItem.Available)
                return Json(new { success = false, message = "Quantity is greater than quantity on hand. \n Available: " + inventoryItem.Available.ToString()   }, JsonRequestBehavior.AllowGet);


            SalesOrderItem orderItem = new SalesOrderItem();

            orderItem.SalesOrderId = salesOrderId;
            orderItem.ShipmentId = inventoryItem.ShipmentId;
            orderItem.ShipmentItemId = inventoryItem.ShipmentItemId;
            orderItem.Quantity = quantity;

            if (salesOrder.InitialPercentage != null && orderItem.Price > 0)
            {
                decimal pct = (decimal)salesOrder.InitialPercentage;
                pct = Math.Round(pct * (decimal).01, 4);
                orderItem.SellingPrice = Math.Round((pct * (decimal)orderItem.Price), 2);
            }
            int salseOrderItemId = _salesOrderBO.AddSalesOrderItem(orderItem);
            if (orderItem.SellingPrice != null)
                _salesOrderBO.UpdateSoldAmount(salesOrder.Id);

            if (salseOrderItemId == 0)
                return Json(new { success = false, message = "An error occurred while trying to add." }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = true, message = message }, JsonRequestBehavior.AllowGet);


        }

        #endregion ASRReceiving


        [HttpGet]
        public ActionResult Sell(Int32 id)
        {
            if (AccessRightsSalesOrdersSell == null || AccessRightsSalesOrdersSell.Update.HasValue == false
                    || AccessRightsSalesOrdersSell.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CurrentSalesOrderID = id;
            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);
            if (salesOrder == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewData["Data"] = "{" + Request["data"] + "}";
            SalesOrderViewModel model = new SalesOrderViewModel(salesOrder);

            if (salesOrder.SoldAmount == null)
            {
                return RedirectToAction("SellingPrices", new { id = id });
            }


            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            if (salesOrder.SoldToStateId != null)
                ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name", salesOrder.SoldToStateId);
            else
                ViewData["BuyerState"] = new SelectList(stateList, "Id", "Name");

            List<City> cityList = new List<City>();
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            if (salesOrder.SoldToCityId != null)
                ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name", salesOrder.SoldToCityId);
            else
                ViewData["ddlBuyerCity"] = new SelectList(cityList, "Id", "Name");

            var buyerList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 7 && x.ParentId == salesOrder.AffiliateId).ToList();
            buyerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            buyerList.Insert(buyerList.Count, new Affiliate() { Id = 999999, Name = "New Buyer" });
            if (salesOrder.SoldToAffiliateId != null)
                ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name", salesOrder.SoldToAffiliateId);
            else
                ViewData["BuyerList"] = new SelectList(buyerList, "Id", "Name");

            if (salesOrder.SoldAmount == null || salesOrder.SoldAmount == 0) 
                ViewData["IsSold"] = false;
            else
                ViewData["IsSold"] = true;

            decimal retailValue = _salesOrderBO.GetRetailValue(CurrentSalesOrderID);

            ViewData["RetailValue"] = retailValue.ToString("0.00");
            ViewData["Price"] = (retailValue * .20m).ToString("0.00");

            return View(model);
        }

        [HttpPost]
        public ActionResult Sell(SalesOrder salesOrder)
        {
            if (AccessRightsSalesOrdersSell == null || AccessRightsSalesOrdersSell.Update.HasValue == false
                 || AccessRightsSalesOrdersSell.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CurrentSalesOrderID = salesOrder.Id;
            SalesOrder order = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);
            if (order == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                try
                {
                    order.SoldDate = salesOrder.SoldDate;
                    //order.SoldAmount = salesOrder.SoldAmount;
                    //order.MinValue = salesOrder.SoldAmount;
                    order.TotalWeight = salesOrder.TotalWeight;
                    order.TotalPallets = salesOrder.TotalPallets;
                    order.Comment = salesOrder.Comment;
                    order.Available = true;

                    if (salesOrder.SoldToAffiliateId == 999999)
                    { 
                        Affiliate buyer = new Affiliate();
                        buyer.Address1 =  salesOrder.SoldToAddress;
                        buyer.StateId =  salesOrder.SoldToStateId;
                        buyer.CityId = salesOrder.SoldToCityId;
                        buyer.Zip = salesOrder.SoldToZip;
                        buyer.Name = salesOrder.SoldToName;
                        buyer.ParentId = salesOrder.AffiliateId;
                        buyer.AffiliateTypeId = 7;
                        int newBuyerId =  _salesOrderBO.AddBuyer(buyer);
                        salesOrder.SoldToAffiliateId = newBuyerId;
                    }

                    if (salesOrder.SoldToAffiliateId == 0)
                        order.SoldToAffiliateId = null;
                    else
                        order.SoldToAffiliateId = salesOrder.SoldToAffiliateId;

                    if (salesOrder.SoldToStateId == 0)
                        order.SoldToStateId = null;
                    else
                        order.SoldToStateId = salesOrder.SoldToStateId;

                    if (salesOrder.SoldToCityId == 0)
                        order.SoldToCityId = null;
                    else
                        order.SoldToCityId = salesOrder.SoldToCityId;

                    
                    
                    order.SoldToName = salesOrder.SoldToName;
                    order.SoldToAddress = salesOrder.SoldToAddress;
                    order.SoldToZip = salesOrder.SoldToZip;
                    _salesOrderBO.Update(order);
                }
                catch (Exception)
                {
                    return RedirectToAction("Error", "Home");
                }


            }
            return RedirectToAction("Index");
        }


        #region SellItems
        [HttpGet]
        public ActionResult GetShipmentItemsToPull(string sidx, string sord, int page, int rows, Int32 shipmentId = 0)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            Int32 _shipmetId;
            _shipmetId = shipmentId;


            Shipment shipment = _shipmentBO.GetShipment(_shipmetId);

            int affiliateTypeId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

            if (shipment == null)
                return Content("{rows:[]}");

            CostcoEntities db = new CostcoEntities();
            IQueryable<V_ShipmentInventory> inventory = db.V_ShipmentInventory.Where(x => x.ShipmentId == _shipmetId && x.Available > 0);
            inventory.OrderBy(e => e.ShipmentItemId).Skip(pageIndex * pageSize).Take(pageSize);

            totalRecords = inventory.Count<V_ShipmentInventory>();
            var data = inventory.AsEnumerable<V_ShipmentInventory>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.ShipmentItemId,
                        cell = new object[] {
                            m.ShipmentId,
                            m.ShipmentItemId,
                            m.ItemId,
                            m.Description,
                            m.ShipmentItem.Disposition,
                            m.ShipmentItem.ConditionType != null ? m.ShipmentItem.ConditionType.Condition : "",
                            m.ShipmentItem.ReceivedCondition2,
                            m.ShipmentItem.CoveredDevice,
                            m.Quantity,
                            m.Available,
                            m.Pulled
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetBulkShipmentItemsToPull(string sidx, string sord, int page, int rows, Int32 shipmentId = 0)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            Int32 _shipmetId;
            _shipmetId = shipmentId;

            BulkShipment shipment = _bulkShipmentBO.GetBulkShipment(_shipmetId);

            int affiliateTypeId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

            if (shipment == null)
                return Content("{rows:[]}");

            if (affiliateTypeId == 5 && affiliateId != shipment.ShipToAffiliateId)
                return Content("{rows:[]}");

            CostcoEntities db = new CostcoEntities();
            IQueryable<V_BulkShipmentInventory> inventory = db.V_BulkShipmentInventory.Where(x => x.ShipmentId == _shipmetId && x.Available > 0);
            inventory.OrderBy(e => e.ShipmentItemId).Skip(pageIndex * pageSize).Take(pageSize);

            totalRecords = inventory.Count<V_BulkShipmentInventory>();
            var data = inventory.AsEnumerable<V_BulkShipmentInventory>();



            //var data = _shipmentBO.GetShipmentItems(pageIndex, pageSize, _shipmetId, out totalRecords).AsEnumerable<ShipmentItem>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.ShipmentItemId,
                        cell = new object[] {
                            m.ShipmentId,
                            m.ShipmentItemId,
                            m.ItemId,
                            m.Description,
                            m.ShipmentItem.Disposition,
                            m.ShipmentItem.ConditionType != null ? m.ShipmentItem.ConditionType.Condition : "",
                            m.ShipmentItem.ReceivedCondition2,
                            m.ShipmentItem.CoveredDevice,
                            m.Quantity,
                            m.Available,
                            m.Pulled
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetSSOrderItemsToPull(string sidx, string sord, int page, int rows, Int32 ssOrderId = 0)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            Int32 _ssOrderId;
            _ssOrderId = ssOrderId;


            SalvageSalesOrder ssOrder = _ssOrderBO.GetSalvageSalesOrder(_ssOrderId);


            int affiliateTypeId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);

            if (ssOrder == null)
                return Content("{rows:[]}");

            CostcoEntities db = new CostcoEntities();
            IQueryable<V_SSOrderInventory> inventory = db.V_SSOrderInventory.Where(x => x.SSOrderId == _ssOrderId && x.Available > 0);
            inventory.OrderBy(e => e.SSOrderItemId).Skip(pageIndex * pageSize).Take(pageSize);

            totalRecords = inventory.Count<V_SSOrderInventory>();
            var data = inventory.AsEnumerable<V_SSOrderInventory>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.SSOrderItemId,
                        cell = new object[] {
                            m.SSOrderId,
                            m.SSOrderItemId,
                            m.ItemId,
                            m.Description,
                            //m.ShipmentItems.Disposition,
                            //m.ShipmentItems.ConditionType != null ? m.ShipmentItems.ConditionType.Condition : "",
                            //m.ShipmentItems.ReceivedCondition2,
                            //m.ShipmentItems.CoveredDevice,
                            m.Quantity,
                            m.Available,
                            m.Pulled
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult AddSalesOrderItem(Int32 shipmentItemId, bool AllItems=true)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }
            
            try
            {
                CostcoEntities db = new CostcoEntities();
                V_ShipmentInventory item = db.V_ShipmentInventory.First(x => x.ShipmentItemId == shipmentItemId && x.Available > 0);
                if (item == null)
                    throw new Exception();

                Int32 salseOrderItemId = 0;
                SalesOrderItem orderItem = new SalesOrderItem();

                orderItem.SalesOrderId = CurrentSalesOrderID;
                orderItem.ShipmentId = item.ShipmentId;
                orderItem.ShipmentItemId = item.ShipmentItemId;

                if (AllItems)
                    orderItem.Quantity = (int)item.Available;
                else {
                    if ((int)item.Available > 0)
                        orderItem.Quantity = 1;
                    else
                        throw new Exception();
                }

                v_MaxProductPrice pp = _productBO.GetProductPriceByItemId(item.ItemId);
                orderItem.Price = pp == null ? 0 : pp.Price;

                Product product = _productBO.GetProduct(item.ItemId);

                if (product != null && product.ItemId.Trim().ToUpper() != "MISC" && product.ItemId.Trim().Length <= 6 && product.ItemId.Trim().Length > 1)
                    orderItem.Description = product.Description;
                else
                    orderItem.Description = item.Description;


                ShipmentItem shipmentItem = _shipmentBO.GetShipmentItem(item.ShipmentItemId);
                if (shipmentItem.ReceivedCondition2 == "Tested OK." ||
                    shipmentItem.ReceivedCondition2 == "Out of box." ||
                    shipmentItem.ReceivedCondition2 == "In box." ||
                    shipmentItem.ReceivedCondition2 == "Parts Only.")
                {
                    orderItem.Comment = shipmentItem.ReceivedCondition2;
                }

                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);

                if (salesOrder.InitialPercentage != null && orderItem.Price > 0)
                {
                    decimal pct = (decimal)salesOrder.InitialPercentage;
                    pct = Math.Round(pct * (decimal).01, 4);
                    orderItem.SellingPrice = Math.Round((pct * (decimal)orderItem.Price), 2);
                }


                salseOrderItemId = _salesOrderBO.AddSalesOrderItem(orderItem);
                
                if (orderItem.SellingPrice != null)
                    _salesOrderBO.UpdateSoldAmount(salesOrder.Id);
                if (salseOrderItemId == 0)
                    throw new Exception();

            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to add item." }, JsonRequestBehavior.AllowGet);
            }
            CostcoEntities db2 = new CostcoEntities();
            V_ShipmentInventory item2 = db2.V_ShipmentInventory.First(x => x.ShipmentItemId == shipmentItemId);

            return Json(new { status = true, available = item2.Available, allocated=item2.Allocated, qty=item2.Quantity, pulled=item2.Pulled }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddSalesOrderBulkItem(Int32 shipmentItemId, bool AllItems=true)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                CostcoEntities db = new CostcoEntities();
                V_BulkShipmentInventory item = db.V_BulkShipmentInventory.First(x => x.ShipmentItemId == shipmentItemId && x.Available > 0);

                Int32 salseOrderItemId = 0;
                SalesOrderBulkItem orderItem = new SalesOrderBulkItem();

                orderItem.SalesOrderId = CurrentSalesOrderID;
                orderItem.BulkShipmentId = item.ShipmentId;
                orderItem.BulkShipmentItemId = item.ShipmentItemId;


                if (AllItems)
                    orderItem.Quantity = (int)item.Available;
                else
                {
                    if ((int)item.Available > 0)
                        orderItem.Quantity = 1;
                    else
                        throw new Exception();
                }


                v_MaxProductPrice pp = _productBO.GetProductPriceByItemId(item.ItemId);
                orderItem.Price = pp == null ? 0 : pp.Price;

                Product product = _productBO.GetProduct(item.ItemId);
                if (product != null && product.ItemId.Trim().ToUpper() != "MISC" && product.ItemId.Trim().Length <= 6 && product.ItemId.Trim().Length > 1)
                    orderItem.Description = product.Description;
                else
                    orderItem.Description = item.Description;


                BulkShipmentItem shipmentItem = _bulkShipmentBO.GetBulkShipmentItemByID(item.ShipmentItemId);
                if (shipmentItem.ReceivedCondition2 == "Tested OK." ||
                    shipmentItem.ReceivedCondition2 == "Out of box." ||
                    shipmentItem.ReceivedCondition2 == "In box." ||
                    shipmentItem.ReceivedCondition2 == "Parts Only.")
                {
                    orderItem.Comment = shipmentItem.ReceivedCondition2;
                }

                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);

                if (salesOrder.InitialPercentage != null && orderItem.Price > 0)
                {
                    decimal pct = (decimal)salesOrder.InitialPercentage;
                    pct = Math.Round(pct * (decimal).01, 4);
                    orderItem.SellingPrice = Math.Round((pct * (decimal)orderItem.Price), 2);
                }


                salseOrderItemId = _salesOrderBO.AddSalesOrderBulkItem(orderItem);
                if (orderItem.SellingPrice != null)
                    _salesOrderBO.UpdateSoldAmount(salesOrder.Id);
                if (salseOrderItemId == 0)
                    throw new Exception();

            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to add shipment." }, JsonRequestBehavior.AllowGet);
            }


            CostcoEntities db2 = new CostcoEntities();
            V_BulkShipmentInventory item2 = db2.V_BulkShipmentInventory.First(x => x.ShipmentItemId == shipmentItemId);
            return Json(new { status = true, available = item2.Available, allocated = item2.Allocated, qty = item2.Quantity, pulled = item2.Pulled }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddSalesOrderSSOItem(Int32 shipmentItemId, bool AllItems = true)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                CostcoEntities db = new CostcoEntities();
                V_SSOrderInventory item = db.V_SSOrderInventory.First(x => x.SSOrderItemId == shipmentItemId && x.Available > 0);

                Int32 salseOrderItemId = 0;
                SalesOrderSSOItem orderItem = new SalesOrderSSOItem();

                orderItem.SalesOrderId = CurrentSalesOrderID;
                orderItem.SSOId = item.SSOrderId;
                orderItem.SSOItemId = item.SSOrderItemId;


                if (AllItems)
                    orderItem.Quantity = (int)item.Available;
                else
                {
                    if ((int)item.Available > 0)
                        orderItem.Quantity = 1;
                    else
                        throw new Exception();
                }


                v_MaxProductPrice pp = _productBO.GetProductPriceByItemId(item.ItemId);
                orderItem.Price = pp == null ? 0 : pp.Price;

                Product product = _productBO.GetProduct(item.ItemId);
                if (product != null && product.ItemId.Trim().ToUpper() != "MISC" && product.ItemId.Trim().Length <= 6 && product.ItemId.Trim().Length > 1)
                    orderItem.Description = product.Description;
                else
                    orderItem.Description = item.Description;



                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);

                if (salesOrder.InitialPercentage != null && orderItem.Price > 0)
                {
                    decimal pct = (decimal)salesOrder.InitialPercentage;
                    pct = Math.Round(pct * (decimal).01, 4);
                    orderItem.SellingPrice = Math.Round((pct * (decimal)orderItem.Price), 2);
                }


                salseOrderItemId = _salesOrderBO.AddSalesOrderSSOItems(orderItem);
                if (orderItem.SellingPrice != null)
                    _salesOrderBO.UpdateSoldAmount(salesOrder.Id);
                if (salseOrderItemId == 0)
                    throw new Exception();

            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to add shipment." }, JsonRequestBehavior.AllowGet);
            }


            CostcoEntities db2 = new CostcoEntities();
            V_SSOrderInventory item2 = db2.V_SSOrderInventory.First(x => x.SSOrderItemId == shipmentItemId);
            return Json(new { status = true, available = item2.Available, allocated = item2.Allocated, qty = item2.Quantity, pulled = item2.Pulled }, JsonRequestBehavior.AllowGet);
        }

        #endregion  

        [HttpGet]
        public ActionResult SellingPrices(Int32 id)
        {
            if (AccessRightsSalesOrdersSell == null || AccessRightsSalesOrdersSell.Update.HasValue == false
                    || AccessRightsSalesOrdersSell.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CurrentSalesOrderID = id;
            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(CurrentSalesOrderID);
            if (salesOrder == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewData["Data"] = "{" + Request["data"] + "}";
            SalesOrderViewModel model = new SalesOrderViewModel(salesOrder);

            decimal initialPercentage = 0;
            decimal retailValue = _salesOrderBO.GetRetailValue(CurrentSalesOrderID);
            if (salesOrder.InitialPercentage != null)
                initialPercentage = (decimal)salesOrder.InitialPercentage; 
            

            ViewData["RetailValue"] = retailValue.ToString("0.00");
            //ViewData["InitialPercentage"] = initialPercentage.ToString("0.00");  // (retailValue * .20m).ToString("0.00");

            return View(model);
        }

        [HttpGet]
        public ActionResult GetSellingPricesList(string sidx, string sord, int page, int rows)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;
            var data = _salesOrderBO.GetSalesOrderItemsAll(pageIndex, pageSize, CurrentSalesOrderID, out totalRecords).AsEnumerable<V_SalesOrderItemAll>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        //  colNames: ['Id', 'SalesOrderId', 'Type', 'Shipment', 'ItemId', 'Description', 'Retail', 'Price', 'Qty', 'Total'],
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.SalesOrderId,
                            m.Type,
                            m.ShipmentId,
                            m.ItemId,
                            m.Description,
                            String.Format("{0:#,0.00}",m.Price),
                            String.Format("{0:#,0.00}",m.SellingPrice),
                            m.Quantity,
                            String.Format("{0:#,0.00}",m.Cost * m.Quantity),
                            String.Format("{0:#,0.00}",m.SellingPrice * m.Quantity)
                            

                            //String.Format("{0:#,0.0000}",invoiceBO.GetInvoiceAmount(m.Id,m.Affiliate.Id,m.InvoiceDate.Value)),
                            //String.Format("{0:MM/dd/yyyy}",m.Shipment.ReceivedDate),
                            //m.Shipment.Affiliate.Name
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangeInitialPercentage(Int32 salesOrderId, decimal newPct)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }
            newPct = Math.Round(newPct * (decimal)(.01), 4);
            decimal totalSellingPrice = 0;
            try
            {
                SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(salesOrderId);
                if (salesOrder == null)
                    throw new Exception();

                //CostcoEntities db = new CostcoEntities();

                int totalRecords;
                var data = _salesOrderBO.GetSalesOrderItemsAll(0, 1000000, salesOrderId, out totalRecords).AsEnumerable<V_SalesOrderItemAll>();
                
                foreach (V_SalesOrderItemAll vItem in data)
                {
                    if (vItem.Type == "S")
                    {
                        SalesOrderItem item = _salesOrderBO.GetSalesOrderItem(vItem.Id);
                        item.SellingPrice =  Math.Round((decimal)(item.Price * newPct),2);
                        if (!_salesOrderBO.UpdateSalesOrderItem(item))
                            throw new Exception();
                    }
                    else if (vItem.Type == "B")
                    {
                        SalesOrderBulkItem item = _salesOrderBO.GetSalesOrderBulkItem(vItem.Id);
                        item.SellingPrice = Math.Round((decimal)(item.Price * newPct), 2);
                        if (!_salesOrderBO.UpdateSalesOrderBulkItem(item))
                            throw new Exception();
                    }
                    else if (vItem.Type == "C")
                    {
                        SalesOrderSSOItem item = _salesOrderBO.GetSalesOrderSSOItem(vItem.Id);
                        item.SellingPrice = Math.Round((decimal)(item.Price * newPct), 2);
                        if (!_salesOrderBO.UpdateSalesOrderSSOItem(item))
                            throw new Exception();
                    }
                
                }
                //salesOrder.SoldAmount = totalSellingPrice;

                salesOrder.InitialPercentage = newPct * 100;
                _salesOrderBO.Update(salesOrder);
                totalSellingPrice = (decimal)_salesOrderBO.UpdateSoldAmount(salesOrder.Id);
            }
            catch (Exception)
            {
                return Json(new { status = false, message = "Unable to apply percentage." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = true, soldAmount = totalSellingPrice.ToString("0.00") , newPct = newPct * 100}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditItemSellingPrice(FormCollection col)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { status = false, message = "You have insufficient rights" }, JsonRequestBehavior.AllowGet);
            }
            bool saved = false;
            decimal newAmount=0;
            decimal newTotal = 0;
            if (col.AllKeys.Contains("oper"))
            {
                if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    if (col["TypeId"] == "B")
                    {
                        int currentSalesOrderItemId = Convert.ToInt32(col["SalesOrderItemId"]);
                        SalesOrderBulkItem item = _salesOrderBO.GetSalesOrderBulkItem(currentSalesOrderItemId);
                        

                        if (item != null)
                        {
                            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(item.SalesOrderId);
                            if (salesOrder != null)
                            {
                                item.SellingPrice = Math.Round( Convert.ToDecimal(col["Price"]),2) ;
                                saved = _salesOrderBO.UpdateSalesOrderBulkItem(item);
                                newTotal = (decimal)(item.SellingPrice * item.Quantity);
                                if (saved)
                                    newAmount = (decimal)_salesOrderBO.UpdateSoldAmount(salesOrder.Id);
                            }
                        }

                    }
                    else if (col["TypeId"] == "S")
                    {
                        int currentSalesOrderItemId = Convert.ToInt32(col["SalesOrderItemId"]);
                        SalesOrderItem item = _salesOrderBO.GetSalesOrderItem(currentSalesOrderItemId);
                        if (item != null)
                        {
                            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(item.SalesOrderId);
                            if (salesOrder != null)
                            {
                                item.SellingPrice = Math.Round(Convert.ToDecimal(col["Price"]), 2);
                                saved = _salesOrderBO.UpdateSalesOrderItem(item);
                                newTotal = (decimal)(item.SellingPrice * item.Quantity);
                                if (saved)
                                    newAmount = (decimal)_salesOrderBO.UpdateSoldAmount(salesOrder.Id);
                            }
                        }
                    }
                    else if (col["TypeId"] == "C")
                    {
                        int currentSalesOrderItemId = Convert.ToInt32(col["SalesOrderItemId"]);
                        SalesOrderSSOItem item = _salesOrderBO.GetSalesOrderSSOItem(currentSalesOrderItemId);
                        if (item != null)
                        {
                            SalesOrder salesOrder = _salesOrderBO.GetSalesOrder(item.SalesOrderId);
                            if (salesOrder != null)
                            {
                                item.SellingPrice = Math.Round(Convert.ToDecimal(col["Price"]), 2);
                                saved = _salesOrderBO.UpdateSalesOrderSSOItem(item);
                                newTotal = (decimal)(item.SellingPrice * item.Quantity);
                                if (saved)
                                    newAmount = (decimal)_salesOrderBO.UpdateSoldAmount(salesOrder.Id);
                             
                            }
                        }
                    }
                }
            }
            if (saved==false)
                return Json(new { status = saved, message = "Unable to change price." }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { status = saved, newAmount = newAmount.ToString("0.00"), rowId = col["id"], newTotal = newTotal.ToString("0.00")  }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetBuyer(int salesOrderId, int SoldToAffiliateId, string SoldToName,string SoldToAddress,int SoldToState, int SoldToCity,string SoldToZip)
        {
            if (AccessRightsSalesOrdersASR == null || AccessRightsSalesOrdersASR.Update.HasValue == false
                    || AccessRightsSalesOrdersASR.Update.Value == false)
            {
                return Json(new { success = false, message = "Access denied." }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                SalesOrder order = _salesOrderBO.GetSalesOrder(salesOrderId);
                if (order == null)
                    throw (new Exception("Sales order does not exist"));

                
                //int salesOrderId, int ddlBuyerList, string SoldToName,string  SoldToAddress,int SoldToBuyerState, int SoldToBuyerCity,string SoldToZip
                if (SoldToAffiliateId == 999999)
                {
                    Affiliate buyer = new Affiliate();
                    buyer.Address1 = SoldToAddress;
                    buyer.StateId = SoldToState;
                    buyer.CityId = SoldToCity;
                    buyer.Zip = SoldToZip;
                    buyer.Name = SoldToName;
                    buyer.ParentId = order.AffiliateId;
                    buyer.AffiliateTypeId = 7;
                    int newBuyerId = _salesOrderBO.AddBuyer(buyer);

                    SoldToAffiliateId = newBuyerId;
                }

                if (SoldToAffiliateId == 0)
                    order.SoldToAffiliateId = null;
                else
                    order.SoldToAffiliateId = SoldToAffiliateId;

                if (SoldToState == 0)
                    order.SoldToStateId = null;
                else
                    order.SoldToStateId = SoldToState;

                if (SoldToCity == 0)
                    order.SoldToCityId = null;
                else
                    order.SoldToCityId = SoldToCity;



                order.SoldToName = SoldToName;
                order.SoldToAddress = SoldToAddress;
                order.SoldToZip = SoldToZip;


                bool saved = _salesOrderBO.Update(order);

                if (!saved)
                    throw new Exception("An error occurred saving sales order!");

                return Json(new { success = true, buyerName = order.SoldToName }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
         
    }
}
