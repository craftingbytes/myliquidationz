﻿using Costco.BLL.BusinessObjects;
using Costco.BLL.ViewModels;
using Costco.Common;
using Costco.DAL.EntityModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Constants = Costco.Common.Constants;

namespace Costco.Web.Controllers
{
    [AllowAnonymous]
    public class AccountController : CostcoApplicationController
    {
        private LoginBO _loginBO;

        public AccountController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            _loginBO = new LoginBO(_sessionValues);
        }

        public ActionResult Login()
        {
            return View(new LoginViewModel()); 
        }

    
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            List<Message> messages = new List<Message>();
            AffiliateContactViewModel vm = _loginBO.LoginAffiliateContact(model.UserName, model.Password);
            if (vm == null)
            {
                LoginViewModel newModel = new LoginViewModel();
                messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.InvalidLoginCredentials));
                newModel.Messages = messages;
                return View(newModel);
            }
            else if (vm.Messages.Count > 0)
            {
                LoginViewModel newModel = new LoginViewModel();
                newModel.Messages = vm.Messages;
                return View(newModel);
            }
            else
            {
                var contact = vm.AffiliateContact;
                var nameParts = new[] { contact.FirstName, contact.MiddleInitials, contact.LastName }.Where(p => p != null);
                var claimsIdentity = new ClaimsIdentity(
                    new Claim[] {
                        new Claim("email", contact.Email),
                        new Claim("role", contact.Role.RoleName),
                        new Claim("name", string.Join(" ", nameParts)),
                    }, DefaultAuthenticationTypes.ApplicationCookie, "name", "role");
                HttpContext.GetOwinContext().Authentication.SignIn(claimsIdentity);

                if (_sessionValues != null)
                {
                    Dictionary<string, string> values = new Dictionary<string, string>()
                    {
                        { Constants.SessionParameters.AffiliateId, vm.AffiliateContact.Affiliate.Id.ToString() },
                        { Constants.SessionParameters.AffiliateContactId, vm.AffiliateContact.Id.ToString() },
                        { Constants.SessionParameters.AffiliateRoleId, vm.AffiliateContact.RoleId.ToString() },
                        { Constants.SessionParameters.AffiliateTypeId, vm.AffiliateContact.Affiliate.AffiliateTypeId.ToString() },
                        { Constants.SessionParameters.AffiliateContactName, vm.AffiliateContact.FirstName + " " + vm.AffiliateContact.LastName },
                        { Constants.SessionParameters.IpAddress, HttpContext.Request.UserHostAddress.ToString() }
                    };

                    _sessionValues.SetSessionValues(values);
                }

                AuditEvent auditEvent = new AuditEvent();
                auditEvent.AffiliateContactId = vm.AffiliateContact.Id;
                auditEvent.Entity = "Login";
                auditEvent.AuditActionId = Convert.ToInt32(Costco.Common.Constants.ActionType.Login);
                auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
                auditEvent.TimeStamp = DateTime.Now;
                auditEvent.Note = HttpContext.Request.Browser.Browser + " " + HttpContext.Request.Browser.Version;
                _loginBO.AddAuditEvent(auditEvent);


                int typeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
                int roleid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId);
                if (typeid == 4 && roleid == 10)
                    return RedirectToAction("SellerList", "SalvageSales");
                else if (typeid == 4)
                    return RedirectToAction("List", "Shipment");

                else
                    return RedirectToAction("Index","Home");
                //if ((int)Constants.AffiliateType.Eworld == SessionParameters.AffiliateTypeId)
                //{
                //    if ((int)Constants.AffiliateContactRole.Finance == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId))
                //        return RedirectToAction("Index", "Invoice");
                //    else
                //        return RedirectToAction("Index", "Affiliate");
                //}
                //else if ((int)Constants.AffiliateType.OEM == SessionParameters.AffiliateTypeId)
                //{
                //    SessionParameters.SelectedAffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                //    MapXMLHandler.CreateMapXMLFile(affiliateContactModel.AffiliateContact.AffiliateId.Value, affiliateContactModel.AffiliateContact.Affiliate.AffiliateTargets.ToList());
                //    return RedirectToAction("Map", "Affiliate", new { id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) });
                //    //return RedirectToAction("Index", "AffiliateContact", new { id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) });
                //}
                //else if ((int)Constants.AffiliateType.Processor == SessionParameters.AffiliateTypeId)
                //{
                //    SessionParameters.SelectedAffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                //    return RedirectToAction("Index", "ProcessingData", new { id = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) });
                //}
                //else if ((int)Constants.AffiliateType.Collector == SessionParameters.AffiliateTypeId)
                //{
                //}
                //else if ((int)Constants.AffiliateType.State == SessionParameters.AffiliateTypeId)
                //{
                //}
                //else if ((int)Constants.AffiliateType.Auditor == SessionParameters.AffiliateTypeId)
                //{
                //    SessionParameters.SelectedAffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                //    return RedirectToAction("Index", "ResetPassword");
                //}
            }

        }

        [HttpGet]
        public ActionResult Logout()
        {
            _sessionValues.ClearCookie();
            HttpContext.GetOwinContext().Authentication.SignOut(/*DefaultAuthenticationTypes.ApplicationCookie*/);
            ViewData["AffiliateContactName"] = "-1";
            return RedirectToAction(nameof(Login));
        }

    }
}
