﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL;
using Costco.BLL.BusinessObjects;
using Costco.DAL.EntityModels;
using Costco.Common;
using Costco.BLL.Authorization;

namespace Costco.Web.Controllers
{
    public class InvoiceController : CostcoApplicationController
    {
        private InvoiceBO _invoiceBO;

        private AccessRights AccessRightsCostcoBill
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "CostcoBill");
            }
        }

        private AccessRights AccessRightsASRPayable
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "ASRPayable");
            }
        }

        public InvoiceController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            _invoiceBO = new InvoiceBO(_sessionValues);
        }

        public ActionResult Index()
        {
            if (AccessRightsCostcoBill == null || AccessRightsCostcoBill.Read.HasValue == false
                    || AccessRightsCostcoBill.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            return RedirectToAction("CostcoBills");
        }

        public ActionResult Create()
        {
            if (AccessRightsCostcoBill == null || AccessRightsCostcoBill.Add.HasValue == false
                    || AccessRightsCostcoBill.Add.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            if (AccessRightsCostcoBill == null || AccessRightsCostcoBill.Add.HasValue == false
                || AccessRightsCostcoBill.Add.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            try
            {
                Affiliate affiliate = new CostcoService<Affiliate>().GetSingle(x => x.AffiliateTypeId == 2);

                Invoice invoice = new Invoice();
                invoice.InvoiceTypeId = 1;
                invoice.AffiliateId = affiliate.Id;
                invoice.InvoiceDate = DateTime.Today;
                invoice.Rate = GetRate(affiliate.Id, 1).Rate;
                Int32 invoiceId = _invoiceBO.Save(invoice);
                if (invoiceId == 0)
                    throw new Exception();

                UpdateShipmentsWithId(invoiceId, form);
                GenerateASRInvoices(invoiceId);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("Create");
        }

        [HttpGet]
        public ActionResult GetShipmentsList(string sidx, string sord, int page, int rows, int Year, string Months)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _invoiceBO.GetShipments(pageIndex, pageSize, Year, Months, out totalRecords).AsEnumerable<Shipment>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.Affiliate1.Name,
                            m.Affiliate.WarehouseNumber,
                            String.Format("{0:MM/dd/yyyy}",m.ReceivedDate),
                            m.ReceivedTotalWeight,
                            m.Id
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        private void GenerateASRInvoices(Int32 invoiceId)
        { 
            
           //var asrs = new CostcoService<Shipment>().GetAll(x => x.Invoiceid == invoiceId).Distinct<Shipment>()
            var asrs = (from asr in new CostcoService<Shipment>().GetAll(x => x.Invoiceid == invoiceId)
                        select asr.ShipToAffiliateId).Distinct();

            foreach (int asr in asrs)
            {
                Invoice invoice = new Invoice();
                invoice.InvoiceTypeId = 2; //payable to ASR
                invoice.AffiliateId = asr;
                invoice.InvoiceDate = DateTime.Today;
                invoice.Rate = GetRate(asr, 1).Rate;
                invoice.ParentInvoiceId = invoiceId;
                Int32 invoiceid = _invoiceBO.Save(invoice);
                if (invoiceid == 0)
                    throw new Exception();
            }

        
        }

        private void UpdateShipmentsWithId(Int32 invoiceId, FormCollection form)
        {
            string shipmentlist = form["shipmentList"];
            List<int> shipmentIds = shipmentlist.Split(',').Select(n => int.Parse(n)).ToList();

            foreach (int shipmentId in shipmentIds)
            {
                _invoiceBO.UpdateShipmentWithInvoiceId(shipmentId, invoiceId);
            }
            //JavaScriptSerializer JSS = new JavaScriptSerializer();

            //string key = string.Empty;
            //string value = string.Empty;
  
            //for (int i = 1; i <= form.AllKeys.Length; i++)
            //{
                
            //    key = "Item_" + i.ToString();
            //    if (form[key] != null)
            //    {
            //        Int32 shipmentId = 0;
            //        value = form[key];
            //        string[] jsonCollection = value.Split(new char[] { ',' });
            //        foreach (var jsonItem in jsonCollection)
            //        {
            //            string[] clientitem = jsonItem.Split(new char[] { ':' });
            //            switch (clientitem[0])
            //            {
            //                case "ShipmentId":
            //                    shipmentId = Convert.ToInt32(clientitem[1]);
            //                    break;
            //            }
            //        }
            //        if (shipmentId > 0)
            //            _invoiceBO.UpdateShipmentWithInvoiceId(shipmentId, invoiceId);

            //    }
            //}
        }

        private AffiliateRate GetRate(Int32 affiliateId, Int32 serviceTypeId)
        {
            AffiliateRate rate = new CostcoService<AffiliateRate>().GetSingle(x => x.AffiliateId == affiliateId && x.ServiceTypeId == serviceTypeId);

            return rate;
        }

        public ActionResult CostcoBills()
        {
            if (AccessRightsCostcoBill == null || AccessRightsCostcoBill.Read.HasValue == false
                    || AccessRightsCostcoBill.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            return View();
        }

        [HttpGet]
        public ActionResult GetCostcoBillsList(string sidx, string sord, int page, int rows, bool _search, string BeginDate, string EndDate, string BeginId, string EndId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _invoiceBO.GetCostcoBills(pageIndex, pageSize, BeginDate, EndDate, BeginId, EndId, out totalRecords).AsEnumerable<V_InvoiceCostcoBill>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.Name,
                            String.Format("{0:MM/dd/yyyy}",m.InvoiceDate),
                            m.TotalWeight,
                            m.Total,
                            m.Id,
                            m.Notes != null ? m.Notes : ""
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult AddAdustment(FormCollection col)
        {

            string message = "";
            bool status = true;
            int invoiceId = Convert.ToInt32(col["InvoiceId"]);
            string adjDescription = col["Description"].ToString();
            decimal adjAmount = Convert.ToDecimal(col["Amount"]);



            string msg = String.Empty;
            if ((AccessRightsCostcoBill != null && AccessRightsCostcoBill.Update.HasValue == true && AccessRightsCostcoBill.Update == true) ||
                (AccessRightsASRPayable != null && AccessRightsASRPayable.Update.HasValue == true && AccessRightsASRPayable.Update == true))
            {
                message = _invoiceBO.AddInvoiceAdjustment(invoiceId, adjDescription, adjAmount);
                if (message == "Success")
                    status = true;
            }
            else
            {
                message = "Access Denied";
                status = false;
            }



            var jsonData = new
            {
                message = message,
                status = status.ToString()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateInvoice(FormCollection col)
        {
            try
            {
                Invoice invoice = _invoiceBO.GetInvoice(Convert.ToInt32(col["InvoiceId"]));
                if (invoice == null)
                    throw (new Exception("Shipment does not exist"));

                invoice.InvoiceDate = Convert.ToDateTime(col["InvoiceDate"]);
                invoice.Notes = col["Notes"];
                bool saved = _invoiceBO.UpdateInvoice(invoice);

                if (!saved)
                    throw new Exception("An error occurred saving invoice!");

                return Json(new { success = true, invoiceDate = String.Format("{0:MM/dd/yyyy}", invoice.InvoiceDate) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ASRPayables()
        {
            ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            return View();
        }

        [HttpGet]
        public ActionResult GetASRPayablesList(string sidx, string sord, int page, int rows, bool _search, string BeginDate, string EndDate, string BeginId, string EndId, string ParentId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _invoiceBO.GetASRPayables(pageIndex, pageSize, BeginDate, EndDate, BeginId, EndId, ParentId, out totalRecords).AsEnumerable<V_InvoiceASRPayable>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.ParentInvoiceId,
                            m.Id,
                            m.Name,
                            String.Format("{0:MM/dd/yyyy}",m.InvoiceDate),
                            m.TotalWeight,
                            m.Total,
                            m.Id,
                            m.Notes != null ? m.Notes : ""
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
    
    }
}
