﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL;
using Costco.BLL.BusinessObjects;
using Costco.DAL.EntityModels;
using Costco.Common;
using Costco.BLL.Authorization;
using System.Web.Script.Serialization;

namespace Costco.Web.Controllers
{
    public class ShipmentController : CostcoApplicationController
    {
        private ShipmentBO _shipmentBO;
        private ProductBO _productBO;
        private AccessRights AccessRightsShipmentCreate
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "ShipmentCreate");
            }
        }
        private AccessRights AccessRightsShipmentList
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "ShipmentList");
            }
        }

        private Int32 CurrentShippingID
        {
            set
            {
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.CurrentShippingID, value);
            }
            get
            {
                return _sessionValues.GetSessionIntValue(Constants.SessionParameters.CurrentShippingID);
            }
        }

        public ShipmentController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            ViewData["AccessRights"] = AccessRightsShipmentCreate;
            _shipmentBO = new ShipmentBO(_sessionValues);
            _productBO = new ProductBO(_sessionValues);
        }

        public class LineItem
        {
            public int PalletUnitNumber { get; set; }
            public string ItemId { get; set; }
            public string Description { get; set; }
            public int Qty { get; set; }
            public string Condition { get; set; }
        }
      
        public ActionResult Index()
        {

            Affiliate affiliateData = _shipmentBO.GetAffiliate(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId));

            ViewData["txtAffiliateName"] = affiliateData.Name;
            ViewData["txtPickupAddress"] = affiliateData.Address1;
            ViewData["txtPickupZip"] = affiliateData.Zip;


            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            ViewData["PickupState"] = new SelectList(stateList, "Id", "Name", affiliateData.StateId);

            List<City> cityList = new List<City>();
            if (affiliateData.StateId > 0)
            {
                var data1 = new CostcoService<City>().GetAll(x => x.StateId == affiliateData.StateId);
                cityList = data1.OrderBy(f => f.Name).ToList();
            }
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            ViewData["PickupCity"] = new SelectList(cityList, "Id", "Name", affiliateData.CityId);


            if (affiliateData.DefaultRecycler != null)
            {
                Affiliate recyclerData = _shipmentBO.GetAffiliate((int)affiliateData.DefaultRecycler);

                ViewData["txtRecyclerName"] = recyclerData.Name;
                ViewData["txtShipToAddress"] = recyclerData.Address1;
                ViewData["txtShipToZip"] = recyclerData.Zip;

                var stateListShipTo = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
                stateListShipTo.Insert(0, new State() { Id = 0, Name = "Select" });
                ViewData["ShipToState"] = new SelectList(stateListShipTo, "Id", "Name", recyclerData.StateId);

                List<City> cityListShipTo = new List<City>();
                if (recyclerData.StateId > 0)
                {
                    var data1 = new CostcoService<City>().GetAll(x => x.StateId == recyclerData.StateId);
                    cityListShipTo = data1.OrderBy(f => f.Name).ToList();
                }

                cityListShipTo.Insert(0, new City() { Id = 0, Name = "Select" });
                ViewData["ShipToCity"] = new SelectList(cityListShipTo, "Id", "Name", recyclerData.CityId);
                

            }
            else
            {
                ViewData["txtRecyclerName"] = "";
                ViewData["txtShipToAddress"] = "";
                ViewData["txtShipToZip"] = "";

                var stateListShipTo = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
                stateListShipTo.Insert(0, new State() { Id = 0, Name = "Select" });
                ViewData["ShipToState"] = new SelectList(stateListShipTo, "Id", "Name");

                List<City> cityListShipTo = new List<City>();
                cityListShipTo.Insert(0, new City() { Id = 0, Name = "Select" });
                ViewData["ShipToCity"] = new SelectList(cityListShipTo, "Id", "Name");         
            }

            var recyclerList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 5 && (x.Id == affiliateData.DefaultRecycler || x.Name == "Donations" )).ToList();
            recyclerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            if (affiliateData.DefaultRecycler != null)
                ViewData["RecyclerList"] = new SelectList(recyclerList, "Id", "Name",affiliateData.DefaultRecycler);
            else
                ViewData["RecyclerList"] = new SelectList(recyclerList, "Id", "Name");

            var carrierList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 6).ToList();
            carrierList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            if (affiliateData.DefaultCarrier != null)
                ViewData["CarrierList"] = new SelectList(carrierList, "Id", "Name", affiliateData.DefaultCarrier);
            else
                ViewData["CarrierList"] = new SelectList(carrierList, "Id", "Name");

            return View();
        }

        public ActionResult Edit(int id)
        {
            CurrentShippingID = id;

            Shipment shipment = _shipmentBO.GetShipment(CurrentShippingID);
            Affiliate affiliateData = _shipmentBO.GetAffiliate(shipment.AffiliateId);

            if (shipment == null)
            {
                return RedirectToAction("Error", "Home");
            }


            ViewData["txtAffiliateName"] = affiliateData.Name;
            ViewData["txtPickupAddress"] = shipment.PickupAddress;
            ViewData["txtPickupZip"] = shipment.PickupZip;


            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            ViewData["PickupState"] = new SelectList(stateList, "Id", "Name", shipment.PickupStateId );

            List<City> cityList = new List<City>();
            if (shipment.PickupStateId  > 0)
            {
                var data1 = new CostcoService<City>().GetAll(x => x.StateId == shipment.PickupStateId) ;
                cityList = data1.OrderBy(f => f.Name).ToList();
            }
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            ViewData["PickupCity"] = new SelectList(cityList, "Id", "Name", shipment.PickupCityId);


            if (shipment.ShipToAffiliateId != 0)
            {
                //Affiliate recyclerData = _shipmentBO.GetAffiliate((int)affiliateData.DefaultRecycler);

                ViewData["txtRecyclerName"] = shipment.ShipToName; // recyclerData.Name;
                ViewData["txtShipToAddress"] = shipment.ShipToAddress; // recyclerData.Address1;
                ViewData["txtShipToZip"] = shipment.ShipToZip;// recyclerData.Zip;

                var stateListShipTo = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
                stateListShipTo.Insert(0, new State() { Id = 0, Name = "Select" });
                ViewData["ShipToState"] = new SelectList(stateListShipTo, "Id", "Name", shipment.ShipToStateId);

                List<City> cityListShipTo = new List<City>();
                if (shipment.ShipToStateId > 0)
                {
                    var data1 = new CostcoService<City>().GetAll(x => x.StateId == shipment.ShipToStateId);
                    cityListShipTo = data1.OrderBy(f => f.Name).ToList();
                }

                cityListShipTo.Insert(0, new City() { Id = 0, Name = "Select" });
                ViewData["ShipToCity"] = new SelectList(cityListShipTo, "Id", "Name", shipment.ShipToCityId);

            }
            else
            {
                ViewData["txtRecyclerName"] = "";
                ViewData["txtShipToAddress"] = "";
                ViewData["txtShipToZip"] = "";

                var stateListShipTo = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
                stateListShipTo.Insert(0, new State() { Id = 0, Name = "Select" });
                ViewData["ShipToState"] = new SelectList(stateListShipTo, "Id", "Name");

                List<City> cityListShipTo = new List<City>();
                cityListShipTo.Insert(0, new City() { Id = 0, Name = "Select" });
                ViewData["ShipToCity"] = new SelectList(cityListShipTo, "Id", "Name");
            }

            var recyclerList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 5).ToList();
            recyclerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            if (shipment.ShipToAffiliateId != 0)
                ViewData["RecyclerList"] = new SelectList(recyclerList, "Id", "Name", shipment.ShipToAffiliateId);
            else
                ViewData["RecyclerList"] = new SelectList(recyclerList, "Id", "Name");

            var carrierList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 6).ToList();
            carrierList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            if (shipment.CarrierAffiliateId != 0)
                ViewData["CarrierList"] = new SelectList(carrierList, "Id", "Name", shipment.CarrierAffiliateId);
            else
                ViewData["CarrierList"] = new SelectList(carrierList, "Id", "Name");

            ViewData["txtShippedBy"] = shipment.ShippedBy;
            ViewData["txtShipDate"] = String.Format("{0:MM/dd/yyyy}", shipment.ShipmentDate);
            ViewData["txtTotalWeight"] = shipment.ShipmentTotalWeight.ToString();
            ViewData["txtShipComments"] = shipment.ShippingComments;

            ViewData["txtReceivedBy"] = shipment.ReceivedBy;
            ViewData["txtReceivedDate"] = String.Format("{0:MM/dd/yyyy}", shipment.ReceivedDate);
            ViewData["txtTotalReceivedWeight"] = shipment.ReceivedTotalWeight.ToString();
            ViewData["txtSalvageableWeight"] = shipment.SalvageableWeight.ToString();
            ViewData["txtReceivedComments"] = shipment.ReceivingComments;
            ViewData["ShipmentId"] = shipment.Id;
            ViewData["Data"] = Request["data"];
            ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            ViewData["AffiliateId"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
            return View();
        }

        public ActionResult EditShipper(int id)
        {
            CurrentShippingID = id;


            Shipment shipment = _shipmentBO.GetShipment(CurrentShippingID);
            Affiliate affiliateData = _shipmentBO.GetAffiliate(shipment.AffiliateId);

            if (shipment == null)
            {
                return RedirectToAction("Error", "Home");
            }


            ViewData["txtAffiliateName"] = affiliateData.Name;
            ViewData["txtPickupAddress"] = shipment.PickupAddress;
            ViewData["txtPickupZip"] = shipment.PickupZip;


            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            ViewData["PickupState"] = new SelectList(stateList, "Id", "Name", shipment.PickupStateId);

            List<City> cityList = new List<City>();
            if (shipment.PickupStateId > 0)
            {
                var data1 = new CostcoService<City>().GetAll(x => x.StateId == shipment.PickupStateId);
                cityList = data1.OrderBy(f => f.Name).ToList();
            }
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            ViewData["PickupCity"] = new SelectList(cityList, "Id", "Name", shipment.PickupCityId);


            if (shipment.ShipToAffiliateId != 0)
            {
                //Affiliate recyclerData = _shipmentBO.GetAffiliate((int)affiliateData.DefaultRecycler);

                ViewData["txtRecyclerName"] = shipment.ShipToName; // recyclerData.Name;
                ViewData["txtShipToAddress"] = shipment.ShipToAddress; // recyclerData.Address1;
                ViewData["txtShipToZip"] = shipment.ShipToZip;// recyclerData.Zip;

                var stateListShipTo = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
                stateListShipTo.Insert(0, new State() { Id = 0, Name = "Select" });
                ViewData["ShipToState"] = new SelectList(stateListShipTo, "Id", "Name", shipment.ShipToStateId);

                List<City> cityListShipTo = new List<City>();
                if (shipment.ShipToStateId > 0)
                {
                    var data1 = new CostcoService<City>().GetAll(x => x.StateId == shipment.ShipToStateId);
                    cityListShipTo = data1.OrderBy(f => f.Name).ToList();
                }

                cityListShipTo.Insert(0, new City() { Id = 0, Name = "Select" });
                ViewData["ShipToCity"] = new SelectList(cityListShipTo, "Id", "Name", shipment.ShipToCityId);

            }
            else
            {
                ViewData["txtRecyclerName"] = "";
                ViewData["txtShipToAddress"] = "";
                ViewData["txtShipToZip"] = "";

                var stateListShipTo = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
                stateListShipTo.Insert(0, new State() { Id = 0, Name = "Select" });
                ViewData["ShipToState"] = new SelectList(stateListShipTo, "Id", "Name");

                List<City> cityListShipTo = new List<City>();
                cityListShipTo.Insert(0, new City() { Id = 0, Name = "Select" });
                ViewData["ShipToCity"] = new SelectList(cityListShipTo, "Id", "Name");
            }

            var recyclerList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 5).ToList();
            recyclerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            if (shipment.ShipToAffiliateId != 0)
                ViewData["RecyclerList"] = new SelectList(recyclerList, "Id", "Name", shipment.ShipToAffiliateId);
            else
                ViewData["RecyclerList"] = new SelectList(recyclerList, "Id", "Name");

            var carrierList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 6).ToList();
            carrierList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            if (shipment.CarrierAffiliateId != 0)
                ViewData["CarrierList"] = new SelectList(carrierList, "Id", "Name", shipment.CarrierAffiliateId);
            else
                ViewData["CarrierList"] = new SelectList(carrierList, "Id", "Name");

            ViewData["txtShippedBy"] = shipment.ShippedBy;
            ViewData["txtShipDate"] = String.Format("{0:MM/dd/yyyy}", shipment.ShipmentDate);
            //ViewData["txtTotalWeight"] = shipment.ShipmentTotalWeight.ToString();
            ViewData["txtShipComments"] = shipment.ShippingComments;

            ViewData["txtReceivedBy"] = shipment.ReceivedBy;
            ViewData["txtReceivedDate"] = String.Format("{0:MM/dd/yyyy}", shipment.ReceivedDate);
            ViewData["txtTotalReceivedWeight"] = shipment.ReceivedTotalWeight.ToString();
            ViewData["txtReceivedComments"] = shipment.ReceivingComments;
            ViewData["ShipmentId"] = shipment.Id;
            ViewData["Data"] = Request["data"];
            return View();
        }

        [HttpPost]
        public ActionResult Edit(FormCollection form)
        {
            if (AccessRightsShipmentList == null || AccessRightsShipmentList.Update.HasValue == false
                    || AccessRightsShipmentList.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            string strData = string.Empty;
            try
            {
                CurrentShippingID = Convert.ToInt32(form["ShipmentId"]);
                Shipment shipment = _shipmentBO.GetShipment(CurrentShippingID);
                shipment.ReceivedBy = form["txtReceivedBy"];
                shipment.ReceivedDate = Convert.ToDateTime(form["txtReceivedDate"]);
                shipment.ReceivedTotalWeight = Convert.ToInt32(form["txtTotalReceivedWeight"]);
                shipment.SalvageableWeight = Convert.ToInt32(form["txtSalvageableWeight"]);
                shipment.ReceivingComments = form["txtReceivedComments"];

                strData = form["Data"];

                bool saved = _shipmentBO.Update(shipment);

                if (!saved)
                    throw new Exception();

            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("List", new {data = strData});



        }



        [HttpPost]
        public JsonResult SaveRecyclerInfo(int shipmentId, int shipToAffiliateId, string shipToName, string shipToAddress, string shipToZip, int shipToCityId, int shipToStateId )
        {
            if (AccessRightsShipmentList == null || AccessRightsShipmentList.Update.HasValue == false
                    || AccessRightsShipmentList.Update.Value == false)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", shipmentId = 0 });
            }
            try
            {
                Shipment shipment = _shipmentBO.GetShipment(shipmentId);
                if (shipment == null)
                    throw (new Exception("Shipment does not exist"));

                shipment.ShipToAddress = shipToAddress;
                shipment.ShipToName = shipToName;
                shipment.ShipToZip = shipToZip;
                shipment.ShipToStateId = shipToStateId;
                shipment.ShipToCityId = shipToCityId;
                shipment.ShipToAffiliateId = shipToAffiliateId;

                bool saved = _shipmentBO.Update(shipment);

                if (!saved)
                    throw new Exception("An error occurred saving shipment!");

                return Json(new { success = true}, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditShipper(FormCollection form)
        {
            if (AccessRightsShipmentList == null || AccessRightsShipmentList.Update.HasValue == false
                    || AccessRightsShipmentList.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            string strData = string.Empty;
            try
            {
                string test1 = form["ShipmentId"];
                CurrentShippingID = Convert.ToInt32(form["ShipmentId"]);
                Shipment shipment = _shipmentBO.GetShipment(CurrentShippingID);
                shipment.ShipmentDate = Convert.ToDateTime(form["txtShipDate"]);
                shipment.AffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                shipment.PickupAddress = form["txtPickupAddress"];
                shipment.PickupZip = form["txtPickupZip"];
                shipment.PickupStateId = Convert.ToInt32(form["ddlPickupState"]);
                shipment.PickupCityId = Convert.ToInt32(form["ddlPickupCity"]);
                shipment.ShippingComments = form["txtShipComments"];
                shipment.ShippedBy = form["txtShippedBy"];
                shipment.ShipToAddress = form["txtShipToAddress"];
                shipment.ShipToName = form["txtRecyclerName"];
                shipment.ShipToZip = form["txtShipToZip"];
                shipment.ShipToStateId = Convert.ToInt32(form["ddlShipToState"]);
                shipment.ShipToCityId = Convert.ToInt32(form["ddlShipToCity"]);
                shipment.ShipToAffiliateId = Convert.ToInt32(form["ddlRecyclerList"]);
                shipment.ShipmentTotalWeight = Convert.ToInt32(form["txtTotalWeight"]);
                shipment.CarrierAffiliateId = Convert.ToInt32(form["ddlCarrierList"]);
                strData = form["Data"];
                bool saved = _shipmentBO.Update(shipment);
                if (!saved)
                    throw new Exception();
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("List", new { data = strData });
        }

        [HttpGet]
        public ActionResult InitItemGrid() {
            return Content("{rows:[]}");
        }

        [HttpGet]
        public ActionResult List()
        {

            if (AccessRightsShipmentList != null && AccessRightsShipmentList.Read.HasValue
                    && AccessRightsShipmentList.Read.Value)
            {
                //IList<Affiliate> wareHouseList;
                //IList<Affiliate> unSortedList = null;

                //if (SessionParameters.AffiliateTypeId == 4) //warehouse
                //    unSortedList = new CostcoService<Affiliate>().GetAll(x => x.Id == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId)).ToList();
                //else if (SessionParameters.AffiliateTypeId == 3) //distribution
                //    unSortedList = new CostcoService<Affiliate>().GetAll(x => x.ParentId == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) && x.AffiliateTypeId == 4).ToList();
                //else if (SessionParameters.AffiliateTypeId == 1 || SessionParameters.AffiliateTypeId == 2) //admin
                //    unSortedList = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 4).ToList();
                //else if (SessionParameters.AffiliateTypeId == 5)
                //    unSortedList = new CostcoService<Affiliate>().GetAll(x => x.DefaultRecycler == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) && x.AffiliateTypeId == 4).ToList();


                //IEnumerable<Affiliate> enumSorted = unSortedList.OrderBy(f => f.Name);
                //try
                //{
                //    wareHouseList = enumSorted.ToList();
                //}
                //catch (Exception)
                //{
                //    wareHouseList = unSortedList;
                //}

                Affiliate _affiliate = new Affiliate();
                _affiliate.Name = "All";
                _affiliate.Id = -1;
                //wareHouseList.Insert(0, _affiliate);
                //SelectList statesWarehouseList = new SelectList(wareHouseList, "Id", "Name");
                //ViewData["warehouseList"] = statesWarehouseList;
                ViewData["AffiliateType"] = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);

                //ViewData["IsAdmin"] = SessionParameters.AffiliateType;
                ViewData["Data"] = "{" + Request["data"] + "}";
                return View();
            }   
            else
            {
                return RedirectToAction("AccessDenied", "Home");
            }
        }

        [HttpGet]
        public ActionResult GetShipmentItems(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _shipmentBO.GetShipmentItems(pageIndex, pageSize, CurrentShippingID, out totalRecords).AsEnumerable<V_ShipmentItemExtended>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.PalletUnitNumber,
                            m.ItemId,
                            m.Description,
                            m.Weight,
                            m.Quantity,
                            m.Condition,
                            m.Disposition,
                            m.ReceivedQuantity,
                            m.ReceivedCondition,
                            m.ReceivedCondition2,
                            m.CoveredDevice,
                            m.Pulled,
                            m.Price,
                            m.DefaultDescription
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetShipmentItemsShipper(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _shipmentBO.GetShipmentItems(pageIndex, pageSize, CurrentShippingID, out totalRecords).AsEnumerable<V_ShipmentItemExtended>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.Store,
                            m.PalletUnitNumber,
                            m.ItemId,
                            m.Description,
                            m.Weight,
                            m.Quantity,
                            m.Condition,
                            m.CoveredDevice,
                            m.Disposition,
                            m.ShipmentId,
                            m.ShipperPrice
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetShippingDataList(string sidx, string sord, int page, int rows, bool _search, string Warehouses, string BeginDate, string EndDate, string BeginId, string EndId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;

            var data = _shipmentBO.GetPaged(pageIndex,pageSize,Warehouses,BeginDate,EndDate,_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId),  BeginId, EndId, out totalRecords).AsEnumerable<Shipment>();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id,
                        cell = new object[] {
                            m.Id,
                            m.Affiliate.Name,
                            m.ShipToName,
                            String.Format("{0:MM/dd/yyyy}",m.ShipmentDate),
                            String.Format("{0:MM/dd/yyyy}",m.ReceivedDate),
                            m.Id,
                            GetDocsListHtml(m.Id),
                            GetDialogueHtml(m.Id),
                            //m.Id,
                            m.ReadyToShip,
                            m.Invoiceid == null ? 0 : m.Invoiceid,
                            m.Complete,
                            m.Complete
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        private string GetDocsListHtml(int pDataId)
        {
            List<Document> _docs = _shipmentBO.GetDocsByPDataId(pDataId);

            string _markup = string.Empty;

            if (_docs.Count > 0)
            {
                _markup = "<div onclick='GetAttachments(" + pDataId + ")'><img src='../Content/images/icon/sm/paperclip.gif' title='View attachments'></img></div>";
            }

            return _markup;
        }
        private string GetDialogueHtml(int pDataId)
        {

            return "<div onclick='javascript:openFileDialog(" + pDataId + ")'><img src='../Content/images/icon/sm/attach_file.gif' title='Attach File' /></div>";
        }

        public ActionResult GetShipToAddress(int id)
        {
            Affiliate shipTo = new CostcoService<Affiliate>().GetSingle(x => x.Id == id);
            if (shipTo == null)
            {
                var jsonData = new
                {
                    address = string.Empty,
                    state = 0,
                    city = 0,
                    zip = string.Empty
                };
                return Json(jsonData);
            }
            else
            {
                var jsonData = new
                {
                    address = shipTo.Address1,
                    state = shipTo.StateId,
                    city = shipTo.CityId,
                    zip = shipTo.Zip
                };
                return Json(jsonData);
            }
        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            if (AccessRightsShipmentCreate == null || AccessRightsShipmentCreate.Add.HasValue == false
                    || AccessRightsShipmentCreate.Add.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            Int32 shipmentId = 0;
            try
            {
                Shipment shipment = new Shipment();
                shipment.ShipmentDate = Convert.ToDateTime(form["txtShipDate"]);
                shipment.AffiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
                shipment.PickupAddress = form["txtPickupAddress"];
                shipment.PickupZip = form["txtPickupZip"];
                shipment.PickupStateId = Convert.ToInt32(form["ddlPickupState"]);
                shipment.PickupCityId = Convert.ToInt32(form["ddlPickupCity"]);
                shipment.ShippingComments = form["txtShipComments"];
                shipment.ShippedBy = form["txtShippedBy"];
                shipment.ShipToAddress = form["txtShipToAddress"];
                shipment.ShipToName = form["txtRecyclerName"];
                shipment.ShipToZip = form["txtShipToZip"];
                shipment.ShipToStateId = Convert.ToInt32(form["ddlShipToState"]);
                shipment.ShipToCityId = Convert.ToInt32(form["ddlShipToCity"]);
                shipment.ShipToAffiliateId = Convert.ToInt32(form["ddlRecyclerList"]);
                shipment.ShipmentTotalWeight = Convert.ToInt32(form["txtTotalWeight"]);
                shipment.CarrierAffiliateId = Convert.ToInt32(form["ddlCarrierList"]);

                //List<ShipmentItem> shipmentItems = FillShipmentItems(form);
                //bool saved = _shipmentBO.Save(shipment, shipmentItems);
                //if (!saved)
                //    throw new Exception();

                shipmentId = _shipmentBO.Save(shipment);
                if (shipmentId == 0)
                    throw new Exception();

            }
            catch (Exception)
            {

                return RedirectToAction("Error", "Home");
            }
            //return RedirectToAction("List");
            return RedirectToAction("EditShipper", new { id = shipmentId });
        }

        private List<ShipmentItem> FillShipmentItems(FormCollection form)
        {
            List<ShipmentItem> shipmentItems = new List<ShipmentItem>();
            ShipmentItem item = null;
            JavaScriptSerializer JSS = new JavaScriptSerializer();

            string key = string.Empty;
            string value = string.Empty;

            for (int i = 1; i <= form.AllKeys.Length; i++)
            {
                key = "Item_" + i.ToString();
                if (form[key] != null)
                {
                    value = form[key];
                    string[] jsonCollection = value.Split(new char[] { ',' });
                    item = new ShipmentItem();

                    foreach (var jsonItem in jsonCollection)
                    {
                        string[] clientitem = jsonItem.Split(new char[] { ':' });
                        switch (clientitem[0])
                        {
                            case "ShipmentItemID":
                                item.Id = Convert.ToInt32(clientitem[1]);
                                break;
                            case "PalletUnitNumber":
                                item.PalletUnitNumber = clientitem[1];
                                break;
                            case "ItemId":
                                item.ItemId = clientitem[1];
                                break;
                            case "Description":
                                item.Description = clientitem[1];
                                break;
                            case "Qty":
                                item.Quantity = Convert.ToInt32(clientitem[1]);
                                break;
                            case "Condition":
                                item.Condition =clientitem[1];
                                break;
                            case "CoveredDevice":
                                item.CoveredDevice = Convert.ToBoolean(clientitem[1]);
                                break;
                            case "Disposition":
                                item.Disposition = clientitem[1];
                                break;
                            case "Weight":
                                item.Weight = Convert.ToDecimal(clientitem[1]);
                                break;
                        }
                    }

                    shipmentItems.Add(item);

                }
            }
            return shipmentItems;
        }

        public ActionResult SaveAttachment(string fileName, string processDataId)
        {
            try
            {

                string path = Server.MapPath("~/Uploads/" + fileName);
                String saveLocation = Server.MapPath("~/Uploads/") + Guid.NewGuid();
                Document doc = new Document();

                doc.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                doc.ServerFileName = saveLocation;
                doc.UploadFileName = fileName;
                doc.UploadDate = System.DateTime.Now;

                if (!string.IsNullOrEmpty(processDataId))
                {
                    DocumentRelation docRel = new DocumentRelation();
                    docRel.DocumentID = _shipmentBO.SaveAttachment(doc);
                    docRel.ForeignID = Convert.ToInt32(processDataId);
                    _shipmentBO.SaveAttachmentRelation(docRel);
                }

                return Json("True", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
            //return null;
        }

        public ActionResult GetAttachments(string processDataId)
        {
            List<Document> _docs = _shipmentBO.GetDocsByPDataId(Convert.ToInt32(processDataId));

            var jsonData = new
            {
                rows = (
                    from d in _docs
                    select new
                    {
                        DocumentID = d.Id,
                        UploadFileName = d.UploadFileName.Replace(" ", "&nbsp;")
                    }).ToList()

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditShipmentItem(FormCollection col)
        {
            if (AccessRightsShipmentList == null || AccessRightsShipmentList.Update.HasValue == false
                    || AccessRightsShipmentList.Update.Value == false)
            {
                return Json(new { status = false}, JsonRequestBehavior.AllowGet);
            }

            bool saved = false;
            if (col.AllKeys.Contains("oper"))
            {
                if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                {
                    int currentShipmentItemID = Convert.ToInt32(col["ShipmentItemID"]);
                    int qtyReceived = Convert.ToInt32(col["QtyReceived"]);
                    int qtyPulled = Convert.ToInt32(col["Pulled"]);
                    
                    string Description = col["Description"];
                    string newDescription = col["DefaultDescription"];
                    //string ItemId = "MISC";

                    decimal newPrice = Convert.ToDecimal(col["Price"]);
                    



                    ShipmentItem shipmentItem = _shipmentBO.GetShipmentItem(currentShipmentItemID);
                    
                    if (shipmentItem != null)
                    {
                        if (qtyReceived > shipmentItem.Quantity || qtyReceived < 0)
                            return Json(new { status = false, message="Received Qty cannot be more than shipped Qty." }, JsonRequestBehavior.AllowGet);
                        if (qtyPulled > qtyReceived || qtyPulled < 0)
                            return Json(new { status = false, message = "Pulled Qty cannot be more than Received Qty." }, JsonRequestBehavior.AllowGet);

                        shipmentItem.ReceivedQuantity = qtyReceived;
                        shipmentItem.Pulled = qtyPulled;
                        shipmentItem.ReceivedCondition = Convert.ToInt32(col["ConditionReceived"]);
                        shipmentItem.ReceivedCondition2 = col["ConditionReceived2"];
                        //shipmentItem.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
                        saved = _shipmentBO.UpdateItem(shipmentItem);

                        if (_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) == 533 && shipmentItem.ItemId.ToString().ToUpper() != "MISC" && shipmentItem.ItemId.ToString().ToUpper() != "UNKNOWN")
                        {
                            //Product product = _productBO.GetProduct(shipmentItem.ItemId);
                            Product product = new CostcoService<Product>().GetSingle(x => x.ItemId == shipmentItem.ItemId);
                            if (product.Description != newDescription)
                            {
                                product.Description = newDescription;
                                _productBO.UpdateProduct(product);

                                ProductDescription pd = new ProductDescription();
                                pd.ItemId = shipmentItem.ItemId;
                                pd.Description = newDescription;
                                pd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                                pd.Timestamp = DateTime.Now;
                                _productBO.AddProductDescription(pd);

                            }

                            v_MaxProductPrice price = _productBO.GetProductPriceByItemId(shipmentItem.ItemId);
                            if (price.Price != newPrice)
                            {
                                ProductPrice productPrice = new ProductPrice();
                                productPrice.ItemId = shipmentItem.ItemId;
                                productPrice.Price = newPrice;
                                productPrice.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                                productPrice.TimeStamp = DateTime.Now;
                                _productBO.AddProductPrice(productPrice);
                            }



                        }


                    }

                }
            }
            return Json(new { status = saved, message = "Error occured updating item." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteShipmentItem(int shipmentItemId)
        {
            try
            {
                _shipmentBO.DeleteShipmentItem(shipmentItemId);
                return Json(new { status = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }


        [HttpPost]
        public JsonResult EditShipmentItemShipper(FormCollection col)
        {
            if (AccessRightsShipmentList == null || AccessRightsShipmentList.Update.HasValue == false
                    || AccessRightsShipmentList.Update.Value == false)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", shipmentId = 0 }, JsonRequestBehavior.AllowGet);
            }
            ShipmentItem shipmentItem = null;
            Int32 shipmentId = 0;
            bool saved = false;
            try
            {
                if (col.AllKeys.Contains("oper"))
                {
                    decimal newPrice = 0;
                    if (col["Price"] != null) 
                    {
                        string strPrice = col["Price"];
                        decimal result;
                        if (decimal.TryParse(strPrice, out result))
                            newPrice = result;
                    }
                    if (col["oper"] == "add")
                    {

                        
                        shipmentItem = new ShipmentItem();
                        shipmentItem.PalletUnitNumber = col["PalletUnitNumber"];
                        shipmentItem.ItemId = col["ItemId"];

                        //string shipmentid = col["ItemsShipmentId"];
                        string storenumber = col["Store"];
                        if (storenumber != null && storenumber.Trim() != "")
                            shipmentItem.Store = Convert.ToInt32(storenumber);

                        shipmentItem.ShipmentId = Convert.ToInt32(col["ItemsShipmentId"]);
                        
                        shipmentItem.Description = col["Description"];
                        shipmentItem.Weight = Convert.ToDecimal(col["Weight"]);
                        shipmentItem.Quantity = Convert.ToInt32(col["Qty"]);
                        shipmentItem.Condition = col["Condition"];
                        shipmentItem.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
                        shipmentItem.Disposition = GetDisposition(col["ItemId"], col["Disposition"]);
                        shipmentItem.ShipperPrice = newPrice;
                        shipmentId = _shipmentBO.AddShipmentItem(shipmentItem);
                    }
                    else if (col["oper"] == "edit" && col.AllKeys.Contains("id") && col["id"] != "")
                    {
                        int currentShipmentItemID = Convert.ToInt32(col["id"]);
                        shipmentItem = _shipmentBO.GetShipmentItem(currentShipmentItemID);
                        if (shipmentItem != null)
                        {
                            string storenumber = col["Store"];
                            if (storenumber != null && storenumber.Trim() != "")
                                shipmentItem.Store = Convert.ToInt32(storenumber);
                            shipmentItem.PalletUnitNumber = col["PalletUnitNumber"];
                            shipmentItem.ItemId = col["ItemId"];
                            shipmentItem.Description = col["Description"]; 
                            shipmentItem.Weight = Convert.ToDecimal( col["Weight"]);
                            shipmentItem.Quantity = Convert.ToInt32(col["Qty"]);
                            shipmentItem.Condition = col["Condition"];
                            shipmentItem.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
                            shipmentItem.Disposition = GetDisposition(col["ItemId"], col["Disposition"]);
                            shipmentItem.ShipperPrice = newPrice;
                            saved = _shipmentBO.UpdateShipmentItem(shipmentItem);
                        }
                    }
                    Product product = new Product();
                    product.ItemId = col["ItemId"];
                    product.Description = col["Description"];
                    product.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
                    product.Weight = Convert.ToDecimal(col["Weight"]);
                    _shipmentBO.AddProduct(product);


                    if (shipmentItem.ItemId.ToString().ToUpper() != "MISC" && shipmentItem.ItemId.ToString().ToUpper() != "UNKNOWN")
                    {
                        v_MaxProductPrice price = _productBO.GetProductPriceByItemId(shipmentItem.ItemId);
                        if (price.Price != newPrice && newPrice != 0)
                        {
                            ProductPrice productPrice = new ProductPrice();
                            productPrice.ItemId = shipmentItem.ItemId;
                            productPrice.Price = newPrice;
                            productPrice.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                            productPrice.TimeStamp = DateTime.Now;
                            _productBO.AddProductPrice(productPrice);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", shipmentId = 0 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, shipmentId = shipmentId, disposition= shipmentItem.Disposition }, JsonRequestBehavior.AllowGet);

        }

        private string GetDisposition(string itemid, string disposition)
        {
            string productDisposition = disposition;
            ProductDisposition pd = new CostcoService<ProductDisposition>().GetSingle(x => x.ItemId == itemid);
            if (pd != null)
                productDisposition = pd.Disposition;

            return productDisposition;
        
        }

        public ActionResult GetDropDownsData()
        {
            string strReceivedCondition = ":Select;";
            foreach (ConditionType s in _shipmentBO.GetConditionTypes())
            {
                strReceivedCondition += s.Id + ":" + s.Condition + ";";
            }

            var jsonData = new
            {
                receivedConditions = strReceivedCondition.TrimEnd(';'),
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddProductItem(FormCollection col)
        {
            string msg = String.Empty;
            Product product = new Product();
            product.ItemId = col["ItemId"];
            product.Description = col["Description"];
            product.CoveredDevice = Convert.ToBoolean(col["CoveredDevice"]);
            product.Weight = Convert.ToDecimal(col["Weight"]);
            _shipmentBO.AddProduct(product);
            return Json(new { success = true, message = msg });
        }


        [HttpGet]
        public ActionResult GetProductDescription(string itemId )
        {
            string description = String.Empty;
            bool? coveredDevice = null;
            decimal weight = 0;
            string disposition = String.Empty;
            decimal price = 0;
            //Product product = _shipmentBO.GetProduct(itemId);
            v_MaxProductPrice product = _productBO.GetProductPriceByItemId(itemId);
            if (product != null)
            {
                description = product.Description;
                coveredDevice = product.CoveredDevice; //!= null ? (bool)product.CoveredDevice : false;
                weight = Convert.ToDecimal(product.Weight);
                disposition = GetDisposition(product.ItemId, disposition);
                price = (decimal)product.Price;

            }
            var jsonData = new
            {
                description = description,
                coveredDevice = coveredDevice,
                weight = weight,
                disposition = disposition,
                price = price
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateReadyToShip(int shipmentId, DateTime shipmentDate)
        {
            if (AccessRightsShipmentList == null || AccessRightsShipmentList.Update.HasValue == false
                    || AccessRightsShipmentList.Update.Value == false)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", shipmentId = 0 });
            }
            try
            {
                Shipment shipment = _shipmentBO.GetShipment(shipmentId);
                if (shipment == null)
                    throw (new Exception("Shipment does not exist"));

                shipment.ShipmentDate = shipmentDate;
                shipment.ReadyToShip = true;
                bool saved = _shipmentBO.Update(shipment);

                if (!saved)
                    throw new Exception("An error occurred saving shipment!");

                return Json(new { success = true, shipmentDate = String.Format("{0:MM/dd/yyyy}", shipment.ShipmentDate) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult RevertReadyToShip(int shipmentId)
        {
            if (AccessRightsShipmentList == null || AccessRightsShipmentList.Update.HasValue == false
                    || AccessRightsShipmentList.Update.Value == false)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", shipmentId = 0 });
            }
            try
            {
                Shipment shipment = _shipmentBO.GetShipment(shipmentId);
                if (shipment == null)
                    throw (new Exception("Shipment does not exist"));

                if (shipment.ReceivedDate != null)
                    throw (new Exception("Cannot revert a shipment that has been received."));


                shipment.ReadyToShip = false;
                bool saved = _shipmentBO.Update(shipment);

                if (!saved)
                    throw new Exception("An error occurred saving shipment!");

                return Json(new { success = true, shipmentDate = String.Format("{0:MM/dd/yyyy}", shipment.ShipmentDate) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateComplete(Int32 id)
        {
            string msg = String.Empty;
            if (AccessRightsShipmentList != null && AccessRightsShipmentList.Update.HasValue == true && AccessRightsShipmentList.Update == true)
            {
                //msg = processingDataBO.UpdateVerifyByInvoiceId(id);
                msg = _shipmentBO.UpdateCompleteById(id);
            }
            else
            {
                msg = "Access Denied";
            }

            if (msg == "Success")
            {
                Shipment shipment = _shipmentBO.GetShipment(id);

                return Json(new { success = true, message = msg, complete = shipment.Complete });
            }
            else
                return Json(new { success = false, message = msg });

        }

        [HttpPost]
        public JsonResult DeleteShipment(int ShipmentId)
        {
            if (AccessRightsShipmentList == null || AccessRightsShipmentList.Delete.HasValue == false
                    || AccessRightsShipmentList.Delete.Value == false)
            {
                return Json(new { success = false, message = "An error occurred, try logging out and back in.", shipmentId = 0 });
            }
            try
            {
                Shipment shipment = _shipmentBO.GetShipment(ShipmentId);
                if (shipment == null)
                    throw (new Exception("Shipment does not exist"));
                if (shipment.Invoiceid != null)
                    throw (new Exception("Cannot delete because already invoiced."));

                CostcoEntities db = new CostcoEntities();
                List<SalesOrderItem> sales = db.SalesOrderItems.Where(x => x.ShipmentId == ShipmentId).ToList<SalesOrderItem>() ;
                if (sales.Count > 0)
                    throw (new Exception("Cannot delete because already assigned to Sales Orders"));


                int totalRecords;
                var data = _shipmentBO.GetShipmentItems(0, 100000, ShipmentId, out totalRecords).AsEnumerable<V_ShipmentItemExtended>();

                foreach (V_ShipmentItemExtended item in data)
                {
                    _shipmentBO.DeleteShipmentItem(item.Id);
                    
                }

                _shipmentBO.DeleteShipment(shipment);

                return Json(new { success = true, shipmentDate = String.Format("{0:MM/dd/yyyy}", shipment.ShipmentDate) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
