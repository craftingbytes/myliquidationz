﻿using System;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL;
using Costco.DAL.EntityModels;
using Costco.Common;
using Costco.BLL.Authorization;

namespace Costco.Web.Controllers
{
    public class ReportListController : CostcoApplicationController
    {
        //
        // GET: /ReportList/

        public ReportListController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
           
        }

        private AccessRights AccessRightsUserReportList
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "UserReportList");
            }
        }
        public ActionResult Index()
        {
            if (AccessRightsUserReportList == null || AccessRightsUserReportList.Read.HasValue == false
                    || AccessRightsUserReportList.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            return View();
        }
        public ActionResult Edit()
        {
            if (AccessRightsUserReportList == null || AccessRightsUserReportList.Read.HasValue == false
                    || AccessRightsUserReportList.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            return View();
        }
        [HttpGet]
        public ActionResult GetReportList(string sidx, string sord, int page, int rows)
        {
            if (AccessRightsUserReportList == null || AccessRightsUserReportList.Read.HasValue == false
                    || AccessRightsUserReportList.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            //int totalRecords;


            CostcoEntities db = new CostcoEntities();
            var data = db.p_GetUserReportList(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId));


            int totalPages = 1;
            var jsonData = new
            {
                total = 1,
                page = 1,
                records = 100,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.Id_m,
                        cell = new object[] {  
                            m.Id_m,
                            m.URL_1 == "#" ? "" : "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + m.URL_1 + "\" href='#' >" + m.Caption_1 + "</a>" ,    
                            m.Id_1,
                            m.URL_2 == "#" ? "" : "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + m.URL_2 + "\" href='#' >" + m.Caption_2 + "</a>" , 
                            m.Id_2,
                            m.URL_3 == "#" ? "" : "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + m.URL_3 + "\" href='#' >" + m.Caption_3 + "</a>",
                            m.Id_3,
                            m.URL_m == "#" ? "" : "<a style='color:#1E78A5;font-size:11px;font-weight:bold;' onclick=\"" + m.URL_m + "\" href='#' >" + m.Caption_m + "</a>",
                            m.Assigned
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult AddReportToGroup(Int32 ReportId, Int32 GroupId)
        {
            if (AccessRightsUserReportList == null || AccessRightsUserReportList.Add.HasValue == false
                    || AccessRightsUserReportList.Add.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CostcoEntities db = new CostcoEntities();
            db.p_InsertUserReport(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId), ReportId, GroupId);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteReportFromGroup(Int32 ReportId)
        {
            if (AccessRightsUserReportList == null || AccessRightsUserReportList.Update.HasValue == false
                    || AccessRightsUserReportList.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            CostcoService<UserReportList> ulsvc = new CostcoService<UserReportList>();
            UserReportList report = ulsvc.GetSingle(x => x.AffiliateContactId == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId) && x.ReportId == ReportId);
            ulsvc.Delete(report);


            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateDisplayOrder(Int32 UserReportId, int Direction)
        {
            if (AccessRightsUserReportList == null || AccessRightsUserReportList.Update.HasValue == false
                    || AccessRightsUserReportList.Update.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            CostcoEntities db = new CostcoEntities();
            db.p_ChangeDisplayOrder(UserReportId, Direction); 
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
