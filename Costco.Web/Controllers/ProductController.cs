﻿using System;
using System.Linq;
using System.Web.Mvc;
using Costco.BLL;
using Costco.BLL.BusinessObjects;
using Costco.DAL.EntityModels;
using Costco.Common;
using Costco.BLL.Authorization;
namespace Costco.Web.Controllers
{
    public class ProductController : CostcoApplicationController
    {
        //
        // GET: /Product/
        private ProductBO _productBO;

        private AccessRights AccessRightsProduct
        {
            get
            {
                // Authorization authorization = new Authorization();
                return Authorization.GetRights(_sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateRoleId), "Product");
            }
        }

        public ProductController(ISessionCookieValues sessionValues)
            : base(sessionValues)
        {
            _productBO = new ProductBO(_sessionValues);
        }

        public ActionResult Index()
        {
            if (AccessRightsProduct == null || AccessRightsProduct.Read.HasValue == false
                    || AccessRightsProduct.Read.Value == false)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            int typeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            if (typeid == 5 && _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) != 533)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            ViewData["AffiliateType"] = typeid;

            var productTypeList = new CostcoService<ProductType>().GetAll().ToList();
            productTypeList.Insert(0, new ProductType() { Id = 99, Name = "All" });
            productTypeList.Insert(0, new ProductType() { Id = 0, Name = "Not Set" });
            ViewData["ProductTypeList"] = new SelectList(productTypeList, "Id", "Name",99);
            return View();
        }

        [HttpGet]
        public ActionResult GetProductList(string sidx, string sord, int page, int rows, string SearchString, int ProductTypeId, bool PriceNotSet, bool WeightNotSet)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords;


            var data = _productBO.SearchProducts(pageIndex, pageSize, SearchString, ProductTypeId, PriceNotSet, WeightNotSet, out totalRecords).AsEnumerable<v_MaxProductPrice>();


            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from m in data
                    select new
                    {
                        id = m.ItemId,
                        cell = new object[] {
                            m.ItemId,
                            m.Description,
                            m.Weight,
                            m.Price,
                            m.DefaultDisposition,
                            m.ProductType,
                            m.Count
                        }
                    }).ToArray()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetProductTypes()
        {
            
            string strProductType = "0:Not Set;";
            foreach (ProductType p in _productBO.GetProductTypes())
            {
                strProductType += p.Id + ":" + p.Name + ";";
            }

            var jsonData = new
            {
                productTypes = strProductType.TrimEnd(';'),
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateProductCell(FormCollection col)
        {
            if (AccessRightsProduct == null || AccessRightsProduct.Update.HasValue == false
                    || AccessRightsProduct.Update.Value == false)
            {
                return Json(new { success = false, message = "Access denied" });
            }
            string msg = String.Empty;
            bool success = false;
            if (col.AllKeys.Contains("Price"))
            {
                
                string itemid = col["id"];
                itemid.Trim();
                int intItemId = 0;

                if ((itemid.Length == 6 || itemid.Length == 5) && int.TryParse(itemid, out intItemId))
                {
                    ProductPrice pp = new ProductPrice();
                    pp.ItemId = col["id"];
                    pp.Price = Convert.ToDecimal(col["Price"]);
                    pp.TimeStamp = DateTime.Now;
                    pp.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);


                    int ppId = _productBO.AddProductPrice(pp);
                    if (ppId == 0)
                    {
                        msg = "Error adding price";
                        success = false;
                        //return Json(new { success = false, message = msg });
                    }
                    else
                        success = true;
                }
                else
                {
                    msg = "Cannot set price of invalid ItemId";
                    success = false;
                }
                return Json(new { success = success, message = msg });
            }
            else if (col.AllKeys.Contains("Disposition"))
            {
                success = _productBO.UpdateProductDisposition(col["id"], col["Disposition"]);
            }
            else if (col.AllKeys.Contains("id"))
            {
                success = true;
                string itemId = col["id"];

                Product product = new CostcoService<Product>().GetSingle(x => x.ItemId == itemId);
                if (col.AllKeys.Contains("Description"))
                {
                    int intItemId;
                    if ((product.ItemId.Length == 6 || product.ItemId.Length == 5) && int.TryParse(itemId, out intItemId))
                    {
                        product.Description = col["Description"];

                        ProductDescription pd = new ProductDescription();
                        pd.ItemId = itemId;
                        pd.Description = col["Description"];
                        pd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                        pd.Timestamp = DateTime.Now;
                        int pdId = _productBO.AddProductDescription(pd);
                        if (pdId == 0)
                        {
                            msg = "Error updating description";
                            success = false;
                            //return Json(new { success = false, message = msg });
                        }
                        else
                            success = true;
                    }
                    else
                    {
                        msg = "Cannot set description of invalid ItemId";
                        success = false;
                        return Json(new { success = success, message = msg });
                    }

                }
                else if (col.AllKeys.Contains("Weight"))
                    product.Weight = Convert.ToDecimal(col["Weight"]);
                else if (col.AllKeys.Contains("Type"))
                {
                    int productTypeId = Convert.ToInt16(col["Type"]);
                    if (productTypeId > 0)
                        product.ProductTypeId = productTypeId;
                    else
                        product.ProductTypeId = null;
                }
                else
                {
                    success = false;
                    msg = "Nothing to update";
                }
                if (success)
                    success = _productBO.UpdateProduct(product);
                

            }

            if (!success)
                    msg = "Error occured updating product";

            return Json(new { success = success, message = msg });

        }

    }
}
