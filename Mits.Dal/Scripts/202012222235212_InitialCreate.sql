﻿CREATE TABLE [dbo].[AccreditationType] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](75),
    [Description] [varchar](300),
    CONSTRAINT [PK_dbo.AccreditationType] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[AffiliateAccreditation] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateId] [int],
    [AccreditationTypeId] [int],
    [Active] [bit],
    CONSTRAINT [PK_dbo.AffiliateAccreditation] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateId] ON [dbo].[AffiliateAccreditation]([AffiliateId])
CREATE INDEX [IX_AccreditationTypeId] ON [dbo].[AffiliateAccreditation]([AccreditationTypeId])
CREATE TABLE [dbo].[Affiliate] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](200),
    [Address1] [varchar](200),
    [Address2] [varchar](200),
    [Phone] [varchar](20),
    [Fax] [varchar](50),
    [Email] [varchar](50),
    [WebSite] [varchar](50),
    [Longitude] [decimal](9, 6),
    [Latitude] [decimal](9, 6),
    [Active] [bit],
    [StateId] [int],
    [CityId] [int],
    [Zip] [varchar](11),
    [AffiliateTypeId] [int],
    [PaymentTermId] [int],
    [Notes] [nvarchar](2048),
    [Directions] [nvarchar](2048),
    [BusinessHours] [nvarchar](2048),
    CONSTRAINT [PK_dbo.Affiliate] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[Affiliate]([StateId])
CREATE INDEX [IX_CityId] ON [dbo].[Affiliate]([CityId])
CREATE INDEX [IX_AffiliateTypeId] ON [dbo].[Affiliate]([AffiliateTypeId])
CREATE TABLE [dbo].[AffiliateContact] (
    [Id] [int] NOT NULL IDENTITY,
    [FirstName] [varchar](100),
    [LastName] [varchar](100),
    [MiddleInitials] [varchar](3),
    [Address1] [varchar](200),
    [Address2] [varchar](200),
    [Phone] [varchar](20),
    [Fax] [varchar](50),
    [UserName] [varchar](100),
    [Email] [varchar](50),
    [Password] [nvarchar](150),
    [Active] [bit],
    [StateId] [int],
    [CityId] [int],
    [Zip] [varchar](11),
    [AffiliateId] [int],
    [SecurityQuestionId] [int],
    [Answer] [varchar](500),
    [LoginTryCount] [int],
    [LastLoginTime] [smalldatetime],
    [Title] [varchar](100),
    [Notes] [nvarchar](200),
    [RoleId] [int],
    CONSTRAINT [PK_dbo.AffiliateContact] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[AffiliateContact]([StateId])
CREATE INDEX [IX_CityId] ON [dbo].[AffiliateContact]([CityId])
CREATE INDEX [IX_AffiliateId] ON [dbo].[AffiliateContact]([AffiliateId])
CREATE INDEX [IX_SecurityQuestionId] ON [dbo].[AffiliateContact]([SecurityQuestionId])
CREATE INDEX [IX_RoleId] ON [dbo].[AffiliateContact]([RoleId])
CREATE TABLE [dbo].[AffiliateContactService] (
    [Id] [int] NOT NULL IDENTITY,
    [AssociationPriority] [int],
    [AssociationExclusive] [bit],
    [ServiceTypeId] [int] NOT NULL,
    [AffiliateContactId] [int] NOT NULL,
    [Active] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.AffiliateContactService] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_ServiceTypeId] ON [dbo].[AffiliateContactService]([ServiceTypeId])
CREATE INDEX [IX_AffiliateContactId] ON [dbo].[AffiliateContactService]([AffiliateContactId])
CREATE TABLE [dbo].[ServiceType] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](100) NOT NULL,
    [Description] [varchar](1000),
    CONSTRAINT [PK_dbo.ServiceType] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[AffiliateContractRate] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int],
    [AffiliateId] [int],
    [ServiceTypeId] [int],
    [ProductTypeId] [int],
    [QuantityTypeId] [int],
    [PriceUnitTypeId] [int],
    [RateStartDate] [datetime],
    [RateEndDate] [datetime],
    [Rate] [money],
    [QtyAdjustPercent] [decimal](4, 3),
    [PlanYear] [int],
    CONSTRAINT [PK_dbo.AffiliateContractRate] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[AffiliateContractRate]([StateId])
CREATE INDEX [IX_AffiliateId] ON [dbo].[AffiliateContractRate]([AffiliateId])
CREATE INDEX [IX_ServiceTypeId] ON [dbo].[AffiliateContractRate]([ServiceTypeId])
CREATE INDEX [IX_ProductTypeId] ON [dbo].[AffiliateContractRate]([ProductTypeId])
CREATE INDEX [IX_QuantityTypeId] ON [dbo].[AffiliateContractRate]([QuantityTypeId])
CREATE INDEX [IX_PriceUnitTypeId] ON [dbo].[AffiliateContractRate]([PriceUnitTypeId])
CREATE TABLE [dbo].[PriceUnitType] (
    [Id] [int] NOT NULL IDENTITY,
    [Type] [varchar](75) NOT NULL,
    [Description] [nvarchar](300),
    CONSTRAINT [PK_dbo.PriceUnitType] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[ProductType] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](75),
    [Description] [nvarchar](500),
    [Notes] [varchar](300),
    [SiteImageID] [int],
    [EstimatedWeight] [smallmoney],
    [WeightQuantityTypeID] [int],
    [EstimatedItemsPerContainer] [int],
    [ContainerQuantityTypeId] [int],
    [ConsumerSelectable] [bit],
    CONSTRAINT [PK_dbo.ProductType] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_SiteImageID] ON [dbo].[ProductType]([SiteImageID])
CREATE INDEX [IX_WeightQuantityTypeID] ON [dbo].[ProductType]([WeightQuantityTypeID])
CREATE INDEX [IX_ContainerQuantityTypeId] ON [dbo].[ProductType]([ContainerQuantityTypeId])
CREATE TABLE [dbo].[AffiliateTargetProduct] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateTargetId] [int],
    [ProductTypeId] [int],
    CONSTRAINT [PK_dbo.AffiliateTargetProduct] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateTargetId] ON [dbo].[AffiliateTargetProduct]([AffiliateTargetId])
CREATE INDEX [IX_ProductTypeId] ON [dbo].[AffiliateTargetProduct]([ProductTypeId])
CREATE TABLE [dbo].[AffiliateTarget] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int],
    [PolicyId] [int],
    [ServiceTypeId] [int],
    [Weight] [decimal](18, 0),
    [QuantityTypeId] [int],
    [StartDate] [datetime],
    [EndDate] [date],
    [AffiliateId] [int],
    [PlanYear] [int],
    [IsPermanentDropOffLocation] [bit],
    [Pace] [int],
    CONSTRAINT [PK_dbo.AffiliateTarget] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[AffiliateTarget]([StateId])
CREATE INDEX [IX_PolicyId] ON [dbo].[AffiliateTarget]([PolicyId])
CREATE INDEX [IX_ServiceTypeId] ON [dbo].[AffiliateTarget]([ServiceTypeId])
CREATE INDEX [IX_QuantityTypeId] ON [dbo].[AffiliateTarget]([QuantityTypeId])
CREATE INDEX [IX_AffiliateId] ON [dbo].[AffiliateTarget]([AffiliateId])
CREATE TABLE [dbo].[Policy] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](100),
    [Description] [varchar](1000),
    CONSTRAINT [PK_dbo.Policy] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[ManufacturerPolicy] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateId] [int],
    [StateId] [int],
    [PolicyId] [int],
    [Active] [bit],
    CONSTRAINT [PK_dbo.ManufacturerPolicy] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateId] ON [dbo].[ManufacturerPolicy]([AffiliateId])
CREATE INDEX [IX_StateId] ON [dbo].[ManufacturerPolicy]([StateId])
CREATE INDEX [IX_PolicyId] ON [dbo].[ManufacturerPolicy]([PolicyId])
CREATE TABLE [dbo].[State] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](50),
    [Abbreviation] [char](2),
    [CountryId] [int],
    CONSTRAINT [PK_dbo.State] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_CountryId] ON [dbo].[State]([CountryId])
CREATE TABLE [dbo].[AffiliateState] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateId] [int] NOT NULL,
    [StateId] [int] NOT NULL,
    [Active] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.AffiliateState] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateId] ON [dbo].[AffiliateState]([AffiliateId])
CREATE INDEX [IX_StateId] ON [dbo].[AffiliateState]([StateId])
CREATE TABLE [dbo].[AuditReports] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int],
    [FacilityName] [nvarchar](100),
    [AuditDate] [datetime],
    [PlanYear] [int],
    CONSTRAINT [PK_dbo.AuditReports] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[AuditReports]([StateId])
CREATE TABLE [dbo].[City] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](50),
    [StateId] [int],
    [CountyId] [int],
    CONSTRAINT [PK_dbo.City] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[City]([StateId])
CREATE INDEX [IX_CountyId] ON [dbo].[City]([CountyId])
CREATE TABLE [dbo].[ClientAddress] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateId] [int] NOT NULL,
    [CompanyName] [varchar](250),
    [Address] [varchar](200),
    [CityId] [int],
    [StateId] [int],
    [Zip] [varchar](10),
    [Phone] [varchar](20),
    CONSTRAINT [PK_dbo.ClientAddress] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateId] ON [dbo].[ClientAddress]([AffiliateId])
CREATE INDEX [IX_CityId] ON [dbo].[ClientAddress]([CityId])
CREATE INDEX [IX_StateId] ON [dbo].[ClientAddress]([StateId])
CREATE TABLE [dbo].[PickUpAddress] (
    [Id] [int] NOT NULL IDENTITY,
    [ClientAddressId] [int] NOT NULL,
    [CompanyName] [varchar](250),
    [Address] [varchar](200),
    [StateId] [int],
    [CityId] [int],
    [Zip] [varchar](10),
    [Phone] [varchar](20),
    [Fax] [varchar](20),
    [Email] [varchar](100),
    [CollectionMethodId] [int],
    CONSTRAINT [PK_dbo.PickUpAddress] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_ClientAddressId] ON [dbo].[PickUpAddress]([ClientAddressId])
CREATE INDEX [IX_StateId] ON [dbo].[PickUpAddress]([StateId])
CREATE INDEX [IX_CityId] ON [dbo].[PickUpAddress]([CityId])
CREATE INDEX [IX_CollectionMethodId] ON [dbo].[PickUpAddress]([CollectionMethodId])
CREATE TABLE [dbo].[CollectionMethod] (
    [Id] [int] NOT NULL IDENTITY,
    [Method] [varchar](80),
    CONSTRAINT [PK_dbo.CollectionMethod] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[ProcessingData] (
    [Id] [int] NOT NULL IDENTITY,
    [ProcessingDate] [datetime],
    [Comments] [varchar](1000),
    [InvoiceId] [int],
    [ShipmentNo] [varchar](50),
    [VendorCompanyName] [varchar](250),
    [VendorAddress] [varchar](200),
    [VendorCityId] [int],
    [VendorStateId] [int],
    [VendorZip] [varchar](10),
    [VendorPhone] [varchar](20),
    [PickUpCompanyName] [varchar](250),
    [PickupAddress] [varchar](200),
    [PickupStateId] [int],
    [PickupCityId] [int],
    [PickupZip] [nchar](10),
    [PickupPhone] [varchar](20),
    [PickupFax] [varchar](20),
    [PickupEmail] [varchar](100),
    [Client] [varchar](200),
    [RecievedBy] [varchar](200),
    [State] [varchar](200),
    [CollectionMethodId] [int],
    [StateWeightCategoryId] [int],
    CONSTRAINT [PK_dbo.ProcessingData] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_InvoiceId] ON [dbo].[ProcessingData]([InvoiceId])
CREATE INDEX [IX_VendorCityId] ON [dbo].[ProcessingData]([VendorCityId])
CREATE INDEX [IX_PickupCityId] ON [dbo].[ProcessingData]([PickupCityId])
CREATE INDEX [IX_CollectionMethodId] ON [dbo].[ProcessingData]([CollectionMethodId])
CREATE INDEX [IX_StateWeightCategoryId] ON [dbo].[ProcessingData]([StateWeightCategoryId])
CREATE TABLE [dbo].[Invoice] (
    [Id] [int] NOT NULL IDENTITY,
    [InvoiceDate] [smalldatetime],
    [PlanYear] [int],
    [InvoicePeriodFrom] [smalldatetime],
    [InvoicePeriodTo] [smalldatetime],
    [InvoiceDueDate] [smalldatetime],
    [Receivable] [bit],
    [Notes] [varchar](2048),
    [ShowNotes] [bit],
    [AffiliateId] [int],
    [StateId] [int],
    [RegionName] [varchar](150),
    [IsPaid] [bit] NOT NULL,
    [Approved] [bit],
    [Verified] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.Invoice] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateId] ON [dbo].[Invoice]([AffiliateId])
CREATE INDEX [IX_StateId] ON [dbo].[Invoice]([StateId])
CREATE TABLE [dbo].[InvoiceAdjustment] (
    [Id] [int] NOT NULL IDENTITY,
    [InvoiceId] [int] NOT NULL,
    [Description] [nvarchar](100) NOT NULL,
    [Amount] [money] NOT NULL,
    [AdjustmentDate] [datetime],
    CONSTRAINT [PK_dbo.InvoiceAdjustment] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_InvoiceId] ON [dbo].[InvoiceAdjustment]([InvoiceId])
CREATE TABLE [dbo].[InvoiceItem] (
    [Id] [int] NOT NULL IDENTITY,
    [InvoiceId] [int],
    [ShipmentId] [int],
    [ServiceTypeId] [int],
    [ServiceDate] [smalldatetime],
    [ProductTypeId] [int],
    [QuantityTypeId] [int],
    [Quantity] [money],
    [Rate] [money],
    [QuantityAdjustPercent] [decimal](4, 3),
    [Tare] [int],
    [PalletNumber] [int],
    [Description] [nvarchar](100),
    [IsMetro] [bit],
    [OriginalQuantity] [decimal](18, 0),
    CONSTRAINT [PK_dbo.InvoiceItem] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_InvoiceId] ON [dbo].[InvoiceItem]([InvoiceId])
CREATE INDEX [IX_ServiceTypeId] ON [dbo].[InvoiceItem]([ServiceTypeId])
CREATE INDEX [IX_ProductTypeId] ON [dbo].[InvoiceItem]([ProductTypeId])
CREATE INDEX [IX_QuantityTypeId] ON [dbo].[InvoiceItem]([QuantityTypeId])
CREATE TABLE [dbo].[InvoiceItemReconciliation] (
    [Id] [int] NOT NULL IDENTITY,
    [ReconcileFromInvoiceItemID] [int],
    [ReconcileToInvoiceItemID] [int],
    [Quantity] [decimal](18, 2),
    CONSTRAINT [PK_dbo.InvoiceItemReconciliation] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_ReconcileFromInvoiceItemID] ON [dbo].[InvoiceItemReconciliation]([ReconcileFromInvoiceItemID])
CREATE INDEX [IX_ReconcileToInvoiceItemID] ON [dbo].[InvoiceItemReconciliation]([ReconcileToInvoiceItemID])
CREATE TABLE [dbo].[InvoiceReconciliation] (
    [Id] [int] NOT NULL IDENTITY,
    [OEMInvoiceId] [int] NOT NULL,
    [ProcesserInvoiceItemId] [int] NOT NULL,
    [Quantity] [decimal](18, 2),
    [ReconcileDate] [datetime],
    [Rate] [money],
    CONSTRAINT [PK_dbo.InvoiceReconciliation] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_OEMInvoiceId] ON [dbo].[InvoiceReconciliation]([OEMInvoiceId])
CREATE INDEX [IX_ProcesserInvoiceItemId] ON [dbo].[InvoiceReconciliation]([ProcesserInvoiceItemId])
CREATE TABLE [dbo].[QuantityType] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](75),
    [Abbrev] [varchar](10),
    [QuantityTypeGroupId] [int],
    CONSTRAINT [PK_dbo.QuantityType] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_QuantityTypeGroupId] ON [dbo].[QuantityType]([QuantityTypeGroupId])
CREATE TABLE [dbo].[QuantityTypeGroup] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](75) NOT NULL,
    CONSTRAINT [PK_dbo.QuantityTypeGroup] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[InvoiceUnpackId] (
    [Id] [int] NOT NULL IDENTITY,
    [OEMInvoiceId] [int],
    [ProcesserInvoiceId] [int],
    [NewProcesserInvoiceId] [nvarchar](50),
    CONSTRAINT [PK_dbo.InvoiceUnpackId] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_OEMInvoiceId] ON [dbo].[InvoiceUnpackId]([OEMInvoiceId])
CREATE INDEX [IX_ProcesserInvoiceId] ON [dbo].[InvoiceUnpackId]([ProcesserInvoiceId])
CREATE TABLE [dbo].[PaymentDetail] (
    [Id] [int] NOT NULL IDENTITY,
    [PaymentID] [int],
    [InvoiceId] [int],
    [PaidAmount] [money],
    CONSTRAINT [PK_dbo.PaymentDetail] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_PaymentID] ON [dbo].[PaymentDetail]([PaymentID])
CREATE INDEX [IX_InvoiceId] ON [dbo].[PaymentDetail]([InvoiceId])
CREATE TABLE [dbo].[Payment] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateId] [int],
    [Date] [datetime],
    [PaymentTypeID] [int],
    [Amount] [money],
    [ReferenceNumber] [varchar](50),
    [Note] [varchar](200),
    CONSTRAINT [PK_dbo.Payment] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateId] ON [dbo].[Payment]([AffiliateId])
CREATE INDEX [IX_PaymentTypeID] ON [dbo].[Payment]([PaymentTypeID])
CREATE TABLE [dbo].[PaymentType] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](50) NOT NULL,
    [Description] [varchar](300),
    CONSTRAINT [PK_dbo.PaymentType] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[StateWeightCategory] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [Name] [nvarchar](50) NOT NULL,
    [Ratio] [decimal](18, 4) NOT NULL,
    CONSTRAINT [PK_dbo.StateWeightCategory] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[StateWeightCategory]([StateId])
CREATE TABLE [dbo].[CollectionEvent] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [CityId] [int] NOT NULL,
    [ZipCode] [nvarchar](10),
    [Address] [nvarchar](200),
    [Hours] [nvarchar](200),
    [Location] [nvarchar](200),
    [Note] [nvarchar](2000),
    CONSTRAINT [PK_dbo.CollectionEvent] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[CollectionEvent]([StateId])
CREATE INDEX [IX_CityId] ON [dbo].[CollectionEvent]([CityId])
CREATE TABLE [dbo].[County] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](50),
    [Abbreviation] [char](5),
    [StateId] [int],
    CONSTRAINT [PK_dbo.County] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[County]([StateId])
CREATE TABLE [dbo].[DropOff_Location] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](200),
    [Address1] [varchar](200),
    [Address2] [varchar](200),
    [Phone] [varchar](20),
    [Fax] [varchar](20),
    [Email] [varchar](50),
    [Website] [varchar](50),
    [Longitude] [decimal](9, 6),
    [Latitude] [decimal](9, 6),
    [Active] [bit],
    [StateId] [int],
    [CityId] [int],
    [ZipCode] [varchar](11),
    [AffiliateTypeId] [int],
    [PaymentTermId] [int],
    [Notes] [nvarchar](2048),
    [Directions] [nvarchar](2048),
    [BusinessHours] [nvarchar](2048),
    [CountyId] [int],
    CONSTRAINT [PK_dbo.DropOff_Location] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[DropOff_Location]([StateId])
CREATE INDEX [IX_CityId] ON [dbo].[DropOff_Location]([CityId])
CREATE TABLE [dbo].[DropOffLocation] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [nvarchar](200),
    [Address1] [nvarchar](200),
    [Address2] [nvarchar](200),
    [Phone] [varchar](20),
    [Fax] [varchar](50),
    [Email] [varchar](50),
    [WebSite] [varchar](50),
    [Longitude] [decimal](9, 6),
    [Latitude] [decimal](9, 6),
    [Active] [bit],
    [StateId] [int],
    [CityId] [int],
    [Zip] [varchar](11),
    [PaymentTermId] [int],
    [Notes] [nvarchar](2048),
    [Directions] [nvarchar](2048),
    [BusinessHours] [nvarchar](2048),
    [OwnerCompany] [int],
    CONSTRAINT [PK_dbo.DropOffLocation] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[DropOffLocation]([StateId])
CREATE INDEX [IX_CityId] ON [dbo].[DropOffLocation]([CityId])
CREATE TABLE [dbo].[DropOffLocationType] (
    [Id] [int] NOT NULL IDENTITY,
    [DropOffLocationId] [int] NOT NULL,
    [TypeId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.DropOffLocationType] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_DropOffLocationId] ON [dbo].[DropOffLocationType]([DropOffLocationId])
CREATE INDEX [IX_TypeId] ON [dbo].[DropOffLocationType]([TypeId])
CREATE TABLE [dbo].[LocationType] (
    [Id] [int] NOT NULL,
    [LevelName] [nvarchar](50) NOT NULL,
    [Description] [nvarchar](500),
    CONSTRAINT [PK_dbo.LocationType] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[Country] (
    [Id] [int] NOT NULL,
    [Name] [varchar](50),
    [Abbreviation] [char](5),
    [RegionId] [int],
    CONSTRAINT [PK_dbo.Country] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_RegionId] ON [dbo].[Country]([RegionId])
CREATE TABLE [dbo].[Region] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](50),
    [Abbreviation] [char](5),
    CONSTRAINT [PK_dbo.Region] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[DocumentLib] (
    [Id] [int] NOT NULL IDENTITY,
    [Title] [varchar](200),
    [Tags] [varchar](200),
    [UploadDate] [datetime],
    [AttachmentPath] [varchar](300),
    [Description] [varchar](300),
    [StateId] [int],
    [PlanYear] [int],
    CONSTRAINT [PK_dbo.DocumentLib] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[DocumentLib]([StateId])
CREATE TABLE [dbo].[Event] (
    [Id] [int] NOT NULL IDENTITY,
    [Title] [varchar](300),
    [Description] [varchar](2000),
    [DateBegin] [date],
    [DateEnd] [date],
    [ActionDate] [date],
    [EventTypeId] [int],
    [RecurrenceTypeId] [int],
    [SingleRecurrenceValue] [date],
    [RecurrenceIntervalForDay] [int],
    [RecurrenceIntervalForMonth] [int],
    [RecurrenceIntervalQuaterly] [int],
    [RecurrenceIntervalYearly] [date],
    [IsTemplate] [bit],
    [TemplateName] [varchar](300),
    [StateId] [int],
    CONSTRAINT [PK_dbo.Event] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_EventTypeId] ON [dbo].[Event]([EventTypeId])
CREATE INDEX [IX_RecurrenceTypeId] ON [dbo].[Event]([RecurrenceTypeId])
CREATE INDEX [IX_StateId] ON [dbo].[Event]([StateId])
CREATE TABLE [dbo].[EventType] (
    [Id] [int] NOT NULL IDENTITY,
    [Type] [varchar](150),
    CONSTRAINT [PK_dbo.EventType] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[RecurrenceType] (
    [Id] [int] NOT NULL IDENTITY,
    [Type] [nvarchar](150),
    CONSTRAINT [PK_dbo.RecurrenceType] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[ReminderGroup] (
    [Id] [int] NOT NULL IDENTITY,
    [EventId] [int],
    [RowId] [int],
    CONSTRAINT [PK_dbo.ReminderGroup] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_EventId] ON [dbo].[ReminderGroup]([EventId])
CREATE TABLE [dbo].[ReminderGroupAffiliate] (
    [Id] [int] NOT NULL IDENTITY,
    [ReminderGroupId] [int],
    [AffiliateId] [int],
    CONSTRAINT [PK_dbo.ReminderGroupAffiliate] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_ReminderGroupId] ON [dbo].[ReminderGroupAffiliate]([ReminderGroupId])
CREATE INDEX [IX_AffiliateId] ON [dbo].[ReminderGroupAffiliate]([AffiliateId])
CREATE TABLE [dbo].[Reminder] (
    [Id] [int] NOT NULL IDENTITY,
    [ReminderGroupId] [int],
    [Title] [varchar](200),
    [Number] [varchar](50),
    [PeriodId] [int],
    [StatusId] [int],
    [ShowOnCalendar] [bit],
    [SendDate] [date],
    [Emails] [nvarchar](1500),
    [SentReminderDate] [date],
    [OccurranceDate] [date],
    CONSTRAINT [PK_dbo.Reminder] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_ReminderGroupId] ON [dbo].[Reminder]([ReminderGroupId])
CREATE INDEX [IX_PeriodId] ON [dbo].[Reminder]([PeriodId])
CREATE INDEX [IX_StatusId] ON [dbo].[Reminder]([StatusId])
CREATE TABLE [dbo].[Period] (
    [Id] [int] NOT NULL IDENTITY,
    [Period] [varchar](50),
    CONSTRAINT [PK_dbo.Period] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[ReminderAttachment] (
    [Id] [int] NOT NULL IDENTITY,
    [ReminderId] [int],
    [AttachmentPath] [nvarchar](300),
    [AttachmentName] [nvarchar](150),
    CONSTRAINT [PK_dbo.ReminderAttachment] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_ReminderId] ON [dbo].[ReminderAttachment]([ReminderId])
CREATE TABLE [dbo].[ReminderStatus] (
    [Id] [int] NOT NULL IDENTITY,
    [Status] [varchar](150),
    CONSTRAINT [PK_dbo.ReminderStatus] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[PlanYear] (
    [Id] [int] NOT NULL IDENTITY,
    [Year] [int] NOT NULL,
    [StartDate] [date],
    [EndDate] [date],
    [StateId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.PlanYear] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[PlanYear]([StateId])
CREATE TABLE [dbo].[StatePolicy] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [PolicyId] [int] NOT NULL,
    [Active] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.StatePolicy] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[StatePolicy]([StateId])
CREATE INDEX [IX_PolicyId] ON [dbo].[StatePolicy]([PolicyId])
CREATE TABLE [dbo].[StateProductType] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [ProductTypeId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.StateProductType] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[StateProductType]([StateId])
CREATE INDEX [IX_ProductTypeId] ON [dbo].[StateProductType]([ProductTypeId])
CREATE TABLE [dbo].[StateSpecificRates] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [Urban] [float] NOT NULL,
    [Rural] [float] NOT NULL,
    [Metro] [float] NOT NULL,
    [NonMetro] [float] NOT NULL,
    CONSTRAINT [PK_dbo.StateSpecificRates] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[StateSpecificRates]([StateId])
CREATE TABLE [dbo].[StateSupportedDevices] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [ProductTypeId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.StateSupportedDevices] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_StateId] ON [dbo].[StateSupportedDevices]([StateId])
CREATE INDEX [IX_ProductTypeId] ON [dbo].[StateSupportedDevices]([ProductTypeId])
CREATE TABLE [dbo].[MailBackDevices] (
    [Id] [int] NOT NULL IDENTITY,
    [MailBackId] [int] NOT NULL,
    [ManufacturerId] [int] NOT NULL,
    [ProductTypeId] [int] NOT NULL,
    [Model] [varchar](100) NOT NULL,
    [Size] [varchar](50) NOT NULL,
    [Quantity] [int] NOT NULL,
    [NeedBoxes] [bit] NOT NULL,
    [BoxSize] [varchar](50),
    [BoxQuantity] [int],
    CONSTRAINT [PK_dbo.MailBackDevices] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_MailBackId] ON [dbo].[MailBackDevices]([MailBackId])
CREATE INDEX [IX_ManufacturerId] ON [dbo].[MailBackDevices]([ManufacturerId])
CREATE INDEX [IX_ProductTypeId] ON [dbo].[MailBackDevices]([ProductTypeId])
CREATE TABLE [dbo].[MailBack] (
    [Id] [int] NOT NULL IDENTITY,
    [MailBackAddress] [varchar](400) NOT NULL,
    CONSTRAINT [PK_dbo.MailBack] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[SiteImage] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](50),
    [Path] [nvarchar](2048),
    CONSTRAINT [PK_dbo.SiteImage] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[DocumentType] (
    [Id] [int] NOT NULL IDENTITY,
    [SiteImageID] [int],
    [FileExtension] [varchar](10),
    [DocumentTypeName] [varchar](100),
    [ContentType] [varchar](100),
    [IsUploadable] [bit] NOT NULL,
    [IsImage] [bit] NOT NULL,
    [EscapeBeforeDisplay] [bit] NOT NULL,
    [DocumentType2_Id] [int] NOT NULL,
    CONSTRAINT [PK_dbo.DocumentType] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_SiteImageID] ON [dbo].[DocumentType]([SiteImageID])
CREATE INDEX [IX_DocumentType2_Id] ON [dbo].[DocumentType]([DocumentType2_Id])
CREATE TABLE [dbo].[Document] (
    [Id] [int] NOT NULL IDENTITY,
    [DocumentTypeId] [int],
    [AffiliateContactId] [int],
    [ServerFileName] [varchar](2048),
    [UploadFileName] [varchar](2048),
    [UploadDate] [smalldatetime],
    [DeleteDate] [smalldatetime],
    CONSTRAINT [PK_dbo.Document] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_DocumentTypeId] ON [dbo].[Document]([DocumentTypeId])
CREATE INDEX [IX_AffiliateContactId] ON [dbo].[Document]([AffiliateContactId])
CREATE TABLE [dbo].[DocumentRelation] (
    [Id] [int] NOT NULL IDENTITY,
    [DocumentID] [int] NOT NULL,
    [ForeignID] [int] NOT NULL,
    [ForeignTableID] [int] NOT NULL,
    CONSTRAINT [PK_dbo.DocumentRelation] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_DocumentID] ON [dbo].[DocumentRelation]([DocumentID])
CREATE INDEX [IX_ForeignTableID] ON [dbo].[DocumentRelation]([ForeignTableID])
CREATE TABLE [dbo].[ForeignTable] (
    [Id] [int] NOT NULL IDENTITY,
    [ForeignTableName] [varchar](255) NOT NULL,
    CONSTRAINT [PK_dbo.ForeignTable] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[AffiliateService] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateId] [int] NOT NULL,
    [ServiceTypeId] [int] NOT NULL,
    [Active] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.AffiliateService] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateId] ON [dbo].[AffiliateService]([AffiliateId])
CREATE INDEX [IX_ServiceTypeId] ON [dbo].[AffiliateService]([ServiceTypeId])
CREATE TABLE [dbo].[ServiceTypeGroupRelation] (
    [Id] [int] NOT NULL IDENTITY,
    [ServiceTypeGroupId] [int],
    [ServiceTypeId] [int],
    [Active] [bit],
    CONSTRAINT [PK_dbo.ServiceTypeGroupRelation] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_ServiceTypeGroupId] ON [dbo].[ServiceTypeGroupRelation]([ServiceTypeGroupId])
CREATE INDEX [IX_ServiceTypeId] ON [dbo].[ServiceTypeGroupRelation]([ServiceTypeId])
CREATE TABLE [dbo].[ServiceTypeGroup] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](100),
    [Description] [varchar](1000),
    CONSTRAINT [PK_dbo.ServiceTypeGroup] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[AffiliateMasterContact] (
    [Id] [int] NOT NULL IDENTITY,
    [MasterAffiliateContactId] [int] NOT NULL,
    [AssociateContactId] [int] NOT NULL,
    [DefaultUser] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.AffiliateMasterContact] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_MasterAffiliateContactId] ON [dbo].[AffiliateMasterContact]([MasterAffiliateContactId])
CREATE INDEX [IX_AssociateContactId] ON [dbo].[AffiliateMasterContact]([AssociateContactId])
CREATE TABLE [dbo].[AuditEvent] (
    [Id] [int] NOT NULL IDENTITY,
    [AuditActionId] [int] NOT NULL,
    [Entity] [nvarchar](50),
    [EntityId] [int],
    [AffiliateContactId] [int] NOT NULL,
    [TimeStamp] [datetime] NOT NULL,
    [IpAddress] [nvarchar](50),
    [Note] [nvarchar](50),
    CONSTRAINT [PK_dbo.AuditEvent] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AuditActionId] ON [dbo].[AuditEvent]([AuditActionId])
CREATE INDEX [IX_AffiliateContactId] ON [dbo].[AuditEvent]([AffiliateContactId])
CREATE TABLE [dbo].[AuditAction] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [nvarchar](50) NOT NULL,
    [Description] [nvarchar](200),
    CONSTRAINT [PK_dbo.AuditAction] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[Role] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateTypeId] [int],
    [RoleName] [varchar](50),
    [CreateByAffiliateId] [int],
    CONSTRAINT [PK_dbo.Role] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateTypeId] ON [dbo].[Role]([AffiliateTypeId])
CREATE INDEX [IX_CreateByAffiliateId] ON [dbo].[Role]([CreateByAffiliateId])
CREATE TABLE [dbo].[AffiliateRole] (
    [Id] [int] NOT NULL IDENTITY,
    [AffiliateId] [int],
    [RoleId] [int],
    CONSTRAINT [PK_dbo.AffiliateRole] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_AffiliateId] ON [dbo].[AffiliateRole]([AffiliateId])
CREATE INDEX [IX_RoleId] ON [dbo].[AffiliateRole]([RoleId])
CREATE TABLE [dbo].[AffiliateType] (
    [Id] [int] NOT NULL IDENTITY,
    [Type] [varchar](100),
    [Description] [varchar](200),
    CONSTRAINT [PK_dbo.AffiliateType] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[Permissions] (
    [Id] [int] NOT NULL IDENTITY,
    [RoleId] [int],
    [AreaId] [int],
    [Read] [bit],
    [Add] [bit],
    [Update] [bit],
    [Delete] [bit],
    CONSTRAINT [PK_dbo.Permissions] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_RoleId] ON [dbo].[Permissions]([RoleId])
CREATE INDEX [IX_AreaId] ON [dbo].[Permissions]([AreaId])
CREATE TABLE [dbo].[Area] (
    [Id] [int] NOT NULL,
    [ParentId] [int],
    [Name] [varchar](50) NOT NULL,
    [Caption] [varchar](200),
    [URL] [varchar](200),
    [IsMenu] [bit],
    [IsArea] [bit],
    [Order] [int],
    CONSTRAINT [PK_dbo.Area] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[SecurityQuestion] (
    [Id] [int] NOT NULL IDENTITY,
    [Question] [varchar](200),
    CONSTRAINT [PK_dbo.SecurityQuestion] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[RecyclerProductWeight] (
    [Id] [int] NOT NULL IDENTITY,
    [RecyclerId] [int] NOT NULL,
    [Year] [nvarchar](4) NOT NULL,
    [Month] [nvarchar](2) NOT NULL,
    [TotalWeightReceived] [decimal](18, 2) NOT NULL,
    [OnHand] [decimal](18, 2) NOT NULL,
    [CRTGlass] [decimal](18, 2) NOT NULL,
    [CircuitBoards] [decimal](18, 2) NOT NULL,
    [Batteries] [decimal](18, 2) NOT NULL,
    [OtherMaterials] [decimal](18, 2) NOT NULL,
    CONSTRAINT [PK_dbo.RecyclerProductWeight] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_RecyclerId] ON [dbo].[RecyclerProductWeight]([RecyclerId])
CREATE TABLE [dbo].[ConsumerProductTypeManufacturer] (
    [ManufacturerID] [int] NOT NULL IDENTITY,
    [AffiliateID] [int] NOT NULL,
    [ManufacturerName] [varchar](75) NOT NULL,
    [Verified] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.ConsumerProductTypeManufacturer] PRIMARY KEY ([ManufacturerID])
)
CREATE TABLE [dbo].[DBAudit] (
    [AuditId] [varchar](200) NOT NULL,
    [RevisionStamp] [datetime],
    [TableName] [nvarchar](50),
    [UserName] [nvarchar](50),
    [Actions] [nvarchar](1),
    [OldData] [xml],
    [NewData] [xml],
    [ChangedColumns] [nvarchar](1000),
    CONSTRAINT [PK_dbo.DBAudit] PRIMARY KEY ([AuditId])
)
CREATE TABLE [dbo].[DropOffDevice] (
    [Id] [int] NOT NULL IDENTITY,
    [DeviceName] [nvarchar](50),
    [Description] [nvarchar](50),
    [Note] [nvarchar](50),
    CONSTRAINT [PK_dbo.DropOffDevice] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[DropOffLocationDevice] (
    [Id] [int] NOT NULL IDENTITY,
    [DropOffLocationId] [int] NOT NULL,
    [DeviceId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.DropOffLocationDevice] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_DeviceId] ON [dbo].[DropOffLocationDevice]([DeviceId])
CREATE TABLE [dbo].[DropOffLocationFromAffiliate] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](200),
    [Address1] [varchar](200),
    [Address2] [varchar](200),
    [Phone] [varchar](20),
    [Fax] [varchar](50),
    [Email] [varchar](50),
    [WebSite] [varchar](50),
    [Longitude] [decimal](9, 6),
    [Latitude] [decimal](9, 6),
    [Active] [bit],
    [StateId] [int],
    [CityId] [int],
    [Zip] [varchar](11),
    [AffiliateTypeId] [int],
    [PaymentTermId] [int],
    [Notes] [nvarchar](2048),
    [Directions] [nvarchar](2048),
    [BusinessHours] [nvarchar](2048),
    CONSTRAINT [PK_dbo.DropOffLocationFromAffiliate] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[InvoiceRequiringFix] (
    [InvoiceId] [int] NOT NULL,
    [OldQuantity] [money],
    [OldTotal] [money],
    [NewQuantity] [decimal](38, 2),
    [NewTotal] [decimal](38, 6),
    [PaidAmount] [money],
    CONSTRAINT [PK_dbo.InvoiceRequiringFix] PRIMARY KEY ([InvoiceId])
)
CREATE TABLE [dbo].[NewsFeed] (
    [Id] [int] NOT NULL,
    [News] [nvarchar](500),
    [FeedLink] [nvarchar](200),
    [Date] [datetime],
    CONSTRAINT [PK_dbo.NewsFeed] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[PaymentTerm] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [varchar](50) NOT NULL,
    [Days] [int] NOT NULL,
    [Description] [varchar](300),
    CONSTRAINT [PK_dbo.PaymentTerm] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[StateGuideline_Contact] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [Name] [nvarchar](100) NOT NULL,
    [Phone] [nvarchar](100),
    [EMail] [nvarchar](200),
    CONSTRAINT [PK_dbo.StateGuideline_Contact] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[StateGuideline_DeadlineDate] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [Name] [nvarchar](200) NOT NULL,
    [Date] [nvarchar](200),
    CONSTRAINT [PK_dbo.StateGuideline_DeadlineDate] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[StateGuideline_Link] (
    [Id] [int] NOT NULL IDENTITY,
    [StateId] [int] NOT NULL,
    [Name] [nvarchar](200) NOT NULL,
    [Link] [nvarchar](500) NOT NULL,
    CONSTRAINT [PK_dbo.StateGuideline_Link] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[StateGuideline] (
    [StateId] [int] NOT NULL,
    [GoverningBody] [nvarchar](2000),
    [Website] [nvarchar](500),
    [CoveredDevices] [nvarchar](2000),
    [OEMObligation] [nvarchar](2000),
    [Updates] [nvarchar](2000),
    [LawState] [int],
    [UpdateDate] [datetime],
    [RegulationType] [nvarchar](2000),
    [RegulationChange] [nvarchar](2000),
    CONSTRAINT [PK_dbo.StateGuideline] PRIMARY KEY ([StateId])
)
CREATE TABLE [dbo].[V_CertificateReport] (
    [ToAffiliateID] [int] NOT NULL,
    [RegionId] [int],
    [Product] [varchar](75),
    [RecQuantity] [int],
    [InvoiceID] [int],
    [ToAffiliateName] [varchar](200),
    [FromAffiliate] [int],
    [AffiliateName] [varchar](200),
    [RecyclerAddress1] [varchar](200),
    [RecyclerAddress2] [varchar](66),
    [Phone] [varchar](20),
    [Fax] [varchar](50),
    [Email] [varchar](50),
    [ProcessingDate] [varchar](10),
    [ProcessingDate1] [datetime],
    [ShipmentNo] [varchar](50),
    [Region] [varchar](50),
    CONSTRAINT [PK_dbo.V_CertificateReport] PRIMARY KEY ([ToAffiliateID])
)
CREATE TABLE [dbo].[V_InvoiceOverView] (
    [InvoiceID] [int] NOT NULL,
    [RegionID] [int] NOT NULL,
    [Receivable] [bit],
    [InvoiceDate] [smalldatetime],
    [InvoicePeriod] [int],
    [Cost] [money],
    [Volume] [money],
    [AdjustedVolume] [decimal](38, 7),
    [RegionName] [varchar](50),
    [AffiliateID] [int],
    [AffiliateName] [varchar](200),
    [PaymentTermID] [int],
    [TargetQuantity] [decimal](38, 0),
    [TargetPercentOfYear] [decimal](38, 6),
    [QuantityTypeID] [int],
    [ServiceTypeID] [int],
    [InvoicePeriodFrom] [smalldatetime],
    [InvoicePeriodTo] [smalldatetime],
    [PeriodBegin] [datetime],
    [PeriodEnd] [date],
    CONSTRAINT [PK_dbo.V_InvoiceOverView] PRIMARY KEY ([InvoiceID], [RegionID])
)
CREATE TABLE [dbo].[v_InvoicesRequiringFix] (
    [InvoiceId] [int] NOT NULL,
    [OldQuantity] [money],
    [OldTotal] [money],
    [NewQuantity] [decimal](38, 2),
    [NewTotal] [decimal](38, 6),
    [PaidAmount] [money],
    CONSTRAINT [PK_dbo.v_InvoicesRequiringFix] PRIMARY KEY ([InvoiceId])
)
CREATE TABLE [dbo].[V_ProcessingReport] (
    [ProcessingDataID] [int] NOT NULL,
    [InvoiceItemID] [int] NOT NULL,
    [Qauntity] [money] NOT NULL,
    [Tare] [int] NOT NULL,
    [AffiliateName] [varchar](200),
    [RecyclerAddress1] [varchar](200),
    [RecyclerAddress2] [varchar](66),
    [Phone] [varchar](20),
    [Fax] [varchar](50),
    [Email] [varchar](50),
    [ProcessingDate] [datetime],
    [InvoiceDate] [smalldatetime],
    [PlanYear] [int],
    [StateName] [varchar](50),
    [Comments] [varchar](1000),
    [InvoiceID] [int],
    [ShipmentNo] [varchar](50),
    [VendorCompanyName] [varchar](250),
    [VendorAddress] [varchar](200),
    [VendorCity] [varchar](50),
    [VendorStateProvinceName] [varchar](50),
    [VendorZip] [varchar](10),
    [VendorPhone] [varchar](20),
    [PickupAddress] [varchar](200),
    [ShipStateProvinceName] [varchar](50),
    [PickupCity] [varchar](50),
    [PickUpCompanyName] [varchar](250),
    [PickupZip] [nchar](10),
    [PickupPhone] [varchar](20),
    [PickupFax] [varchar](20),
    [PickupEmail] [varchar](100),
    [Client] [varchar](200),
    [RecievedBy] [varchar](200),
    [AffiliateID] [int],
    [ShipmentID] [int],
    [ProductTypeName] [varchar](75),
    [ServiceTypeName] [varchar](100),
    [PalletNumber] [int],
    [Description] [nvarchar](100),
    [IsMetro] [bit],
    [CollectionMethod] [varchar](80),
    [Total] [money],
    [OriginalQuantity] [decimal](18, 0),
    [WeightCategory] [nvarchar](50),
    CONSTRAINT [PK_dbo.V_ProcessingReport] PRIMARY KEY ([ProcessingDataID], [InvoiceItemID], [Qauntity], [Tare])
)
CREATE TABLE [dbo].[V_ReminderEmail] (
    [ReminderGroupId] [int] NOT NULL,
    [AffiliateId] [int] NOT NULL,
    [Name] [varchar](1) NOT NULL,
    [Email] [varchar](1) NOT NULL,
    [EventId] [int] NOT NULL,
    [ReminderId] [int] NOT NULL,
    [EventTitle] [varchar](300),
    [EventDescription] [varchar](2000),
    [Number] [varchar](50),
    [Period] [varchar](50),
    [PeriodId] [int],
    [Title] [varchar](200),
    [SendDate] [datetime],
    [Emails] [nvarchar](1500),
    [SentReminderDate] [date],
    [DateBegin] [date],
    [DateEnd] [date],
    [ActionDate] [date],
    [RecurrenceTypeId] [int],
    [SingleRecurrenceValue] [date],
    [RecurrenceIntervalForDay] [int],
    [RecurrenceIntervalForMonth] [int],
    [RecurrenceIntervalQuaterly] [int],
    [RecurrenceIntervalYearly] [date],
    [AttachmentName] [nvarchar](150),
    [eType] [varchar](150),
    [OccurranceDate] [datetime],
    CONSTRAINT [PK_dbo.V_ReminderEmail] PRIMARY KEY ([ReminderGroupId], [AffiliateId], [Name], [Email], [EventId], [ReminderId])
)
CREATE TABLE [dbo].[V_ReoncileInvoiceDetail] (
    [Item] [int] NOT NULL,
    [RegionName] [varchar](150),
    [AffiliateDetailValue] [varchar](200),
    [FromInvoice] [int],
    [ToInvoice] [int],
    [Quantity] [decimal](18, 2),
    [ProductTypeName] [varchar](75),
    [ReconcileFromInvoiceItemID] [int],
    CONSTRAINT [PK_dbo.V_ReoncileInvoiceDetail] PRIMARY KEY ([Item])
)
CREATE TABLE [dbo].[V_ReportEntitytargets] (
    [InvoiceID] [int] NOT NULL,
    [StateId] [int] NOT NULL,
    [IndividualTargetStatus] [varchar](22) NOT NULL,
    [StateAffiliateTargetStatus] [varchar](22) NOT NULL,
    [StateTargetStatus] [varchar](22) NOT NULL,
    [AffiliateStateTargetStatus] [varchar](22) NOT NULL,
    [AffiliateTargetStatus] [varchar](22) NOT NULL,
    [State] [varchar](50),
    [EntityName] [varchar](200),
    [InvoiceAmount] [decimal](38, 6),
    [PaymentAmount] [money],
    [RemainingAmount] [decimal](38, 6),
    [InvoiceDate] [varchar](10),
    [InvoiceDate1] [smalldatetime],
    [InvoiceMonthName] [nvarchar](30),
    [InvoiceMonth] [int],
    [InvoiceYear] [int],
    [PlanYear] [int],
    [InvoiceYearHalf] [varchar](13),
    [InvoiceQaurter] [varchar](9),
    [ProcessingDate] [varchar](10),
    [InvoiceDueDate] [varchar](10),
    [PaymentDate] [varchar](10),
    [InvoicePeriodFrom] [varchar](10),
    [InvoicePeriodTo] [varchar](10),
    [InvoiceItemCount] [int],
    [PaymentItemCount] [int],
    [PaidStatus] [varchar](12),
    [Receivable] [bit],
    [EntityId] [int],
    [IsPaid] [bit],
    [Aging] [int],
    [EntityType] [int],
    [InvoiceWeight] [decimal](38, 2),
    [RecAssignedWeight] [decimal](38, 2),
    [RecUnAssignedWeight] [decimal](38, 2),
    [RecWeight] [decimal](38, 2),
    [InvRemainingWeight] [decimal](38, 2),
    [TargetWeight] [decimal](18, 0),
    [InvoiceStatus] [varchar](17),
    [QuantityTypeID] [int],
    [ServiceTypeID] [int],
    [TargetStartDate] [varchar](10),
    [TargetEndDate] [varchar](10),
    [StartDate] [datetime],
    [EndDate] [datetime],
    [TargetId] [int],
    [InvoiceCountPerTarget] [int],
    [InvAvgWeight] [decimal](29, 11),
    [InvRemWeight] [decimal](38, 2),
    [TargetRange] [varchar](24),
    [IndTargetWeight] [decimal](38, 0),
    [IndRecWeight] [decimal](38, 2),
    [RemIndTarget] [decimal](38, 2),
    [StateAffilaiteTarget] [decimal](38, 0),
    [StateAffilaiteRec] [decimal](38, 2),
    [StateAffilaiteRem] [decimal](38, 0),
    [StateTarget] [decimal](38, 0),
    [StateRec] [decimal](38, 2),
    [StateRem] [decimal](38, 0),
    [AffilaiteStateTarget] [decimal](38, 0),
    [AffilaiteStateRec] [decimal](38, 2),
    [AffilaiteStateRem] [decimal](38, 0),
    [AffilaiteTarget] [decimal](38, 0),
    [AffilaiteRec] [decimal](38, 2),
    [AffilaiteRem] [decimal](38, 0),
    CONSTRAINT [PK_dbo.V_ReportEntitytargets] PRIMARY KEY ([InvoiceID], [StateId], [IndividualTargetStatus], [StateAffiliateTargetStatus], [StateTargetStatus], [AffiliateStateTargetStatus], [AffiliateTargetStatus])
)
CREATE TABLE [dbo].[V_ReportInvoice] (
    [InvoiceID] [int] NOT NULL,
    [Quantity] [int] NOT NULL,
    [Rate] [money] NOT NULL,
    [AffiliateName] [varchar](200),
    [RecyclerAddress1] [varchar](200),
    [RecyclerAddress2] [varchar](66),
    [Phone] [varchar](20),
    [VendorCompanyName] [varchar](250),
    [StateProvinceName] [varchar](50),
    [VendorAddress] [varchar](200),
    [VendorAddress1] [varchar](65),
    [InvStateName] [varchar](50),
    [AffiliateID] [int],
    [ProcessingDate] [varchar](10),
    [InvoiceDate] [smalldatetime],
    [InvoiceDueDate] [varchar](10),
    [ServiceDate] [varchar](10),
    [InvoicePeriodFrom] [varchar](10),
    [InvoicePeriodTo] [varchar](10),
    [ShipmentID] [varchar](50),
    [ProductTypeName] [varchar](75),
    [ServiceTypeName] [varchar](100),
    [Total] [money],
    [PickupAddress] [varchar](200),
    [ShipStateProvinceName] [varchar](50),
    [PickupCity] [varchar](50),
    [PickupZip] [nchar](10),
    [PickupPhone] [varchar](20),
    [PickupFax] [varchar](20),
    [PickupEmail] [varchar](100),
    [CollectionMethod] [varchar](80),
    [Notes] [varchar](2048),
    CONSTRAINT [PK_dbo.V_ReportInvoice] PRIMARY KEY ([InvoiceID], [Quantity], [Rate])
)
CREATE TABLE [dbo].[V_ReportInvoiceForOEM] (
    [InvoiceID] [int] NOT NULL,
    [Quantity] [int] NOT NULL,
    [Rate] [money] NOT NULL,
    [AffiliateName] [varchar](200),
    [RecyclerAddress1] [varchar](200),
    [RecyclerAddress2] [varchar](66),
    [Phone] [varchar](20),
    [VendorCompanyName] [varchar](250),
    [StateProvinceName] [varchar](50),
    [VendorAddress] [varchar](200),
    [VendorAddress1] [varchar](65),
    [InvStateName] [varchar](50),
    [AffiliateId] [int],
    [ProcessingDate] [varchar](10),
    [InvoiceDate] [smalldatetime],
    [InvoiceDueDate] [varchar](10),
    [ServiceDate] [varchar](10),
    [InvoicePeriodFrom] [varchar](10),
    [InvoicePeriodTo] [varchar](10),
    [ShipmentID] [varchar](50),
    [ProductTypeName] [varchar](75),
    [ServiceTypeName] [varchar](100),
    [Total] [decimal](38, 6),
    [PickupAddress] [varchar](200),
    [ShipStateProvinceName] [varchar](50),
    [PickupCity] [varchar](50),
    [PickupZip] [nchar](10),
    [PickupPhone] [varchar](20),
    [PickupFax] [varchar](20),
    [PickupEmail] [varchar](100),
    [CollectionMethod] [varchar](80),
    [Notes] [varchar](2048),
    CONSTRAINT [PK_dbo.V_ReportInvoiceForOEM] PRIMARY KEY ([InvoiceID], [Quantity], [Rate])
)
CREATE TABLE [dbo].[V_ReportReconcileDetail] (
    [FromStateID] [int] NOT NULL,
    [FromEntityId] [int] NOT NULL,
    [RecAssignedWeight] [decimal](18, 2) NOT NULL,
    [FromStateName] [varchar](50),
    [FromEntityName] [varchar](200),
    [FromPlanYear] [int],
    [FromInvoiceId] [int],
    [ToStateId] [int],
    [ToStateName] [varchar](50),
    [ToEntityId] [int],
    [ToEntityName] [varchar](200),
    [ToInvoiceId] [int],
    [ReconcileFromInvoiceItemID] [int],
    [ReconcileToInvoiceItemID] [int],
    [FromInvoiceWeight] [money],
    [ReconcileItemCount] [int],
    CONSTRAINT [PK_dbo.V_ReportReconcileDetail] PRIMARY KEY ([FromStateID], [FromEntityId], [RecAssignedWeight])
)
ALTER TABLE [dbo].[AffiliateAccreditation] ADD CONSTRAINT [FK_dbo.AffiliateAccreditation_dbo.AccreditationType_AccreditationTypeId] FOREIGN KEY ([AccreditationTypeId]) REFERENCES [dbo].[AccreditationType] ([Id])
ALTER TABLE [dbo].[AffiliateAccreditation] ADD CONSTRAINT [FK_dbo.AffiliateAccreditation_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[Affiliate] ADD CONSTRAINT [FK_dbo.Affiliate_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[Affiliate] ADD CONSTRAINT [FK_dbo.Affiliate_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[Affiliate] ADD CONSTRAINT [FK_dbo.Affiliate_dbo.AffiliateType_AffiliateTypeId] FOREIGN KEY ([AffiliateTypeId]) REFERENCES [dbo].[AffiliateType] ([Id])
ALTER TABLE [dbo].[AffiliateContact] ADD CONSTRAINT [FK_dbo.AffiliateContact_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[AffiliateContact] ADD CONSTRAINT [FK_dbo.AffiliateContact_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[AffiliateContact] ADD CONSTRAINT [FK_dbo.AffiliateContact_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[AffiliateContact] ADD CONSTRAINT [FK_dbo.AffiliateContact_dbo.Role_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([Id])
ALTER TABLE [dbo].[AffiliateContact] ADD CONSTRAINT [FK_dbo.AffiliateContact_dbo.SecurityQuestion_SecurityQuestionId] FOREIGN KEY ([SecurityQuestionId]) REFERENCES [dbo].[SecurityQuestion] ([Id])
ALTER TABLE [dbo].[AffiliateContactService] ADD CONSTRAINT [FK_dbo.AffiliateContactService_dbo.ServiceType_ServiceTypeId] FOREIGN KEY ([ServiceTypeId]) REFERENCES [dbo].[ServiceType] ([Id])
ALTER TABLE [dbo].[AffiliateContactService] ADD CONSTRAINT [FK_dbo.AffiliateContactService_dbo.AffiliateContact_AffiliateContactId] FOREIGN KEY ([AffiliateContactId]) REFERENCES [dbo].[AffiliateContact] ([Id])
ALTER TABLE [dbo].[AffiliateContractRate] ADD CONSTRAINT [FK_dbo.AffiliateContractRate_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[AffiliateContractRate] ADD CONSTRAINT [FK_dbo.AffiliateContractRate_dbo.PriceUnitType_PriceUnitTypeId] FOREIGN KEY ([PriceUnitTypeId]) REFERENCES [dbo].[PriceUnitType] ([Id])
ALTER TABLE [dbo].[AffiliateContractRate] ADD CONSTRAINT [FK_dbo.AffiliateContractRate_dbo.ProductType_ProductTypeId] FOREIGN KEY ([ProductTypeId]) REFERENCES [dbo].[ProductType] ([Id])
ALTER TABLE [dbo].[AffiliateContractRate] ADD CONSTRAINT [FK_dbo.AffiliateContractRate_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[AffiliateContractRate] ADD CONSTRAINT [FK_dbo.AffiliateContractRate_dbo.QuantityType_QuantityTypeId] FOREIGN KEY ([QuantityTypeId]) REFERENCES [dbo].[QuantityType] ([Id])
ALTER TABLE [dbo].[AffiliateContractRate] ADD CONSTRAINT [FK_dbo.AffiliateContractRate_dbo.ServiceType_ServiceTypeId] FOREIGN KEY ([ServiceTypeId]) REFERENCES [dbo].[ServiceType] ([Id])
ALTER TABLE [dbo].[ProductType] ADD CONSTRAINT [FK_dbo.ProductType_dbo.QuantityType_WeightQuantityTypeID] FOREIGN KEY ([WeightQuantityTypeID]) REFERENCES [dbo].[QuantityType] ([Id])
ALTER TABLE [dbo].[ProductType] ADD CONSTRAINT [FK_dbo.ProductType_dbo.QuantityType_ContainerQuantityTypeId] FOREIGN KEY ([ContainerQuantityTypeId]) REFERENCES [dbo].[QuantityType] ([Id])
ALTER TABLE [dbo].[ProductType] ADD CONSTRAINT [FK_dbo.ProductType_dbo.SiteImage_SiteImageID] FOREIGN KEY ([SiteImageID]) REFERENCES [dbo].[SiteImage] ([Id])
ALTER TABLE [dbo].[AffiliateTargetProduct] ADD CONSTRAINT [FK_dbo.AffiliateTargetProduct_dbo.AffiliateTarget_AffiliateTargetId] FOREIGN KEY ([AffiliateTargetId]) REFERENCES [dbo].[AffiliateTarget] ([Id])
ALTER TABLE [dbo].[AffiliateTargetProduct] ADD CONSTRAINT [FK_dbo.AffiliateTargetProduct_dbo.ProductType_ProductTypeId] FOREIGN KEY ([ProductTypeId]) REFERENCES [dbo].[ProductType] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AffiliateTarget] ADD CONSTRAINT [FK_dbo.AffiliateTarget_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[AffiliateTarget] ADD CONSTRAINT [FK_dbo.AffiliateTarget_dbo.Policy_PolicyId] FOREIGN KEY ([PolicyId]) REFERENCES [dbo].[Policy] ([Id])
ALTER TABLE [dbo].[AffiliateTarget] ADD CONSTRAINT [FK_dbo.AffiliateTarget_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[AffiliateTarget] ADD CONSTRAINT [FK_dbo.AffiliateTarget_dbo.QuantityType_QuantityTypeId] FOREIGN KEY ([QuantityTypeId]) REFERENCES [dbo].[QuantityType] ([Id])
ALTER TABLE [dbo].[AffiliateTarget] ADD CONSTRAINT [FK_dbo.AffiliateTarget_dbo.ServiceType_ServiceTypeId] FOREIGN KEY ([ServiceTypeId]) REFERENCES [dbo].[ServiceType] ([Id])
ALTER TABLE [dbo].[ManufacturerPolicy] ADD CONSTRAINT [FK_dbo.ManufacturerPolicy_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[ManufacturerPolicy] ADD CONSTRAINT [FK_dbo.ManufacturerPolicy_dbo.Policy_PolicyId] FOREIGN KEY ([PolicyId]) REFERENCES [dbo].[Policy] ([Id])
ALTER TABLE [dbo].[ManufacturerPolicy] ADD CONSTRAINT [FK_dbo.ManufacturerPolicy_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[State] ADD CONSTRAINT [FK_dbo.State_dbo.Country_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id])
ALTER TABLE [dbo].[AffiliateState] ADD CONSTRAINT [FK_dbo.AffiliateState_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[AffiliateState] ADD CONSTRAINT [FK_dbo.AffiliateState_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[AuditReports] ADD CONSTRAINT [FK_dbo.AuditReports_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[City] ADD CONSTRAINT [FK_dbo.City_dbo.County_CountyId] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[County] ([Id])
ALTER TABLE [dbo].[City] ADD CONSTRAINT [FK_dbo.City_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[ClientAddress] ADD CONSTRAINT [FK_dbo.ClientAddress_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[ClientAddress] ADD CONSTRAINT [FK_dbo.ClientAddress_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[ClientAddress] ADD CONSTRAINT [FK_dbo.ClientAddress_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[PickUpAddress] ADD CONSTRAINT [FK_dbo.PickUpAddress_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[PickUpAddress] ADD CONSTRAINT [FK_dbo.PickUpAddress_dbo.CollectionMethod_CollectionMethodId] FOREIGN KEY ([CollectionMethodId]) REFERENCES [dbo].[CollectionMethod] ([Id])
ALTER TABLE [dbo].[PickUpAddress] ADD CONSTRAINT [FK_dbo.PickUpAddress_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[PickUpAddress] ADD CONSTRAINT [FK_dbo.PickUpAddress_dbo.ClientAddress_ClientAddressId] FOREIGN KEY ([ClientAddressId]) REFERENCES [dbo].[ClientAddress] ([Id])
ALTER TABLE [dbo].[ProcessingData] ADD CONSTRAINT [FK_dbo.ProcessingData_dbo.CollectionMethod_CollectionMethodId] FOREIGN KEY ([CollectionMethodId]) REFERENCES [dbo].[CollectionMethod] ([Id])
ALTER TABLE [dbo].[ProcessingData] ADD CONSTRAINT [FK_dbo.ProcessingData_dbo.Invoice_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoice] ([Id])
ALTER TABLE [dbo].[ProcessingData] ADD CONSTRAINT [FK_dbo.ProcessingData_dbo.StateWeightCategory_StateWeightCategoryId] FOREIGN KEY ([StateWeightCategoryId]) REFERENCES [dbo].[StateWeightCategory] ([Id])
ALTER TABLE [dbo].[ProcessingData] ADD CONSTRAINT [FK_dbo.ProcessingData_dbo.City_VendorCityId] FOREIGN KEY ([VendorCityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[ProcessingData] ADD CONSTRAINT [FK_dbo.ProcessingData_dbo.City_PickupCityId] FOREIGN KEY ([PickupCityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[Invoice] ADD CONSTRAINT [FK_dbo.Invoice_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[Invoice] ADD CONSTRAINT [FK_dbo.Invoice_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[InvoiceAdjustment] ADD CONSTRAINT [FK_dbo.InvoiceAdjustment_dbo.Invoice_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoice] ([Id])
ALTER TABLE [dbo].[InvoiceItem] ADD CONSTRAINT [FK_dbo.InvoiceItem_dbo.Invoice_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoice] ([Id])
ALTER TABLE [dbo].[InvoiceItem] ADD CONSTRAINT [FK_dbo.InvoiceItem_dbo.ProductType_ProductTypeId] FOREIGN KEY ([ProductTypeId]) REFERENCES [dbo].[ProductType] ([Id])
ALTER TABLE [dbo].[InvoiceItem] ADD CONSTRAINT [FK_dbo.InvoiceItem_dbo.QuantityType_QuantityTypeId] FOREIGN KEY ([QuantityTypeId]) REFERENCES [dbo].[QuantityType] ([Id])
ALTER TABLE [dbo].[InvoiceItem] ADD CONSTRAINT [FK_dbo.InvoiceItem_dbo.ServiceType_ServiceTypeId] FOREIGN KEY ([ServiceTypeId]) REFERENCES [dbo].[ServiceType] ([Id])
ALTER TABLE [dbo].[InvoiceItemReconciliation] ADD CONSTRAINT [FK_dbo.InvoiceItemReconciliation_dbo.InvoiceItem_ReconcileFromInvoiceItemID] FOREIGN KEY ([ReconcileFromInvoiceItemID]) REFERENCES [dbo].[InvoiceItem] ([Id])
ALTER TABLE [dbo].[InvoiceItemReconciliation] ADD CONSTRAINT [FK_dbo.InvoiceItemReconciliation_dbo.InvoiceItem_ReconcileToInvoiceItemID] FOREIGN KEY ([ReconcileToInvoiceItemID]) REFERENCES [dbo].[InvoiceItem] ([Id])
ALTER TABLE [dbo].[InvoiceReconciliation] ADD CONSTRAINT [FK_dbo.InvoiceReconciliation_dbo.InvoiceItem_ProcesserInvoiceItemId] FOREIGN KEY ([ProcesserInvoiceItemId]) REFERENCES [dbo].[InvoiceItem] ([Id])
ALTER TABLE [dbo].[InvoiceReconciliation] ADD CONSTRAINT [FK_dbo.InvoiceReconciliation_dbo.Invoice_OEMInvoiceId] FOREIGN KEY ([OEMInvoiceId]) REFERENCES [dbo].[Invoice] ([Id])
ALTER TABLE [dbo].[QuantityType] ADD CONSTRAINT [FK_dbo.QuantityType_dbo.QuantityTypeGroup_QuantityTypeGroupId] FOREIGN KEY ([QuantityTypeGroupId]) REFERENCES [dbo].[QuantityTypeGroup] ([Id])
ALTER TABLE [dbo].[InvoiceUnpackId] ADD CONSTRAINT [FK_dbo.InvoiceUnpackId_dbo.Invoice_OEMInvoiceId] FOREIGN KEY ([OEMInvoiceId]) REFERENCES [dbo].[Invoice] ([Id])
ALTER TABLE [dbo].[InvoiceUnpackId] ADD CONSTRAINT [FK_dbo.InvoiceUnpackId_dbo.Invoice_ProcesserInvoiceId] FOREIGN KEY ([ProcesserInvoiceId]) REFERENCES [dbo].[Invoice] ([Id])
ALTER TABLE [dbo].[PaymentDetail] ADD CONSTRAINT [FK_dbo.PaymentDetail_dbo.Invoice_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoice] ([Id])
ALTER TABLE [dbo].[PaymentDetail] ADD CONSTRAINT [FK_dbo.PaymentDetail_dbo.Payment_PaymentID] FOREIGN KEY ([PaymentID]) REFERENCES [dbo].[Payment] ([Id])
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [FK_dbo.Payment_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [FK_dbo.Payment_dbo.PaymentType_PaymentTypeID] FOREIGN KEY ([PaymentTypeID]) REFERENCES [dbo].[PaymentType] ([Id])
ALTER TABLE [dbo].[StateWeightCategory] ADD CONSTRAINT [FK_dbo.StateWeightCategory_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[CollectionEvent] ADD CONSTRAINT [FK_dbo.CollectionEvent_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[CollectionEvent] ADD CONSTRAINT [FK_dbo.CollectionEvent_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[County] ADD CONSTRAINT [FK_dbo.County_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[DropOff_Location] ADD CONSTRAINT [FK_dbo.DropOff_Location_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[DropOff_Location] ADD CONSTRAINT [FK_dbo.DropOff_Location_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[DropOffLocation] ADD CONSTRAINT [FK_dbo.DropOffLocation_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[DropOffLocation] ADD CONSTRAINT [FK_dbo.DropOffLocation_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[DropOffLocationType] ADD CONSTRAINT [FK_dbo.DropOffLocationType_dbo.LocationType_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[LocationType] ([Id])
ALTER TABLE [dbo].[DropOffLocationType] ADD CONSTRAINT [FK_dbo.DropOffLocationType_dbo.DropOffLocation_DropOffLocationId] FOREIGN KEY ([DropOffLocationId]) REFERENCES [dbo].[DropOffLocation] ([Id])
ALTER TABLE [dbo].[Country] ADD CONSTRAINT [FK_dbo.Country_dbo.Region_RegionId] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id])
ALTER TABLE [dbo].[DocumentLib] ADD CONSTRAINT [FK_dbo.DocumentLib_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[Event] ADD CONSTRAINT [FK_dbo.Event_dbo.EventType_EventTypeId] FOREIGN KEY ([EventTypeId]) REFERENCES [dbo].[EventType] ([Id])
ALTER TABLE [dbo].[Event] ADD CONSTRAINT [FK_dbo.Event_dbo.RecurrenceType_RecurrenceTypeId] FOREIGN KEY ([RecurrenceTypeId]) REFERENCES [dbo].[RecurrenceType] ([Id])
ALTER TABLE [dbo].[Event] ADD CONSTRAINT [FK_dbo.Event_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[ReminderGroup] ADD CONSTRAINT [FK_dbo.ReminderGroup_dbo.Event_EventId] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id])
ALTER TABLE [dbo].[ReminderGroupAffiliate] ADD CONSTRAINT [FK_dbo.ReminderGroupAffiliate_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[ReminderGroupAffiliate] ADD CONSTRAINT [FK_dbo.ReminderGroupAffiliate_dbo.ReminderGroup_ReminderGroupId] FOREIGN KEY ([ReminderGroupId]) REFERENCES [dbo].[ReminderGroup] ([Id])
ALTER TABLE [dbo].[Reminder] ADD CONSTRAINT [FK_dbo.Reminder_dbo.Period_PeriodId] FOREIGN KEY ([PeriodId]) REFERENCES [dbo].[Period] ([Id])
ALTER TABLE [dbo].[Reminder] ADD CONSTRAINT [FK_dbo.Reminder_dbo.ReminderStatus_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[ReminderStatus] ([Id])
ALTER TABLE [dbo].[Reminder] ADD CONSTRAINT [FK_dbo.Reminder_dbo.ReminderGroup_ReminderGroupId] FOREIGN KEY ([ReminderGroupId]) REFERENCES [dbo].[ReminderGroup] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[ReminderAttachment] ADD CONSTRAINT [FK_dbo.ReminderAttachment_dbo.Reminder_ReminderId] FOREIGN KEY ([ReminderId]) REFERENCES [dbo].[Reminder] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[PlanYear] ADD CONSTRAINT [FK_dbo.PlanYear_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[StatePolicy] ADD CONSTRAINT [FK_dbo.StatePolicy_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[StatePolicy] ADD CONSTRAINT [FK_dbo.StatePolicy_dbo.Policy_PolicyId] FOREIGN KEY ([PolicyId]) REFERENCES [dbo].[Policy] ([Id])
ALTER TABLE [dbo].[StateProductType] ADD CONSTRAINT [FK_dbo.StateProductType_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[StateProductType] ADD CONSTRAINT [FK_dbo.StateProductType_dbo.ProductType_ProductTypeId] FOREIGN KEY ([ProductTypeId]) REFERENCES [dbo].[ProductType] ([Id])
ALTER TABLE [dbo].[StateSpecificRates] ADD CONSTRAINT [FK_dbo.StateSpecificRates_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[StateSupportedDevices] ADD CONSTRAINT [FK_dbo.StateSupportedDevices_dbo.State_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
ALTER TABLE [dbo].[StateSupportedDevices] ADD CONSTRAINT [FK_dbo.StateSupportedDevices_dbo.ProductType_ProductTypeId] FOREIGN KEY ([ProductTypeId]) REFERENCES [dbo].[ProductType] ([Id])
ALTER TABLE [dbo].[MailBackDevices] ADD CONSTRAINT [FK_dbo.MailBackDevices_dbo.MailBack_MailBackId] FOREIGN KEY ([MailBackId]) REFERENCES [dbo].[MailBack] ([Id])
ALTER TABLE [dbo].[MailBackDevices] ADD CONSTRAINT [FK_dbo.MailBackDevices_dbo.ProductType_ProductTypeId] FOREIGN KEY ([ProductTypeId]) REFERENCES [dbo].[ProductType] ([Id])
ALTER TABLE [dbo].[MailBackDevices] ADD CONSTRAINT [FK_dbo.MailBackDevices_dbo.Affiliate_ManufacturerId] FOREIGN KEY ([ManufacturerId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[DocumentType] ADD CONSTRAINT [FK_dbo.DocumentType_dbo.DocumentType_DocumentType2_Id] FOREIGN KEY ([DocumentType2_Id]) REFERENCES [dbo].[DocumentType] ([Id])
ALTER TABLE [dbo].[DocumentType] ADD CONSTRAINT [FK_dbo.DocumentType_dbo.SiteImage_SiteImageID] FOREIGN KEY ([SiteImageID]) REFERENCES [dbo].[SiteImage] ([Id])
ALTER TABLE [dbo].[Document] ADD CONSTRAINT [FK_dbo.Document_dbo.AffiliateContact_AffiliateContactId] FOREIGN KEY ([AffiliateContactId]) REFERENCES [dbo].[AffiliateContact] ([Id])
ALTER TABLE [dbo].[Document] ADD CONSTRAINT [FK_dbo.Document_dbo.DocumentType_DocumentTypeId] FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[DocumentType] ([Id])
ALTER TABLE [dbo].[DocumentRelation] ADD CONSTRAINT [FK_dbo.DocumentRelation_dbo.ForeignTable_ForeignTableID] FOREIGN KEY ([ForeignTableID]) REFERENCES [dbo].[ForeignTable] ([Id])
ALTER TABLE [dbo].[DocumentRelation] ADD CONSTRAINT [FK_dbo.DocumentRelation_dbo.Document_DocumentID] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[Document] ([Id])
ALTER TABLE [dbo].[AffiliateService] ADD CONSTRAINT [FK_dbo.AffiliateService_dbo.ServiceType_ServiceTypeId] FOREIGN KEY ([ServiceTypeId]) REFERENCES [dbo].[ServiceType] ([Id])
ALTER TABLE [dbo].[AffiliateService] ADD CONSTRAINT [FK_dbo.AffiliateService_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[ServiceTypeGroupRelation] ADD CONSTRAINT [FK_dbo.ServiceTypeGroupRelation_dbo.ServiceType_ServiceTypeId] FOREIGN KEY ([ServiceTypeId]) REFERENCES [dbo].[ServiceType] ([Id])
ALTER TABLE [dbo].[ServiceTypeGroupRelation] ADD CONSTRAINT [FK_dbo.ServiceTypeGroupRelation_dbo.ServiceTypeGroup_ServiceTypeGroupId] FOREIGN KEY ([ServiceTypeGroupId]) REFERENCES [dbo].[ServiceTypeGroup] ([Id])
ALTER TABLE [dbo].[AffiliateMasterContact] ADD CONSTRAINT [FK_dbo.AffiliateMasterContact_dbo.AffiliateContact_MasterAffiliateContactId] FOREIGN KEY ([MasterAffiliateContactId]) REFERENCES [dbo].[AffiliateContact] ([Id])
ALTER TABLE [dbo].[AffiliateMasterContact] ADD CONSTRAINT [FK_dbo.AffiliateMasterContact_dbo.AffiliateContact_AssociateContactId] FOREIGN KEY ([AssociateContactId]) REFERENCES [dbo].[AffiliateContact] ([Id])
ALTER TABLE [dbo].[AuditEvent] ADD CONSTRAINT [FK_dbo.AuditEvent_dbo.AuditAction_AuditActionId] FOREIGN KEY ([AuditActionId]) REFERENCES [dbo].[AuditAction] ([Id])
ALTER TABLE [dbo].[AuditEvent] ADD CONSTRAINT [FK_dbo.AuditEvent_dbo.AffiliateContact_AffiliateContactId] FOREIGN KEY ([AffiliateContactId]) REFERENCES [dbo].[AffiliateContact] ([Id])
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [FK_dbo.Role_dbo.AffiliateType_AffiliateTypeId] FOREIGN KEY ([AffiliateTypeId]) REFERENCES [dbo].[AffiliateType] ([Id])
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [FK_dbo.Role_dbo.Affiliate_CreateByAffiliateId] FOREIGN KEY ([CreateByAffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[AffiliateRole] ADD CONSTRAINT [FK_dbo.AffiliateRole_dbo.Affiliate_AffiliateId] FOREIGN KEY ([AffiliateId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[AffiliateRole] ADD CONSTRAINT [FK_dbo.AffiliateRole_dbo.Role_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([Id])
ALTER TABLE [dbo].[Permissions] ADD CONSTRAINT [FK_dbo.Permissions_dbo.Area_AreaId] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([Id])
ALTER TABLE [dbo].[Permissions] ADD CONSTRAINT [FK_dbo.Permissions_dbo.Role_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([Id])
ALTER TABLE [dbo].[RecyclerProductWeight] ADD CONSTRAINT [FK_dbo.RecyclerProductWeight_dbo.Affiliate_RecyclerId] FOREIGN KEY ([RecyclerId]) REFERENCES [dbo].[Affiliate] ([Id])
ALTER TABLE [dbo].[DropOffLocationDevice] ADD CONSTRAINT [FK_dbo.DropOffLocationDevice_dbo.DropOffDevice_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DropOffDevice] ([Id])
CREATE TABLE [dbo].[__MigrationHistory] (
    [MigrationId] [nvarchar](150) NOT NULL,
    [ContextKey] [nvarchar](300) NOT NULL,
    [Model] [varbinary](max) NOT NULL,
    [ProductVersion] [nvarchar](32) NOT NULL,
    CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY ([MigrationId], [ContextKey])
)
INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion])
VALUES (N'202012222235212_InitialCreate', N'Mits.DAL.Migrations.Configuration',  0x1F8B0800000000000400ECBDDD72DCB8B22E783F11F30E0E5F9ED8A7DD72F7FA8DEE39614B722FC7B12CB5247BCDDE370ABA0A92B85C45D666B1646B4FCC939D8BF348F30AC37FE22701648200C9AAAE5811BDE4422201243F24128944E2FFFB5FFFFB97FFF17DBD7AF1C4B26D9C26BFBE3CF9E1C7972F58B2489771F2F0EBCB5D7EFFDFFFFAF27FFC5FFFE7FFF1CBF972FDFDC5E796EEA792AEA8996C7F7DF998E79BBFBF7AB55D3CB275B4FD611D2FB2749BDEE73F2CD2F5AB6899BE7AFDE38F7F7B7572F28A152C5E16BC5EBCF8E57A97E4F19A55FF28FE799A260BB6C977D1EA225DB2D5B6F9BD28B9A9B8BEF818ADD976132DD8AF2F2FE27CFBC3D99B0F3F9C172CF2E7BAC2CB176F56715474E686ADEE5FBE889224CDA3BCE8EADF3F6DD94D9EA5C9C3CDA6F8215ADD3E6F5841771FADB6AC19C2DF7B72EC687E7C5D8EE6555FB165B5D86DF3744D6478F253239E5772752721BFECC45708B0965239EA4A88BFBE7CB358646C19D7FCCBDF5FBE905BFDFBE92A2B4B34C2FE4161F16F2F5AC27FEB305240A9FCDFBFBD38DDADF25DC67E4DD82ECFA2D5BFBDB8DA7D59C58BFFC99E6FD3AF2CF935D9AD567C978B4E1765C20FC54F5759BA6159FE7CCDEE9B81BC5FBE7CF14AACF74AAED855E3EAD4237B9FE43FBD7EF9E263D178F465C53A447052B8C9D38CFDC6129645395B5E4579CEB2A4E4C12A6128AD4B6D95FF6D5B2B20584CA8972F2EA2EF1F58F2903FFEFAF22F7F7AF9E25DFC9D2DDB1F9A0E7C4AE262FAF51D323772C6B68B2CDED458D1B6F5D38F3F3A35F6317A8A1F2A6148CDBEB9BF8F8B099733010AC534BC66ABFACFC77853CFC61F60DA3B0087EFB2747D9DAEB4FCD53A77B751F6C0F262E829B1E24DBACB16D2887F79D5CF16F31C025B719848209FE36C02DAEA44A5346AA9277F7972FD3C7EEAE6F1DBB440599410268A8AF2B1A7488B74F21469E716592998C778A7551EC0E074C4FA89AFADE167C60F98E4C7790DB46559255F3BAE5C323497CB8C6DB727A3B5F43A7C4B578F6962169D8F56DE45DF0D6DFCC94B1BE7EB285E056FE59FECCB4D9C9B24E6A79D0FC56623CE77CBAEA533B688D75131C0ABACF8ABDE40FDEDE58B9B4554CEA83F5BF915F3CC233BDC822655BAC9E92BEF6931FB8955FE23DE183ECFC9899709DAAA630763E02A7A5E176AED96656B62CD8F69CEB6C6C9FAF35F2DA3CBB39D7D3B1017986856D3D06DBDDD6DE3A45075FF28D6D510CDF9DD7CF8B13AB4B694D54E210FEE344DF268915B86D550DD71F609301E854A6F3EA9A490DD841E4156F0BA8E2AECDB86D19222C6A2929A0704D03B8FAA646F194D4962198548A2EFBD44E7DCEB1B963DC50B5BC7EF007AE3F468C93013A3A3759E12D52A841F42436D1E404584EA7E4DE9DCF9BAA2A5F73591053B32911E3D0AA5337EEC5BD76ACBD8FD0B96BB4463E8B84448EDF769B5F300BA5B16687A2915299D93CBC97D5AC585EDD0EC0FAC3056A821184B440618CB945418BF4F9E52ADFE680A61D82A858A60550AAA682F8AFDC3DB68F1F58C61949C420D895622328856A6A48AF6224A76F7C50255ECFEB3ABB4D8F3C79A112884CFB0C44D748AF08DC4D4EFD0D8C770F79B42B8CF4AA1D2519582DABB6BB6785EAC8A6166E972B7C8FFC9E287479B42BED355823003D31AA0A3A94045D0355BC7C99265BF65E96EC32B376064302DFC516CB4CA37B256207F32BBB175D7D0801FA42C32C9BF2AA78ABBB204C04E55259AE5452E5384A710F871583606FD00BF65C3E1E8BE04DA7A1767DBDCE2C33CF1E3EFFB108DD5D245BC5CAED8FB242E0FCC4D5BFB9F8EBE59B8A5C3F1CD7EDAB26C1CD48DE305BE8AB6DB6F69B6348DC7DA12C63D76F4B3BA1DD8DEB0C52E2B86F4FB8E6D4B0D4D3DAF4DB6DF58668491272FFF439CDC66CFA7E92EC9493D2CF5785D3BEEA7D559B9E9ADFE6DAE7C1BE7AB11E6A2DD5DED638A94768FEDF30E3D6D7675906A8D46BD2FD5D5C58BF3CD29ED2AB531E3922A118629D7741EF545817F9639BAB7E5CA98318B750843962A7A1AF1C9A0219FB88CF9C479D027F451EF96717EFEA47503A82DF3154C63EBE910C3E188A923403A0F2110C2141657A2824C6C47CFD2C54EEF6F694B9586F80E6B89943EEB295D36F5B06F4238EC80E40B53A87E08988CDA51D91481F7FC12917900766AD52F60AF421E18D683010E0126B1F9338C9D75766B34CBD170EF46C3E8E8E400DA7AB3DDA68BB801529C6655158A4DDED73FFFBE58EDB60E1BA3FAF380311BCAC8911B93E6CB0FE6076FF46C5CD0E6E11466213E3C406750E2756CF76135EAB52B478D11430FA85844A5417A4B1826555771958FFA0968CB977F8A38F171772F8AD6435DBEC0EC1F83CC1FD9FC264DBAD1E2888439678B24E287808B25E26B8489CB01C58AFE68B4AF35FC3351025C6C9F4625B306B90CF91C4D04C2FB9CAD8D110E2581AEEF1A125DB483423760E1AC8E5BBB5EDBA02450EB4683A1372DA3FA4AFECCFF76260E34FE5B36C7A51568CBE540C2DDF3AFB7F12D873A75FC8443CDDF7751873172A345678B35DCA5D9126F8568B3FC2CEA6F2460CF00CACAE7C9D2B5AAE91EC1497F91E067ABE8F2E737CB7F1533EF8A1513BA3F0481F8FEDCB1FDC92AD65594FC3B8B32B33C7D1C0B0C0B3836BA200D01CAE8582A1E5E7040154F71A73397B8182B04BD1A7685A9448E13EBE7AB66645D396A5C366A6054D62AD431F19A041C144F801815825C1916A68EEF0DBA67DB1B37AD202B2488F313FC34263A8C1BD4FC31D0C690A421A8469050FD68FC006DD572D5EEECB1191C82B815EC291D1C2F55D977DB41D61D79DE93162BC7E9C32D42F4C9D3553E4E1DA02D8B4B6ECCE427F6801F4CAC8A2D22C635C58ABC38C5C5B6691D3DB0F767A40DC5F9362F8CEEE24BD531E4461BFF47BC8D5F731336498EFDAA7C2B574D60419C308B7D2F1FC9B7B506ECD70A1EDBDD9A65850E638BBCC4BD7B560F8AB2F46CCAAA8A126DFD3ABAF49A1608E393EB59062892E34628D571BCCF84F3F8092B05ECF1E3FB68F2F8F174216E39F1FC0DF79C0C6446F10FBDEB44DB24712DEBB7463C91794324500ED9DEC1115CDAB64E507D3FC177FE84BE896B971578DFD3966A250E53A87B1D98CC293C45EC8A05EA400D18EC0AA111EE2AB5D366F366B7D9A459B1086267AEA696614832B17D584A8D41C634AC970778E6053E47131B684B1255389739D64EA05D6DD7DB08C82AF86BEF1AFB22A8B772980984F2572286E53A7F074FDCE38C05DA72394CABEF648F769286D83EFEB5DB3EFE18F074CDF974CCF164CCF5D012775C2583AEDC091793AB80CD595172797FFF215D44BC1F03191679152DD850E51D222389D68BAFCD5D12626F1A70ED410F70E03EB59EFEF0B223656BE8E26FB8A506265197170DDD48676040CF8D84C8732FD751A04FBCDC83996CF019E57C0B103B4C613BD332091AEF8E6F904EF6C457F58EF606D096AFB8D89987C19A020F07284965076D51A678379E7BBE9F76921893FD34FF87C8F4D3FC9F9B7FC6D4F596B148A8C85C28D7095C241AE4A5504540D7372A8FA3EE01DA720E021C6B8F3434B1BBD17CF692AA4B9E0FA8BC5E1E0CCB817A07D16D4943F9B26C60CD2A5B371095C6C2014987DD03AA8740BE0154563BEA19A02D8B8DE32769CC9B2F5F32F61447161BE7B5D4561D57406BAACA27925995D9C0FCC103AEF92AB7492CD781039CA97B89D3438CC3C3093A6A00E63C71968E86C9A72B35A2E6D205096C7D0D9943D779736BE9B3AB995F65A0B866E5199BC63BD513DC35CB02E73C900BD5D30E85C22149AECE7EAF1254289DEA7F85335A387603911757A0013AA616AB3D0468C85D4D57AB3ABFBE21C149CD5A25958128536880A89091D399942B8AF6439785D0A7E67E5745C917D265582D70FADE640DE7ADD29FAE04EE515FEC9A48E543FCC59C4BA5205065A5146A73A7F414E41ED6C70477FD3901D44B8908E82A48A1F6172673EC74CB45235A9148DB675C9707F6D830ADAB22B577DCCF4A9FF832C72BA9E684DB4A6F84026DA26DB71EA1FD56833763B02234EEDBD0DBDE78F1F5D3C6BCD80834AA8C8162F54005A021871D34278B26217334B264BB228D38FB72A76D3802015AEF1F50ACE9A4DDF7373C8E8C6F4917FE0693183B3D3C6A6CC316F17DBCB0ED832062B0EB028DA9EF2261B89037BE4943B09B81CC380A4C809B75207540C469F1D7439A21F0AED2830391C94C035168FD44EA39BAA2C4FA479F14D096DEF74DBC0A063BC3E7929F2AC4433CDA983EADABC1FBB138D6D76139143774133F57FB3DBEC344ED2B1F67297A7299EBBC8B16C5D7CD9F079FACA33288971FD025966D600A05FD4C21BAAC942817AD53CB6976D4795AA9D3A2AC759C0F405BA39CA4B84CBADADD14F844C439B7AF0C724B0A604F27088477CCCC1D74F02792FDB5F544D5B96BABFE98BDB5B5773984B3B6E26CF2D58204B04CFD786A0D9ED1E7BBD665AF386AEB029DA7B6291DC7E9287F6D90C0EE7274FAE64E1E474D8751FD1DD65DBA374AEEAB5A6AF145B9F5334B8B0DF4B658120A132432CC2485509A4852393C8F64227AE624A13E7C5B136AE9C4D65F358F3E4CE52FB0C57A100776C88771C52B60072B8BAF7E34B780B6BC39094ED3F5264A6C5B92D79E22615A40E81BF21355ECF0AA918B6D69790969CE8F810D76D0383D31AB77D0E81EA3258423686C1FAA31A9E844BDBDE973A5169B516AE8BA2C115A7A2F537B5C6AE83116E6AE7A588584D13ADC0DE1AB1F572148CDF25FECB812F95A56FCBFC937E7954871929A9EA5F4D386EDB1484F779BFA2DFD05CB1FD3A5B3134ABBB891B777CA2D19FD0ED0C9A3137269337B79740B21DDE1537F2C8D234524320EC6420BB85B2C15FC1D5A3904A99891E363CFA87C00F2B651E2705CB381B65AE16A95DF5F3D6F2850C6AFCF79A5D895D889E8D3A125D2DCA9E01652BA9848A1742E46FA81698179E64EA94DB9FAC71908B42588887C3C5B98C9CDAB8DFE6F4BC351A454474A3109CA1E7E4C839F077E66C932CD46DD38D44D8EB67D684648DF10D4155D761F75CD313614754BE36C2B6A9D3F2A54CA26779BD1A05237E7F2C5EB9A0E20AB2B12A122DD6BC4C4B2D4ED8C0794DD668C5D68DDD2587BD16AB3141E85D76C11B327B67CFB1CBEAD66DF12FC3081B88DB70602BB47A3981F97A69E9F82D7F07487AC942E7A3D34C5F4929ED316B5DDF768C403195551463FF1C68FF1C28F011F1A12ED25A0A1280126853EBA56A4330D03410E47DE5AEA0CDA52759F86BA976A2A1E3751505BB56CC205B8C2ED5DB12C4E9725E0A8AD0A0C6E53C7EA673BA72117AB328B9F30AF16C8619D96872B5EFFF8F35FBD2CE58FE937A12D64FFC64C1474CD1E0AE0DA82B7FDEC1DDE6FAFA278A908837A7963B3C9D2C218230AB55032F17D0C540B74EBA35D54C024474AA1B294AA148E8B67FD20E15A1BEED9360491AB1D56A9B43D0748833E49D1AD48F073146DB74C4F5174B781DD6E26170A294D16D5E7D2465A4AD251AA68452E51DAC42E933B8AFE53B289165FDF2F5183E188B5C3E8686C03E80987761D36DC75CD9D603AAF463DEA29C98EF8E8B99C2D672C2FF6D01A3F3C4F02011F24507DEE20558848D801C6BA4ED0434362F5E788C85402BA8E7938355454A7B399DDB3381ADC505B9AC38020EF1362AFC051EDA17579A3A1339EAD4F1853D9770842D9E883F6F2CE36886E476F3057864CCCD258709E9265E5E364244C46DCC9DC68EF4134359D36E9533CCADE56C56B08CB8E911FF8D007D39BBE857835BDD0098C24A8AB68B562F9C7DDFA0BF12946BFEADFB687BE6079961237BF9759FC1027D10A8505D36B2643D43B6597A631AEC09D9CC36E92B053E39B04AB1A0702D4C08C0DAAE66FB8C60D91A51BD0F6C856C571C4F4AD137D1FCE7781F6651DBEEAD02F7A6579756BC0039DA6DEF374D42ED39E3F11FD3D9AA74F7822F3B32702A5EF274F7809699E3BD19018856D7BE6C4C554157137C87015591DCD58A0AD5644AC0402273AE233D11D9BDBD49D0971BD271CDA0B3BA131574F84FFD6B4E83A8C6FF4E5D26D8870B800555F78D215473D61D51397E7179EDC4F8D2B9465BCA218C873B0E6D0A933A793DE215B4E1F1E29F7631A8B67CAA39A1AC7CC45E826CC98D06A493420A9DA88AF7D5442405B9683FFBFFCC9DF5B21C123AFF98FFD5B96EE367EF263991FDE80DF59D4BEBF812057B607983A41DE8FF0F434256E3CAE8F4A58630406EE308DBD07F7A20EDB7944CFAF348998B544E69E5F0D49C6CC5786AD636D5B27A8BEAB2E2303E9109F44A527AC23A8A8EEF85FB49F0020353A29207A6F0B6633BA21AB66C5E2B874865B3A9DC3CF24388685B0693A9A203F642BDAC6D0386F425B0647F8026D19B69FC4DD26ADFA47F64DCFC1E55225786AE663D3470B67B36CF4D4B837628F8344B1613B3DD0B724049A395CBFE6AB1F273334276B0911BDC38EF33F8A97D498231FB3D331E851B9A3640C8D2406699A3A7A2707722A3D952874F19932998FA9E83C098FD30F68CBF5C286530C51FD1DAA4020DA741F38691587EE3DCB58B26062C04CB0B408E5459A00D7621D6F77B45313BCDDA114EA94D090C7AAD151E2743DA4EBAD465D11BBAC3D51E7CADBA6A0DE0AE53A9D2912F9D0986E0E68AEF2517342B3DABC89C6AA8E20F1DA3FF956293DA87DA15F3353E129E2847EF04EB3D35BED2293E36C00DAF2F2B8D5D029E57409E1BA1C3FF2AC18710761D065A300D7EBC1A7E89057F2875F48F2F0961EFC3297F9DD3D277D21BDF83124DF61C5E0A82742E90930351291C77FC49BD3E2B30D39DA453DFBE521DD14A69D7F14C80FDF4AFF764CE086EC5B26BFBE5573221FFA934260921CEDC343BEF4ACC333F5B07645F594A058EB9790E8FAB4AC7754A3D0FCF0B3F9C004BFC4B6192F9F164A09E5FCA56537CD5EDD63BEC8A7B6D41CB5D0435CC3E769C3577D8F81FB5DD7190FB7B3D537C0A87352E6709C9DF4D9E9290F5E635A9C8CD6D2EBF02DFD71DE34F0A3A2FFC9BE6C63A3A9E4A79D0F69F210E7BBDE5E8636CABD0BFECF567EC53CF3C84EF32AB6873507B3F318BCD138F13249BB70C467F245EBD6F9C63235FADE6EA80F4C4886D90F9CC5596D8F8ED0D6DBDD364E0A7567DF5279696EE083B8DA9D8BE3539EB2F1617EF073B845A4F0576C2398C2DE517FF6D26073E9682D85B496087E9861C612A1A161B6122A09F70C4C253FC6C568A6D2CDD1549ABBA914DC4C3A9A3AD3993A97DF8A35A279F32190B9437B075C63430CB3752426FA2077B935B8A27E0810BD75486025EFE69CD59AC31973016D39B7A01880C9D1A683949828A7A18775E096D6F9245EB1EAC79A9BF2610B6942634727021C181A4F601D9795581994BDC6A0C3A461F3F70F3B713F569B058BA5C99ED86A92181C5C54DB9F3C1FFAA2176ACF33465EECF073CCFDF8D525F6ADA9789C27077EF85A3FA4E0EC81ACAB8313A72EBAAB81241DC0CA65CA42A21038854B188E85B3E64C553D17EE4AE0B3D8BE78D0C46C05479D9775BD3FD4B4FCC3C445181EC8EAE6D0D08926635A3B13DDF659E962577A583EC45F1CF6577DE523BE81B66EE37C6502B8A763F8DBE8618417323F6D5669B474B949F626CFA3C563099442828F012E428C7AEBC283B315F786968B3FA79F90802F472E54FD380AC520DDE218987D0CC776D527A3CE1F44542FAEB542146F8B352DA1AA9592EE3C5992B551E5AE77D16315301DE235AED962975557581D2ADF14725FB19EC5E768B523F7BCAF5EB4C9B2A768F52ECDCE228B5B1FC3E4224D7A95EEC8E6F75D318C6C35B437A546ED99A09F06DCDEB2F566C5C10179CAD656B318B1632E33DA15A3432EB86A74A5776A20BA5CA6ECFB1402EABE4F9C1D1A7B9927017A0912005B54888ADEDF759C2C59566523D2D9F71CC95DB30CF2BD558A81BEAA34FE2E2034FC640B81FB59B10DF8B2E156819B3FB8AB7AB40E208554C9547F20EFB89D362B15780290340A8834681E3BBA6D44F54277DFF0F58FC00B013CDA418001778E0B85EA62312D278E30E4F4B90B0AB9EA4710026D551F896A98A7DF869954BE567F15807A0BC1C94EE9C2CD11064B9F2B4602AD660C1A7AB349A3ABE46A8D610C318E56F7393A12CB27E9E9FC69052EC5CF20F5D0F139EA0968CEF3A222EA0B74FA2DC7644F9A1902E67EB2D19A010C5518A460C6532BC871C1BA68D02C759F97C799E879268E74B23252CAB92B96C5E992EA182CBEC26E4B7EC434FD76999C462B962CFBF3076C5C7951C9C96FBAAEF3C19976083EEE5814FDCB5B50B9F4F372519AFD51827C73559F3BA8FA9C70F2ADAA08B644E43235E59C4CE06A2CF5077266B3E90EACA0EA6B884EABA741E2C06B0FCDF433FBE2868BBF9AB9C64E5714E64E4B24DA4ECB74C3521436C8266727ACEA1D5741A0AD5A34A69B6EBEFD76E62D134943296901752A6C90DDD52B0A770BACE7714421D0562B26EA86C85F6407EAB666D79CE5D0CDB7CBB1DF01845A2D75EADBB8B40E9A55CD2AE43AA1AAEAC7B904B4551BE6639EC4E09C600E4685CE86D3191F6E4645170D45362B9A9A4718026D010166C47B1EC557CE72A70D9FDB3ED12DDBA473FEBD163D40E2BDAE489371AF2F1FA48A2B6657698146D75CC775E523FEBDA149B6CE2BF90EE5A2B9C5EF9E9CB8810C64BE5745773D38A4F072A85C75344044BED35F6ABB08149BD20A1B3B489C8AFD737FAEF3B1E7709C94C126652F647FAB85F0EDA189D597DFC99F5A9C61264275AA19A9C3CC394DC73524C6B967EB2C6DFEDD6CD822BE8F17D74E87A00A8BE30C0C35033F655FA23E5E3B2D24C7E899FC7759B41AC8E382E5593A90C7C7347162336C12F238D5CD4281C6340D45420FF370B7D9A45981AE33F654BD7DE63615452EC7D9186A36CE633D943EB7794D9489EDEBA25223C8DA681A8481CC3839311D47CFCF8B285EBD8D165F5D67A658FF382781B65A110D9D961751B2BB8F16A50C55BFFA88535CEE5609049377121B3A41F526C5FF6572D9077A81AB7D2D7A98CC3E32B67C9B7EEF13CAA1F7F012A38289273958DBD18CDC4FA45A1F67252A1441616A891475A9A7A46AF9B63ED8EBB6D0D4691D8DD2672D21B5CB9495D6D07103997175C5749FBC3CB92F4CC72509D2D98D70ECEF13FDEC4B7B5B67580741DF134D3E74B2CE48B78D4E5C18DEEBE8C16577D3563D621568CB7234EF2962D21C6EE09A01559F7CACC98CD038CD0CA917EA1D4B0F2E35FF8248A1A61984C906AC2A9AD70B3BD65A872048A1BE51089379494BE3E68AE76B1FE728D056F7C9882F7BBF8B57ECFC7BCE92AD393784F5313E5C6608EE33DA027EFCC47B9FA649DEA12E7463EFB7754A9DE84B1FCAEEBAAF78BF6DD4CD3036E7DB45B4616FD97D81AEB378BB59F52922869F5AB69FD3AC3FEFC4A9AFAA4F8140AB3D452AAAF2E42B9FD8D5BD44AE51F9029536F70E404A4E69CDD57E1DA6F7C6450B1E28DA4FD7AF9D3E9759A3BCD5D578D0AAE5BE621D572BCB3AE07A63AFD4ECD1827A41F886654F2C2B573DCBFA8330380949D6466FD0255AEB8CAD583EF0A68CFC79CC2B834A0DAC0E329176E2AB94AE7AB6EB336E61E3C80DAB5B47A51F804A3A64A5F0B92A5BBB2CACDD83746DDB69779DDB7238EA5E83EE55360A44CBF25DD18BF821F1C4E6B6AC42E6659D0661E6AFD54455A73AB6E3BC38C0CEF304E6019829954158C8073991C55151A7355FFB38A52D73C86665FC494E8BECDB9B8C5B43BDC1585E9890A8778271675F9456A4D361BDCCE10867A02D7D520EEA01752D641F67ECBE03BEB187B1326034C7B10A99E14056A5256FE87BB9C28EE8BEDC3C0023A11A8263A41E1624D7B3AEEE84BBDBA03A4EC7996E9EA02E39434CF3DB693A0798000208EE848AE03CD0D2ABA734984AE497E524A6A8E1DDE9BAA29BEC965AA6996FABEA550D0C9FFEC7690FB435CED10F2EE9F9896BD273F414321BC2C1E7924D6D20A6E13063F922DAE6E5B3A38D87CFD96416F81CE715D0562D21AB879C6AFA6EB7E9C223BF33761F151FE8D3B6CFC3E5D19E36F99D15FF308C2DD8C6B6D4D11BDCB68A54EB5B660C1F67629B3F7119AC7A5C88AF3968817EB35BC6B9E32B247DDDA3F280A679299EFA298DA133FC5C0853767FC512D38AEFE343EACBB8F19ADDE4D1BA7B38BD3F32A3067C6CECB1A05EE456BEA3EEBD0DDF8AB99BAB1665DCD3215412474C56BAFDEC8047D0976B3B0F93A8FDD6D00D579C6DFF9D34675DF9A83AA11935304036E09BBDF6B4A9C4E9CC63DBEF3C90372AB6F9E2340F4AD60E29B2D2E3C913D856A7631D7C70A55047092D3FCD58D1C3B7CFA1937D73A9AA8BA1693CF55591C13B5F970FDD1368F28615BC952019316D1848A1660D83C9A8CEC68E43232F934CAB26EBD90B88B52BD54BB6277116AED6DF2B5018BE7E5FACEF274743BE21C0B275BCDD6A7D4D7DB92249A908CA542C940F3346F86F32C0237454CB36B5ECA092432B475177E867B3FEDD031D1D39E574AA89B1212B1D730F8509356CC2D41AC879C29444C70903B455CB751E2712AE2F1DD827A66589150FD60D2B584FA347BE4CE8323931FD452DB6E65E8A0BB2D304ED9747A754EA4DDDE3D404DAC2AC4932E00B839FFCA26FD455403E9EF16649ADF169B38CC8AFD2D651F9CEA10AA52CE0695414DC0916233787A432D55A9509C82F27E8D65E828D0AE4AA076D58B725B7921B79A52D6AFDA166F1C734B12E7B5751C6C82FF979F20C109D6AA751A8D5595605D71FC237F27E7BC1921D51DFBCDFD6D02755BACC96FD692E71DF60DBB5921495B2D2EB3499637CD06297153FFDBE635BD7F04091C31F4A596097FC5EBC1318C9460F9AFCF9CCDE343B35109D63AD32F4F1E4E7C58A654D62877FB2F8E1D1E9111680CD11CB405BADA4861E34F3C9FFA12C41218E982E0AC81993B28468F4B610FCAA0654213B163FB14E72676C11AFCB14B35759F15785AF97277F7DF9E2661195ECE942BD4CFE1125E1D89F5EDFFEB68AFA137DFF0DC4D96217E76FD3285B866BE56D85F998856BE1327F64D94554B612AD3C3633FC2007D273BA931D90D674D4035718E45A2F5689ED6EDDF12C4BF8AC95744D6F613889CE17F2709ED1F5BF5C7F7CB7FCC04BB3FC002C1BA5BF78BAEF27F5A0F8D6F17DCC540F88910FFE2EF8DBEAC4DFE10A785D711258562DBBD8235DC5E1E62E35653B7BAA14AB2684CDB25223EE9B7A89582B4385C768A70E36313EF4E5A195CB55999D2332B452FCE923CE8F7D1BA59DD3C7287960CBD374B55B9B8567BF6AA1B687571945AF2EEFEF5DD3680BD58F3B19A0AD5A3463CC43DC39D91E06C33620FB902EAA5253C255018F77BA8A5CBA0A04BD9AC505536958521788A7F3EC14D91C6729347744490DBFAC534ADADF231492961E0BF84AC616CA6CF101FCB2BFDCA67320FE056EC76900B46559A63C1DB03437364C6F7DFB6DE975F896AE1EAB233D43333E5A79579A9E81E37ECFD7516C7A17C34F2BFF645FCA6493C1DBF990260F71BE5B32937BEC6F9D77ECCF567EC53CF3C80E975141BE2A0D3E9664F379922F7DFD47BC316D4B6C9B3ADC041D10097F153D5729F458B6A69E92A739336DB95CD3A6CBAB765C60C2B637F6D4D6DBDD364E0A55F78F62F5F5DE1C7AF97E9F3CA5851170CDFE7317974D17EDD0576D80C9348B75DD0FA735BBAF3A46A8C8E56A29BF72039E02F48AE96704CBEA48C917BF8FEC1BA68B3F0907155696D62E72FCACBAF82A8A976FD6E9AEBC25ED3268F42C293ABE7DC74A2F30756AB435FF50C62B6606948231DA123EBC1EA5E43FC4C957FF1707E585C39A4C181F62DB2F920E31B67DE53F14E2467EE285EA5C889EB7B631FAF014FEE4B233A23DB0FADB2E2E8056182E77CE896E603E47B8026D797964D58279FB9D0FA7F016DBF61ADBAC65EB7B61DEFABA6877D70971C6A265F9C79993E7CBC0EC3835269A1A58F4909703A31B654CCCD6C6D140AC964C8E183D2C8C5AAC66AC79EE274A46C4DB50BC4E02D50E0A54BC2231E46753F65BFA54D0161FFB6DBA34E5B57AED12E100BA94B73697B28F764ECB61712FA3871ED7E5F9C565819A87C81EDCEF256CA8BA6E37C2B83E44DF9AB7E809AEDBBA772EEFF45CB3875D7D6C5AD60B3FBCBEBD3AD4C7778B687DF7F9EEB4E8567C1F2FCAABF56C93660EDB2C80C9249AEF3615C241A9FA4FAA3E86162C7000451458F63B75DCB0870855EB3D0BDCFBE81A0737EDD94C4EFAE39C344BF10384AE8EDCD136A87DBCA371A945D311F99FFF7C3C211FFF84BCF867615E6C0BE6961D9E9F4766C5E64EA8ABEBCD63BC297DC51FD3E082A915AAE76608AB69A3FB2E0B2BF073CCBEB9ACA5128B498F37955514A46E5631872557B754845D6E476BADBC6B07BE5C8C5A405D0CD9A6EA15CBE2941804926E1DCF36555E9FCBC07563180C85DB9BE5BF8A49C49676AEDC89EE5F7058F0744AE37A49695676061F3343B4DDAA5850E219FE8F38AE05941745AF2EEFF9EBBA438FF3DBAE569145B4B1F22FE4D06A0A73B3B43E074DEEDB945ABDAEF7B6C07DE256F59CBBD83BECE4F9A95DE5B6C3C281603EC788A06344D0E144047DBEEBCD6F770F8DCC63923922EC2322A475D9CE8E9CAD91357E8F76E0A109EC348A3275C658A7AC3A923166AE248A319AEC6589053935394525FF31867274DB1CDD36BA36A674DB10CD3F97AA57AB28E1ED679CA59BDBE78AA774F2E97A5DBF69600AE2F193C0CDC9393DA207EB334B96695648641325CF364DE5B349FB5B399ED45433C281EF19119AAA805C943CC5C9F0DBD684762D97643CB6348E22BE8A175F778837953CE1A49C74E37FBA7A90A3A0B36CEAD366D4A95E8F8E08CCFA7C9974C05DB733262CCD5682CF966CB682A7D4E6A7AB9825A6535E7F5663CC9ED8F2AD250C674A4F68BBFE12AB5DF519B62CD3CBCF8939E71BB4B4E7092357D16AC5F28FBBF5175BAE58A7287F3F91D365CEDC3CEB2C27E421C8695A0CADBA2159D47EEC0F33806EFED58B2CBD3AB02EB3F8214EA215CAD146F089D719ED4E8B19F490669EDF832478A4AED93A4E962C6B5421DD1D253098C417D5F6A07AA059F5DA827584476010F4B51240103672C4503E3589BF51E7B2F5105D7CD28A7846F5D08C7635D0A2A583DCD1B2DA0F615A6D8133CED1778FBC31DAAB06771BE72BD3D774BAB3A6690BFDEE8C9726C5053EDC7E44881A08DC0C31E8D1F6717DED398BEDB48B83AD9AD346FF959FF8F2A27F793BB95CFA59D2399D009774D6F35F40A357169C5B48F66297652C59B8A4FFB829C4BF623D8BCFD16A37A007459B85651FADDEA585D069C1B02013210BB8239BC2B22CFE580DED4DE915EE99A03F6B9E478BC7CA236A5941BDA4146C1E691BD00A6AF25F2E4AE94409C6C74E3296D36411AF58EBBF67B9B3D10C309A26D821676B873887AAD678817E83B149F369D4DF43503561A3D71B1C10C3EC5D6A11B7B1B373CC147AAF9E3A9CD8C05374E7295EC654D4057915B6B6759BE00A9B49639970E1169A0B871ACECBF8295EEEA2551DDD57D6DD6DF1CDF439C19CAA936B75ED0DAC6EAA39BB30E9512F82EA10A157A0B63752DCF6C726784DD29769BB6002FE647D99C157091F895269FF7142941ABD620F63A405465661E44362234187521497D7C5FDF61508A9097652CFB545BE41D5D4ADF68D1668FCE4E5C8866BCF25C69D1C6BE414A0C435F68F68756FFA823F79FC82BF47BB2C37BAE3FEB68777FC5A78EEC6990D8D9618736857CAC58B715ABC35058D796DAFDCD29CF2FA91922BD7B572BCB42EC827C805D9BA8973BB4F572FA94417E2FB6D3934624B6F1EAAA1139AE937932EAAAF7D6C12B51A62BC836F0A85F390B0A577C69F9260ACFD322C24DB191B7E39D7D6AB9D27251EA0C1817D06FEC5C70C9CE4DA5C67F467E3AC16757BE7E23950B0D69471A14F9CDC0EAAEAD15175618DB26A7528D6B4EEE90F1287374F88F9F4BADF139449E4315335C424BDB625E5B13D0E8B5CB597589D40B9375BB0F5AE150B3977BDF5C5B3F7B84471BBB7F725039177218E309DBEEE0F34BCF43880107C8FDDE3903B310618BBC8DBA31064C6FEA51148102164E03C7CF2D94A776AE57AAAD230D883F394FEB00D13E357D900333F50D0A4110B75F8CB9B45BEAFAB1E2F91EEE725D209AED54D75ED6CE48B7C0868FED9CB217EA1B4C6BB92EA7A536522AFED80EBC423397C1B07C0D1E1EBED6EA478212AE4A5F5C3BE44E5F5E2CFF18E6C88A68E175687B434D685D5B16FEC7978AA1168C7756BFA2ECD2ECF2F066F506B36C76DEA719B7ADCA61EB7A9C76DEA719B8ADBA692DF10386E53C1D970DCA61EB7A9FBB94D2505E91EB7A9C76DEA719BFA47DAA67677E0865C4005184DB2552D57CBFA9A126EB35AD2F761A0980DAB1A0B49DDBD0A7D1C6347290E72943D2C256254BC204ADCD076C21C65E5E82539CEFEB96CCFE90A047F9F9598502385AFF9A16A8DF2116E53A7C0EDB6DA385FAEBB4F4DECA5D38D64039BBE1F74265C1710B398746BABE91DE66E816E412B144CBA882B55256F7ADF2C16195BC6B51EBB13FE5572911696F364F9E23A5D1118DCDDA4BBACBF2A7FC356F73FC0750B68152B5DBC29D6B66208BFBEFC6F8A681C5A1703C1EAD695314A0DFFF8C30F6ADC6CB1ACB1AC7C48385A9DA6C9B65889E32457D7C0C2308F37D1CAB19B123FD8C30C2DBEAFBA96E59233B66149F902B2E377C37449A90CF7B1EB8A6404D844FBCB2B0EBF4858DFC1E3DCDAF1ACAB6904320A4294C620DC3ACE9A61C8B589634CC8DAA485C2AA31FFE0A8182D18E5D122EF476507A752C588CA86DA49B1AA2D992039820AD576684C046AE5BF1FD0E31C6E77F2989A32BD86C45486E0C8D5936162D291A8E64C98146903E9498A5046002A456898EEF057CE66A12FB36220D7C25280539A6A3DABE66CAB38AB4FA0CD19E8507DAFC656A4FA6FB21FDAB418F582155BDD5C9D6EEDD0F4DA14531982A8508F6876A2DAB4A954FCB470C22A452E23009622324C77047E1322B73BE8A3E3D65615466D578B8C596B7B9323162B9151F08A15170EAD1DB7E997FFBADF94855FAE615CF25B14392CF64A3B132FF3BAFE8CB9C0EB64BF1F4BBB7618CD83CAF5BC407893CCF51D0089F52C591A362154200DED5FC209684AE4C29223E1B8CF1831CD729F165FEF591E91618D87E9C185BD22A5AEE91AFE7650865AC2CD031E63DD368B04B558572C2603D94594ECEE0B236397B14C1E8D1669A64A10DC547ACA826D6C6D9A351BD3A511E087F90EFBB17203236980424045F37F6300B0F93F007D04D5EA0B7A6267A6C19D28FB3D507C553487E26D35F8D1617AD0759ED3CF1675EC11AEF2503E72F380C7708B9B6582F2846B33B44F8431BBABC6542920DA66E398C18C7F1AF039396366854034EA4221CD8CAE711035218AF61739D53FD1F0A9A9D1182200A8E16C4211D844182889E31C1F4FA234F60E54368F064C1E48354DEECE308F767C70393833A645D79BDD32CEEB9B1A773520B4FE56991274E9F644A4F305853780283C5EDDFCB4BA3E8CE191D5C976FE002A2FCC11B684303904A5FA261E493B69984FB71F348F760460992582E9C0A9EE16D4F8E0C2A22A0C9C2631C335031B1D397B0499555CBE2754DF55BEABBEBAF6FB2AA42070782ACAA206B0071084C6A51B7EB47D1803425AF9CE1E45E51DE34F1B148A5452F0F490A7A2A008603F3A8AF47D1801457AF9CE1E45F2E5F13B612CA6F5CC5211D453CA4D75DA62676B123AB7A181DA4D83214531863E438A08854B89D79401927D6E9FE84E4191562F99EB694223B92A242568690CD2882ED3C1352A12D3B9315425EE9BEC153E9BABC688080B8512C260F732001E7C2ADF690228B4FD1801595AD962DA9E3E54A2ED7EFB84E9F25FBB6D5EA69AD22FC1FA2A045899165C430300BE14AA40CBAD7DD823A24D2F1A5427FA540B5382AECC66D08EC486369ED600B392CC418309DCF5180BAEC1A07E8C872A48C67B0AA7F2EF366B466CB9F58FAB4E049D654F816C530F45953AACDA434A671AB01A4487E9902987CC8C017D3210D1275340FA647F307D323B509F38A15A496934174C0F50D0C3943311C678AD3C157A67A58D0768E2669BCE321EB173302BF8BBAB18E4F05730C358AB7C0B90039278D976301281FE8C0C3F40E648CC4D7EBF957F109A1AE18BA80B4190AF465CE9312D4E1CF64B10CA082825080CD31BE1F9F07941D6168367AC350A4C270FCC43896032503A84E9CD0F8EDCAA8084225F23240C8576CC3BA131E0070D7B6CE84122D94FD871960412767C8D90B013DAA1598B2160070D7B6CD84122C1F4A1CEEC2B806FAA2DB5763C7ACF90BECA58F0031D4013E26F4C2F8F5D28984E54B1AA71C2B2D969C0DFB274B7B9E37FC1E941A09E0D8E55950198849A0480699A0101906910C4C8F034C887BA3857AC66E1C5E1B390627C2C7C26CD305E1CBE05E862873E6D6A182F0ED09F91BD3880CC313D98439252E9709EE8DEA6BBB607C454CCD3A53D1B77B60757F6E5F9C54CCEC5DBC17C4A36D1E2EBFB251A895D0502067187846A0B7A00B634E3404F19F3F8A05384B2E770B39E53AB35C203CE740E3D11E226386FD68B057972229ED64D16A91D3D973170F53B6AD68832901A8CCAE6094941D9600B53C495197B3202CE8CB246B53F0F682182AC154A03A41CC0347990B5B61FE3C1685F83ACDBEE0BB3C1907A132427C0C9967A13E60FF9E5681A7010AEE0D18E082E581CA885B0AE399923B869BF762BD67F5BD125101BB0E5E0FB055BD0A32B2CAEC0718E872A5008044C4D7AC0D0AED7E2ED29FBFE51A2F76FCDCB0DC0470AA42B75834C79CD804734E43512D90713ABB30A8D4976042ACFF7D7A64AAC03B63F2268F62CA14ED57A7DFE7A5AFCF59066CF68D584A8ABCD04265673C90B6669782AF54510CA08A824480A8D5591DD4C926698151D401B306DC654AACFD08B318C33BD8CE7AF06C5E431E8C419A65AE4EC3EC67C19C696A64A968119FE08C8430907D30F81D14CB068566C006DC0AC52532936432F4687D7BE29B63283519F5EE3FCC9E8C700A9B1F9EDACC9ED14C6C6442A154DC84477BA718E01299338509A6ADA4453BBA4EC7F9CC7C6AC521C159C42AA2420278EE2B9227393794B0E058C680CB4406346A1A4AA38354E2C2B184784478971CDE2394EB05801CD8F07923D5B9ECE8AFE5CDEDFDF7D4817D5CFE6E4872035041A9990021FB891D1B3201ABB31029E8CB29EFD12A5F4DEAC8460F2E0C89A4A45993B3205B8F6536D91B41656690D40D63C54D6C41A6B3F1556DBEBEA6C531A89F9D287B52684359E80B28FB33706A00EA00B64A1A36531021CD1A2C2F465D2187C796291E089A9ECA00D4D20453539294E29429940730E45AB547F36C0451981681BD0C7423DB10538B501B8AFF65F655A608FC921629F6F86CCE5E4DB34CEB17CAA03CEB63F17DCD26C5AC72A3004FD4D0F903A20B074B7CCA741D698373B8CD2C0B45F9E49EE36D363CBE28A8D8D8E5894631DE436811356697C2C98ECD94A5677177D4008937B7C46743E6784E6A18E0027B340E60FAD6BF650B961CA8389CC745228134270AA6988AB98C21804535918EAC85037B411E0A31B3CA6E9BAEEB4078759A34B2D27CC1D99F6F0901C5E2AF1C5AE5E3E4F9995518DB17C81C346794EEB9AD3EDF3D3C5AEBCD8F121FE62DBE3CB94E0FEBE2722EDED15DE13ECEB757D18634FAF93EDFC97AA6A69AD1CB516FB472684E0D3D110D58EC21B804F48834737B611A0A31B3AA6E9AEEE8486CE629715BC9A0C3B160881D4B0C9C313924D1FA8959111651CEA2826904108383B88673021C0D671D144562710ABBF98FEC32BB430B83832CA2A07F1C7C2CADF3A67E8C528B8D2CA18ADB1E601A6FEF2BF3D1983ADA2156684E7AB09AD4E93B901DBADB1D168F82E98AE4C9FD741331A515F11C122CED731612AB60C40D5A88783C315ECDE749005BF136E9DE62A4E7723916571BAEC4661481721118277112B1A6A960899B10171A1524468C63602AA74A3C7345DD79D5CEB759D7F93E7D1E2D19C75C454C9A4E54050C9A3BE4CCED88AE5ECC59BCAF9FCEBCBD368BB88964C41C9ABA2434E3D3460B3270BB633B18B6E443D68920F45FF4D8EDFD28DB34368400DBD09B5152979170C3733BE5AB40C7844AC69648175D2EDA6BB182B9A07789049F4E4ED7078FD28F7703A7C6A6435FAA6DA099FF330031B7F80F11C82A3D1BA9029DB0F9EDFF8A70E40EB63398DF7EDA4A1EEEE4594ECEEA345BECB587695961FC470CCA9AF828EBBC0E4D3815B00C0A4D0853A3CB70F7C0494D965B32FA0BB5A45C9BFB3C8B0604A741EC37A7A9E500C6153181446CAA846C38E32F67D014CF55FA47A12683D0247E4AB5BDA465043E00047C3102886FDC211E621370DBD773C599E6E93894600D634AFB75964B25700BBD9B0457C1F2FCC2FFBEA2AF88698C85C87319E2A3CC8C0018F8B32502CFB05B3DD669366395B9EB1F271241CD2A43ADEC126F3D7E24D241C01729A918F8C3A8D7CF60A7842DA4DA44126D7F10D3C85BF0E78E674AFFE71A71BF8B8B8D38967FEB8AB0D6AE40E0022068F34412BDD789E09719E6413601AE308A0320902D37C5D7F3238494FA1A31E8D34D481C0457E681ED7D2540F4822FA3402EC10DF00A5CB66F09024B7BFB99386D51419149CBD2EA8EFF43BC870474A98BE9A42E604D250CA142FCF31742B5E622855DBB39B0CEC1751BC7A1B2DBEDEB57FD8362BBA0A10AC5B12CA42AEE50F9E2FF0248120681BF008B8B3C904D385B6EA2CD42A166C863A44356AB41D0DAD4C853AC4C847567803B0370745D7DE39EB1577C1318F16FAFB11DA1AA69B802ED700D5064CCB6EDBEDE03703B5DD1A017856D963FA20D79D0C7BEFD2ACD8DF27B7D19715BB6B8776CD56158D5EFB99AB4128E46B5034A0A52528039A4414480DE2443002207112C27484E734D9DB72DDFCC2A3515F85A20F8DD9F8F42D4C8740FBB0C754878390D7569E0FEA8C6E1D90DAFBDA2B3037C02CAC13C7D89D29004675DCF0F526B7F1EA44AADC3FF4A9D1F4554C48A3EE300CAD0C869CAB7C6EE29CBD5F470FF609289253E5829985520B902BB5EB6DF039087766C44908CB1BE53E6D2B4DA6E2FB6EE3C2784072F0F09104006B0B7082C2D0613CC6D18E8030B330F60361BC4F021F2F66AC15C89734A70832D4F847F6280D8A269B834F49190C3AE4C75A3324246716048496C514F01C12103407880A3EB02C5AE4D755CC09E5545D57D378B6CE57723A61D7B63AF939BBAD6723C014FD6D504BFA0CCEDCF9AE77836B7E34588FA65AA011A9078AD18634B66372D63744A1EC49CCF8C7302B31F2D9432856B7435B571B4A69622A5B8029D4A3A84E54DB53694F4AE7C645ACF53BED2B70EF74834429540B0B0C88A93B757CE36614136790173CDB253501AAED1224627BDAEBF2CA59B0FC83D5604073B0DAB6C009BC09DBF8861147FF618D0AB28CC6B67731E2C3F46936E101FAD15D44DB9C65CDCF2EB016198C876AA95D13A805D2D1310D0B685248C3B2C374A9AEB977B8D69FCEA1394C856CF0FC6E96D01EF31524B2F450EABAC1D7F4B0DE2DE3BC0EBCBFABFEB6A4F1D6D08390ED494968D5B40061B32B0E8547F368C7409F591A28ACF52C66A43D315833540AAF23A7861E62F093683F4710CE651D2F87A558DC86647A2039984B2F55C3546D791A61E60E01D3BED2E119473B02DCCC12C174A0E430BD9A13C761D77022BD51B94138431D42496D4C9340DED29B31351A2C7392329B09D02A48E0BE7FF59F70F0AAFE03A5F1C42A470FA0E2FB303A9E78F9EE8FBA120FBB10169954C17C331D7B16836887A2B47C1B629A318F09318D38483A6B1E111CD548CAD161C156D186C559DD04527B05419730C8D181258C7F8F3095B1E8EE8A65EB78BB351E08CA84209A0A1A2A8864B650386E571C0A479AB18D0122CDF851082AEA4EF97049D367B30525D1699E2D417F5F3DE3D10D274D0746008D46A6B337996ECAA7F18AAFF0FB8E6D6BA71CDA9560AF0AC72188B5C87108D646A77337E025320222F192C27446E636BDC545084534D4315A604E0ED759052122463EA659362C0071466E0A6E20E6C7CDB53582024FFBE2B9483222E8C67E05DD2A957D03DCE92A2E58BE592E33B6DDA20027D5080038B9050070024970BC69863C2ADE3442D937BC61B340696B04C0DB5CF23F59C73C2AE006E51DEBDF439901E6AED9E279B162597337ABCEBB8B811E5C310002350D810F7D0194C1F16816C4A8B0348B0AB5316E38CC019938CF2EC2ABEBEAD19DD69B3B9D2797EEC53DCD5851EFEDF3F4ABE959D1B7CBFBFB5A31DF35FFFA902E2A1ADBC28AA90CA6FBE0EB91F2A0601A8432A24094A1123E118432024A2922C374A7AE32125ECF8B3AF973E50C8A1396B526C1FBDB9BAAA44C15FF62B1DBE6E93A4A9234AFEAFFFDD3969DAEB22A21C0AF2FF36CA77A994BA6372C6F55E86291B1655CD7AEB340BCA86978E52813012895D8B6735BA80AF20629F10D1879E2D974DE5403B7CE394964DAF9BFECBC3B6F93A509EE5A11C456B89548E96D7B1DDADAD7FE7ABA857D315717EC5312E7BABE0A0408765CBE12881997E5013B70293DB961E452D66E5A0308CEF6E1B78F4400436FDE65B07000DFC454B941AF4FDA30996B80D3F8B2B0D2D2F2919D63368665E4E8352B335080DCFA622BABD346DD2A4C8A02BB6064D710C046F4C2D840102FBE7EDA98F80914F6FEA5AB15ABE2B52F58FE982EC11E4A3498895AA8BC6D9C3C9C4579A499AB1C8595E1FBE429D568D1A608CBE2CDF25FC5B259BDFEAE67D61361D9BECFD9DAC0B02CA6B02A76576961E5AC62EDD2A925C636836E82C8FEF75DD43182B8F2E52466D5155A1BC7E6FA344E049F924D95D7DD30F896C40EF9E8B9C4CB192B0CB61588789E00CBCEC008CB42BB5AF6C538EDAE3C8EA551F6F23B546805D4DCD030E99FE6128495E52ED1E9ECB2C8DEA966B770D76E17004E2A098E674B0FF54E22A1B2D47D6A80CCCADAC693C4AC92BA06337599FD935CB3078DD4EA12BBB09A24911FE22FA090FA622B2B2D5271F8ACA87482ED0A111259ECB26277A9DF0A88140886EBB8D8C5665A4D2B10D0D819F76C3025BA01134BBB9A64590C9B3D7509BA176FF26213F7A8D3DA2A159A71A95577269E15817DA0DDDBE4C050BBE7C1312B8161EB20BC1287E265DED3A9491E314CC5776A355CC58761516CE524853ACE72F63FEBB64C3CCE837664E2C91992A189957DC86DC252709C7D5259A4DAD52E4E4206642433132334933EAB8F9E599F0CC7C2947F9C0062283E3081DE0E23BC470E6E2329A991D18724E504223682618E178794D5C32014298100C65BA05DD4F9EBB71846F5F5672DA7F696BA4DFBA73090EA1325ACC4745CA42B58686796CE492CDE8BB02FB95DFC3CB8EC76E1CDB67E650CF42ED481F556A88A51933054E58058BB4D069D9283A61974046D35A4936DA197326E45E49D75B07D6DAC62D7966F2BD4824AB22EC26E53F4CB9B742C46DBF558D9CA875F34F6EFB2746D345E4DF47867CC7FEEE22C4E1EDEC5DF4D5E2E9ECECAFB23FBB67DC71868DBB665682F4231294D5E84A2186743FDB68B976C1527ECAE53CEB0110510D2F89FB16859FE71567D075B232235ADA50F71F2D5DE424D45E2ACB5303B0A2BC3CF77A72CCB4B3BB754F68DC75B610A52593937A0BC7C62D9E7987D03F92A3416AE4F6D8DAD887585B58ED0DAEBDEF56C10874A64E5DBEEC6CED795830F602A512038568E57D60CB4751D829C414A440BE5E0EADF73ED4195860EC9BD73D06BF9625DF8127D61525F9E5FD8F9B67448EEADBFDB2C6F90526A813BDBB79D8EDF01C7EC5C75E3793950570E500062962C2CBAE8954E4C86837D2520C2A1BDF610946F0F884F9044FC4A943145FE77DAE11804AFAD849080AEAE51D448E96A794362C57DC621B255D2971985AA522346AC54328AB1DF3362A4A9F23689D187E4C044E04A08892A43543DFD8831D521B98A21280691A21A30095791815F90764F00E0910A5441424AAD69C52C179383052ED04A68F40A613C77BA01A87245D5D30F1B531D92B01C9664902CAA091B7EB51FD149D4FD3B2B14415B6B996460AB0C0B9977EC1B456C653FA680A5A82CA4625088119355AE635406B7B0D58DE41C5A01685B96C3EB082294AA3A0C5BE4104EBC523B2661CBF218AE0FAAB336B92FB012D0901AA6265C039CEEDD99A069AA6BF8D925E643546A58A379721BE9F583345583240707651AA4686C20F44C071A6F8F7B51126C8849A36BFE2F90EC9AFF0304A7E3E862DFE78245D66F4700935E436A30B2E11AA0E19E5B8D491D3784951E485246D3C6484F1AA5D19C7113DC34268CD4118CD808A2F2241EB3480288A1EDA2551650D608D348A49C1183A4226587804403331D2E1F83E5A0A1448FCA6037D04534AED1C0DD14B86BAE1E0036AC4264B022655AD02E15AE2F986C52851B20166F88296F3CE016310DA57E2070054836ED7D0C835434CC465AC1C4D611E2C1CBC58F40C2AB5EE14ECB5D7D510690824A651882420CCA42BE6E63128ACA11900B2C5D979D1A7F2F472B1380CAB09F5288C1AD997C65C8B44353398694897CB3E84EE92B801A5B1DC317B7540511A55E903261CAD602B4CD307F1F37272177BBEA4EBDBF057A088D558CFE3B534D8D6F50BC1F66760F1AD943F0B47D3207913667C9668F814AA41F97420B09AABFF7669090CA29B47A6F5B84EED4E9A50250DB07A5561A2C2780252030686CBE0457DEDABBEBA21EB41213C8ACE3E2A90D326A6E2ADAE524F0D30B289058C05B902849413549830518F89627D4845EC4A02C4612FA89BBD44F068BFD6404B99FCC41F06E481F84F2C008C7A33BAC8085BB4366A9F2A4B8B1F24797BEE4C7F3848C45C3B1AA83A8F8BBD304A72BA69A7EB888DA9038A59BE50679621A18D3210BF7C7E0703357A00EDCE07E1B2AD3719D71421FC4D96591A1408C1C245FC7A3EC04B6E6B5C7BBCC44ED619199408C1C1C5FC7A3CC04B6236B459E37680D19A81D8607DA3C1EC406DA3901E5565DEDBB933A6E161E54053752A0A64D8CEDF544A42CA11600811A3FD440534648EC65366578529CD9C1076AFA3265789ED0B184218874B853026F523B9BD374539AE4A0988109DD7685CB946315634F8B1E6957C597E87A867AA171630A262ED3EE1920A68FCFB443769398693FEC5364423E2493330C2634388C217AD00B2DA56C3239A1419E619D624D9B668FB34A641D84D9E3DC279EB28B63CC08F3A645F99BE9252251DA072356182C1B891D647C19F1E72EA4DAF4EBFAAA159148671D91406E100F22E21B62A817904F05AD9C3CE9D5B34C6A57A5528DC1AA59E6071BF0A693B40192D206BB8804F65168835C48B2081FDC0224AFC3C00553CD12DB64AEAD8D9A5213F2D982A82C0D8D822F31C2408B32880C1BB6A0459C7B24C408B15542340A260EC258011BF6828980708EA99924F841EC8216601019765C5A80B98B6AA4E03D35DBA626384D21B40497C9F44302D5145EC6188F76201E8291CADCA0776D2FA1C8239EC01404C4D1C131456D8252632811CF256C1456D5927EAAF0E5D60EEB270761D4E1A7839CBF551B910713EA4700D243F2B0659045700D890AA5452D3E349484C16831334C46A3A1080B222286B010E2720ADBC5331E82F81CC1F2032CDA03017B25FD08AD7521194A3D3208D0CE1E90263C6EEFC0C38A17550F8F21AC9007017566A2B6E941AA1A446B4137E98D645922B6CA209DC516446C86B166E524DB5DA865F0540026A40D093C0D70148FEEBC38847CF496678CB13B63A3D5891CFE480E26D40E4C4369F1EEA0F660E8FB76136DC2EA64FE77DC8B01AA78141AFD5864524824DDCB020699287C4069747DF6B325CB1A54EA37A33D856547D5116A3766560FA2C427A855DDBFC560586D1522C39A28D382ABACF04084698555B8851446F720C49D5E612834FACECBA49024F8072A0C7250580162F0A818F8B72C0CB280094D531BA087F584F4DE86515F404CC3CA877B43E3AE49570E494725330D43A18625233E1C62148CCA7134B100E912AD3282EA20870754B54A4F7F6A4E6B27F481BCA603D20B3168D98AF5C8E316511556C6625B809CCDB3C1E5C8AC7A88E68E7BEE06382F93690C275B12297852D6BE8A633A2193F91864E1137277E08B3B7AA881F4F6CF0E5533410B072690AB4170C218BD89B07A31C80C281DA97D88520D93CCDAA78D108293B98E04B67A96A324259362174194A4C84BEB68926AAC029D99CE175B4C49BD690E9B035A0EE177F170E231DD4E1EA4B66DC0A14A8376F42043405498A46ACE82E35E21D349AB27B18DA8A31C24979E0BE4F1EAFBEB4902D2136A3A298864B63108D483A42172D2CD24EFB8A8B95AAE27E9487163B25C4D224AC8722309188F4F49492FDC194525D2A2462754192E2C919D4E5AD298BC8A4B79B9CF2C31991C374AA99607B9C91CB5A253C6E7537A6AA0A1517A0A396AAC72ADE1D25338EAA4670BA4744FCB6BD5F5209D61CB05900FCDC63BBAC297F3285BEEB599C8F58333D4F29E087ADC7B6E60BE747B9E6D4C350356ECB5FD27819F32AF76FB4CEA9DF238AB2A5A2DAD7EBCBA2A7092E3EE35578304B51C410B5F1A93575022246622C78104213727F04D22BDF64049C959693AF75289ED27564A1DD33918EE104C6539529A4DFE45DD3BE04D5F5572961AFAB19A2B4232949E0336C8D1C21B0A7352C7EA0F8128411AA8118041099008C2F9084E67C6C08484A1E94C174749E9AC15E9BD6E6F02AA8326B97F805150066AFBD8D44A2679D9D70503DFB105C7BDB76E115A4F891C5857C1A3B07A9E903DCC3D2C3F7C9BDAB564751EC19486AD245801DC976A478461183E2F8B90A70CE3683357C0595228A79B938D3699034EE904C6AF64AF441835C6C7E42ED329FD4DF01B7658DF80B6126217AFAB1BF471BE717D06E0338CE477255D1F940CF79224EA09498F6F47729DA84E595B4BD28653543DD4D8B5D52DC215EAA125AD6F6D54CC36613DFAD1D8256EA88D97849E0946FA34A91BDA32CBDEFCA5BDBEEA8B78A0165F19A9414D3C423E013CA3476B857E5D44DB9C65A6B758D0755DC422B208F901A4964CF29765329AF8C13D2DBEF270B1803BDE305F00DC0407FC04E54B476FAACB2677D5DFFA08701DA961CC700DEDD34C35A9458E1A9E90D4F8F10400AB455C267A0A4E2C821B88C2314558F601F7D09586D210A507560043FF529BEF58C36B24077CC753EC8611621229E2FB8B358CC0B20B4CC73574B4BCD86EF91FBB942A2AEC50AAFFF8944DF51F287214E233280840D8D09915944C8B39A217AB98CFFEAD9B502DDBD1005435DC7C049BA46A32EC682A6AAFF2A939860451C6A2BB2B96ADE3ED56B71154680CBD9748416114341619C85C20FF2EDF671FF7431A6E5AD52293189C8222A5E67288AEF7465601917053DEB72B98FEBE63DBDAE2C22CDE885AA60DBBAD32EC14106BD99C02D636C65EEC71FE431339427DE07C87C87B5C26AE23F90DA12E686F76EB896963D4DEF71E2037EDDD6F655C1E65A664D533C94C26C68C4FAAE34566324F286180395BE020992182B0F4C498F12102B0C8329B24F8AA6FFE9A2D9E172B9635E7627558B045749A3A98D1C255BD0852C31ABC09068FD9A758ADD62ADE524558A9681985B64E9B444C3552E52456867989AA6708C44054372499D24C2C7A13F6045DF439FCCBAB9A5B65D8C409CBBAB25F5EDD2C1ED93A6A7EF8E55541B2609B7C17AD2ED2255B6DDB828B68B38993876D5FB3F9E5C5CD265A14833BFDEF372F5F7C5FAF92EDAF2F1FF37CF3F757AFB615EBED0FEB7891A5DBF43EFF6191AE5F45CBF4D5EB1F7FFCDBAB939357EB9AC7AB85F0657E917ADBB594A759F4C0A4D232C5CA92BD8BB36D5E6657FA126D8B0F74BA5C2B6417EF6F6FAA1F95CB02BF74C26E9B6A26E36291B1659C5722D505719435CBB2B66AF977130C1DE7DB1FCEDE7CF8A126AA05FA83C254E6D9CBF65D31DC3238A81A39E367AD9D47C1E56611ADA252456D58963FB789CA978574D2D56E9DF4FF9631ABAF5DFE57AC5FFF82E770C6B68B2CDE941D171909052ABF5F5E494291BFC42BE55348D343FEC83808B48A4F10B9171C809C5DC080641406115DEB321BA180C04F86B6C21722A0F0CFE32726B3AC7F9B1FEABC026D08B6F647C1341B8313E91377BF9239BD0639BDA670BA7A4C136958CD4F781EEFA2EF2287EA077CFDF375B1071039343FE179FC937D29A331452EDD8F783E1FD2E421CE774B8913F7338157A1080056DDAFA154838E4BB593972741F7239E4F995E5166D3FE86E7F21FF1466451FDE0A0DE41552C17126644F3980DCBD63257A988A03BD26A1B23288F14DCD9E8799CC5196B4FA905EB84FB1DCFEDED6E5BD89EDBED3F8A7D83C4502A9ADFD263F0E53AAF409A2B57948548CB22CC7A54ED2BD44589FB99A2A92056FDAF784E17F172B962EF93622F13AD2460C965C79573E8CAF969CB32F5BBF5BF8EBB065F45DBEDB734539466FBEB71BDF3B2DE79DACEC847518AA08072426F93ED3796491D6D7EA358630F71729B3D57A96A658B4C28A2E9BABA720C293CAE08CFF336CE5712AFE6A7716D84D29B277FC7F6B7D9AEE38673C4A1CB79C3DAC3AAAEE514C877D13B4CAFB2382DE7A13497200227FEE7DF17ABC2E253D42D4841512F5D00BBAA5984220715D87C14AD26E4CA0FCCFF22DCC9183E610CD7551093C4587BAE5E987D75F38A17D43CEBCA96F1504DA9E713060EBE4C3CFFC6957FEDC7DDEE541C1362119EE7EFBBA8038ECC542EA3F4B47A5B3CD6F4552A24983775384A561EA7495A402AA2F13C4F9630C7AE80C64F6544FA26F9F39BE5BF76DBFC8A650B26DBBD6A29E1BB34B92FA50FD2FD3A1BA52760C487B213183A28394BFD30CAADBE7C2A6C2AAA5F0E76ADB32425A07FF48E9DD32737D43E34E326E4A6B4CB92F1FE4C5A09F90282AB6A9BC7EB42A72EEBD826C9692517528E90CA1AC2AA277518A670E8F9FB9CADB757CD75BA3206443308808EE0B66A2B9916792D11A99DED6ECDB21B56BE1D55A6BD529A50CA67A372FA83225BE64467FB5AE03CC4C0B6300A1C4551B7AE3F68EB8AC7B168E781970040198E907DDB7CD5796B150474BF4EBBED825619FAE21266A3A5D90C396D84C04D90C306C8F7569ABE69D1A2BD5C4AD751524C212970559A05063ACA69D8423E1EAC7E998D2A33A4A7265BDAD55C7531B235150FCDBE9EE81323DF29A17E6E85ADCBA7C730096CD2F872F6CD6E1DDC8FA30CDD1D38F22106F41611E6F802AE3757DDF3E6CB978C3DC5C082259650766ED50BB0EA9EB0FB793660B1DF9C7436BB5DE16363F0C7525E7BA172AAA432D7AC4C75E905423D3B17FC986ACF7BC7F62E5A1458CB9F55A5269610E0530A43DD7F703F1FE031C72970E3CB098865349A0302E16A735DFFBC85F395CB1BBCE8CD6BCDB3DF7C27E384E7E8021873FDFD58F14ED3F5264A00DD251490438AC188E2F1C34C7DCD92A1E1AA94F0E8A91C20F1E2EBA78DCFF9257074718798EB87995FC2A456002817EEFF3C3BACB0F0395C43F07179A0A8BCAAEF315DB0FC315DAAEBB35A3E1B4D2277CECB5A2DF17459AEAD2CC26894A635F1F24FF3DB6CBE59D1E745A1398A1FCAE40C9EE24D388E6E2127460661BE96D0AAAC4AA43292F66F1E319254BFE66923C3E892A7345E281A9BFB99A0FB1FE34DD9818FA9A4FEB9DFF1DC3EB3649966DA750E28A6F206D73CA988DC5F60DD124BA81CC115552AA2F254D645EE672A2F608D140A0873A532D0B45F1C28A6F1DE6DC02F2E15517982DF472AA2F28450249650392A5F9CFB99CA0BB28AF8022A3FC542E27EA6F202AC25A1806AB943063B2962992D62F6C4966FA52B46FCEF44EB1AB0ADC7B7058D3D149E885680AC21998DEDD2AC7D3E8C96869583B5A2AD19C64C699A536D14A16092B892BA03572C8BD3E5BB2C5D83FDE38B1D79DFA606CEB724D3A515DACE20D01D59A685CA60F1931A02CAFF3E72F0F163FA0DE0C3FDBCFF476FD7ECA15085AA35C2FF4E8A928A6279A636BF1164B5D96469B1784882EA7EA59870597C1FCB9CFA5FE7A694EBDB39F52EC79B7AEE99BA2B6A138FA02ADBCBCECDF7CD86376B35A740FB1BC527D9CA1438BB94CAE606D4EA72813F8896ECDCC109D79E3F2C5BC781A2C6B9DFA78D5F6E2A0251C27CC13841FB3A9E61A2A3DB9A303FD20A3BFC5267D3AAE966274C42B82F1865F27DC1EA174ACC72B1B1CA3FEED65FE42B4962C9746AFBFDB6D8F265B21DDCFE88E77399C50F7112AD608CA8A57354DE85559D268BCAF2F494F955CB7C9862B7F10AA3E6DB5659B9E7E27A23DFEA33D139B4769B62DA52A842AAB569A11A0CA6BE203A0D3C2FCF2F34868858423EC861190F2D75890669265955DBC900245F108B42ACD4134D0ADE9CF13117787E0E53C05C3D0CF2554F05D5475107BC4341F0AE5667F5C8B2C9F4EC086689A4E63D6ABF70AA980EC49486C7D4C09A7645FC946CA2C5D7F75EC2442496EEABA09EC3DEAE7FB6B58F062EF6CDD680866436F06BF2579FB13C8A575E225E78862E012FE6FA6180D7342A9BE6DCCFD3B8924A173BE495E47F9F1B943C82C81D3EFB1A88AE9ABD645F5C93901EC85523158DED18BF66F72C63C98241CE24A5907626A81E09CE6F6278CB61D5B3739F20FB65DCEFD91D7B3556C4CF9D2E2006C5F58EB38D4B1824F83ADE1E8EA8EBD2B1A3780562D0613C7904F9F993A775556239287E5CC361DEC0F17641A37CC64FB9A451FF38FEF515E0C516ED4B2D3A1E70DE1B972C37B35F8FAB7B9D9E6ED95657449DE6115C71AE2BB0FF4C13D4293D11589A744F77DD4C180E1A1B4B047CEC2C660BA4E3BB3DC12ECCFD937DD9422FDE6D8F2FDE1DBA25717CF94EF91DCF6DC8CB775A7CEC4B4A09299FA10FAB484E91E8BCBE1D97B7E3F2262C6FC7075D2D5C66B7BC294BDB71F91963F9B9FC96B0F69EB174942A94CC7519F2E52C07D80E5F8EC6749E4B4DCBCC8062427430602CCE2EBDBA6F440C84C21418F8C09ED84AB54DB89F0FF628A54E3CEAD159E77464A2AD395B33D4BBBBAEBE39288FABFF753688A9BBE4032F352707B8E82A1E1E5AA63216D2C5AE6CED43FCC58B91D0B373310E4CB5C37CF25B0F8F0BDF460F92B159FF82E7F069B34A23E08508FE770208F33C5A3C9682BC8AF247098652D974978DBC2576DF9784B4DECE9E5D4F9C473D67F631B17C43AE9C486F8B1545E6D6FF4CE3759EC87B88F6479AB3224D800BCEDCEF04E7D0531B9626F54C2820DD22D965553419C4542D254CFF02A92BD673F81CAD76F21D5A98C4A5F7EF939C654FD1EA5D9A9D456A2A1E0DD5C0962ED24456BF26BA21ADFDBE2B8092ADAC23EBE986B4562A567B5B2D15E5FEE92D5B6F566ACE14EE7782066AEAA8569D58727007EDDD5CF7B6D838EEF00D75032D3ACFD8475D27DB52F11AD3CFD68AE7E8B4C53233F8C37EA9759C2C59E6EDF297C0D0E93B19EB87F94CD50C066D09A21D917E538C87FAA7797EF0EEA0DFFB97EF380F85808151182C08ADABA6A05418FEE6C7C438F1898C0158D8F7AFEF639F085D83A1DF7EB9AA32FD29A7A3DDAF343B71A764FBEF7F25707A4CBF5D26A7D18A254BD9C5219711B832E839D2FE57623082E4FC6A7F23F5276F2104F64B2A251CDE2E4AE3264AA0144D72D96C344C0D392F179C2A4E2E779B3415C3E896BAB51368E2CDE8B3B418EC5DA83E97809EEB80C5C0C424ECB2A05B1188A64010CF755F537505C865B3435BBD6A78045AC57000C634F5C3C0AB193DB08ECEE853B5670D5E34767B6EE1A0B3B555C37C1BF5D0859ADB798E2FACEF898FAFEA91C757A77B7ECE1761C77D67FAF82EF400DCF4A944FD61A7E7E90C20138B99A36840DED6299170B3618BF83E5E5CFB7C2D9C67EA8A05338F7983E153F625928E989B9F0876F52E8BA428FFE6273C0F20992B3995EBC7EA1910994DFFEBCCE0BCDB942F3EB3E5197BF2F48407C4D719D43636F3C6F51E2AB98B285EBD8D165FFDE141E4E880041B833018685B95B9F0BF53B825BBFB6891EF3275E72F978D832F6D4FCBAF2675B0FE8912AAF25F4A64CA7F39252817B9B8A475FDC8D8F26DFA5DB977D3FF4CB82D937E5787D6FD48E2030F5028989D4EF0A90D06E881B13500982A45299CCDD72AAF24BE5F470F7E16F39699CB0AAEAF1BE683A9CE4A9D8B52AB501527AACE753A71F4B9B73B6A1CBF01F1E7A3EE405B58C939068502C2F5E278C5CEBFE72CD92A51B3521121D695938B8A4AB59472B73FC95B814BD7FBB9024A04611D32AFBE9226965038D6935E62D6FC48F04F6E17D186BD65F705E4CEE2ED662587A18204B39BA93E67E980193ADA9D510EDBCA8551A9CC21E2A54479612A6B035FB872CA697AF6C4B272B2AB73552EA35E5181B9CA65535D7C39632B960327FDFCEFB39B51D76CE52F5D88C473C00CD3B3083BD3E46590FF9DB00A16838A1F129919F73399D76DB97268187665B34117DF311FC8E2F939A0CA5C3D0CA2F836559DA596CEE6DB75EABF7944CEC7F793793A7C433B8B30DFD1FBCBB001DE01DC8FB3BF7E7455A0A6CF9547C7DB65A38D6615684726B56F008943BCEB117D1E1FD491797A40DBFE7877F62CFF48A7AD2FA26D5EE64DAAB6365ED73681F39015CEC2289483B66CD4B637D45311F4C5769B2E4CFB4FA09C82CBFB68B7CA3F6DE5907CA1603EB8DC2DE3DCDBDDFC9E9B0BFE0C9503D956658BF555730504621125124F3D096A7FA37251AE9E75BFCEC5DB721BAFD94D1EADA5F47CDCCF045FE3063C9CE17EA6046ECC3C773C872F6F13AF66E73AF374B58F06809F88F6D48F2BA2E4E312BF0E560BBC6585CCFC0149A4CB31A8B8E97F251CBC64ACCC7AF2ACDD5C8304B3C152D72B5FA012180E311D27819927CF48D977F59AF66AA69FDED7A1B1C070C8A79F67FE8403593DAE58B68EB75B4FD642CFCDED72A4B67298AF4D9B985A7551A874455334BF516EFC45CA5DBF88D68FA5DC89252DBE7AB3040E0C974E8785D041E18C505F7E1E2F3AAEE0E3A2DAC06A61307E15654CCD35D2FF3AA6657D1A017AB1FB9180D4EB0F124CCB1F28C127172CD9C9B127F56F142ED57794B8D4BF11AED2674BD9A1D3FC349BD97253A6322A0A7EDFB1ADBF830D91A7938BD9C622CC8CEADA93A28EDB5F67F3DDAED9E279B1625913E55DBFA8E9293996CAD82D4716864FA0C5BF695CBD62DFFF3EE6ADDD0B35ADE1053583E16D9A47AB5A8CC53058FCC4A4C18104046595FC23929373B6BF119681EBDBDF5691EC8EEB7F25708AB3C52ECEDFA651B694D9894584B0FB28CF5995D05D0CBAEF7F26C82B7F64D945991B328EE47C2E72D96CD4C669A1E177EB6E56D6DCFBCB277E12DD1B9B704A804FE4A83F2EE2EED948614972998B4B41622914B8DD1452ED32B514CFF97381C7FB58D61BFDAFB381E9D9DBCABBEC25BEAF66E512D6A7AB693C1902CF84A8ABD7535C6E95813312A988B07AC0215DC6582EAD89BE8580D9FF4A8BC250DE317A437FC4E8725506BE4AF67AF723E5AAD837954FF72361ED7A8C9207B6ACB9C88B9754369F499755AFF9F8BBF72930747FF868DC5B9F756B2ABCF9DFC37B0CF7F688527A12CA3B9844C6C35FD31A195C59B8F7B4EA91282CBB5FE70A917759BAF69A3BD6C47F38602CECC2E066B87BEEF834A82666E6F834A8ED7B1F9F061D2D58E1F8E4A8EDC9D189D6ACF7C9535AACA3D7EC3F777156B61E7FF7B256017C1D96281417EDCA54575616A8FE67D23E0C4E6B211490F8555E4D8559F32B696707F74C2820F1037AD6FF4A99F3F1F2CDBA7CE3519EF0FDEFB39907C500B7EF18F39269B9E5E500787DD540F657D19EF2A5493AAFECED8738F92A192BDDAF045DACDED59DD72D5D6EA5F21277D2B373093C31D59EABB17E163DCB0B6DF5CBC1462A5586E06FBBB8F8E0C5F27FA7B945E29A8ACECA1873208D64140652BEACE7E1D0F4B1673BBF50F75C179A3DD73C0079C6A265F9C719B0E71F8E4A13773A34CDDC0E1D9FB35F1DA5AF55ADFEDE110571A52309E672E80852AD349D85360B04794BD8DA711C0C1C3C667C7DF5DFD2279625450FDFA64B697F271591BC7A5BC8ABB7257AF54ECB0EF4E97545CF955446D8199F5F5C7E59C50FC0B3E95211354858EA62F723C5F7F8ADFA86B2EFB1FD95DA2328D751FF3BE55CFD6157E74550AF02C8652E5CEB935D1DDFB674367AE4F3DD69318C3263787907879569963DE812045784424171D17D91DB541B92231591BE327060D7FF4AB0A0EB2826C9866E7F24C538C21E2DA18010F2DC38FC2489713F5362153B31AB0BB25248F0A208876FA22B452C72F0E5AB3D75EE671B670A9FBAA9A5CE9C5F1B39FF214FE38A7F166BE9B6D02EEABA2197B97295DF5D930B0976EF63BCA99ED2929E0BE07FA76A294847CD6ADD6974CA6561FC7C8ED9372FAB8E85276ACDB1F2184377366BCA19B8D210399551E06A5E57FE77F2FAA04E2AA180CCEFAA79B310E078A57DCE506F6B6F73D9C2DE9256D5CF655D6980ED6F9418897FEDB6395B42DCE4322A32D4658AFF7DBAB0E510EB287FE82DF5522AA244C2660F2C874D27B98CCAB540ECA2E8D4E5BD7A650424A05C4C8A3AA5288B422E23AC3E5CFA3489A954E438AF4BABCC30B7EB6247DEB7A981F32D69DDACEBBC2DE691B4780A05547EE7F2751AEEE7D9ACC64FEDAAB735850F382CC938C6887519CBE818E0700C7050CBC9C6696FCC7BF489D898A2CC533B13D4EE25529652A594EE35C899B2424B4584B52EDA0138EE7F25ADCACA8597ECE81D387A073C7B077CEF8DBA7789C5FE75BF12CFD554EC723F537657EBFA3D0B6987D5FE3A8DAFD1AF07E5334B9669568C691325CFAADC80622A6F305FA15444EEAFA22DF9DFA9DCDA97659FE204BA09A525A2B6A34461733F537901FA492820CCBD78F175076795948A6818B548554342EDB78A04FE771AB74F1BED3C008AA93D55BE3EF7339517B43AF105547ECA4AC5FD4CE505AC5A420141FFAE62261BD1ED6F241B21664F6CF9563D36EA7E9FCE8BD46A6DC51DC1FD4E3E6B6BED73F0CCAD2F74F29B0033592EA46C95562B967FDCADBFC87983C492F081A0DA957B0BBC6DDCFD48B1258AF15457478AAA8FB203582DA59C3D2ABB56F296F5328B1FE2245A69F6FA4A2925AAA4CCCF725ACC8B873493F8CA6533DA145FB3759C2C5956EB2C1F3B622347D476D8C241AFFFEA6AE043154AA18B26F4935954552C546DE263BF54A55657528AB73F52569D5AB03A8113F3A5971DB88D73F9648BFF9DC84DAB28D552C2370434395D87434764F4B3B1BA8672ADB0FB95A06155C193657E5318E7EA76B9FF9588F22D00739A85CECAF7DB6A2C82FD924A6911C9C0C902F7338D9772AAD0FD48D055D5DAAA8E93FF9D6451EEB28C25F0133D6A29E1AB142BC08AF51C3E47AB9DFC69601297DEBF4FF2C2748B56EFD2E223ABD6B1866A604B176A6A3813DD90D60A7BA5F863651D594F37A4B5D253656FABA5226037CFA3C563E5D9515648B90CCF95A921A18C1A097AB9284717259023502E9B956D97268B78C55A5F25CBFDD97808CE485B0FC549BB6BC9997C0C5DFD3279EC453D1040ADC114B448C946586A9C645740D95581DCB89FE9B10C7014C31CF6F7859EAAF1C6490B3AE232D1CD6A8697C78575715EC5A1F8B83582E28B9CDD083EA39C2578BA8FF23E59C64FF17217ADEAB09F92C54E7EA2484343EC6D9F6D44DB92898ED89AA51157DEFD3BA9E6464C740EAD211A1A242D4042A49D4D352154C5C6FF4E3E73832231A422727C1E1CDD2114913C05515C5ED882B82A8593C7AF02E1E06209996365E9AB5F5D2D75E3ACE7EAC0513DA4160AA638F1E63AF08F6875AFED5D5D48E6FB7BB4CB72D9A72397CD251E616780FB8E1E95504F69A0A37CC161478A7246DEA9569172A564E96A38ABA5B4003C6891E37F27D9C61E6F15D46B998F372BDF6FCBF1C80753F56F04DBE0A1986E922D50FF441D93EA50E07F2723AE794001825B5B44FA866F0AC5F290B025C4172826F1FE9458B8AB0424FE1AAE745E85003B8B422360A59C147D58D88E105FB1848C05783B2114D177E3F3BF59D0D9E219B006298554BEE7D081845444B2FFA15E3AF50FEC99439FEAC1C8BAB6FF958CC36A252A56D19A0588479984D4C69B27DDACE44AA8B35D3FCF5D67F8B57AED5F2820F90DF44A432924F1D5A84DB184B44BEBFAA36CD1B812176F4614B7FB6E9D1F83A7706DA118B6897D55ECCE7B6DE64DB265396787D6E742E6070FDF65D4F0608963EC64A31D2C4CE1DA82327CA0D89DF7DACCDB4D3246A10C90875E14AE52D00B4033F6893DE3ED19863F9FB88623C11BAEE530861FDCDF09CDB562B85C1FF3734C760327E4ED065B7CFBF0FB02216E4EC00890CB4836A6E6CA8D58325D8475588FA36FEFBA5F0F66B3F58402BCB882C3F660FEB123EC7D44871F6F08F5B58EB778C8B77882DD7CA0BCF4310F8BFB5D9A5D9E5FF8B7BB61BE74EB5BC7E768831F6DF0A30DFE47B4C1FDDCED39DAE0471BFC68831F6DF0A30D4EE175B4C1BDDAE05DF0BAD71B1F08CE043BDCCA49F71D4A4D5F076E4B2A5628C07FD7B21A1CAB2496CC2306A71BA4AAC1A4221709C04CDDA292CB9A70ECA75842E3A849F9271551562CF06600F73399972A44A180C20FC625FF3B9D1BD43DB72FDC5D11523BE8F42DC2DCCDB1B6D6F7D6D49642E5845B481F00C50EA3D0449742E5E3AF5785BE4B1771F55886FA6878BAD855F97E0BDE77FC3F8A9DA1F54170435DE5B56FAE185811979260F5ACEF6ED25D069E00C33B0DF95237C7EBF51D34434A6976DD19D4D32676C0B1A7B4BEFDF20AFCCA1C4D8B92EA89BD3861994CD2C1B0F9A5FBF7B6FDA1FCA8D103AB3F7F5FEF66F1C8D651358CED265A5496DE92BD8BB36D1992177D89B6AC2679F9A232EF972C2B2CFEE76D31277E28097EB8F9CF559B6FA925B88892F89E6DF3DBF42B4B7E7DF9FAC793D72F5FBC59C5D1B6DC2295370CBEAF57C9F6EF8BDD364FD75192141B9D72E8BFBE7CCCF3CDDF5FBDDA562D6E7F58C78B2CDDA6F7F90F8B74FD2A5AA6AF0A5E3FBD3A3979C596EB5772F5862D8ACB8F7F6BB96CB74BC184E24CBCD6D1B158646C19E7FD4B38E2D7FB9F4C815AFB99AFD9FD0B1D227E792557FC054055D9DEAF2FE352BCD5B4FC8D155FBF58119757519EB32C29A958E3FDFCB85BADCA58F35F5FDE47ABADB220C9ECEB45AB6EE029CA168FA54D71117DFFC09287FCF1D7977FF913CF32CFD4CBB632472123889EF14F3FFE68E6CC2B4DF3A7695D4DC2373A90EF23F8D1B87664DCFFFD7DB264DF7F7DF9FF54F5FEFEE2FDFF7DC755FDB71797593127FFFEE2C717FF2FF57B2AC877E889CA62588FF2F8A943ED9738F70DA403C18E6D6EBFB64D4140F49DD73904DBD79ED936AE19134F2ACBCA3FA367F82732C3C645E393E53FD9979BEA2D3E9F4C3FA4C9439CEF961DDB255BC4EBD2377995157F6DABA5FB6F05880BD3A828FD33997FA11C02B227EA0C9541B7A1A5A9BEA6DA107577DAEC55690DD7B586B45BB949F5103A39217F83EEEEB8DB2A22561F3234FE8D14B11F343E8D07B3AE9FC01AE6E7BF922DA838ABBDACFE59BFDD6DCBA751B7FF28F66043B9D3D755F071F0BD5D5EABFD916D8D3DA1AF5A1FA2206C2FE2E5B2CC1314E771B4DA1A4DF3A35530B555F069CBB2001808606C5C45DBEDB7345B1A95C9099DEF71C19ED1823DD9E6F3A6CC8B5708E6F71DDBE6CD8BB2C4EFA97018B4F54CB6DFCA3C16A619E4605A3FC4C96DF6DCF8979DAD9172E1A879C5BDE6D81646F4AA7C143AAF7EA4716CB2977AD541189B89CCF43A5D3980B4AE85C783B3C5D3C4841C88E1C339AAAFB238CD2A1ECEA8E5B89D7F5FAC0A037598DAE72ED13B280BAEB20E1734BF5D8380011AB4E330AC43A6F514E080C63A27B303C137D1F042F1447AC50BD6DEDDE2257EB20240D787E3D99CCEB69B8349145AC1A1CCFE3E7A92DE0BA1F2905E086958C8DD106B0F934621D44FC5D6DA551E42F5213DB9AE7340B6B9541A5FAAA3F95732EBD2A90C67D5F25817FBEF67F2A7CE9FEB07A2AFEA97824D7EE29F3B3F31D995D10756210D1AB43E163EF281E8E16A28E8D363D78512DC24F83B3FE6B4D1817C96510EF5C1AFE2B0FB15F6834E5F1A5823E362955D470F552C2B7185ECAB0ED1C3E7DB424B965FB38D49E376E34EFAAFE623E71DA38D0DE2E165906574DCB650CC5D68D280FD60C763D8E2AE6133C8AF9726DBDD9A6537AC8C82AF133A068B41A863CF1ADD74207A491ADC9023C086C1BE99B0AE383810004CB76FBB4A57F1C2C12BDFD6DBFF1D9BB8104166F3C95F3BBB99BCE2CE6527E66DEF03EC7BF6CF5740DECD001AA15CD78BCE9669AC8B92CBFBFB0FE922E20D410727ED55B460017658D55C3D105519E0F4777C1FE44594ECEEA345BECB5876505F67FAA9FDC75C4A4385FED64F901C06366D9AC32116E3CB978C3DC582D6574F6DE9DBA95D92672E91126DC59076F7212122ACB69AD0F29FF6E4F3CD6E19E7F575EC0301CA748BCABB6851E0ACBD446B0A257388232CBF938F3D41B8C3812ABFC56160C8FFF23361245FB9D2B8AE506116A8FA9E659BC4E530203383F549C82BA607EF6B07E3A9CDB763E04AD76953C5984E37176DD1ADE3C781E31D13F1E2EBA7CD61CD59410F392051ACFE879ABBC7C8F4E9E72E64811AEF70D0195A2F5B3898B272062E17D344E610C2489113851D86CE6BB39EE93FE95F3D06A5F4C92EA30391DF9594C173D86EAC50FBE5254EA376B6FBB20121F4C9976853ABAB38C8C06A325D7E4CFD6EA280F4B95E974A29D5ADD705B3E9BBD3E2C5D71DF25D6A3ED0D2EDC2C7FB8258B30DB02CD6B66B38DC484949BDE2A6E63DFC9BF539481D4E3DB8BA834E5DFAD4A58D77CC8719C56730F58D9A2699A97FB6410CAB267D9557FC5DB345CC9ED8F2ED73881D846F8FC2A4A6A5668C75D0CA69F1D743EA725404320960F0828F92EDAD9D26A445E7025747F39B6BBB74C5652BF7D0312553B93F9E5D02780F2CF917C49D636EAC71D62EF9446E1ED36F0263878EFD916329AED943C1DB665639A44F681F78C71E7A029F65B3C9D262F11AF0653F17F3EA3ED6B21872F6DACCB2FA4A50B95F3A2CD51B60F3E9F5F68DCB15D8376B3EBF81720902C7A3FBDE740F02155BE5A58623AA3CBA3406EDC1E611442D3CC5E2C3323A5EA305FA31E8A2E8F09BA64D2FC6B86E7A1B656CC0A4B82AA0C7F28FBBF5974157AE1CB53ECE0E29F682593AC088B8CCE287388956323886DD6070590CDA64F0F121251836BD0640D3037A4EC32C64DD0B028EBD93F8F8D45716485A2264A9903C48385E9E5F381B2B7CDD415670734EC5321E292ECB33C06550C7FC02CE30D92648F980863F6F6F1C08EA6D0E00FA85FD3A64DFEF19132FF8DFB274B719662B362C0278639556FE9030F1E05EF9946CA2C5D7F78712BF117A7141EEF8C45561F8BA32A83F1FD9377D9734393E7CC5B8D459B8A117ECF61662CD98E8F6695771C8C79CD6CF537A9C2D4EBEE16839109C4C7FE8E1E5424C9347FFD9251B8D5079D0A5502AE620C3F79E65AC7CB656F0A5F809BD2A0FC8061DD653E7C81FC832FE13FD1C62F407AA80208403F93A13DEEAE48181305370BEB0B2C3C8BDF5CFBE4CED3E6AE6FCE970D6B7099111E29203AAE1FF8837E5BB891617FAC03B28BE32A6635E8071C8642FA686F1C5975F41753C7D69EBFAEEE6814C43DAF2E92D1704D95D3556040E1A054DAEA3BB0ED17F0C3C1C9F04DC878B634E4F026E8F4F024A083BBE30446D57B270C23F0BE8E68E383EEBE7953B9805C3E72A7BC08BAC2F0B585E653DF37DED9BEF3E3CB2777C7AF7B8CECE769DF5BBC61E57C6202BE3E5B784B577AA83AF8E07E4D59746469F620A83418E3BC0F044F4A1AE456818FDE167F0C58912FCC09ED82A844BDEDBC32334FFDF64273407EADFABEFB9D1E7585B2F8087AF667D200A759E38C0AF74E96257DA271FE22F07F2416E6D2FE73AECA96EA307DF19323E6D5669E4E581B737791E2D1ECB8F5808EF71D0C17AB823FB39ED2DC225933DA47364EB3C0A8726FB9922C0BA18F5DB626149F8E9E4C2E43C590E61F1A6DA4E0D7DC0A402929B71CC551D78FB67975581596EDD90EB0FBAFB19270F2BD673FC1CAD7683C4DBB37A9F14D3E4295ABD4BB3B368C863DA20CB8B34E935B217A6BFEF8AE1662BBF3D2D9561CFD245A0EFB7B76CBD5971A877F036B52C6C26D58C171BDA4A71407E856A2826C799B7D87551B11CA0F8E0E8258FF25BC705D0B343BA9E53CD26C7A572E032997E0B713C277CA4EE98F640BE96303817CB42A8BE4F6FCA93BFFFF18B7BFEE221BC03216E2E5C5589D11CAEA835F58626E5DA3964966FEB0DCB58937EBB4C4EA3154B96FD0EDDE5E8920D7F46B23AA6369F5D9D38BCBB5D742D6F213DB48B978BD21A8A92857271DCD7C5960A5007A2879AC10C98A9640DDE7BE70E4486EDC0DCD5F8C0351B74778293D361A3D873B79EA9F937C92B057A2030A9178371B6849D43F7304467F04D6345AFBEC44C5EFB862F9FA3DD44A2DD073CA8E77027BCED15EA35DA699F96AC21D2E7F73BE2C4434EA7C09916077FEF9B0D5BC4F7F1E2FA709C2C137EF04FD997A83B86BB5FA5117D09BBDE65D16A180B21FBA11B8B8F553E7D1A1722F0769BF2115BB63C634F8793C5FEA86C34DFFC228A576FA3C5D783FADAEDA0E8F2EE6B0EFAE61751B2BB8F16F92E73D9958AB5F70F7C803CD225A3BCD2829BD3F17F11E2DC9C722A3A856132B67C9B7ED73E8380625230A00D0F15FF9E7EB70C70C08EB79D3907A642108F51FD6CC52E7E058E8BE5661D3D1C8A22B6C54D389C42D83C6CF6AB1BE448D443DA74B500A3A7E8E2AA0E7196BE8B57ECFC7BCE92AD25E48E9E1D85FF5E36E439BDB79AE42D1AFC727EBFAD636FCB1A43568DF7DB5A790C6071BE5D441BF696DD17B83B8BB79B551F82E6C28EFF24AFEF1CEEF648F54398C06D1B0732C579910D13B8A7488672DE14D6EC8080868EC3D0E73A5856AA1F9B6E70B9FD574FE0B0DCBDBD3472C6562C777DB8843CAFAED9EA906EF4B7C3A2AFA07DCD419BA977C578E287446ADF8DC96D59853E12B17608A5CCB77020C0E1876455127FF296BEBBD3A2CD7B450722CDB0817238633AF83354131F3CF55DACE2DC0E4C91CBC31BF40D3D0402CEE3553362860E67301D088802EC339157D24EFCA5B9EC14E245B4CDCB241695B97D205FA81ED3F0DD888ECF30DDBDDDA68B619B2485C3C0473FEFA3DD2AFFB465BA50D741B6C86E19E7877425B51A507DC1D2E1D3F195077DB573C1A9EFF452878EE9A0144163FA00905788D7EC268FD65D4A2568CF8BF3B56D30C998873D1010F6C9150E7E07321BF9C578ECA433FE1E6EB84E0F66CB6B4AE74999FCC3CDDA52A8FE4FA34E33565EF27F1EB019055804B8BDD5713F44708D74550E0695C3A583AA56C88F7C400796014EDBF0293FFC5D585AC7DBEDE1ACB3E3E01E98F185AE7498EC55AD619947A225DA2302747B39A4F6A7CD7258128BFAB4C7BF47A794EB7E64C6BB8A32F5363ED12C27D90EB8B769A2E13A0880CBF507CF1CCB67DB93DD0000BEDF5650716750CD5CFF216B3765DE8EE2A7DF776C7B40BBA06E38A32C6ED76CF1BC58B1AC0916AD9F363B1049B66373CA35D5D41CE4AEE0EFDD81BBCE9F1D4271135B041DDD482B04B1AABF7C3170163FB13E6119E15572DCBBBDC93FA2241CFBD3EBDBDF5651EFD5F1DF409C2D7671FE368DB265B856DE56D88F59B8162EF347965D94A9BFE268E5AB194262DE64BB5B775AA72CE1A3E65DF48F10757F46D74572FD5176BE836240F81EDBEC1BEB73E22AFBCF0534EE63A6B37D0785ECBDAD9C972E9FB9AAE8B2D67415D1AB2A728D79AAE689D52B8DCB0A2BC59678724A97C74101D8BE4124FA273F6F70B92A03E53A73F3FB7A45B6F5D9B7811C4E1FA3E4812D4FD3D56E6D1B9FBF63DC2611FE415DE5AA0713007BF864F2F33DC1915E3E38AC0F9F999E85704291433474532F48D8B938C07759BA3EB4B47A36A3E2F8C2E4F1E52B34D3E3CB57EA5117D12A191ACEE1FD29AAE3738FB37AD40ABD76BD4F9ED26261BC66FFB98BB3387978177F775AB26A364E2B575FD5DD2C28B60AF285E875A1B09F1DB61C950B6C109362D721770652413F69BC29C826847E5AF89375DC55142FDFACCB379308924083AEE8FDF61D6353657EA4DA3E456F2DBB00FADA5F8CFE439C7C1D168804A827F29B2FF8B3EF5E2FFF418C5A9718B3E8793B046BBE9EC4A1650AFA6D172FD9AA587CEE0E2B60DC6C68F9094474C977226C26305C51D6FF0567FD070E6C945073C6A265F9C7D9E16C78C7808E8B939957F0E37EE46AB93A7EDC901FD76A11FCC963821AE1EBBA7CD84E8AD4AFEB41FCBFA54F05048AFDCADB7469BE2DE1F2DED73FD9976D6C75FD3AA41D293ADDA6E3B3EDF0E8EC2FCF2F2EBFACE2870813E1EEF0A660694FFAEFF587E85B8587013BFDBA6B3E9E3BBC660FBB55FF48AFEFB1F6ECEB63A5810DA0E7FAE7BBD3A2176512D2326E9C9559215D26FC6D2A1C5753A7BD54DD7DF283CFCE12F7B775B401FE9C1CF9F819392B9BDE1FA21311EAF4B8177500CFBD78C0E1E14E59803EB6615B814E1924F6C6D3863FD3BD2F7FCCC386E29FC5BAB82D56755E97FBC92826F23E19BA50DC3CC69BEA2187D4AF089AE7AB07F0242C0A8DA6B92CAC92CF31FB36C4EFAB2C072075A3B81DD60E9B52A4AC1B037994519165852161D4F568BC65816AF85D092FD0B81C2BA55B8ABF5765F0B90C93618358BC59FE6BB72D7679222B8B7FFB2F6EF38CE600242D698396EF900B237FBC36C8C488B2079613CF391C1E472F5B2980BD28FA7C79CF4751FB3EF26887521D600E910C9FC065081F61569736977F5D719B7AE15933535EC87667457A271BBDE23DB52BDEF678DC793CEE1CEFB8F3F35D6F7DBAEFBE050B36425A5C2DDA72B646D6F83DDA359F12415CA86755B6D629A08EC47D2648037467D48F5B0300DCFD994A20EEBD386E897598396E8987BB377DEF3BBAB7F106184B3902EC0EE93CD27569E59ADF0874F0E0FA70CE85F21D7C66C932CD8A916FA2E4D9AA3F5CF9239E5170501E4DDF39FDEB5326EDBB6B4F71B20800B6BA115B48A523DB008AEF2A5E7CDDC949AF3C7DC812DC81C55D77DF3F564ABE9F36E1E64FDD6F0E26890FBF66C53418482C6BA42B5BEB4AE9F2A8C32A6689F144C7CDA889D9135BBE3562CDE59A82174751BB940C6272D5DFBCB5A19E7E26C639436CCC1DBEF95561BCB0FCE36EFD859055426583BD41E6F42088F0CA9F83B3F8342DC658859C179C1ECD2F8CFF95EE601BBCFDBFCCE287388956181FC0C90057609D95E0B498320F69362C432661E7DE3E9F5D6B2C876D7BCBA04B4C8DD8560BF9D010F4F5CC4210367A1743F9D4E4D9411DEBF4AFB6537D018A783CEC9DFD457659AF31E372BA5A573B3ACBF6F30C39C9EABF9A3B97AA23B771BE32CACC1AAFAC618C4FF046BFDB2B2C1A9E2C48E100CE27CF41C12ED6CFE362E733F50D7717174235372CF7EA1D42ED6EAAD76A6A800F7D6ABEACAF9C6DB830219D6A00FAAD3202868EE6BA4C9595B1047C9E8028E4387958B19EE1E768B5F3D4B7F7495E188ED1EA5D5A7CBE21D154204B21739217A685F153FCB1F2DBD3D2B5D6B374824C9E478BC7CAD914D9EE31D055539D9DD4B4AED1795E2E4A2944C9C2219E9364D6A5C9225EB1D623CA7247F3AE3C78703804AC6A0D8D64B15A2A03E2176A890833DA63F46023F641118EC37910772CE453CBA01BEB425DD410E6E4693C051B385FCA63CBBA20AF6232B6E1C3C534170C349C97F153BCDC45AB3A64A4ACBBDBE29BE9EFAF3B5527D7EADFF21A56DD5473A4B03A0FD739745FCFA078E879EE4CDFD97F4301F99BB013A6A1D0C2F27C0E5BA9A90027E8CD74112345FCC7A254017AE4701470871FC5E525A9B01D064E95FD1C80718CBB60051FB16F95F96F35887F72EDEFD0DD45C366E0C1BA87B379AE23FF8856F7C68FFB9323F3DFA35D969BFD307FF31334E11793BB00786F667EB04E5F2991A401D8DF1AA31A9C9997A6ED29AFC5DC330BF961152FEDCBE10979CBE0E57A8187E7BEDE6FCB110EE8C39B8762FA0DE840BF0719AEC09A4CE68162538B4FF6A650360F095B066FE853325A53611B28BE4C679D846DA9365DED6D0C399A6C70865008E48B2AB3BB95D06D05B2000B55CDFC5C3C5AF0C35AE9B1F389859F838F7AA8C394740DBB6A2D2B16DF9AE3307E6F9E10D3F1759F3C919E28B09EF963CCF96BFE8A3DB803FC99DEF925569F0CB9F55434135C03171FA11B4DA8367A8F4B14B74E84503213DB2AC437CEA0AE7B477E90118D20B4D0B20A28A2EE338C202BB1AD8042931B0A2FBD91043786CCBC898B7C38D29E47053F16E98FAD30B15995A932C9B98025170AEEA093B3B4DC6E641D6F5469B80788190F7D0365947B1C212FB86080F267F25170315B035DA3F213A53E863FD6732A8D20FEDD660B7EF4EF0A4251AE3078BB39B9AFB71A86C7E41F6F7819F81E6F6085BA8115F4A68AF09E43E05711249BFE5D9A5D9E5F1C2DFBA3657FB4EC8F96FD8159F603D3A81E2DFBA365AF11CAD1B2375AF6DE03078F46BF9EEFD1E83F1AFD48A3BFBB59E17E2BA8D4D475583CCEF02FE9FBA82D8CF1AF061D517702421FDDED78B1EB03F6039430AA41EFDD7703F7AFC37A6104D89394CC3D44F5F2F78506DD2A4E87BF63D9F0F0FF216E530F41902D9300DFB2BBBE36A8831EEE801998F67D1CCA92EB9E38B71DAF35341DA407F2EAD68242F3A48BB87A9943DE1FBD592C32B68CEB87A5EF847F55D1A96257CF93E58BEBB46C5CA56C3A79C356F73F00A517BB551E6F56F1A2E8DFAF2F7FFCE1871345101C77B07B62131A12B19DFFA634522C322C2B1F568A56A769B2CDB3A810ABBA22154660BC8956DAD14A3560EF18B4D8BDEA78CB25676C53ECB58BAE9985806959E92FDC95AE45696DB5C9E897571CA29040BB838723DFB30430007FF67D4714FC82FAFC9064CCD5332A829AC71BEF34A20B0F9DF6F548906957784070019FCB9C2D5038DFC69D3C84A64CAF6DB8CAC2F7157E173F2D05330D1B23743A9A2008E207320186DAC1619AE663F967A175B26200D7C21A3685EA697BA107514F71604AA81BD87E68A262900BF62989735517B523D16B22A1B2F0ADA5923D4692389259A349E8EA8488EA0E05E878EAAA4A68E27EDF6B2CF5E3983992BA8E4EBFACD531F1D32D68624CBEC8B32D3BA045AC19D27E2C5F5A9034CF55D53846ECDCE91F99069F2BF19953A8818E242C9808DFD72FA4DAF19190D55FCE9C66314B8BCFF02CC3CAB08255F4E2E2D5FCB467CAA7E9F64C354FDDBBC960711125BBFB6241DD652C932132D2F2A47641600B15EFFF22058C6A3FD629003050FF036892B9E0643C7DE2089289554A75FCAA38030D5EC05CD624CD2F7BE730AEFB3D5767B13EF5E544A8B06FA3034063F2ADF33420216D9A678594D1D0813366F61605FBF7E5AB7F0EFEFCA86FAFF2928B0E0405F816670505DB6ED5B32A986CAF3A3E20085BD56911F166B78CF33A9AF60E10937F30F4ED8940E07FDF73107043993F00CA307EC2B6828FFAAF3E5CFDC3DE6D2AAA6ECF754F71AA0B2F1F1F10232161542B71F46FBF071FBD7A92B3B99174A70AC8F377179A139989257BFDFDC5B1CC1E03F5ABBAA36140684EF4638A257B8D01712CB3C7807C47EC4EE8BF6935D05C2EABD1A114EE1352E4CECF1335522FA70CEFE92F9A477766E98D0520A14F72E890507438101207B657186A6E208D7E662A3D5456F1EA7EDBFFD35130EFA5A6A9E98F445B10B44FF92CFFB5DBE6659602FD2244F87E0818F42D420CF9D220D0A07C2D3FC0E086846AB4BF17392540CABB8577A0B0DC9081D311FCBB903CB7FAF703014435983D8542F9777BFF34B6DCCF73F8A67894889DD0F197A9422208FF59BDA1481A1EA671D395E91983ECE488B2BD449972877E2E180BA8C410D8B2E3EA8031E580A766DBC5321E4D735816B5774B82DEAF99CE5A1AF92E0D15A073B841C3BF58458D21E3EB0A5F562CD8E3883261202320C83DB04C78796C5E60B245980485D164F1269341871076323FD0702A745CC04CB7488D0D14EA2A353F9070EBE6B82071B08DF6142454CBA94E14263F7F392FA8E877E547AC8C89952A0A2A4E58363BCDF25B96EE3677FC2F38FD52D5D302A72925A1C7099101F053F77D04109141ABF472163B6C6D5AA261B999666BBC8C9C86896ABBCC21F59274864974E5793AC79C970B6FDEEEBBCBF38B999C65B598F9946CA2C5D7F74B2F68C1E992B64988635F762020E906B4E7F0B09E3B1DF1111E1F8AF77FB228BCE8B98CE3A8F3E28F181D21B42BEE7DC4927D4787381C5483F380C4E841754DBB10180E21A8AE1D0AA6A9E983EA5A1008F035A42BC27FBB592B06CA471A5D31343526739A35EDD73EB3FA6F2B2254D716FFFBCCF503DFD7996908AE6B53C7A0DC8911E463EC3FA68ED31FD37EA087E74F6D40B4B818E39AF8F891F8A35D0FA7806C06D922EAD39DD3E2AF87347B462B05A0AE0A0AB97CAF94053482792A0EA0A733B9643A862A99F6F6E0686A857E6570E2BC13C28D73F43553B78BE3F3C507FD8EF8F8574BF92ECE042F63288E6993148CA638E8089C41C29AFEAAEAF99371BF8ACA54604481D892E65A7253B6D7A90AE4D1A074C3B4C90A76498985388F8D99094A2AE9BBD53FD1D4010649DE2E8C571D1CE3A363C1557769EA6F3D8ADEC70266EF343D1E55D3AAF8B3A23F97F7F7771FD245F573F86434728B023FB570AFF5BC329CD92B7A050F63A88139406234D5E08489592889D17584090F87A521F64741B43DADCEB0A4DE9B4379792AE18B8A05F8FD02D0BA09290163798526C6470DBAD9492334652D4202CF008DB01F1072510413A148AA371B408D68AA4CB8328D6DA8EC919D52F9ABB0C7665E126B4E9DED6E242BC5E124EC73C120CDA64FAF2CC1411FBB7BC443503C948721BBCDF47818C5AD35A2FF723C9716165B3388A4C01F5A78788365FA638B11DD9AF4738B69E170CD1E2A8F45E98FCD4C671735A1F0F1DA9FE82EED0CF069AB51379EBE7ED3CD513E7F3D0E4C5375AFA63DC4C81A756F39B1C27D2DCB77472A129FA756E34441E1954BD3A9E9F683E9625786F47E88BF8CB317ECDB13F781FCEFFBADF0F9A1CC5FD9578B52E598B4ACFA1DA1F0DDB85F49385097FD908B7DDFCB11BE3F7E99EFBA35E152BFD86505AFFA22BA0D0222B5B4EE8B45330683D4D5592142ECDB84B058C74513599D7504E83EFDE3592020B428214B2809A71FC640823818B48E980710BAFB81A35FD284BBA18749E827BA46BDC2A91919A6E5E96F746A00649807C395C29E8189AE13A603945075BA5B1A2C8BD3650722C375D08A50F429373F3941060449A86BA0754747C403CE275D756B727DD27DFC37791E2D1ECDB782499FAF82823CB4CBE48CAD58CE5EBCA93C79C5BE3DDA2EA225533EF5ABA2555B37FA2E831DE28B832A9C31B1C50D8AA26726C759B939DF21348D400F7ED6A664F67A47ECEFDCD44FD5ABE9AEF9881B233C2C5C2C98A03A680A448D6EE0EC9149536DF346F1C08EEB6C19CDEBBA2F876BF559EB4594ECEEA345BECB5876959632371CB8F8F9EE4A93E2090E54BCDF880046B42FF0B85A45C9BFB3C8B0B07838836F1B11B747DD8FFBFDF1BB71ECCB27AFFEEB4B15580F5D010520FCBEDF1F9F1FCA7E7D7FCCF301DE30A04903AF161E021A8899E06703899B0D5BC4F7F1C2FCDE912F4CF0ADA9ECC4D203408530A0FD82C56EB349B39C2DCF5899467C0464880D02E090090E011FD298F60A22424AA5318C8A61F9B4F612200E09B6A6C5476D1521AD4DC04A840DC479DA9B040B706483B3269D0C04D2836C133D0D32FD4378233F11E2F20EDE1C9E09E16C67F9E9C4A6C8A0421CDF6C95C7E9C5F90D77DE04BC8EE4101E81D58C0DA5B066F01EEC4514AFDE468BAF77ED1F36D3B7A593FC9CED8FF8254C6C106417D4E8EDFA3C8AD7541810A6C5B6CA2C1414161C0E8A69CE1019599538A0640E2AA40DCD179F938E6425A87BF439D22D165DA1D35D08F02244E8C84570E061C0D20D09D396DCC1C9B0F22ECD8ACD5D721B7D59B1BB7608D76C55D1E8F50A5F4DF8B0620121DD86D4348896BE30086A84BE8F88986E589836F94E4EF63A41A761F08821A981B9238534D7A740495B693E08316EBB0522E87B3A6CBCA75877F4830D8B0D122666619BD449C2B87FE8D36F388103838CB25EC908CD74CF51D28D978A96D777F3C0CB4D9CB3F7EBE8C1E0BFEB2804EF5DFFAB930A2121CF97D70E1E6B7884E01C766DEF265B60BA1E084FCC8F040C876DF53EC2821C4E30392A781F093ED6C4B397640E7127237B4A9CA24FE6E02B5100830E3908019A99841F4C011E972084390048F0E464D122BFAE4213A63D6BE4FBA2F7E5F5140775EE280C0DB56CCDE0F491EB43EFB66D7E3458357408A1F0D3D487A1D3151E146ADA51ED2160AA3B3DAD7B6822D5A3EB8E8EBD44740850D28E6E5F2175A71B114A21A99701D5C24382D868F7FE7CE16CDAAB80F209987258695DFC061D5AA2CDA8BE2746FE6117C529CE337523C434FDFFB77765BD71E348F8AF2CE60F18F3EE5D2089E34180643C8991BC363A6EC6D16C1F5E757766FCEF571275B0C8BAA4262939BB4F899B1F59878A64F1AA5ACCF1266D651FD6C79329DB9FE73432C008DEBC07F9F94C0C0AA8A16C6BBC383BA34F39FE6F68CB34B4EE5BCF6F62E74D71B2F74257CDFF8530860E1E7E6CF7F71166D4930C5B4BF9B4DE653787A50CF2A8AC63E06E41638FC63A920D3773D9C91CC3CA486359CA6C55731FF8DB4C78978377C9CBFE307D6B72ACBDC58AD072C873AF6B9229D655E71F42A065648A7B09A8E32D6216F712A35C4281460D1A0B318E90F15423C52CD6907D8C50135CC600010F2814FE45B0C7EC954CB30DDDB8137B84C87E28316E8458C639686321B51CB98D4335F8BC5493183F5FCC6B0DA559AFFE30E5AE381ED903831A082DA0F961DC6DAC9E8E1F03B7FF398D11D4AC66F8F68E202A0BA8D89A336871CB6B7A4F61AEEF9ECB4718F9DD677510EEEB940E95923F9ECDD16E51A997977E55EF3CCF2F7C71CBCE4084A52E417D46E7F725465C8519E91A2EFD1ACC3CABD249677C8B32133EA95B122309E20AF9453F9381A8E30E2DC93CDE6C8BAAC9579B4D698EC73CE60148C22482B0E4E51B0714E8A5D98636924244DB983B8A4256EB981469630869BC000BF9641E9E1FB6A66C6F9BDB6068590C05A5EC27A4C3102FDF6C70C154CBA0B6E6122C47B70B162779D81CBB5F8BDAF97A539A8AA5D7CFF3CF2E37156F77DFBED9216FD5FEF5FEF0D060A489065486AF4E61C98807CF180758D33E22CDF3672046060BC285D310B6D04C76F4B6AA737A6E16ECC5DE949D551F36E6B6288FA79BF569FD757D0CB7D4EA5AF7E6D48D3E0F0FA5D914A7A679BB8B6E21EE181362EE1FBE9BDDFA9FBF6CBE1E2AC3A8838BA04D0526E611EFBA1EA88A714000513608AC96178EBC40514FA4DF42A269F51096648F1A49B9DF8E1019E8911A3E7AB0C00E783912B0004A31B2003046F2E10D1B2FF78013A51EA00223D500F4603EEF8B1321B5578E11F6202241E7692942CE29C5893900AD92BD0891B4963D20AB660F3B8E179909157559DB6DD4DE50D16D01AAE3B64C681BCB4313D0C140184D0C2775D713DE67DADFD12E7A1A352052147C00FBA59434EB6B789F4CFD7C1823E896A2D406C051A4F5A6FA0B21627FC65AB72552AB704F286C1E96A3742044B2EDE2E1DF9F9F68825E396AE910224978D86E4D734FF683397DAF53938642061054CE00250F9A95BB7D2CF68FB507858F9B00400C9D0023D07CB7FF71C027E3BE04A3D217EA9A7FB5F9F37C3CD97056142117C39074613AE2EF4E664793B5A50C410BD093AA56F587CADBDF169447C96005367CB88E292D43239819C9C8C7F3BAA789D087C51859881841AD7DBDC9926C3112DD16A6D3F9E7FD531B8097D2F68060F43C80A4A163FD5C77891B53ADC8B6D8C801CBD1810342740469522C116DF394FFE896326454E682A6C2C0DD0D1F453A1F3E503DE5B4EF0E9819A745F0134E0B12C99EF7B88FD016E0446C99D076BB93B1EAB632102A2104A317A2749465C22ABA53C912968BA214E455962CD096898EA2D61802DA57FA12D27E147DE29379C4BF5E578035DE9549DFAA0D22F7BEF88A7D23B714FD362E4020457568A61BEB3A6F8322BEB4534612507DE34FF53D9BD2ECC9ED121F807F148811698244CC0849508E5304903104B9BD380A28B2A0DFA51B323F93D4797AF2CC6ACA025DD47405E87CDA9629B97792CBD372B8204E2217A7A4DFA6762749B7E51C559BD75CD6669F9E35D4675F846AB42FD5B827E4C60B2825DD91315B2DFCCE5D08A1898ED8C343B219E2A42186A40D9334EAA8FBF1EA08067C18CD831F9E4FDE6983372A905D3608C077D8E025112D51861C4F48D6EE10B93454E95086EA712856CEE894DB058AB9395D65AE436C6E92124F454D61080A44521A201CC501255086691102AAB018A30811EA4D58F95048771A34E118C80BBF145A2909150E883CF4488E149CE838D07F052FAA06FD2D3C20FB453CAC668B9CF29ADD4272835CE73F83E01238A1AE94A4D40124B7E4807625FB33EA841CC6741DA279AF9CFD44E3085267F5B09C3FD8521D223ACF633077B52F245CD6FE49972859F3922B14A8F91995A32911FBB3FF2004E9CF3E04EFCF3E4A5EB66177EDB0D51B862316711854DC23D81FABE9A7747C41F70810DD3B106AE07B0A422569B67DDD74676C92ED4AD0B9B52BD46D0D917E9D57CE6C077590717B5112611FA7D88F9AC6C86D79D8710B6C1EAE60CBABA13D1FF9CFB9288BFDE36DF137773AE2A2D8B3111728B0F0BBF9EB786B0CB61E1F8A306243A97227BD1A0E999DF4A694DB496F009A75D46FE76263B6C5DEAC68378202926B29043B8E971BB3DED4FFB961D696145AC115AC308EB5F7C51E5B7DA128052B16388A0591BA86B048F3CBEA8D294FF5AABC7638A83B10280AA38E024516DA3E7AF7C3945F0AF317CA4080C1C9073081F88FAEC65118702820C606851515315C13603E4508C25511E244FADD3EDBDB1D7E60192070CA1E4841B639B736ADD6C8F3521249B1818215ECD4EAB2BF9F9ABB5ED815170247B18240958CD097410204475C7B39C4C3DF1ECABBB71F64E21D4EC142075532D25D6BE0ED024572CC04608F1DE796B574417985DC7476AAB3779691BAFE5571E7C9077539BBE25071DF1A7F3E82DEB5B60DEA6E4E5F41558D51E30A277164F5475652088BCBB77C45057161590D85E8C4AAF1BC4AD804E51D8E57079AE5C28FF78D2846558F1613B99CDE48C8DC3A67D5E46DABA2DAA2764C2FB4A13EC38CDE90902A19ACC9BF981F2A89BC673F5E45E01AFD0AA582DA95AA1E2D30FA00A01194BDD7BF18A50DA9AFC6A84CACC5292C38066DD5459E6A2E4459DEE30165F70BC0893B1E7C20015B205E3D44540578D5C1BB0042D524A272ED78EF5BB0E6A8D72A133A5E7371C0171EEF6D0494E962E05283ED5DE84585192D287CB0C2F727169FA64BD10F749A46144F6EA2A8A56B5BA593161CDB36E65045B30B86C410C37C4302CAB88327DF26D07746F37ACAA154ECB4CCE213AA22EB24EC09A9514432E17563CAE542B611AD6449B1D057F1C40D2B0B8FF42E169C9915096422D1F33855C323C455CB1BE23E05A0C822870F29ADB8EEEF178B5ABF88D40DED049216C07D9DD9708EBDBE9C755C872229A44E246EB2E10BBC435D5986101943544C31D1F7B4B62AFB4C76822FEFBE8225C5455031C5455FF35AF78E7DA43BE1EB7AAF6F57800061CF521D4615C4AB61AB16E125F04C2A824F85572197E89E0A5B258B82D067D2DDEE0CF7F879BC8ADA73247ED11782D2ACF4BCE3B9A62675D6365DD4E09D35DA5518741401B07AE12B72B705E661F86465D48FAE573D87A416002C99F8EE3B76B72EFA403D8AC8E19B73CE16A49A5105A4EAE38FDDFDD684F7EBA994F7EB74EDFDFABFA6BE697697DDE66485255516388EE035E442D39D7BE419ABDCB80C23B69E34D568D1B028168D6C5C748A85EC47E192331B177C852C4ACAB29B01E4748C54560A00A754C80C5DCAE9EDB2220038A522268C4D311581CEDE0CFAE75245F3926CE5FE225A065645A714F0FE2DD00CFAA8ED72055F3815832B3AFC54EC42997DD10BEF02655D368C77DB26BB6C53D74FF3B96A9DA85D38278D5A066C3285F841AADCFA64E4A9784AE0564308F8A5AB01C4D7E2361570607CF1D1986176166143814D169DDF450B416976D1BCC865AEC01145052AC42FCCE0C8288CCFFB9DAD2F64FFCF890E71A200A1B3434779CBF8C5BB110BEE3DB343BC0F4DD0B767D823270F832120EE4170DA3D7124989FE63B6BAA096AC003120E4AE1230CCE660BF0F890B4080C16D72E321E34820362CD29235B21FA51F122D4425A02068B6B09F94ED89BEB105E104EFADE44008C7AAD008F28EA1DB6E29152A69CB5D73141EBDB020575B0EE0238B6DD88A42DB758BCD1F1EA992E166DBB6E7964A3D52A6282587E4455F22E080E8C69A65494D8A6B618F93582E8E4B7259071BFF24CE26B3F7CF2EFCEC91D516C37ACAD9FE38ADCC8942BD1E26181781BD9B800BB9C82828634E1812FB70FADAE54F59258C30295260D293946944C1DABF16314EB211417733CC9B9C4C18441B73271E08B169B76858A548E5022DFCE5ECCD738EB0432B6BF97D35FB761D25736143BE1B207189A7918AABDE1190FC41EBAB825E2E322C1E127FAEE656B93F49A64404461748A1D4C99728600F4CC741380224F3561107D3BCD30D1F1C78BDAC7AA5FD13D34C0D04C0751F31B96C980F861DDB05E94FEE846C86724C5815CCFC4E2F7B73D948BCA9F4D70274AFEAA6D15133B845DCCAEA72824E940AB272E97C0852223816644F9B13A694ECAF8C408A176623ECE2144F6BE865A55B05E743B588CDA6CA2865E5CFCF4CDC7300E264816611D4B341504AA0054E488B6B10A534410A326838F24065A33CC8301DA60325B4C574A93B88237000A2A0B04126B0059DA920558859D24540AF0A179C685540A686639DA1F748BE37A82E9FC03BBF60AE236104B16061D57E0F9E24C74696318F90748E413552FD58D9D0DA8343653051CF2D5B01F19C2E20A8A64DC196A46FE9E7EA61C5164DD5DEBE96213F78AC58C3E1729C04BD7C36A006213A8004B4334D487A5519510E40BE2F5E0C353A802CF89E468C307C45408B8E2228F06013C81422EBB95333D0097382AA238662C9F145E29CFC0E8475E13EED573704694587136B33C52422319CA31EC34D5D8C38538111067885BD725275B05E9D0422D9158CEFF8349D35AAF0F4F8886D6440654BF28AAD528F4C0C1D359494E85743BD341A01E6EFB3C042B4C7E4A881E74731DDD598FA00A3749DBCACF11879A87508316054B28D788C3258A4395E1A768034A21D3AC5D60272AC530E848DF7759CAA026621C280B125ED360523166ED242E23402AF4B89F41E750420DACF950B772A9429CE49C82320624E390F95942AD3B462500BD5C8953D6261D37E2FB68029946FC3C6F815DF740B56BC15748E751E4DEC708E4D42CE4E54A891534C3D21E0FF5AF5DDE9195322CF2F2878FC5B2488C4E3B9133DF049B68225E86092AB9AE6446AA7AE9D424251BF61BE313075FAEB615454B322EA9B65E05ACE88B56269DFB46911F465F39C7327341E962802E40CAE8917AF4EA6655239A3F1BB6C767C34EA74474CDA2AFFCD3AB71C8E7BD1AB288E3A6474019C1C25CE3561A3A8978583F3C6567B29DC7B023410B1C3E8BB564504A4D4917009C4032B7410EDE7698FD61C4603D4A75975804948D35060FAA1066C2BD44400DAF8F29F36215D84625E91B5492EF9E4F5CB88E1012247958CD8990EF477B251A65E8ECE66225D45415F25B585AD155963345E0D2AC577F9872571C8FD41220C030A256502861F303B781D5B7EBDF34ED7F8E7131B66D8CECC63E24661FCE21E27D7DA3BF6AF9E3D91CAD73A299B814B5B8A518ACEC2DC1FCC2254E6EBAFD150E9E7892CBB4B3824948BE9DA2C1A995913EB9CF201B880F2229C207A75144BE602683648ABB0334388D1A72DE1A701F913C3C3F6C4DD96EB4DBCB5E823E883A69D48212F35F7D6188984A121D26BDB3344505899CA4F6F1BCB52A3FC001D33354F598A34DB73AF69A1F3773AC0D48948B0C30BAEB5C5FD9C69AE9B9D89BB22FBBBEBA7FF86E76EBF687EACFD3A15C3F9A0F878DD91E9B5FAFAF3E9DABDA3B63FFBA31C7E27168E2BA6A736F5F3F0F8D769877FB6F87CA929F4CD908E272D441BAE2EE629839AD37EBD3FA55792AEA2BFAC333F95FFEF165BD3D5790B7BBAF66F36E7F773E3D9D4F95C866F7750BEE355E5FF1F4AFAF029EAFEF9E9AFDF11822546C169508E66EFFFA5C6C373DDFB7EBEDD1FB6854136F2AEDFF66AADFEDB73C55FF9AC7E7BEA5DF0F7B6543ADFA6ECC93D96FEA8376B37BDAD67EC0DDFE7EFDC34CE1EDF3D1BC378FEB87E7EAF71F45F332866A44FE1050EDD737C5FAB15CEF8E6D1B43FDEACFCA8637BBBFFFF55FC582A7854C9D0800 , N'6.4.4')

