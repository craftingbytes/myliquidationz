﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using Mits.DAL.Interfaces;
using System.Data.Entity;

namespace Mits.DAL.Infrastructure
{
    public class MitsRepositoryContext : IRepositoryContext
    {
        private const string OBJECT_CONTEXT_KEY = "Mits.Dal.EF.EntityModels";
        public IDbSet<T> GetObjectSet<T>() 
            where T : class
        {
            return ContextManager.GetObjectContext(OBJECT_CONTEXT_KEY).Set<T>();
        }

        /// <summary>
        /// Returns the active object context
        /// </summary>
        public DbContext ObjectContext
        {
            get
            {
                return ContextManager.GetObjectContext(OBJECT_CONTEXT_KEY);
            }
        }

        public int SaveChanges()
        {
            return this.ObjectContext.SaveChanges();
        }

        public void Terminate()
        {
            ContextManager.SetRepositoryContext(null, OBJECT_CONTEXT_KEY);
        }

    }
}
