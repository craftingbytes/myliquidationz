﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Mits.DAL.EntityModels
{
    public partial class MITSEntities : DbContext
    {
        public int p_AddAuditReportState(int? auditReportId, int? stateId)
        {
            return Database.ExecuteSqlCommand("p_AddAuditReportState @AuditReportId, @StateId",
                new SqlParameter("@AuditReportId", auditReportId),
                new SqlParameter("@StateId", stateId));
        }

        public void p_AddDocumentToAuditReport(int documentRelationId)
        {
            Database.ExecuteSqlCommand("p_AddDocumentToAuditReport @DocumentRelationId",
                new SqlParameter("@DocumentRelationId", documentRelationId));
        }

        public p_isBelowTarget_Result p_isBelowTarget(int? invoiceId, decimal? weight)
        {
            var result = Database.SqlQuery<p_isBelowTarget_Result>("p_isBelowTarget @InvoiceId, @Weight",
                new SqlParameter("@InvoiceId", invoiceId),
                new SqlParameter("@Weight", weight)
                ).FirstOrDefault();
            return result;
        }

        public p_changePickupState_Result p_changePickupState(int processingDataId, int newPickupStateId)
        {
            p_changePickupState_Result result = null;

            using (var conn = Database.Connection)
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "p_changePickupState";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ProcessingDataId", processingDataId));
                    cmd.Parameters.Add(new SqlParameter("@NewPickupStateId", newPickupStateId));
                    cmd.Parameters.Add(new SqlParameter("@Success", SqlDbType.Bit) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@Message", SqlDbType.NVarChar, 200) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@OldPickupStateId", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    using (var reader = cmd.ExecuteReader())
                    {
                        result = new p_changePickupState_Result()
                        {
                            Success = (Boolean?)cmd.Parameters[0].Value,
                            PickupStateId = (int?)cmd.Parameters[1].Value,
                            Message = (string)cmd.Parameters[2].Value
                        };
                    };
                }
            }

            return result;
        }

        public p_changeProcessingDataWeightCategory_Result p_changeProcessingDataWeightCategory(int processingDataId, int newCategoryId)
        {
            p_changeProcessingDataWeightCategory_Result result = null;

            using (var conn = Database.Connection)
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "p_changeProcessingDataWeightCategory";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ProcessingDataId", processingDataId));
                    cmd.Parameters.Add(new SqlParameter("@NewCategoryId", newCategoryId));
                    cmd.Parameters.Add(new SqlParameter("@Success", SqlDbType.Bit) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@CategoryId", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    using (var reader = cmd.ExecuteReader())
                    {
                        result = new p_changeProcessingDataWeightCategory_Result()
                        {
                            Success = (Boolean?)cmd.Parameters[0].Value,
                            CategoryId = (int?)cmd.Parameters[1].Value,
                        };
                    };
                }
            }

            return result;
        }

        public p_changePlanYear_Result p_changePlanYear(int processingDataId, int newPlanYear)
        {
            p_changePlanYear_Result result = null;

            using (var conn = Database.Connection)
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "p_changePlanYear";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ProcessingDataId", processingDataId));
                    cmd.Parameters.Add(new SqlParameter("@NewPlanYear", newPlanYear));
                    cmd.Parameters.Add(new SqlParameter("@Success", SqlDbType.Bit) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@OldPlanYear", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@Message", SqlDbType.NVarChar, 200) { Direction = ParameterDirection.Output });
                    using (var reader = cmd.ExecuteReader())
                    {
                        result = new p_changePlanYear_Result()
                        {
                            Success = (Boolean?)cmd.Parameters[0].Value,
                            OldPlanYear = (int?)cmd.Parameters[1].Value,
                            Message = (string)cmd.Parameters[2].Value
                        };
                    };
                }
            }

            return result;
        }

        public List<TargetWeights> p_getOEMTargetAssignedWeight(int oemId, int stateId, int planYear)
        {
            return Database.SqlQuery<TargetWeights>("p_getOEMTargetAssignedWeight @OEMId, @StateId, @PlanYear",
                new SqlParameter("@OEMId", oemId),
                new SqlParameter("@StateId", stateId),
                new SqlParameter("@PlanYear", planYear))
                .ToList();
        }

        public List<ProcessedInvoiceWeight> p_getAvailableProcessedWeight(int stateId, int planYear, bool displayReconciled, System.Data.Objects.ObjectParameter totalAvailableWeight)
        {
            List<ProcessedInvoiceWeight> result = null;

            using (var conn = Database.Connection)
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "p_getAvailableProcessedWeight";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@StateId", stateId));
                    cmd.Parameters.Add(new SqlParameter("@PlanYear", planYear));
                    cmd.Parameters.Add(new SqlParameter("@displayReconciled", displayReconciled));
                    cmd.Parameters.Add(new SqlParameter("@TotalAvailableWeight", SqlDbType.Decimal) { Direction = ParameterDirection.Output });
                    using (var reader = cmd.ExecuteReader())
                    {
                        totalAvailableWeight.Value = (Decimal?)cmd.Parameters[3].Value;

                        result = new List<ProcessedInvoiceWeight>();
                        while (reader.Read())
                        {
                            result.Add(new ProcessedInvoiceWeight
                            {
                                Active = reader.GetFieldValue<Boolean?>(0),
                                InvoiceId = reader.GetFieldValue<int?>(1),
                                ProcessingDate = reader.GetString(2),
                                CollectionMethod = reader.GetString(3),
                                Weight = reader.GetFieldValue<Decimal?>(4)
                            });
                        }
                    };
                }
            }

            return result;
        }
    }

    public partial class p_isBelowTarget_Result
    {
        public bool? Success { get; set; }
    }

    public partial class p_changePickupState_Result
    {
        public bool? Success { get; set; }
        public int? PickupStateId { get; set; }
        public string Message { get; set; }
    }

    public partial class p_changeProcessingDataWeightCategory_Result
    {
        public bool? Success { get; set; }
        public int? CategoryId { get; set; }
    }

    public partial class p_changePlanYear_Result
    {
        public bool? Success { get; set; }
        public int? OldPlanYear { get; set; }
        public string Message { get; set; }
    }

    public partial class TargetWeights
    {
        public bool? Active { get; set; }
        public int? OEMId { get; set; }
        public string OEMName { get; set; }
        public Decimal? TargetWeight { get; set; }
        public int? Pace { get; set; }
        public Decimal? PaceWeight { get; set; }
        public Decimal? AssignedWeight { get; set; }
        public Decimal? RemainingWeight { get; set; }
        public Decimal? RecommendedWeight { get; set; }
        public bool? IsPermanentDropOffLocation { get; set; }
    }

    public partial class ProcessedInvoiceWeight
    {
        public bool? Active { get; set; }
        public int? InvoiceId { get; set; }
        public string ProcessingDate { get; set; }
        public string CollectionMethod { get; set; }
        public Decimal? Weight { get; set; }
    }
}
