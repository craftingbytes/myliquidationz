﻿namespace Mits.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccreditationType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 75, unicode: false),
                        Description = c.String(maxLength: 300, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AffiliateAccreditation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateId = c.Int(),
                        AccreditationTypeId = c.Int(),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccreditationType", t => t.AccreditationTypeId)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .Index(t => t.AffiliateId)
                .Index(t => t.AccreditationTypeId);
            
            CreateTable(
                "dbo.Affiliate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200, unicode: false),
                        Address1 = c.String(maxLength: 200, unicode: false),
                        Address2 = c.String(maxLength: 200, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        Fax = c.String(maxLength: 50, unicode: false),
                        Email = c.String(maxLength: 50, unicode: false),
                        WebSite = c.String(maxLength: 50, unicode: false),
                        Longitude = c.Decimal(precision: 9, scale: 6),
                        Latitude = c.Decimal(precision: 9, scale: 6),
                        Active = c.Boolean(),
                        StateId = c.Int(),
                        CityId = c.Int(),
                        Zip = c.String(maxLength: 11, unicode: false),
                        AffiliateTypeId = c.Int(),
                        PaymentTermId = c.Int(),
                        Notes = c.String(maxLength: 2048),
                        Directions = c.String(maxLength: 2048),
                        BusinessHours = c.String(maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.City", t => t.CityId)
                .ForeignKey("dbo.AffiliateType", t => t.AffiliateTypeId)
                .Index(t => t.StateId)
                .Index(t => t.CityId)
                .Index(t => t.AffiliateTypeId);
            
            CreateTable(
                "dbo.AffiliateContact",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 100, unicode: false),
                        LastName = c.String(maxLength: 100, unicode: false),
                        MiddleInitials = c.String(maxLength: 3, unicode: false),
                        Address1 = c.String(maxLength: 200, unicode: false),
                        Address2 = c.String(maxLength: 200, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        Fax = c.String(maxLength: 50, unicode: false),
                        UserName = c.String(maxLength: 100, unicode: false),
                        Email = c.String(maxLength: 50, unicode: false),
                        Password = c.String(maxLength: 150),
                        Active = c.Boolean(),
                        StateId = c.Int(),
                        CityId = c.Int(),
                        Zip = c.String(maxLength: 11, unicode: false),
                        AffiliateId = c.Int(),
                        SecurityQuestionId = c.Int(),
                        Answer = c.String(maxLength: 500, unicode: false),
                        LoginTryCount = c.Int(),
                        LastLoginTime = c.DateTime(storeType: "smalldatetime"),
                        Title = c.String(maxLength: 100, unicode: false),
                        Notes = c.String(maxLength: 200),
                        RoleId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.City", t => t.CityId)
                .ForeignKey("dbo.Role", t => t.RoleId)
                .ForeignKey("dbo.SecurityQuestion", t => t.SecurityQuestionId)
                .Index(t => t.StateId)
                .Index(t => t.CityId)
                .Index(t => t.AffiliateId)
                .Index(t => t.SecurityQuestionId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AffiliateContactService",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssociationPriority = c.Int(),
                        AssociationExclusive = c.Boolean(),
                        ServiceTypeId = c.Int(nullable: false),
                        AffiliateContactId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ServiceType", t => t.ServiceTypeId)
                .ForeignKey("dbo.AffiliateContact", t => t.AffiliateContactId)
                .Index(t => t.ServiceTypeId)
                .Index(t => t.AffiliateContactId);
            
            CreateTable(
                "dbo.ServiceType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100, unicode: false),
                        Description = c.String(maxLength: 1000, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AffiliateContractRate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(),
                        AffiliateId = c.Int(),
                        ServiceTypeId = c.Int(),
                        ProductTypeId = c.Int(),
                        QuantityTypeId = c.Int(),
                        PriceUnitTypeId = c.Int(),
                        RateStartDate = c.DateTime(),
                        RateEndDate = c.DateTime(),
                        Rate = c.Decimal(storeType: "money"),
                        QtyAdjustPercent = c.Decimal(precision: 4, scale: 3),
                        PlanYear = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .ForeignKey("dbo.PriceUnitType", t => t.PriceUnitTypeId)
                .ForeignKey("dbo.ProductType", t => t.ProductTypeId)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.QuantityType", t => t.QuantityTypeId)
                .ForeignKey("dbo.ServiceType", t => t.ServiceTypeId)
                .Index(t => t.StateId)
                .Index(t => t.AffiliateId)
                .Index(t => t.ServiceTypeId)
                .Index(t => t.ProductTypeId)
                .Index(t => t.QuantityTypeId)
                .Index(t => t.PriceUnitTypeId);
            
            CreateTable(
                "dbo.PriceUnitType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 75, unicode: false),
                        Description = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 75, unicode: false),
                        Description = c.String(maxLength: 500),
                        Notes = c.String(maxLength: 300, unicode: false),
                        SiteImageID = c.Int(),
                        EstimatedWeight = c.Decimal(storeType: "smallmoney"),
                        WeightQuantityTypeID = c.Int(),
                        EstimatedItemsPerContainer = c.Int(),
                        ContainerQuantityTypeId = c.Int(),
                        ConsumerSelectable = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuantityType", t => t.WeightQuantityTypeID)
                .ForeignKey("dbo.QuantityType", t => t.ContainerQuantityTypeId)
                .ForeignKey("dbo.SiteImage", t => t.SiteImageID)
                .Index(t => t.SiteImageID)
                .Index(t => t.WeightQuantityTypeID)
                .Index(t => t.ContainerQuantityTypeId);
            
            CreateTable(
                "dbo.AffiliateTargetProduct",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateTargetId = c.Int(),
                        ProductTypeId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AffiliateTarget", t => t.AffiliateTargetId)
                .ForeignKey("dbo.ProductType", t => t.ProductTypeId, cascadeDelete: true)
                .Index(t => t.AffiliateTargetId)
                .Index(t => t.ProductTypeId);
            
            CreateTable(
                "dbo.AffiliateTarget",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(),
                        PolicyId = c.Int(),
                        ServiceTypeId = c.Int(),
                        Weight = c.Decimal(precision: 18, scale: 0),
                        QuantityTypeId = c.Int(),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(storeType: "date"),
                        AffiliateId = c.Int(),
                        PlanYear = c.Int(),
                        IsPermanentDropOffLocation = c.Boolean(),
                        Pace = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .ForeignKey("dbo.Policy", t => t.PolicyId)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.QuantityType", t => t.QuantityTypeId)
                .ForeignKey("dbo.ServiceType", t => t.ServiceTypeId)
                .Index(t => t.StateId)
                .Index(t => t.PolicyId)
                .Index(t => t.ServiceTypeId)
                .Index(t => t.QuantityTypeId)
                .Index(t => t.AffiliateId);
            
            CreateTable(
                "dbo.Policy",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100, unicode: false),
                        Description = c.String(maxLength: 1000, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ManufacturerPolicy",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateId = c.Int(),
                        StateId = c.Int(),
                        PolicyId = c.Int(),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .ForeignKey("dbo.Policy", t => t.PolicyId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.AffiliateId)
                .Index(t => t.StateId)
                .Index(t => t.PolicyId);
            
            CreateTable(
                "dbo.State",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50, unicode: false),
                        Abbreviation = c.String(maxLength: 2, fixedLength: true, unicode: false),
                        CountryId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Country", t => t.CountryId)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.AffiliateState",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateId = c.Int(nullable: false),
                        StateId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .Index(t => t.AffiliateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.AuditReports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(),
                        FacilityName = c.String(maxLength: 100),
                        AuditDate = c.DateTime(),
                        PlanYear = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.City",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50, unicode: false),
                        StateId = c.Int(),
                        CountyId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.County", t => t.CountyId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId)
                .Index(t => t.CountyId);
            
            CreateTable(
                "dbo.ClientAddress",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateId = c.Int(nullable: false),
                        CompanyName = c.String(maxLength: 250, unicode: false),
                        Address = c.String(maxLength: 200, unicode: false),
                        CityId = c.Int(),
                        StateId = c.Int(),
                        Zip = c.String(maxLength: 10, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.City", t => t.CityId)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .Index(t => t.AffiliateId)
                .Index(t => t.CityId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.PickUpAddress",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientAddressId = c.Int(nullable: false),
                        CompanyName = c.String(maxLength: 250, unicode: false),
                        Address = c.String(maxLength: 200, unicode: false),
                        StateId = c.Int(),
                        CityId = c.Int(),
                        Zip = c.String(maxLength: 10, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        Fax = c.String(maxLength: 20, unicode: false),
                        Email = c.String(maxLength: 100, unicode: false),
                        CollectionMethodId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.City", t => t.CityId)
                .ForeignKey("dbo.CollectionMethod", t => t.CollectionMethodId)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.ClientAddress", t => t.ClientAddressId)
                .Index(t => t.ClientAddressId)
                .Index(t => t.StateId)
                .Index(t => t.CityId)
                .Index(t => t.CollectionMethodId);
            
            CreateTable(
                "dbo.CollectionMethod",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Method = c.String(maxLength: 80, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProcessingData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcessingDate = c.DateTime(),
                        Comments = c.String(maxLength: 1000, unicode: false),
                        InvoiceId = c.Int(),
                        ShipmentNo = c.String(maxLength: 50, unicode: false),
                        VendorCompanyName = c.String(maxLength: 250, unicode: false),
                        VendorAddress = c.String(maxLength: 200, unicode: false),
                        VendorCityId = c.Int(),
                        VendorStateId = c.Int(),
                        VendorZip = c.String(maxLength: 10, unicode: false),
                        VendorPhone = c.String(maxLength: 20, unicode: false),
                        PickUpCompanyName = c.String(maxLength: 250, unicode: false),
                        PickupAddress = c.String(maxLength: 200, unicode: false),
                        PickupStateId = c.Int(),
                        PickupCityId = c.Int(),
                        PickupZip = c.String(maxLength: 10, fixedLength: true),
                        PickupPhone = c.String(maxLength: 20, unicode: false),
                        PickupFax = c.String(maxLength: 20, unicode: false),
                        PickupEmail = c.String(maxLength: 100, unicode: false),
                        Client = c.String(maxLength: 200, unicode: false),
                        RecievedBy = c.String(maxLength: 200, unicode: false),
                        State = c.String(maxLength: 200, unicode: false),
                        CollectionMethodId = c.Int(),
                        StateWeightCategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionMethod", t => t.CollectionMethodId)
                .ForeignKey("dbo.Invoice", t => t.InvoiceId)
                .ForeignKey("dbo.StateWeightCategory", t => t.StateWeightCategoryId)
                .ForeignKey("dbo.City", t => t.VendorCityId)
                .ForeignKey("dbo.City", t => t.PickupCityId)
                .Index(t => t.InvoiceId)
                .Index(t => t.VendorCityId)
                .Index(t => t.PickupCityId)
                .Index(t => t.CollectionMethodId)
                .Index(t => t.StateWeightCategoryId);
            
            CreateTable(
                "dbo.Invoice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceDate = c.DateTime(storeType: "smalldatetime"),
                        PlanYear = c.Int(),
                        InvoicePeriodFrom = c.DateTime(storeType: "smalldatetime"),
                        InvoicePeriodTo = c.DateTime(storeType: "smalldatetime"),
                        InvoiceDueDate = c.DateTime(storeType: "smalldatetime"),
                        Receivable = c.Boolean(),
                        Notes = c.String(maxLength: 2048, unicode: false),
                        ShowNotes = c.Boolean(),
                        AffiliateId = c.Int(),
                        StateId = c.Int(),
                        RegionName = c.String(maxLength: 150, unicode: false),
                        IsPaid = c.Boolean(nullable: false),
                        Approved = c.Boolean(),
                        Verified = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.AffiliateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.InvoiceAdjustment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceId = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 100),
                        Amount = c.Decimal(nullable: false, storeType: "money"),
                        AdjustmentDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoice", t => t.InvoiceId)
                .Index(t => t.InvoiceId);
            
            CreateTable(
                "dbo.InvoiceItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceId = c.Int(),
                        ShipmentId = c.Int(),
                        ServiceTypeId = c.Int(),
                        ServiceDate = c.DateTime(storeType: "smalldatetime"),
                        ProductTypeId = c.Int(),
                        QuantityTypeId = c.Int(),
                        Quantity = c.Decimal(storeType: "money"),
                        Rate = c.Decimal(storeType: "money"),
                        QuantityAdjustPercent = c.Decimal(precision: 4, scale: 3),
                        Tare = c.Int(),
                        PalletNumber = c.Int(),
                        Description = c.String(maxLength: 100),
                        IsMetro = c.Boolean(),
                        OriginalQuantity = c.Decimal(precision: 18, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoice", t => t.InvoiceId)
                .ForeignKey("dbo.ProductType", t => t.ProductTypeId)
                .ForeignKey("dbo.QuantityType", t => t.QuantityTypeId)
                .ForeignKey("dbo.ServiceType", t => t.ServiceTypeId)
                .Index(t => t.InvoiceId)
                .Index(t => t.ServiceTypeId)
                .Index(t => t.ProductTypeId)
                .Index(t => t.QuantityTypeId);
            
            CreateTable(
                "dbo.InvoiceItemReconciliation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReconcileFromInvoiceItemID = c.Int(),
                        ReconcileToInvoiceItemID = c.Int(),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InvoiceItem", t => t.ReconcileFromInvoiceItemID)
                .ForeignKey("dbo.InvoiceItem", t => t.ReconcileToInvoiceItemID)
                .Index(t => t.ReconcileFromInvoiceItemID)
                .Index(t => t.ReconcileToInvoiceItemID);
            
            CreateTable(
                "dbo.InvoiceReconciliation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OEMInvoiceId = c.Int(nullable: false),
                        ProcesserInvoiceItemId = c.Int(nullable: false),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                        ReconcileDate = c.DateTime(),
                        Rate = c.Decimal(storeType: "money"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InvoiceItem", t => t.ProcesserInvoiceItemId)
                .ForeignKey("dbo.Invoice", t => t.OEMInvoiceId)
                .Index(t => t.OEMInvoiceId)
                .Index(t => t.ProcesserInvoiceItemId);
            
            CreateTable(
                "dbo.QuantityType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 75, unicode: false),
                        Abbrev = c.String(maxLength: 10, unicode: false),
                        QuantityTypeGroupId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuantityTypeGroup", t => t.QuantityTypeGroupId)
                .Index(t => t.QuantityTypeGroupId);
            
            CreateTable(
                "dbo.QuantityTypeGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 75, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InvoiceUnpackId",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OEMInvoiceId = c.Int(),
                        ProcesserInvoiceId = c.Int(),
                        NewProcesserInvoiceId = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoice", t => t.OEMInvoiceId)
                .ForeignKey("dbo.Invoice", t => t.ProcesserInvoiceId)
                .Index(t => t.OEMInvoiceId)
                .Index(t => t.ProcesserInvoiceId);
            
            CreateTable(
                "dbo.PaymentDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaymentID = c.Int(),
                        InvoiceId = c.Int(),
                        PaidAmount = c.Decimal(storeType: "money"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoice", t => t.InvoiceId)
                .ForeignKey("dbo.Payment", t => t.PaymentID)
                .Index(t => t.PaymentID)
                .Index(t => t.InvoiceId);
            
            CreateTable(
                "dbo.Payment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateId = c.Int(),
                        Date = c.DateTime(),
                        PaymentTypeID = c.Int(),
                        Amount = c.Decimal(storeType: "money"),
                        ReferenceNumber = c.String(maxLength: 50, unicode: false),
                        Note = c.String(maxLength: 200, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .ForeignKey("dbo.PaymentType", t => t.PaymentTypeID)
                .Index(t => t.AffiliateId)
                .Index(t => t.PaymentTypeID);
            
            CreateTable(
                "dbo.PaymentType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Description = c.String(maxLength: 300, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateWeightCategory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Ratio = c.Decimal(nullable: false, precision: 18, scale: 4),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.CollectionEvent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                        ZipCode = c.String(maxLength: 10),
                        Address = c.String(maxLength: 200),
                        Hours = c.String(maxLength: 200),
                        Location = c.String(maxLength: 200),
                        Note = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.City", t => t.CityId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.County",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50, unicode: false),
                        Abbreviation = c.String(maxLength: 5, fixedLength: true, unicode: false),
                        StateId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.DropOff_Location",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200, unicode: false),
                        Address1 = c.String(maxLength: 200, unicode: false),
                        Address2 = c.String(maxLength: 200, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        Fax = c.String(maxLength: 20, unicode: false),
                        Email = c.String(maxLength: 50, unicode: false),
                        Website = c.String(maxLength: 50, unicode: false),
                        Longitude = c.Decimal(precision: 9, scale: 6),
                        Latitude = c.Decimal(precision: 9, scale: 6),
                        Active = c.Boolean(),
                        StateId = c.Int(),
                        CityId = c.Int(),
                        ZipCode = c.String(maxLength: 11, unicode: false),
                        AffiliateTypeId = c.Int(),
                        PaymentTermId = c.Int(),
                        Notes = c.String(maxLength: 2048),
                        Directions = c.String(maxLength: 2048),
                        BusinessHours = c.String(maxLength: 2048),
                        CountyId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.City", t => t.CityId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.DropOffLocation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Address1 = c.String(maxLength: 200),
                        Address2 = c.String(maxLength: 200),
                        Phone = c.String(maxLength: 20, unicode: false),
                        Fax = c.String(maxLength: 50, unicode: false),
                        Email = c.String(maxLength: 50, unicode: false),
                        WebSite = c.String(maxLength: 50, unicode: false),
                        Longitude = c.Decimal(precision: 9, scale: 6),
                        Latitude = c.Decimal(precision: 9, scale: 6),
                        Active = c.Boolean(),
                        StateId = c.Int(),
                        CityId = c.Int(),
                        Zip = c.String(maxLength: 11, unicode: false),
                        PaymentTermId = c.Int(),
                        Notes = c.String(maxLength: 2048),
                        Directions = c.String(maxLength: 2048),
                        BusinessHours = c.String(maxLength: 2048),
                        OwnerCompany = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.City", t => t.CityId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.DropOffLocationType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DropOffLocationId = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LocationType", t => t.TypeId)
                .ForeignKey("dbo.DropOffLocation", t => t.DropOffLocationId)
                .Index(t => t.DropOffLocationId)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.LocationType",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        LevelName = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Country",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(maxLength: 50, unicode: false),
                        Abbreviation = c.String(maxLength: 5, fixedLength: true, unicode: false),
                        RegionId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50, unicode: false),
                        Abbreviation = c.String(maxLength: 5, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocumentLib",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 200, unicode: false),
                        Tags = c.String(maxLength: 200, unicode: false),
                        UploadDate = c.DateTime(),
                        AttachmentPath = c.String(maxLength: 300, unicode: false),
                        Description = c.String(maxLength: 300, unicode: false),
                        StateId = c.Int(),
                        PlanYear = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 300, unicode: false),
                        Description = c.String(maxLength: 2000, unicode: false),
                        DateBegin = c.DateTime(storeType: "date"),
                        DateEnd = c.DateTime(storeType: "date"),
                        ActionDate = c.DateTime(storeType: "date"),
                        EventTypeId = c.Int(),
                        RecurrenceTypeId = c.Int(),
                        SingleRecurrenceValue = c.DateTime(storeType: "date"),
                        RecurrenceIntervalForDay = c.Int(),
                        RecurrenceIntervalForMonth = c.Int(),
                        RecurrenceIntervalQuaterly = c.Int(),
                        RecurrenceIntervalYearly = c.DateTime(storeType: "date"),
                        IsTemplate = c.Boolean(),
                        TemplateName = c.String(maxLength: 300, unicode: false),
                        StateId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EventType", t => t.EventTypeId)
                .ForeignKey("dbo.RecurrenceType", t => t.RecurrenceTypeId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.EventTypeId)
                .Index(t => t.RecurrenceTypeId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.EventType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(maxLength: 150, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RecurrenceType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReminderGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventId = c.Int(),
                        RowId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Event", t => t.EventId)
                .Index(t => t.EventId);
            
            CreateTable(
                "dbo.ReminderGroupAffiliate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReminderGroupId = c.Int(),
                        AffiliateId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .ForeignKey("dbo.ReminderGroup", t => t.ReminderGroupId)
                .Index(t => t.ReminderGroupId)
                .Index(t => t.AffiliateId);
            
            CreateTable(
                "dbo.Reminder",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReminderGroupId = c.Int(),
                        Title = c.String(maxLength: 200, unicode: false),
                        Number = c.String(maxLength: 50, unicode: false),
                        PeriodId = c.Int(),
                        StatusId = c.Int(),
                        ShowOnCalendar = c.Boolean(),
                        SendDate = c.DateTime(storeType: "date"),
                        Emails = c.String(maxLength: 1500),
                        SentReminderDate = c.DateTime(storeType: "date"),
                        OccurranceDate = c.DateTime(storeType: "date"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Period", t => t.PeriodId)
                .ForeignKey("dbo.ReminderStatus", t => t.StatusId)
                .ForeignKey("dbo.ReminderGroup", t => t.ReminderGroupId, cascadeDelete: true)
                .Index(t => t.ReminderGroupId)
                .Index(t => t.PeriodId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.Period",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Period = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReminderAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReminderId = c.Int(),
                        AttachmentPath = c.String(maxLength: 300),
                        AttachmentName = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Reminder", t => t.ReminderId, cascadeDelete: true)
                .Index(t => t.ReminderId);
            
            CreateTable(
                "dbo.ReminderStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(maxLength: 150, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PlanYear",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        StartDate = c.DateTime(storeType: "date"),
                        EndDate = c.DateTime(storeType: "date"),
                        StateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.StatePolicy",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        PolicyId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.Policy", t => t.PolicyId)
                .Index(t => t.StateId)
                .Index(t => t.PolicyId);
            
            CreateTable(
                "dbo.StateProductType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        ProductTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.ProductType", t => t.ProductTypeId)
                .Index(t => t.StateId)
                .Index(t => t.ProductTypeId);
            
            CreateTable(
                "dbo.StateSpecificRates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        Urban = c.Double(nullable: false),
                        Rural = c.Double(nullable: false),
                        Metro = c.Double(nullable: false),
                        NonMetro = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.StateSupportedDevices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        ProductTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .ForeignKey("dbo.ProductType", t => t.ProductTypeId)
                .Index(t => t.StateId)
                .Index(t => t.ProductTypeId);
            
            CreateTable(
                "dbo.MailBackDevices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MailBackId = c.Int(nullable: false),
                        ManufacturerId = c.Int(nullable: false),
                        ProductTypeId = c.Int(nullable: false),
                        Model = c.String(nullable: false, maxLength: 100, unicode: false),
                        Size = c.String(nullable: false, maxLength: 50, unicode: false),
                        Quantity = c.Int(nullable: false),
                        NeedBoxes = c.Boolean(nullable: false),
                        BoxSize = c.String(maxLength: 50, unicode: false),
                        BoxQuantity = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MailBack", t => t.MailBackId)
                .ForeignKey("dbo.ProductType", t => t.ProductTypeId)
                .ForeignKey("dbo.Affiliate", t => t.ManufacturerId)
                .Index(t => t.MailBackId)
                .Index(t => t.ManufacturerId)
                .Index(t => t.ProductTypeId);
            
            CreateTable(
                "dbo.MailBack",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MailBackAddress = c.String(nullable: false, maxLength: 400, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SiteImage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50, unicode: false),
                        Path = c.String(maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocumentType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SiteImageID = c.Int(),
                        FileExtension = c.String(maxLength: 10, unicode: false),
                        DocumentTypeName = c.String(maxLength: 100, unicode: false),
                        ContentType = c.String(maxLength: 100, unicode: false),
                        IsUploadable = c.Boolean(nullable: false),
                        IsImage = c.Boolean(nullable: false),
                        EscapeBeforeDisplay = c.Boolean(nullable: false),
                        DocumentType2_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentType", t => t.DocumentType2_Id)
                .ForeignKey("dbo.SiteImage", t => t.SiteImageID)
                .Index(t => t.SiteImageID)
                .Index(t => t.DocumentType2_Id);
            
            CreateTable(
                "dbo.Document",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentTypeId = c.Int(),
                        AffiliateContactId = c.Int(),
                        ServerFileName = c.String(maxLength: 2048, unicode: false),
                        UploadFileName = c.String(maxLength: 2048, unicode: false),
                        UploadDate = c.DateTime(storeType: "smalldatetime"),
                        DeleteDate = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AffiliateContact", t => t.AffiliateContactId)
                .ForeignKey("dbo.DocumentType", t => t.DocumentTypeId)
                .Index(t => t.DocumentTypeId)
                .Index(t => t.AffiliateContactId);
            
            CreateTable(
                "dbo.DocumentRelation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentID = c.Int(nullable: false),
                        ForeignID = c.Int(nullable: false),
                        ForeignTableID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ForeignTable", t => t.ForeignTableID)
                .ForeignKey("dbo.Document", t => t.DocumentID)
                .Index(t => t.DocumentID)
                .Index(t => t.ForeignTableID);
            
            CreateTable(
                "dbo.ForeignTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ForeignTableName = c.String(nullable: false, maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AffiliateService",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateId = c.Int(nullable: false),
                        ServiceTypeId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ServiceType", t => t.ServiceTypeId)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .Index(t => t.AffiliateId)
                .Index(t => t.ServiceTypeId);
            
            CreateTable(
                "dbo.ServiceTypeGroupRelation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ServiceTypeGroupId = c.Int(),
                        ServiceTypeId = c.Int(),
                        Active = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ServiceType", t => t.ServiceTypeId)
                .ForeignKey("dbo.ServiceTypeGroup", t => t.ServiceTypeGroupId)
                .Index(t => t.ServiceTypeGroupId)
                .Index(t => t.ServiceTypeId);
            
            CreateTable(
                "dbo.ServiceTypeGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100, unicode: false),
                        Description = c.String(maxLength: 1000, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AffiliateMasterContact",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MasterAffiliateContactId = c.Int(nullable: false),
                        AssociateContactId = c.Int(nullable: false),
                        DefaultUser = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AffiliateContact", t => t.MasterAffiliateContactId)
                .ForeignKey("dbo.AffiliateContact", t => t.AssociateContactId)
                .Index(t => t.MasterAffiliateContactId)
                .Index(t => t.AssociateContactId);
            
            CreateTable(
                "dbo.AuditEvent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AuditActionId = c.Int(nullable: false),
                        Entity = c.String(maxLength: 50),
                        EntityId = c.Int(),
                        AffiliateContactId = c.Int(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        IpAddress = c.String(maxLength: 50),
                        Note = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AuditAction", t => t.AuditActionId)
                .ForeignKey("dbo.AffiliateContact", t => t.AffiliateContactId)
                .Index(t => t.AuditActionId)
                .Index(t => t.AffiliateContactId);
            
            CreateTable(
                "dbo.AuditAction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateTypeId = c.Int(),
                        RoleName = c.String(maxLength: 50, unicode: false),
                        CreateByAffiliateId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AffiliateType", t => t.AffiliateTypeId)
                .ForeignKey("dbo.Affiliate", t => t.CreateByAffiliateId)
                .Index(t => t.AffiliateTypeId)
                .Index(t => t.CreateByAffiliateId);
            
            CreateTable(
                "dbo.AffiliateRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AffiliateId = c.Int(),
                        RoleId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.AffiliateId)
                .ForeignKey("dbo.Role", t => t.RoleId)
                .Index(t => t.AffiliateId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AffiliateType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(maxLength: 100, unicode: false),
                        Description = c.String(maxLength: 200, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(),
                        AreaId = c.Int(),
                        Read = c.Boolean(),
                        Add = c.Boolean(),
                        Update = c.Boolean(),
                        Delete = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Area", t => t.AreaId)
                .ForeignKey("dbo.Role", t => t.RoleId)
                .Index(t => t.RoleId)
                .Index(t => t.AreaId);
            
            CreateTable(
                "dbo.Area",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ParentId = c.Int(),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Caption = c.String(maxLength: 200, unicode: false),
                        URL = c.String(maxLength: 200, unicode: false),
                        IsMenu = c.Boolean(),
                        IsArea = c.Boolean(),
                        Order = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SecurityQuestion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(maxLength: 200, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RecyclerProductWeight",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecyclerId = c.Int(nullable: false),
                        Year = c.String(nullable: false, maxLength: 4),
                        Month = c.String(nullable: false, maxLength: 2),
                        TotalWeightReceived = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OnHand = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CRTGlass = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CircuitBoards = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Batteries = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OtherMaterials = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Affiliate", t => t.RecyclerId)
                .Index(t => t.RecyclerId);
            
            CreateTable(
                "dbo.ConsumerProductTypeManufacturer",
                c => new
                    {
                        ManufacturerID = c.Int(nullable: false, identity: true),
                        AffiliateID = c.Int(nullable: false),
                        ManufacturerName = c.String(nullable: false, maxLength: 75, unicode: false),
                        Verified = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ManufacturerID);
            
            CreateTable(
                "dbo.DBAudit",
                c => new
                    {
                        AuditId = c.String(nullable: false, maxLength: 200, unicode: false),
                        RevisionStamp = c.DateTime(),
                        TableName = c.String(maxLength: 50),
                        UserName = c.String(maxLength: 50),
                        Actions = c.String(maxLength: 1),
                        OldData = c.String(storeType: "xml"),
                        NewData = c.String(storeType: "xml"),
                        ChangedColumns = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.AuditId);
            
            CreateTable(
                "dbo.DropOffDevice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeviceName = c.String(maxLength: 50),
                        Description = c.String(maxLength: 50),
                        Note = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DropOffLocationDevice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DropOffLocationId = c.Int(nullable: false),
                        DeviceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DropOffDevice", t => t.DeviceId)
                .Index(t => t.DeviceId);
            
            CreateTable(
                "dbo.DropOffLocationFromAffiliate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200, unicode: false),
                        Address1 = c.String(maxLength: 200, unicode: false),
                        Address2 = c.String(maxLength: 200, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        Fax = c.String(maxLength: 50, unicode: false),
                        Email = c.String(maxLength: 50, unicode: false),
                        WebSite = c.String(maxLength: 50, unicode: false),
                        Longitude = c.Decimal(precision: 9, scale: 6),
                        Latitude = c.Decimal(precision: 9, scale: 6),
                        Active = c.Boolean(),
                        StateId = c.Int(),
                        CityId = c.Int(),
                        Zip = c.String(maxLength: 11, unicode: false),
                        AffiliateTypeId = c.Int(),
                        PaymentTermId = c.Int(),
                        Notes = c.String(maxLength: 2048),
                        Directions = c.String(maxLength: 2048),
                        BusinessHours = c.String(maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InvoiceRequiringFix",
                c => new
                    {
                        InvoiceId = c.Int(nullable: false),
                        OldQuantity = c.Decimal(storeType: "money"),
                        OldTotal = c.Decimal(storeType: "money"),
                        NewQuantity = c.Decimal(precision: 38, scale: 2),
                        NewTotal = c.Decimal(precision: 38, scale: 6),
                        PaidAmount = c.Decimal(storeType: "money"),
                    })
                .PrimaryKey(t => t.InvoiceId);
            
            CreateTable(
                "dbo.NewsFeed",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        News = c.String(maxLength: 500),
                        FeedLink = c.String(maxLength: 200),
                        Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentTerm",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Days = c.Int(nullable: false),
                        Description = c.String(maxLength: 300, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateGuideline_Contact",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Phone = c.String(maxLength: 100),
                        EMail = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateGuideline_DeadlineDate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        Date = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateGuideline_Link",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        Link = c.String(nullable: false, maxLength: 500),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateGuideline",
                c => new
                    {
                        StateId = c.Int(nullable: false),
                        GoverningBody = c.String(maxLength: 2000),
                        Website = c.String(maxLength: 500),
                        CoveredDevices = c.String(maxLength: 2000),
                        OEMObligation = c.String(maxLength: 2000),
                        Updates = c.String(maxLength: 2000),
                        LawState = c.Int(),
                        UpdateDate = c.DateTime(),
                        RegulationType = c.String(maxLength: 2000),
                        RegulationChange = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.StateId);
            
            CreateTable(
                "dbo.V_CertificateReport",
                c => new
                    {
                        ToAffiliateID = c.Int(nullable: false),
                        RegionId = c.Int(),
                        Product = c.String(maxLength: 75, unicode: false),
                        RecQuantity = c.Int(),
                        InvoiceID = c.Int(),
                        ToAffiliateName = c.String(maxLength: 200, unicode: false),
                        FromAffiliate = c.Int(),
                        AffiliateName = c.String(maxLength: 200, unicode: false),
                        RecyclerAddress1 = c.String(maxLength: 200, unicode: false),
                        RecyclerAddress2 = c.String(maxLength: 66, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        Fax = c.String(maxLength: 50, unicode: false),
                        Email = c.String(maxLength: 50, unicode: false),
                        ProcessingDate = c.String(maxLength: 10, unicode: false),
                        ProcessingDate1 = c.DateTime(),
                        ShipmentNo = c.String(maxLength: 50, unicode: false),
                        Region = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.ToAffiliateID);
            
            CreateTable(
                "dbo.V_InvoiceOverView",
                c => new
                    {
                        InvoiceID = c.Int(nullable: false),
                        RegionID = c.Int(nullable: false),
                        Receivable = c.Boolean(),
                        InvoiceDate = c.DateTime(storeType: "smalldatetime"),
                        InvoicePeriod = c.Int(),
                        Cost = c.Decimal(storeType: "money"),
                        Volume = c.Decimal(storeType: "money"),
                        AdjustedVolume = c.Decimal(precision: 38, scale: 7),
                        RegionName = c.String(maxLength: 50, unicode: false),
                        AffiliateID = c.Int(),
                        AffiliateName = c.String(maxLength: 200, unicode: false),
                        PaymentTermID = c.Int(),
                        TargetQuantity = c.Decimal(precision: 38, scale: 0),
                        TargetPercentOfYear = c.Decimal(precision: 38, scale: 6),
                        QuantityTypeID = c.Int(),
                        ServiceTypeID = c.Int(),
                        InvoicePeriodFrom = c.DateTime(storeType: "smalldatetime"),
                        InvoicePeriodTo = c.DateTime(storeType: "smalldatetime"),
                        PeriodBegin = c.DateTime(),
                        PeriodEnd = c.DateTime(storeType: "date"),
                    })
                .PrimaryKey(t => new { t.InvoiceID, t.RegionID });
            
            CreateTable(
                "dbo.v_InvoicesRequiringFix",
                c => new
                    {
                        InvoiceId = c.Int(nullable: false),
                        OldQuantity = c.Decimal(storeType: "money"),
                        OldTotal = c.Decimal(storeType: "money"),
                        NewQuantity = c.Decimal(precision: 38, scale: 2),
                        NewTotal = c.Decimal(precision: 38, scale: 6),
                        PaidAmount = c.Decimal(storeType: "money"),
                    })
                .PrimaryKey(t => t.InvoiceId);
            
            CreateTable(
                "dbo.V_ProcessingReport",
                c => new
                    {
                        ProcessingDataID = c.Int(nullable: false),
                        InvoiceItemID = c.Int(nullable: false),
                        Qauntity = c.Decimal(nullable: false, storeType: "money"),
                        Tare = c.Int(nullable: false),
                        AffiliateName = c.String(maxLength: 200, unicode: false),
                        RecyclerAddress1 = c.String(maxLength: 200, unicode: false),
                        RecyclerAddress2 = c.String(maxLength: 66, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        Fax = c.String(maxLength: 50, unicode: false),
                        Email = c.String(maxLength: 50, unicode: false),
                        ProcessingDate = c.DateTime(),
                        InvoiceDate = c.DateTime(storeType: "smalldatetime"),
                        PlanYear = c.Int(),
                        StateName = c.String(maxLength: 50, unicode: false),
                        Comments = c.String(maxLength: 1000, unicode: false),
                        InvoiceID = c.Int(),
                        ShipmentNo = c.String(maxLength: 50, unicode: false),
                        VendorCompanyName = c.String(maxLength: 250, unicode: false),
                        VendorAddress = c.String(maxLength: 200, unicode: false),
                        VendorCity = c.String(maxLength: 50, unicode: false),
                        VendorStateProvinceName = c.String(maxLength: 50, unicode: false),
                        VendorZip = c.String(maxLength: 10, unicode: false),
                        VendorPhone = c.String(maxLength: 20, unicode: false),
                        PickupAddress = c.String(maxLength: 200, unicode: false),
                        ShipStateProvinceName = c.String(maxLength: 50, unicode: false),
                        PickupCity = c.String(maxLength: 50, unicode: false),
                        PickUpCompanyName = c.String(maxLength: 250, unicode: false),
                        PickupZip = c.String(maxLength: 10, fixedLength: true),
                        PickupPhone = c.String(maxLength: 20, unicode: false),
                        PickupFax = c.String(maxLength: 20, unicode: false),
                        PickupEmail = c.String(maxLength: 100, unicode: false),
                        Client = c.String(maxLength: 200, unicode: false),
                        RecievedBy = c.String(maxLength: 200, unicode: false),
                        AffiliateID = c.Int(),
                        ShipmentID = c.Int(),
                        ProductTypeName = c.String(maxLength: 75, unicode: false),
                        ServiceTypeName = c.String(maxLength: 100, unicode: false),
                        PalletNumber = c.Int(),
                        Description = c.String(maxLength: 100),
                        IsMetro = c.Boolean(),
                        CollectionMethod = c.String(maxLength: 80, unicode: false),
                        Total = c.Decimal(storeType: "money"),
                        OriginalQuantity = c.Decimal(precision: 18, scale: 0),
                        WeightCategory = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => new { t.ProcessingDataID, t.InvoiceItemID, t.Qauntity, t.Tare });
            
            CreateTable(
                "dbo.V_ReminderEmail",
                c => new
                    {
                        ReminderGroupId = c.Int(nullable: false),
                        AffiliateId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 1, unicode: false),
                        Email = c.String(nullable: false, maxLength: 1, unicode: false),
                        EventId = c.Int(nullable: false),
                        ReminderId = c.Int(nullable: false),
                        EventTitle = c.String(maxLength: 300, unicode: false),
                        EventDescription = c.String(maxLength: 2000, unicode: false),
                        Number = c.String(maxLength: 50, unicode: false),
                        Period = c.String(maxLength: 50, unicode: false),
                        PeriodId = c.Int(),
                        Title = c.String(maxLength: 200, unicode: false),
                        SendDate = c.DateTime(),
                        Emails = c.String(maxLength: 1500),
                        SentReminderDate = c.DateTime(storeType: "date"),
                        DateBegin = c.DateTime(storeType: "date"),
                        DateEnd = c.DateTime(storeType: "date"),
                        ActionDate = c.DateTime(storeType: "date"),
                        RecurrenceTypeId = c.Int(),
                        SingleRecurrenceValue = c.DateTime(storeType: "date"),
                        RecurrenceIntervalForDay = c.Int(),
                        RecurrenceIntervalForMonth = c.Int(),
                        RecurrenceIntervalQuaterly = c.Int(),
                        RecurrenceIntervalYearly = c.DateTime(storeType: "date"),
                        AttachmentName = c.String(maxLength: 150),
                        eType = c.String(maxLength: 150, unicode: false),
                        OccurranceDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ReminderGroupId, t.AffiliateId, t.Name, t.Email, t.EventId, t.ReminderId });
            
            CreateTable(
                "dbo.V_ReoncileInvoiceDetail",
                c => new
                    {
                        Item = c.Int(nullable: false),
                        RegionName = c.String(maxLength: 150, unicode: false),
                        AffiliateDetailValue = c.String(maxLength: 200, unicode: false),
                        FromInvoice = c.Int(),
                        ToInvoice = c.Int(),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                        ProductTypeName = c.String(maxLength: 75, unicode: false),
                        ReconcileFromInvoiceItemID = c.Int(),
                    })
                .PrimaryKey(t => t.Item);
            
            CreateTable(
                "dbo.V_ReportEntitytargets",
                c => new
                    {
                        InvoiceID = c.Int(nullable: false),
                        StateId = c.Int(nullable: false),
                        IndividualTargetStatus = c.String(nullable: false, maxLength: 22, unicode: false),
                        StateAffiliateTargetStatus = c.String(nullable: false, maxLength: 22, unicode: false),
                        StateTargetStatus = c.String(nullable: false, maxLength: 22, unicode: false),
                        AffiliateStateTargetStatus = c.String(nullable: false, maxLength: 22, unicode: false),
                        AffiliateTargetStatus = c.String(nullable: false, maxLength: 22, unicode: false),
                        State = c.String(maxLength: 50, unicode: false),
                        EntityName = c.String(maxLength: 200, unicode: false),
                        InvoiceAmount = c.Decimal(precision: 38, scale: 6),
                        PaymentAmount = c.Decimal(storeType: "money"),
                        RemainingAmount = c.Decimal(precision: 38, scale: 6),
                        InvoiceDate = c.String(maxLength: 10, unicode: false),
                        InvoiceDate1 = c.DateTime(storeType: "smalldatetime"),
                        InvoiceMonthName = c.String(maxLength: 30),
                        InvoiceMonth = c.Int(),
                        InvoiceYear = c.Int(),
                        PlanYear = c.Int(),
                        InvoiceYearHalf = c.String(maxLength: 13, unicode: false),
                        InvoiceQaurter = c.String(maxLength: 9, unicode: false),
                        ProcessingDate = c.String(maxLength: 10, unicode: false),
                        InvoiceDueDate = c.String(maxLength: 10, unicode: false),
                        PaymentDate = c.String(maxLength: 10, unicode: false),
                        InvoicePeriodFrom = c.String(maxLength: 10, unicode: false),
                        InvoicePeriodTo = c.String(maxLength: 10, unicode: false),
                        InvoiceItemCount = c.Int(),
                        PaymentItemCount = c.Int(),
                        PaidStatus = c.String(maxLength: 12, unicode: false),
                        Receivable = c.Boolean(),
                        EntityId = c.Int(),
                        IsPaid = c.Boolean(),
                        Aging = c.Int(),
                        EntityType = c.Int(),
                        InvoiceWeight = c.Decimal(precision: 38, scale: 2),
                        RecAssignedWeight = c.Decimal(precision: 38, scale: 2),
                        RecUnAssignedWeight = c.Decimal(precision: 38, scale: 2),
                        RecWeight = c.Decimal(precision: 38, scale: 2),
                        InvRemainingWeight = c.Decimal(precision: 38, scale: 2),
                        TargetWeight = c.Decimal(precision: 18, scale: 0),
                        InvoiceStatus = c.String(maxLength: 17, unicode: false),
                        QuantityTypeID = c.Int(),
                        ServiceTypeID = c.Int(),
                        TargetStartDate = c.String(maxLength: 10, unicode: false),
                        TargetEndDate = c.String(maxLength: 10, unicode: false),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        TargetId = c.Int(),
                        InvoiceCountPerTarget = c.Int(),
                        InvAvgWeight = c.Decimal(precision: 29, scale: 11),
                        InvRemWeight = c.Decimal(precision: 38, scale: 2),
                        TargetRange = c.String(maxLength: 24, unicode: false),
                        IndTargetWeight = c.Decimal(precision: 38, scale: 0),
                        IndRecWeight = c.Decimal(precision: 38, scale: 2),
                        RemIndTarget = c.Decimal(precision: 38, scale: 2),
                        StateAffilaiteTarget = c.Decimal(precision: 38, scale: 0),
                        StateAffilaiteRec = c.Decimal(precision: 38, scale: 2),
                        StateAffilaiteRem = c.Decimal(precision: 38, scale: 0),
                        StateTarget = c.Decimal(precision: 38, scale: 0),
                        StateRec = c.Decimal(precision: 38, scale: 2),
                        StateRem = c.Decimal(precision: 38, scale: 0),
                        AffilaiteStateTarget = c.Decimal(precision: 38, scale: 0),
                        AffilaiteStateRec = c.Decimal(precision: 38, scale: 2),
                        AffilaiteStateRem = c.Decimal(precision: 38, scale: 0),
                        AffilaiteTarget = c.Decimal(precision: 38, scale: 0),
                        AffilaiteRec = c.Decimal(precision: 38, scale: 2),
                        AffilaiteRem = c.Decimal(precision: 38, scale: 0),
                    })
                .PrimaryKey(t => new { t.InvoiceID, t.StateId, t.IndividualTargetStatus, t.StateAffiliateTargetStatus, t.StateTargetStatus, t.AffiliateStateTargetStatus, t.AffiliateTargetStatus });
            
            CreateTable(
                "dbo.V_ReportInvoice",
                c => new
                    {
                        InvoiceID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Rate = c.Decimal(nullable: false, storeType: "money"),
                        AffiliateName = c.String(maxLength: 200, unicode: false),
                        RecyclerAddress1 = c.String(maxLength: 200, unicode: false),
                        RecyclerAddress2 = c.String(maxLength: 66, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        VendorCompanyName = c.String(maxLength: 250, unicode: false),
                        StateProvinceName = c.String(maxLength: 50, unicode: false),
                        VendorAddress = c.String(maxLength: 200, unicode: false),
                        VendorAddress1 = c.String(maxLength: 65, unicode: false),
                        InvStateName = c.String(maxLength: 50, unicode: false),
                        AffiliateID = c.Int(),
                        ProcessingDate = c.String(maxLength: 10, unicode: false),
                        InvoiceDate = c.DateTime(storeType: "smalldatetime"),
                        InvoiceDueDate = c.String(maxLength: 10, unicode: false),
                        ServiceDate = c.String(maxLength: 10, unicode: false),
                        InvoicePeriodFrom = c.String(maxLength: 10, unicode: false),
                        InvoicePeriodTo = c.String(maxLength: 10, unicode: false),
                        ShipmentID = c.String(maxLength: 50, unicode: false),
                        ProductTypeName = c.String(maxLength: 75, unicode: false),
                        ServiceTypeName = c.String(maxLength: 100, unicode: false),
                        Total = c.Decimal(storeType: "money"),
                        PickupAddress = c.String(maxLength: 200, unicode: false),
                        ShipStateProvinceName = c.String(maxLength: 50, unicode: false),
                        PickupCity = c.String(maxLength: 50, unicode: false),
                        PickupZip = c.String(maxLength: 10, fixedLength: true),
                        PickupPhone = c.String(maxLength: 20, unicode: false),
                        PickupFax = c.String(maxLength: 20, unicode: false),
                        PickupEmail = c.String(maxLength: 100, unicode: false),
                        CollectionMethod = c.String(maxLength: 80, unicode: false),
                        Notes = c.String(maxLength: 2048, unicode: false),
                    })
                .PrimaryKey(t => new { t.InvoiceID, t.Quantity, t.Rate });
            
            CreateTable(
                "dbo.V_ReportInvoiceForOEM",
                c => new
                    {
                        InvoiceID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Rate = c.Decimal(nullable: false, storeType: "money"),
                        AffiliateName = c.String(maxLength: 200, unicode: false),
                        RecyclerAddress1 = c.String(maxLength: 200, unicode: false),
                        RecyclerAddress2 = c.String(maxLength: 66, unicode: false),
                        Phone = c.String(maxLength: 20, unicode: false),
                        VendorCompanyName = c.String(maxLength: 250, unicode: false),
                        StateProvinceName = c.String(maxLength: 50, unicode: false),
                        VendorAddress = c.String(maxLength: 200, unicode: false),
                        VendorAddress1 = c.String(maxLength: 65, unicode: false),
                        InvStateName = c.String(maxLength: 50, unicode: false),
                        AffiliateId = c.Int(),
                        ProcessingDate = c.String(maxLength: 10, unicode: false),
                        InvoiceDate = c.DateTime(storeType: "smalldatetime"),
                        InvoiceDueDate = c.String(maxLength: 10, unicode: false),
                        ServiceDate = c.String(maxLength: 10, unicode: false),
                        InvoicePeriodFrom = c.String(maxLength: 10, unicode: false),
                        InvoicePeriodTo = c.String(maxLength: 10, unicode: false),
                        ShipmentID = c.String(maxLength: 50, unicode: false),
                        ProductTypeName = c.String(maxLength: 75, unicode: false),
                        ServiceTypeName = c.String(maxLength: 100, unicode: false),
                        Total = c.Decimal(precision: 38, scale: 6),
                        PickupAddress = c.String(maxLength: 200, unicode: false),
                        ShipStateProvinceName = c.String(maxLength: 50, unicode: false),
                        PickupCity = c.String(maxLength: 50, unicode: false),
                        PickupZip = c.String(maxLength: 10, fixedLength: true),
                        PickupPhone = c.String(maxLength: 20, unicode: false),
                        PickupFax = c.String(maxLength: 20, unicode: false),
                        PickupEmail = c.String(maxLength: 100, unicode: false),
                        CollectionMethod = c.String(maxLength: 80, unicode: false),
                        Notes = c.String(maxLength: 2048, unicode: false),
                    })
                .PrimaryKey(t => new { t.InvoiceID, t.Quantity, t.Rate });
            
            CreateTable(
                "dbo.V_ReportReconcileDetail",
                c => new
                    {
                        FromStateID = c.Int(nullable: false),
                        FromEntityId = c.Int(nullable: false),
                        RecAssignedWeight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FromStateName = c.String(maxLength: 50, unicode: false),
                        FromEntityName = c.String(maxLength: 200, unicode: false),
                        FromPlanYear = c.Int(),
                        FromInvoiceId = c.Int(),
                        ToStateId = c.Int(),
                        ToStateName = c.String(maxLength: 50, unicode: false),
                        ToEntityId = c.Int(),
                        ToEntityName = c.String(maxLength: 200, unicode: false),
                        ToInvoiceId = c.Int(),
                        ReconcileFromInvoiceItemID = c.Int(),
                        ReconcileToInvoiceItemID = c.Int(),
                        FromInvoiceWeight = c.Decimal(storeType: "money"),
                        ReconcileItemCount = c.Int(),
                    })
                .PrimaryKey(t => new { t.FromStateID, t.FromEntityId, t.RecAssignedWeight });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DropOffLocationDevice", "DeviceId", "dbo.DropOffDevice");
            DropForeignKey("dbo.Role", "CreateByAffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.RecyclerProductWeight", "RecyclerId", "dbo.Affiliate");
            DropForeignKey("dbo.MailBackDevices", "ManufacturerId", "dbo.Affiliate");
            DropForeignKey("dbo.ClientAddress", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateState", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateService", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateContact", "SecurityQuestionId", "dbo.SecurityQuestion");
            DropForeignKey("dbo.Permissions", "RoleId", "dbo.Role");
            DropForeignKey("dbo.Permissions", "AreaId", "dbo.Area");
            DropForeignKey("dbo.Role", "AffiliateTypeId", "dbo.AffiliateType");
            DropForeignKey("dbo.Affiliate", "AffiliateTypeId", "dbo.AffiliateType");
            DropForeignKey("dbo.AffiliateRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.AffiliateRole", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateContact", "RoleId", "dbo.Role");
            DropForeignKey("dbo.AuditEvent", "AffiliateContactId", "dbo.AffiliateContact");
            DropForeignKey("dbo.AuditEvent", "AuditActionId", "dbo.AuditAction");
            DropForeignKey("dbo.AffiliateMasterContact", "AssociateContactId", "dbo.AffiliateContact");
            DropForeignKey("dbo.AffiliateMasterContact", "MasterAffiliateContactId", "dbo.AffiliateContact");
            DropForeignKey("dbo.AffiliateContactService", "AffiliateContactId", "dbo.AffiliateContact");
            DropForeignKey("dbo.ServiceTypeGroupRelation", "ServiceTypeGroupId", "dbo.ServiceTypeGroup");
            DropForeignKey("dbo.ServiceTypeGroupRelation", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.AffiliateService", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.AffiliateContractRate", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.StateSupportedDevices", "ProductTypeId", "dbo.ProductType");
            DropForeignKey("dbo.StateProductType", "ProductTypeId", "dbo.ProductType");
            DropForeignKey("dbo.ProductType", "SiteImageID", "dbo.SiteImage");
            DropForeignKey("dbo.DocumentType", "SiteImageID", "dbo.SiteImage");
            DropForeignKey("dbo.DocumentType", "DocumentType2_Id", "dbo.DocumentType");
            DropForeignKey("dbo.Document", "DocumentTypeId", "dbo.DocumentType");
            DropForeignKey("dbo.DocumentRelation", "DocumentID", "dbo.Document");
            DropForeignKey("dbo.DocumentRelation", "ForeignTableID", "dbo.ForeignTable");
            DropForeignKey("dbo.Document", "AffiliateContactId", "dbo.AffiliateContact");
            DropForeignKey("dbo.MailBackDevices", "ProductTypeId", "dbo.ProductType");
            DropForeignKey("dbo.MailBackDevices", "MailBackId", "dbo.MailBack");
            DropForeignKey("dbo.AffiliateTargetProduct", "ProductTypeId", "dbo.ProductType");
            DropForeignKey("dbo.AffiliateTarget", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.StatePolicy", "PolicyId", "dbo.Policy");
            DropForeignKey("dbo.StateWeightCategory", "StateId", "dbo.State");
            DropForeignKey("dbo.StateSupportedDevices", "StateId", "dbo.State");
            DropForeignKey("dbo.StateSpecificRates", "StateId", "dbo.State");
            DropForeignKey("dbo.StateProductType", "StateId", "dbo.State");
            DropForeignKey("dbo.StatePolicy", "StateId", "dbo.State");
            DropForeignKey("dbo.PlanYear", "StateId", "dbo.State");
            DropForeignKey("dbo.ManufacturerPolicy", "StateId", "dbo.State");
            DropForeignKey("dbo.Event", "StateId", "dbo.State");
            DropForeignKey("dbo.Reminder", "ReminderGroupId", "dbo.ReminderGroup");
            DropForeignKey("dbo.Reminder", "StatusId", "dbo.ReminderStatus");
            DropForeignKey("dbo.ReminderAttachment", "ReminderId", "dbo.Reminder");
            DropForeignKey("dbo.Reminder", "PeriodId", "dbo.Period");
            DropForeignKey("dbo.ReminderGroupAffiliate", "ReminderGroupId", "dbo.ReminderGroup");
            DropForeignKey("dbo.ReminderGroupAffiliate", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.ReminderGroup", "EventId", "dbo.Event");
            DropForeignKey("dbo.Event", "RecurrenceTypeId", "dbo.RecurrenceType");
            DropForeignKey("dbo.Event", "EventTypeId", "dbo.EventType");
            DropForeignKey("dbo.DocumentLib", "StateId", "dbo.State");
            DropForeignKey("dbo.State", "CountryId", "dbo.Country");
            DropForeignKey("dbo.Country", "RegionId", "dbo.Region");
            DropForeignKey("dbo.CollectionEvent", "StateId", "dbo.State");
            DropForeignKey("dbo.City", "StateId", "dbo.State");
            DropForeignKey("dbo.ProcessingData", "PickupCityId", "dbo.City");
            DropForeignKey("dbo.ProcessingData", "VendorCityId", "dbo.City");
            DropForeignKey("dbo.DropOffLocation", "StateId", "dbo.State");
            DropForeignKey("dbo.DropOffLocationType", "DropOffLocationId", "dbo.DropOffLocation");
            DropForeignKey("dbo.DropOffLocationType", "TypeId", "dbo.LocationType");
            DropForeignKey("dbo.DropOffLocation", "CityId", "dbo.City");
            DropForeignKey("dbo.DropOff_Location", "StateId", "dbo.State");
            DropForeignKey("dbo.DropOff_Location", "CityId", "dbo.City");
            DropForeignKey("dbo.County", "StateId", "dbo.State");
            DropForeignKey("dbo.City", "CountyId", "dbo.County");
            DropForeignKey("dbo.CollectionEvent", "CityId", "dbo.City");
            DropForeignKey("dbo.ClientAddress", "StateId", "dbo.State");
            DropForeignKey("dbo.PickUpAddress", "ClientAddressId", "dbo.ClientAddress");
            DropForeignKey("dbo.PickUpAddress", "StateId", "dbo.State");
            DropForeignKey("dbo.ProcessingData", "StateWeightCategoryId", "dbo.StateWeightCategory");
            DropForeignKey("dbo.Invoice", "StateId", "dbo.State");
            DropForeignKey("dbo.ProcessingData", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Payment", "PaymentTypeID", "dbo.PaymentType");
            DropForeignKey("dbo.PaymentDetail", "PaymentID", "dbo.Payment");
            DropForeignKey("dbo.Payment", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.PaymentDetail", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.InvoiceUnpackId", "ProcesserInvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.InvoiceUnpackId", "OEMInvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.InvoiceReconciliation", "OEMInvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.InvoiceItem", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.QuantityType", "QuantityTypeGroupId", "dbo.QuantityTypeGroup");
            DropForeignKey("dbo.ProductType", "ContainerQuantityTypeId", "dbo.QuantityType");
            DropForeignKey("dbo.ProductType", "WeightQuantityTypeID", "dbo.QuantityType");
            DropForeignKey("dbo.InvoiceItem", "QuantityTypeId", "dbo.QuantityType");
            DropForeignKey("dbo.AffiliateTarget", "QuantityTypeId", "dbo.QuantityType");
            DropForeignKey("dbo.AffiliateContractRate", "QuantityTypeId", "dbo.QuantityType");
            DropForeignKey("dbo.InvoiceItem", "ProductTypeId", "dbo.ProductType");
            DropForeignKey("dbo.InvoiceReconciliation", "ProcesserInvoiceItemId", "dbo.InvoiceItem");
            DropForeignKey("dbo.InvoiceItemReconciliation", "ReconcileToInvoiceItemID", "dbo.InvoiceItem");
            DropForeignKey("dbo.InvoiceItemReconciliation", "ReconcileFromInvoiceItemID", "dbo.InvoiceItem");
            DropForeignKey("dbo.InvoiceItem", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.InvoiceAdjustment", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Invoice", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.ProcessingData", "CollectionMethodId", "dbo.CollectionMethod");
            DropForeignKey("dbo.PickUpAddress", "CollectionMethodId", "dbo.CollectionMethod");
            DropForeignKey("dbo.PickUpAddress", "CityId", "dbo.City");
            DropForeignKey("dbo.ClientAddress", "CityId", "dbo.City");
            DropForeignKey("dbo.Affiliate", "CityId", "dbo.City");
            DropForeignKey("dbo.AffiliateContact", "CityId", "dbo.City");
            DropForeignKey("dbo.AuditReports", "StateId", "dbo.State");
            DropForeignKey("dbo.AffiliateTarget", "StateId", "dbo.State");
            DropForeignKey("dbo.AffiliateState", "StateId", "dbo.State");
            DropForeignKey("dbo.Affiliate", "StateId", "dbo.State");
            DropForeignKey("dbo.AffiliateContractRate", "StateId", "dbo.State");
            DropForeignKey("dbo.AffiliateContact", "StateId", "dbo.State");
            DropForeignKey("dbo.ManufacturerPolicy", "PolicyId", "dbo.Policy");
            DropForeignKey("dbo.ManufacturerPolicy", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateTarget", "PolicyId", "dbo.Policy");
            DropForeignKey("dbo.AffiliateTargetProduct", "AffiliateTargetId", "dbo.AffiliateTarget");
            DropForeignKey("dbo.AffiliateTarget", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateContractRate", "ProductTypeId", "dbo.ProductType");
            DropForeignKey("dbo.AffiliateContractRate", "PriceUnitTypeId", "dbo.PriceUnitType");
            DropForeignKey("dbo.AffiliateContractRate", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateContactService", "ServiceTypeId", "dbo.ServiceType");
            DropForeignKey("dbo.AffiliateContact", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateAccreditation", "AffiliateId", "dbo.Affiliate");
            DropForeignKey("dbo.AffiliateAccreditation", "AccreditationTypeId", "dbo.AccreditationType");
            DropIndex("dbo.DropOffLocationDevice", new[] { "DeviceId" });
            DropIndex("dbo.RecyclerProductWeight", new[] { "RecyclerId" });
            DropIndex("dbo.Permissions", new[] { "AreaId" });
            DropIndex("dbo.Permissions", new[] { "RoleId" });
            DropIndex("dbo.AffiliateRole", new[] { "RoleId" });
            DropIndex("dbo.AffiliateRole", new[] { "AffiliateId" });
            DropIndex("dbo.Role", new[] { "CreateByAffiliateId" });
            DropIndex("dbo.Role", new[] { "AffiliateTypeId" });
            DropIndex("dbo.AuditEvent", new[] { "AffiliateContactId" });
            DropIndex("dbo.AuditEvent", new[] { "AuditActionId" });
            DropIndex("dbo.AffiliateMasterContact", new[] { "AssociateContactId" });
            DropIndex("dbo.AffiliateMasterContact", new[] { "MasterAffiliateContactId" });
            DropIndex("dbo.ServiceTypeGroupRelation", new[] { "ServiceTypeId" });
            DropIndex("dbo.ServiceTypeGroupRelation", new[] { "ServiceTypeGroupId" });
            DropIndex("dbo.AffiliateService", new[] { "ServiceTypeId" });
            DropIndex("dbo.AffiliateService", new[] { "AffiliateId" });
            DropIndex("dbo.DocumentRelation", new[] { "ForeignTableID" });
            DropIndex("dbo.DocumentRelation", new[] { "DocumentID" });
            DropIndex("dbo.Document", new[] { "AffiliateContactId" });
            DropIndex("dbo.Document", new[] { "DocumentTypeId" });
            DropIndex("dbo.DocumentType", new[] { "DocumentType2_Id" });
            DropIndex("dbo.DocumentType", new[] { "SiteImageID" });
            DropIndex("dbo.MailBackDevices", new[] { "ProductTypeId" });
            DropIndex("dbo.MailBackDevices", new[] { "ManufacturerId" });
            DropIndex("dbo.MailBackDevices", new[] { "MailBackId" });
            DropIndex("dbo.StateSupportedDevices", new[] { "ProductTypeId" });
            DropIndex("dbo.StateSupportedDevices", new[] { "StateId" });
            DropIndex("dbo.StateSpecificRates", new[] { "StateId" });
            DropIndex("dbo.StateProductType", new[] { "ProductTypeId" });
            DropIndex("dbo.StateProductType", new[] { "StateId" });
            DropIndex("dbo.StatePolicy", new[] { "PolicyId" });
            DropIndex("dbo.StatePolicy", new[] { "StateId" });
            DropIndex("dbo.PlanYear", new[] { "StateId" });
            DropIndex("dbo.ReminderAttachment", new[] { "ReminderId" });
            DropIndex("dbo.Reminder", new[] { "StatusId" });
            DropIndex("dbo.Reminder", new[] { "PeriodId" });
            DropIndex("dbo.Reminder", new[] { "ReminderGroupId" });
            DropIndex("dbo.ReminderGroupAffiliate", new[] { "AffiliateId" });
            DropIndex("dbo.ReminderGroupAffiliate", new[] { "ReminderGroupId" });
            DropIndex("dbo.ReminderGroup", new[] { "EventId" });
            DropIndex("dbo.Event", new[] { "StateId" });
            DropIndex("dbo.Event", new[] { "RecurrenceTypeId" });
            DropIndex("dbo.Event", new[] { "EventTypeId" });
            DropIndex("dbo.DocumentLib", new[] { "StateId" });
            DropIndex("dbo.Country", new[] { "RegionId" });
            DropIndex("dbo.DropOffLocationType", new[] { "TypeId" });
            DropIndex("dbo.DropOffLocationType", new[] { "DropOffLocationId" });
            DropIndex("dbo.DropOffLocation", new[] { "CityId" });
            DropIndex("dbo.DropOffLocation", new[] { "StateId" });
            DropIndex("dbo.DropOff_Location", new[] { "CityId" });
            DropIndex("dbo.DropOff_Location", new[] { "StateId" });
            DropIndex("dbo.County", new[] { "StateId" });
            DropIndex("dbo.CollectionEvent", new[] { "CityId" });
            DropIndex("dbo.CollectionEvent", new[] { "StateId" });
            DropIndex("dbo.StateWeightCategory", new[] { "StateId" });
            DropIndex("dbo.Payment", new[] { "PaymentTypeID" });
            DropIndex("dbo.Payment", new[] { "AffiliateId" });
            DropIndex("dbo.PaymentDetail", new[] { "InvoiceId" });
            DropIndex("dbo.PaymentDetail", new[] { "PaymentID" });
            DropIndex("dbo.InvoiceUnpackId", new[] { "ProcesserInvoiceId" });
            DropIndex("dbo.InvoiceUnpackId", new[] { "OEMInvoiceId" });
            DropIndex("dbo.QuantityType", new[] { "QuantityTypeGroupId" });
            DropIndex("dbo.InvoiceReconciliation", new[] { "ProcesserInvoiceItemId" });
            DropIndex("dbo.InvoiceReconciliation", new[] { "OEMInvoiceId" });
            DropIndex("dbo.InvoiceItemReconciliation", new[] { "ReconcileToInvoiceItemID" });
            DropIndex("dbo.InvoiceItemReconciliation", new[] { "ReconcileFromInvoiceItemID" });
            DropIndex("dbo.InvoiceItem", new[] { "QuantityTypeId" });
            DropIndex("dbo.InvoiceItem", new[] { "ProductTypeId" });
            DropIndex("dbo.InvoiceItem", new[] { "ServiceTypeId" });
            DropIndex("dbo.InvoiceItem", new[] { "InvoiceId" });
            DropIndex("dbo.InvoiceAdjustment", new[] { "InvoiceId" });
            DropIndex("dbo.Invoice", new[] { "StateId" });
            DropIndex("dbo.Invoice", new[] { "AffiliateId" });
            DropIndex("dbo.ProcessingData", new[] { "StateWeightCategoryId" });
            DropIndex("dbo.ProcessingData", new[] { "CollectionMethodId" });
            DropIndex("dbo.ProcessingData", new[] { "PickupCityId" });
            DropIndex("dbo.ProcessingData", new[] { "VendorCityId" });
            DropIndex("dbo.ProcessingData", new[] { "InvoiceId" });
            DropIndex("dbo.PickUpAddress", new[] { "CollectionMethodId" });
            DropIndex("dbo.PickUpAddress", new[] { "CityId" });
            DropIndex("dbo.PickUpAddress", new[] { "StateId" });
            DropIndex("dbo.PickUpAddress", new[] { "ClientAddressId" });
            DropIndex("dbo.ClientAddress", new[] { "StateId" });
            DropIndex("dbo.ClientAddress", new[] { "CityId" });
            DropIndex("dbo.ClientAddress", new[] { "AffiliateId" });
            DropIndex("dbo.City", new[] { "CountyId" });
            DropIndex("dbo.City", new[] { "StateId" });
            DropIndex("dbo.AuditReports", new[] { "StateId" });
            DropIndex("dbo.AffiliateState", new[] { "StateId" });
            DropIndex("dbo.AffiliateState", new[] { "AffiliateId" });
            DropIndex("dbo.State", new[] { "CountryId" });
            DropIndex("dbo.ManufacturerPolicy", new[] { "PolicyId" });
            DropIndex("dbo.ManufacturerPolicy", new[] { "StateId" });
            DropIndex("dbo.ManufacturerPolicy", new[] { "AffiliateId" });
            DropIndex("dbo.AffiliateTarget", new[] { "AffiliateId" });
            DropIndex("dbo.AffiliateTarget", new[] { "QuantityTypeId" });
            DropIndex("dbo.AffiliateTarget", new[] { "ServiceTypeId" });
            DropIndex("dbo.AffiliateTarget", new[] { "PolicyId" });
            DropIndex("dbo.AffiliateTarget", new[] { "StateId" });
            DropIndex("dbo.AffiliateTargetProduct", new[] { "ProductTypeId" });
            DropIndex("dbo.AffiliateTargetProduct", new[] { "AffiliateTargetId" });
            DropIndex("dbo.ProductType", new[] { "ContainerQuantityTypeId" });
            DropIndex("dbo.ProductType", new[] { "WeightQuantityTypeID" });
            DropIndex("dbo.ProductType", new[] { "SiteImageID" });
            DropIndex("dbo.AffiliateContractRate", new[] { "PriceUnitTypeId" });
            DropIndex("dbo.AffiliateContractRate", new[] { "QuantityTypeId" });
            DropIndex("dbo.AffiliateContractRate", new[] { "ProductTypeId" });
            DropIndex("dbo.AffiliateContractRate", new[] { "ServiceTypeId" });
            DropIndex("dbo.AffiliateContractRate", new[] { "AffiliateId" });
            DropIndex("dbo.AffiliateContractRate", new[] { "StateId" });
            DropIndex("dbo.AffiliateContactService", new[] { "AffiliateContactId" });
            DropIndex("dbo.AffiliateContactService", new[] { "ServiceTypeId" });
            DropIndex("dbo.AffiliateContact", new[] { "RoleId" });
            DropIndex("dbo.AffiliateContact", new[] { "SecurityQuestionId" });
            DropIndex("dbo.AffiliateContact", new[] { "AffiliateId" });
            DropIndex("dbo.AffiliateContact", new[] { "CityId" });
            DropIndex("dbo.AffiliateContact", new[] { "StateId" });
            DropIndex("dbo.Affiliate", new[] { "AffiliateTypeId" });
            DropIndex("dbo.Affiliate", new[] { "CityId" });
            DropIndex("dbo.Affiliate", new[] { "StateId" });
            DropIndex("dbo.AffiliateAccreditation", new[] { "AccreditationTypeId" });
            DropIndex("dbo.AffiliateAccreditation", new[] { "AffiliateId" });
            DropTable("dbo.V_ReportReconcileDetail");
            DropTable("dbo.V_ReportInvoiceForOEM");
            DropTable("dbo.V_ReportInvoice");
            DropTable("dbo.V_ReportEntitytargets");
            DropTable("dbo.V_ReoncileInvoiceDetail");
            DropTable("dbo.V_ReminderEmail");
            DropTable("dbo.V_ProcessingReport");
            DropTable("dbo.v_InvoicesRequiringFix");
            DropTable("dbo.V_InvoiceOverView");
            DropTable("dbo.V_CertificateReport");
            DropTable("dbo.StateGuideline");
            DropTable("dbo.StateGuideline_Link");
            DropTable("dbo.StateGuideline_DeadlineDate");
            DropTable("dbo.StateGuideline_Contact");
            DropTable("dbo.PaymentTerm");
            DropTable("dbo.NewsFeed");
            DropTable("dbo.InvoiceRequiringFix");
            DropTable("dbo.DropOffLocationFromAffiliate");
            DropTable("dbo.DropOffLocationDevice");
            DropTable("dbo.DropOffDevice");
            DropTable("dbo.DBAudit");
            DropTable("dbo.ConsumerProductTypeManufacturer");
            DropTable("dbo.RecyclerProductWeight");
            DropTable("dbo.SecurityQuestion");
            DropTable("dbo.Area");
            DropTable("dbo.Permissions");
            DropTable("dbo.AffiliateType");
            DropTable("dbo.AffiliateRole");
            DropTable("dbo.Role");
            DropTable("dbo.AuditAction");
            DropTable("dbo.AuditEvent");
            DropTable("dbo.AffiliateMasterContact");
            DropTable("dbo.ServiceTypeGroup");
            DropTable("dbo.ServiceTypeGroupRelation");
            DropTable("dbo.AffiliateService");
            DropTable("dbo.ForeignTable");
            DropTable("dbo.DocumentRelation");
            DropTable("dbo.Document");
            DropTable("dbo.DocumentType");
            DropTable("dbo.SiteImage");
            DropTable("dbo.MailBack");
            DropTable("dbo.MailBackDevices");
            DropTable("dbo.StateSupportedDevices");
            DropTable("dbo.StateSpecificRates");
            DropTable("dbo.StateProductType");
            DropTable("dbo.StatePolicy");
            DropTable("dbo.PlanYear");
            DropTable("dbo.ReminderStatus");
            DropTable("dbo.ReminderAttachment");
            DropTable("dbo.Period");
            DropTable("dbo.Reminder");
            DropTable("dbo.ReminderGroupAffiliate");
            DropTable("dbo.ReminderGroup");
            DropTable("dbo.RecurrenceType");
            DropTable("dbo.EventType");
            DropTable("dbo.Event");
            DropTable("dbo.DocumentLib");
            DropTable("dbo.Region");
            DropTable("dbo.Country");
            DropTable("dbo.LocationType");
            DropTable("dbo.DropOffLocationType");
            DropTable("dbo.DropOffLocation");
            DropTable("dbo.DropOff_Location");
            DropTable("dbo.County");
            DropTable("dbo.CollectionEvent");
            DropTable("dbo.StateWeightCategory");
            DropTable("dbo.PaymentType");
            DropTable("dbo.Payment");
            DropTable("dbo.PaymentDetail");
            DropTable("dbo.InvoiceUnpackId");
            DropTable("dbo.QuantityTypeGroup");
            DropTable("dbo.QuantityType");
            DropTable("dbo.InvoiceReconciliation");
            DropTable("dbo.InvoiceItemReconciliation");
            DropTable("dbo.InvoiceItem");
            DropTable("dbo.InvoiceAdjustment");
            DropTable("dbo.Invoice");
            DropTable("dbo.ProcessingData");
            DropTable("dbo.CollectionMethod");
            DropTable("dbo.PickUpAddress");
            DropTable("dbo.ClientAddress");
            DropTable("dbo.City");
            DropTable("dbo.AuditReports");
            DropTable("dbo.AffiliateState");
            DropTable("dbo.State");
            DropTable("dbo.ManufacturerPolicy");
            DropTable("dbo.Policy");
            DropTable("dbo.AffiliateTarget");
            DropTable("dbo.AffiliateTargetProduct");
            DropTable("dbo.ProductType");
            DropTable("dbo.PriceUnitType");
            DropTable("dbo.AffiliateContractRate");
            DropTable("dbo.ServiceType");
            DropTable("dbo.AffiliateContactService");
            DropTable("dbo.AffiliateContact");
            DropTable("dbo.Affiliate");
            DropTable("dbo.AffiliateAccreditation");
            DropTable("dbo.AccreditationType");
        }
    }
}
