﻿namespace Mits.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSecurityStamp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AffiliateContact", "SecurityStamp", c => c.String(maxLength: 40));
            AddColumn("dbo.AffiliateContact", "EmailConfirmed", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AffiliateContact", "EmailConfirmed");
            DropColumn("dbo.AffiliateContact", "SecurityStamp");
        }
    }
}
