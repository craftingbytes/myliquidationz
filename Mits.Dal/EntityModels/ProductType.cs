namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductType")]
    public partial class ProductType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductType()
        {
            AffiliateContractRates = new HashSet<AffiliateContractRate>();
            AffiliateTargetProducts = new HashSet<AffiliateTargetProduct>();
            InvoiceItems = new HashSet<InvoiceItem>();
            MailBackDevices = new HashSet<MailBackDevice>();
            StateProductTypes = new HashSet<StateProductType>();
            StateSupportedDevices = new HashSet<StateSupportedDevice>();
        }

        public int Id { get; set; }

        [StringLength(75)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(300)]
        public string Notes { get; set; }

        public int? SiteImageID { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? EstimatedWeight { get; set; }

        public int? WeightQuantityTypeID { get; set; }

        public int? EstimatedItemsPerContainer { get; set; }

        public int? ContainerQuantityTypeId { get; set; }

        public bool? ConsumerSelectable { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateContractRate> AffiliateContractRates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateTargetProduct> AffiliateTargetProducts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MailBackDevice> MailBackDevices { get; set; }

        public virtual QuantityType QuantityType { get; set; }

        public virtual QuantityType QuantityType1 { get; set; }

        public virtual SiteImage SiteImage { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StateProductType> StateProductTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StateSupportedDevice> StateSupportedDevices { get; set; }
    }
}
