namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Document")]
    public partial class Document
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Document()
        {
            DocumentRelations = new HashSet<DocumentRelation>();
        }

        public int Id { get; set; }

        public int? DocumentTypeId { get; set; }

        public int? AffiliateContactId { get; set; }

        [StringLength(2048)]
        public string ServerFileName { get; set; }

        [StringLength(2048)]
        public string UploadFileName { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UploadDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DeleteDate { get; set; }

        public virtual AffiliateContact AffiliateContact { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DocumentRelation> DocumentRelations { get; set; }
    }
}
