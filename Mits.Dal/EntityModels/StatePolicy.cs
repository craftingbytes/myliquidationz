namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StatePolicy")]
    public partial class StatePolicy
    {
        public int Id { get; set; }

        public int StateId { get; set; }

        public int PolicyId { get; set; }

        public bool Active { get; set; }

        public virtual Policy Policy { get; set; }

        public virtual State State { get; set; }
    }
}
