namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ReoncileInvoiceDetail
    {
        [StringLength(150)]
        public string RegionName { get; set; }

        [StringLength(200)]
        public string AffiliateDetailValue { get; set; }

        public int? FromInvoice { get; set; }

        public int? ToInvoice { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Item { get; set; }

        public decimal? Quantity { get; set; }

        [StringLength(75)]
        public string ProductTypeName { get; set; }

        public int? ReconcileFromInvoiceItemID { get; set; }
    }
}
