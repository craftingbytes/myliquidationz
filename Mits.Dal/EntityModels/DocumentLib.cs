namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DocumentLib")]
    public partial class DocumentLib
    {
        public int Id { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        [StringLength(200)]
        public string Tags { get; set; }

        public DateTime? UploadDate { get; set; }

        [StringLength(300)]
        public string AttachmentPath { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        public int? StateId { get; set; }

        public int? PlanYear { get; set; }

        public virtual State State { get; set; }
    }
}
