namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ReportInvoice
    {
        [StringLength(200)]
        public string AffiliateName { get; set; }

        [StringLength(200)]
        public string RecyclerAddress1 { get; set; }

        [StringLength(66)]
        public string RecyclerAddress2 { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(250)]
        public string VendorCompanyName { get; set; }

        [StringLength(50)]
        public string StateProvinceName { get; set; }

        [StringLength(200)]
        public string VendorAddress { get; set; }

        [StringLength(65)]
        public string VendorAddress1 { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InvoiceID { get; set; }

        [StringLength(50)]
        public string InvStateName { get; set; }

        public int? AffiliateID { get; set; }

        [StringLength(10)]
        public string ProcessingDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoiceDate { get; set; }

        [StringLength(10)]
        public string InvoiceDueDate { get; set; }

        [StringLength(10)]
        public string ServiceDate { get; set; }

        [StringLength(10)]
        public string InvoicePeriodFrom { get; set; }

        [StringLength(10)]
        public string InvoicePeriodTo { get; set; }

        [StringLength(50)]
        public string ShipmentID { get; set; }

        [StringLength(75)]
        public string ProductTypeName { get; set; }

        [StringLength(100)]
        public string ServiceTypeName { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Quantity { get; set; }

        [Key]
        [Column(Order = 2, TypeName = "money")]
        public decimal Rate { get; set; }

        [Column(TypeName = "money")]
        public decimal? Total { get; set; }

        [StringLength(200)]
        public string PickupAddress { get; set; }

        [StringLength(50)]
        public string ShipStateProvinceName { get; set; }

        [StringLength(50)]
        public string PickupCity { get; set; }

        [StringLength(10)]
        public string PickupZip { get; set; }

        [StringLength(20)]
        public string PickupPhone { get; set; }

        [StringLength(20)]
        public string PickupFax { get; set; }

        [StringLength(100)]
        public string PickupEmail { get; set; }

        [StringLength(80)]
        public string CollectionMethod { get; set; }

        [StringLength(2048)]
        public string Notes { get; set; }
    }
}
