namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateTargetProduct")]
    public partial class AffiliateTargetProduct
    {
        public int Id { get; set; }

        public int? AffiliateTargetId { get; set; }

        public int? ProductTypeId { get; set; }

        public virtual AffiliateTarget AffiliateTarget { get; set; }

        public virtual ProductType ProductType { get; set; }
    }
}
