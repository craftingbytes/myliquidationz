namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_CertificateReport
    {
        public int? RegionId { get; set; }

        [StringLength(75)]
        public string Product { get; set; }

        public int? RecQuantity { get; set; }

        public int? InvoiceID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ToAffiliateID { get; set; }

        [StringLength(200)]
        public string ToAffiliateName { get; set; }

        public int? FromAffiliate { get; set; }

        [StringLength(200)]
        public string AffiliateName { get; set; }

        [StringLength(200)]
        public string RecyclerAddress1 { get; set; }

        [StringLength(66)]
        public string RecyclerAddress2 { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(10)]
        public string ProcessingDate { get; set; }

        public DateTime? ProcessingDate1 { get; set; }

        [StringLength(50)]
        public string ShipmentNo { get; set; }

        [StringLength(50)]
        public string Region { get; set; }
    }
}
