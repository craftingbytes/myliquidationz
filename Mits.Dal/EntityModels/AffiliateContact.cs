namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateContact")]
    public partial class AffiliateContact
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AffiliateContact()
        {
            AffiliateContactServices = new HashSet<AffiliateContactService>();
            AffiliateMasterContacts = new HashSet<AffiliateMasterContact>();
            AffiliateMasterContacts1 = new HashSet<AffiliateMasterContact>();
            AuditEvents = new HashSet<AuditEvent>();
            Documents = new HashSet<Document>();
        }

        public int Id { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(3)]
        public string MiddleInitials { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(100)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(150)]
        public string Password { get; set; }

        public bool? Active { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        [StringLength(11)]
        public string Zip { get; set; }

        [StringLength(40)]
        public string SecurityStamp { get; set; }

        public bool? EmailConfirmed { get; set; }

        public int? AffiliateId { get; set; }

        public int? SecurityQuestionId { get; set; }

        [StringLength(500)]
        public string Answer { get; set; }

        public int? LoginTryCount { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastLoginTime { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(200)]
        public string Notes { get; set; }

        public int? RoleId { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual City City { get; set; }

        public virtual Role Role { get; set; }

        public virtual SecurityQuestion SecurityQuestion { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateContactService> AffiliateContactServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateMasterContact> AffiliateMasterContacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateMasterContact> AffiliateMasterContacts1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AuditEvent> AuditEvents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> Documents { get; set; }
    }
}
