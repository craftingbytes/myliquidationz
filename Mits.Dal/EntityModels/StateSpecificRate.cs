namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class StateSpecificRate
    {
        public int Id { get; set; }

        public int StateId { get; set; }

        public double Urban { get; set; }

        public double Rural { get; set; }

        public double Metro { get; set; }

        public double NonMetro { get; set; }

        public virtual State State { get; set; }
    }
}
