namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateContractRate")]
    public partial class AffiliateContractRate
    {
        public int Id { get; set; }

        public int? StateId { get; set; }

        public int? AffiliateId { get; set; }

        public int? ServiceTypeId { get; set; }

        public int? ProductTypeId { get; set; }

        public int? QuantityTypeId { get; set; }

        public int? PriceUnitTypeId { get; set; }

        public DateTime? RateStartDate { get; set; }

        public DateTime? RateEndDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? Rate { get; set; }

        public decimal? QtyAdjustPercent { get; set; }

        public int? PlanYear { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual PriceUnitType PriceUnitType { get; set; }

        public virtual ProductType ProductType { get; set; }

        public virtual QuantityType QuantityType { get; set; }

        public virtual ServiceType ServiceType { get; set; }

        public virtual State State { get; set; }
    }
}
