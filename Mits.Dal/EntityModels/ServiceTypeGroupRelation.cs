namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ServiceTypeGroupRelation")]
    public partial class ServiceTypeGroupRelation
    {
        public int Id { get; set; }

        public int? ServiceTypeGroupId { get; set; }

        public int? ServiceTypeId { get; set; }

        public bool? Active { get; set; }

        public virtual ServiceType ServiceType { get; set; }

        public virtual ServiceTypeGroup ServiceTypeGroup { get; set; }
    }
}
