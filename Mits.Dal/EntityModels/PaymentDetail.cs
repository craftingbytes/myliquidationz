namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PaymentDetail")]
    public partial class PaymentDetail
    {
        public int Id { get; set; }

        public int? PaymentID { get; set; }

        public int? InvoiceId { get; set; }

        [Column(TypeName = "money")]
        public decimal? PaidAmount { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual Payment Payment { get; set; }
    }
}
