namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StateGuideline")]
    public partial class StateGuideline
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StateId { get; set; }

        [StringLength(2000)]
        public string GoverningBody { get; set; }

        [StringLength(500)]
        public string Website { get; set; }

        [StringLength(2000)]
        public string CoveredDevices { get; set; }

        [StringLength(2000)]
        public string OEMObligation { get; set; }

        [StringLength(2000)]
        public string Updates { get; set; }

        public int? LawState { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(2000)]
        public string RegulationType { get; set; }

        [StringLength(2000)]
        public string RegulationChange { get; set; }
    }
}
