namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DropOffLocationDevice")]
    public partial class DropOffLocationDevice
    {
        public int Id { get; set; }

        public int DropOffLocationId { get; set; }

        public int DeviceId { get; set; }

        public virtual DropOffDevice DropOffDevice { get; set; }
    }
}
