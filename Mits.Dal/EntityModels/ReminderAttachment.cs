namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReminderAttachment")]
    public partial class ReminderAttachment
    {
        public int Id { get; set; }

        public int? ReminderId { get; set; }

        [StringLength(300)]
        public string AttachmentPath { get; set; }

        [StringLength(150)]
        public string AttachmentName { get; set; }

        public virtual Reminder Reminder { get; set; }
    }
}
