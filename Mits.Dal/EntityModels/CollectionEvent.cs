namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CollectionEvent")]
    public partial class CollectionEvent
    {
        public int Id { get; set; }

        public int StateId { get; set; }

        public int CityId { get; set; }

        [StringLength(10)]
        public string ZipCode { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(200)]
        public string Hours { get; set; }

        [StringLength(200)]
        public string Location { get; set; }

        [StringLength(2000)]
        public string Note { get; set; }

        public virtual City City { get; set; }

        public virtual State State { get; set; }
    }
}
