namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvoiceAdjustment")]
    public partial class InvoiceAdjustment
    {
        public int Id { get; set; }

        public int InvoiceId { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [Column(TypeName = "money")]
        public decimal Amount { get; set; }

        public DateTime? AdjustmentDate { get; set; }

        public virtual Invoice Invoice { get; set; }
    }
}
