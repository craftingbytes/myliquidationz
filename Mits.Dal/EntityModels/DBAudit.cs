namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DBAudit")]
    public partial class DBAudit
    {
        [Key]
        [StringLength(200)]
        public string AuditId { get; set; }

        public DateTime? RevisionStamp { get; set; }

        [StringLength(50)]
        public string TableName { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(1)]
        public string Actions { get; set; }

        [Column(TypeName = "xml")]
        public string OldData { get; set; }

        [Column(TypeName = "xml")]
        public string NewData { get; set; }

        [StringLength(1000)]
        public string ChangedColumns { get; set; }
    }
}
