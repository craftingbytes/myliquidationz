namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ReportReconcileDetail
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FromStateID { get; set; }

        [StringLength(50)]
        public string FromStateName { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FromEntityId { get; set; }

        [StringLength(200)]
        public string FromEntityName { get; set; }

        public int? FromPlanYear { get; set; }

        public int? FromInvoiceId { get; set; }

        public int? ToStateId { get; set; }

        [StringLength(50)]
        public string ToStateName { get; set; }

        public int? ToEntityId { get; set; }

        [StringLength(200)]
        public string ToEntityName { get; set; }

        public int? ToInvoiceId { get; set; }

        public int? ReconcileFromInvoiceItemID { get; set; }

        public int? ReconcileToInvoiceItemID { get; set; }

        [Column(TypeName = "money")]
        public decimal? FromInvoiceWeight { get; set; }

        [Key]
        [Column(Order = 2)]
        public decimal RecAssignedWeight { get; set; }

        public int? ReconcileItemCount { get; set; }
    }
}
