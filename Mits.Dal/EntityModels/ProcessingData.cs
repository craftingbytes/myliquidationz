namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProcessingData")]
    public partial class ProcessingData
    {
        public int Id { get; set; }

        public DateTime? ProcessingDate { get; set; }

        [StringLength(1000)]
        public string Comments { get; set; }

        public int? InvoiceId { get; set; }

        [StringLength(50)]
        public string ShipmentNo { get; set; }

        [StringLength(250)]
        public string VendorCompanyName { get; set; }

        [StringLength(200)]
        public string VendorAddress { get; set; }

        public int? VendorCityId { get; set; }

        public int? VendorStateId { get; set; }

        [StringLength(10)]
        public string VendorZip { get; set; }

        [StringLength(20)]
        public string VendorPhone { get; set; }

        [StringLength(250)]
        public string PickUpCompanyName { get; set; }

        [StringLength(200)]
        public string PickupAddress { get; set; }

        public int? PickupStateId { get; set; }

        public int? PickupCityId { get; set; }

        [StringLength(10)]
        public string PickupZip { get; set; }

        [StringLength(20)]
        public string PickupPhone { get; set; }

        [StringLength(20)]
        public string PickupFax { get; set; }

        [StringLength(100)]
        public string PickupEmail { get; set; }

        [StringLength(200)]
        public string Client { get; set; }

        [StringLength(200)]
        public string RecievedBy { get; set; }

        [StringLength(200)]
        public string State { get; set; }

        public int? CollectionMethodId { get; set; }

        public int? StateWeightCategoryId { get; set; }

        public virtual City City { get; set; }

        public virtual City City1 { get; set; }

        public virtual CollectionMethod CollectionMethod { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual StateWeightCategory StateWeightCategory { get; set; }
    }
}
