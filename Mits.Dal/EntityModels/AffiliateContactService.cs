namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateContactService")]
    public partial class AffiliateContactService
    {
        public int Id { get; set; }

        public int? AssociationPriority { get; set; }

        public bool? AssociationExclusive { get; set; }

        public int ServiceTypeId { get; set; }

        public int AffiliateContactId { get; set; }

        public bool Active { get; set; }

        public virtual AffiliateContact AffiliateContact { get; set; }

        public virtual ServiceType ServiceType { get; set; }
    }
}
