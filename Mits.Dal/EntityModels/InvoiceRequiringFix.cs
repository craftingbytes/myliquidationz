namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvoiceRequiringFix")]
    public partial class InvoiceRequiringFix
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InvoiceId { get; set; }

        [Column(TypeName = "money")]
        public decimal? OldQuantity { get; set; }

        [Column(TypeName = "money")]
        public decimal? OldTotal { get; set; }

        public decimal? NewQuantity { get; set; }

        public decimal? NewTotal { get; set; }

        [Column(TypeName = "money")]
        public decimal? PaidAmount { get; set; }
    }
}
