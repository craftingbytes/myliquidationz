namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MailBackDevice
    {
        public int Id { get; set; }

        public int MailBackId { get; set; }

        public int ManufacturerId { get; set; }

        public int ProductTypeId { get; set; }

        [Required]
        [StringLength(100)]
        public string Model { get; set; }

        [Required]
        [StringLength(50)]
        public string Size { get; set; }

        public int Quantity { get; set; }

        public bool NeedBoxes { get; set; }

        [StringLength(50)]
        public string BoxSize { get; set; }

        public int? BoxQuantity { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual MailBack MailBack { get; set; }

        public virtual ProductType ProductType { get; set; }
    }
}
