namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateState")]
    public partial class AffiliateState
    {
        public int Id { get; set; }

        public int AffiliateId { get; set; }

        public int StateId { get; set; }

        public bool Active { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual State State { get; set; }
    }
}
