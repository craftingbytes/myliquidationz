using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Mits.DAL.EntityModels
{
    public partial class MITSEntities : DbContext
    {
        public MITSEntities()
            : base("name=MITSEntities")
        {
        }

        public virtual DbSet<AccreditationType> AccreditationTypes { get; set; }
        public virtual DbSet<Affiliate> Affiliates { get; set; }
        public virtual DbSet<AffiliateAccreditation> AffiliateAccreditations { get; set; }
        public virtual DbSet<AffiliateContact> AffiliateContacts { get; set; }
        public virtual DbSet<AffiliateContactService> AffiliateContactServices { get; set; }
        public virtual DbSet<AffiliateContractRate> AffiliateContractRates { get; set; }
        public virtual DbSet<AffiliateMasterContact> AffiliateMasterContacts { get; set; }
        public virtual DbSet<AffiliateRole> AffiliateRoles { get; set; }
        public virtual DbSet<AffiliateService> AffiliateServices { get; set; }
        public virtual DbSet<AffiliateState> AffiliateStates { get; set; }
        public virtual DbSet<AffiliateTarget> AffiliateTargets { get; set; }
        public virtual DbSet<AffiliateTargetProduct> AffiliateTargetProducts { get; set; }
        public virtual DbSet<AffiliateType> AffiliateTypes { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<AuditAction> AuditActions { get; set; }
        public virtual DbSet<AuditEvent> AuditEvents { get; set; }
        public virtual DbSet<AuditReport> AuditReports { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<ClientAddress> ClientAddresses { get; set; }
        public virtual DbSet<CollectionEvent> CollectionEvents { get; set; }
        public virtual DbSet<CollectionMethod> CollectionMethods { get; set; }
        public virtual DbSet<ConsumerProductTypeManufacturer> ConsumerProductTypeManufacturers { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<County> Counties { get; set; }
        public virtual DbSet<DBAudit> DBAudits { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentLib> DocumentLibs { get; set; }
        public virtual DbSet<DocumentRelation> DocumentRelations { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<DropOff_Location> DropOff_Location { get; set; }
        public virtual DbSet<DropOffDevice> DropOffDevices { get; set; }
        public virtual DbSet<DropOffLocation> DropOffLocations { get; set; }
        public virtual DbSet<DropOffLocationDevice> DropOffLocationDevices { get; set; }
        public virtual DbSet<DropOffLocationType> DropOffLocationTypes { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<EventType> EventTypes { get; set; }
        public virtual DbSet<ForeignTable> ForeignTables { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<InvoiceAdjustment> InvoiceAdjustments { get; set; }
        public virtual DbSet<InvoiceItem> InvoiceItems { get; set; }
        public virtual DbSet<InvoiceItemReconciliation> InvoiceItemReconciliations { get; set; }
        public virtual DbSet<InvoiceReconciliation> InvoiceReconciliations { get; set; }
        public virtual DbSet<InvoiceUnpackId> InvoiceUnpackIds { get; set; }
        public virtual DbSet<LocationType> LocationTypes { get; set; }
        public virtual DbSet<MailBack> MailBacks { get; set; }
        public virtual DbSet<MailBackDevice> MailBackDevices { get; set; }
        public virtual DbSet<ManufacturerPolicy> ManufacturerPolicies { get; set; }
        public virtual DbSet<NewsFeed> NewsFeeds { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<PaymentDetail> PaymentDetails { get; set; }
        public virtual DbSet<PaymentTerm> PaymentTerms { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public virtual DbSet<Period> Periods { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<PickUpAddress> PickUpAddresses { get; set; }
        public virtual DbSet<PlanYear> PlanYears { get; set; }
        public virtual DbSet<Policy> Policies { get; set; }
        public virtual DbSet<PriceUnitType> PriceUnitTypes { get; set; }
        public virtual DbSet<ProcessingData> ProcessingDatas { get; set; }
        public virtual DbSet<ProductType> ProductTypes { get; set; }
        public virtual DbSet<QuantityType> QuantityTypes { get; set; }
        public virtual DbSet<QuantityTypeGroup> QuantityTypeGroups { get; set; }
        public virtual DbSet<RecurrenceType> RecurrenceTypes { get; set; }
        public virtual DbSet<RecyclerProductWeight> RecyclerProductWeights { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Reminder> Reminders { get; set; }
        public virtual DbSet<ReminderAttachment> ReminderAttachments { get; set; }
        public virtual DbSet<ReminderGroup> ReminderGroups { get; set; }
        public virtual DbSet<ReminderGroupAffiliate> ReminderGroupAffiliates { get; set; }
        public virtual DbSet<ReminderStatu> ReminderStatus { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<SecurityQuestion> SecurityQuestions { get; set; }
        public virtual DbSet<ServiceType> ServiceTypes { get; set; }
        public virtual DbSet<ServiceTypeGroup> ServiceTypeGroups { get; set; }
        public virtual DbSet<ServiceTypeGroupRelation> ServiceTypeGroupRelations { get; set; }
        public virtual DbSet<SiteImage> SiteImages { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<StateGuideline> StateGuidelines { get; set; }
        public virtual DbSet<StateGuideline_Contact> StateGuideline_Contact { get; set; }
        public virtual DbSet<StateGuideline_DeadlineDate> StateGuideline_DeadlineDate { get; set; }
        public virtual DbSet<StateGuideline_Link> StateGuideline_Link { get; set; }
        public virtual DbSet<StatePolicy> StatePolicies { get; set; }
        public virtual DbSet<StateProductType> StateProductTypes { get; set; }
        public virtual DbSet<StateSpecificRate> StateSpecificRates { get; set; }
        public virtual DbSet<StateSupportedDevice> StateSupportedDevices { get; set; }
        public virtual DbSet<StateWeightCategory> StateWeightCategories { get; set; }
        public virtual DbSet<DropOffLocationFromAffiliate> DropOffLocationFromAffiliates { get; set; }
        public virtual DbSet<InvoiceRequiringFix> InvoiceRequiringFixes { get; set; }
        public virtual DbSet<V_CertificateReport> V_CertificateReport { get; set; }
        public virtual DbSet<V_InvoiceOverView> V_InvoiceOverView { get; set; }
        public virtual DbSet<v_InvoicesRequiringFix> v_InvoicesRequiringFix { get; set; }
        public virtual DbSet<V_ProcessingReport> V_ProcessingReport { get; set; }
        public virtual DbSet<V_ReminderEmail> V_ReminderEmail { get; set; }
        public virtual DbSet<V_ReoncileInvoiceDetail> V_ReoncileInvoiceDetail { get; set; }
        public virtual DbSet<V_ReportEntitytargets> V_ReportEntitytargets { get; set; }
        public virtual DbSet<V_ReportInvoice> V_ReportInvoice { get; set; }
        public virtual DbSet<V_ReportInvoiceForOEM> V_ReportInvoiceForOEM { get; set; }
        public virtual DbSet<V_ReportReconcileDetail> V_ReportReconcileDetail { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccreditationType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<AccreditationType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.WebSite)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Latitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.AffiliateServices)
                .WithRequired(e => e.Affiliate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.AffiliateStates)
                .WithRequired(e => e.Affiliate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.ClientAddresses)
                .WithRequired(e => e.Affiliate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.MailBackDevices)
                .WithRequired(e => e.Affiliate)
                .HasForeignKey(e => e.ManufacturerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.RecyclerProductWeights)
                .WithRequired(e => e.Affiliate)
                .HasForeignKey(e => e.RecyclerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.Roles)
                .WithOptional(e => e.Affiliate)
                .HasForeignKey(e => e.CreateByAffiliateId);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.MiddleInitials)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Answer)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .HasMany(e => e.AffiliateContactServices)
                .WithRequired(e => e.AffiliateContact)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AffiliateContact>()
                .HasMany(e => e.AffiliateMasterContacts)
                .WithRequired(e => e.AffiliateContact)
                .HasForeignKey(e => e.MasterAffiliateContactId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AffiliateContact>()
                .HasMany(e => e.AffiliateMasterContacts1)
                .WithRequired(e => e.AffiliateContact1)
                .HasForeignKey(e => e.AssociateContactId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AffiliateContact>()
                .HasMany(e => e.AuditEvents)
                .WithRequired(e => e.AffiliateContact)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AffiliateContractRate>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AffiliateContractRate>()
                .Property(e => e.QtyAdjustPercent)
                .HasPrecision(4, 3);

            modelBuilder.Entity<AffiliateTarget>()
                .Property(e => e.Weight)
                .HasPrecision(18, 0);

            modelBuilder.Entity<AffiliateType>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Area>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Area>()
                .Property(e => e.Caption)
                .IsUnicode(false);

            modelBuilder.Entity<Area>()
                .Property(e => e.URL)
                .IsUnicode(false);

            modelBuilder.Entity<AuditAction>()
                .HasMany(e => e.AuditEvents)
                .WithRequired(e => e.AuditAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<City>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<City>()
                .HasMany(e => e.CollectionEvents)
                .WithRequired(e => e.City)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<City>()
                .HasMany(e => e.ProcessingDatas)
                .WithOptional(e => e.City)
                .HasForeignKey(e => e.VendorCityId);

            modelBuilder.Entity<City>()
                .HasMany(e => e.ProcessingDatas1)
                .WithOptional(e => e.City1)
                .HasForeignKey(e => e.PickupCityId);

            modelBuilder.Entity<ClientAddress>()
                .Property(e => e.CompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<ClientAddress>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<ClientAddress>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<ClientAddress>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<ClientAddress>()
                .HasMany(e => e.PickUpAddresses)
                .WithRequired(e => e.ClientAddress)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CollectionMethod>()
                .Property(e => e.Method)
                .IsUnicode(false);

            modelBuilder.Entity<ConsumerProductTypeManufacturer>()
                .Property(e => e.ManufacturerName)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.Abbreviation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.Abbreviation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DBAudit>()
                .Property(e => e.AuditId)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.ServerFileName)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.UploadFileName)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.DocumentRelations)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DocumentLib>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentLib>()
                .Property(e => e.Tags)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentLib>()
                .Property(e => e.AttachmentPath)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentLib>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .Property(e => e.FileExtension)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .Property(e => e.DocumentTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .Property(e => e.ContentType)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .HasOptional(e => e.DocumentType1)
                .WithRequired(e => e.DocumentType2);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Website)
                .IsUnicode(false);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.Latitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<DropOff_Location>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffDevice>()
                .HasMany(e => e.DropOffLocationDevices)
                .WithRequired(e => e.DropOffDevice)
                .HasForeignKey(e => e.DeviceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DropOffLocation>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocation>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocation>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocation>()
                .Property(e => e.WebSite)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocation>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<DropOffLocation>()
                .Property(e => e.Latitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<DropOffLocation>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocation>()
                .HasMany(e => e.DropOffLocationTypes)
                .WithRequired(e => e.DropOffLocation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.TemplateName)
                .IsUnicode(false);

            modelBuilder.Entity<EventType>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<ForeignTable>()
                .Property(e => e.ForeignTableName)
                .IsUnicode(false);

            modelBuilder.Entity<ForeignTable>()
                .HasMany(e => e.DocumentRelations)
                .WithRequired(e => e.ForeignTable)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invoice>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<Invoice>()
                .Property(e => e.RegionName)
                .IsUnicode(false);

            modelBuilder.Entity<Invoice>()
                .HasMany(e => e.InvoiceAdjustments)
                .WithRequired(e => e.Invoice)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invoice>()
                .HasMany(e => e.InvoiceReconciliations)
                .WithRequired(e => e.Invoice)
                .HasForeignKey(e => e.OEMInvoiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invoice>()
                .HasMany(e => e.InvoiceUnpackIds)
                .WithOptional(e => e.Invoice)
                .HasForeignKey(e => e.OEMInvoiceId);

            modelBuilder.Entity<Invoice>()
                .HasMany(e => e.InvoiceUnpackIds1)
                .WithOptional(e => e.Invoice1)
                .HasForeignKey(e => e.ProcesserInvoiceId);

            modelBuilder.Entity<InvoiceAdjustment>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceItem>()
                .Property(e => e.Quantity)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceItem>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceItem>()
                .Property(e => e.QuantityAdjustPercent)
                .HasPrecision(4, 3);

            modelBuilder.Entity<InvoiceItem>()
                .Property(e => e.OriginalQuantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<InvoiceItem>()
                .HasMany(e => e.InvoiceItemReconciliations)
                .WithOptional(e => e.InvoiceItem)
                .HasForeignKey(e => e.ReconcileFromInvoiceItemID);

            modelBuilder.Entity<InvoiceItem>()
                .HasMany(e => e.InvoiceItemReconciliations1)
                .WithOptional(e => e.InvoiceItem1)
                .HasForeignKey(e => e.ReconcileToInvoiceItemID);

            modelBuilder.Entity<InvoiceItem>()
                .HasMany(e => e.InvoiceReconciliations)
                .WithRequired(e => e.InvoiceItem)
                .HasForeignKey(e => e.ProcesserInvoiceItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvoiceReconciliation>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<LocationType>()
                .HasMany(e => e.DropOffLocationTypes)
                .WithRequired(e => e.LocationType)
                .HasForeignKey(e => e.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MailBack>()
                .Property(e => e.MailBackAddress)
                .IsUnicode(false);

            modelBuilder.Entity<MailBack>()
                .HasMany(e => e.MailBackDevices)
                .WithRequired(e => e.MailBack)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MailBackDevice>()
                .Property(e => e.Model)
                .IsUnicode(false);

            modelBuilder.Entity<MailBackDevice>()
                .Property(e => e.Size)
                .IsUnicode(false);

            modelBuilder.Entity<MailBackDevice>()
                .Property(e => e.BoxSize)
                .IsUnicode(false);

            modelBuilder.Entity<Payment>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payment>()
                .Property(e => e.ReferenceNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Payment>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<PaymentDetail>()
                .Property(e => e.PaidAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<PaymentTerm>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PaymentTerm>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<PaymentType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PaymentType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Period>()
                .Property(e => e.Period1)
                .IsUnicode(false);

            modelBuilder.Entity<PickUpAddress>()
                .Property(e => e.CompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<PickUpAddress>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<PickUpAddress>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<PickUpAddress>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<PickUpAddress>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<PickUpAddress>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Policy>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Policy>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Policy>()
                .HasMany(e => e.StatePolicies)
                .WithRequired(e => e.Policy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PriceUnitType>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.ShipmentNo)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.VendorCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.VendorAddress)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.VendorZip)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.VendorPhone)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.PickUpCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.PickupAddress)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.PickupZip)
                .IsFixedLength();

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.PickupPhone)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.PickupFax)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.PickupEmail)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.Client)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.RecievedBy)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessingData>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<ProductType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ProductType>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<ProductType>()
                .Property(e => e.EstimatedWeight)
                .HasPrecision(10, 4);

            modelBuilder.Entity<ProductType>()
                .HasMany(e => e.AffiliateTargetProducts)
                .WithOptional(e => e.ProductType)
                .WillCascadeOnDelete();

            modelBuilder.Entity<ProductType>()
                .HasMany(e => e.MailBackDevices)
                .WithRequired(e => e.ProductType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductType>()
                .HasMany(e => e.StateProductTypes)
                .WithRequired(e => e.ProductType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductType>()
                .HasMany(e => e.StateSupportedDevices)
                .WithRequired(e => e.ProductType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<QuantityType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<QuantityType>()
                .Property(e => e.Abbrev)
                .IsUnicode(false);

            modelBuilder.Entity<QuantityType>()
                .HasMany(e => e.ProductTypes)
                .WithOptional(e => e.QuantityType)
                .HasForeignKey(e => e.WeightQuantityTypeID);

            modelBuilder.Entity<QuantityType>()
                .HasMany(e => e.ProductTypes1)
                .WithOptional(e => e.QuantityType1)
                .HasForeignKey(e => e.ContainerQuantityTypeId);

            modelBuilder.Entity<QuantityTypeGroup>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.Abbreviation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Reminder>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Reminder>()
                .Property(e => e.Number)
                .IsUnicode(false);

            modelBuilder.Entity<Reminder>()
                .HasMany(e => e.ReminderAttachments)
                .WithOptional(e => e.Reminder)
                .WillCascadeOnDelete();

            modelBuilder.Entity<ReminderGroup>()
                .HasMany(e => e.Reminders)
                .WithOptional(e => e.ReminderGroup)
                .WillCascadeOnDelete();

            modelBuilder.Entity<ReminderStatu>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<ReminderStatu>()
                .HasMany(e => e.Reminders)
                .WithOptional(e => e.ReminderStatu)
                .HasForeignKey(e => e.StatusId);

            modelBuilder.Entity<Role>()
                .Property(e => e.RoleName)
                .IsUnicode(false);

            modelBuilder.Entity<SecurityQuestion>()
                .Property(e => e.Question)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceType>()
                .HasMany(e => e.AffiliateContactServices)
                .WithRequired(e => e.ServiceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceType>()
                .HasMany(e => e.AffiliateServices)
                .WithRequired(e => e.ServiceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceTypeGroup>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceTypeGroup>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<SiteImage>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.Abbreviation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.AffiliateStates)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.CollectionEvents)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.PlanYears)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.StatePolicies)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.StateProductTypes)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.StateSpecificRates)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.StateSupportedDevices)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.StateWeightCategories)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StateWeightCategory>()
                .Property(e => e.Ratio)
                .HasPrecision(18, 4);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.WebSite)
                .IsUnicode(false);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Latitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<DropOffLocationFromAffiliate>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<InvoiceRequiringFix>()
                .Property(e => e.OldQuantity)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceRequiringFix>()
                .Property(e => e.OldTotal)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceRequiringFix>()
                .Property(e => e.NewQuantity)
                .HasPrecision(38, 2);

            modelBuilder.Entity<InvoiceRequiringFix>()
                .Property(e => e.NewTotal)
                .HasPrecision(38, 6);

            modelBuilder.Entity<InvoiceRequiringFix>()
                .Property(e => e.PaidAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.Product)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.ToAffiliateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.AffiliateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.RecyclerAddress1)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.RecyclerAddress2)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.ProcessingDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.ShipmentNo)
                .IsUnicode(false);

            modelBuilder.Entity<V_CertificateReport>()
                .Property(e => e.Region)
                .IsUnicode(false);

            modelBuilder.Entity<V_InvoiceOverView>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceOverView>()
                .Property(e => e.Volume)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceOverView>()
                .Property(e => e.AdjustedVolume)
                .HasPrecision(38, 7);

            modelBuilder.Entity<V_InvoiceOverView>()
                .Property(e => e.RegionName)
                .IsUnicode(false);

            modelBuilder.Entity<V_InvoiceOverView>()
                .Property(e => e.AffiliateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_InvoiceOverView>()
                .Property(e => e.TargetQuantity)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_InvoiceOverView>()
                .Property(e => e.TargetPercentOfYear)
                .HasPrecision(38, 6);

            modelBuilder.Entity<v_InvoicesRequiringFix>()
                .Property(e => e.OldQuantity)
                .HasPrecision(19, 4);

            modelBuilder.Entity<v_InvoicesRequiringFix>()
                .Property(e => e.OldTotal)
                .HasPrecision(19, 4);

            modelBuilder.Entity<v_InvoicesRequiringFix>()
                .Property(e => e.NewQuantity)
                .HasPrecision(38, 2);

            modelBuilder.Entity<v_InvoicesRequiringFix>()
                .Property(e => e.NewTotal)
                .HasPrecision(38, 6);

            modelBuilder.Entity<v_InvoicesRequiringFix>()
                .Property(e => e.PaidAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.AffiliateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.RecyclerAddress1)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.RecyclerAddress2)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.StateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.ShipmentNo)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.VendorCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.VendorAddress)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.VendorCity)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.VendorStateProvinceName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.VendorZip)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.VendorPhone)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.PickupAddress)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.ShipStateProvinceName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.PickupCity)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.PickUpCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.PickupZip)
                .IsFixedLength();

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.PickupPhone)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.PickupFax)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.PickupEmail)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.Client)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.RecievedBy)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.ProductTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.ServiceTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.CollectionMethod)
                .IsUnicode(false);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.Qauntity)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_ProcessingReport>()
                .Property(e => e.OriginalQuantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<V_ReminderEmail>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReminderEmail>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReminderEmail>()
                .Property(e => e.EventTitle)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReminderEmail>()
                .Property(e => e.EventDescription)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReminderEmail>()
                .Property(e => e.Number)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReminderEmail>()
                .Property(e => e.Period)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReminderEmail>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReminderEmail>()
                .Property(e => e.eType)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReoncileInvoiceDetail>()
                .Property(e => e.RegionName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReoncileInvoiceDetail>()
                .Property(e => e.AffiliateDetailValue)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReoncileInvoiceDetail>()
                .Property(e => e.ProductTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.EntityName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoiceAmount)
                .HasPrecision(38, 6);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.PaymentAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.RemainingAmount)
                .HasPrecision(38, 6);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoiceDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoiceYearHalf)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoiceQaurter)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.ProcessingDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoiceDueDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.PaymentDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoicePeriodFrom)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoicePeriodTo)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.PaidStatus)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoiceWeight)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.RecAssignedWeight)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.RecUnAssignedWeight)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.RecWeight)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvRemainingWeight)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.TargetWeight)
                .HasPrecision(18, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvoiceStatus)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.TargetStartDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.TargetEndDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvAvgWeight)
                .HasPrecision(29, 11);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.InvRemWeight)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.TargetRange)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.IndTargetWeight)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.IndRecWeight)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.RemIndTarget)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.StateAffilaiteTarget)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.StateAffilaiteRec)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.StateAffilaiteRem)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.StateTarget)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.StateRec)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.StateRem)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.AffilaiteStateTarget)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.AffilaiteStateRec)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.AffilaiteStateRem)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.AffilaiteTarget)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.AffilaiteRec)
                .HasPrecision(38, 2);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.AffilaiteRem)
                .HasPrecision(38, 0);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.IndividualTargetStatus)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.StateAffiliateTargetStatus)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.StateTargetStatus)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.AffiliateStateTargetStatus)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportEntitytargets>()
                .Property(e => e.AffiliateTargetStatus)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.AffiliateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.RecyclerAddress1)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.RecyclerAddress2)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.VendorCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.StateProvinceName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.VendorAddress)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.VendorAddress1)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.InvStateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.ProcessingDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.InvoiceDueDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.ServiceDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.InvoicePeriodFrom)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.InvoicePeriodTo)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.ShipmentID)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.ProductTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.ServiceTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.PickupAddress)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.ShipStateProvinceName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.PickupCity)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.PickupZip)
                .IsFixedLength();

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.PickupPhone)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.PickupFax)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.PickupEmail)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.CollectionMethod)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoice>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.AffiliateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.RecyclerAddress1)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.RecyclerAddress2)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.VendorCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.StateProvinceName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.VendorAddress)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.VendorAddress1)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.InvStateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.ProcessingDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.InvoiceDueDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.ServiceDate)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.InvoicePeriodFrom)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.InvoicePeriodTo)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.ShipmentID)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.ProductTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.ServiceTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.Total)
                .HasPrecision(38, 6);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.PickupAddress)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.ShipStateProvinceName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.PickupCity)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.PickupZip)
                .IsFixedLength();

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.PickupPhone)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.PickupFax)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.PickupEmail)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.CollectionMethod)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportInvoiceForOEM>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportReconcileDetail>()
                .Property(e => e.FromStateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportReconcileDetail>()
                .Property(e => e.FromEntityName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportReconcileDetail>()
                .Property(e => e.ToStateName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportReconcileDetail>()
                .Property(e => e.ToEntityName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ReportReconcileDetail>()
                .Property(e => e.FromInvoiceWeight)
                .HasPrecision(19, 4);
        }
    }
}
