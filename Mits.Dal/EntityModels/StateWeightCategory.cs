namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StateWeightCategory")]
    public partial class StateWeightCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public StateWeightCategory()
        {
            ProcessingDatas = new HashSet<ProcessingData>();
        }

        public int Id { get; set; }

        public int StateId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public decimal Ratio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcessingData> ProcessingDatas { get; set; }

        public virtual State State { get; set; }
    }
}
