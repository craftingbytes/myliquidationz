namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PickUpAddress")]
    public partial class PickUpAddress
    {
        public int Id { get; set; }

        public int ClientAddressId { get; set; }

        [StringLength(250)]
        public string CompanyName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(20)]
        public string Fax { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public int? CollectionMethodId { get; set; }

        public virtual City City { get; set; }

        public virtual ClientAddress ClientAddress { get; set; }

        public virtual CollectionMethod CollectionMethod { get; set; }

        public virtual State State { get; set; }
    }
}
