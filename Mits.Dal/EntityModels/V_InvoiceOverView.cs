namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_InvoiceOverView
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InvoiceID { get; set; }

        public bool? Receivable { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoiceDate { get; set; }

        public int? InvoicePeriod { get; set; }

        [Column(TypeName = "money")]
        public decimal? Cost { get; set; }

        [Column(TypeName = "money")]
        public decimal? Volume { get; set; }

        public decimal? AdjustedVolume { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RegionID { get; set; }

        [StringLength(50)]
        public string RegionName { get; set; }

        public int? AffiliateID { get; set; }

        [StringLength(200)]
        public string AffiliateName { get; set; }

        public int? PaymentTermID { get; set; }

        public decimal? TargetQuantity { get; set; }

        public decimal? TargetPercentOfYear { get; set; }

        public int? QuantityTypeID { get; set; }

        public int? ServiceTypeID { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoicePeriodFrom { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoicePeriodTo { get; set; }

        public DateTime? PeriodBegin { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PeriodEnd { get; set; }
    }
}
