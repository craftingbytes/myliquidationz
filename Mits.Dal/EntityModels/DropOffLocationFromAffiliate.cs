namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DropOffLocationFromAffiliate")]
    public partial class DropOffLocationFromAffiliate
    {
        public int Id { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string WebSite { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? Latitude { get; set; }

        public bool? Active { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        [StringLength(11)]
        public string Zip { get; set; }

        public int? AffiliateTypeId { get; set; }

        public int? PaymentTermId { get; set; }

        [StringLength(2048)]
        public string Notes { get; set; }

        [StringLength(2048)]
        public string Directions { get; set; }

        [StringLength(2048)]
        public string BusinessHours { get; set; }
    }
}
