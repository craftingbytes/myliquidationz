namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvoiceItem")]
    public partial class InvoiceItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvoiceItem()
        {
            InvoiceItemReconciliations = new HashSet<InvoiceItemReconciliation>();
            InvoiceItemReconciliations1 = new HashSet<InvoiceItemReconciliation>();
            InvoiceReconciliations = new HashSet<InvoiceReconciliation>();
        }

        public int Id { get; set; }

        public int? InvoiceId { get; set; }

        public int? ShipmentId { get; set; }

        public int? ServiceTypeId { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ServiceDate { get; set; }

        public int? ProductTypeId { get; set; }

        public int? QuantityTypeId { get; set; }

        [Column(TypeName = "money")]
        public decimal? Quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal? Rate { get; set; }

        public decimal? QuantityAdjustPercent { get; set; }

        public int? Tare { get; set; }

        public int? PalletNumber { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public bool? IsMetro { get; set; }

        public decimal? OriginalQuantity { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual ProductType ProductType { get; set; }

        public virtual QuantityType QuantityType { get; set; }

        public virtual ServiceType ServiceType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceItemReconciliation> InvoiceItemReconciliations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceItemReconciliation> InvoiceItemReconciliations1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceReconciliation> InvoiceReconciliations { get; set; }
    }
}
