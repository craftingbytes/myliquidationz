namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ProcessingReport
    {
        [StringLength(200)]
        public string AffiliateName { get; set; }

        [StringLength(200)]
        public string RecyclerAddress1 { get; set; }

        [StringLength(66)]
        public string RecyclerAddress2 { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public DateTime? ProcessingDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoiceDate { get; set; }

        public int? PlanYear { get; set; }

        [StringLength(50)]
        public string StateName { get; set; }

        [StringLength(1000)]
        public string Comments { get; set; }

        public int? InvoiceID { get; set; }

        [StringLength(50)]
        public string ShipmentNo { get; set; }

        [StringLength(250)]
        public string VendorCompanyName { get; set; }

        [StringLength(200)]
        public string VendorAddress { get; set; }

        [StringLength(50)]
        public string VendorCity { get; set; }

        [StringLength(50)]
        public string VendorStateProvinceName { get; set; }

        [StringLength(10)]
        public string VendorZip { get; set; }

        [StringLength(20)]
        public string VendorPhone { get; set; }

        [StringLength(200)]
        public string PickupAddress { get; set; }

        [StringLength(50)]
        public string ShipStateProvinceName { get; set; }

        [StringLength(50)]
        public string PickupCity { get; set; }

        [StringLength(250)]
        public string PickUpCompanyName { get; set; }

        [StringLength(10)]
        public string PickupZip { get; set; }

        [StringLength(20)]
        public string PickupPhone { get; set; }

        [StringLength(20)]
        public string PickupFax { get; set; }

        [StringLength(100)]
        public string PickupEmail { get; set; }

        [StringLength(200)]
        public string Client { get; set; }

        [StringLength(200)]
        public string RecievedBy { get; set; }

        public int? AffiliateID { get; set; }

        public int? ShipmentID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProcessingDataID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InvoiceItemID { get; set; }

        [StringLength(75)]
        public string ProductTypeName { get; set; }

        [StringLength(100)]
        public string ServiceTypeName { get; set; }

        public int? PalletNumber { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public bool? IsMetro { get; set; }

        [StringLength(80)]
        public string CollectionMethod { get; set; }

        [Key]
        [Column(Order = 2, TypeName = "money")]
        public decimal Qauntity { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Tare { get; set; }

        [Column(TypeName = "money")]
        public decimal? Total { get; set; }

        public decimal? OriginalQuantity { get; set; }

        [StringLength(50)]
        public string WeightCategory { get; set; }
    }
}
