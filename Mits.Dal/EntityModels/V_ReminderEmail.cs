namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ReminderEmail
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ReminderGroupId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AffiliateId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(1)]
        public string Name { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(1)]
        public string Email { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EventId { get; set; }

        [StringLength(300)]
        public string EventTitle { get; set; }

        [StringLength(2000)]
        public string EventDescription { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ReminderId { get; set; }

        [StringLength(50)]
        public string Number { get; set; }

        [StringLength(50)]
        public string Period { get; set; }

        public int? PeriodId { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        public DateTime? SendDate { get; set; }

        [StringLength(1500)]
        public string Emails { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SentReminderDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateBegin { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateEnd { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ActionDate { get; set; }

        public int? RecurrenceTypeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SingleRecurrenceValue { get; set; }

        public int? RecurrenceIntervalForDay { get; set; }

        public int? RecurrenceIntervalForMonth { get; set; }

        public int? RecurrenceIntervalQuaterly { get; set; }

        [Column(TypeName = "date")]
        public DateTime? RecurrenceIntervalYearly { get; set; }

        [StringLength(150)]
        public string AttachmentName { get; set; }

        [StringLength(150)]
        public string eType { get; set; }

        public DateTime? OccurranceDate { get; set; }
    }
}
