namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvoiceItemReconciliation")]
    public partial class InvoiceItemReconciliation
    {
        public int Id { get; set; }

        public int? ReconcileFromInvoiceItemID { get; set; }

        public int? ReconcileToInvoiceItemID { get; set; }

        public decimal? Quantity { get; set; }

        public virtual InvoiceItem InvoiceItem { get; set; }

        public virtual InvoiceItem InvoiceItem1 { get; set; }
    }
}
