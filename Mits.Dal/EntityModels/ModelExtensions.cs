﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data;
using System.Runtime.Serialization;
using System.Data.Objects.DataClasses;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Xml.Linq;
using Mits.DAL.Infrastructure;
using Mits.DAL.EntityModels;
using Mits.Common;
using System.Web.Mvc;
using System.Data.Entity;

namespace Mits.DAL.EntityModels
{
    public partial class MITSEntities : DbContext
    {
        public string UserName {
            get
            {
                ISessionCookieValues _sessionValues = (ISessionCookieValues)DependencyResolver.Current.GetService(typeof(ISessionCookieValues));
                if (_sessionValues != null)
                {
                    return _sessionValues.GetSessionValue(Constants.SessionParameters.AffiliateContactId);
                }

                return string.Empty;                
            }
        }
        List<DBAudit> AuditTrailList = new List<DBAudit>();

        //public enum AuditActions
        //{
        //    I,
        //    U,
        //    D
        //}

        //TODO: Need to get the new EF 6 way of hooking this event
        //partial void OnContextCreated()
        //{
        //    if (bool.Parse(ConfigurationManager.AppSettings["AuditTrail"]) == true)
        //        this.SavingChanges += new EventHandler(MITSEntities_SavingChanges);
        //}

        //TODO: This is the we we do auditing in EF 6 DbContext
        /*
        public override int SaveChanges()
        {
            var modifiedEntities = ChangeTracker.Entries()
                .Where(p => p.State == EntityState.Modified).ToList();
            var now = DateTime.UtcNow;

            foreach (var change in modifiedEntities)
            {
                var entityName = change.Entity.GetType().Name;
                var primaryKey = GetPrimaryKeyValue(change);

                foreach (var prop in change.OriginalValues.PropertyNames)
                {
                    var originalValue = change.OriginalValues[prop].ToString();
                    var currentValue = change.CurrentValues[prop].ToString();
                    if (originalValue != currentValue) //Only create a log if the value changes
                    {
                        //Create the Change Log
                    }
                }
            }
            return base.SaveChanges();
        }

        void MITSEntities_SavingChanges(object sender, EventArgs e)
        {
            AuditTrailList.Clear();
            IEnumerable<ObjectStateEntry> changes = this.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Deleted | EntityState.Modified);
            foreach (ObjectStateEntry stateEntryEntity in changes)
            {
                if (!stateEntryEntity.IsRelationship &&
                        stateEntryEntity.Entity != null &&
                            !(stateEntryEntity.Entity is DBAudit))
                {//is a normal entry, not a relationship, add audit in in-memory AuditTrailList
                    DBAudit audit = this.AuditTrailFactory(stateEntryEntity, UserName);
                    AuditTrailList.Add(audit);
                }
            }

            if (AuditTrailList.Count > 0)
            {
                foreach (var audit in AuditTrailList)
                {
                    // no need to add audit entry if both old and new data are null
                    if (audit.OldData == null && audit.NewData == null)
                        continue;
                    //add all audits to database 
                    this.DBAudits.Add(audit);
                }
            }            
        }

        private DBAudit AuditTrailFactory(ObjectStateEntry entry, string UserName)
        {
            DBAudit audit = new DBAudit();
            audit.AuditId = Guid.NewGuid().ToString();
            audit.RevisionStamp = DateTime.Now;
            audit.TableName = entry.EntitySet.Name;
            audit.UserName = UserName;

            if (entry.State == EntityState.Added)
            {//entry is Added                 
                //audit.Actions = AuditActions.I.ToString();
                //List<StringBuilder> auditValues = GetChanges(entry, true);
                //if (auditValues.Count == 1)
                //    audit.NewData = auditValues[0].ToString();                               
            }
            else if (entry.State == EntityState.Deleted)
            {//entry in deleted
                //audit.OldData = GetChanges(entry, true);
                //audit.Actions = AuditActions.D.ToString();
            }
            else
            {//entry is modified
                //audit.Actions = AuditActions.U.ToString();
                //List<StringBuilder> auditValues = GetChanges(entry, false);
                //if (auditValues.Count == 2)
                //{
                //    audit.OldData = auditValues[0].ToString();
                //    audit.NewData = auditValues[1].ToString();

                //}                
            }
            return audit;
        }

        private List<StringBuilder> GetChanges(ObjectStateEntry entry, bool added)
        {
            // oldValues variable is used to build xml for old values
            StringBuilder oldValues = new StringBuilder();
            oldValues.Append("<Fields>");
            // newValues variable is used to build xml for new values
            StringBuilder newValues = new StringBuilder();
            newValues.Append("<Fields>");
            // auditValues is used to return oldValues and newValues
            List<StringBuilder> auditValues = new List<StringBuilder>();
            bool isEntityChanged = false;
            if (entry.Entity is EntityObject)
            {
                if (added == true)
                {// if entry is added build only newValues xml
                    for (int i = 0; i < entry.CurrentValues.FieldCount; i++)
                    {
                        string propName = entry.CurrentValues.DataRecordInfo.FieldMetadata[i].FieldType.Name;
                        string nVal = entry.CurrentValues[i].ToString();
                        newValues.Append("<Field name='" + propName + "'>" + nVal + "</Field>");
                    }
                    newValues.Append("</Fields>");
                    auditValues.Add(newValues);
                }
                else
                {// if entry is modified build oldValues and newValues xml
                    string keyName = entry.CurrentValues.DataRecordInfo.FieldMetadata[0].FieldType.Name;
                    string keyVal = entry.CurrentValues[0].ToString();
                    oldValues.Append("<Field name='" + keyName + "'>" + keyVal + "</Field>");
                    newValues.Append("<Field name='" + keyName + "'>" + keyVal + "</Field>");

                    foreach (string propName in entry.GetModifiedProperties())
                    {
                        string oVal = entry.OriginalValues[propName].ToString();
                        string nVal = entry.CurrentValues[propName].ToString();
                        // only append changed values in xml
                        if (oVal != nVal)
                        {
                            oldValues.Append("<Field name='" + propName + "'>" + oVal + "</Field>");
                            newValues.Append("<Field name='" + propName + "'>" + nVal + "</Field>");
                            isEntityChanged = true;
                        }
                    }                    
                    if (isEntityChanged == true)
                    {
                        oldValues.Append("</Fields>");
                        newValues.Append("</Fields>");
                        auditValues.Add(oldValues);
                        auditValues.Add(newValues);
                    }
                }
            }
            return auditValues;
        }
        */
    }
}
