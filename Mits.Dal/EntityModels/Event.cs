namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Event")]
    public partial class Event
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Event()
        {
            ReminderGroups = new HashSet<ReminderGroup>();
        }

        public int Id { get; set; }

        [StringLength(300)]
        public string Title { get; set; }

        [StringLength(2000)]
        public string Description { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateBegin { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateEnd { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ActionDate { get; set; }

        public int? EventTypeId { get; set; }

        public int? RecurrenceTypeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SingleRecurrenceValue { get; set; }

        public int? RecurrenceIntervalForDay { get; set; }

        public int? RecurrenceIntervalForMonth { get; set; }

        public int? RecurrenceIntervalQuaterly { get; set; }

        [Column(TypeName = "date")]
        public DateTime? RecurrenceIntervalYearly { get; set; }

        public bool? IsTemplate { get; set; }

        [StringLength(300)]
        public string TemplateName { get; set; }

        public int? StateId { get; set; }

        public virtual EventType EventType { get; set; }

        public virtual RecurrenceType RecurrenceType { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReminderGroup> ReminderGroups { get; set; }
    }
}
