namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QuantityType")]
    public partial class QuantityType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QuantityType()
        {
            AffiliateContractRates = new HashSet<AffiliateContractRate>();
            AffiliateTargets = new HashSet<AffiliateTarget>();
            InvoiceItems = new HashSet<InvoiceItem>();
            ProductTypes = new HashSet<ProductType>();
            ProductTypes1 = new HashSet<ProductType>();
        }

        public int Id { get; set; }

        [StringLength(75)]
        public string Name { get; set; }

        [StringLength(10)]
        public string Abbrev { get; set; }

        public int? QuantityTypeGroupId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateContractRate> AffiliateContractRates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateTarget> AffiliateTargets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductType> ProductTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductType> ProductTypes1 { get; set; }

        public virtual QuantityTypeGroup QuantityTypeGroup { get; set; }
    }
}
