namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReminderGroupAffiliate")]
    public partial class ReminderGroupAffiliate
    {
        public int Id { get; set; }

        public int? ReminderGroupId { get; set; }

        public int? AffiliateId { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual ReminderGroup ReminderGroup { get; set; }
    }
}
