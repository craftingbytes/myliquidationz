namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvoiceReconciliation")]
    public partial class InvoiceReconciliation
    {
        public int Id { get; set; }

        public int OEMInvoiceId { get; set; }

        public int ProcesserInvoiceItemId { get; set; }

        public decimal? Quantity { get; set; }

        public DateTime? ReconcileDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? Rate { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual InvoiceItem InvoiceItem { get; set; }
    }
}
