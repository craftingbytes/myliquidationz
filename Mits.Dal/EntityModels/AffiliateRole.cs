namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateRole")]
    public partial class AffiliateRole
    {
        public int Id { get; set; }

        public int? AffiliateId { get; set; }

        public int? RoleId { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual Role Role { get; set; }
    }
}
