namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DocumentRelation")]
    public partial class DocumentRelation
    {
        public int Id { get; set; }

        public int DocumentID { get; set; }

        public int ForeignID { get; set; }

        public int ForeignTableID { get; set; }

        public virtual Document Document { get; set; }

        public virtual ForeignTable ForeignTable { get; set; }
    }
}
