namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Invoice")]
    public partial class Invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Invoice()
        {
            InvoiceAdjustments = new HashSet<InvoiceAdjustment>();
            InvoiceItems = new HashSet<InvoiceItem>();
            InvoiceReconciliations = new HashSet<InvoiceReconciliation>();
            InvoiceUnpackIds = new HashSet<InvoiceUnpackId>();
            InvoiceUnpackIds1 = new HashSet<InvoiceUnpackId>();
            PaymentDetails = new HashSet<PaymentDetail>();
            ProcessingDatas = new HashSet<ProcessingData>();
        }

        public int Id { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoiceDate { get; set; }

        public int? PlanYear { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoicePeriodFrom { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoicePeriodTo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoiceDueDate { get; set; }

        public bool? Receivable { get; set; }

        [StringLength(2048)]
        public string Notes { get; set; }

        public bool? ShowNotes { get; set; }

        public int? AffiliateId { get; set; }

        public int? StateId { get; set; }

        [StringLength(150)]
        public string RegionName { get; set; }

        public bool IsPaid { get; set; }

        public bool? Approved { get; set; }

        public bool Verified { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceAdjustment> InvoiceAdjustments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceReconciliation> InvoiceReconciliations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceUnpackId> InvoiceUnpackIds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceUnpackId> InvoiceUnpackIds1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentDetail> PaymentDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcessingData> ProcessingDatas { get; set; }
    }
}
