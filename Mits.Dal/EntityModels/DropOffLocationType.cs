namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DropOffLocationType")]
    public partial class DropOffLocationType
    {
        public int Id { get; set; }

        public int DropOffLocationId { get; set; }

        public int TypeId { get; set; }

        public virtual DropOffLocation DropOffLocation { get; set; }

        public virtual LocationType LocationType { get; set; }
    }
}
