namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AuditEvent")]
    public partial class AuditEvent
    {
        public int Id { get; set; }

        public int AuditActionId { get; set; }

        [StringLength(50)]
        public string Entity { get; set; }

        public int? EntityId { get; set; }

        public int AffiliateContactId { get; set; }

        public DateTime TimeStamp { get; set; }

        [StringLength(50)]
        public string IpAddress { get; set; }

        [StringLength(50)]
        public string Note { get; set; }

        public virtual AffiliateContact AffiliateContact { get; set; }

        public virtual AuditAction AuditAction { get; set; }
    }
}
