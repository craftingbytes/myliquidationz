namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerProductTypeManufacturer")]
    public partial class ConsumerProductTypeManufacturer
    {
        [Key]
        public int ManufacturerID { get; set; }

        public int AffiliateID { get; set; }

        [Required]
        [StringLength(75)]
        public string ManufacturerName { get; set; }

        public bool Verified { get; set; }
    }
}
