namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Reminder")]
    public partial class Reminder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Reminder()
        {
            ReminderAttachments = new HashSet<ReminderAttachment>();
        }

        public int Id { get; set; }

        public int? ReminderGroupId { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Number { get; set; }

        public int? PeriodId { get; set; }

        public int? StatusId { get; set; }

        public bool? ShowOnCalendar { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SendDate { get; set; }

        [StringLength(1500)]
        public string Emails { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SentReminderDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? OccurranceDate { get; set; }

        public virtual Period Period { get; set; }

        public virtual ReminderGroup ReminderGroup { get; set; }

        public virtual ReminderStatu ReminderStatu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReminderAttachment> ReminderAttachments { get; set; }
    }
}
