namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RecyclerProductWeight")]
    public partial class RecyclerProductWeight
    {
        public int Id { get; set; }

        public int RecyclerId { get; set; }

        [Required]
        [StringLength(4)]
        public string Year { get; set; }

        [Required]
        [StringLength(2)]
        public string Month { get; set; }

        public decimal TotalWeightReceived { get; set; }

        public decimal OnHand { get; set; }

        public decimal CRTGlass { get; set; }

        public decimal CircuitBoards { get; set; }

        public decimal Batteries { get; set; }

        public decimal OtherMaterials { get; set; }

        public virtual Affiliate Affiliate { get; set; }
    }
}
