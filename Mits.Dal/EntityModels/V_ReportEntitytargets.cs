namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ReportEntitytargets
    {
        [StringLength(50)]
        public string State { get; set; }

        [StringLength(200)]
        public string EntityName { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InvoiceID { get; set; }

        public decimal? InvoiceAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal? PaymentAmount { get; set; }

        public decimal? RemainingAmount { get; set; }

        [StringLength(10)]
        public string InvoiceDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? InvoiceDate1 { get; set; }

        [StringLength(30)]
        public string InvoiceMonthName { get; set; }

        public int? InvoiceMonth { get; set; }

        public int? InvoiceYear { get; set; }

        public int? PlanYear { get; set; }

        [StringLength(13)]
        public string InvoiceYearHalf { get; set; }

        [StringLength(9)]
        public string InvoiceQaurter { get; set; }

        [StringLength(10)]
        public string ProcessingDate { get; set; }

        [StringLength(10)]
        public string InvoiceDueDate { get; set; }

        [StringLength(10)]
        public string PaymentDate { get; set; }

        [StringLength(10)]
        public string InvoicePeriodFrom { get; set; }

        [StringLength(10)]
        public string InvoicePeriodTo { get; set; }

        public int? InvoiceItemCount { get; set; }

        public int? PaymentItemCount { get; set; }

        [StringLength(12)]
        public string PaidStatus { get; set; }

        public bool? Receivable { get; set; }

        public int? EntityId { get; set; }

        public bool? IsPaid { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StateId { get; set; }

        public int? Aging { get; set; }

        public int? EntityType { get; set; }

        public decimal? InvoiceWeight { get; set; }

        public decimal? RecAssignedWeight { get; set; }

        public decimal? RecUnAssignedWeight { get; set; }

        public decimal? RecWeight { get; set; }

        public decimal? InvRemainingWeight { get; set; }

        public decimal? TargetWeight { get; set; }

        [StringLength(17)]
        public string InvoiceStatus { get; set; }

        public int? QuantityTypeID { get; set; }

        public int? ServiceTypeID { get; set; }

        [StringLength(10)]
        public string TargetStartDate { get; set; }

        [StringLength(10)]
        public string TargetEndDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? TargetId { get; set; }

        public int? InvoiceCountPerTarget { get; set; }

        public decimal? InvAvgWeight { get; set; }

        public decimal? InvRemWeight { get; set; }

        [StringLength(24)]
        public string TargetRange { get; set; }

        public decimal? IndTargetWeight { get; set; }

        public decimal? IndRecWeight { get; set; }

        public decimal? RemIndTarget { get; set; }

        public decimal? StateAffilaiteTarget { get; set; }

        public decimal? StateAffilaiteRec { get; set; }

        public decimal? StateAffilaiteRem { get; set; }

        public decimal? StateTarget { get; set; }

        public decimal? StateRec { get; set; }

        public decimal? StateRem { get; set; }

        public decimal? AffilaiteStateTarget { get; set; }

        public decimal? AffilaiteStateRec { get; set; }

        public decimal? AffilaiteStateRem { get; set; }

        public decimal? AffilaiteTarget { get; set; }

        public decimal? AffilaiteRec { get; set; }

        public decimal? AffilaiteRem { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(22)]
        public string IndividualTargetStatus { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(22)]
        public string StateAffiliateTargetStatus { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(22)]
        public string StateTargetStatus { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(22)]
        public string AffiliateStateTargetStatus { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(22)]
        public string AffiliateTargetStatus { get; set; }
    }
}
