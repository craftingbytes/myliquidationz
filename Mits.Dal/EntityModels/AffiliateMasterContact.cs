namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateMasterContact")]
    public partial class AffiliateMasterContact
    {
        public int Id { get; set; }

        public int MasterAffiliateContactId { get; set; }

        public int AssociateContactId { get; set; }

        public bool DefaultUser { get; set; }

        public virtual AffiliateContact AffiliateContact { get; set; }

        public virtual AffiliateContact AffiliateContact1 { get; set; }
    }
}
