namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateAccreditation")]
    public partial class AffiliateAccreditation
    {
        public int Id { get; set; }

        public int? AffiliateId { get; set; }

        public int? AccreditationTypeId { get; set; }

        public bool? Active { get; set; }

        public virtual AccreditationType AccreditationType { get; set; }

        public virtual Affiliate Affiliate { get; set; }
    }
}
