namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvoiceUnpackId")]
    public partial class InvoiceUnpackId
    {
        public int Id { get; set; }

        public int? OEMInvoiceId { get; set; }

        public int? ProcesserInvoiceId { get; set; }

        [StringLength(50)]
        public string NewProcesserInvoiceId { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual Invoice Invoice1 { get; set; }
    }
}
