namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateTarget")]
    public partial class AffiliateTarget
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AffiliateTarget()
        {
            AffiliateTargetProducts = new HashSet<AffiliateTargetProduct>();
        }

        public int Id { get; set; }

        public int? StateId { get; set; }

        public int? PolicyId { get; set; }

        public int? ServiceTypeId { get; set; }

        public decimal? Weight { get; set; }

        public int? QuantityTypeId { get; set; }

        public DateTime? StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }

        public int? AffiliateId { get; set; }

        public int? PlanYear { get; set; }

        public bool? IsPermanentDropOffLocation { get; set; }

        public int? Pace { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual Policy Policy { get; set; }

        public virtual QuantityType QuantityType { get; set; }

        public virtual ServiceType ServiceType { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateTargetProduct> AffiliateTargetProducts { get; set; }
    }
}
