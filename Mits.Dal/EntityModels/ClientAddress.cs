namespace Mits.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ClientAddress")]
    public partial class ClientAddress
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClientAddress()
        {
            PickUpAddresses = new HashSet<PickUpAddress>();
        }

        public int Id { get; set; }

        public int AffiliateId { get; set; }

        [StringLength(250)]
        public string CompanyName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        public int? CityId { get; set; }

        public int? StateId { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual City City { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PickUpAddress> PickUpAddresses { get; set; }
    }
}
