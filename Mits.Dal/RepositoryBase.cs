﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Linq.Expressions;
using Mits.DAL.Interfaces;
using Mits.DAL.Infrastructure;
using Mits.DAL.EntityModels;
using System.Data.Entity;

namespace Mits.DAL
{
    public abstract class RepositoryBase<T> : IRepository<T>
        where T: class
    {
        IRepositoryContext _repositoryContext;

        public RepositoryBase()
            : this(new MitsRepositoryContext())
        {
        }

        public RepositoryBase(IRepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext ?? new MitsRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<T>();
        }

        private IDbSet<T> _objectSet;
        public IDbSet<T> ObjectSet
        {
            get
            {
                return _objectSet;
            }
        }

        #region IRepository Members

        public void Add(T entity)
        {
            this.ObjectSet.Add(entity);
            
            _repositoryContext.SaveChanges();
            
        }

        public void Delete(T entity)
        {
            this.ObjectSet.Remove(entity);
            _repositoryContext.SaveChanges();
        }

        public void Save(T entity)
        {
            _repositoryContext.SaveChanges();
        }

        public IList<T> GetAll()
        {
            return this.ObjectSet.ToList<T>();
        }

        public IList<T> GetAll(Expression<Func<T, bool>> whereCondition)
        {
            return this.ObjectSet.Where(whereCondition).ToList<T>();
        }

        public T GetSingle(Expression<Func<T, bool>> whereCondition)
        {
            return this.ObjectSet.Where(whereCondition).FirstOrDefault<T>();
        }


        public IQueryable<T> GetQueryable(Expression<Func<T, bool>> whereCondition)
        {
            return this.ObjectSet.Where(whereCondition).AsQueryable<T>();
        }

        public long Count()
        {
            return this.ObjectSet.LongCount<T>();
        }

        public long Count(Expression<Func<T, bool>> whereCondition)
        {
            return this.ObjectSet.Where(whereCondition).LongCount<T>();
        }


        /// <summary>
        /// Returns the active object context
        /// </summary>
        public DbContext ObjectContext
        {
            get
            {
                return _repositoryContext.ObjectContext;
            }
        }


    

        #endregion
    }
}
