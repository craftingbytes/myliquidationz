﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Linq.Expressions;
using Mits.DAL.Interfaces;
using System.Data.Objects;
using Mits.DAL.Infrastructure;

namespace Mits.DAL
{
    public class MITSRepository<T> : RepositoryBase<T>, IRepository<T> where T : class
    {
        public MITSRepository()
            : this(new MitsRepositoryContext())
        {
        }

        public MITSRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }

  
}
