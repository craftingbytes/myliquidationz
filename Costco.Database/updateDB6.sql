ALTER TABLE dbo.Affiliate ADD
	DefaultCarrier int NULL
	
ALTER TABLE dbo.Shipment ADD
	CarrierAffiliateId int NOT NULL CONSTRAINT DF_Shipment_CarrierAffiliateId DEFAULT 0
	
INSERT INTO [Affiliate]
           ([Name]
           ,[Address1]
           ,[Phone]
           ,[Longitude]
           ,[Latitude]
           ,[Active]
           ,[StateId]
           ,[CityId]
           ,[Zip]
           ,[AffiliateTypeId])
values
('Fed-Ex Economy',	
'1715 Aaron Brenner Drive, Suite 600',		
'760-599-0888',	
-89.855181,	
35.121290,
1,
50,	
41751,	
38120,	
6)

INSERT INTO [Affiliate]
           ([Name]
           ,[Address1]
           ,[Phone]
           ,[Longitude]
           ,[Latitude]
           ,[Active]
           ,[StateId]
           ,[CityId]
           ,[Zip]
           ,[AffiliateTypeId])
values(
'Apollo Southwest',	
'10365 Roselle Street Suite A',
'760-599-0888',
-117.202936,	
32.898114,
1,
6,
12946,	
92121,	
6)	

select * from Affiliate where AffiliateTypeId = 3

select * 
--update a set DefaultCarrier = (select Id from Affiliate where Name = 'Fed-Ex Economy' and AffiliateTypeId = 6)
from Affiliate a 
where AffiliateTypeId = 4 
and ParentId <> (Select Id from Affiliate where AffiliateTypeId = 3 and Name = 'SD & LA - Mira Loma Depot')

select * 
--update a set DefaultCarrier = (select Id from Affiliate where Name = 'Apollo Southwest' and AffiliateTypeId = 6)
from Affiliate a 
where AffiliateTypeId = 4 
and ParentId = (Select Id from Affiliate where AffiliateTypeId = 3 and Name = 'SD & LA - Mira Loma Depot')

