ALTER TABLE dbo.ShipmentItem ADD
	Disposition nvarchar(50) NULL
GO

ALTER TABLE dbo.BulkShipmentItem ADD
	Disposition nvarchar(50) NULL
GO