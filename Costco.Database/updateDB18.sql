
alter table SalesOrderItem add [Description] nvarchar(100) null
go

alter table SalesOrderBulkItem add [Description] nvarchar(100) null
go

	
alter View [dbo].[V_SalesOrderItemAll]
as
Select 
	'S' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.ShipmentId 
	,soi.ShipmentItemId
	,soi.Quantity
	,si.ItemId
	,coalesce(soi.Description, si.Description) Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
FROM SalesOrderItem soi
Inner Join ShipmentItem si on si.Id = soi.ShipmentItemId
Union All
Select 
	'B' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.BulkShipmentId 
	,soi.BulkShipmentItemId
	,soi.Quantity
	,si.ItemId
	,coalesce(soi.Description, si.Description) Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
FROM SalesOrderBulkItem soi
Inner Join BulkShipmentItem si on si.Id = soi.BulkShipmentItemId



CREATE TABLE [dbo].[ProductDescription](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[AffiliateContactId] [int] NULL,
 CONSTRAINT [PK_ProductDescription] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProductDescription]  WITH CHECK ADD  CONSTRAINT [FK_ProductDescription_AffiliateContact] FOREIGN KEY([AffiliateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO

ALTER TABLE [dbo].[ProductDescription] CHECK CONSTRAINT [FK_ProductDescription_AffiliateContact]
GO

ALTER TABLE [dbo].[ProductDescription]  WITH CHECK ADD  CONSTRAINT [FK_ProductDescription_Product] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Product] ([ItemId])
GO

ALTER TABLE [dbo].[ProductDescription] CHECK CONSTRAINT [FK_ProductDescription_Product]
GO

ALTER TABLE [dbo].[ProductDescription] ADD  CONSTRAINT [DF_ProductDescription_ItemId]  DEFAULT (N'Misc') FOR [ItemId]
GO

ALTER TABLE [dbo].[ProductDescription] ADD  CONSTRAINT [DF_ProductDescription_Timestamp]  DEFAULT (getdate()) FOR [Timestamp]
GO

