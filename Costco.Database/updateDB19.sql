
CREATE TABLE [dbo].[AuditAction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
 CONSTRAINT [PK_AuditAction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[AuditEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AuditActionId] [int] NOT NULL,
	[Entity] [nvarchar](50) NULL,
	[EntityId] [int] NULL,
	[AffiliateContactId] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[IpAddress] [nvarchar](50) NULL,
	[Note] [nvarchar](50) NULL,
 CONSTRAINT [PK_AuditEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AuditEvent]  WITH CHECK ADD  CONSTRAINT [FK_AuditEvent_AffiliateContact] FOREIGN KEY([AffiliateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO

ALTER TABLE [dbo].[AuditEvent] CHECK CONSTRAINT [FK_AuditEvent_AffiliateContact]
GO

ALTER TABLE [dbo].[AuditEvent]  WITH CHECK ADD  CONSTRAINT [FK_AuditEvent_AuditAction] FOREIGN KEY([AuditActionId])
REFERENCES [dbo].[AuditAction] ([Id])
GO

ALTER TABLE [dbo].[AuditEvent] CHECK CONSTRAINT [FK_AuditEvent_AuditAction]
GO


SET IDENTITY_INSERT [dbo].[AuditAction] ON
INSERT [dbo].[AuditAction] ([Id], [Name], [Description]) VALUES (1, N'Insert', N'Insert Record')
INSERT [dbo].[AuditAction] ([Id], [Name], [Description]) VALUES (2, N'Update', N'Update Record')
INSERT [dbo].[AuditAction] ([Id], [Name], [Description]) VALUES (3, N'Delete', N'Delete Record')
INSERT [dbo].[AuditAction] ([Id], [Name], [Description]) VALUES (4, N'Read', N'View Area')
INSERT [dbo].[AuditAction] ([Id], [Name], [Description]) VALUES (5, N'Login', N'Logged Into System')
SET IDENTITY_INSERT [dbo].[AuditAction] OFF


