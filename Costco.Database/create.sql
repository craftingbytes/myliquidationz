/****** Object:  Table [dbo].[SiteImage]    Script Date: 11/17/2011 14:59:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SiteImage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Path] [nvarchar](2048) NULL,
 CONSTRAINT [SiteImage_Id_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds the paths for the database images.  These are used throughout the site so usage requires only a link to the id.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SiteImage'
GO
/****** Object:  Table [dbo].[ForeignTable]    Script Date: 11/17/2011 14:59:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ForeignTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ForeignTableName] [varchar](255) NOT NULL,
 CONSTRAINT [ForeignTable_Id_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'In tables where relationshiips may refer to more then one table this keeps track or which table an id is linked to.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ForeignTable'
GO
/****** Object:  Table [dbo].[DocumentType]    Script Date: 11/17/2011 14:59:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SiteImageID] [int] NULL,
	[FileExtension] [varchar](10) NULL,
	[DocumentTypeName] [varchar](100) NULL,
	[ContentType] [varchar](100) NULL,
	[IsUploadable] [bit] NOT NULL,
	[IsImage] [bit] NOT NULL,
	[EscapeBeforeDisplay] [bit] NOT NULL,
 CONSTRAINT [DocumentType_Id_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Document]    Script Date: 11/17/2011 14:59:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Document](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentTypeId] [int] NULL,
	[AffiliateContactId] [int] NULL,
	[ServerFileName] [varchar](2048) NULL,
	[UploadFileName] [varchar](2048) NULL,
	[UploadDate] [smalldatetime] NULL,
	[DeleteDate] [smalldatetime] NULL,
 CONSTRAINT [Document_DocumentationID_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path and filename a browser will use to access the file.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Document', @level2type=N'COLUMN',@level2name=N'ServerFileName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Files uploaded by users to the server.  Most oftern supporting documentation.  Is linked to data via DocumentRelation.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Document'
GO
/****** Object:  Table [dbo].[DocumentRelation]    Script Date: 11/17/2011 14:59:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentRelation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentID] [int] NOT NULL,
	[ForeignID] [int] NOT NULL,
	[ForeignTableID] [int] NOT NULL,
 CONSTRAINT [PK_DocumentRelation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [DocumentRelation_CK] UNIQUE NONCLUSTERED 
(
	[DocumentID] ASC,
	[ForeignID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tracks the relationship between files uploaded by users to the server and the database item it pertains to - most often Invoices or Shipments.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DocumentRelation'
GO
/****** Object:  Default [DF__Document__Docum__30E33A54]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[Document] ADD  CONSTRAINT [DF__Document__Docum__30E33A54]  DEFAULT ((0)) FOR [DocumentTypeId]
GO
/****** Object:  Default [Document_UploadDate_DF]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[Document] ADD  CONSTRAINT [Document_UploadDate_DF]  DEFAULT (getdate()) FOR [UploadDate]
GO
/****** Object:  Default [DF__DocumentT__IsUpl__5006DFF2]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[DocumentType] ADD  CONSTRAINT [DF__DocumentT__IsUpl__5006DFF2]  DEFAULT ((0)) FOR [IsUploadable]
GO
/****** Object:  Default [DF__DocumentT__IsIma__50FB042B]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[DocumentType] ADD  CONSTRAINT [DF__DocumentT__IsIma__50FB042B]  DEFAULT ((0)) FOR [IsImage]
GO
/****** Object:  Default [DF__DocumentT__Escap__51EF2864]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[DocumentType] ADD  CONSTRAINT [DF__DocumentT__Escap__51EF2864]  DEFAULT ((1)) FOR [EscapeBeforeDisplay]
GO
/****** Object:  ForeignKey [FK_Document_AffiliateContact]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_AffiliateContact] FOREIGN KEY([AffiliateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_AffiliateContact]
GO
/****** Object:  ForeignKey [FK_Document_DocumentType]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_DocumentType] FOREIGN KEY([DocumentTypeId])
REFERENCES [dbo].[DocumentType] ([Id])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_DocumentType]
GO
/****** Object:  ForeignKey [FK_DocumentRelation_Document]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[DocumentRelation]  WITH CHECK ADD  CONSTRAINT [FK_DocumentRelation_Document] FOREIGN KEY([DocumentID])
REFERENCES [dbo].[Document] ([Id])
GO
ALTER TABLE [dbo].[DocumentRelation] CHECK CONSTRAINT [FK_DocumentRelation_Document]
GO
/****** Object:  ForeignKey [FK_DocumentRelation_ForeignTable]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[DocumentRelation]  WITH CHECK ADD  CONSTRAINT [FK_DocumentRelation_ForeignTable] FOREIGN KEY([ForeignTableID])
REFERENCES [dbo].[ForeignTable] ([Id])
GO
ALTER TABLE [dbo].[DocumentRelation] CHECK CONSTRAINT [FK_DocumentRelation_ForeignTable]
GO
/****** Object:  ForeignKey [FK_DocumentType_DocumentType]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[DocumentType]  WITH CHECK ADD  CONSTRAINT [FK_DocumentType_DocumentType] FOREIGN KEY([Id])
REFERENCES [dbo].[DocumentType] ([Id])
GO
ALTER TABLE [dbo].[DocumentType] CHECK CONSTRAINT [FK_DocumentType_DocumentType]
GO
/****** Object:  ForeignKey [FK_DocumentType_SiteImage]    Script Date: 11/17/2011 14:59:20 ******/
ALTER TABLE [dbo].[DocumentType]  WITH CHECK ADD  CONSTRAINT [FK_DocumentType_SiteImage] FOREIGN KEY([SiteImageID])
REFERENCES [dbo].[SiteImage] ([Id])
GO
ALTER TABLE [dbo].[DocumentType] CHECK CONSTRAINT [FK_DocumentType_SiteImage]
GO
