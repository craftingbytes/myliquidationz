/****** Object:  Table [dbo].[ConditionType]    Script Date: 11/21/2011 17:55:40 ******/
SET IDENTITY_INSERT [dbo].[ConditionType] ON
INSERT [dbo].[ConditionType] ([Id], [Condition]) VALUES (1, N'Like New')
INSERT [dbo].[ConditionType] ([Id], [Condition]) VALUES (2, N'Used')
INSERT [dbo].[ConditionType] ([Id], [Condition]) VALUES (3, N'Repairable')
INSERT [dbo].[ConditionType] ([Id], [Condition]) VALUES (4, N'Scratch and Dent')
INSERT [dbo].[ConditionType] ([Id], [Condition]) VALUES (5, N'Damaged(parts)')
INSERT [dbo].[ConditionType] ([Id], [Condition]) VALUES (6, N'Destroyed')
INSERT [dbo].[ConditionType] ([Id], [Condition]) VALUES (7, N'Other')
SET IDENTITY_INSERT [dbo].[ConditionType] OFF
