ALTER TABLE dbo.SalesOrder ADD
	MinValue money NULL,
	Invoiced bit NOT NULL CONSTRAINT DF_SalesOrder_Invoiced DEFAULT 0
GO