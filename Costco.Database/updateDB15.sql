alter table SalesOrderItem add
	Comment nvarchar(200) null,
	Price money null
go

alter table SalesOrderBulkItem add
	Comment nvarchar(200) null,
	Price money null
go
	
	
Alter View [dbo].[V_SalesOrderItemAll]
as
Select 
	'S' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.ShipmentId 
	,soi.ShipmentItemId
	,soi.Quantity
	,si.ItemId
	,si.Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
FROM SalesOrderItem soi
Inner Join ShipmentItem si on si.Id = soi.ShipmentItemId
Union All
Select 
	'B' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.BulkShipmentId 
	,soi.BulkShipmentItemId
	,soi.Quantity
	,si.ItemId
	,si.Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
FROM SalesOrderBulkItem soi
Inner Join BulkShipmentItem si on si.Id = soi.BulkShipmentItemId

GO


select soi.*
-- update soi set price = coalesce(p.price,0)
from SalesOrderItem soi
inner join ShipmentItem si on si.Id = soi.ShipmentId
left join v_MaxProductPrice p on p.ItemId = si.ItemId
where soi.Price is null 
and len(LTRIM(RTRIM(si.ItemId))) in (5,6)
and ISNUMERIC(LTRIM(RTRIM(si.ItemId))) <> 0
go

select soi.*
-- update soi set price = coalesce(p.price,0)
from SalesOrderBulkItem soi
inner join BulkShipmentItem si on si.Id = soi.BulkShipmentItemId
left join v_MaxProductPrice p on p.ItemId = si.ItemId
where soi.Price is null 
and len(LTRIM(RTRIM(si.ItemId))) in (5,6)
and ISNUMERIC(LTRIM(RTRIM(si.ItemId))) <> 0
go