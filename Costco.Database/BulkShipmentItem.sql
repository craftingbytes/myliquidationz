
/****** Object:  Table [dbo].[BulkShipmentItem]    Script Date: 01/24/2012 14:40:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BulkShipmentItem](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[BulkShipmentId] [int] NOT NULL,
	[Store] [int] NOT NULL,
	[ItemId] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Qty] [int] NOT NULL,
	[ReceivedCondition] [int] NOT NULL,
	[CoveredDevice] [bit] NULL,
 CONSTRAINT [PK_BulkShipmentItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BulkShipmentItem]  WITH CHECK ADD  CONSTRAINT [FK_BulkShipmentItem_BulkShipment] FOREIGN KEY([BulkShipmentId])
REFERENCES [dbo].[BulkShipment] ([Id])
GO

ALTER TABLE [dbo].[BulkShipmentItem] CHECK CONSTRAINT [FK_BulkShipmentItem_BulkShipment]
GO

ALTER TABLE [dbo].[BulkShipmentItem]  WITH CHECK ADD  CONSTRAINT [FK_BulkShipmentItem_ConditionType] FOREIGN KEY([ReceivedCondition])
REFERENCES [dbo].[ConditionType] ([Id])
GO

ALTER TABLE [dbo].[BulkShipmentItem] CHECK CONSTRAINT [FK_BulkShipmentItem_ConditionType]
GO


