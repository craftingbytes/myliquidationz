ALTER TABLE dbo.ShipmentItem ADD
	Sold int NOT NULL CONSTRAINT DF_ShipmentItem_Sold DEFAULT 0
GO

ALTER TABLE dbo.BulkShipmentItem ADD
	Sold int NOT NULL CONSTRAINT DF_BulkShipmentItem_Sold DEFAULT 0
GO


SELECT *
--update si SET sold = soi.Quantity
FROM ShipmentItem si
INNER Join (
SELECT ShipmentItemId, SUM(Quantity) Quantity
FROM SalesOrderItem soi
GROUP BY soi.ShipmentItemId   ) soi ON soi.ShipmentItemId    = si.Id



SELECT *
--update si SET sold = soi.Quantity
FROM BulkShipmentItem si
INNER Join (
SELECT BulkShipmentItemId, SUM(Quantity) Quantity
FROM SalesOrderBulkItem soi
GROUP BY soi.BulkShipmentItemId   ) soi ON soi.BulkShipmentItemId    = si.Id


SELECT 
    coalesce(si.ReceivedQuantity,0), si.pulled, si.Sold, coalesce(si.ReceivedQuantity,0) - si.pulled - si.Sold ,vsi.Available
FROM ShipmentItem si
INNER JOIN V_ShipmentInventory vsi ON vsi.ShipmentItemId = si.Id
WHERE   coalesce(si.ReceivedQuantity,0) - si.pulled - si.Sold < 0
OR	coalesce(si.ReceivedQuantity,0) - si.pulled - si.Sold <> vsi.Available


SELECT 
    coalesce(si.Qty,0), si.pulled, si.Sold, coalesce(si.Qty,0) - si.pulled - si.Sold ,vsi.Available
FROM BulkShipmentItem si
INNER JOIN V_BulkShipmentInventory vsi ON vsi.ShipmentItemId = si.Id
WHERE   coalesce(si.Qty,0) - si.pulled - si.Sold < 0
OR	coalesce(si.Qty,0) - si.pulled - si.Sold <> vsi.Available


ALTER View [dbo].[V_ShipmentInventory]
as 
Select 
si.ShipmentId,
Id ShipmentItemId,
ItemId,
Description,
Coalesce(ReceivedQuantity,0) Quantity,
Coalesce(Sold,0) Allocated,
Coalesce(Pulled,0) Pulled,
Coalesce(ReceivedQuantity,0) - Coalesce(Sold,0) - Coalesce(Pulled,0) Available
from ShipmentItem si  (nolock)
--left join 
--(select 
--	ShipmentId, 
--	ShipmentItemId, 
--	SUM(Quantity) Allocated
--from
--SalesOrderItem  (nolock)
--group by 
--	ShipmentId, 
--	ShipmentItemId) al on al.ShipmentItemId = si.Id
where si.Disposition = 'Salvage'
or (si.Disposition = 'Recycle' and (si.description like '%tv%' or si.description like '%television%' or si.description like '%monitor%'	 ))

ALTER View [dbo].[V_BulkShipmentInventory]
as 
Select 
si.BulkShipmentId ShipmentId,
Id ShipmentItemId,
ItemId,
Description,
Coalesce(Qty,0) Quantity,
Coalesce(Sold,0) Allocated,
Coalesce(Pulled,0) Pulled,
Coalesce(Qty,0) - Coalesce(Sold,0) - Coalesce(Pulled,0) Available
from BulkShipmentItem si  (nolock)
--left join 
--(select 
--	BulkShipmentId, 
--	BulkShipmentItemId, 
--	SUM(Quantity) Allocated
--from
--SalesOrderBulkItem  (nolock)
--group by 
--	BulkShipmentId, 
--	BulkShipmentItemId) al   on al.BulkShipmentItemId = si.Id
where si.Disposition <> 'Recycle'	
or (si.Disposition = 'Recycle' and (si.description like '%tv%' or si.description like '%television%' or si.description like '%monitor%'	 ))

go


ALTER View [dbo].[V_BulkShipmentInventory]
as 
Select 
si.BulkShipmentId ShipmentId,
Id ShipmentItemId,
ItemId,
Description,
Coalesce(Qty,0) Quantity,
Coalesce(Sold,0) Allocated,
Coalesce(Pulled,0) Pulled,
Coalesce(Qty,0) - Coalesce(Sold,0) - Coalesce(Pulled,0) Available
from BulkShipmentItem si  (nolock)
--left join 
--(select 
--	BulkShipmentId, 
--	BulkShipmentItemId, 
--	SUM(Quantity) Allocated
--from
--SalesOrderBulkItem  (nolock)
--group by 
--	BulkShipmentId, 
--	BulkShipmentItemId) al   on al.BulkShipmentItemId = si.Id
where si.Disposition <> 'Recycle'	
or (si.Disposition = 'Recycle' and (si.description like '%tv%' or si.description like '%television%' or si.description like '%monitor%'	 ))



GO


-- =============================================
-- Author:		Jei Jacinto
-- Create date: 09/13/2013
-- Description: Update Sold Column
-- =============================================
create PROCEDURE p_UpdateShipmentItemSold 
	@ShipmentItemId as int
AS
BEGIN
    SET NOCOUNT ON;
    
    UPDATE si SET sold =  coalesce(soi.Quantity,0)
    FROM ShipmentItem si
    Left Join (
    SELECT ShipmentItemId, SUM(Quantity) Quantity
    FROM SalesOrderItem soi
    WHERE ShipmentItemId = @ShipmentItemId
    GROUP BY soi.ShipmentItemId   ) soi ON soi.ShipmentItemId    = si.Id
    WHERE si.Id = @ShipmentItemId
    
END
GO

-- =============================================
-- Author:		Jei Jacinto
-- Create date: 09/13/2013
-- Description: Update Sold Column
-- =============================================
create PROCEDURE p_UpdateBulkShipmentItemSold 
	@BulkShipmentItemId as int
AS
BEGIN
    SET NOCOUNT ON;
    
    UPDATE si SET sold =  coalesce(soi.Quantity,0)
    FROM BulkShipmentItem si
    Left Join (
    SELECT BulkShipmentItemId, SUM(Quantity) Quantity
    FROM SalesOrderBulkItem soi
    WHERE BulkShipmentItemId = @BulkShipmentItemId
    GROUP BY soi.BulkShipmentItemId   ) soi ON soi.BulkShipmentItemId    = si.Id
    WHERE si.Id = @BulkShipmentItemId
    
END
GO



alter View [dbo].[V_ShipmentAvailableToSellAll]
as
Select 
    
	'S' Type
	,s.Id
	,S.ReceivedDate
	,s.ShipToAffiliateId 
From Shipment s (nolock)
where  ReceivedDate is not null
and exists (select 0x0 from V_ShipmentInventory si (nolock) where si.ShipmentId = s.Id and si.Available >= 1) 

union all
Select 
    
	'B' Type
	,s.Id
	,S.ReceivedDate
	,s.ShipToAffiliateId 
From BulkShipment s (nolock)
where  ReceivedDate is not null
and exists (select 0x0 from V_BulkShipmentInventory si  (nolock) where si.ShipmentId = s.Id and si.Available >= 1)
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create TRIGGER [dbo].[tr_SalesOrderItem] 
   ON  [dbo].[SalesOrderItem]
   AFTER  INSERT,DELETE,UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;
	Declare @ShipmentItemId int
	
	Select @ShipmentItemId = ShipmentItemId from deleted
	
	if @ShipmentItemId is null
	begin
	   Select @ShipmentItemId = ShipmentItemId from inserted 
	end 
	
     exec p_UpdateShipmentItemSold @ShipmentItemid
	

END


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create TRIGGER [dbo].[tr_SalesOrderBulkItem] 
   ON  [dbo].[SalesOrderBulkItem]
   AFTER  INSERT,DELETE,UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;
	Declare @BulkShipmentItemId int
	
	Select @BulkShipmentItemId = BulkShipmentItemId from deleted
	
	if @BulkShipmentItemId is null
	begin
	   Select @BulkShipmentItemId = BulkShipmentItemId from inserted 
	end 
	
     exec p_UpdateBulkShipmentItemSold @BulkShipmentItemid
	

END
