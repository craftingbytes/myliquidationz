

/****** Object:  Table [dbo].[BulkShipment]    Script Date: 01/24/2012 14:40:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BulkShipment](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[ShipToAffiliateId] [int] NOT NULL,
	[ReceivedBy] [nvarchar](100) NULL,
	[ReceivingComments] [nvarchar](1000) NULL,
	[ShipmentTotalWeight] [int] NULL,
	[ReceivedDate] [datetime] NULL,
	[ReceivedTotalWeight] [int] NULL,
 CONSTRAINT [PK_BulkShipment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BulkShipment]  WITH CHECK ADD  CONSTRAINT [FK_BulkShipment_Affiliate] FOREIGN KEY([ShipToAffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[BulkShipment] CHECK CONSTRAINT [FK_BulkShipment_Affiliate]
GO


