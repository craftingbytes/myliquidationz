ALTER TABLE dbo.SalesOrderItem ADD
	SellingPrice money NULL
GO

ALTER TABLE dbo.SalesOrderBulkItem ADD
	SellingPrice money NULL
GO

ALTER table dbo.SalesOrder ADD
    InitialPercentage decimal(18,2) NULL
go


ALTER TABLE dbo.ShipmentItem ADD
	ShipperPrice money NULL
GO


CREATE PROCEDURE [dbo].[p_UpdateSalesOrderSoldAmount] 
	@SalesOrderId as int
AS
BEGIN
    SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;
    
    Declare @SoldAmount money
    Declare @SoldBulkAmount money
    
    select @SoldAmount = Sum(soi.sellingPrice * soi.Quantity)
    from SalesOrderItem soi
    where soi.SalesOrderId =	@SalesOrderId  
    
    select @SoldBulkAmount = Sum(soi.sellingPrice * soi.Quantity)
    from SalesOrderBulkItem soi
    where soi.SalesOrderId =	@SalesOrderId  
    
    if @SoldAmount is not null or @SoldBulkAmount is not null
    begin
	   update SalesOrder
	   set SoldAmount = coalesce(@SoldAmount,0) + coalesce(@SoldBulkAmount,0),
		  MinValue = coalesce(@SoldAmount,0) + coalesce(@SoldBulkAmount,0)
	   where id = @SalesOrderId
    end
    select SoldAmount 
    from  SalesOrder
    where id = @SalesOrderId
    
END


GO

ALTER view [dbo].[V_ShipmentItemExtended]   as
SELECT 
	si.[Id]
    ,si.[ShipmentId]
	,si.[PalletUnitNumber]
	,si.[ItemId]
	,si.[Description]
	,si.[Condition]
	,si.[Quantity]
	,si.[ReceivedQuantity]
	,coalesce(c.Condition,'')  [ReceivedCondition]
	,si.[CoveredDevice]
	,si.[ReceivedCondition2]
	,si.[Disposition]
	,si.[Weight]
	,si.[Pulled]
	,p.Description DefaultDescription
	,coalesce(pp.Price,0) Price
	,si.Store
	,si.ShipperPrice ShipperPrice
FROM [ShipmentItem] si
Left Join ConditionType	 c on c.Id = si.ReceivedCondition
INNER JOIN Product  p on p.ItemId = si.ItemId
LEFT JOIN (
select
	ItemId
	,Max(Id) Id
from ProductPrice 
group by ItemId) mp on mp.ItemId = p.ItemId
left join ProductPrice pp on pp.Id = mp.Id and pp.ItemId = si.ItemId		  

GO




ALTER View [dbo].[V_SalesOrderItemAll]
as
Select 
	'S' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.ShipmentId 
	,soi.ShipmentItemId
	,soi.Quantity
	,si.ItemId
	,coalesce(soi.Description, si.Description) Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
	,coalesce(soi.SellingPrice, 0.0) SellingPrice
FROM SalesOrderItem soi
Inner Join ShipmentItem si on si.Id = soi.ShipmentItemId
Union All
Select 
	'B' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.BulkShipmentId 
	,soi.BulkShipmentItemId
	,soi.Quantity
	,si.ItemId
	,coalesce(soi.Description, si.Description) Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
	,coalesce(soi.SellingPrice, 0.0) SellingPrice
FROM SalesOrderBulkItem soi
Inner Join BulkShipmentItem si on si.Id = soi.BulkShipmentItemId


GO


SELECT s.Id
    , s.soldamount
    , sum(vsoia.price) total	
    , cast(round(s.soldamount / sum(vsoia.price)	 , 4)  * 100 AS decimal(18,2)) initialPercent
    INTO tempSalesOrders
from	salesorder  s
inner join V_SalesOrderItemAll vsoia   ON vsoia.SalesOrderId = s.Id
WHERE s.Available = 1 AND s.SoldDate IS NOT NULL
AND s.InitialPercentage IS null
GROUP BY s.id, s.soldamount

go

UPDATE s
SET	initialPercentage = ts.InitialPercent
from SalesOrder s
INNER join tempSalesOrders  tS on ts.Id = s.Id
go


update soi
set sellingPrice = round( price *  (s.initialPercent * .01)	, 2)
from salesOrderItem	 soi
INNER join tempSalesOrders s ON s.Id = soi.SalesOrderId
WHERE soi.SellingPrice IS NULL 
go

update soi
set sellingPrice = round( price *  (s.initialPercent * .01)	, 2)
from salesOrderBulkItem	 soi
INNER join tempSalesOrders s ON s.Id = soi.SalesOrderId
WHERE soi.SellingPrice IS NULL 
go

SELECT s.Id
    , s.soldamount
    , sum(vsoia.SellingPrice) sellingPrice  
from	salesorder  s
inner join V_SalesOrderItemAll vsoia   ON vsoia.SalesOrderId = s.Id
WHERE s.Available = 1 AND s.SoldDate IS NOT NULL
GROUP BY s.id, s.soldamount
HAVING   sum(vsoia.SellingPrice) - s.soldamount   < -1
