
CREATE TABLE [dbo].[UserReportList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AffiliateContactId] [int] NOT NULL,
	[ReportId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[SequenceId] [int] NOT NULL,
	[Notes] [nvarchar](250) NULL,
 CONSTRAINT [PK_UserReportList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserReportList]  WITH CHECK ADD  CONSTRAINT [FK_UserReportList_AffiliateContact] FOREIGN KEY([AffiliateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO

ALTER TABLE [dbo].[UserReportList] CHECK CONSTRAINT [FK_UserReportList_AffiliateContact]
GO

ALTER TABLE [dbo].[UserReportList]  WITH CHECK ADD  CONSTRAINT [FK_UserReportList_Area] FOREIGN KEY([ReportId])
REFERENCES [dbo].[Area] ([Id])
GO

ALTER TABLE [dbo].[UserReportList] CHECK CONSTRAINT [FK_UserReportList_Area]
GO


CREATE PROCEDURE [dbo].[p_GetUserReportList]
    @AffiliateContactId as int
AS 

    WITH MainGroup AS ( 
    SELECT  ROW_NUMBER() OVER (ORDER BY a.[Order]) AS RowNumber ,
    a.* ,
    ac.Id AffiliateContactId
    FROM AffiliateContact ac
    INNER JOIN Role r ON r.Id = ac.RoleId
    INNER JOIN Permissions p ON p.RoleId = ac.RoleId
    INNER JOIN Area a ON a.Id = p.AreaId 
    WHERE p.[Read] = 1
    AND a.ParentId = 26
    AND a.IsMenu = 1 
    AND a.IsArea = 1	
    AND ac.Id =  @AffiliateContactId 
    ),
    Group1 AS (
    SELECT 
  ROW_NUMBER() OVER (ORDER BY coalesce(ul.SequenceId,999) ) AS RowNumber,
  Coalesce(ul.Id,0) Id,	
  Coalesce(ul.SequenceId,0) SequenceId,									    
  coalesce(a.Caption,'') Caption,
  coalesce(a.URL,'#') URL,
  coalesce(a.Id,0) ReportId  
    FROM	 MainGroup mg
    LEFT JOIN UserReportList ul ON ul.AffiliateContactId = mg.AffiliateContactId 
							 AND ul.ReportId = mg.Id 
							 AND ul.GroupId = 1
    LEFT JOIN Area a ON a.Id = ul.ReportId 
    )
    ,
    Group2 AS (
    SELECT 
  ROW_NUMBER() OVER (ORDER BY coalesce(ul.SequenceId,999)) AS RowNumber,
  Coalesce(ul.Id,0) Id,	
  Coalesce(ul.SequenceId,0) SequenceId,									    
  coalesce(a.Caption,'') Caption,
  coalesce(a.URL,'#') URL,
  coalesce(a.Id,0) ReportId  
    FROM	 MainGroup mg
    LEFT JOIN UserReportList ul ON ul.AffiliateContactId = mg.AffiliateContactId 
							 AND ul.ReportId = mg.Id 
							 AND ul.GroupId = 2
    LEFT JOIN Area a ON a.Id = ul.ReportId 
    )
    ,
    Group3 AS (
    SELECT 
  ROW_NUMBER() OVER (ORDER BY coalesce(ul.SequenceId,999)) AS RowNumber,
  Coalesce(ul.Id,0) Id,	
  Coalesce(ul.SequenceId,0) SequenceId,									    
  coalesce(a.Caption,'') Caption,
  coalesce(a.URL,'#') URL,
  coalesce(a.Id,0) ReportId  
    FROM	 MainGroup mg
    LEFT JOIN UserReportList ul ON ul.AffiliateContactId = mg.AffiliateContactId 
							 AND ul.ReportId = mg.Id 
							 AND ul.GroupId = 3
    LEFT JOIN Area a ON a.Id = ul.ReportId 
    )


    SELECT
	   g1.Id Id_1,
	   g1.SequenceId SequenceId_1,
	   g1.Caption Caption_1,
	   g1.URL URL_1,
	   g2.Id Id_2,
	   g2.SequenceId SequenceId_2,
	   g2.Caption Caption_2,
	   g2.URL URL_2,
	   g3.Id Id_3,
	   g3.SequenceId SequenceId_3,
	   g3.Caption Caption_3,
	   g3.URL URL_3,
	   mg.Id Id_m,
	   mg.Caption Caption_m,
	   mg.URL URL_m,
	   mg.AffiliateContactId,
	   CASE
		  When g11.ReportId IS NOT NULL 
				or g22.ReportId IS NOT NULL
				or g33.ReportId IS NOT NULL 
		  THEN 1
		  ELSE 0
	   END Assigned
    FROM MainGroup mg 
    INNER JOIN Group1 g1 ON g1.RowNumber = mg.RowNumber
    INNER join Group2 g2 ON g2.RowNumber = mg.RowNumber
    INNER join Group3 g3 ON g3.RowNumber = mg.RowNumber
    LEFT JOIN Group1 g11 ON g11.ReportId = mg.Id
    LEFT JOIN Group2 g22 ON g22.ReportId = mg.Id
    LEFT JOIN Group3 g33 ON g33.ReportId = mg.Id
    ORDER BY mg.RowNumber

 
GO



CREATE PROCEDURE [dbo].[p_ChangeDisplayOrder]
	-- Add the parameters for the stored procedure here
	@UserReportListId BIGINT,
	@Direction int = 0 -- 0=down, 1=up
AS
BEGIN
    set nocount on
	Declare	@GroupId Int, 
			@CurrentDisplayOrder int,
			@Down int = 0,
			@Up int = 1
	Declare	@tblSequence Table(Id int,  DisplayOrder int)
	
	
	select	@GroupId = url.GroupId, 
			@CurrentDisplayOrder = url.SequenceId
	from UserReportList url 
	where  url.Id = @UserReportListId
    
    if (@Direction = @Down)
	begin
		insert into @tblSequence values(@UserReportListId,@CurrentDisplayOrder + 1)
		
		insert into @tblSequence
		select Id, SequenceId - 1
		from UserReportList url 
		where GroupId = @GroupId 
		and SequenceId = @CurrentDisplayOrder + 1
	end
	else if (@Direction = @Up)
	BEGIN
	     insert into @tblSequence values(@UserReportListId,@CurrentDisplayOrder - 1)
		
		insert into @tblSequence
		select Id, SequenceId + 1
		from UserReportList url 
		where GroupId = @GroupId 
		and SequenceId = @CurrentDisplayOrder - 1
	end
	
	if ((select COUNT(1) from @tblSequence) = 2)
	begin
		update f 
		set SequenceId = s.DisplayOrder
		from UserReportList f
		inner join @tblSequence s on s.Id = f.Id
	end
	set nocount off
	
END

GO




CREATE PROCEDURE [dbo].[p_InsertUserReport]
	@AffiliateContactId int,
	@ReportId int,
     @GroupId int
AS
	Declare @DisplayOrder as int

	select @DisplayOrder  = Coalesce(MAX(ul.SequenceId),0) + 1
	from UserReportList	 ul
	where ul.AffiliateContactId = @AffiliateContactId
	AND ul.GroupId = @GroupId
	
	Insert into UserReportList (
	   AffiliateContactId,
	   ReportId,
	   GroupId,
	   SequenceId
	)
	SELECT 
	   @AffiliateContactId,
	   a.Id,
	   @GroupId,
	   @DisplayOrder
	FROM Area a
	Inner Join Permissions p on p.AreaId = a.Id
	INNER JOIN Role r ON r.id = p.RoleId
	INNER JOIN AffiliateContact ac ON ac.RoleId = r.Id
	WHERE ac.Id = @AffiliateContactId
	and a.Id = @ReportId
	AND not EXISTS (select 0x0 FROM UserReportList url
				 WHERE url.ReportId = @ReportId
				 AND url.AffiliateContactId = @AffiliateContactId	 )
	
	


GO







