ALTER TABLE dbo.Shipment ADD
	ItemsAvailable int NULL
GO

CREATE PROCEDURE [dbo].[p_ShipmentShipmentItemsAvailable] 
	@ShipmentId as int
AS
BEGIN
    SET NOCOUNT ON;
    
    UPDATE s SET ItemsAvailable =  coalesce(si.Available,0)
    FROM Shipment s
    Left Join (
	   Select 
	   ShipmentId,
	   sum(Coalesce(ReceivedQuantity,0)) - sum(Coalesce(Sold,0)) - sum(Coalesce(Pulled,0)) Available
	   from ShipmentItem (nolock)
	   where 
	   shipmentId = @ShipmentId
	   and (Disposition = 'Salvage'
	   or (Disposition = 'Recycle' and (description like '%tv%' or description like '%television%' or description like '%monitor%'	 )))
	   group by ShipmentId 
			 ) si ON si.ShipmentId    = s.Id
    WHERE s.Id = @ShipmentId
    
END

GO


CREATE TRIGGER [dbo].[tr_ShipmentItem] 
   ON  [dbo].[ShipmentItem]
   AFTER  INSERT,DELETE,UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;
	Declare @ShipmentId int
	
	Select @ShipmentId = ShipmentId from deleted
	
	if @ShipmentId is null
	begin
	   Select @ShipmentId = ShipmentId from inserted 
	end 
	
     exec [p_ShipmentShipmentItemsAvailable] @ShipmentId
	

END

GO


UPDATE s SET ItemsAvailable =  coalesce(si.Available,0)
FROM Shipment s
Left Join (
   Select 
   ShipmentId,
   sum(Coalesce(ReceivedQuantity,0)) - sum(Coalesce(Sold,0)) - sum(Coalesce(Pulled,0)) Available
   from ShipmentItem (nolock)
   where 
    (Disposition = 'Salvage'
   or (Disposition = 'Recycle' and (description like '%tv%' or description like '%television%' or description like '%monitor%'	 )))
   group by ShipmentId 
		 ) si ON si.ShipmentId    = s.Id

GO



Alter View [dbo].[V_ShipmentAvailableToSellAll]
as
Select 
    
	'S' Type
	,s.Id
	,S.ReceivedDate
	,s.ShipToAffiliateId 
From Shipment s (nolock)
where  ReceivedDate is not null
and ItemsAvailable > 0 

union all
Select 
    
	'B' Type
	,s.Id
	,S.ReceivedDate
	,s.ShipToAffiliateId 
From BulkShipment s (nolock)
where  ReceivedDate is not null
and exists (select 0x0 from V_BulkShipmentInventory si  (nolock) where si.ShipmentId = s.Id and si.Available >= 1)




GO


