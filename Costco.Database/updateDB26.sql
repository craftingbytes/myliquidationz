

CREATE TABLE [dbo].[SalvageSalesOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AffiliateId] [int] NOT NULL,
	[SoldToAffiliateId] [int] NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateSold] [datetime] NULL,
	[SoldAmount] [money] NULL,
	[RetailAmount] [money] NULL,
	[SoldToName] [nvarchar](100) NULL,
	[SoldToAddress] [nvarchar](100) NULL,
	[SoldToStateId] [int] NULL,
	[SoldToCityId] [int] NULL,
	[SoldToZip] [nvarchar](20) NULL,
	[SoldBy] [nvarchar](50) NULL,
	[Notes] [nvarchar](2000) NULL,
	[ReceivedBy] [nvarchar](50) NULL,
	[ReceivedDate] [datetime] NULL,
 CONSTRAINT [PK_SalvageSalesOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SalvageSalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalvageSalesOrder_Affiliate] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[SalvageSalesOrder] CHECK CONSTRAINT [FK_SalvageSalesOrder_Affiliate]
GO

ALTER TABLE [dbo].[SalvageSalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalvageSalesOrder_City] FOREIGN KEY([SoldToCityId])
REFERENCES [dbo].[City] ([Id])
GO

ALTER TABLE [dbo].[SalvageSalesOrder] CHECK CONSTRAINT [FK_SalvageSalesOrder_City]
GO

ALTER TABLE [dbo].[SalvageSalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalvageSalesOrder_SoldToAffiliate] FOREIGN KEY([SoldToAffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[SalvageSalesOrder] CHECK CONSTRAINT [FK_SalvageSalesOrder_SoldToAffiliate]
GO

ALTER TABLE [dbo].[SalvageSalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalvageSalesOrder_State] FOREIGN KEY([SoldToStateId])
REFERENCES [dbo].[State] ([Id])
GO

ALTER TABLE [dbo].[SalvageSalesOrder] CHECK CONSTRAINT [FK_SalvageSalesOrder_State]
GO

ALTER TABLE [dbo].[SalvageSalesOrder] ADD  CONSTRAINT [DF_SalvageSalesOrder_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO



CREATE TABLE [dbo].[SalvageSalesOrderItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SalvageSalesOrderId] [int] NOT NULL,
	[ItemId] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[RetailValue] [money] NOT NULL,
	[SellingPrice] [money] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Missing] [int] NULL,
	[PalletUnitNumber] [nvarchar](50) NULL,
	[Store] [int] NULL,
 CONSTRAINT [PK_SalvageSalesOrderItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SalvageSalesOrderItem]  WITH CHECK ADD  CONSTRAINT [FK_SalvageSalesOrderItem_SalvageSalesOrder] FOREIGN KEY([SalvageSalesOrderId])
REFERENCES [dbo].[SalvageSalesOrder] ([Id])
GO

ALTER TABLE [dbo].[SalvageSalesOrderItem] CHECK CONSTRAINT [FK_SalvageSalesOrderItem_SalvageSalesOrder]
GO

ALTER TABLE [dbo].[SalvageSalesOrderItem] ADD  CONSTRAINT [DF_SalvageSalesOrderItem_Quantity]  DEFAULT ((1)) FOR [Quantity]
GO





CREATE PROCEDURE [dbo].[p_UpdateSalvageSalesOrderSoldAmount] 
	@SalvageSalesOrderId as int,
	@SoldAmount as money output,
	@RetailAmount as money output
	
AS
BEGIN
    SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;
    
    Declare @SumSales money
    Declare @SumRetail money
    
    select 
	   @SumSales = coalesce(Sum(soi.sellingPrice * soi.Quantity),0),
	   @SumRetail = coalesce(Sum(soi.retailValue * soi.Quantity),0)
    from SalvageSalesOrderItem soi
    where soi.SalvageSalesOrderId = @SalvageSalesOrderId  
    
    update SalvageSalesOrder
	   set 
		  SoldAmount = @SumSales,
		  RetailAmount = @SumRetail
	   where id = @SalvageSalesOrderId 
    
    select @SoldAmount = @SumSales, @RetailAmount = @SumRetail

END

GO



CREATE TABLE [dbo].[AffiliateShared](
	[Id] [int] NOT NULL,
	[ParentAffiliateId] [int] NOT NULL,
	[SharedAffiliateId] [int] NOT NULL,
 CONSTRAINT [PK_AffiliateShared] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AffiliateShared]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateShared_Affiliate] FOREIGN KEY([ParentAffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[AffiliateShared] CHECK CONSTRAINT [FK_AffiliateShared_Affiliate]
GO

ALTER TABLE [dbo].[AffiliateShared]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateShared_Affiliate1] FOREIGN KEY([SharedAffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[AffiliateShared] CHECK CONSTRAINT [FK_AffiliateShared_Affiliate1]
GO



CREATE view [dbo].[V_SalvagSalesOrderBought]
as
select  
    sso.*,
    cast(Coalesce(afs.ParentAffiliateId,0) as int) BuyerAffiliateId,
    a.Name SellerName
from salvageSalesOrder sso
inner join Affiliate a on a.Id = sso.AffiliateId
left join AffiliateShared afs on afs.SharedAffiliateId = sso.SoldToAffiliateId
where sso.DateSold is not null

GO




CREATE TABLE [dbo].[AuditReports](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StateId] [int] NULL,
	[FacilityName] [nvarchar](100) NULL,
	[AuditDate] [datetime] NULL,
	[PlanYear] [int] NULL,
 CONSTRAINT [PK_AuditReports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AuditReports]  WITH CHECK ADD  CONSTRAINT [FK_AuditReports_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO

ALTER TABLE [dbo].[AuditReports] CHECK CONSTRAINT [FK_AuditReports_State]
GO



CREATE procedure [dbo].[p_AddAuditReportState] 
    @AuditReportId int,
    --set @AuditReportId = 63 
    @StateId int
    --set @StateId = 57
as 
begin
    Declare @NewId int
    Declare @FacilityName nvarchar(200)
    Declare @PlanYear int
    declare @AuditDate datetime



    select top 1 
	   @FacilityName=ar.FacilityName,
	   @PlanYear=ar.PlanYear, 
	   @AuditDate=ar.AuditDate 
    from AuditReports ar
    where ar.Id = @AuditReportId
    and not exists (select 0x0
				from AuditReports ar2
				where ar.FacilityName = ar2.FacilityName
				and ar.PlanYear = ar2.PlanYear
				and ar2.StateId = @StateId)


    if (@FacilityName is not null)
    begin
	   insert into AuditReports (FacilityName, PlanYear, AuditDate, StateId)
	   values (@FacilityName,@PlanYear,@AuditDate,@StateId)
        
	   select top 1 @NewId = Id 
	   from AuditReports ar
	   where ar.FacilityName =  @FacilityName
	   and ar.PlanYear = @PlanYear
	   and ar.StateId = @StateId
    end
    
    if (@NewId is not null)
    begin 
	   insert into DocumentRelation (ForeignID, ForeignTableID, DocumentID)
	   select @NewId, 7, dr.DocumentId
	   from DocumentRelation dr
	   where dr.ForeignID = @AuditReportId
	   and dr.ForeignTableID = 7
    end

end
GO




CREATE procedure [dbo].[p_AddDocumentToAuditReport] 
    @DocumentRelationId int
    --set @DocumentRelationId = 3851
as
begin
    Declare @AuditReportId int
    Declare @DocumentId int

    select @AuditReportId = ForeignId, @DocumentId = dr.DocumentID
    from DocumentRelation dr
    where dr.Id = @DocumentRelationId
    and ForeignTableID = 7


    insert into DocumentRelation (DocumentID,ForeignID,ForeignTableID)
    select  distinct @DocumentId, dr.ForeignId,7
    from document d
    inner join DocumentRelation dr on dr.DocumentID = d.Id
    inner join AuditReports ar on ar.Id = dr.ForeignID
    where dr.ForeignTableID = 7 
    and exists (select 0x0
			 from DocumentRelation dr2
			 where dr2.ForeignID = @AuditReportId
			 and dr2.ForeignTableID = 7
			 and dr2.DocumentID = dr.DocumentID)
    and ar.Id <> @AuditReportId
    and ar.Id not in (select dr3.ForeignID
				    from  DocumentRelation dr3
				    where dr3.DocumentID = @DocumentId
				    and dr3.ForeignTableID = 7)

end



GO




CREATE TABLE [dbo].[PlanYear](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Year] [int] NOT NULL,
 CONSTRAINT [PK_PlanYear] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

insert into PlanYear (Year) values (2012)
insert into PlanYear (Year) values (2013)
insert into PlanYear (Year) values (2014)
insert into PlanYear (Year) values (2015)
insert into PlanYear (Year) values (2016)


insert into Role (AffiliateTypeId, RoleName)
values (4, 'CostcoWarehouseSeller')


--declare @MaxId int

--select @MaxId = max(Id) + 1 from Area

--insert into Area (Id, IsArea

ALTER TABLE dbo.SalvageSalesOrder ADD
	ItemsAvailable int NULL
GO

ALTER TABLE dbo.SalvageSalesOrderItem ADD
    Sold int NULL
GO

ALTER TABLE dbo.SalvageSalesOrderItem ADD
    Pulled int NULL
GO

CREATE 
-- alter 
PROCEDURE [dbo].[p_SalvageSalesOrderItemsAvailable] 
	@SalvageSalesOrderId as int
AS
BEGIN
    SET NOCOUNT ON;
    
    UPDATE s SET ItemsAvailable =  coalesce(si.Available,0)
    FROM SalvageSalesOrder s
    Left Join (
	   Select 
	   SalvageSalesOrderId,
	   sum(Coalesce(Quantity,0)) - sum(Coalesce(Sold,0)) - sum(Coalesce(Missing,0)) - SUM(coalesce(Pulled,0)) Available
	   from SalvageSalesOrderItem (nolock)
	   where 
	   SalvageSalesOrderId = @SalvageSalesOrderId
	   group by SalvageSalesOrderId 
			 ) si ON si.SalvageSalesOrderId    = s.Id
    WHERE s.Id = @SalvageSalesOrderId
    
END


GO

CREATE TRIGGER [dbo].[tr_SalvageSalesOrderItem] 
   ON  [dbo].[SalvageSalesOrderItem]
   AFTER  INSERT,DELETE,UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;
	Declare @SalvageSalesOrderId int
	
	Select @SalvageSalesOrderId = SalvageSalesOrderId from deleted
	
	if @SalvageSalesOrderId is null
	begin
	   Select @SalvageSalesOrderId = SalvageSalesOrderId from inserted 
	end 
	
     exec [p_SalvageSalesOrderItemsAvailable] @SalvageSalesOrderId
	

END


GO

CREATE TABLE [dbo].[SalesOrderSSOItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesOrderId] [int] NOT NULL,
	[SSOId] [int] NOT NULL,
	[SSOItemId] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Comment] [nvarchar](200) NULL,
	[Price] [money] NULL,
	[Description] [nvarchar](100) NULL,
	[SellingPrice] [money] NULL,
 CONSTRAINT [PK_SalesOrderSSItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SalesOrderSSOItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderSSItem_SalvageSalesOrder] FOREIGN KEY([SalesOrderId])
REFERENCES [dbo].[SalesOrder] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderSSOItem] CHECK CONSTRAINT [FK_SalesOrderSSItem_SalvageSalesOrder]
GO

--alter table  [dbo].[SalesOrderSSOItem]  drop CONSTRAINT [FK_SalesOrderSSItem_SalvageSalesOrder]

ALTER TABLE [dbo].[SalesOrderSSOItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderSSItem_SalvageSalesOrder1] FOREIGN KEY([SSOId])
REFERENCES [dbo].[SalvageSalesOrder] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderSSOItem] CHECK CONSTRAINT [FK_SalesOrderSSItem_SalvageSalesOrder1]
GO

ALTER TABLE [dbo].[SalesOrderSSOItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderSSItem_SalvageSalesOrderItem] FOREIGN KEY([SSOItemId])
REFERENCES [dbo].[SalvageSalesOrderItem] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderSSOItem] CHECK CONSTRAINT [FK_SalesOrderSSItem_SalvageSalesOrderItem]
GO


create PROCEDURE [dbo].[p_UpdateSSOItemSold] 
	@SalvageSalesOrderItemId as bigint
AS
BEGIN
    SET NOCOUNT ON;
    
    UPDATE si SET sold =  coalesce(soi.Quantity,0)
    FROM SalvageSalesOrderItem si
    Left Join (
    SELECT SSOItemId, SUM(Quantity) Quantity
    FROM SalesOrderSSOItem soi
    WHERE SSOItemId = @SalvageSalesOrderItemId
    GROUP BY soi.SSOItemId   ) soi ON soi.SSOItemId    = si.Id
    WHERE si.Id = @SalvageSalesOrderItemId
    
END

GO


create TRIGGER [dbo].[tr_SalesOrderSSOItem] 
   ON  [dbo].[SalesOrderSSOItem]
   AFTER  INSERT,DELETE,UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;
	Declare @SalvageSalesOrderItemId int
	
	Select @SalvageSalesOrderItemId = [SSOItemId] from deleted
	
	if @SalvageSalesOrderItemId is null
	begin
	   Select @SalvageSalesOrderItemId = [SSOItemId] from inserted 
	end 
	
     exec p_UpdateSSOItemSold @SalvageSalesOrderItemId
	

END

go


--stopped here


ALTER PROCEDURE [dbo].[p_UpdateSalesOrderSoldAmount] 
	@SalesOrderId as int
AS
BEGIN
    SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;
    
    Declare @SoldAmount money
    Declare @SoldBulkAmount money
    Declare @SoldSSOAmount money
    
    select @SoldAmount = Sum(soi.sellingPrice * soi.Quantity)
    from SalesOrderItem soi
    where soi.SalesOrderId =	@SalesOrderId  
    
    select @SoldBulkAmount = Sum(soi.sellingPrice * soi.Quantity)
    from SalesOrderBulkItem soi
    where soi.SalesOrderId =	@SalesOrderId  
    
    select @SoldSSOAmount = Sum(soi.sellingPrice * soi.Quantity)
    from SalesOrderSSOItem soi
    where soi.SalesOrderId =	@SalesOrderId  
    
    if @SoldAmount is not null or @SoldBulkAmount is not null or @SoldSSOAmount is not null
    begin
	   update SalesOrder
	   set SoldAmount = coalesce(@SoldAmount,0) + coalesce(@SoldBulkAmount,0) + coalesce(@SoldSSOAmount,0),
		  MinValue = coalesce(@SoldAmount,0) + coalesce(@SoldBulkAmount,0) + coalesce(@SoldSSOAmount,0)
	   where id = @SalesOrderId
    end
    select SoldAmount 
    from  SalesOrder
    where id = @SalesOrderId
    
END

go


Alter View [dbo].[V_ShipmentAvailableToSellAll]
as
Select 
    
	'S' Type
	,s.Id
	,S.ReceivedDate
	,s.ShipToAffiliateId 
From Shipment s (nolock)
where  ReceivedDate is not null
and ItemsAvailable > 0 

union all
Select 
    
	'B' Type
	,s.Id
	,S.ReceivedDate
	,s.ShipToAffiliateId 
From BulkShipment s (nolock)
where  ReceivedDate is not null
and exists (select 0x0 from V_BulkShipmentInventory si  (nolock) where si.ShipmentId = s.Id and si.Available >= 1)

union all

Select 
    
	'C' Type
	,s.Id
	,s.ReceivedDate
	,s.SoldToAffiliateId ShipToAffiliateId
From SalvageSalesOrder s (nolock)
where  ReceivedDate is not null
and ItemsAvailable > 0 



GO



ALTER View [dbo].[V_SalesOrderItemAll]
as
Select 
	'C' Type 
	,soi.Id
	,soi.SalesOrderId
	,si.SalvageSalesOrderId  ShipmentId 
	,soi.SSOItemId ShipmentItemId
	,soi.Quantity
	,si.ItemId
	,coalesce(soi.Description, si.Description) Description
	,'Salvage' Disposition
	,'' ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
	,coalesce(soi.SellingPrice, 0.0) SellingPrice
	,coalesce(si.SellingPrice,0) Cost
FROM SalesOrderSSOItem soi
Inner Join SalvageSalesOrderItem si on si.Id = soi.SSOItemId
Union All

Select 
	'S' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.ShipmentId 
	,soi.ShipmentItemId
	,soi.Quantity
	,si.ItemId
	,coalesce(soi.Description, si.Description) Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
	,coalesce(soi.SellingPrice, 0.0) SellingPrice
	,0.00 Cost
FROM SalesOrderItem soi
Inner Join ShipmentItem si on si.Id = soi.ShipmentItemId
Union All
Select 
	'B' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.BulkShipmentId 
	,soi.BulkShipmentItemId
	,soi.Quantity
	,si.ItemId
	,coalesce(soi.Description, si.Description) Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
	,coalesce(soi.Comment,'') SaleComment
	,coalesce(soi.Price,0.0) Price
	,coalesce(soi.SellingPrice, 0.0) SellingPrice
	,0.00 Cost
FROM SalesOrderBulkItem soi
Inner Join BulkShipmentItem si on si.Id = soi.BulkShipmentItemId



GO



CREATE View [dbo].[V_SSOrderInventory]
as 

Select 
si.SalvageSalesOrderId SSOrderId,
Id SSOrderItemId,
ItemId,
Description,
Coalesce(Quantity,0) Quantity,
Coalesce(Sold,0) Allocated,
Coalesce(Pulled,0) Pulled,
Coalesce(Quantity,0) - Coalesce(Sold,0) - Coalesce(Missing,0) - Coalesce(Pulled,0) Available
from SalvageSalesOrderItem si  (nolock)

go


 
declare @MaxId as int
Select @MaxId = MAX(id) + 1 from area  
insert into Area(
  Id	
  ,ParentId	
  ,Name	
  ,Caption	
  ,URL	
  ,IsMenu	
  ,IsArea	
  ,[Order])
values (
    @MaxId
    ,4	
    ,'SalvageSalesOrderSell'
    ,'Salvage Sales'	
    ,'SalvageSales/SellerList'
    ,1	
    ,1	
    ,6)

Select @MaxId = MAX(id) + 1 from area 
    
insert into Area(
  Id	
  ,ParentId	
  ,Name	
  ,Caption	
  ,URL	
  ,IsMenu	
  ,IsArea	
  ,[Order])
values (
    @MaxId	
    ,4	
    ,'SalvageSalesOrderReport'	
    ,'Salvage Sales Order'	
    ,NULL	
    ,0	
    ,1	
    ,6)
Select @MaxId = MAX(id) + 1 from area 
    
insert into Area(
  Id	
  ,ParentId	
  ,Name	
  ,Caption	
  ,URL	
  ,IsMenu	
  ,IsArea	
  ,[Order])
values (
    @MaxId	
    ,4	
    ,'SalvageSalesOrderBuy'	
    ,'Salvage Sales (Buyer)'	
    ,'SalvageSales/BuyerList'	
    ,1	
    ,1	
    ,6)
    
insert into role(
    AffiliateTypeId
    ,RoleName	
    ,CreateByAffiliateId)
values (
    4	
    ,'CostcoWarehouseSeller'
    ,NULL)
    
    
DBCC CHECKIDENT ('SalvageSalesOrder', RESEED, 5000);
