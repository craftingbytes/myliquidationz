CREATE TABLE [dbo].[ItemProfitCost](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ShipmentType] [nvarchar](50) NOT NULL,
	[Date] [date] NOT NULL,
	[ASR] [int] NOT NULL,
	[Region] [nvarchar](200) NOT NULL,
	[District] [nvarchar](200) NOT NULL,
	[Warehouse] [nvarchar](200) NOT NULL,
	[WarehouseInt] [int] NOT NULL,
	[Department] [nvarchar](200) NOT NULL,
	[DeppartmentNumber] [int] NOT NULL,
	[ItemId] [nvarchar](200) NOT NULL,
	[Salvage] [int] NOT NULL,
	[Recycle] [int] NOT NULL,
	[Sold] [int] NOT NULL,
	[Weight] [decimal](18, 2) NOT NULL,
	[SoldWeight] [decimal](18, 2) NOT NULL,
	[SoldQuantity] [int] NOT NULL,
	[Sales] [money] NOT NULL,
 CONSTRAINT [PK_ItemProfitCost] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE NONCLUSTERED INDEX [IX_ItemProfitCost] ON [dbo].[ItemProfitCost] 
(
	[Date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


truncate table ItemProfitCost
go

declare
    @StartDate as datetime,
    @EndDate as datetime,
    @CostPerPound as decimal(18,4)

set @StartDate = '08/01/2013'
set @EndDate =  GETDATE()

INSERT INTO ItemProfitCost
           ([ShipmentType]
           ,[Date]
           ,[ASR]
           ,[Region]
           ,[District]
           ,[Warehouse]
           ,[WarehouseInt]
           ,[Department]
           ,[DeppartmentNumber]
           ,[ItemId]
           ,[Salvage]
           ,[Sold]
           ,Recycle 
           ,[Weight]
           ,[SoldWeight]
           ,[SoldQuantity]
           ,[Sales])
select 

	  'Shipment' [Type],
	  cast(s.ShipmentDate as DATE) ShipmentDate,
	  s.shiptoAffiliateId ASR,
	  coalesce(tr.Region, 'Unknown') Region,
	  coalesce(tr.District, 'Unknown') District,
	  a.WarehouseNumber Warehouse,
	  case when isnumeric(a.WarehouseNumber) = 1 then CAST(a.warehousenumber as int)
		  else 99999
	  end WarehouseNumber,
	  coalesce(pfc.DepartmentName,'Unknown') Department,
	  coalesce(pfc.DepartmentId, 99999) DepartmentNumber,
	  si.ItemId,
	  SUM(
	  case 
	   when (si.disposition = 'Salvage' OR si.Sold > 1) 
	   then si.Quantity
	   else 0 
	  end) Salvage, 
	  SUM(si.Sold) Sold,
	  SUM(
	  case 
	   when (si.disposition = 'Salvage' OR si.Sold > 1) 
	   then 0
	   else si.Quantity
	  end) Recycle ,
	  SUM(coalesce(si.Quantity,0) * coalesce(si.Weight,p.weight, 1)) Weight,
	  SUM(coalesce(si.Sold,0) * coalesce(si.Weight,p.weight, 1)) SoldWeight,
	  SUM(coalesce(sold.Quantity,0)) SoldQuantity,
	  SUM(coalesce(sold.sales,0)) Sales
from Shipment s
inner join Affiliate a on a.Id = s.AffiliateId
left join tempRegions tr on tr.whse = a.WarehouseNumber and a.AffiliateTypeId = 4
inner join ShipmentItem si on s.Id = si.ShipmentId
left join Product p on p.ItemId = si.ItemId
left join ProductFromCostco pfc on pfc.ItemId = si.ItemId
left join (
    select
	   soi.ShipmentItemId,
	   SUM(coalesce(soi.Quantity,0) * coalesce(soi.SellingPrice,0)) sales,
	   SUM(coalesce(soi.Quantity,0)) quantity
    from SalesOrderItem soi
    group by soi.ShipmentItemId) sold on sold.ShipmentItemId = si.Id
where 
s.ReceivedDate is not null
and (a.WarehouseNumber not in ('1998','1997'))
and ReceivedDate >= '10/01/2012'
and s.ShipmentDate >= @StartDate
and s.ShipmentDate < @EndDate
--and s.ShipToAffiliateId = 533
group by 
    cast(s.ShipmentDate as DATE),
    s.shiptoAffiliateId,
    coalesce(tr.Region, 'Unknown'),
    coalesce(tr.District, 'Unknown'),
    a.WarehouseNumber,
    case when isnumeric(a.WarehouseNumber) = 1 then CAST(a.warehousenumber as int)
		  else 99999
	  end,
    coalesce(pfc.DepartmentName,'Unknown'), 
    coalesce(pfc.DepartmentId, 99999) ,
    si.ItemId
    
    
    
INSERT INTO ItemProfitCost
           ([ShipmentType]
           ,[Date]
           ,[ASR]
           ,[Region]
           ,[District]
           ,[Warehouse]
           ,[WarehouseInt]
           ,[Department]
           ,[DeppartmentNumber]
           ,[ItemId]
           ,[Salvage]
           ,[Sold]
           ,Recycle
           ,[Weight]
           ,[SoldWeight]
           ,[SoldQuantity]
           ,[Sales])
select 
	  'DC' [Type],
	  cast(s.ShipmentDate as DATE) ShipmentDate,
	  s.shiptoAffiliateId ASR,
	  coalesce(tr.Region, 'Unknown') Region,
	  coalesce(tr.District, 'Unknown') District,
	  a.WarehouseNumber Warehouse,
	  case when isnumeric(a.WarehouseNumber) = 1 then CAST(a.warehousenumber as int)
		  else 99999
	  end WarehouseNumber,
	  coalesce(pfc.DepartmentName,'Unknown') Department,
	  coalesce(pfc.DepartmentId, 99999) DepartmentNumber,
	  si.ItemId,
	  SUM(
	  case 
	   when (si.disposition = 'Salvage' OR si.Sold > 1) 
	   then si.Quantity
	   else 0 
	  end) Salvage, 
	  SUM(si.Sold) Sold,
	  SUM(
	  case 
	   when (si.disposition = 'Salvage' OR si.Sold > 1) 
	   then 0
	   else si.Quantity
	  end) Recycle ,
	  SUM(coalesce(si.Quantity,0) * coalesce(si.Weight,p.weight, 1)) Weight,
	  SUM(coalesce(si.Sold,0) * coalesce(si.Weight,p.weight, 1)) SoldWeight,
	  SUM(coalesce(sold.Quantity,0)) SoldQuantity,
	  SUM(coalesce(sold.sales,0)) Sales
from Shipment s
inner join ShipmentItem si on s.Id = si.ShipmentId
inner join Affiliate dc on dc.Id = s.AffiliateId
inner join Affiliate a on a.WarehouseNumber = cast(coalesce(si.store,0) as nvarchar(10))
left join tempRegions tr on tr.whse = a.WarehouseNumber and a.AffiliateTypeId = 4
left join Product p on p.ItemId = si.ItemId
left join ProductFromCostco pfc on pfc.ItemId = si.ItemId
left join (
    select
	   soi.ShipmentItemId,
	   SUM(coalesce(soi.Quantity,0) * coalesce(soi.SellingPrice,0)) sales,
	   SUM(coalesce(soi.Quantity,0)) quantity
    from SalesOrderItem soi
    group by soi.ShipmentItemId) sold on sold.ShipmentItemId = si.Id
where 
s.ReceivedDate is not null
and dc.WarehouseNumber in ('1998','1997')
and ReceivedDate >= '10/01/2012'
and s.ShipmentDate >= @StartDate
and s.ShipmentDate < @EndDate
--and s.ShipToAffiliateId = 533
group by 
    cast(s.ShipmentDate as DATE),
    s.shiptoAffiliateId,
    coalesce(tr.Region, 'Unknown'),
    coalesce(tr.District, 'Unknown'),
    a.WarehouseNumber,
    case when isnumeric(a.WarehouseNumber) = 1 then CAST(a.warehousenumber as int)
		  else 99999
	  end ,
    coalesce(pfc.DepartmentName,'Unknown'), 
    coalesce(pfc.DepartmentId, 99999) ,
    si.ItemId
----having SUM(coalesce(si.sold,0)) <> SUM(coalesce(sold.quantity,0))

INSERT INTO ItemProfitCost
           ([ShipmentType]
           ,[Date]
           ,[ASR]
           ,[Region]
           ,[District]
           ,[Warehouse]
           ,[WarehouseInt]
           ,[Department]
           ,[DeppartmentNumber]
           ,[ItemId]
           ,[Salvage]
           ,[Sold]
           ,Recycle
           ,[Weight]
           ,[SoldWeight]
           ,[SoldQuantity]
           ,[Sales])
select 
	  'Shipment' [Type],
	  cast(s.ShipmentDate as DATE) ShipmentDate,
	  s.shiptoAffiliateId ASR,
	  coalesce(tr.Region, 'Unknown') Region,
	  coalesce(tr.District, 'Unknown') District,
	  a.WarehouseNumber Warehouse,
	  case when isnumeric(a.WarehouseNumber) = 1 then CAST(a.warehousenumber as int)
		  else 99999
	  end WarehouseNumber,
	  coalesce(pfc.DepartmentName,'Unknown') Department,
	  coalesce(pfc.DepartmentId, 99999) DepartmentNumber,
	  si.ItemId,
	  SUM(
	  case 
	   when (si.disposition = 'Salvage' OR si.Sold > 1) 
	   then si.Quantity
	   else 0 
	  end) Salvage, 
	  SUM(si.Sold) Sold,
	  SUM(
	  case 
	   when (si.disposition = 'Salvage' OR si.Sold > 1) 
	   then 0
	   else si.Quantity
	  end) Recycle ,
	  SUM(coalesce(si.Quantity,0) * coalesce(si.Weight,p.weight, 1)) Weight,
	  SUM(coalesce(si.Sold,0) * coalesce(si.Weight,p.weight, 1)) SoldWeight,
	  SUM(coalesce(sold.Quantity,0)) SoldQuantity,
	  SUM(coalesce(sold.sales,0)) Sales
from Shipment s
inner join Affiliate a on a.Id = s.AffiliateId
left join tempRegions tr on tr.whse = a.WarehouseNumber and a.AffiliateTypeId = 4
inner join ShipmentItem si on s.Id = si.ShipmentId
left join Affiliate dc on dc.WarehouseNumber = cast(coalesce(si.store,0) as nvarchar(10))
left join Product p on p.ItemId = si.ItemId
left join ProductFromCostco pfc on pfc.ItemId = si.ItemId
left join (
    select
	   soi.ShipmentItemId,
	   SUM(coalesce(soi.Quantity,0) * coalesce(soi.SellingPrice,0)) sales,
	   SUM(coalesce(soi.Quantity,0)) quantity
    from SalesOrderItem soi
    group by soi.ShipmentItemId) sold on sold.ShipmentItemId = si.Id
where 
s.ReceivedDate is not null
and (a.WarehouseNumber in ('1998','1997'))
and ReceivedDate >= '10/01/2012'
and s.ShipmentDate >= @StartDate
and s.ShipmentDate < @EndDate
and (a.WarehouseNumber in ('1998','1997')
and coalesce(si.Store,0) = 0)
--and s.ShipToAffiliateId = 533
group by 
    cast(s.ShipmentDate as DATE),
    s.shiptoAffiliateId,
    coalesce(tr.Region, 'Unknown'),
    coalesce(tr.District, 'Unknown'),
    a.WarehouseNumber,
    case when isnumeric(a.WarehouseNumber) = 1 then CAST(a.warehousenumber as int)
		  else 99999
	  end ,
    coalesce(pfc.DepartmentName,'Unknown'), 
    coalesce(pfc.DepartmentId, 99999) ,
    si.ItemId



INSERT INTO ItemProfitCost
           ([ShipmentType]
           ,[Date]
           ,[ASR]
           ,[Region]
           ,[District]
           ,[Warehouse]
           ,[WarehouseInt]
           ,[Department]
           ,[DeppartmentNumber]
           ,[ItemId]
           ,[Salvage]
           ,[Sold]
           ,Recycle
           ,[Weight]
           ,[SoldWeight]
           ,[SoldQuantity]
           ,[Sales])
    select 
	 'Bulk' [Type],
	 cast(s.ReceivedDate as DATE) ShipmentDate,
		  s.shiptoAffiliateId ASR,
	 coalesce(tr.Region, 'Unknown') Region,
	 coalesce(tr.District, 'Unknown') District,
	 coalesce(a.WarehouseNumber,'Unknown') Warehouse,
	 case when isnumeric(a.WarehouseNumber) = 1 then CAST(a.warehousenumber as int)
		  else 99999
	  end ,
	 coalesce(pfc.DepartmentName,'Unknown') Department,
	 coalesce(pfc.DepartmentId, 99999) DepartmentNumber,
	 si.ItemId,
	 SUM(
	 case 
		  when (si.disposition = 'Salvage' OR si.Sold > 1) 
		  then si.qty
		  else 0 
	 end) Salvage, 
	 SUM(si.Sold) Sold,
	 SUM(
	 case 
		  when (si.disposition = 'Salvage' OR si.Sold > 1) 
		  then 0
		  else si.qty
	 end) Recycle ,
	 SUM(si.Qty * coalesce(p.weight, 1)) Weight,
	 SUM(coalesce(si.Sold,0) * coalesce(p.weight, 1)) SoldWeight,
	 SUM(coalesce(sold.Quantity,0)) SoldQuantity,
	 SUM(coalesce(sold.sales,0)) Sales
    	  
    from BulkShipment s
    inner join BulkShipmentItem si on s.Id = si.BulkShipmentId
    left join Affiliate a on a.Id = cast(si.Store as nvarchar(20))
    left join tempRegions tr on tr.whse = a.WarehouseNumber and a.AffiliateTypeId = 4
    left join Product p on p.ItemId = si.ItemId
    left join ProductFromCostco pfc on pfc.ItemId = si.ItemId
    left join (
	   select
		  soi.BulkShipmentItemId,
		  SUM(coalesce(soi.Quantity,0) * coalesce(soi.SellingPrice,0)) sales,
		  SUM(coalesce(soi.Quantity,0)) quantity
	   from SalesOrderBulkItem soi
	   group by soi.BulkShipmentItemId) sold on sold.BulkShipmentItemId = si.Id
    where 
    ReceivedDate >= '10/01/2012'
    and s.ReceivedDate >= @StartDate
    and s.ReceivedDate < @EndDate
    --and s.ShipToAffiliateId = 533
    group by 
	   cast(s.ReceivedDate as DATE),
	    s.shiptoAffiliateId,
	   coalesce(tr.Region, 'Unknown'),
	   coalesce(tr.District, 'Unknown'),
	   coalesce(a.WarehouseNumber,'Unknown'),
	   case when isnumeric(a.WarehouseNumber) = 1 then CAST(a.warehousenumber as int)
		  else 99999
	  end,
	   coalesce(pfc.DepartmentName,'Unknown'), 
	   coalesce(pfc.DepartmentId, 99999) ,
	   si.ItemId 