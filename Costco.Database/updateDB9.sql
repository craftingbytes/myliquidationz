

CREATE TABLE [dbo].[ProductPrice](
	[Id] [int] IDENTITY(10,1) NOT NULL,
	[ItemId] [nvarchar](100) NOT NULL,
	[Price] [money] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[AffiliateContactId] [int] NULL,
 CONSTRAINT [PK_ProductPrice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProductPrice]  WITH CHECK ADD  CONSTRAINT [FK_ProductPrice_AffiliateContact] FOREIGN KEY([AffiliateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO

ALTER TABLE [dbo].[ProductPrice] CHECK CONSTRAINT [FK_ProductPrice_AffiliateContact]
GO

ALTER TABLE [dbo].[ProductPrice]  WITH CHECK ADD  CONSTRAINT [FK_ProductPrice_ProductPrice] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Product] ([ItemId])
GO

ALTER TABLE [dbo].[ProductPrice] CHECK CONSTRAINT [FK_ProductPrice_ProductPrice]
GO

ALTER TABLE [dbo].[ProductPrice] ADD  CONSTRAINT [DF_ProductPrice_Price]  DEFAULT ((0)) FOR [Price]
GO

ALTER TABLE [dbo].[ProductPrice] ADD  CONSTRAINT [DF_ProductPrice_TimeStamp]  DEFAULT (getdate()) FOR [TimeStamp]
GO




Create View [dbo].[v_MaxProductPrice]
as
select 
	p.Id,
	p.ItemId,
	p.Description,
	p.Description2,
	coalesce(p.Weight,0) Weight,
	p.CoveredDevice,
	coalesce(pp.Price,0) Price,
	coalesce(pp.TimeStamp, Cast('01/01/1999' as datetime)) Timestamp,
	count(si.itemid) Count
from Product p
left join 
	(	select 
			max(Id) Id,
			ItemId 
		from ProductPrice 
		group by ItemId) mp on mp.ItemId = p.ItemId
left join ProductPrice pp on pp.Id = mp.Id
left join ShipmentItem si on si.ItemId = p.ItemId
group by 
		p.Id,
	p.ItemId,
	p.Description,
	p.Description2,
	p.Weight,
	p.CoveredDevice,
	pp.Price,
	pp.TimeStamp



GO


