/****** Object:  Table [dbo].[Role]    Script Date: 11/21/2011 17:55:40 ******/
SET IDENTITY_INSERT [dbo].[Role] ON
INSERT [dbo].[Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (0, 0, N'Public', NULL)
INSERT [dbo].[Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (1, 1, N'Administrator', NULL)
INSERT [dbo].[Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (2, 1, N'Manager', NULL)
INSERT [dbo].[Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (3, 2, N'CostcoCorpMgr', NULL)
INSERT [dbo].[Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (4, 3, N'CostcoDistMgr', NULL)
INSERT [dbo].[Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (5, 4, N'CostcoWareHouseMgr', NULL)
INSERT [dbo].[Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (6, 5, N'Recycler', NULL)
SET IDENTITY_INSERT [dbo].[Role] OFF
