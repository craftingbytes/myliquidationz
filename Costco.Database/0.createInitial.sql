/****** Object:  Table [dbo].[Area]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Area]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Area](
	[Id] [int] NOT NULL,
	[ParentId] [int] NULL,
	[Name] [varchar](50) NOT NULL,
	[Caption] [varchar](200) NULL,
	[URL] [varchar](200) NULL,
	[IsMenu] [bit] NULL,
	[IsArea] [bit] NULL,
	[Order] [int] NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AffiliateType]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AffiliateType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AffiliateType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](100) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_AffiliateType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConditionType]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConditionType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ConditionType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Condition] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ConditionType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[State]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[State]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Abbreviation] [char](2) NULL,
	[CountryId] [int] NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SiteImage]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteImage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SiteImage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Path] [nvarchar](2048) NULL,
 CONSTRAINT [SiteImage_Id_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SiteImage', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds the paths for the database images.  These are used throughout the site so usage requires only a link to the id.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SiteImage'
GO
/****** Object:  Table [dbo].[SecurityQuestion]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SecurityQuestion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SecurityQuestion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Question] [varchar](200) NULL,
 CONSTRAINT [PK_SecurityQuestion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ForeignTable]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ForeignTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ForeignTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ForeignTableName] [varchar](255) NOT NULL,
 CONSTRAINT [ForeignTable_Id_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ForeignTable', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'In tables where relationshiips may refer to more then one table this keeps track or which table an id is linked to.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ForeignTable'
GO
/****** Object:  Table [dbo].[DocumentType]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DocumentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SiteImageID] [int] NULL,
	[FileExtension] [varchar](10) NULL,
	[DocumentTypeName] [varchar](100) NULL,
	[ContentType] [varchar](100) NULL,
	[IsUploadable] [bit] NOT NULL,
	[IsImage] [bit] NOT NULL,
	[EscapeBeforeDisplay] [bit] NOT NULL,
 CONSTRAINT [DocumentType_Id_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[County]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[County]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[County](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Abbreviation] [char](5) NULL,
	[StateId] [int] NULL,
 CONSTRAINT [PK_County] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[City]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[City]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[City](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[StateId] [int] NULL,
	[CountyId] [int] NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Affiliate]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Affiliate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Affiliate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[Address1] [varchar](200) NULL,
	[Address2] [varchar](200) NULL,
	[Phone] [varchar](20) NULL,
	[Fax] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[WebSite] [varchar](50) NULL,
	[Longitude] [decimal](9, 6) NULL,
	[Latitude] [decimal](9, 6) NULL,
	[Active] [bit] NULL,
	[StateId] [int] NULL,
	[CityId] [int] NULL,
	[Zip] [varchar](11) NULL,
	[AffiliateTypeId] [int] NULL,
	[PaymentTermId] [int] NULL,
	[Notes] [nvarchar](2048) NULL,
	[Directions] [nvarchar](2048) NULL,
	[BusinessHours] [nvarchar](2048) NULL,
	[ParentId] [int] NULL,
	[DefaultRecycler] [int] NULL,
	[WarehouseNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_Affiliate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Shipment]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Shipment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Shipment](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[ShipmentDate] [datetime] NULL,
	[ShippingComments] [nvarchar](1000) NULL,
	[AffiliateId] [int] NOT NULL,
	[PickupAddress] [nvarchar](200) NULL,
	[PickupAddress2] [nvarchar](200) NULL,
	[PickupStateId] [int] NULL,
	[PickupCityId] [int] NULL,
	[PickupZip] [nvarchar](20) NULL,
	[PickupPhone] [nvarchar](20) NULL,
	[ShipToAffiliateId] [int] NOT NULL,
	[ShipToName] [nvarchar](100) NULL,
	[ShipToAddress] [nvarchar](200) NULL,
	[ShipToAddress2] [nvarchar](200) NULL,
	[ShipToStateId] [int] NULL,
	[ShipToCityId] [int] NULL,
	[ShipToZip] [nvarchar](20) NULL,
	[ShipToPhone] [nvarchar](20) NULL,
	[ShippedBy] [nvarchar](100) NULL,
	[ReceivedBy] [nvarchar](100) NULL,
	[ReceivingComments] [nvarchar](1000) NULL,
	[ShipmentTotalWeight] [int] NULL,
	[ReceivedDate] [datetime] NULL,
	[ReceivedTotalWeight] [int] NULL,
 CONSTRAINT [PK_Shipment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Role]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AffiliateTypeId] [int] NULL,
	[RoleName] [varchar](50) NULL,
	[CreateByAffiliateId] [int] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ShipmentItem]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShipmentItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ShipmentItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShipmentId] [int] NOT NULL,
	[PalletUnitNumber] [nvarchar](50) NOT NULL,
	[ItemId] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Condition] [nvarchar](100) NULL,
	[Quantity] [int] NOT NULL,
	[ReceivedQuantity] [int] NULL,
	[ReceivedCondition] [int] NULL,
 CONSTRAINT [PK_ShipmentItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Permissions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Permissions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[AreaId] [int] NULL,
	[Read] [bit] NULL,
	[Add] [bit] NULL,
	[Update] [bit] NULL,
	[Delete] [bit] NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AffiliateContact]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AffiliateContact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AffiliateContact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[MiddleInitials] [varchar](3) NULL,
	[Address1] [varchar](200) NULL,
	[Address2] [varchar](200) NULL,
	[Phone] [varchar](20) NULL,
	[Fax] [varchar](50) NULL,
	[UserName] [varchar](100) NULL,
	[Email] [varchar](50) NULL,
	[Password] [nvarchar](150) NULL,
	[Active] [bit] NULL,
	[StateId] [int] NULL,
	[CityId] [int] NULL,
	[Zip] [varchar](11) NULL,
	[AffiliateId] [int] NULL,
	[SecurityQuestionId] [int] NULL,
	[Answer] [varchar](500) NULL,
	[LoginTryCount] [int] NULL,
	[LastLoginTime] [smalldatetime] NULL,
	[Title] [varchar](100) NULL,
	[Notes] [nvarchar](200) NULL,
	[RoleId] [int] NULL,
 CONSTRAINT [PK_AffiliateContact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Document]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Document]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Document](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentTypeId] [int] NULL,
	[AffiliateContactId] [int] NULL,
	[ServerFileName] [varchar](2048) NULL,
	[UploadFileName] [varchar](2048) NULL,
	[UploadDate] [smalldatetime] NULL,
	[DeleteDate] [smalldatetime] NULL,
 CONSTRAINT [Document_DocumentationID_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Document', N'COLUMN',N'ServerFileName'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path and filename a browser will use to access the file.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Document', @level2type=N'COLUMN',@level2name=N'ServerFileName'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Document', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Files uploaded by users to the server.  Most oftern supporting documentation.  Is linked to data via DocumentRelation.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Document'
GO
/****** Object:  Table [dbo].[DocumentRelation]    Script Date: 11/21/2011 17:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentRelation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DocumentRelation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentID] [int] NOT NULL,
	[ForeignID] [int] NOT NULL,
	[ForeignTableID] [int] NOT NULL,
 CONSTRAINT [PK_DocumentRelation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [DocumentRelation_CK] UNIQUE NONCLUSTERED 
(
	[DocumentID] ASC,
	[ForeignID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'DocumentRelation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tracks the relationship between files uploaded by users to the server and the database item it pertains to - most often Invoices or Shipments.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DocumentRelation'
GO
/****** Object:  Default [DF_AffiliateContact_LoginTryCount]    Script Date: 11/21/2011 17:51:59 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AffiliateContact_LoginTryCount]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AffiliateContact_LoginTryCount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AffiliateContact] ADD  CONSTRAINT [DF_AffiliateContact_LoginTryCount]  DEFAULT ((0)) FOR [LoginTryCount]
END


End
GO
/****** Object:  Default [DF__Document__Docum__30E33A54]    Script Date: 11/21/2011 17:51:59 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Document__Docum__30E33A54]') AND parent_object_id = OBJECT_ID(N'[dbo].[Document]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Document__Docum__30E33A54]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Document] ADD  CONSTRAINT [DF__Document__Docum__30E33A54]  DEFAULT ((0)) FOR [DocumentTypeId]
END


End
GO
/****** Object:  Default [Document_UploadDate_DF]    Script Date: 11/21/2011 17:51:59 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[Document_UploadDate_DF]') AND parent_object_id = OBJECT_ID(N'[dbo].[Document]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Document_UploadDate_DF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Document] ADD  CONSTRAINT [Document_UploadDate_DF]  DEFAULT (getdate()) FOR [UploadDate]
END


End
GO
/****** Object:  Default [DF__DocumentT__IsUpl__5006DFF2]    Script Date: 11/21/2011 17:51:59 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DocumentT__IsUpl__5006DFF2]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DocumentT__IsUpl__5006DFF2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DocumentType] ADD  CONSTRAINT [DF__DocumentT__IsUpl__5006DFF2]  DEFAULT ((0)) FOR [IsUploadable]
END


End
GO
/****** Object:  Default [DF__DocumentT__IsIma__50FB042B]    Script Date: 11/21/2011 17:51:59 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DocumentT__IsIma__50FB042B]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DocumentT__IsIma__50FB042B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DocumentType] ADD  CONSTRAINT [DF__DocumentT__IsIma__50FB042B]  DEFAULT ((0)) FOR [IsImage]
END


End
GO
/****** Object:  Default [DF__DocumentT__Escap__51EF2864]    Script Date: 11/21/2011 17:51:59 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DocumentT__Escap__51EF2864]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DocumentT__Escap__51EF2864]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DocumentType] ADD  CONSTRAINT [DF__DocumentT__Escap__51EF2864]  DEFAULT ((1)) FOR [EscapeBeforeDisplay]
END


End
GO
/****** Object:  ForeignKey [FK_Affiliate_AffiliateType]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Affiliate_AffiliateType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Affiliate]'))
ALTER TABLE [dbo].[Affiliate]  WITH CHECK ADD  CONSTRAINT [FK_Affiliate_AffiliateType] FOREIGN KEY([AffiliateTypeId])
REFERENCES [dbo].[AffiliateType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Affiliate_AffiliateType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Affiliate]'))
ALTER TABLE [dbo].[Affiliate] CHECK CONSTRAINT [FK_Affiliate_AffiliateType]
GO
/****** Object:  ForeignKey [FK_Affiliate_City]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Affiliate_City]') AND parent_object_id = OBJECT_ID(N'[dbo].[Affiliate]'))
ALTER TABLE [dbo].[Affiliate]  WITH CHECK ADD  CONSTRAINT [FK_Affiliate_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Affiliate_City]') AND parent_object_id = OBJECT_ID(N'[dbo].[Affiliate]'))
ALTER TABLE [dbo].[Affiliate] CHECK CONSTRAINT [FK_Affiliate_City]
GO
/****** Object:  ForeignKey [FK_Affiliate_State]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Affiliate_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[Affiliate]'))
ALTER TABLE [dbo].[Affiliate]  WITH CHECK ADD  CONSTRAINT [FK_Affiliate_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Affiliate_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[Affiliate]'))
ALTER TABLE [dbo].[Affiliate] CHECK CONSTRAINT [FK_Affiliate_State]
GO
/****** Object:  ForeignKey [FK_AffiliateContact_Affiliate]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_Affiliate]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateContact_Affiliate] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_Affiliate]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact] CHECK CONSTRAINT [FK_AffiliateContact_Affiliate]
GO
/****** Object:  ForeignKey [FK_AffiliateContact_City]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_City]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateContact_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_City]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact] CHECK CONSTRAINT [FK_AffiliateContact_City]
GO
/****** Object:  ForeignKey [FK_AffiliateContact_Role]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateContact_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact] CHECK CONSTRAINT [FK_AffiliateContact_Role]
GO
/****** Object:  ForeignKey [FK_AffiliateContact_SecurityQuestion]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_SecurityQuestion]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateContact_SecurityQuestion] FOREIGN KEY([SecurityQuestionId])
REFERENCES [dbo].[SecurityQuestion] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_SecurityQuestion]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact] CHECK CONSTRAINT [FK_AffiliateContact_SecurityQuestion]
GO
/****** Object:  ForeignKey [FK_AffiliateContact_State]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateContact_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AffiliateContact_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliateContact]'))
ALTER TABLE [dbo].[AffiliateContact] CHECK CONSTRAINT [FK_AffiliateContact_State]
GO
/****** Object:  ForeignKey [FK_City_County]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_City_County]') AND parent_object_id = OBJECT_ID(N'[dbo].[City]'))
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_County] FOREIGN KEY([CountyId])
REFERENCES [dbo].[County] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_City_County]') AND parent_object_id = OBJECT_ID(N'[dbo].[City]'))
ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_City_County]
GO
/****** Object:  ForeignKey [FK_City_State]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_City_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[City]'))
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_City_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[City]'))
ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_City_State]
GO
/****** Object:  ForeignKey [FK_County_State]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_County_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[County]'))
ALTER TABLE [dbo].[County]  WITH CHECK ADD  CONSTRAINT [FK_County_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_County_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[County]'))
ALTER TABLE [dbo].[County] CHECK CONSTRAINT [FK_County_State]
GO
/****** Object:  ForeignKey [FK_Document_AffiliateContact]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Document_AffiliateContact]') AND parent_object_id = OBJECT_ID(N'[dbo].[Document]'))
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_AffiliateContact] FOREIGN KEY([AffiliateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Document_AffiliateContact]') AND parent_object_id = OBJECT_ID(N'[dbo].[Document]'))
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_AffiliateContact]
GO
/****** Object:  ForeignKey [FK_Document_DocumentType]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Document_DocumentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Document]'))
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_DocumentType] FOREIGN KEY([DocumentTypeId])
REFERENCES [dbo].[DocumentType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Document_DocumentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Document]'))
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_DocumentType]
GO
/****** Object:  ForeignKey [FK_DocumentRelation_Document]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentRelation_Document]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentRelation]'))
ALTER TABLE [dbo].[DocumentRelation]  WITH CHECK ADD  CONSTRAINT [FK_DocumentRelation_Document] FOREIGN KEY([DocumentID])
REFERENCES [dbo].[Document] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentRelation_Document]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentRelation]'))
ALTER TABLE [dbo].[DocumentRelation] CHECK CONSTRAINT [FK_DocumentRelation_Document]
GO
/****** Object:  ForeignKey [FK_DocumentRelation_ForeignTable]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentRelation_ForeignTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentRelation]'))
ALTER TABLE [dbo].[DocumentRelation]  WITH CHECK ADD  CONSTRAINT [FK_DocumentRelation_ForeignTable] FOREIGN KEY([ForeignTableID])
REFERENCES [dbo].[ForeignTable] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentRelation_ForeignTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentRelation]'))
ALTER TABLE [dbo].[DocumentRelation] CHECK CONSTRAINT [FK_DocumentRelation_ForeignTable]
GO
/****** Object:  ForeignKey [FK_DocumentType_DocumentType]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentType_DocumentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentType]'))
ALTER TABLE [dbo].[DocumentType]  WITH CHECK ADD  CONSTRAINT [FK_DocumentType_DocumentType] FOREIGN KEY([Id])
REFERENCES [dbo].[DocumentType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentType_DocumentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentType]'))
ALTER TABLE [dbo].[DocumentType] CHECK CONSTRAINT [FK_DocumentType_DocumentType]
GO
/****** Object:  ForeignKey [FK_DocumentType_SiteImage]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentType_SiteImage]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentType]'))
ALTER TABLE [dbo].[DocumentType]  WITH CHECK ADD  CONSTRAINT [FK_DocumentType_SiteImage] FOREIGN KEY([SiteImageID])
REFERENCES [dbo].[SiteImage] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentType_SiteImage]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentType]'))
ALTER TABLE [dbo].[DocumentType] CHECK CONSTRAINT [FK_DocumentType_SiteImage]
GO
/****** Object:  ForeignKey [FK_Permissions_Area]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permissions_Area]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permissions]'))
ALTER TABLE [dbo].[Permissions]  WITH CHECK ADD  CONSTRAINT [FK_Permissions_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permissions_Area]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permissions]'))
ALTER TABLE [dbo].[Permissions] CHECK CONSTRAINT [FK_Permissions_Area]
GO
/****** Object:  ForeignKey [FK_Permissions_Role]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permissions_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permissions]'))
ALTER TABLE [dbo].[Permissions]  WITH CHECK ADD  CONSTRAINT [FK_Permissions_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permissions_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permissions]'))
ALTER TABLE [dbo].[Permissions] CHECK CONSTRAINT [FK_Permissions_Role]
GO
/****** Object:  ForeignKey [FK_Role_Affiliate]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Role_Affiliate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Role]'))
ALTER TABLE [dbo].[Role]  WITH CHECK ADD  CONSTRAINT [FK_Role_Affiliate] FOREIGN KEY([CreateByAffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Role_Affiliate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Role]'))
ALTER TABLE [dbo].[Role] CHECK CONSTRAINT [FK_Role_Affiliate]
GO
/****** Object:  ForeignKey [FK_Role_AffiliateType]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Role_AffiliateType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Role]'))
ALTER TABLE [dbo].[Role]  WITH CHECK ADD  CONSTRAINT [FK_Role_AffiliateType] FOREIGN KEY([AffiliateTypeId])
REFERENCES [dbo].[AffiliateType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Role_AffiliateType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Role]'))
ALTER TABLE [dbo].[Role] CHECK CONSTRAINT [FK_Role_AffiliateType]
GO
/****** Object:  ForeignKey [FK_Shipment_Affiliate]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_Affiliate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipment_Affiliate] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_Affiliate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment] CHECK CONSTRAINT [FK_Shipment_Affiliate]
GO
/****** Object:  ForeignKey [FK_Shipment_PickupCity]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_PickupCity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipment_PickupCity] FOREIGN KEY([PickupCityId])
REFERENCES [dbo].[City] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_PickupCity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment] CHECK CONSTRAINT [FK_Shipment_PickupCity]
GO
/****** Object:  ForeignKey [FK_Shipment_PickupState]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_PickupState]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipment_PickupState] FOREIGN KEY([PickupStateId])
REFERENCES [dbo].[State] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_PickupState]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment] CHECK CONSTRAINT [FK_Shipment_PickupState]
GO
/****** Object:  ForeignKey [FK_Shipment_ShipToAffiliate]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_ShipToAffiliate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipment_ShipToAffiliate] FOREIGN KEY([ShipToAffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_ShipToAffiliate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment] CHECK CONSTRAINT [FK_Shipment_ShipToAffiliate]
GO
/****** Object:  ForeignKey [FK_Shipment_ShipToCity]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_ShipToCity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipment_ShipToCity] FOREIGN KEY([ShipToCityId])
REFERENCES [dbo].[City] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_ShipToCity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment] CHECK CONSTRAINT [FK_Shipment_ShipToCity]
GO
/****** Object:  ForeignKey [FK_Shipment_ShipToState]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_ShipToState]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipment_ShipToState] FOREIGN KEY([ShipToStateId])
REFERENCES [dbo].[State] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Shipment_ShipToState]') AND parent_object_id = OBJECT_ID(N'[dbo].[Shipment]'))
ALTER TABLE [dbo].[Shipment] CHECK CONSTRAINT [FK_Shipment_ShipToState]
GO
/****** Object:  ForeignKey [FK_ShipmentItem_ConditionType]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ShipmentItem_ConditionType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ShipmentItem]'))
ALTER TABLE [dbo].[ShipmentItem]  WITH CHECK ADD  CONSTRAINT [FK_ShipmentItem_ConditionType] FOREIGN KEY([ReceivedCondition])
REFERENCES [dbo].[ConditionType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ShipmentItem_ConditionType]') AND parent_object_id = OBJECT_ID(N'[dbo].[ShipmentItem]'))
ALTER TABLE [dbo].[ShipmentItem] CHECK CONSTRAINT [FK_ShipmentItem_ConditionType]
GO
/****** Object:  ForeignKey [FK_ShipmentItem_ShipmentItem]    Script Date: 11/21/2011 17:51:59 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ShipmentItem_ShipmentItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[ShipmentItem]'))
ALTER TABLE [dbo].[ShipmentItem]  WITH CHECK ADD  CONSTRAINT [FK_ShipmentItem_ShipmentItem] FOREIGN KEY([ShipmentId])
REFERENCES [dbo].[Shipment] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ShipmentItem_ShipmentItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[ShipmentItem]'))
ALTER TABLE [dbo].[ShipmentItem] CHECK CONSTRAINT [FK_ShipmentItem_ShipmentItem]
GO
