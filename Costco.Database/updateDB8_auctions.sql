

/****** Object:  Table [dbo].[SalesOrder]    Script Date: 08/16/2012 09:12:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Alter Table dbo.BulkShipmentItem add Pulled int not null default 0;
Alter Table dbo.ShipmentItem add Pulled int not null default 0;

CREATE TABLE [dbo].[SalesOrder](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[AffiliateId] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[SoldToAffiliateId] [int] NULL,
	[SoldToName] [nvarchar](100) NULL,
	[SoldToAddress] [nvarchar](200) NULL,
	[SoldToAddress2] [nvarchar](200) NULL,
	[SoldToZip] [nvarchar](20) NULL,
	[SoldToStateId] [int] NULL,
	[SoldToCityId] [int] NULL,
	[TotalWeight] [int] NULL,
	[SoldAmount] [money] NULL,
	[SoldDate] [datetime] NULL,
	[Available] [bit] NOT NULL,
	[Comment] [nvarchar](500) NULL,
	[TotalPallets] [int] NULL,
 CONSTRAINT [PK_SalesOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrder_Affiliate] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_Affiliate]
GO

ALTER TABLE [dbo].[SalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrder_City] FOREIGN KEY([SoldToCityId])
REFERENCES [dbo].[City] ([Id])
GO

ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_City]
GO

ALTER TABLE [dbo].[SalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrder_State] FOREIGN KEY([SoldToStateId])
REFERENCES [dbo].[State] ([Id])
GO

ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_State]
GO

ALTER TABLE [dbo].[SalesOrder] ADD  CONSTRAINT [DF_SalesOrder_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [dbo].[SalesOrder] ADD  CONSTRAINT [DF_SalesOrder_Available]  DEFAULT ((0)) FOR [Available]
GO


CREATE TABLE [dbo].[SalesOrderBulkItem](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[SalesOrderId] [int] NOT NULL,
	[BulkShipmentId] [int] NOT NULL,
	[BulkShipmentItemId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_SalesOrderBulkItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SalesOrderBulkItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderBulkItem_BulkShipment] FOREIGN KEY([BulkShipmentId])
REFERENCES [dbo].[BulkShipment] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderBulkItem] CHECK CONSTRAINT [FK_SalesOrderBulkItem_BulkShipment]
GO

ALTER TABLE [dbo].[SalesOrderBulkItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderBulkItem_BulkShipmentItem] FOREIGN KEY([BulkShipmentItemId])
REFERENCES [dbo].[BulkShipmentItem] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderBulkItem] CHECK CONSTRAINT [FK_SalesOrderBulkItem_BulkShipmentItem]
GO

ALTER TABLE [dbo].[SalesOrderBulkItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderBulkItem_SalesOrder] FOREIGN KEY([SalesOrderId])
REFERENCES [dbo].[SalesOrder] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderBulkItem] CHECK CONSTRAINT [FK_SalesOrderBulkItem_SalesOrder]
GO

ALTER TABLE [dbo].[SalesOrderBulkItem] ADD  CONSTRAINT [DF_SalesOrderBulkItem_Quantity]  DEFAULT ((0)) FOR [Quantity]
GO



CREATE TABLE [dbo].[SalesOrderItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesOrderId] [int] NOT NULL,
	[ShipmentId] [int] NOT NULL,
	[ShipmentItemId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_SalesOrderItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SalesOrderItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderItem_SalesOrder] FOREIGN KEY([SalesOrderId])
REFERENCES [dbo].[SalesOrder] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderItem] CHECK CONSTRAINT [FK_SalesOrderItem_SalesOrder]
GO

ALTER TABLE [dbo].[SalesOrderItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderItem_Shipment] FOREIGN KEY([ShipmentId])
REFERENCES [dbo].[Shipment] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderItem] CHECK CONSTRAINT [FK_SalesOrderItem_Shipment]
GO

ALTER TABLE [dbo].[SalesOrderItem]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrderItem_ShipmentItem] FOREIGN KEY([ShipmentItemId])
REFERENCES [dbo].[ShipmentItem] ([Id])
GO

ALTER TABLE [dbo].[SalesOrderItem] CHECK CONSTRAINT [FK_SalesOrderItem_ShipmentItem]
GO

ALTER TABLE [dbo].[SalesOrderItem] ADD  CONSTRAINT [DF_SalesOrderItem_Quantity]  DEFAULT ((0)) FOR [Quantity]
GO


CREATE TABLE [dbo].[Bid](
	[Id] [bigint] IDENTITY(1000,1) NOT NULL,
	[SalesOrderId] [int] NOT NULL,
	[AffiliateId] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Amount] [money] NOT NULL,
 CONSTRAINT [PK_Bid] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_Affiliate] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_Affiliate]
GO

ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_SalesOrder] FOREIGN KEY([SalesOrderId])
REFERENCES [dbo].[SalesOrder] ([Id])
GO

ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_SalesOrder]
GO

ALTER TABLE [dbo].[Bid] ADD  CONSTRAINT [DF_Bid_Created]  DEFAULT (getdate()) FOR [Created]
GO





CREATE View [dbo].[V_ShipmentInventory]
as 
Select 
si.ShipmentId,
Id ShipmentItemId,
ItemId,
Description,
Coalesce(ReceivedQuantity,0) Quantity,
Coalesce(Allocated,0) Allocated,
Coalesce(Pulled,0) Pulled,
Coalesce(ReceivedQuantity,0) - Coalesce(Allocated,0) - Coalesce(Pulled,0) Available
from ShipmentItem si
left join 
(select 
	ShipmentId, 
	ShipmentItemId, 
	SUM(Quantity) Allocated
from
SalesOrderItem
group by 
	ShipmentId, 
	ShipmentItemId) al on al.ShipmentItemId = si.Id
	
GO


CREATE View [dbo].[V_BulkShipmentInventory]
as 
Select 
si.BulkShipmentId ShipmentId,
Id ShipmentItemId,
ItemId,
Description,
Coalesce(Qty,0) Quantity,
Coalesce(Allocated,0) Allocated,
Coalesce(Pulled,0) Pulled,
Coalesce(Qty,0) - Coalesce(Allocated,0) - Coalesce(Pulled,0) Available
from BulkShipmentItem si
left join 
(select 
	BulkShipmentId, 
	BulkShipmentItemId, 
	SUM(Quantity) Allocated
from
SalesOrderBulkItem
group by 
	BulkShipmentId, 
	BulkShipmentItemId) al on al.BulkShipmentItemId = si.Id
	

GO




Create View [dbo].[V_ShipmentAvailableToSellAll]
as
Select Distinct
	'S' Type
	,s.Id
	,S.ReceivedDate
	,s.ShipToAffiliateId 
From Shipment s
inner join V_ShipmentInventory si on si.ShipmentId = s.Id
where  ReceivedDate is not null
and si.Available >= 1
union all
Select Distinct
	'B' Type
	,s.Id
	,S.ReceivedDate
	,s.ShipToAffiliateId 
From BulkShipment s
inner join V_BulkShipmentInventory si on si.ShipmentId = s.Id
where  ReceivedDate is not null
and si.Available >= 1
GO




CREATE View [dbo].[V_ShipmentsAvailableToSell]
as
select s.* 
from Shipment s
where ReceivedDate is not null
and Id in (select ShipmentId 
		   from V_ShipmentInventory 
		   group by ShipmentId 
		   having sum(Available) >= 1)


GO




CREATE View [dbo].[V_SalesOrderItemAll]
as
Select 
	'S' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.ShipmentId 
	,soi.ShipmentItemId
	,soi.Quantity
	,si.ItemId
	,si.Description
FROM SalesOrderItem soi
Inner Join ShipmentItem si on si.Id = soi.ShipmentItemId
Union All
Select 
	'B' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.BulkShipmentId 
	,soi.BulkShipmentItemId
	,soi.Quantity
	,si.ItemId
	,si.Description
FROM SalesOrderBulkItem soi
Inner Join BulkShipmentItem si on si.Id = soi.BulkShipmentItemId



 
GO


Create view [dbo].[V_MaxBid]
as 
select 
	s.Id SalesOrderId,
	s.Created,
	s.AffiliateId ASR_ID,
	a.Name ASR_Name,
	Max(coalesce(b.Amount,0)) Amount,
	Count(b.Id) BidCount
from SalesOrder s 
inner join Affiliate a on a.Id = s.AffiliateId
inner join Bid b on b.SalesOrderId = s.Id
inner join Affiliate a2 on a2.Id = b.AffiliateId
group by 
	s.Id ,
	s.Created,
	s.AffiliateId,
	a.Name
GO

CREATE view [dbo].[V_MaxAffiliateBid]
as 
select 
	s.Id SalesOrderId,
	s.Created,
	s.AffiliateId ASR_ID,
	a.Name ASR_Name,
	b.AffiliateId Buyer_Id,
	Coalesce(a2.Name,'') Buyer_Name,
	c.Name Buyer_City,
	st.Abbreviation Buyer_State,
	a2.Zip Buyer_Zip,
	Max(coalesce(b.Amount,0)) Amount,
	Count(b.Id) BidCount,
	case 
		when s.SoldDate is not null and s.SoldToAffiliateId = b.AffiliateId 
		then 'Won'
		when s.SoldDate is not null and s.SoldToAffiliateId <> b.AffiliateId 
		then 'Lost'
		else 'Open'
	end Status
from SalesOrder s 
inner join Affiliate a on a.Id = s.AffiliateId
inner join Bid b on b.SalesOrderId = s.Id
inner join Affiliate a2 on a2.Id = b.AffiliateId
inner join City c on c.Id = a2.CityId
inner join State st on st.Id = a2.StateId
--where s.Available = 1 and SoldDate is null
group by 
	s.Id ,
	s.Created,
	s.AffiliateId,
	a.Name,
	b.AffiliateId,
	a2.Name,
	c.Name,
	st.Abbreviation,
	a2.Zip,
	s.SoldDate,
	s.SoldToAffiliateId


GO


/****** Object:  Table [dbo].[Area]    Script Date: 08/16/2012 09:29:09 ******/
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (30, 4, N'SalesASR', N'Sales ASR', NULL, 1, 0, 10)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (31, 30, N'SalesOrdersASR', N'Sales Orders', N'SalesOrder/Index', 1, 1, 20)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (33, 30, N'PullBulkShipmentItem', N'Pull Bulk Item', N'PullShipmentItem/BulkIndex', 1, 1, 12)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (34, 30, N'PullShipmentItem', N'Pull Item', N'PullShipmentItem/Index', 1, 1, 10)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (35, 4, N'SalesOrdersSeller', N'Sales E-World', N'SalesOrder/IndexSeller', 1, 1, 20)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (38, 26, N'SalesOrderItemReport', N'Sales Order Report', NULL, 0, 1, 21)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (39, 4, N'SalesOrdersBuyer', N'Bids', N'SalesOrder/IndexBuyer', 1, 1, 30)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (40, 26, N'GenericReport', N'Generic Report', N'javascript:return showReport(''/Reports/GenericReport.aspx?ReportName=SalesOrderItemReport'',''GenericReport'');', 1, 1, 20)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (41, 26, N'GRShippingActivity', N'Generic Report Shipping Activity', N'javascript:return showReport(''/Reports/GenericReport.aspx?ReportName=ShippingActivity'',''GRShippingActivity'');', 1, 1, 20)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (42, 26, N'AdminAllItemReport', N'All Item Report', N'javascript:return showReport(''/Reports/GenericReport.aspx?ReportName=CostcoAdminAllItemReport&DisplayName=AllItemReport'',''AdminAllItemReport'');', 1, 1, 30)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (43, 26, N'AdminItemByLocation', N'Items By Location', N'javascript:return showReport(''/Reports/GenericReport.aspx?ReportName=CostcoAdminItemsByLocation&DisplayName=ItemsByLocation'',''AdminItemByLocation'');', 1, 1, 40)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (44, 26, N'AdminTopTenByState', N'Top Ten by State', N'javascript:return showReport(''/Reports/GenericReport.aspx?ReportName=CostcoAdminTopTenByState&DisplayName=TopTenByState'',''AdminTopTenByState'');', 1, 1, 50)
INSERT [Area] ([Id], [ParentId], [Name], [Caption], [URL], [IsMenu], [IsArea], [Order]) VALUES (45, 26, N'AdminTopTenByLocation', N'Top Ten by Location', N'javascript:return showReport(''/Reports/GenericReport.aspx?ReportName=CostcoAdminTopTenByLocation&DisplayName=TopTenByState'',''AdminTopTenByLocation'');', 1, 1, 60)


/****** Object:  Table [dbo].[Role]    Script Date: 08/16/2012 09:29:09 ******/
go

SET IDENTITY_INSERT [dbo].[AffiliateType] ON
INSERT [dbo].[AffiliateType] ([Id], [Type], [Description]) VALUES (7, N'Buyer', N'Buyer')
SET IDENTITY_INSERT [dbo].[AffiliateType] OFF


SET IDENTITY_INSERT [Role] ON
INSERT [Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (7, 5, N'RecyclerSales', NULL)
INSERT [Role] ([Id], [AffiliateTypeId], [RoleName], [CreateByAffiliateId]) VALUES (8, 7, N'Buyer', NULL)
SET IDENTITY_INSERT [Role] OFF
go

/****** Object:  Table [dbo].[Permissions]    Script Date: 08/16/2012 09:29:09 ******/
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 8, 1, 1, 0, 0, 0)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 8, 39, 1, 1, 1, 1)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 8, 38, 1, 0, 0, 0)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 8, 2, 1, 0, 0, 0)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 1, 1, 1, 1, 1)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 25, 1, 0, 0, 0)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 23, 1, 0, 0, 0)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 34, 1, 1, 1, 1)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 33, 1, 1, 1, 1)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 31, 1, 1, 1, 1)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 38, 1, 0, 0, 0)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 2, 1, 0, 0, 0)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 7, 16, 0, 1, 1, 1)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 35, 1, 1, 1, 1)
INSERT [Permissions] ([RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 38, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 40, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 41, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 38, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 42, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 43, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 44, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 1, 45, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 2, 42, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 2, 43, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 2, 44, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 2, 45, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 3, 42, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 3, 43, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 3, 44, 1, 0, 0, 0)
INSERT [Permissions] ( [RoleId], [AreaId], [Read], [Add], [Update], [Delete]) VALUES ( 3, 45, 1, 0, 0, 0)

go

