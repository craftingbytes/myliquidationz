

CREATE view [dbo].[V_ShipmentItemExtended]   as
SELECT 
	si.[Id]
    ,si.[ShipmentId]
	,si.[PalletUnitNumber]
	,si.[ItemId]
	,si.[Description]
	,si.[Condition]
	,si.[Quantity]
	,si.[ReceivedQuantity]
	,coalesce(c.Condition,'')  [ReceivedCondition]
	,si.[CoveredDevice]
	,si.[ReceivedCondition2]
	,si.[Disposition]
	,si.[Weight]
	,si.[Pulled]
	,p.Description DefaultDescription
	,coalesce(pp.Price,0) Price
FROM [ShipmentItem] si
Left Join ConditionType	 c on c.Id = si.ReceivedCondition
INNER JOIN Product  p on p.ItemId = si.ItemId
LEFT JOIN (
select
	ItemId
	,Max(Id) Id
from ProductPrice 
group by ItemId) mp on mp.ItemId = p.ItemId
left join ProductPrice pp on pp.Id = mp.Id and pp.ItemId = si.ItemId		  


