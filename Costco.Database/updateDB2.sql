--alter table ShipmentItem add  ReceivedCondition2 nvarchar(200) null

--alter table BulkShipmentItem add  ReceivedCondition2 nvarchar(200) null

SET IDENTITY_INSERT ConditionType ON
Insert into ConditionType(Id, Condition)
values (99,'Other')

update ShipmentItem 
set ReceivedCondition = 99
where ReceivedCondition = 7

update BulkShipmentItem 
set ReceivedCondition = 99
where ReceivedCondition = 7

update ConditionType
set Condition = 'Opened/Damaged box'
where Id = 7 

Insert into ConditionType(Id, Condition)
values (8,'Untested')

Insert into ConditionType(Id, Condition)
values (9,'Incomplete')

SET IDENTITY_INSERT ConditionType OFF

ALTER TABLE dbo.BulkShipment ADD
	PickupCompanyName varchar(50) NULL,
	PickupAddress varchar(100) NULL,
	PickupStateId int NULL,
	PickupCityId int NULL,
	PickupZip varchar(20) NULL,
	PickupDate datetime NULL
GO

ALTER TABLE dbo.BulkShipment ADD CONSTRAINT
	FK_BulkShipment_State FOREIGN KEY
	(
	PickupStateId
	) REFERENCES dbo.State
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.BulkShipment ADD CONSTRAINT
	FK_BulkShipment_City FOREIGN KEY
	(
	PickupCityId
	) REFERENCES dbo.City
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO