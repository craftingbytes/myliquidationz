ALTER TABLE dbo.Shipment ADD
	SalvageableWeight int NULL,
	ReadyToShip bit NOT NULL CONSTRAINT DF_Shipment_ReadyToShip DEFAULT 0
GO

update Shipment
set readytoship = 1
where ReceivedDate is not null