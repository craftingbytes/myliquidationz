

CREATE TABLE [ProductType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](75) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE dbo.Product ADD
	ProductTypeId int NULL
GO
ALTER TABLE dbo.Product ADD CONSTRAINT
	FK_Product_ProductType FOREIGN KEY
	(
	ProductTypeId
	) REFERENCES dbo.ProductType
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

insert into ProductType (Name,Description)
values('CRT', '')
,('Laptop', '')
,('Mobile Phone', '')
,('Printers', '')
,('Peripherals', '')
,('Monitors', '')
,('EEDs', '')
,('Reportable Devices', '')
,('TVs', '')
,('Computer', '')
,('TVs/Monitors', '')
,('Small Electronic Equipment', '')
,('Computer Peripherals', '')
,('Small Scale Servers', '')


ALTER View [dbo].[v_MaxProductPrice]
as
select 
	p.Id,
	p.ItemId,
	p.Description,
	p.Description2,
	coalesce(p.Weight,0) Weight,
	p.CoveredDevice,
	coalesce(pp.Price,0) Price,
	coalesce(pp.TimeStamp, Cast('01/01/1999' as datetime)) Timestamp,
	count(si.itemid) Count,
	coalesce(p.ProductTypeId,0) ProductTypeId,
	coalesce(pt.Name,'Not Set') ProductType,
	coalesce(pd.Disposition,'Not Set') DefaultDisposition
from Product p
left join 
	(	select 
			max(Id) Id,
			ItemId 
		from ProductPrice 
		group by ItemId) mp on mp.ItemId = p.ItemId
left join ProductPrice pp on pp.Id = mp.Id
left join ShipmentItem si on si.ItemId = p.ItemId
left join ProductDisposition pd on pd.ItemId = p.ItemId
left join ProductType pt on pt.Id = p.ProductTypeId
group by 
	p.Id,
	p.ItemId,
	p.Description,
	p.Description2,
	p.Weight,
	p.CoveredDevice,
	pp.Price,
	pp.TimeStamp,
	p.ProductTypeId,
	pt.Name,
	pd.Disposition