CREATE TABLE [dbo].[InvoiceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_InvoiceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE TABLE [dbo].[Invoice](
	[Id] [int] IDENTITY(10000,1) NOT NULL,
	[InvoiceTypeId] [int] NOT NULL,
	[AffiliateId] [int] NOT NULL,
	[InvoiceDate] [datetime] NOT NULL,
	[Rate] [money] NOT NULL,
	[Notes] [nvarchar](200) NULL,
	[Updated] [date] NOT NULL,
 CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Affiliate] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_Affiliate]
GO

ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_InvoiceType] FOREIGN KEY([InvoiceTypeId])
REFERENCES [dbo].[InvoiceType] ([Id])
GO

ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_InvoiceType]
GO

ALTER TABLE [dbo].[Invoice] ADD  CONSTRAINT [DF_Invoice_InvoiceDate]  DEFAULT (getdate()) FOR [InvoiceDate]
GO

ALTER TABLE [dbo].[Invoice] ADD  CONSTRAINT [DF_Invoice_Rate]  DEFAULT ((0.0)) FOR [Rate]
GO

ALTER TABLE [dbo].[Invoice] ADD  CONSTRAINT [DF_Invoice_Updated]  DEFAULT (getdate()) FOR [Updated]
GO


ALTER TABLE dbo.Shipment ADD
	Complete bit NOT NULL CONSTRAINT DF_Shipment_Complete DEFAULT 0
GO


ALTER TABLE dbo.Shipment ADD
	Invoiceid int NULL
GO
ALTER TABLE dbo.Shipment ADD CONSTRAINT
	FK_Shipment_Invoice FOREIGN KEY
	(
	Invoiceid
	) REFERENCES dbo.Invoice
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

SET IDENTITY_INSERT [dbo].[InvoiceType] ON
INSERT [dbo].[InvoiceType] ([Id], [Name], [Description]) VALUES (1, N'Bill Costco', N'Invoice Billing Costco')
INSERT [dbo].[InvoiceType] ([Id], [Name], [Description]) VALUES (2, N'Payable ASR', N'Invoice Payable To ASR')
INSERT [dbo].[InvoiceType] ([Id], [Name], [Description]) VALUES (3, N'Payable Costco', N'Invoice Payable to Costco')
INSERT [dbo].[InvoiceType] ([Id], [Name], [Description]) VALUES (4, N'Bill ASR', N'Invoice Billing ASR')
SET IDENTITY_INSERT [dbo].[InvoiceType] OFF

Go


ALTER TABLE dbo.Invoice ADD
	ParentInvoiceId int NULL
GO
ALTER TABLE dbo.Invoice ADD CONSTRAINT
	FK_Invoice_Invoice FOREIGN KEY
	(
	ParentInvoiceId
	) REFERENCES dbo.Invoice
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO



CREATE TABLE [dbo].[ServiceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ServiceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE TABLE [dbo].[AffiliateRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AffiliateId] [int] NOT NULL,
	[ServiceTypeId] [int] NOT NULL,
	[Rate] [money] NOT NULL,
 CONSTRAINT [PK_AffiliateRate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AffiliateRate]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateRate_Affiliate] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[Affiliate] ([Id])
GO

ALTER TABLE [dbo].[AffiliateRate] CHECK CONSTRAINT [FK_AffiliateRate_Affiliate]
GO

ALTER TABLE [dbo].[AffiliateRate]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateRate_ServiceType] FOREIGN KEY([ServiceTypeId])
REFERENCES [dbo].[ServiceType] ([Id])
GO

ALTER TABLE [dbo].[AffiliateRate] CHECK CONSTRAINT [FK_AffiliateRate_ServiceType]
GO




CREATE TABLE [dbo].[InvoiceAdjustment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Amount] [money] NOT NULL,
	[AdjustmentDate] [datetime] NULL,
 CONSTRAINT [PK_InvoiceAdjustment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InvoiceAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceAdjustment_Invoice] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoice] ([Id])
GO

ALTER TABLE [dbo].[InvoiceAdjustment] CHECK CONSTRAINT [FK_InvoiceAdjustment_Invoice]
GO

ALTER TABLE [dbo].[InvoiceAdjustment] ADD  CONSTRAINT [DF_InvoiceAdjustment_AdjustmentDate]  DEFAULT (getdate()) FOR [AdjustmentDate]
GO

Create view V_InvoiceCostcoBill
as
select 
	i.Id
	,i.InvoiceTypeId
	,a.Name
	,i.AffiliateId
	,i.InvoiceDate
	,i.Rate
	,i.Notes
	,SUM(s.ReceivedTotalWeight)  TotalWeight
	,cast((i.Rate * SUM(s.ReceivedTotalWeight)) as money) SubTotal  
	,ia.AdjustmentCount AdjustmentCount
	,coalesce(ia.Amount,0) Ajustments
	,cast(((i.Rate * SUM(s.ReceivedTotalWeight)) + coalesce(ia.Amount,0) ) as money) Total   
from invoice i
inner join Affiliate a on a.Id = i.AffiliateId
inner join Shipment s on s.Invoiceid = i.Id
left join 
	(select 
		invoiceId, 
		COUNT(Id) AdjustmentCount, 
		SUM(coalesce(Amount,0)) Amount
	from InvoiceAdjustment
	group by 
		invoiceId) ia on ia.InvoiceId = i.Id
where InvoiceTypeId = 1
group by 
	i.Id
	,i.InvoiceTypeId
	,a.Name
	,i.AffiliateId
	,i.InvoiceDate
	,i.Rate
	,i.Notes
	,ia.AdjustmentCount
	,ia.Amount
go

Create  view V_InvoiceASRPayable
as
select 
	i.Id
	,i.ParentInvoiceId
	,i.InvoiceTypeId
	,a.Name
	,i.AffiliateId
	,i.InvoiceDate
	,i.Rate
	,i.Notes
	,SUM(s.ReceivedTotalWeight)  TotalWeight
	,cast((i.Rate * SUM(s.ReceivedTotalWeight)) as money) SubTotal  
	,ia.AdjustmentCount AdjustmentCount
	,coalesce(ia.Amount,0) Ajustments
	,cast(((i.Rate * SUM(s.ReceivedTotalWeight)) + coalesce(ia.Amount,0) ) as money) Total   
from invoice i
inner join Affiliate a on a.Id = i.AffiliateId
inner join Shipment s on s.Invoiceid = i.ParentInvoiceId and s.ShipToAffiliateId = i.AffiliateId
left join 
	(select 
		invoiceId, 
		COUNT(Id) AdjustmentCount, 
		SUM(coalesce(Amount,0)) Amount
	from InvoiceAdjustment
	group by 
		invoiceId) ia on ia.InvoiceId = i.Id
where InvoiceTypeId = 2
and a.AffiliateTypeId = 5
group by 
	i.Id
	,i.ParentInvoiceId
	,i.InvoiceTypeId
	,a.Name
	,i.AffiliateId
	,i.InvoiceDate
	,i.Rate
	,i.Notes
	,ia.AdjustmentCount
	,ia.Amount
go



ALTER View [dbo].[V_SalesOrderItemAll]
as
Select 
	'S' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.ShipmentId 
	,soi.ShipmentItemId
	,soi.Quantity
	,si.ItemId
	,si.Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
FROM SalesOrderItem soi
Inner Join ShipmentItem si on si.Id = soi.ShipmentItemId
Union All
Select 
	'B' Type 
	,soi.Id
	,soi.SalesOrderId
	,soi.BulkShipmentId 
	,soi.BulkShipmentItemId
	,soi.Quantity
	,si.ItemId
	,si.Description
	,si.Disposition
	,substring(si.ReceivedCondition2,1,20) ReceivedCondition2
FROM SalesOrderBulkItem soi
Inner Join BulkShipmentItem si on si.Id = soi.BulkShipmentItemId
go



ALTER View [dbo].[V_ShipmentInventory]
as 
Select 
si.ShipmentId,
Id ShipmentItemId,
ItemId,
Description,
Coalesce(ReceivedQuantity,0) Quantity,
Coalesce(Allocated,0) Allocated,
Coalesce(Pulled,0) Pulled,
Coalesce(ReceivedQuantity,0) - Coalesce(Allocated,0) - Coalesce(Pulled,0) Available
from ShipmentItem si
left join 
(select 
	ShipmentId, 
	ShipmentItemId, 
	SUM(Quantity) Allocated
from
SalesOrderItem
group by 
	ShipmentId, 
	ShipmentItemId) al on al.ShipmentItemId = si.Id
where si.Disposition = 'Salvage'

GO


/****** Object:  Table [dbo].[ServiceType]    Script Date: 12/09/2012 01:41:35 ******/
SET IDENTITY_INSERT [dbo].[ServiceType] ON
INSERT [dbo].[ServiceType] ([Id], [Name]) VALUES (1, N'Processing')
INSERT [dbo].[ServiceType] ([Id], [Name]) VALUES (2, N'PayableWeight')
INSERT [dbo].[ServiceType] ([Id], [Name]) VALUES (3, N'RevenueShare')
SET IDENTITY_INSERT [dbo].[ServiceType] OFF



/****** Object:  Table [dbo].[AffiliateRate]    Script Date: 12/09/2012 01:43:13 ******/
SET IDENTITY_INSERT [dbo].[AffiliateRate] ON
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (1, 1041, 1, 0.2500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (2, 533, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (3, 538, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (4, 539, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (5, 540, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (6, 1001, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (7, 1002, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (8, 1003, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (9, 1004, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (10, 1005, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (11, 1006, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (12, 1007, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (13, 1008, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (14, 1010, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (15, 1014, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (16, 1015, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (17, 1016, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (18, 1017, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (19, 1018, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (20, 1019, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (21, 1020, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (22, 1026, 1, 0.1500)
INSERT [dbo].[AffiliateRate] ([Id], [AffiliateId], [ServiceTypeId], [Rate]) VALUES (23, 1034, 1, 0.1500)
SET IDENTITY_INSERT [dbo].[AffiliateRate] OFF
