/****** Object:  Table [dbo].[AffiliateType]    Script Date: 11/21/2011 17:55:40 ******/
SET IDENTITY_INSERT [dbo].[AffiliateType] ON
INSERT [dbo].[AffiliateType] ([Id], [Type], [Description]) VALUES (0, N'Public', N'Public Anonymous User')
INSERT [dbo].[AffiliateType] ([Id], [Type], [Description]) VALUES (1, N'Eworld', N'Eworld')
INSERT [dbo].[AffiliateType] ([Id], [Type], [Description]) VALUES (2, N'CostcoCorp', N'Costco Corp Headquarters')
INSERT [dbo].[AffiliateType] ([Id], [Type], [Description]) VALUES (3, N'CostcoDistribution', N'Costco Distribution Center')
INSERT [dbo].[AffiliateType] ([Id], [Type], [Description]) VALUES (4, N'CostcoWarehouse', N'Costco Warehouse')
INSERT [dbo].[AffiliateType] ([Id], [Type], [Description]) VALUES (5, N'Recycler', N'Recycler')
SET IDENTITY_INSERT [dbo].[AffiliateType] OFF
