

CREATE TABLE [dbo].[ProductDisposition](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [nvarchar](100) NOT NULL,
	[Disposition] [nvarchar](50) NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[AffiliateContactId] [int] NULL,
 CONSTRAINT [PK_ProductDisposition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProductDisposition]  WITH CHECK ADD  CONSTRAINT [FK_ProductDisposition_AffiliateContact] FOREIGN KEY([AffiliateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO

ALTER TABLE [dbo].[ProductDisposition] CHECK CONSTRAINT [FK_ProductDisposition_AffiliateContact]
GO

ALTER TABLE [dbo].[ProductDisposition]  WITH CHECK ADD  CONSTRAINT [FK_ProductDisposition_Product] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Product] ([ItemId])
GO

ALTER TABLE [dbo].[ProductDisposition] CHECK CONSTRAINT [FK_ProductDisposition_Product]
GO

ALTER TABLE [dbo].[ProductDisposition] ADD  CONSTRAINT [DF_ProductDisposition_TimeStamp]  DEFAULT (getdate()) FOR [TimeStamp]
GO

ALTER TABLE [dbo].[ProductDisposition] ADD  CONSTRAINT [IX_ProductDisposition] UNIQUE NONCLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


