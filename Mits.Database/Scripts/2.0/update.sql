
--add missing columns
alter table InvoiceReconciliation add ReconcileDate datetime
alter table CollectionEvent add Location nvarchar(200)
alter table CollectionEvent add Note nvarchar(2000)


--fix OEM contractrate planyear
select *
--update cr set planyear = py.year
from AffiliateContractRate cr
inner join PlanYear py on py.StateId = cr.StateId and  py.StartDate = cr.RateStartDate
where cr.PlanYear is null
and servicetypeid = 10

--fix westinghouse
select * 
--update cr set ServiceTypeId = 10
from AffiliateContractRate cr 
where StateId = 38 and AffiliateId = 5819
and ServiceTypeId <> 10