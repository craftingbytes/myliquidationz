
/****** Object:  View [dbo].[v_InvoicesRequiringFix]    Script Date: 08/11/2011 11:41:28 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_InvoicesRequiringFix]'))
DROP VIEW [dbo].[v_InvoicesRequiringFix]
GO


/****** Object:  View [dbo].[v_InvoicesRequiringFix]    Script Date: 08/11/2011 11:41:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_InvoicesRequiringFix]  
AS
SELECT 
	old.InvoiceId, 
	old.quantity OldQuantity, 
	old.total OldTotal, 
	coalesce(new.quantity,0) NewQuantity, 
	coalesce(new.total,0) NewTotal, 
	coalesce(pay.PaidAmount,0) PaidAmount
FROM (
SELECT 
	inv.id InvoiceId,
	sum (coalesce(ii.Quantity,0) ) quantity,
	sum (coalesce(ii.Quantity,0) * coalesce(ii.Rate,0)) total
FROM dbo.invoice as inv 
INNER JOIN dbo.InvoiceItem AS ii ON ii.InvoiceId=inv.Id
INNER JOIN dbo.Affiliate a on a.Id = inv.AffiliateId
WHERE AffiliateTypeId = 1
AND inv.Id < 2047
group by inv.id) old 
LEFT JOIN
(SELECT 
	inv.Id InvoiceId,
	sum (coalesce(ir.Quantity,0) ) quantity,
	sum (coalesce(ir.Quantity,0) * coalesce(cr.Rate,0)) total
FROM dbo.invoice as inv 
INNER JOIN dbo.InvoiceReconciliation AS IR ON IR.OEMInvoiceId=inv.Id
INNER JOIN dbo.InvoiceItem AS ii ON ii.Id=IR.ProcesserInvoiceItemId  
LEFT OUTER JOIN AffiliateContractRate CR on CR.StateId=inv.StateId 
											and CR.AffiliateId=inv.AffiliateId
											and CR.ServiceTypeId=10 
											and CR.ProductTypeId= ii.ProductTypeId
											and inv.PlanYear = cr.PlanYear
WHERE inv.Id < 2047
GROUP BY inv.Id ) new on new.InvoiceId = old.InvoiceId
LEFT JOIN
(SELECT InvoiceId, Sum(PaidAmount) PaidAmount
FROM PaymentDetail 
WHERE InvoiceId < 2047
GROUP BY InvoiceId ) pay ON pay.InvoiceId = old.InvoiceId
WHERE old.quantity <> coalesce(new.quantity,0) OR old.total <> coalesce(new.total,0)
--WHERE old.quantity = coalesce(new.quantity,0) AND old.total = coalesce(new.total,0)
--ORDER BY old.InvoiceId 



 
GO

