IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_ReportInvoiceForOem]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_ReportInvoiceForOem]
GO



/****** Object:  StoredProcedure [dbo].[p_ReportInvoiceForOem]    Script Date: 08/04/2011 10:36:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[p_ReportInvoiceForOem] 
	 @InvoiceId as int
	 -- p_ReportInvoiceForOem 1320 
AS 
--set @InvoiceId = 1320
declare @InvoiceItem as table(
	PlanYear int,
	InvoiceId int,
	InvoiceItemId int,
	Quantity int,
	Rate money,
	ProductTypeId int,
	ServiceTypeId int,
	ServiceDate datetime 
)

IF 1=2
BEGIN
	SELECT CAST(NULL AS [int]) [PlanYear] ,
	CAST(NULL AS  [varchar](200) ) [AffiliateName],
	CAST(NULL AS   [varchar](200) ) [RecyclerAddress1] ,
	CAST(NULL AS  [varchar](66)  ) [RecyclerAddress2] ,
	CAST(NULL AS  [varchar](20)  ) [Phone]  ,
	CAST(NULL AS  [varchar](250)  ) [VendorCompanyName] ,
	CAST(NULL AS  [varchar](50)  ) [StateProvinceName] ,
	CAST(NULL AS  [varchar](200)   ) [VendorAddress],
	CAST(NULL AS  [varchar](65)  ) [VendorAddress1],
	CAST(NULL AS  [int]  ) [InvoiceID] ,
	CAST(NULL AS  [varchar](50) ) [InvStateName] ,
	CAST(NULL AS  [int] ) [AffiliateId],
	CAST(NULL AS  [varchar](10) ) [ProcessingDate],
	CAST(NULL AS  [smalldatetime]) [InvoiceDate]  ,
	CAST(NULL AS  [varchar](10) ) [InvoiceDueDate]  ,
	CAST(NULL AS  [varchar](10)) [ServiceDate] ,
	CAST(NULL AS   [varchar](10) ) [InvoicePeriodFrom] ,
	CAST(NULL AS   [varchar](10)  ) [InvoicePeriodTo] ,
	CAST(NULL AS   [varchar](50) ) [ShipmentID] ,
	CAST(NULL AS   [varchar](75)  ) [ProductTypeName],
	CAST(NULL AS   [varchar](100)) [ServiceTypeName]  ,
	CAST(NULL AS    [decimal] ) [Quantity] ,
	CAST(NULL AS   [money]) [Rate]  ,
	CAST(NULL AS    [money]) [Total] ,
	CAST(NULL AS    [varchar](200)) [PickupAddress] ,
	CAST(NULL AS   [varchar](50)) [ShipStateProvinceName]  ,
	CAST(NULL AS   [varchar](50)) [PickupCity]  ,
	CAST(NULL AS  [nchar](10) ) [PickupZip]  ,
	CAST(NULL AS  [varchar](20) ) [PickupPhone]  ,
	CAST(NULL AS   [varchar](20) ) [PickupFax] ,
	CAST(NULL AS   [varchar](100)) [PickupEmail]  ,
	CAST(NULL AS  [varchar](80) ) [CollectionMethod]  ,
	CAST(NULL AS  [varchar](2048) ) [Notes]  
END


INSERT INTO @InvoiceItem
SELECT 
	inv.PlanYear,
	inv.Id,
	ii.id, 
	ii.Quantity,
	ii.Rate,
	ii.ProductTypeId,
	ii.ServiceTypeId,
	ii.ServiceDate
FROM dbo.invoice as inv 
INNER JOIN dbo.InvoiceItem AS ii ON ii.InvoiceId=inv.Id
INNER JOIN dbo.Affiliate a on a.Id = inv.AffiliateId
INNER JOIN dbo.v_InvoicesRequiringFix f on f.invoiceId = inv.Id
WHERE AffiliateTypeId = 1
AND inv.Id = @InvoiceId   

if @@ROWCOUNT <= 0
BEGIN
	INSERT INTO @InvoiceItem
	SELECT 
		inv.PlanYear,
		inv.Id,
		ii.id, 
		ir.Quantity,
		cr.Rate,
		ii.ProductTypeId,
		ii.ServiceTypeId,
		ii.ServiceDate
	FROM dbo.invoice as inv 
	INNER JOIN dbo.InvoiceReconciliation AS IR ON IR.OEMInvoiceId=inv.Id
	INNER JOIN dbo.InvoiceItem AS ii ON ii.Id=IR.ProcesserInvoiceItemId  
	LEFT OUTER JOIN AffiliateContractRate CR on CR.StateId=inv.StateId 
												and CR.AffiliateId=inv.AffiliateId
												and CR.ServiceTypeId=10 
												and CR.ProductTypeId= ii.ProductTypeId
												and inv.PlanYear = cr.PlanYear
	WHERE
	inv.Id = @InvoiceId  
END




SELECT     
	inv.PlanYear,
	AF.Name AS AffiliateName, 
	ISNULL(AF.Address1, AF.Address2) AS RecyclerAddress1, 
	Ct3.Name + ', ' + St3.Abbreviation + ' ' + AF.Zip AS RecyclerAddress2, 
	AF.Phone, 
	PD.VendorCompanyName, 
	St1.Name AS StateProvinceName, 
	PD.VendorAddress, 
	Ct1.Name + ', ' + St1.Abbreviation + ' ' + PD.VendorZip AS VendorAddress1, 
	inv.Id AS InvoiceID, 
	St4.Name AS InvStateName, 
	inv.AffiliateId, 
	CONVERT(VARCHAR(10), PD.ProcessingDate, 101) AS ProcessingDate, 
	inv.InvoiceDate, 
	CONVERT(VARCHAR(10),inv.InvoiceDueDate, 101) AS InvoiceDueDate, 
	CONVERT(VARCHAR(10),ii.ServiceDate, 101) AS ServiceDate, 
	CONVERT(VARCHAR(10),py.StartDate, 101) AS InvoicePeriodFrom, 
	CONVERT(VARCHAR(10), py.EndDate, 101) AS InvoicePeriodTo, 
	PD.ShipmentNo AS ShipmentID, 
	PT.Name AS ProductTypeName, 
	ST.Name AS ServiceTypeName,
	ISNULL(CONVERT(int, ii.Quantity), 0) AS Quantity, 
	ISNULL(ii.Rate, 0) AS Rate, 
	ISNULL(ii.Quantity, 0) * ISNULL(ii.Rate, 0) AS Total, 
	PD.PickupAddress, 
	St2.Name AS ShipStateProvinceName, 
	Ct2.Name AS PickupCity, 
	PD.PickupZip, 
	PD.PickupPhone, 
	PD.PickupFax, 
	PD.PickupEmail, 
	cm.Method AS CollectionMethod, 
	inv.Notes 

FROM dbo.Invoice AS inv 
INNER JOIN dbo.State AS St4 ON inv.StateId = St4.Id 
INNER JOIN @InvoiceItem ii ON ii.InvoiceId = inv.Id
LEFT OUTER JOIN dbo.ProcessingData AS PD ON PD.InvoiceId = inv.Id 
LEFT OUTER JOIN dbo.State AS St1 ON PD.VendorStateId = St1.Id 
LEFT OUTER JOIN dbo.State AS St2 ON PD.PickupStateId = St2.Id 
LEFT OUTER JOIN dbo.CollectionMethod AS cm ON cm.Id = PD.CollectionMethodId 
LEFT OUTER JOIN dbo.City AS Ct1 ON Ct1.Id = PD.VendorCityId 
LEFT OUTER JOIN dbo.City AS Ct2 ON Ct2.Id = PD.PickupCityId 
LEFT OUTER JOIN dbo.Affiliate AS AF ON AF.Id  = inv.AffiliateId
LEFT OUTER JOIN dbo.City AS Ct3 ON AF.CityId = Ct3.Id
LEFT OUTER JOIN dbo.State AS St3 ON AF.StateId = St3.Id
LEFT OUTER JOIN dbo.ProductType AS PT ON PT.Id = ii.ProductTypeId 
LEFT OUTER JOIN dbo.ServiceType AS ST ON ST.Id = ii.ServiceTypeId
LEFT OUTER JOIN PlanYear py on py.StateId = inv.StateId and py.Year = inv.PlanYear
WHERE inv.Id = @InvoiceId




GO


