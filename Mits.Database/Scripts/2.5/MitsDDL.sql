Alter table dbo.Invoice add Approved bit default 0

update i
set Approved = 1
from Invoice i
inner join Affiliate a on a.Id =  i.AffiliateId
inner join InvoiceItem ii on ii.InvoiceId = i.Id
inner join InvoiceReconciliation ir on ir.ProcesserInvoiceItemId = ii.Id
where a.AffiliateTypeId = 2