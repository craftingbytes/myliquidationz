ALTER VIEW [dbo].[V_Accounting2]
AS
SELECT st.Name AS [State]
		, St.Id AS StateId
		, Inv.AffiliateId AS AffiliateId
		, aff.Name AS AffiliateName
		, At.Id AS EntityType
		, Inv.PlanYear
		, Inv.id AS InvoiceId
		, InvAmt.InvoiceAmount AS InvoiceAmount
		, IsNULL(InvPmt.PaidAmount, 0) AS PaymentAmount
		,(IsNULL(InvAmt.InvoiceAmount, 0) - IsNULL(InvPmt.PaidAmount, 0)) AS RemainingAmount
		, CONVERT(VARCHAR(10), Inv.InvoiceDate, 101) AS InvoiceDate
		, Inv.InvoiceDate AS InvoiceDate1
        , CONVERT(VARCHAR(10), PD.ProcessingDate, 101) AS ProcessingDate
        , CONVERT(VARCHAR(10), Inv.InvoiceDueDate, 101) AS InvoiceDueDate
		, CONVERT(VARCHAR(10), InvPmt.Date, 101) AS PaymentDate
		, PaidStatus = 
			CASE WHEN ((IsNULL(InvAmt.InvoiceAmount, 0) - IsNULL(InvPmt.PaidAmount, 0) = 0) 
					AND (IsNULL(InvAmt.InvoiceAmount, 0) > 0) 
					AND IsNULL(InvPmt.PaidAmount, 0) > 0) THEN 'Full Paid' 
               WHEN (IsNULL(InvAmt.InvoiceAmount, 0) - IsNULL(InvPmt.PaidAmount, 0)) < 0 THEN 'Over Paid' 
               WHEN ((IsNULL(InvAmt.InvoiceAmount, 0) 
					- IsNULL(InvPmt.PaidAmount, 0)) > 0) AND IsNULL(InvPmt.PaidAmount, 0) <> 0 THEN 'Partial Paid' 
               WHEN IsNULL(InvPmt.PaidAmount, 0) = 0 THEN 'Not Paid' 
               ELSE 'Unknown' END
         , Aging = CASE /*As Aging	*/ WHEN ((IsNULL(InvAmt.InvoiceAmount, 
               0) - IsNULL(InvPmt.PaidAmount, 0)) > 0) AND IsNULL(InvPmt.PaidAmount, 0) <> 0 OR
               (IsNULL(InvPmt.PaidAmount, 0) = 0) THEN Datediff(day, GETDATE(), Inv.InvoiceDueDate) END
FROM  Invoice Inv 
INNER JOIN /*Table1: Inv*/ 
      (SELECT InvoiceId AS InvoiceId
              , Count(*) AS InvoiceItemCount
              , Sum((IsNull(Rate, 0)) * Abs((Isnull(ii.Quantity, 0)))) AS InvoiceAmount
              , InvoiceQuantity = SUM(CASE WHEN (ServiceTypeId BETWEEN 1 AND 5 OR
                ServiceTypeId = 10) THEN Abs(Isnull(ii.Quantity, 0)) ELSE 0 END)
              , 0 AS RecQuantity/*sum(Total ReconcileQuantity to one OEMInvoiceId)*/ 
              , 0 AS RecAssignedQuantity/*sum(Total ReconcileQuantity in one ProcesserInvoiceItemId)*/
              , MAX(CASE ServiceTypeId WHEN 3 THEN 2 WHEN 10 THEN 1 ELSE 0 END) 
                AS InvAffiliateTypeID
              , ServiceTypeID = MAX(CASE WHEN (ServiceTypeId BETWEEN 1 AND 5) THEN ServiceTypeId ELSE 0 END)
              , QuantityTypeID = MAX(CASE WHEN (ServiceTypeId BETWEEN 1 AND 5) THEN QuantityTypeID ELSE 0 END)
		FROM  InvoiceItem ii 
		--LEFT OUTER JOIN V_OEMReconciliation iir ON ii.InvoiceId = iir.OEMInvoiceId 
		--LEFT OUTER JOIN V_ProcesserReconciliation iir2 ON ii.Id = iir2.ProcesserInvoiceItemId
		WHERE Abs((IsNull(Rate, 0)) * Abs((Isnull(ii.Quantity, 0)))) > 0
		AND ii.ServiceTypeId <> 10
		GROUP BY InvoiceId
		union All
		SELECT ii.InvoiceId AS InvoiceId
              , Count(*) AS InvoiceItemCount
              , Sum((IsNull(Rate, 0)) * Abs((Isnull(ii.Quantity, 0)))) AS InvoiceAmount
              , InvoiceQuantity = SUM(CASE WHEN (ServiceTypeId BETWEEN 1 AND 5 OR
                ServiceTypeId = 10) THEN Abs(Isnull(ii.Quantity, 0)) ELSE 0 END)
              , Sum(ISNULL(iir.ReconcileQuantity, 0)) AS RecQuantity/*sum(Total ReconcileQuantity to one OEMInvoiceId)*/ 
              , Sum(ISNULL(iir2.RecAssignedQuantity, 0)) AS RecAssignedQuantity/*sum(Total ReconcileQuantity in one ProcesserInvoiceItemId)*/
              , MAX(CASE ServiceTypeId WHEN 3 THEN 2 WHEN 10 THEN 1 ELSE 0 END) 
                AS InvAffiliateTypeID
              , ServiceTypeID = MAX(CASE WHEN (ServiceTypeId = 10) THEN ServiceTypeId ELSE 0 END)
              , QuantityTypeID = MAX(CASE WHEN (ServiceTypeId = 10) THEN QuantityTypeID ELSE 0 END)
		FROM  InvoiceItem ii 
		INNER JOIN v_InvoicesRequiringFix fix ON fix.InvoiceId = ii.InvoiceId
		INNER JOIN V_OEMReconciliation iir ON ii.InvoiceId = iir.OEMInvoiceId 
		INNER JOIN V_ProcesserReconciliation iir2 ON ii.Id = iir2.ProcesserInvoiceItemId
		WHERE Abs((IsNull(Rate, 0)) * Abs((Isnull(ii.Quantity, 0)))) > 0
		AND ii.ServiceTypeId = 10
		GROUP BY ii.InvoiceId
		UNION All
				SELECT ir.OemInvoiceId AS InvoiceId
              , Count(*) AS InvoiceItemCount
              , Sum((IsNull(cr.Rate, 0)) * Abs((round(Isnull(ir.Quantity, 0),0)))) AS InvoiceAmount
              , abs(sum(round(Isnull(ir.Quantity, 0),0)))  InvoiceQuantity
              , Abs(sum(Isnull(ir.Quantity, 0)))  RecQuantity
              , 0
              , 1 InvAffiliateTypeID 
			  , 10 ServiceTypeID
              , max(cr.QuantityTypeID) QuantityTypeID
		FROM  Invoice i
		INNER JOIN InvoiceReconciliation ir on ir.OEMInvoiceId = i.Id
		inner join InvoiceItem ii on ii.Id = ir.ProcesserInvoiceItemId
		inner JOIN AffiliateContractRate cr on	cr.AffiliateId = i.AffiliateId
												and cr.StateId = i.StateId
												and cr.PlanYear = i.PlanYear
												and cr.ServiceTypeId = 10
												and cr.ProductTypeId = ii.ProductTypeId
												and cr.QuantityTypeId = ii.QuantityTypeId 
		where  cr.ServiceTypeId = 10
		and i.Id not in (select InvoiceId from v_InvoicesRequiringFix)
		GROUP BY ir.OemInvoiceId
		) AS InvAmt ON Inv.id = InvAmt.InvoiceId 
		
LEFT OUTER JOIN/*Table2: InvAmt*/ 
	(SELECT PmtD.InvoiceID
			, MAX(Pmt.Date) Date
			, SUM(IsNull(PmtD.PaidAmount, 0)) PaidAmount
			, Count(*) AS PaymentItemCount
	 FROM  Payment Pmt INNER JOIN
			PaymentDetail PmtD ON Pmt.Id = PmtD.PaymentID
	 GROUP BY PmtD.InvoiceID
	) InvPmt ON InvPmt.InvoiceId = Inv.Id 
	
INNER JOIN State St ON Inv.StateId = St.Id 
INNER JOIN Affiliate Aff ON Inv.AffiliateId = Aff.Id 
LEFT JOIN AffiliateType At ON Aff.AffiliateTypeId = At.Id 
LEFT JOIN ProcessingData PD ON Inv.Id = PD.InvoiceId 
--LEFT JOIN V_AffiliateTargetWithPlanYear ATr  ON ATr.AffiliateId = Inv.AffiliateId 
--												AND ATr.StateId = Inv.StateId 
--												AND ATr.ServiceTypeId = InvAmt.ServiceTypeID 
--												AND ATr.QuantityTypeId = InvAmt.QuantityTypeID 
--												AND Inv.PlanYear = ATr.PlanYear
WHERE aff.AffiliateTypeId = 1
or (aff.AffiliateTypeId = 2 and inv.Approved = 1)



GO


