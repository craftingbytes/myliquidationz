ALTER View [dbo].[V_AssignedWeightDetail]
As
SELECT StFrom.Id AS FromStateID
	, StFrom.Name AS FromStateName
	, AfFrom.Id AS FromEntityId
	, AfFrom.Name AS FromEntityName
	, invFrom.PlanYear AS FromPlanYear
	, iiFrom.InvoiceId AS FromInvoiceId
	, StTo.Id AS ToStateId
	, StTo.Name AS ToStateName
	, AfTo.Id AS ToEntityId
	, AfTo.Name AS ToEntityName
	, invTo.Id AS ToInvoiceId
	, iiRec.ProcesserInvoiceItemId AS ReconcileFromInvoiceItemID
    , OEMInvoiceId AS ReconcileToInvoiceID
    , CASE iiFrom.Quantity WHEN NULL THEN 0 WHEN 0 THEN 0 ELSE Abs(iiFrom.Quantity) 
      / Count(iiRec.ProcesserInvoiceItemId) OVER (Partition BY iiRec.ProcesserInvoiceItemId) END AS FromInvoiceWeight
    , ISNULL(iiRec.Quantity, 0) AS RecAssignedWeight
    , Count(iiRec.ProcesserInvoiceItemId) OVER (Partition BY iiRec.ProcesserInvoiceItemId) AS ReconcileItemCount
FROM  InvoiceReconciliation iiRec INNER JOIN
               invoiceitem iiFrom ON iiRec.ProcesserInvoiceItemId = iiFrom.Id INNER JOIN
               invoice invTo ON invTo.Id = iiRec.OEMInvoiceId INNER JOIN
               Invoice invFrom ON iiFrom.InvoiceId = invFrom.Id INNER JOIN
               Affiliate AfFrom ON invFrom.AffiliateId = AfFrom.Id INNER JOIN
               State StFrom ON invFrom.StateId = StFrom.Id INNER JOIN
               --Invoice invTo ON iiTo.InvoiceId = invTo.Id INNER JOIN
               Affiliate AfTo ON invTo.AffiliateId = AfTo.Id INNER JOIN
               State StTo ON invTo.StateId = StTo.Id
UNION ALL
(SELECT StFrom.Id AS FromStateID, StFrom.Name AS FromStateName
		, AfFrom.Id AS FromEntityId, AfFrom.Name AS FromEntityName
		, invFrom.PlanYear AS FromPlanYear, iiFrom.InvoiceId AS FromInvoiceId
		, NULL AS ToStateId, NULL AS ToStateName, NULL AS ToEntityId
		, NULL AS ToEntityName, NULL AS ToInvoiceId, iiFrom.id AS ReconcileFromInvoiceItemID
		, NULL AS ReconcileToInvoiceID
		, ISNULL(Abs(iiFrom.Quantity), 0) AS FromInvoiceWeight
		, 0 AS RecAssignedWeight, 0 AS ReconcileItemCount
 FROM   invoiceitem iiFrom INNER JOIN
                Invoice invFrom ON iiFrom.InvoiceId = invFrom.Id INNER JOIN
                Affiliate AfFrom ON invFrom.AffiliateId = AfFrom.Id INNER JOIN
                State StFrom ON invFrom.StateId = StFrom.Id
 WHERE iiFrom.Id NOT IN (SELECT ProcesserInvoiceItemId
						 FROM   InvoiceReconciliation) 
       AND iiFrom.ServiceTypeId = 3 
       AND invFrom.Approved = 1 
       AND ISNULL(Abs(iiFrom.Quantity), 0) <> 0)


GO


