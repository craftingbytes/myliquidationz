
-- =============================================
-- Author:		Jei Jacinto
-- Create date: 09/14/2011
-- Description:	Returns list of valid reminders
-- =============================================
ALTER FUNCTION [dbo].[udf_getRemindersForDate]
(	
	@SendDate DATETIME
)
RETURNS @Reminder TABLE(
	ReminderId  int,
	ActionDate Datetime,
	DateEnd DateTime,
	SendDate DateTime,
	Number varchar(50),
	OccurranceDate Datetime
	)

AS BEGIN

	insert into @Reminder
	SELECT 
		r.Id ReminderId, 
		e.ActionDate, 
		e.DateEnd,
		@SendDate,
		r.Number,
		@SendDate + CONVERT(int,r.Number)
	FROM Reminder r
	inner join ReminderGroup rg on rg.Id = r.ReminderGroupId
	inner join Event e on e.Id = rg.EventId
	where r.StatusId = 3 --pending
	AND e.RecurrenceTypeId = 7 -- every
	AND e.RecurrenceIntervalForDay > 0
	AND (e.DateEnd >= @SendDate or e.DateEnd is NULL)
	AND dateadd(DAY,-CONVERT(int,r.Number), e.ActionDate) <= @SendDate
	AND (r.SentReminderDate is null or r.SentReminderDate < @SendDate)
	AND isnumeric(r.number) > 0
	AND DATEDIFF(DAY,CONVERT(DATE,e.ActionDate), DATEADD(DAY,convert(int,r.number),@SendDate)) % e.RecurrenceIntervalForDay = 0


	insert into @Reminder
	SELECT 
		r.Id ReminderId, 
		e.ActionDate, 
		e.DateEnd,
		@SendDate,
		r.Number,
		@SendDate + CONVERT(int,r.Number)
	FROM Reminder r
	inner join ReminderGroup rg on rg.Id = r.ReminderGroupId
	inner join Event e on e.Id = rg.EventId
	where r.StatusId = 3 --pending
	AND e.RecurrenceTypeId = 8 -- every
	AND r.PeriodId = 5
	AND (e.DateEnd >= @SendDate or e.DateEnd is NULL)
	AND dateadd(DAY,-CONVERT(int,r.Number), e.ActionDate) <= @SendDate
	AND (r.SentReminderDate is null or r.SentReminderDate < @SendDate)
	AND e.RecurrenceIntervalForMonth  = DAY(DATEADD(DAY,CONVERT(int,r.Number), @SendDate))

	insert into @Reminder
	SELECT 
		r.Id ReminderId, 
		e.ActionDate, 
		e.DateEnd,
		@SendDate,
		r.Number,
		@SendDate + CONVERT(int,r.Number)
	FROM Reminder r
	inner join ReminderGroup rg on rg.Id = r.ReminderGroupId
	inner join Event e on e.Id = rg.EventId
	where r.StatusId = 3 
	AND e.RecurrenceTypeId = 10 
	AND r.PeriodId = 5
	AND (e.DateEnd >= @SendDate or e.DateEnd is NULL)
	AND dateadd(DAY,-CONVERT(int,r.Number), e.ActionDate) <= @SendDate
	AND (r.SentReminderDate is null or r.SentReminderDate < @SendDate)
	AND DAY(@SendDate) = DAY(DATEADD(DAY,- CONVERT(int,r.Number), RecurrenceIntervalYearly) )
	AND MONTH(@SendDate) = MONTH(DATEADD(DAY,- CONVERT(int,r.Number), RecurrenceIntervalYearly) )

	insert into @Reminder
	SELECT 
		r.Id ReminderId, 
		e.ActionDate, 
		e.DateEnd,
		@SendDate,
		r.Number,
		@SendDate + CONVERT(int,r.Number)
	FROM Reminder r
	inner join ReminderGroup rg on rg.Id = r.ReminderGroupId
	inner join Event e on e.Id = rg.EventId
	where r.StatusId = 3 
	AND e.RecurrenceTypeId = 9 
	AND r.PeriodId = 5
	AND (e.DateEnd >= @SendDate or e.DateEnd is NULL)
	AND dateadd(DAY,-CONVERT(int,r.Number), e.ActionDate) <= @SendDate
	AND (r.SentReminderDate is null or r.SentReminderDate < @SendDate)
	AND  e.RecurrenceIntervalQuaterly  = DAY(DATEADD(DAY,CONVERT(int,r.Number), @SendDate))
	AND (MONTH(DATEADD(DAY,CONVERT(int,r.Number), @SendDate)) - MONTH(e.ActionDate)) % 3 = 0

	insert into @Reminder
	SELECT 
		r.Id ReminderId, 
		e.ActionDate, 
		e.DateEnd,
		@SendDate,
		r.Number,
		@SendDate + CONVERT(int,r.Number)
	FROM Reminder r
	inner join ReminderGroup rg on rg.Id = r.ReminderGroupId
	inner join Event e on e.Id = rg.EventId
	where r.StatusId = 3 
	AND e.RecurrenceTypeId is null 
	AND r.PeriodId = 5
	AND dateadd(DAY,-CONVERT(int,r.Number), e.ActionDate) = @SendDate


	insert into @Reminder
	SELECT 
		r.Id ReminderId, 
		e.ActionDate, 
		e.DateEnd,
		@SendDate,
		r.Number,
		@SendDate + CONVERT(int,r.Number)
	FROM Reminder r
	inner join ReminderGroup rg on rg.Id = r.ReminderGroupId
	inner join Event e on e.Id = rg.EventId
	where r.StatusId = 3 
	AND e.RecurrenceTypeId = 6 
	AND r.PeriodId = 5
	AND (dateadd(DAY,-CONVERT(int,r.Number), e.ActionDate) = @SendDate
		 or
		 dateadd(DAY,-CONVERT(int,r.Number), e.SingleRecurrenceValue) = @SendDate 
		 )
	AND (r.SentReminderDate is null or r.SentReminderDate < @SendDate)

	RETURN
END



GO


