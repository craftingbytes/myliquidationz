ALTER View 
[dbo].[AllEntityAccountsDetail2]
As 

SELECT 
St.Name																			As State
,Aff.Name																		As EntityName
,Inv.Id																			As InvoiceID
,InvAmt.InvoiceAmount															As InvoiceAmount
,IsNULL(InvPmt.PaidAmount,0)													As PaymentAmount
,(IsNULL(InvAmt.InvoiceAmount,0) - IsNULL(InvPmt.PaidAmount,0))					As RemainingAmount 
,CONVERT(VARCHAR(10) ,Inv.InvoiceDate ,101)										As InvoiceDate
,Inv.InvoiceDate 																As InvoiceDate1
,Datename(MONTH ,Inv.InvoiceDate)												As InvoiceMonthName 	
,Month(Inv.InvoiceDate)															As InvoiceMonth
,Year(Inv.InvoiceDate)															As InvoiceYear
,inv.PlanYear																	As PlanYear
,Case 
When Month(Inv.InvoiceDate) Between 1 and 6
Then 'Year 1st Half'
Else 'Year 2nd Half'
End																				As InvoiceYearHalf
,Case 
When Month(Inv.InvoiceDate) Between 1 and 3
Then 'Quarter-1'
When Month(Inv.InvoiceDate) Between 4 and 6
Then 'Quarter-2'
When Month(Inv.InvoiceDate) Between 7 and 9
Then 'Quarter-3'
Else 'Quarter-4'
End																				AS InvoiceQaurter
,CONVERT(VARCHAR(10) ,PD.ProcessingDate	 ,101)									As ProcessingDate
,CONVERT(VARCHAR(10) ,Inv.InvoiceDueDate ,101)									AS InvoiceDueDate
,CONVERT(VARCHAR(10) ,InvPmt.Date ,101)											As PaymentDate
,CONVERT(VARCHAR(10) ,Inv.InvoicePeriodFrom ,101)								As InvoicePeriodFrom
,CONVERT(VARCHAR(10) ,Inv.InvoicePeriodTo ,101)									As InvoicePeriodTo 
,InvAmt.InvoiceItemCount														AS InvoiceItemCount
,InvPmt.PaymentItemCount														As PaymentItemCount
,PaidStatus = Case															  --AS PaidStatus  
When ((IsNULL(InvAmt.InvoiceAmount,0) - IsNULL(InvPmt.PaidAmount,0) = 0 ) 
		AND (IsNULL(InvAmt.InvoiceAmount,0) > 0) 
		AND IsNULL(InvPmt.PaidAmount,0) > 0)
 Then 'Full Paid'
 When   (IsNULL(InvAmt.InvoiceAmount,0) - IsNULL(InvPmt.PaidAmount,0))< 0 
 Then 'Over Paid'
 When ((IsNULL(InvAmt.InvoiceAmount,0) - IsNULL(InvPmt.PaidAmount,0)) > 0)
	   AND  IsNULL(InvPmt.PaidAmount,0) <> 0 
 Then 'Partial Paid'
 When  IsNULL(InvPmt.PaidAmount,0) = 0  
 Then 'Not Paid'
 Else 'Unknown'
 End																			      
,Inv.Receivable																	As Receivable
,Inv.AffiliateId																AS EntityId    
,Inv.IsPaid																		AS IsPaid
,St.Id																			As StateId  
 ,Aging = Case																  --As Aging	
 When 
 ((IsNULL(InvAmt.InvoiceAmount,0) - IsNULL(InvPmt.PaidAmount,0)) > 0) 
  AND  IsNULL(InvPmt.PaidAmount,0) <> 0 OR ( IsNULL(InvPmt.PaidAmount,0) = 0) 
 Then Datediff(day,GETDATE()  , Inv.InvoiceDueDate)
 End 
 ,At.Id																			As EntityType
 --,EntityType =Case															  --As EntityType
 --When (Inv.Receivable = 1  AND InvAffiliateTypeID = 1) OR  InvAffiliateTypeID = 1    
 --Then 1
 --When (Inv.Receivable = 0 AND InvAffiliateTypeID = 2 ) OR  InvAffiliateTypeID = 2    
 --Then 2
 --Else 0
 --End 
,InvAmt.InvoiceQuantity															As InvoiceWeight
,InvAmt.RecAssignedQuantity														As RecAssignedWeight
,(InvAmt.InvoiceQuantity - InvAmt.RecAssignedQuantity)							As RecUnAssignedWeight
,InvAmt.RecQuantity																As RecWeight 
,IsNull(InvAmt.InvoiceQuantity,0) - IsNull(InvAmt.RecQuantity,0)				As InvRemainingWeight
,ATr.Weight																		As TargetWeight
 ,InvoiceStatus = Case														--	AS InvoiceStatus  
 When IsNull(InvAmt.InvoiceQuantity,0) - IsNull(InvAmt.RecQuantity,0) = 0 
 Then 'Completed'
 When IsNull(InvAmt.InvoiceQuantity,0) - IsNull(InvAmt.RecQuantity,0)< 0  AND IsNull(InvAmt.RecQuantity,0)IS Not NULL 
 Then 'Over Assigned'
 When IsNull(InvAmt.InvoiceQuantity,0) - IsNull(InvAmt.RecQuantity,0) > 0 AND IsNull(InvAmt.RecQuantity,0) <> 0 
 Then 'Partial Completed'
 When   IsNull(InvAmt.RecQuantity,0) = 0  
 Then 'Not Completed'
 Else 'Unknown'
 End 
,InvAmt.QuantityTypeID															AS QuantityTypeID
,InvAmt.ServiceTypeID															As ServiceTypeID										
,CONVERT(VARCHAR(10) ,ATr.StartDate,101)										As TargetStartDate
,CONVERT(VARCHAR(10) ,ATr.EndDate ,101)											As TargetEndDate 
,ATr.StartDate																	As StartDate
,ATr.EndDate																	As EndDate	
,ATr.Id																			As TargetId
,COUNT(Inv.Id) OVER (Partition BY ATr.StateID, ATr.AffiliateID, Atr.Id)			AS InvoiceCountPerTarget
,IsNULL(ATr.Weight,0)/Isnull(Nullif(COUNT(Inv.Id) OVER (Partition BY ATr.StateID, ATr.AffiliateID, Atr.Id),0),1) As InvAvgWeight
, (IsNULL(ATr.Weight,0)/Isnull(Nullif(COUNT(Inv.Id) OVER (Partition BY ATr.StateID, ATr.AffiliateID, Atr.Id),0),1)- InvAmt.RecQuantity) As InvRemWeight
,Inv.Approved

FROM Invoice Inv
  INNER JOIN
  (
       SELECT InvoiceId AS InvoiceId
              , Count(*) AS InvoiceItemCount
              , Sum((IsNull(Rate, 0)) * Abs((Isnull(ii.Quantity, 0)))) AS InvoiceAmount
              , InvoiceQuantity = SUM(CASE WHEN (ServiceTypeId BETWEEN 1 AND 5 OR
                ServiceTypeId = 10) THEN Abs(Isnull(ii.Quantity, 0)) ELSE 0 END)
              , 0 AS RecQuantity/*sum(Total ReconcileQuantity to one OEMInvoiceId)*/ 
              , 0 AS RecAssignedQuantity/*sum(Total ReconcileQuantity in one ProcesserInvoiceItemId)*/
              , MAX(CASE ServiceTypeId WHEN 3 THEN 2 WHEN 10 THEN 1 ELSE 0 END) 
                AS InvAffiliateTypeID
              , ServiceTypeID = MAX(CASE WHEN (ServiceTypeId BETWEEN 1 AND 5) THEN ServiceTypeId ELSE 0 END)
              , QuantityTypeID = MAX(CASE WHEN (ServiceTypeId BETWEEN 1 AND 5) THEN QuantityTypeID ELSE 0 END)
		FROM  InvoiceItem ii 
		--LEFT OUTER JOIN V_OEMReconciliation iir ON ii.InvoiceId = iir.OEMInvoiceId 
		--LEFT OUTER JOIN V_ProcesserReconciliation iir2 ON ii.Id = iir2.ProcesserInvoiceItemId
		WHERE ii.ServiceTypeId <> 10
		GROUP BY InvoiceId
		union All
		SELECT ii.InvoiceId AS InvoiceId
              , Count(*) AS InvoiceItemCount
              , Sum((IsNull(Rate, 0)) * Abs((Isnull(ii.Quantity, 0)))) AS InvoiceAmount
              , InvoiceQuantity = SUM(CASE WHEN (ServiceTypeId BETWEEN 1 AND 5 OR
                ServiceTypeId = 10) THEN Abs(Isnull(ii.Quantity, 0)) ELSE 0 END)
              , Sum(ISNULL(iir.ReconcileQuantity, 0)) AS RecQuantity/*sum(Total ReconcileQuantity to one OEMInvoiceId)*/ 
              , Sum(ISNULL(iir2.RecAssignedQuantity, 0)) AS RecAssignedQuantity/*sum(Total ReconcileQuantity in one ProcesserInvoiceItemId)*/
              , MAX(CASE ServiceTypeId WHEN 3 THEN 2 WHEN 10 THEN 1 ELSE 0 END) 
                AS InvAffiliateTypeID
              , ServiceTypeID = MAX(CASE WHEN (ServiceTypeId = 10) THEN ServiceTypeId ELSE 0 END)
              , QuantityTypeID = MAX(CASE WHEN (ServiceTypeId = 10) THEN QuantityTypeID ELSE 0 END)
		FROM  InvoiceItem ii 
		INNER JOIN v_InvoicesRequiringFix fix ON fix.InvoiceId = ii.InvoiceId
		INNER JOIN V_OEMReconciliation iir ON ii.InvoiceId = iir.OEMInvoiceId 
		INNER JOIN V_ProcesserReconciliation iir2 ON ii.Id = iir2.ProcesserInvoiceItemId
		WHERE Abs((IsNull(Rate, 0)) * Abs((Isnull(ii.Quantity, 0)))) > 0
		AND ii.ServiceTypeId = 10
		GROUP BY ii.InvoiceId
		UNION All
				SELECT ir.OemInvoiceId AS InvoiceId
              , Count(*) AS InvoiceItemCount
              , Sum((IsNull(cr.Rate, 0)) * Abs((round(Isnull(ir.Quantity, 0),0)))) AS InvoiceAmount
              , abs(sum(round(Isnull(ir.Quantity, 0),0)))  InvoiceQuantity
              , Abs(sum(Isnull(ir.Quantity, 0)))  RecQuantity
              , 0
              , 1 InvAffiliateTypeID 
			  , 10 ServiceTypeID
              , max(cr.QuantityTypeID) QuantityTypeID
		FROM  Invoice i
		INNER JOIN InvoiceReconciliation ir on ir.OEMInvoiceId = i.Id
		inner join InvoiceItem ii on ii.Id = ir.ProcesserInvoiceItemId
		inner JOIN AffiliateContractRate cr on	cr.AffiliateId = i.AffiliateId
												and cr.StateId = i.StateId
												and cr.PlanYear = i.PlanYear
												and cr.ServiceTypeId = 10
												and cr.ProductTypeId = ii.ProductTypeId
												and cr.QuantityTypeId = ii.QuantityTypeId 
		where  cr.ServiceTypeId = 10
		and i.Id not in (select InvoiceId from v_InvoicesRequiringFix)
		GROUP BY ir.OemInvoiceId
  ) AS InvAmt ON Inv.id = InvAmt.InvoiceId
  LEFT OUTER JOIN 
  (
		Select
		PmtD.InvoiceID,
		MAX(Pmt.Date) Date, 
		SUM(IsNull(PmtD.PaidAmount,0)) PaidAmount,
		Count(*) AS PaymentItemCount
		FROM Payment Pmt
		INNER JOIN PaymentDetail PmtD ON Pmt.Id = PmtD.PaymentID 
		Group By PmtD.InvoiceID  
  ) InvPmt ON InvPmt.InvoiceId =  Inv.Id
  INNER JOIN State St On Inv.StateId = St.Id
  INNER JOIN Affiliate Aff ON Inv.AffiliateId = Aff.Id 
  LEFT OUTER JOIN  AffiliateType At ON Aff.AffiliateTypeId  = At.Id  
  LEFT OUTER JOIN ProcessingData PD ON Inv.Id = PD.InvoiceId 
  LEFT OUTER JOIN AffiliateTarget ATr ON ATr.AffiliateId = Inv.AffiliateId
  AND ATr.StateId = Inv.StateId 
  AND ATr.ServiceTypeId = InvAmt.ServiceTypeID
  AND ATr.QuantityTypeId = InvAmt.QuantityTypeID
  AND Inv.PlanYear = ATr.PlanYear
  --AND Inv.InvoicePeriodFrom >= ATr.StartDate AND Inv.InvoicePeriodTo <= ATr.EndDat




GO


