
ALTER VIEW [dbo].[V_ReminderEmail]
AS
SELECT     rg.Id AS ReminderGroupId, a.Id AS AffiliateId, a.Name, '' Email, dbo.Event.Id AS EventId, dbo.Event.Title AS EventTitle, 
                      dbo.Event.Description AS EventDescription, r.Id AS ReminderId, r.Number, dbo.Period.Period, r.PeriodId, r.Title, fr.SendDate, r.Emails, r.SentReminderDate, 
                      dbo.Event.DateBegin, dbo.Event.DateEnd, dbo.Event.ActionDate, dbo.Event.RecurrenceTypeId, dbo.Event.SingleRecurrenceValue, 
                      dbo.Event.RecurrenceIntervalForDay, dbo.Event.RecurrenceIntervalForMonth, dbo.Event.RecurrenceIntervalQuaterly, dbo.Event.RecurrenceIntervalYearly, 
                      dbo.ReminderAttachment.AttachmentName, dbo.EventType.Type AS eType,     fr.OccurranceDate OccurranceDate
FROM  udf_getRemindersForDate(CONVERT(DATE,GETDATE())) fr 
INNER JOIN dbo.Reminder AS r on r.Id = fr.ReminderId
INNER JOIN dbo.ReminderGroup AS rg ON rg.Id = r.ReminderGroupId 
INNER JOIN dbo.ReminderGroupAffiliate AS rga ON rga.ReminderGroupId = rg.Id 
INNER JOIN dbo.Event ON rg.EventId = dbo.Event.Id 
INNER JOIN dbo.Affiliate AS a ON a.Id = rga.AffiliateId 
--INNER JOIN dbo.AffiliateContact ON a.Id = dbo.AffiliateContact.AffiliateId 
INNER JOIN dbo.ReminderStatus ON r.StatusId = dbo.ReminderStatus.Id 
INNER JOIN dbo.Period ON r.PeriodId = dbo.Period.Id 
INNER JOIN dbo.EventType ON dbo.Event.EventTypeId = dbo.EventType.Id 
LEFT OUTER JOIN dbo.ReminderAttachment ON r.Id = dbo.ReminderAttachment.ReminderId



GO


