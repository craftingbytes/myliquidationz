

/****** Object:  Table [dbo].[InvoiceAdjustment]    Script Date: 12/22/2011 10:07:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[InvoiceAdjustment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Amount] [money] NOT NULL,
	[AdjustmentDate] [datetime] NULL,
 CONSTRAINT [PK_InvoiceAdjustment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InvoiceAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceAdjustment_Invoice] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoice] ([Id])
GO

ALTER TABLE [dbo].[InvoiceAdjustment] CHECK CONSTRAINT [FK_InvoiceAdjustment_Invoice]
GO

ALTER TABLE [dbo].[InvoiceAdjustment] ADD  CONSTRAINT [DF_InvoiceAdjustment_AdjustmentDate]  DEFAULT (getdate()) FOR [AdjustmentDate]
GO


