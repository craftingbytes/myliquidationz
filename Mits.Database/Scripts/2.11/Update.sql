
ALTER TABLE dbo.AffiliateTarget ADD
	Pace int NULL
GO
ALTER TABLE dbo.AffiliateTarget ADD CONSTRAINT
	DF_AffiliateTarget_Pace DEFAULT 0 FOR Pace
GO
ALTER TABLE dbo.AffiliateTarget ADD CONSTRAINT
	CK_AffiliateTarget CHECK (Pace<=100)
GO



ALTER procedure [dbo].[p_isBelowTarget]
	@InvoiceId int,
	@Weight decimal(10,2)
as
	declare @TargetWeight decimal(10,2)
	declare @ProcessedWeight decimal(10,2)
	declare @AffiliateId int
	declare @StateId int
	declare @Planyear int
	declare @Success bit
	
	
	
	select 
		@AffiliateId = i.AffiliateId,
		@StateId = i.StateId,
		@Planyear = i.Planyear
	from Invoice i 
	left join InvoiceItem ii on ii.InvoiceId = i.Id
	where i.Id = @InvoiceId
	group by 
		i.AffiliateId,
		i.StateId,
		i.Planyear,
		i.Id
	
	select 
		@ProcessedWeight = SUM(coalesce(ii.quantity,0))
	from Invoice i
	left join InvoiceItem ii on ii.InvoiceId = i.Id
	where i.AffiliateId = @AffiliateId
	and	i.StateId = @StateId
	and	i.Planyear = @Planyear
		
	set @TargetWeight = 0.0
	
	select 
		top 1
		@TargetWeight = coalesce(floor ( Weight * (.01 * coalesce(Pace,100)) ) ,0)
	from AffiliateTarget 
	where StateId = @StateId
	and AffiliateId = @AffiliateId
	and PlanYear = @Planyear
	and ServiceTypeId = 3
	and QuantityTypeId = 2
	
	if (@TargetWeight < @ProcessedWeight + @Weight)
		set @Success = 0 
	else
		set @Success = 1
		
	select @Success Success
GO


alter PROCEDURE [dbo].[p_getOEMTargetAssignedWeight]
	@OEMId int,
	@StateId int,
	@PlanYear int
	-- p_getOEMTargetAssignedWeight 5550, 37, 2011
	-- p_getOEMTargetAssignedWeight 0, 27, 2014
AS

IF 1=2
BEGIN
	SELECT 
		CAST(NULL AS  BIT ) Active,
		CAST(NULL AS  INT )OEMId,
		CAST(NULL AS  [varchar](100) ) OEMName ,
		CAST(NULL AS  Decimal(18,2)  ) TargetWeight ,
		CAST(NULL AS  INT		    ) Pace ,
		CAST(NULL AS  Decimal(18,2)  ) PaceWeight ,
		CAST(NULL AS  Decimal(18,2)  ) AssignedWeight,
		CAST(NULL AS  Decimal(18,2)  ) RemainingWeight,
		CAST(NULL AS  Decimal(18,2)  ) RecommendedWeight,
		CAST(NULL AS  BIT )  IsPermanentDropOffLocation
END 

SET NOCOUNT ON

declare @TotalAvailableWeight decimal(18,2)
declare @TotalProcessedWeight decimal(18,2)
declare @TotalAssignedWeight decimal(18,2)
declare @TotalTargetWeight decimal(18,2)
declare @TotalPaceWeight decimal(18,2)
declare @Targets table(
	Active bit,
	OEMId int,
	OEMName nvarchar(100),
	TargetWeight Decimal(18,2),
	Pace int,
	PaceWeight Decimal(18,2),
	AssignedWeight Decimal(18,2),
	RemainingWeight Decimal (18,2),
	RecommendedWeight Decimal (18,2),
	IsPermanentDropOffLocation bit	
) 


SELECT @TotalProcessedWeight = coalesce(SUM(COALESCE(ii.Quantity,0)),0)
FROM Invoice i
INNER JOIN InvoiceItem ii ON ii.InvoiceId = i.Id
INNER JOIN Affiliate a on a.Id = i.AffiliateId
WHERE a.AffiliateTypeId = 2
AND i.PlanYear = @PlanYear
AND i.StateId = @StateId
AND i.Approved = 1

SELECT @TotalAssignedWeight = SUM(COALESCE(ir.Quantity,0))
FROM Invoice i
INNER JOIN InvoiceReconciliation ir ON ir.OEMInvoiceId = i.Id
WHERE i.StateId = @StateId
AND i.PlanYear = @PlanYear

set @TotalAssignedWeight = coalesce(@TotalAssignedWeight,0)


SELECT @TotalTargetWeight = SUM(COALESCE(t.Weight,0))
FROM Affiliate a
INNER JOIN AffiliateTarget t on a.Id = t.AffiliateId
WHERE a.AffiliateTypeId = 1
AND (@OEMId = 0 OR a.Id = @OEMId)
AND t.StateId = @StateId
AND t.PlanYear = @PlanYear


SELECT @TotalPaceWeight = SUM(COALESCE(floor(coalesce(t.pace,100) * .01 * t.Weight )  ,0))
FROM Affiliate a
INNER JOIN AffiliateTarget t on a.Id = t.AffiliateId
WHERE a.AffiliateTypeId = 1
AND (@OEMId = 0 OR a.Id = @OEMId)
AND t.StateId = @StateId
AND t.PlanYear = @PlanYear


SELECT @TotalAvailableWeight = @TotalProcessedWeight - @TotalAssignedWeight

--SELECT 'TotalAssignedWeight', @TotalAssignedWeight
--SELECT 'TotalProcessedWeight', @TotalProcessedWeight
--SELECT 'TotalTargetWeight', @TotalTargetWeight
--SELECT 'TotalAvailableWeight',@TotalAvailableWeight



INSERT INTO @Targets
SELECT 
    CASE
		WHEN  Coalesce(aw.AssignedWeight,0) < t.Weight THEN 1
		ELSE 0
	END Active,
	a.Id,
	a.Name, 
	t.Weight TargetWeight, 
	Coalesce(t.Pace, 100) Pace,
	floor(Coalesce(t.Pace,100) * .01 * t.Weight) PaceWeight,
	Coalesce(aw.AssignedWeight,0) AssignedWeight, 
	t.Weight - Coalesce(aw.AssignedWeight,0) RemainingWeight,
	CASE
		WHEN floor(Coalesce(t.Pace,100) * .01 * t.Weight) = Coalesce(aw.AssignedWeight,0)
		THEN 0 
		WHEN FLOOR((floor(Coalesce(t.Pace,100) * .01 * t.Weight)/@TotalPaceWeight) * @TotalAvailableWeight) > floor(Coalesce(t.Pace,100) * .01 * t.Weight) - Coalesce(aw.AssignedWeight,0)
		THEN floor(Coalesce(t.Pace,100) * .01 * t.Weight) - Coalesce(aw.AssignedWeight,0)
		ELSE FLOOR((floor(Coalesce(t.Pace,100) * .01 * t.Weight)/@TotalPaceWeight) * @TotalAvailableWeight)
	END ,
	coalesce(t.IsPermanentDropOffLocation,0) IsPermanentDropOffLocation 
FROM Affiliate a
INNER JOIN AffiliateTarget t on a.Id = t.AffiliateId
LEFT JOIN (	
	SELECT 
		i.AffiliateId AffiliateId,
		SUM(COALESCE(ir.Quantity,0)) AssignedWeight
	FROM Invoice i
	INNER JOIN InvoiceReconciliation ir ON ir.OEMInvoiceId = i.Id
	WHERE i.StateId = @StateId
	AND i.PlanYear = @PlanYear
	GROUP BY i.AffiliateId) aw ON aw.AffiliateId = a.Id
WHERE a.AffiliateTypeId = 1
AND (@OEMId = 0 OR a.Id = @OEMId)
AND t.PlanYear = @PlanYear
AND t.StateId = @StateId

SET NOCOUNT OFF
select * from @Targets

GO


insert into Role (AffiliateTypeId, RoleName)
values (6, 'Multi OEM User')
go




CREATE TABLE [dbo].[AffiliateMasterContact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasterAffiliateContactId] [int] NOT NULL,
	[AssociateContactId] [int] NOT NULL,
	[DefaultUser] [bit] NOT NULL,
 CONSTRAINT [PK_AffiliateMasterContact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AffiliateMasterContact]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateMasterContact_AffiliateContact] FOREIGN KEY([MasterAffiliateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO

ALTER TABLE [dbo].[AffiliateMasterContact] CHECK CONSTRAINT [FK_AffiliateMasterContact_AffiliateContact]
GO

ALTER TABLE [dbo].[AffiliateMasterContact]  WITH CHECK ADD  CONSTRAINT [FK_AffiliateMasterContact_AffiliateContact1] FOREIGN KEY([AssociateContactId])
REFERENCES [dbo].[AffiliateContact] ([Id])
GO

ALTER TABLE [dbo].[AffiliateMasterContact] CHECK CONSTRAINT [FK_AffiliateMasterContact_AffiliateContact1]
GO

ALTER TABLE [dbo].[AffiliateMasterContact] ADD  CONSTRAINT [DF_AffiliateMasterContact_DefaultUser]  DEFAULT ((0)) FOR [DefaultUser]
GO

