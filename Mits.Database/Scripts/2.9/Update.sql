

alter table invoice add  Verified bit not null DEFAULT 0
go

update Invoice set Verified = 1 where Verified = 0 and Approved = 1
go