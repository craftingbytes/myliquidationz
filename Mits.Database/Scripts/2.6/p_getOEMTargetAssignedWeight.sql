CREATE PROCEDURE [dbo].[p_getOEMTargetAssignedWeight]
	@OEMId int,
	@StateId int,
	@PlanYear int
	-- p_getOEMTargetAssignedWeight 5550, 37, 2011
	-- p_getOEMTargetAssignedWeight 0, 37, 2011
AS

IF 1=2
BEGIN
	SELECT 
		CAST(NULL AS  BIT ) Active,
		CAST(NULL AS  INT )OEMId,
		CAST(NULL AS  [varchar](100) ) OEMName ,
		CAST(NULL AS  Decimal(18,2)  ) TargetWeight ,
		CAST(NULL AS  Decimal(18,2)  ) AssigneWeight,
		CAST(NULL AS  Decimal(18,2)  ) RemainingWeight,
		CAST(NULL AS  Decimal(18,2)  ) RecommendedWeight,
		CAST(NULL AS  BIT )  IsPermanentDropOffLocation
END 

SET NOCOUNT ON

declare @TotalAvailableWeight decimal(18,2)
declare @TotalProcessedWeight decimal(18,2)
declare @TotalAssignedWeight decimal(18,2)
declare @TotalTargetWeight decimal(18,2)
declare @Targets table(
	Active bit,
	OEMId int,
	OEMName nvarchar(100),
	TargetWeight Decimal(18,2),
	AssigneWeight Decimal(18,2),
	RemainingWeight Decimal (18,2),
	RecommendedWeight Decimal (18,2),
	IsPermanentDropOffLocation bit	
) 


SELECT @TotalProcessedWeight = SUM(COALESCE(ii.Quantity,0))
FROM Invoice i
INNER JOIN InvoiceItem ii ON ii.InvoiceId = i.Id
INNER JOIN Affiliate a on a.Id = i.AffiliateId
WHERE a.AffiliateTypeId = 2
AND i.PlanYear = @PlanYear
AND i.StateId = @StateId
AND i.Approved = 1

SELECT @TotalAssignedWeight = SUM(COALESCE(ir.Quantity,0))
FROM Invoice i
INNER JOIN InvoiceReconciliation ir ON ir.OEMInvoiceId = i.Id
WHERE i.StateId = @StateId
AND i.PlanYear = @PlanYear


SELECT @TotalTargetWeight = SUM(COALESCE(t.Weight,0))
FROM Affiliate a
INNER JOIN AffiliateTarget t on a.Id = t.AffiliateId
WHERE a.AffiliateTypeId = 1
AND (@OEMId = 0 OR a.Id = @OEMId)
AND t.StateId = @StateId
AND t.PlanYear = @PlanYear


SELECT @TotalAvailableWeight = @TotalProcessedWeight - @TotalAssignedWeight

--SELECT 'TotalAssignedWeight', @TotalAssignedWeight
--SELECT 'TotalProcessedWeight', @TotalProcessedWeight
--SELECT 'TotalTargetWeight', @TotalTargetWeight
--SELECT 'TotalAvailableWeight',@TotalAvailableWeight



INSERT INTO @Targets
SELECT 
    CASE
		WHEN  Coalesce(aw.AssignedWeight,0) < t.Weight THEN 1
		ELSE 0
	END Active,
	a.Id,
	a.Name, 
	t.Weight TargetWeight, 
	Coalesce(aw.AssignedWeight,0) AssignedWeight, 
	t.Weight - Coalesce(aw.AssignedWeight,0) RemainingWeight,
	CASE
		WHEN t.Weight = Coalesce(aw.AssignedWeight,0)
		THEN 0 
		WHEN (t.Weight/@TotalTargetWeight) * @TotalAvailableWeight > t.Weight - Coalesce(aw.AssignedWeight,0)
		THEN t.Weight - Coalesce(aw.AssignedWeight,0)
		ELSE (t.Weight/@TotalTargetWeight) * @TotalAvailableWeight 
	END ,
	coalesce(t.IsPermanentDropOffLocation,0) IsPermanentDropOffLocation 
FROM Affiliate a
INNER JOIN AffiliateTarget t on a.Id = t.AffiliateId
LEFT JOIN (	
	SELECT 
		i.AffiliateId AffiliateId,
		SUM(COALESCE(ir.Quantity,0)) AssignedWeight
	FROM Invoice i
	INNER JOIN InvoiceReconciliation ir ON ir.OEMInvoiceId = i.Id
	WHERE i.StateId = @StateId
	AND i.PlanYear = @PlanYear
	GROUP BY i.AffiliateId) aw ON aw.AffiliateId = a.Id
WHERE a.AffiliateTypeId = 1
AND (@OEMId = 0 OR a.Id = @OEMId)
AND t.PlanYear = @PlanYear
AND t.StateId = @StateId

SET NOCOUNT OFF
select * from @Targets
GO


