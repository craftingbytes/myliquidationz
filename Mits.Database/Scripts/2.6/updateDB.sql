Alter table InvoiceReconciliation add Rate money null


update ir
set Rate =  cr.Rate
FROM  InvoiceReconciliation ir
INNER JOIN Invoice i on ir.OEMInvoiceId = i.Id
inner join InvoiceItem ii on ii.Id = ir.ProcesserInvoiceItemId
inner JOIN AffiliateContractRate cr on	cr.AffiliateId = i.AffiliateId
										and cr.StateId = i.StateId
										and cr.PlanYear = i.PlanYear
										and cr.ServiceTypeId = 10
										and cr.ProductTypeId = ii.ProductTypeId
										and cr.QuantityTypeId = ii.QuantityTypeId 
where  cr.ServiceTypeId = 10

