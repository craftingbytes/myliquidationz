

CREATE PROCEDURE [dbo].[p_getAvailableProcessedWeight]
	@StateId int,
	@PlanYear int,
	@DisplayReconciled bit,
	@TotalAvailableWeight decimal(18,2) OUTPUT
	/*
	DECLARE @STUFF DECIMAL(18,2)
	exec p_getAvailableProcessedWeight 37,2011, @Stuff OUTPUT
	select @stuff  
	*/
as 
IF 1=2 
BEGIN
	SELECT 
		CAST(NULL AS  BIT ) Active,
		CAST(NULL AS  INT ) InvoiceId,
		CAST(NULL AS  [varchar](100) ) ProcessingDate ,
		CAST(NULL AS  [varchar](100) ) CollectionMethod,
		CAST(NULL AS  Decimal(18,2)  ) Weight
END

SET NOCOUNT ON

DECLARE @ProcessedInvoiceWeight TABLE(
	Active bit,
	InvoiceId int,
	ProcessingDate nvarchar(100),
	CollectionMethod nvarchar(100),
	Weight decimal(18,2)
)

INSERT INTO @ProcessedInvoiceWeight
SELECT  
	CASE 
		WHEN ii.AvailableWeight > 0 THEN 1
		ELSE 0
	END Active,
	i.Id InvoiceId,
	COALESCE(CONVERT(nvarchar(10),pd.ProcessingDate, 101),'') ProcessingDate,
	COALESCE(cm.Method,'') CollectionMethod,
	ii.AvailableWeight
FROM Invoice i
INNER JOIN Affiliate a on a.Id = i.AffiliateId
LEFT JOIN ProcessingData pd ON pd.InvoiceId = i.Id
LEFT JOIN CollectionMethod cm ON cm.Id = pd.CollectionMethodId
LEFT JOIN (
	SELECT InvoiceId, SUM(coalesce(Quantity,0) - coalesce(ir.AssignedWeight,0)) AvailableWeight 
	FROM InvoiceItem ii
	LEFT JOIN (
		SELECT 
			ProcesserInvoiceItemId, 
			SUM(Coalesce(Quantity,0)) AssignedWeight 
		FROM InvoiceReconciliation
		GROUP BY ProcesserInvoiceItemId) ir  on ir.ProcesserInvoiceItemId = ii.Id		
	GROUP BY InvoiceId) ii on ii.InvoiceId = i.Id  
WHERE A.AffiliateTypeId =2
AND i.StateId = @StateId
AND i.PlanYear = @PlanYear
AND i.Approved = 1

SELECT @TotalAvailableWeight = COALESCE(SUM(COALESCE(Weight,0)),0) 
FROM @ProcessedInvoiceWeight


SET NOCOUNT OFF
SELECT * FROM @ProcessedInvoiceWeight
WHERE Active = 1
OR Active <> @DisplayReconciled
GO


