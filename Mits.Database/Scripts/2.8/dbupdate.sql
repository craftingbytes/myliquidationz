

CREATE TABLE [dbo].[StateWeightCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StateId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Ratio] [decimal](18, 4) NOT NULL,
 CONSTRAINT [PK_StateWeightCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[StateWeightCategory]  WITH CHECK ADD  CONSTRAINT [FK_StateWeightCategory_StateWeightCategory] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO

ALTER TABLE [dbo].[StateWeightCategory] CHECK CONSTRAINT [FK_StateWeightCategory_StateWeightCategory]
GO


ALTER TABLE [dbo].[ProcessingData] Add [StateWeightCategoryId] int null
GO


ALTER TABLE [dbo].[ProcessingData]  WITH CHECK ADD  CONSTRAINT [FK_ProcessingData_StateWeightCategory] FOREIGN KEY([StateWeightCategoryId])
REFERENCES [dbo].[StateWeightCategory] ([Id])
GO

ALTER TABLE [dbo].[ProcessingData] CHECK CONSTRAINT [FK_ProcessingData_StateWeightCategory]
GO


ALTER TABLE dbo.InvoiceItem Add OriginalQuantity Decimal(18,0) null
GO

update ii
	set OriginalQuantity = coalesce(Quantity,0) + coalesce(Tare,0)
from InvoiceItem ii
inner join Invoice i on i.Id = ii.InvoiceId
inner join Affiliate a on i.AffiliateId = a.Id
where a.AffiliateTypeId = 2 
and OriginalQuantity is null
GO





CREATE procedure [dbo].[p_changePickupState]
	@ProcessingDataId as int,
	@NewPickupStateId as int
		--  [p_changePickupState] 1681,28
as
	declare @OldPickupStateId int
	declare @AffiliateId int
	declare @Planyear int
	declare @InvoiceId int
	declare @Success bit
	declare @ReconciledWeight decimal(18,2)
	declare @Message nvarchar(200)
	declare @Approved bit

	
	
	select 
		@OldPickupStateId = i.StateId,
		@AffiliateId = i.AffiliateId,
		@Planyear = i.Planyear,
		@InvoiceId = i.Id,
		@Approved = Coalesce(i.Approved,0)
	from ProcessingData pd
	inner join Invoice i on i.Id = pd.InvoiceId
	left join InvoiceItem ii on ii.InvoiceId = i.Id
	where pd.Id = @ProcessingDataId

	
	set @ReconciledWeight = 0
	select 
		@ReconciledWeight = SUM(coalesce(ir.quantity,0))
	from Invoice i
	inner join InvoiceItem ii on i.Id = ii.InvoiceId
	inner join InvoiceReconciliation ir on ir.ProcesserInvoiceItemId = ii.Id
	where i.Id = @InvoiceId

	if (@ReconciledWeight > 0 )
	begin
		set @Success = 0
		set @Message = 'Cannot change pickup state. Weight has already been reconciled'
	end
	else if (@Approved = 1)
	begin
		set @Success = 0
		set @Message = 'Cannot change pickup state. Invoice is already approved'
	end
	else
	begin
		delete InvoiceItem where InvoiceId = @InvoiceId
		update i
			set i.StateId = @NewPickupStateId,
				i.RegionName = (select name from State where Id = @NewPickupStateId) 
		from Invoice i 
		where i.Id = @InvoiceId
		
		update p 
		set p.PickupStateId = @NewPickupStateId, 
			p.StateWeightCategoryId = null,
			p.PickupCityId = null,
			p.State = (select name from State where Id = @NewPickupStateId) 
		from ProcessingData p  
		where Id = @ProcessingDataId
		set @Message = ''
		set @Success = 1
	end
	
	if (@@ERROR = 0 and @Success = 1)
	begin
		select 
			@Success Success,
			@NewPickupStateId PickupStateId,
			@Message [Message]
	end
	else if (@@ERROR = 0)
	begin
		select 
			@Success Success,
			@OldPickupStateId PickupStateId,
			@Message [Message]
	end
	else
	begin
		select 
			0 Success,
			@OldPickupStateId PickupStateId,
			'An error occured while processing your request' [Message]
	end
	 
	
	
GO



CREATE procedure [dbo].[p_changePlanYear]
	@ProcessingDataId as int,
	@NewPlanYear as int
		--  [p_changePlanYear] 1681,2014
as
	declare @OldPlanyear int
	declare @AffiliateId int
	declare @InvoiceId int
	declare @Success bit
	declare @ReconciledWeight decimal(18,2)
	declare @Message nvarchar(200)
	declare @Approved bit
	declare @MissingRates int

	
	
	select 
		@AffiliateId = i.AffiliateId,
		@OldPlanyear = i.Planyear,
		@InvoiceId = i.Id,
		@Approved = Coalesce(i.Approved,0)
	from ProcessingData pd
	inner join Invoice i on i.Id = pd.InvoiceId
	left join InvoiceItem ii on ii.InvoiceId = i.Id
	where pd.Id = @ProcessingDataId
	
	select 
		@MissingRates = COUNT(*) 
	from Invoice i
	inner join InvoiceItem ii on i.Id = ii.InvoiceId
	where i.Id = @InvoiceId
	and Not Exists 
	   (select 0x0
		from AffiliateContractRate ac
		where  ac.Planyear = @NewPlanYear 
		and ac.AffiliateId = i.AffiliateId
		and ac.StateId = i.StateId
		and ac.ProductTypeId = ii.ProductTypeId
		and ac.ServiceTypeId = ii.ServiceTypeId
		and ac.QuantityTypeId = ii.QuantityTypeId)


	
	set @ReconciledWeight = 0
	select 
		@ReconciledWeight = SUM(coalesce(ir.quantity,0))
	from Invoice i
	inner join InvoiceItem ii on i.Id = ii.InvoiceId
	inner join InvoiceReconciliation ir on ir.ProcesserInvoiceItemId = ii.Id
	where i.Id = @InvoiceId

	if (@Approved = 1)
	begin
		set @Success = 0
		set @Message = 'Cannot change planyear. Invoice is already approved'
	end
	else if (@ReconciledWeight > 0 )
	begin
		set @Success = 0
		set @Message = 'Cannot change planyear. Weight has already been reconciled'
	end
	else if (@MissingRates > 0 )
	begin
		set @Success = 0
		set @Message = 'Cannot change planyear. Rates not available for some or all products listed.'
	end
	else
	begin
		set @Success = 1
		set @Message = ''
		update i
			set i.Planyear = i.Planyear
		from Invoice i 
		where i.Id = @InvoiceId
		
		update ii
		set ii.Rate = ac.Rate
		from InvoiceItem ii
		inner join Invoice i on i.Id = ii.Invoiceid
		inner join AffiliateContractRate ac on	ac.ProductTypeId = ii.ProductTypeId
												and ac.ServiceTypeId = ii.ServiceTypeId
												and ac.QuantityTypeId = ii.QuantityTypeId
												and ac.Planyear = i.PlanYear 
												and ac.AffiliateId = i.AffiliateId
												and ac.StateId = i.StateId
		where i.Id = @InvoiceId  
		

	end
	
	if (@@ERROR = 0 and @Success = 1)
	begin
		select 
			@Success Success,
			@NewPlanYear NewPlanYear,
			@Message [Message]
	end
	else if (@@ERROR = 0)
	begin
		select 
			@Success Success,
			@OldPlanYear NewPlanYear,
			@Message [Message]
	end
	else
	begin
		select 
			0 Success,
			@OldPlanYear NewPlanYear,
			'An error occured while processing your request' [Message]
	end
	 
	
	
GO

CREATE procedure [dbo].[p_changeProcessingDataWeightCategory]
	@ProcessingDataId as int,
	@NewCategoryId as int
		--p_changeProcessingDataWeightCategory 1681,1
as
	declare @OldCategoryId int
	declare @AffiliateId int
	declare @StateId int
	declare @Planyear int
	declare @InvoiceId int
	declare @NewWeight decimal(10,2)
	declare @OldWeight decimal(10,2)
	declare @OldOriginalWeight decimal(10,2)
	declare @TargetWeight decimal(10,2)
	declare @ProcessedWeight decimal(10,2)
	declare @Success bit
	declare @NewRatio decimal(10,2)
	
	
	select 
		@OldCategoryId = coalesce(pd.StateWeightCategoryId,0),
		@AffiliateId = i.AffiliateId,
		@StateId = i.StateId,
		@Planyear = i.Planyear,
		@InvoiceId = i.Id,
		@OldOriginalWeight = sum(coalesce(ii.OriginalQuantity,0) - coalesce(ii.Tare,0)),
		@OldWeight = SUM(coalesce(ii.Quantity,0))
	from ProcessingData pd
	inner join Invoice i on i.Id = pd.InvoiceId
	left join InvoiceItem ii on ii.InvoiceId = i.Id
	where pd.Id = @ProcessingDataId
	group by 
		pd.StateWeightCategoryId,
		i.AffiliateId,
		i.StateId,
		i.Planyear,
		i.Id
	
	set @ProcessedWeight = 0
	select 
		@ProcessedWeight = SUM(coalesce(ii.quantity,0))
	from Invoice i
	left join InvoiceItem ii on i.Id = ii.InvoiceId
	where i.StateId = @StateId
	and i.AffiliateId = @AffiliateId
	and i.PlanYear = @Planyear
	
	set @TargetWeight = 0.0
	select 
		top 1
		@TargetWeight = coalesce(Weight,0)
	from AffiliateTarget 
	where StateId = @StateId
	and AffiliateId = @AffiliateId
	and PlanYear = @Planyear
	and ServiceTypeId = 3
	and QuantityTypeId = 2
	
	if (@NewCategoryId = 0)
		set @NewRatio = 1
	else
	begin
		select  
			@NewRatio = coalesce(ratio,1),
			@NewWeight = coalesce(ratio,1) * @OldOriginalWeight
		from StateWeightCategory
		where Id = @NewCategoryId 
	end
	
	if (@NewRatio is null)
		Set @Success = 0
	else if (@TargetWeight < @ProcessedWeight + (@NewWeight - @OldWeight))
		Set @Success = 0
	else 
		Set @Success = 1
		
	if (@Success = 1 and @NewCategoryId = 0)
	begin
		update ProcessingData
		set StateWeightCategoryId = null
		where Id = @ProcessingDataId
	end
	else if (@Success = 1 )
	begin
		update ProcessingData
		set StateWeightCategoryId = @NewCategoryId
		where Id = @ProcessingDataId
	end
	
	if (@Success = 1 and @@ERROR = 0)
	begin
		update InvoiceItem
		set Quantity = (OriginalQuantity - coalesce(Tare,0)) * @NewRatio
		where InvoiceId = @InvoiceId
	end
	else
	begin
		set @Success = 0
	end
		
	if (@Success = 1 and @@ERROR = 0)
	begin
		select
			@Success Success,
			@NewCategoryId CategoryId
	end
	else
	begin
		select
			@Success Success,
			@OldCategoryId CategoryId
	end
	 
	 
	
	
GO





create procedure [dbo].[p_isBelowTarget]
	@InvoiceId int,
	@Weight decimal(10,2)
as
	declare @TargetWeight decimal(10,2)
	declare @ProcessedWeight decimal(10,2)
	declare @AffiliateId int
	declare @StateId int
	declare @Planyear int
	declare @Success bit
	
	
	
	select 
		@AffiliateId = i.AffiliateId,
		@StateId = i.StateId,
		@Planyear = i.Planyear,
		@InvoiceId = i.Id,
		@ProcessedWeight = SUM(coalesce(ii.quantity,0))
	from Invoice i 
	left join InvoiceItem ii on ii.InvoiceId = i.Id
	where i.Id = @InvoiceId
	group by 
		i.AffiliateId,
		i.StateId,
		i.Planyear,
		i.Id
		
	set @TargetWeight = 0.0
	
	select 
		top 1
		@TargetWeight = coalesce(Weight,0)
	from AffiliateTarget 
	where StateId = @StateId
	and AffiliateId = @AffiliateId
	and PlanYear = @Planyear
	and ServiceTypeId = 3
	and QuantityTypeId = 2
	
	if (@TargetWeight < @ProcessedWeight + @Weight)
		set @Success = 0 
	else
		set @Success = 1
		
	select @Success Success
	
		
	
GO

