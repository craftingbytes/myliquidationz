﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using Costco.DAL.Interfaces;
using System.Data.Entity;

namespace Costco.DAL.Infrastructure
{
    class CostcoRepositoryContext : IRepositoryContext
    {
        private const string OBJECT_CONTEXT_KEY = "Costco.DAL.EntityModels";
        public DbSet<T> GetObjectSet<T>()
            where T : class
        {
            return ContextManager.GetDbContext(OBJECT_CONTEXT_KEY).Set<T>();
        }

        /// <summary>
        /// Returns the active object context
        /// </summary>
        public DbContext DbContext
        {
            get
            {
                return ContextManager.GetDbContext(OBJECT_CONTEXT_KEY);
            }
        }

        public int SaveChanges()
        {
            return this.DbContext.SaveChanges();
        }

        public void Terminate()
        {
            ContextManager.SetRepositoryContext(null, OBJECT_CONTEXT_KEY);
        }
    }
}
