﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.Objects;
using System.Web;
using System.Threading;
using Costco.DAL.EntityModels;
using System.Data.Entity;

namespace Costco.DAL.Infrastructure
{
    /// <summary>
    /// Manages the lifecycle of the EF's object context
    /// </summary>
    /// <remarks>Uses a context per http request approach or one per thread in non web applications</remarks>
    public static class ContextManager
    {
        #region Private Members

        // accessed via lock(_threadDbContexts), only required for multi threaded non web applications
        private static readonly Hashtable _threadDbContexts = new Hashtable();

        #endregion

        public static DbSet<T> GetSet<T>(T entity, string contextKey)
            where T : class
        {
            return GetDbContext(contextKey).Set<T>();
        }

        /// <summary>
        /// Returns the active object context
        /// </summary>
        public static DbContext GetDbContext(string contextKey)
        {
            DbContext objectContext = GetCurrentDbContext(contextKey);
            if (objectContext == null) // create and store the object context
            {
                objectContext = new CostcoEntities();
                StoreCurrentDbContext(objectContext, contextKey);
            }
            return objectContext;
        }

        /// <summary>
        /// Gets the repository context
        /// </summary>
        /// <returns>An object representing the repository context</returns>
        public static object GetRepositoryContext(string contextKey)
        {
            return GetDbContext(contextKey);
        }

        /// <summary>
        /// Sets the repository context
        /// </summary>
        /// <param name="repositoryContext">An object representing the repository context</param>
        public static void SetRepositoryContext(object repositoryContext, string contextKey)
        {
            if (repositoryContext == null)
            {
                RemoveCurrentDbContext(contextKey);
            }
            else if (repositoryContext is DbContext)
            {
                StoreCurrentDbContext((DbContext)repositoryContext, contextKey);
            }
        }


        #region Object Context Lifecycle Management

        /// <summary>
        /// gets the current object context 		
        /// </summary>
        private static DbContext GetCurrentDbContext(string contextKey)
        {
            DbContext objectContext = null;
            if (HttpContext.Current == null)
                objectContext = GetCurrentThreadDbContext(contextKey);
            else
                objectContext = GetCurrentHttpContextDbContext(contextKey);
            return objectContext;
        }

        /// <summary>
        /// sets the current session 		
        /// </summary>
        private static void StoreCurrentDbContext(DbContext objectContext, string contextKey)
        {
            if (HttpContext.Current == null)
                StoreCurrentThreadDbContext(objectContext, contextKey);
            else
                StoreCurrentHttpContextDbContext(objectContext, contextKey);
        }

        /// <summary>
        /// remove current object context 		
        /// </summary>
        private static void RemoveCurrentDbContext(string contextKey)
        {
            if (HttpContext.Current == null)
                RemoveCurrentThreadDbContext(contextKey);
            else
                RemoveCurrentHttpContextDbContext(contextKey);
        }

        #region private methods - HttpContext related

        /// <summary>
        /// gets the object context for the current thread
        /// </summary>
        private static DbContext GetCurrentHttpContextDbContext(string contextKey)
        {
            DbContext objectContext = null;
            if (HttpContext.Current.Items.Contains(contextKey))
                objectContext = (DbContext)HttpContext.Current.Items[contextKey];
            return objectContext;
        }

        private static void StoreCurrentHttpContextDbContext(DbContext objectContext, string contextKey)
        {
            if (HttpContext.Current.Items.Contains(contextKey) && HttpContext.Current.Items[contextKey] != null)
                HttpContext.Current.Items[contextKey] = objectContext;
            else
                HttpContext.Current.Items.Add(contextKey, objectContext);
        }

        /// <summary>
        /// remove the session for the currennt HttpContext
        /// </summary>
        private static void RemoveCurrentHttpContextDbContext(string contextKey)
        {
            DbContext objectContext = GetCurrentHttpContextDbContext(contextKey);
            if (objectContext != null)
            {
                HttpContext.Current.Items.Remove(contextKey);
                objectContext.Dispose();
            }
        }

        #endregion

        #region private methods - ThreadContext related

        /// <summary>
        /// gets the session for the current thread
        /// </summary>
        private static DbContext GetCurrentThreadDbContext(string contextKey)
        {
            DbContext objectContext = null;
            Thread threadCurrent = Thread.CurrentThread;
            if (threadCurrent.Name == null)
                threadCurrent.Name = contextKey;
            else
            {
                object threadDbContext = null;
                lock (_threadDbContexts.SyncRoot)
                {
                    threadDbContext = _threadDbContexts[contextKey];
                }
                if (threadDbContext != null)
                    objectContext = (DbContext)threadDbContext;
            }
            return objectContext;
        }

        private static void StoreCurrentThreadDbContext(DbContext objectContext, string contextKey)
        {
            lock (_threadDbContexts.SyncRoot)
            {
                if (_threadDbContexts.Contains(contextKey))
                    _threadDbContexts[contextKey] = objectContext;
                else
                    _threadDbContexts.Add(contextKey, objectContext);
            }
        }

        private static void RemoveCurrentThreadDbContext(string contextKey)
        {
            lock (_threadDbContexts.SyncRoot)
            {
                if (_threadDbContexts.Contains(contextKey))
                {
                    DbContext objectContext = (DbContext)_threadDbContexts[contextKey];
                    if (objectContext != null)
                    {
                        objectContext.Dispose();
                    }
                    _threadDbContexts.Remove(contextKey);
                }
            }
        }

        /*
        private static string BuildContextThreadName()
        {
            return Thread.CurrentThread.Name;
        }

        private static string BuildHttpContextName()
        {
            return OBJECT_CONTEXT_KEY;
        }*/

        #endregion

        #endregion

    }
}
