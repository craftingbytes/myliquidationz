﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Costco.DAL.Models
{
    public class UserReport
    {
        public object Id_m { get; set; }
        public string URL_1 { get; set; }
        public object Id_1 { get; set; }
        public string URL_2 { get; set; }
        public object Id_2 { get; set; }
        public string URL_3 { get; set; }
        public object Id_3 { get; set; }
        public string URL_m { get; set; }
        public object Assigned { get; set; }
        public string Caption_1 { get; set; }
        public string Caption_2 { get; set; }
        public string Caption_3 { get; set; }
        public string Caption_m { get; set; }
    }
}
