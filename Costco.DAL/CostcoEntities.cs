﻿using Costco.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Costco.DAL.EntityModels
{
    public partial class CostcoEntities
    {
        public void p_AddAuditReportState(int auditReportId, int? stateId)
        {
            Database.ExecuteSqlCommand("p_AddAuditReportState @AuditReportId, @StateId",
                new SqlParameter("@AuditReportId", auditReportId),
                new SqlParameter("@StateId", stateId));
        }

        public void p_AddDocumentToAuditReport(int documentRelationId)
        {
            Database.ExecuteSqlCommand("p_AddDocumentToAuditReport @DocumentRelationId",
                new SqlParameter("@DocumentRelationId", documentRelationId));
        }

        public void p_InsertUserReport(int affiliateContactId, int reportId, int groupId)
        {
            Database.ExecuteSqlCommand("p_InsertUserReport @AffiliateContactId, @ReportId, @GroupId",
                new SqlParameter("@AffiliateContactId", affiliateContactId),
                new SqlParameter("@ReportId", reportId),
                new SqlParameter("@GroupId", groupId)
                );
        }

        public void p_ChangeDisplayOrder(long userReportListId, int direction)
        {
            Database.ExecuteSqlCommand("p_ChangeDisplayOrder @UserReportListId, @Direction",
                new SqlParameter("@UserReportListId", userReportListId),
                new SqlParameter("@Direction", direction)
                );
        }

        public List<decimal?> p_UpdateSalesOrderSoldAmount(int salesOrderId)
        {
            var result = Database.SqlQuery<decimal?>("p_UpdateSalesOrderSoldAmount @SalesOrderId",
                new SqlParameter("@SalesOrderId", salesOrderId))
                .ToList();
            return result;
        }

        public void p_UpdateSalvageSalesOrderSoldAmount(int salvageSalesOrderId, out decimal soldAmount, out decimal retailAmount)
        {
            using (var conn = Database.Connection)
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "p_UpdateSalvageSalesOrderSoldAmount";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@SalvageSalesOrderId", salvageSalesOrderId));
                    cmd.Parameters.Add(new SqlParameter("@SoldAmount", SqlDbType.Money, 4) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@RetailAmount", SqlDbType.Money, 4) { Direction = ParameterDirection.Output });
                    using (var reader = cmd.ExecuteReader())
                    {
                        soldAmount = (decimal)cmd.Parameters[1].Value;
                        retailAmount = (decimal)cmd.Parameters[2].Value;
                    }
                }
            }
        }

        public List<UserReport> p_GetUserReportList(int affiliateContactId)
        {
            return Database.SqlQuery<UserReport>("p_GetUserReportList @AffiliateContactId",
                new SqlParameter("@AffiliateContactId", affiliateContactId))
                .ToList();
        }
    }
}
