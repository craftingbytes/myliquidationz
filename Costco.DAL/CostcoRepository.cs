﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;
using System.Linq.Expressions;
using Costco.DAL.Interfaces;
using System.Data.Objects;
using Costco.DAL.Infrastructure;

namespace Costco.DAL
{
    public class CostcoRepository<T> : RepositoryBase<T>, IRepository<T> where T : class
    {
        public CostcoRepository()
            : this(new CostcoRepositoryContext())
        {
        }

        public CostcoRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
