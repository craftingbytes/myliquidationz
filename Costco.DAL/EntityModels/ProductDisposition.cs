namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductDisposition")]
    public partial class ProductDisposition
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string ItemId { get; set; }

        [Required]
        [StringLength(50)]
        public string Disposition { get; set; }

        public DateTime TimeStamp { get; set; }

        public int? AffiliateContactId { get; set; }

        public virtual AffiliateContact AffiliateContact { get; set; }
    }
}
