namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_MaxBid
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SalesOrderId { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime Created { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ASR_ID { get; set; }

        [StringLength(200)]
        public string ASR_Name { get; set; }

        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }

        public int? BidCount { get; set; }
    }
}
