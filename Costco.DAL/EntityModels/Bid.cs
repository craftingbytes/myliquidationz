namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bid")]
    public partial class Bid
    {
        public long Id { get; set; }

        public int SalesOrderId { get; set; }

        public int AffiliateId { get; set; }

        public DateTime Created { get; set; }

        [Column(TypeName = "money")]
        public decimal Amount { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual SalesOrder SalesOrder { get; set; }
    }
}
