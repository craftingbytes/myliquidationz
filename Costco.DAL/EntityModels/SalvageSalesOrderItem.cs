namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SalvageSalesOrderItem")]
    public partial class SalvageSalesOrderItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SalvageSalesOrderItem()
        {
            SalesOrderSSOItems = new HashSet<SalesOrderSSOItem>();
        }

        public long Id { get; set; }

        public int SalvageSalesOrderId { get; set; }

        [Required]
        [StringLength(50)]
        public string ItemId { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        [Column(TypeName = "money")]
        public decimal RetailValue { get; set; }

        [Column(TypeName = "money")]
        public decimal SellingPrice { get; set; }

        public int Quantity { get; set; }

        public int? Missing { get; set; }

        [StringLength(50)]
        public string PalletUnitNumber { get; set; }

        public int? Store { get; set; }

        public int? Sold { get; set; }

        public int? Pulled { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderSSOItem> SalesOrderSSOItems { get; set; }

        public virtual SalvageSalesOrder SalvageSalesOrder { get; set; }
    }
}
