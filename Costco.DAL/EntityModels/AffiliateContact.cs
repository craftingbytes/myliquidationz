namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateContact")]
    public partial class AffiliateContact
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AffiliateContact()
        {
            AuditEvents = new HashSet<AuditEvent>();
            Documents = new HashSet<Document>();
            ProductDescriptions = new HashSet<ProductDescription>();
            ProductDispositions = new HashSet<ProductDisposition>();
            ProductPrices = new HashSet<ProductPrice>();
            UserReportLists = new HashSet<UserReportList>();
        }

        public int Id { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(3)]
        public string MiddleInitials { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(100)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(150)]
        public string Password { get; set; }

        public bool? Active { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        [StringLength(11)]
        public string Zip { get; set; }

        public int? AffiliateId { get; set; }

        public int? SecurityQuestionId { get; set; }

        [StringLength(500)]
        public string Answer { get; set; }

        public int? LoginTryCount { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? LastLoginTime { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(200)]
        public string Notes { get; set; }

        public int? RoleId { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual City City { get; set; }

        public virtual Role Role { get; set; }

        public virtual SecurityQuestion SecurityQuestion { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AuditEvent> AuditEvents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> Documents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductDescription> ProductDescriptions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductDisposition> ProductDispositions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductPrice> ProductPrices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserReportList> UserReportLists { get; set; }
    }
}
