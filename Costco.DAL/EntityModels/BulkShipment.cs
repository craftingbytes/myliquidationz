namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BulkShipment")]
    public partial class BulkShipment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BulkShipment()
        {
            BulkShipmentItems = new HashSet<BulkShipmentItem>();
            SalesOrderBulkItems = new HashSet<SalesOrderBulkItem>();
        }

        public int Id { get; set; }

        public int ShipToAffiliateId { get; set; }

        [StringLength(100)]
        public string ReceivedBy { get; set; }

        [StringLength(1000)]
        public string ReceivingComments { get; set; }

        public int? ShipmentTotalWeight { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public int? ReceivedTotalWeight { get; set; }

        [StringLength(50)]
        public string PickupCompanyName { get; set; }

        [StringLength(100)]
        public string PickupAddress { get; set; }

        public int? PickupStateId { get; set; }

        public int? PickupCityId { get; set; }

        [StringLength(20)]
        public string PickupZip { get; set; }

        public DateTime? PickupDate { get; set; }

        public int? PayableWeight { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual City City { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BulkShipmentItem> BulkShipmentItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderBulkItem> SalesOrderBulkItems { get; set; }
    }
}
