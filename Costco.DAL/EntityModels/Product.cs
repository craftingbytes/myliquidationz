namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string ItemId { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [StringLength(100)]
        public string Description2 { get; set; }

        public decimal? Weight { get; set; }

        public bool? CoveredDevice { get; set; }

        public int? ProductTypeId { get; set; }

        public virtual ProductType ProductType { get; set; }
    }
}
