namespace Costco.DAL.EntityModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CostcoEntities : DbContext
    {
        public CostcoEntities()
            : base("name=CostcoEntities")
        {
        }

        public virtual DbSet<Affiliate> Affiliates { get; set; }
        public virtual DbSet<AffiliateCertification> AffiliateCertifications { get; set; }
        public virtual DbSet<AffiliateContact> AffiliateContacts { get; set; }
        public virtual DbSet<AffiliateRate> AffiliateRates { get; set; }
        public virtual DbSet<AffiliateShared> AffiliateShareds { get; set; }
        public virtual DbSet<AffiliateType> AffiliateTypes { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<AuditAction> AuditActions { get; set; }
        public virtual DbSet<AuditEvent> AuditEvents { get; set; }
        public virtual DbSet<AuditReport> AuditReports { get; set; }
        public virtual DbSet<Bid> Bids { get; set; }
        public virtual DbSet<BulkShipment> BulkShipments { get; set; }
        public virtual DbSet<BulkShipmentItem> BulkShipmentItems { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<ConditionType> ConditionTypes { get; set; }
        public virtual DbSet<County> Counties { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentRelation> DocumentRelations { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<ForeignTable> ForeignTables { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<InvoiceAdjustment> InvoiceAdjustments { get; set; }
        public virtual DbSet<InvoiceType> InvoiceTypes { get; set; }
        public virtual DbSet<ItemProfitCost> ItemProfitCosts { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<PlanYear> PlanYears { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductDescription> ProductDescriptions { get; set; }
        public virtual DbSet<ProductDisposition> ProductDispositions { get; set; }
        public virtual DbSet<ProductPrice> ProductPrices { get; set; }
        public virtual DbSet<ProductType> ProductTypes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<SalesOrder> SalesOrders { get; set; }
        public virtual DbSet<SalesOrderBulkItem> SalesOrderBulkItems { get; set; }
        public virtual DbSet<SalesOrderItem> SalesOrderItems { get; set; }
        public virtual DbSet<SalesOrderSSOItem> SalesOrderSSOItems { get; set; }
        public virtual DbSet<SalvageSalesOrder> SalvageSalesOrders { get; set; }
        public virtual DbSet<SalvageSalesOrderItem> SalvageSalesOrderItems { get; set; }
        public virtual DbSet<SecurityQuestion> SecurityQuestions { get; set; }
        public virtual DbSet<ServiceType> ServiceTypes { get; set; }
        public virtual DbSet<Shipment> Shipments { get; set; }
        public virtual DbSet<ShipmentItem> ShipmentItems { get; set; }
        public virtual DbSet<SiteImage> SiteImages { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<UserReportList> UserReportLists { get; set; }
        public virtual DbSet<ActiveItem> ActiveItems { get; set; }
        public virtual DbSet<V_BulkShipmentInventory> V_BulkShipmentInventory { get; set; }
        public virtual DbSet<V_InvoiceASRPayable> V_InvoiceASRPayable { get; set; }
        public virtual DbSet<V_InvoiceCostcoBill> V_InvoiceCostcoBill { get; set; }
        public virtual DbSet<V_MaxAffiliateBid> V_MaxAffiliateBid { get; set; }
        public virtual DbSet<V_MaxBid> V_MaxBid { get; set; }
        public virtual DbSet<v_MaxProductPrice> v_MaxProductPrice { get; set; }
        public virtual DbSet<V_SalesOrderItemAll> V_SalesOrderItemAll { get; set; }
        public virtual DbSet<V_SalvagSalesOrderBought> V_SalvagSalesOrderBought { get; set; }
        public virtual DbSet<V_ShipmentAvailableToSellAll> V_ShipmentAvailableToSellAll { get; set; }
        public virtual DbSet<V_ShipmentInventory> V_ShipmentInventory { get; set; }
        public virtual DbSet<V_ShipmentItemExtended> V_ShipmentItemExtended { get; set; }
        public virtual DbSet<V_SSOrderInventory> V_SSOrderInventory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.WebSite)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Latitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Affiliate>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.AffiliateCertifications)
                .WithRequired(e => e.Affiliate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.AffiliateRates)
                .WithRequired(e => e.Affiliate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.AffiliateShareds)
                .WithRequired(e => e.Affiliate)
                .HasForeignKey(e => e.ParentAffiliateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.AffiliateShareds1)
                .WithRequired(e => e.Affiliate1)
                .HasForeignKey(e => e.SharedAffiliateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.Bids)
                .WithRequired(e => e.Affiliate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.BulkShipments)
                .WithRequired(e => e.Affiliate)
                .HasForeignKey(e => e.ShipToAffiliateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.Invoices)
                .WithRequired(e => e.Affiliate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.Roles)
                .WithOptional(e => e.Affiliate)
                .HasForeignKey(e => e.CreateByAffiliateId);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.SalesOrders)
                .WithRequired(e => e.Affiliate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.SalvageSalesOrders)
                .WithRequired(e => e.Affiliate)
                .HasForeignKey(e => e.AffiliateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.SalvageSalesOrders1)
                .WithOptional(e => e.Affiliate1)
                .HasForeignKey(e => e.SoldToAffiliateId);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.Shipments)
                .WithRequired(e => e.Affiliate)
                .HasForeignKey(e => e.AffiliateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Affiliate>()
                .HasMany(e => e.Shipments1)
                .WithRequired(e => e.Affiliate1)
                .HasForeignKey(e => e.ShipToAffiliateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.MiddleInitials)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Answer)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateContact>()
                .HasMany(e => e.AuditEvents)
                .WithRequired(e => e.AffiliateContact)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AffiliateContact>()
                .HasMany(e => e.UserReportLists)
                .WithRequired(e => e.AffiliateContact)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AffiliateRate>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AffiliateType>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<AffiliateType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Area>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Area>()
                .Property(e => e.Caption)
                .IsUnicode(false);

            modelBuilder.Entity<Area>()
                .Property(e => e.URL)
                .IsUnicode(false);

            modelBuilder.Entity<Area>()
                .HasMany(e => e.UserReportLists)
                .WithRequired(e => e.Area)
                .HasForeignKey(e => e.ReportId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AuditAction>()
                .HasMany(e => e.AuditEvents)
                .WithRequired(e => e.AuditAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Bid>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BulkShipment>()
                .Property(e => e.PickupCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<BulkShipment>()
                .Property(e => e.PickupAddress)
                .IsUnicode(false);

            modelBuilder.Entity<BulkShipment>()
                .Property(e => e.PickupZip)
                .IsUnicode(false);

            modelBuilder.Entity<BulkShipment>()
                .HasMany(e => e.BulkShipmentItems)
                .WithRequired(e => e.BulkShipment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BulkShipment>()
                .HasMany(e => e.SalesOrderBulkItems)
                .WithRequired(e => e.BulkShipment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BulkShipmentItem>()
                .HasMany(e => e.SalesOrderBulkItems)
                .WithRequired(e => e.BulkShipmentItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<City>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<City>()
                .HasMany(e => e.BulkShipments)
                .WithOptional(e => e.City)
                .HasForeignKey(e => e.PickupCityId);

            modelBuilder.Entity<City>()
                .HasMany(e => e.SalesOrders)
                .WithOptional(e => e.City)
                .HasForeignKey(e => e.SoldToCityId);

            modelBuilder.Entity<City>()
                .HasMany(e => e.SalvageSalesOrders)
                .WithOptional(e => e.City)
                .HasForeignKey(e => e.SoldToCityId);

            modelBuilder.Entity<City>()
                .HasMany(e => e.Shipments)
                .WithOptional(e => e.City)
                .HasForeignKey(e => e.PickupCityId);

            modelBuilder.Entity<City>()
                .HasMany(e => e.Shipments1)
                .WithOptional(e => e.City1)
                .HasForeignKey(e => e.ShipToCityId);

            modelBuilder.Entity<ConditionType>()
                .HasMany(e => e.BulkShipmentItems)
                .WithRequired(e => e.ConditionType)
                .HasForeignKey(e => e.ReceivedCondition)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConditionType>()
                .HasMany(e => e.ShipmentItems)
                .WithOptional(e => e.ConditionType)
                .HasForeignKey(e => e.ReceivedCondition);

            modelBuilder.Entity<County>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<County>()
                .Property(e => e.Abbreviation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.ServerFileName)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.UploadFileName)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.DocumentRelations)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DocumentType>()
                .Property(e => e.FileExtension)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .Property(e => e.DocumentTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .Property(e => e.ContentType)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .HasOptional(e => e.DocumentType1)
                .WithRequired(e => e.DocumentType2);

            modelBuilder.Entity<ForeignTable>()
                .Property(e => e.ForeignTableName)
                .IsUnicode(false);

            modelBuilder.Entity<ForeignTable>()
                .HasMany(e => e.DocumentRelations)
                .WithRequired(e => e.ForeignTable)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invoice>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Invoice>()
                .HasMany(e => e.Invoice1)
                .WithOptional(e => e.Invoice2)
                .HasForeignKey(e => e.ParentInvoiceId);

            modelBuilder.Entity<Invoice>()
                .HasMany(e => e.InvoiceAdjustments)
                .WithRequired(e => e.Invoice)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvoiceAdjustment>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceType>()
                .HasMany(e => e.Invoices)
                .WithRequired(e => e.InvoiceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ItemProfitCost>()
                .Property(e => e.Sales)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ProductPrice>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Role>()
                .Property(e => e.RoleName)
                .IsUnicode(false);

            modelBuilder.Entity<SalesOrder>()
                .Property(e => e.SoldAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalesOrder>()
                .Property(e => e.MinValue)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalesOrder>()
                .HasMany(e => e.Bids)
                .WithRequired(e => e.SalesOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalesOrder>()
                .HasMany(e => e.SalesOrderBulkItems)
                .WithRequired(e => e.SalesOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalesOrder>()
                .HasMany(e => e.SalesOrderItems)
                .WithRequired(e => e.SalesOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalesOrder>()
                .HasMany(e => e.SalesOrderSSOItems)
                .WithRequired(e => e.SalesOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalesOrderBulkItem>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalesOrderBulkItem>()
                .Property(e => e.SellingPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalesOrderItem>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalesOrderItem>()
                .Property(e => e.SellingPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalesOrderSSOItem>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalesOrderSSOItem>()
                .Property(e => e.SellingPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalvageSalesOrder>()
                .Property(e => e.SoldAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalvageSalesOrder>()
                .Property(e => e.RetailAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalvageSalesOrder>()
                .HasMany(e => e.SalesOrderSSOItems)
                .WithRequired(e => e.SalvageSalesOrder)
                .HasForeignKey(e => e.SSOId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalvageSalesOrder>()
                .HasMany(e => e.SalvageSalesOrderItems)
                .WithRequired(e => e.SalvageSalesOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalvageSalesOrderItem>()
                .Property(e => e.RetailValue)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalvageSalesOrderItem>()
                .Property(e => e.SellingPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SalvageSalesOrderItem>()
                .HasMany(e => e.SalesOrderSSOItems)
                .WithRequired(e => e.SalvageSalesOrderItem)
                .HasForeignKey(e => e.SSOItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SecurityQuestion>()
                .Property(e => e.Question)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceType>()
                .HasMany(e => e.AffiliateRates)
                .WithRequired(e => e.ServiceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Shipment>()
                .HasMany(e => e.SalesOrderItems)
                .WithRequired(e => e.Shipment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Shipment>()
                .HasMany(e => e.ShipmentItems)
                .WithRequired(e => e.Shipment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipmentItem>()
                .Property(e => e.ShipperPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ShipmentItem>()
                .HasMany(e => e.SalesOrderItems)
                .WithRequired(e => e.ShipmentItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SiteImage>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.Abbreviation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.BulkShipments)
                .WithOptional(e => e.State)
                .HasForeignKey(e => e.PickupStateId);

            modelBuilder.Entity<State>()
                .HasMany(e => e.SalesOrders)
                .WithOptional(e => e.State)
                .HasForeignKey(e => e.SoldToStateId);

            modelBuilder.Entity<State>()
                .HasMany(e => e.SalvageSalesOrders)
                .WithOptional(e => e.State)
                .HasForeignKey(e => e.SoldToStateId);

            modelBuilder.Entity<State>()
                .HasMany(e => e.Shipments)
                .WithOptional(e => e.State)
                .HasForeignKey(e => e.PickupStateId);

            modelBuilder.Entity<State>()
                .HasMany(e => e.Shipments1)
                .WithOptional(e => e.State1)
                .HasForeignKey(e => e.ShipToStateId);

            modelBuilder.Entity<ActiveItem>()
                .Property(e => e.ListingPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ActiveItem>()
                .Property(e => e.CurrentPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceASRPayable>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<V_InvoiceASRPayable>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceASRPayable>()
                .Property(e => e.SubTotal)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceASRPayable>()
                .Property(e => e.Ajustments)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceASRPayable>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceCostcoBill>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<V_InvoiceCostcoBill>()
                .Property(e => e.Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceCostcoBill>()
                .Property(e => e.SubTotal)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceCostcoBill>()
                .Property(e => e.Ajustments)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_InvoiceCostcoBill>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_MaxAffiliateBid>()
                .Property(e => e.ASR_Name)
                .IsUnicode(false);

            modelBuilder.Entity<V_MaxAffiliateBid>()
                .Property(e => e.Buyer_Name)
                .IsUnicode(false);

            modelBuilder.Entity<V_MaxAffiliateBid>()
                .Property(e => e.Buyer_City)
                .IsUnicode(false);

            modelBuilder.Entity<V_MaxAffiliateBid>()
                .Property(e => e.Buyer_State)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<V_MaxAffiliateBid>()
                .Property(e => e.Buyer_Zip)
                .IsUnicode(false);

            modelBuilder.Entity<V_MaxAffiliateBid>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_MaxAffiliateBid>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<V_MaxBid>()
                .Property(e => e.ASR_Name)
                .IsUnicode(false);

            modelBuilder.Entity<V_MaxBid>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<v_MaxProductPrice>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_SalesOrderItemAll>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<V_SalesOrderItemAll>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_SalesOrderItemAll>()
                .Property(e => e.SellingPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_SalesOrderItemAll>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_SalvagSalesOrderBought>()
                .Property(e => e.SoldAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_SalvagSalesOrderBought>()
                .Property(e => e.RetailAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_SalvagSalesOrderBought>()
                .Property(e => e.SellerName)
                .IsUnicode(false);

            modelBuilder.Entity<V_ShipmentAvailableToSellAll>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<V_ShipmentItemExtended>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<V_ShipmentItemExtended>()
                .Property(e => e.ShipperPrice)
                .HasPrecision(19, 4);
        }
    }
}
