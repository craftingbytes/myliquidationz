namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BulkShipmentItem")]
    public partial class BulkShipmentItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BulkShipmentItem()
        {
            SalesOrderBulkItems = new HashSet<SalesOrderBulkItem>();
        }

        public int Id { get; set; }

        public int BulkShipmentId { get; set; }

        public int Store { get; set; }

        [Required]
        [StringLength(50)]
        public string ItemId { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public int Qty { get; set; }

        public int ReceivedCondition { get; set; }

        public bool? CoveredDevice { get; set; }

        [StringLength(200)]
        public string ReceivedCondition2 { get; set; }

        [StringLength(50)]
        public string Disposition { get; set; }

        public int Pulled { get; set; }

        public int Sold { get; set; }

        public virtual BulkShipment BulkShipment { get; set; }

        public virtual ConditionType ConditionType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderBulkItem> SalesOrderBulkItems { get; set; }
    }
}
