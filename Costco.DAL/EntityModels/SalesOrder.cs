namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SalesOrder")]
    public partial class SalesOrder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SalesOrder()
        {
            Bids = new HashSet<Bid>();
            SalesOrderBulkItems = new HashSet<SalesOrderBulkItem>();
            SalesOrderItems = new HashSet<SalesOrderItem>();
            SalesOrderSSOItems = new HashSet<SalesOrderSSOItem>();
        }

        public int Id { get; set; }

        public int AffiliateId { get; set; }

        public DateTime Created { get; set; }

        public int? SoldToAffiliateId { get; set; }

        [StringLength(100)]
        public string SoldToName { get; set; }

        [StringLength(200)]
        public string SoldToAddress { get; set; }

        [StringLength(200)]
        public string SoldToAddress2 { get; set; }

        [StringLength(20)]
        public string SoldToZip { get; set; }

        public int? SoldToStateId { get; set; }

        public int? SoldToCityId { get; set; }

        public int? TotalWeight { get; set; }

        [Column(TypeName = "money")]
        public decimal? SoldAmount { get; set; }

        public DateTime? SoldDate { get; set; }

        public bool Available { get; set; }

        [StringLength(500)]
        public string Comment { get; set; }

        public int? TotalPallets { get; set; }

        [Column(TypeName = "money")]
        public decimal? MinValue { get; set; }

        public bool Invoiced { get; set; }

        public DateTime? ShipDate { get; set; }

        public decimal? InitialPercentage { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bid> Bids { get; set; }

        public virtual City City { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderBulkItem> SalesOrderBulkItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderItem> SalesOrderItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderSSOItem> SalesOrderSSOItems { get; set; }
    }
}
