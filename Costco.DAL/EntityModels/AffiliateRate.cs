namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateRate")]
    public partial class AffiliateRate
    {
        public int Id { get; set; }

        public int AffiliateId { get; set; }

        public int ServiceTypeId { get; set; }

        [Column(TypeName = "money")]
        public decimal Rate { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual ServiceType ServiceType { get; set; }
    }
}
