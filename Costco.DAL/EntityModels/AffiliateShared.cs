namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateShared")]
    public partial class AffiliateShared
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int ParentAffiliateId { get; set; }

        public int SharedAffiliateId { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual Affiliate Affiliate1 { get; set; }
    }
}
