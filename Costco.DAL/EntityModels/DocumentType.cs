namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DocumentType")]
    public partial class DocumentType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DocumentType()
        {
            Documents = new HashSet<Document>();
        }

        public int Id { get; set; }

        public int? SiteImageID { get; set; }

        [StringLength(10)]
        public string FileExtension { get; set; }

        [StringLength(100)]
        public string DocumentTypeName { get; set; }

        [StringLength(100)]
        public string ContentType { get; set; }

        public bool IsUploadable { get; set; }

        public bool IsImage { get; set; }

        public bool EscapeBeforeDisplay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> Documents { get; set; }

        public virtual DocumentType DocumentType1 { get; set; }

        public virtual DocumentType DocumentType2 { get; set; }

        public virtual SiteImage SiteImage { get; set; }
    }
}
