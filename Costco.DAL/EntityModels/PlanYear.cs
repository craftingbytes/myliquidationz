namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PlanYear")]
    public partial class PlanYear
    {
        public int Id { get; set; }

        public int Year { get; set; }
    }
}
