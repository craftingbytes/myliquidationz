namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_SalvagSalesOrderBought
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AffiliateId { get; set; }

        public int? SoldToAffiliateId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime DateCreated { get; set; }

        public DateTime? DateSold { get; set; }

        [Column(TypeName = "money")]
        public decimal? SoldAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal? RetailAmount { get; set; }

        [StringLength(100)]
        public string SoldToName { get; set; }

        [StringLength(100)]
        public string SoldToAddress { get; set; }

        public int? SoldToStateId { get; set; }

        public int? SoldToCityId { get; set; }

        [StringLength(20)]
        public string SoldToZip { get; set; }

        [StringLength(50)]
        public string SoldBy { get; set; }

        [StringLength(2000)]
        public string Notes { get; set; }

        [StringLength(50)]
        public string ReceivedBy { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public int? BuyerAffiliateId { get; set; }

        [StringLength(200)]
        public string SellerName { get; set; }
    }
}
