namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Affiliate")]
    public partial class Affiliate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Affiliate()
        {
            AffiliateCertifications = new HashSet<AffiliateCertification>();
            AffiliateContacts = new HashSet<AffiliateContact>();
            AffiliateRates = new HashSet<AffiliateRate>();
            AffiliateShareds = new HashSet<AffiliateShared>();
            AffiliateShareds1 = new HashSet<AffiliateShared>();
            Bids = new HashSet<Bid>();
            BulkShipments = new HashSet<BulkShipment>();
            Invoices = new HashSet<Invoice>();
            Roles = new HashSet<Role>();
            SalesOrders = new HashSet<SalesOrder>();
            SalvageSalesOrders = new HashSet<SalvageSalesOrder>();
            SalvageSalesOrders1 = new HashSet<SalvageSalesOrder>();
            Shipments = new HashSet<Shipment>();
            Shipments1 = new HashSet<Shipment>();
        }

        public int Id { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string WebSite { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? Latitude { get; set; }

        public bool? Active { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        [StringLength(11)]
        public string Zip { get; set; }

        public int? AffiliateTypeId { get; set; }

        public int? PaymentTermId { get; set; }

        [StringLength(2048)]
        public string Notes { get; set; }

        [StringLength(2048)]
        public string Directions { get; set; }

        [StringLength(2048)]
        public string BusinessHours { get; set; }

        public int? ParentId { get; set; }

        public int? DefaultRecycler { get; set; }

        [StringLength(50)]
        public string WarehouseNumber { get; set; }

        public int? DefaultCarrier { get; set; }

        public virtual AffiliateType AffiliateType { get; set; }

        public virtual City City { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateCertification> AffiliateCertifications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateContact> AffiliateContacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateRate> AffiliateRates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateShared> AffiliateShareds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AffiliateShared> AffiliateShareds1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bid> Bids { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BulkShipment> BulkShipments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Role> Roles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrder> SalesOrders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalvageSalesOrder> SalvageSalesOrders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalvageSalesOrder> SalvageSalesOrders1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shipment> Shipments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shipment> Shipments1 { get; set; }
    }
}
