namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SalesOrderBulkItem")]
    public partial class SalesOrderBulkItem
    {
        public int Id { get; set; }

        public int SalesOrderId { get; set; }

        public int BulkShipmentId { get; set; }

        public int BulkShipmentItemId { get; set; }

        public int Quantity { get; set; }

        [StringLength(200)]
        public string Comment { get; set; }

        [Column(TypeName = "money")]
        public decimal? Price { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [Column(TypeName = "money")]
        public decimal? SellingPrice { get; set; }

        public virtual BulkShipment BulkShipment { get; set; }

        public virtual BulkShipmentItem BulkShipmentItem { get; set; }

        public virtual SalesOrder SalesOrder { get; set; }
    }
}
