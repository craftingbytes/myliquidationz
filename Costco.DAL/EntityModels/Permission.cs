namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Permission
    {
        public int Id { get; set; }

        public int? RoleId { get; set; }

        public int? AreaId { get; set; }

        public bool? Read { get; set; }

        public bool? Add { get; set; }

        public bool? Update { get; set; }

        public bool? Delete { get; set; }

        public virtual Area Area { get; set; }

        public virtual Role Role { get; set; }
    }
}
