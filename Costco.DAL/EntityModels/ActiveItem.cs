namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActiveItem")]
    public partial class ActiveItem
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(250)]
        public string ItemId { get; set; }

        [StringLength(500)]
        public string Title { get; set; }

        public DateTime? ListingDate { get; set; }

        public DateTime? EndDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? ListingPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal? CurrentPrice { get; set; }

        public int? BidCount { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime LastUpdated { get; set; }

        public int? Quantity { get; set; }

        public int? QuantityAvaialbe { get; set; }
    }
}
