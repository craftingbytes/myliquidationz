namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductDescription")]
    public partial class ProductDescription
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string ItemId { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public DateTime Timestamp { get; set; }

        public int? AffiliateContactId { get; set; }

        public virtual AffiliateContact AffiliateContact { get; set; }
    }
}
