namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SalesOrderItem")]
    public partial class SalesOrderItem
    {
        public int Id { get; set; }

        public int SalesOrderId { get; set; }

        public int ShipmentId { get; set; }

        public int ShipmentItemId { get; set; }

        public int Quantity { get; set; }

        [StringLength(200)]
        public string Comment { get; set; }

        [Column(TypeName = "money")]
        public decimal? Price { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [Column(TypeName = "money")]
        public decimal? SellingPrice { get; set; }

        public virtual SalesOrder SalesOrder { get; set; }

        public virtual Shipment Shipment { get; set; }

        public virtual ShipmentItem ShipmentItem { get; set; }
    }
}
