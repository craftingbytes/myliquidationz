namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ShipmentItemExtended
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ShipmentId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string PalletUnitNumber { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string ItemId { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(100)]
        public string Description { get; set; }

        [StringLength(100)]
        public string Condition { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Quantity { get; set; }

        public int? ReceivedQuantity { get; set; }

        [StringLength(50)]
        public string ReceivedCondition { get; set; }

        public bool? CoveredDevice { get; set; }

        [StringLength(200)]
        public string ReceivedCondition2 { get; set; }

        [StringLength(50)]
        public string Disposition { get; set; }

        [Key]
        [Column(Order = 6)]
        public decimal Weight { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Pulled { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(100)]
        public string DefaultDescription { get; set; }

        [Column(TypeName = "money")]
        public decimal? Price { get; set; }

        public int? Store { get; set; }

        [Column(TypeName = "money")]
        public decimal? ShipperPrice { get; set; }
    }
}
