namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_InvoiceASRPayable
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int? ParentInvoiceId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InvoiceTypeId { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AffiliateId { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime InvoiceDate { get; set; }

        [Key]
        [Column(Order = 4, TypeName = "money")]
        public decimal Rate { get; set; }

        [StringLength(200)]
        public string Notes { get; set; }

        public int? TotalWeight { get; set; }

        [Column(TypeName = "money")]
        public decimal? SubTotal { get; set; }

        public int? AdjustmentCount { get; set; }

        [Column(TypeName = "money")]
        public decimal? Ajustments { get; set; }

        [Column(TypeName = "money")]
        public decimal? Total { get; set; }
    }
}
