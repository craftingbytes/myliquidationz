namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AffiliateCertification")]
    public partial class AffiliateCertification
    {
        public int Id { get; set; }

        public int AffiliateId { get; set; }

        [Required]
        [StringLength(250)]
        public string Certification { get; set; }

        public virtual Affiliate Affiliate { get; set; }
    }
}
