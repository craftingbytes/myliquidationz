namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserReportList")]
    public partial class UserReportList
    {
        public int Id { get; set; }

        public int AffiliateContactId { get; set; }

        public int ReportId { get; set; }

        public int GroupId { get; set; }

        public int SequenceId { get; set; }

        [StringLength(250)]
        public string Notes { get; set; }

        public virtual AffiliateContact AffiliateContact { get; set; }

        public virtual Area Area { get; set; }
    }
}
