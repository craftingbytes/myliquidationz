namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ItemProfitCost")]
    public partial class ItemProfitCost
    {
        public long Id { get; set; }

        [Required]
        [StringLength(50)]
        public string ShipmentType { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public int ASR { get; set; }

        [Required]
        [StringLength(200)]
        public string Region { get; set; }

        [Required]
        [StringLength(200)]
        public string District { get; set; }

        [Required]
        [StringLength(200)]
        public string Warehouse { get; set; }

        public int WarehouseInt { get; set; }

        [Required]
        [StringLength(200)]
        public string Department { get; set; }

        public int DeppartmentNumber { get; set; }

        [Required]
        [StringLength(200)]
        public string ItemId { get; set; }

        public int Salvage { get; set; }

        public int Recycle { get; set; }

        public int Sold { get; set; }

        public decimal Weight { get; set; }

        public decimal SoldWeight { get; set; }

        public int SoldQuantity { get; set; }

        [Column(TypeName = "money")]
        public decimal Sales { get; set; }
    }
}
