namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ShipmentInventory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ShipmentId { get; set; }

        public virtual Shipment Shipment { get; set; }

        [Key]
        [Column(Order = 1)]
        public int ShipmentItemId { get; set; }

        public virtual ShipmentItem ShipmentItem { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string ItemId { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(100)]
        public string Description { get; set; }

        public int? Quantity { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Allocated { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Pulled { get; set; }

        public int? Available { get; set; }
    }
}
