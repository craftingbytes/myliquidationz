namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shipment")]
    public partial class Shipment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Shipment()
        {
            SalesOrderItems = new HashSet<SalesOrderItem>();
            ShipmentItems = new HashSet<ShipmentItem>();
        }

        public int Id { get; set; }

        public DateTime? ShipmentDate { get; set; }

        [StringLength(1000)]
        public string ShippingComments { get; set; }

        public int AffiliateId { get; set; }

        [StringLength(200)]
        public string PickupAddress { get; set; }

        [StringLength(200)]
        public string PickupAddress2 { get; set; }

        public int? PickupStateId { get; set; }

        public int? PickupCityId { get; set; }

        [StringLength(20)]
        public string PickupZip { get; set; }

        [StringLength(20)]
        public string PickupPhone { get; set; }

        public int ShipToAffiliateId { get; set; }

        [StringLength(100)]
        public string ShipToName { get; set; }

        [StringLength(200)]
        public string ShipToAddress { get; set; }

        [StringLength(200)]
        public string ShipToAddress2 { get; set; }

        public int? ShipToStateId { get; set; }

        public int? ShipToCityId { get; set; }

        [StringLength(20)]
        public string ShipToZip { get; set; }

        [StringLength(20)]
        public string ShipToPhone { get; set; }

        [StringLength(100)]
        public string ShippedBy { get; set; }

        [StringLength(100)]
        public string ReceivedBy { get; set; }

        [StringLength(1000)]
        public string ReceivingComments { get; set; }

        public int? ShipmentTotalWeight { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public int? ReceivedTotalWeight { get; set; }

        public int? SalvageableWeight { get; set; }

        public bool ReadyToShip { get; set; }

        public int CarrierAffiliateId { get; set; }

        public bool Complete { get; set; }

        public int? Invoiceid { get; set; }

        public int? ItemsAvailable { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual Affiliate Affiliate1 { get; set; }

        public virtual City City { get; set; }

        public virtual City City1 { get; set; }

        public virtual Invoice Invoice { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderItem> SalesOrderItems { get; set; }

        public virtual State State { get; set; }

        public virtual State State1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipmentItem> ShipmentItems { get; set; }
    }
}
