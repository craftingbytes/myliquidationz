namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShipmentItem")]
    public partial class ShipmentItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ShipmentItem()
        {
            SalesOrderItems = new HashSet<SalesOrderItem>();
        }

        public int Id { get; set; }

        public int ShipmentId { get; set; }

        [Required]
        [StringLength(50)]
        public string PalletUnitNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string ItemId { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [StringLength(100)]
        public string Condition { get; set; }

        public int Quantity { get; set; }

        public int? ReceivedQuantity { get; set; }

        public int? ReceivedCondition { get; set; }

        public bool? CoveredDevice { get; set; }

        [StringLength(200)]
        public string ReceivedCondition2 { get; set; }

        [StringLength(50)]
        public string Disposition { get; set; }

        public decimal Weight { get; set; }

        public int Pulled { get; set; }

        public int Sold { get; set; }

        public int? Store { get; set; }

        [Column(TypeName = "money")]
        public decimal? ShipperPrice { get; set; }

        public virtual ConditionType ConditionType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderItem> SalesOrderItems { get; set; }

        public virtual Shipment Shipment { get; set; }
    }
}
