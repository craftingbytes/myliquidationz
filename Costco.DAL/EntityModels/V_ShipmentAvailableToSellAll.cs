namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class V_ShipmentAvailableToSellAll
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string Type { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public int? ShipToAffiliateId { get; set; }
    }
}
