namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SalesOrderSSOItem")]
    public partial class SalesOrderSSOItem
    {
        public int Id { get; set; }

        public int SalesOrderId { get; set; }

        public int SSOId { get; set; }

        public long SSOItemId { get; set; }

        public int Quantity { get; set; }

        [StringLength(200)]
        public string Comment { get; set; }

        [Column(TypeName = "money")]
        public decimal? Price { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [Column(TypeName = "money")]
        public decimal? SellingPrice { get; set; }

        public virtual SalesOrder SalesOrder { get; set; }

        public virtual SalvageSalesOrder SalvageSalesOrder { get; set; }

        public virtual SalvageSalesOrderItem SalvageSalesOrderItem { get; set; }
    }
}
