namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Invoice")]
    public partial class Invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Invoice()
        {
            Invoice1 = new HashSet<Invoice>();
            InvoiceAdjustments = new HashSet<InvoiceAdjustment>();
            Shipments = new HashSet<Shipment>();
        }

        public int Id { get; set; }

        public int InvoiceTypeId { get; set; }

        public int AffiliateId { get; set; }

        public DateTime InvoiceDate { get; set; }

        [Column(TypeName = "money")]
        public decimal Rate { get; set; }

        [StringLength(200)]
        public string Notes { get; set; }

        [Column(TypeName = "date")]
        public DateTime Updated { get; set; }

        public int? ParentInvoiceId { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoice1 { get; set; }

        public virtual Invoice Invoice2 { get; set; }

        public virtual InvoiceType InvoiceType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceAdjustment> InvoiceAdjustments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shipment> Shipments { get; set; }
    }
}
