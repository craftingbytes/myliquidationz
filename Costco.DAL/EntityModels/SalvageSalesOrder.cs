namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SalvageSalesOrder")]
    public partial class SalvageSalesOrder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SalvageSalesOrder()
        {
            SalesOrderSSOItems = new HashSet<SalesOrderSSOItem>();
            SalvageSalesOrderItems = new HashSet<SalvageSalesOrderItem>();
        }

        public int Id { get; set; }

        public int AffiliateId { get; set; }

        public int? SoldToAffiliateId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateSold { get; set; }

        [Column(TypeName = "money")]
        public decimal? SoldAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal? RetailAmount { get; set; }

        [StringLength(100)]
        public string SoldToName { get; set; }

        [StringLength(100)]
        public string SoldToAddress { get; set; }

        public int? SoldToStateId { get; set; }

        public int? SoldToCityId { get; set; }

        [StringLength(20)]
        public string SoldToZip { get; set; }

        [StringLength(50)]
        public string SoldBy { get; set; }

        [StringLength(2000)]
        public string Notes { get; set; }

        [StringLength(50)]
        public string ReceivedBy { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public int? ItemsAvailable { get; set; }

        public virtual Affiliate Affiliate { get; set; }

        public virtual Affiliate Affiliate1 { get; set; }

        public virtual City City { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesOrderSSOItem> SalesOrderSSOItems { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalvageSalesOrderItem> SalvageSalesOrderItems { get; set; }
    }
}
