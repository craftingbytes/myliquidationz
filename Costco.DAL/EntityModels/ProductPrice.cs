namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductPrice")]
    public partial class ProductPrice
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string ItemId { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        public DateTime TimeStamp { get; set; }

        public int? AffiliateContactId { get; set; }

        public virtual AffiliateContact AffiliateContact { get; set; }
    }
}
