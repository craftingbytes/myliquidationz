namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class v_MaxProductPrice
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string ItemId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string Description { get; set; }

        [StringLength(100)]
        public string Description2 { get; set; }

        public decimal? Weight { get; set; }

        public bool? CoveredDevice { get; set; }

        [Column(TypeName = "money")]
        public decimal? Price { get; set; }

        public DateTime? Timestamp { get; set; }

        public int? Count { get; set; }

        public int? ProductTypeId { get; set; }

        [StringLength(75)]
        public string ProductType { get; set; }

        [StringLength(50)]
        public string DefaultDisposition { get; set; }
    }
}
