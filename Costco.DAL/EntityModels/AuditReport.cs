namespace Costco.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AuditReport
    {
        public int Id { get; set; }

        public int? StateId { get; set; }

        [StringLength(100)]
        public string FacilityName { get; set; }

        public DateTime? AuditDate { get; set; }

        public int? PlanYear { get; set; }

        public virtual State State { get; set; }
    }
}
