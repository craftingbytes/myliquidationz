﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Costco.BLL.ViewModels;
using Costco.BLL.Interfaces;
using Costco.DAL.EntityModels;
using Costco.DAL;
using Costco.DAL.Interfaces;
using System.Data;

using Costco.Common;


namespace Costco.BLL.BusinessObjects
{
    public class AffiliateBO
    {
        public CostcoEntities entities = null;
        Logger logger = null;
        CostcoService<Affiliate> affiliateSvc = null;

        public AffiliateBO()
        {
            logger = new Logger(this.GetType());
            entities = new CostcoEntities();
            affiliateSvc = new CostcoService<Affiliate>();
        }

        public List<Affiliate> GetAffiliatesPaged(int pageIndex, int pageSize, string searchString, int entity, out int totalRecords)
        {
            List<Affiliate> affiliateList = null;

            try
            {
                var _affList = from a in entities.Affiliates
                                where (a.Name.Contains(searchString) || string.IsNullOrEmpty(searchString))
                                    && (a.AffiliateTypeId == entity || entity <=0) //&& a.Longitude == null
                                orderby a.Name
                                select a;

                affiliateList = _affList.ToList();
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get entity list", ex);
            }
            if (affiliateList != null)
            {
                totalRecords = affiliateList.Count();
                return affiliateList.OrderBy(x => x.Name).Skip(pageIndex * pageSize).Take(pageSize).ToList();

            }
            else {
                totalRecords = 0;
                return affiliateList;
            }
        }

        public List<Message> Add(Affiliate _affiliateModel)
        {
            List<Message> messages = new List<Message>();
            try
            {
                State state = new CostcoService<State>().GetSingle(t => t.Id == _affiliateModel.StateId);
                _affiliateModel.State = state;

                City city = new CostcoService<City>().GetSingle(t => t.Id == _affiliateModel.CityId);
                _affiliateModel.City = city;

                AffiliateType affiliateType = new CostcoService<AffiliateType>().GetSingle(t => t.Id == _affiliateModel.AffiliateTypeId);
                _affiliateModel.AffiliateType = affiliateType;

                if (_affiliateModel.AffiliateTypeId != 4)
                {
                    _affiliateModel.ParentId = null;
                }

                if (_affiliateModel.AffiliateTypeId != 4 && _affiliateModel.AffiliateTypeId != 3)
                {
                    _affiliateModel.DefaultRecycler = null;
                    _affiliateModel.DefaultCarrier = null;
                }

                new CostcoService<Affiliate>().Add(_affiliateModel);

                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                logger.Error("Failed to add an entity: " + _affiliateModel.Name + "", ex);
            }
            return messages;
        }

        public List<Message> Update(Affiliate _affiliateModel)
        {
            List<Message> messages = new List<Message>();
            try
            {
                State state = new CostcoService<State>().GetSingle(t => t.Id == _affiliateModel.StateId);
                _affiliateModel.State = state;

                City city = new CostcoService<City>().GetSingle(t => t.Id == _affiliateModel.CityId);
                _affiliateModel.City = city;

                AffiliateType affiliateType = new CostcoService<AffiliateType>().GetSingle(t => t.Id == _affiliateModel.AffiliateTypeId);
                _affiliateModel.AffiliateType = affiliateType;
                affiliateSvc.CostcoEntities.SaveChanges();

                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                logger.Error("Failed to update entity: " + _affiliateModel.Name + "", ex);
            }
            return messages;
        }

        public Affiliate GetAffiliate(int id)
        {
            Affiliate _aff = null;
            try
            {
                _aff = affiliateSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _aff;
        }

        public bool DeleteDocument(int id)
        {
            CostcoService<Document> mitsDocumentSvc = new CostcoService<Document>();
            CostcoService<DocumentRelation> mitsDocumentRelationSvc = new CostcoService<DocumentRelation>();

            foreach (DocumentRelation _documentRelation in mitsDocumentRelationSvc.GetAll().Where(X => X.DocumentID == id))
            {
                mitsDocumentRelationSvc.Delete(_documentRelation);
            }

            Document _document = mitsDocumentSvc.GetSingle(X => X.Id == id);
            mitsDocumentSvc.Delete(_document);
            return true;
        }

    }
}
