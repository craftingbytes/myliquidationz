﻿using System;
using System.Collections.Generic;
using System.Linq;
using Costco.DAL.EntityModels;
using System.Data;
using Costco.Common;
using System.Data.Entity.Core.Objects;

namespace Costco.BLL.BusinessObjects
{
    public class ShipmentBO
    {
        CostcoEntities entities = null;
        Logger logger = null;
        //CostcoService<AffiliateContact> shipmentSvc = null;
        CostcoService<Shipment> shipmentSvc = null;
        CostcoService<ShipmentItem> shipmentItemSvc = null;
        CostcoService<Product> productService = null;
        //CostcoService<ProductDescription> descriptionService = null;
        private ISessionCookieValues _sessionValues = null;

        public ShipmentBO(ISessionCookieValues sessionValues) 
        {
            logger = new Logger(this.GetType());
            entities = new CostcoEntities();
            shipmentSvc = new CostcoService<Shipment>();
            shipmentItemSvc = new CostcoService<ShipmentItem>();
            productService = new CostcoService<Product>();
            //descriptionService = new CostcoService<ProductDescription>();
            _sessionValues = sessionValues;
        }

        public Affiliate GetAffiliate(int id)
        {
            Affiliate _aff = null;
            try
            {
                _aff = entities.Affiliates.Where(d => d.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _aff;
        }

        public bool Save(Shipment shipment, List<ShipmentItem> shipmentItems)
        {
            bool isSaved = true;
            try
            {
                entities.Shipments.Add(shipment);
                foreach (var item in shipmentItems)
                {
                    item.Shipment = shipment;
                    entities.ShipmentItems.Add(item);
                    
                }

                entities.SaveChanges();
            }
            catch (Exception)
            {
                isSaved = false;
            }
            return isSaved;

        }

        public Int32 Save(Shipment shipment)
        {
            Int32 id = 0;
            try
            {
                entities.Shipments.Add(shipment);
                entities.SaveChanges();
                id = shipment.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save Shipment: ", ex);
            }
            return id;

        }

        public bool Update(Shipment shipment)
        {
            bool isSaved = true;
            try
            {
                shipmentSvc.Save(shipment);
            }
            catch (Exception)
            {
                isSaved = false;
            }
            return isSaved;
        }

        public bool UpdateItem(ShipmentItem shipmentItem)
        {
            bool isSaved = true;
            try
            {
                shipmentItemSvc.Save(shipmentItem);
                AuditEvent auditEvent = new AuditEvent();
                auditEvent.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                auditEvent.Entity = "ShipmentItem";
                auditEvent.AuditActionId = Convert.ToInt32(Costco.Common.Constants.ActionType.Update);
                auditEvent.EntityId = shipmentItem.Id;
                auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
                auditEvent.TimeStamp = DateTime.Now;
                //auditEvent.Note = SessionParameters.Browser;
                AddAuditEvent(auditEvent);
                //Product product = new Product();
                //product.ItemId = shipmentItem.ItemId;
                //product.Description = shipmentItem.Description;
                ////product.CoveredDevice = shipmentItem.CoveredDevice;
                //UpdateProduct(product);
            }
            catch (Exception)
            {
                isSaved = false;
            }
            return isSaved;
        }

        public bool UpdateShipmentItems(Shipment shipment, List<ShipmentItem> shipmentItems)
        {
            bool isSaved = true;
            try
            {

               


                foreach (var updatedItem in shipmentItems)
                {  
                    ShipmentItem _shipmentItem = shipment.ShipmentItems.Where(it => it.Id == updatedItem.Id).FirstOrDefault();
                    if (_shipmentItem == null || _shipmentItem.Id == 0)
                    {
                        updatedItem.Shipment = shipment;
                        shipmentItemSvc.Add(updatedItem);
                        //entities.ShipmentItems.Add(updatedItem);
                    }
                    else
                    {
                        _shipmentItem.PalletUnitNumber = updatedItem.PalletUnitNumber;
                        _shipmentItem.ItemId = updatedItem.ItemId;
                        _shipmentItem.Description = updatedItem.Description;
                        _shipmentItem.Quantity = updatedItem.Quantity;
                        _shipmentItem.Condition = updatedItem.Condition;
                        _shipmentItem.CoveredDevice = updatedItem.CoveredDevice;
                        _shipmentItem.Disposition = updatedItem.Disposition;
                        shipmentItemSvc.Save(_shipmentItem);
                    }
                }
                //var _shipmentItems = from _si in shipment.ShipmentItems
                //                     where !(from si in shipmentItems
                //                             select si.Id).Contains(_si.Id)
                //                     select _si.Id;

                //foreach (int _shipmentId in _shipmentItems)
                foreach (int _shipmentId in shipment.ShipmentItems.Where(it => !shipmentItems.Select(i => i.Id).Contains(it.Id)).Select(it => it.Id).ToList())
                {
                    new CostcoService<ShipmentItem>().Delete(new CostcoService<ShipmentItem>().GetSingle(i => i.Id == _shipmentId));
                }

                entities.SaveChanges();
            }
            catch (Exception)
            {
                isSaved = false;
            }
            return isSaved;

        }

        public IQueryable<V_ShipmentItemExtended> GetShipmentItems(int pageIndex, int pageSize, int shipmentId,  out int totalRecords)
        {

            IQueryable<V_ShipmentItemExtended> shipmentItemList = null;
            shipmentItemList = entities.V_ShipmentItemExtended.Where(x => x.ShipmentId == shipmentId);
            totalRecords = shipmentItemList.Count<V_ShipmentItemExtended>();
            return shipmentItemList.OrderBy(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public IQueryable<Shipment> GetPaged(int pageIndex, int pageSize, string warehouses, string beginDate, string endDate, int affiliateId, string beginId, string endId, out int totalRecords)
 
        {

            //int numricValue;

            //if (string.IsNullOrEmpty(warehouses) || warehouses == "null")
            //{
            //    warehouses = string.Empty;
            //    IList<Affiliate> warehousesList;
            //    IList<Affiliate> unSortedList = entities.Affiliates.Where(x => x.AffiliateTypeId == 4).ToList(); //mitsDataContext.States.ToList();
            //    IEnumerable<Affiliate> sortedEnum = unSortedList.OrderBy(f => f.Id);
            //    try
            //    {
            //        warehousesList = sortedEnum.ToList();
            //    }
            //    catch (Exception)
            //    {
            //        warehousesList = unSortedList;
            //    }
            //    for (int i = 0; i < warehousesList.Count; i++)
            //    {
            //        if (i == warehousesList.Count - 1)
            //            warehouses += warehousesList[i].Name.ToString();
            //        else
            //            warehouses += warehousesList[i].Name.ToString() + ",";
            //    }

            //}

            //DateTime bDate = Convert.ToDateTime(beginDate);
            //DateTime eDate = Convert.ToDateTime(endDate);
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;

            int bId;
            int eId;

            if (!int.TryParse(beginId, out bId))
                bId = 0;
            if (!int.TryParse(endId, out eId))
                eId = int.MaxValue;
            IQueryable<Shipment> shipmentList = null;
            int typeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            if (typeid == 4) //Warehouse
            {
                shipmentList = entities.Shipments.Include("Affiliate").Where(x => x.AffiliateId == affiliateId
                                                                            && x.ShipmentDate >= bDate
                                                                            && EntityFunctions.TruncateTime(x.ShipmentDate) <= eDate
                                                                            && x.Id >= bId
                                                                            && x.Id <= eId
                                                                            );
            }
            //else if (typeid == 2) //Recycler
            //{
            //    shipmentList = entities.Shipments.Include("Affiliate").Where(x => (x.ShipToAffiliateId == affiliateId) 
            //            && ((!string.IsNullOrEmpty(beginDate) && !string.IsNullOrEmpty(endDate)) ? (x.ShipmentDate >= bDate && x.ShipmentDate <= eDate) : true)
            //            && (!string.IsNullOrEmpty(x.Affiliate.Name) ? warehouses.Contains(x.Affiliate.Name) : true)
            //            );
            //}
            else if (typeid == 1 || typeid == 2)
            {
                shipmentList = entities.Shipments.Include("Affiliate").Where(x => x.ShipmentDate >= bDate
                                                                            && EntityFunctions.TruncateTime(x.ShipmentDate) <= eDate
                                                                            && x.Id >= bId
                                                                            && x.Id <= eId
                                                                            && (!string.IsNullOrEmpty(warehouses) ? x.Affiliate.WarehouseNumber == warehouses : true)
                                                                            );
            }
            else if (typeid == 3) 
            {
                shipmentList = entities.Shipments.Include("Affiliate").Where(x =>  (x.Affiliate.ParentId == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId) )
                                                                            && EntityFunctions.TruncateTime(x.ShipmentDate) <= eDate
                                                                            && x.Id >= bId
                                                                            && x.Id <= eId
                                                                            && (!string.IsNullOrEmpty(warehouses) ? x.Affiliate.WarehouseNumber == warehouses : true)
                                                                            );
            }

            else if (typeid == 5)
            {
                shipmentList = entities.Shipments.Include("Affiliate").Where(x => (x.ShipToAffiliateId == _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId))
                                                                            && EntityFunctions.TruncateTime(x.ShipmentDate) <= eDate
                                                                            && x.Id >= bId
                                                                            && x.Id <= eId
                                                                            && (!string.IsNullOrEmpty(warehouses) ? x.Affiliate.WarehouseNumber == warehouses : true)
                                                                            );
            }

            totalRecords = shipmentList.Count<Shipment>();
            return shipmentList.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public Shipment GetShipment(int id)
        {
            Shipment _shipment = null;
            try
            {
                _shipment = shipmentSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _shipment;
        }

        public ShipmentItem GetShipmentItem(int id)
        {
            ShipmentItem _shipmentItem = null;
            try
            {
                _shipmentItem = shipmentItemSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _shipmentItem;
        }

        public Int32 AddShipmentItem(ShipmentItem shipmentItem)
        {
            new CostcoService<ShipmentItem>().Add(shipmentItem);
            return shipmentItem.Id;
        }


        public bool UpdateShipmentItem(ShipmentItem shipmentItem)
        {
            new CostcoService<ShipmentItem>().Save(shipmentItem);
            return true;
        }
        public bool DeleteShipmentItem(Int32 id)
        {
            new CostcoService<ShipmentItem>().Delete(GetShipmentItem(id));
            return true;
        }

        public bool DeleteShipment(Shipment shipment)
        {
            new CostcoService<Shipment>().Delete(shipment);
            return true;
        }

        public int SaveAttachment(Document document)
        {
            try
            {
                DocumentType documentType = new DocumentType();
                String extension = System.IO.Path.GetExtension(document.UploadFileName);

                document.DocumentType = entities.DocumentTypes.FirstOrDefault<DocumentType>(i => i.FileExtension == extension);
                entities.Documents.Add(document);
                int affected = entities.SaveChanges();

                return document.Id;
            }
            catch (Exception)
            {

            }
            return -1;
        }

        public bool SaveAttachmentRelation(DocumentRelation docRelation)
        {
            try
            {
                docRelation.ForeignTableID = entities.ForeignTables.ToList()
                    .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "Shipment").Id;

                docRelation.Document = entities.Documents.ToList().FirstOrDefault<Document>(i => i.Id == docRelation.DocumentID);
                docRelation.ForeignTable = entities.ForeignTables.ToList()
                    .FirstOrDefault<ForeignTable>(i => i.Id == docRelation.ForeignTableID);


                entities.DocumentRelations.Add(docRelation);
                int affected = entities.SaveChanges();

                return affected > 0;
            }
            catch (Exception)
            {

            }
            return false;
        }

        public List<Document> GetDocsByPDataId(int pdId)
        {

            ForeignTable _forgeignTable = entities.ForeignTables.ToList()
                .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "Shipment");

            int processDataTableId = _forgeignTable != null ? _forgeignTable.Id : 0;

            List<DocumentRelation> _docRels = entities.DocumentRelations
                .Where(i => i.ForeignTableID == processDataTableId && i.ForeignID == pdId).ToList();


            List<Document> _docs = new List<Document>();
            foreach (DocumentRelation docrel in _docRels)
            {

                _docs.Add(entities.Documents.FirstOrDefault(i => i.Id == docrel.DocumentID));
            }


            return _docs;
        }

        public IList<ConditionType> GetConditionTypes()
        {
            IList<ConditionType> unSortedList = entities.ConditionTypes.ToList();
            try
            {
                IEnumerable<ConditionType> sortedEnum = unSortedList.OrderBy(f => f.Id);
                return sortedEnum.ToList();
            }
            catch (Exception)
            {
                return unSortedList;
            }
        }

        public bool AddProduct(Product product)
        {
            bool isSaved = true;
            try
            {
                var products = entities.Products.Where(x => x.ItemId == product.ItemId).FirstOrDefault();
                if (product.ItemId.ToString().ToUpper() != "UNKNOWN" && products == null)
                {
                    entities.Products.Add(product);
                    entities.SaveChanges();
                    ProductDescription pd = new ProductDescription();
                    pd.ItemId = product.ItemId;
                    pd.Description = product.Description;
                    pd.Timestamp = DateTime.Now;
                    pd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                    new CostcoService<ProductDescription>().Add(pd);
                }
                else if (product.ItemId.ToString().ToUpper() != "UNKNOWN" && product.ItemId.ToString().ToUpper() != "MISC")
                {
                    if (products.CoveredDevice != product.CoveredDevice)
                        products.CoveredDevice = product.CoveredDevice;
                    if (products.Weight == 0 || products.Weight == null)
                        products.Weight = product.Weight;
                    //if (products.Weight != product.Weight && product.Weight > 0)
                    //    products.Weight = product.Weight;
                    productService.Save(products);
                    entities.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Error adding description" + ex.Message.ToString());
            }

            return isSaved;
        }

        public bool UpdateProduct(Product product)
        {
            bool isSaved = true;
            try
            {
                var products = entities.Products.Where(x => x.ItemId == product.ItemId).FirstOrDefault();
                if ((product.ItemId.ToString().ToUpper() != "UNKNOWN" || product.ItemId.ToString().ToUpper() != "MISC") && products != null)
                {
                    products.CoveredDevice = product.CoveredDevice;
                    if (products.Weight == 0 || products.Weight == null)
                        products.Weight = product.Weight;
                    productService.Save(products);
                    entities.SaveChanges();
                }

            }
            catch
            {
                isSaved = false;
            }

            return isSaved;
        }


        public Product GetProduct(string itemId )
        {
            Product product = null;
            try
            {
                product = entities.Products.Where(x => x.ItemId == itemId).FirstOrDefault();
            }
            catch
            {
                return product;
            }

            return product;
        }

        public string UpdateCompleteById(Int32 id)
        {
            string msg = string.Empty;
            try
            {
                Shipment shipment = GetShipment(id);


                if (shipment.Complete == true)
                {


                    if (shipment.Invoiceid != null)
                    {
                        return "This Shipment is already assigned to an invoice and cannot be reset.";
                    }
                    else
                    {
                        shipment.Complete = false;
                        shipmentSvc.Save(shipment);
                        return "Success";
                    }

                }
                else
                {
                    if (shipment.ReceivedDate == null || shipment.ReceivedTotalWeight <= 0)
                        return "This shipment has not been received.";      
                    shipment.Complete = true;
                    shipmentSvc.Save(shipment);
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public Int32 AddAuditEvent(AuditEvent ae)
        {
            new CostcoService<AuditEvent>().Add(ae);
            return ae.Id;
        }

    }
}

