﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Costco.BLL.ViewModels;
using Costco.BLL.Interfaces;
using Costco.DAL.EntityModels;
using Costco.DAL;
using Costco.DAL.Interfaces;
using System.Data;
using Costco.Common;

namespace Costco.BLL.BusinessObjects
{
    public class BulkShipmentBO
    {
        CostcoEntities entities = null;
        Logger logger = null;
        //CostcoService<AffiliateContact> shipmentSvc = null;
        CostcoService<BulkShipment> bulkShipmentSvc = null;
        CostcoService<BulkShipmentItem> bulkShipmentItemSvc = null;
        CostcoService<Product> productService = null;
        private ISessionCookieValues _sessionValues = null;

        public BulkShipmentBO(ISessionCookieValues sessionValues) 
        {
            logger = new Logger(this.GetType());
            entities = new CostcoEntities();
            bulkShipmentSvc = new CostcoService<BulkShipment>();
            bulkShipmentItemSvc = new CostcoService<BulkShipmentItem>();
            productService = new CostcoService<Product>();
            _sessionValues = sessionValues;
        }

        public IQueryable<BulkShipment> GetPaged(int pageIndex, int pageSize, string beginDate, string endDate, int affiliateId, out int totalRecords)
        {

            //int numricValue;

            DateTime bDate = Convert.ToDateTime(beginDate);
            DateTime eDate = Convert.ToDateTime(endDate);

            IQueryable<BulkShipment> shipmentList = null;
            int typeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            if (typeid == 5) //Recycler
            {
                shipmentList = entities.BulkShipments.Include("Affiliate").Where(x => x.ShipToAffiliateId == affiliateId  
                                                            && ((!string.IsNullOrEmpty(beginDate) && !string.IsNullOrEmpty(endDate)) ? (x.ReceivedDate >= bDate && x.ReceivedDate <= eDate) : true)
                     );
            }
            else if (typeid == 1 || typeid == 2)
            {
                shipmentList = entities.BulkShipments.Include("Affiliate").Where(x => ((!string.IsNullOrEmpty(beginDate) 
                                                                                    && !string.IsNullOrEmpty(endDate)) ? (x.ReceivedDate >= bDate 
                                                                                    && x.ReceivedDate <= eDate) : true)
                        );
            }


            totalRecords = shipmentList.Count<BulkShipment>();
            return shipmentList.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public BulkShipment GetBulkShipment(int id)
        {
            BulkShipment _shipment = null;
            try
            {
                _shipment = bulkShipmentSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _shipment;
        }

        public IQueryable<BulkShipmentItem> GetBulkShipmentItems(int pageIndex, int pageSize, int shipmentId, out int totalRecords)
        {

            IQueryable<BulkShipmentItem> shipmentItemList = null;
            shipmentItemList = entities.BulkShipmentItems.Include("ConditionType").Where(x => x.BulkShipmentId == shipmentId);
            totalRecords = shipmentItemList.Count<BulkShipmentItem>();
            return shipmentItemList.OrderBy(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }


        public Affiliate GetAffiliate(int id)
        {
            Affiliate _aff = null;
            try
            {
                _aff = entities.Affiliates.Where(d => d.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _aff;
        }

        public bool Save(BulkShipment shipment, List<BulkShipmentItem> shipmentItems)
        {
            bool isSaved = true;
            try
            {
                entities.BulkShipments.Add(shipment);
                foreach (var item in shipmentItems)
                {
                    item.BulkShipment = shipment;
                    entities.BulkShipmentItems.Add(item);

                }

                entities.SaveChanges();
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to save BulkShipment: ", ex);
            }
            return isSaved;

        }

        public Int32 Save(BulkShipment shipment)
        {
            Int32 id = 0;
            try
            {
                entities.BulkShipments.Add(shipment);
                entities.SaveChanges();
                id = shipment.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save BulkShipment: ", ex);
            }
            return id;

        }

        public bool Update(BulkShipment shipment)
        {
            bool isSaved = true;
            try
            {
                bulkShipmentSvc.Save(shipment);
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update BulkShipment: ", ex);
            }
            return isSaved;
        }

        public BulkShipmentItem GetBulkShipmentItemByID(Int32 id)
        {
            return new CostcoService<BulkShipmentItem>().GetSingle(i => i.Id == id);
        }
        public Int32 AddBulkShipmentItem(BulkShipmentItem bulkShipmentItem)
        {
            new CostcoService<BulkShipmentItem>().Add(bulkShipmentItem);
            AuditEvent auditEvent = new AuditEvent();
            auditEvent.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
            auditEvent.Entity = "BulkShipmentItem";
            auditEvent.AuditActionId = Convert.ToInt32(Costco.Common.Constants.ActionType.Insert);
            auditEvent.EntityId = bulkShipmentItem.Id;
            auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
            auditEvent.TimeStamp = DateTime.Now;
            //auditEvent.Note = SessionParameters.Browser;
            AddAuditEvent(auditEvent);
            return bulkShipmentItem.Id;
        }
        public bool UpdateBulkShipmentItem(BulkShipmentItem bulkShipmentItem)
        {
            new CostcoService<BulkShipmentItem>().Save(bulkShipmentItem);
            AuditEvent auditEvent = new AuditEvent();
            auditEvent.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
            auditEvent.Entity = "BulkShipmentItem";
            auditEvent.AuditActionId = Convert.ToInt32(Costco.Common.Constants.ActionType.Update);
            auditEvent.EntityId = bulkShipmentItem.Id;
            auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
            auditEvent.TimeStamp = DateTime.Now;
            //auditEvent.Note = SessionParameters.Browser;
            AddAuditEvent(auditEvent);
            return true;
        }
        public bool DeleteBulkShipmentItem(Int32 id)
        {
            new CostcoService<BulkShipmentItem>().Delete(GetBulkShipmentItemByID(id));
            AuditEvent auditEvent = new AuditEvent();
            auditEvent.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
            auditEvent.Entity = "BulkShipmentItem";
            auditEvent.AuditActionId = Convert.ToInt32(Costco.Common.Constants.ActionType.Delete);
            auditEvent.EntityId = id;
            auditEvent.IpAddress = _sessionValues.GetSessionValue(Constants.SessionParameters.IpAddress);
            auditEvent.TimeStamp = DateTime.Now;
            //auditEvent.Note = SessionParameters.Browser;
            AddAuditEvent(auditEvent);
            return true;
        }


        public bool UpdateBulkShipmentItems(BulkShipment shipment, List<BulkShipmentItem> shipmentItems)
        {
            bool isSaved = true;
            try
            {
                foreach (var updatedItem in shipmentItems)
                {
                    BulkShipmentItem _shipmentItem = shipment.BulkShipmentItems.Where(it => it.Id == updatedItem.Id).FirstOrDefault();
                    if (_shipmentItem == null || _shipmentItem.Id == 0)
                    {
                        updatedItem.BulkShipment = shipment;
                        bulkShipmentItemSvc.Add(updatedItem);
                        //entities.ShipmentItems.Add(updatedItem);
                    }
                    else
                    {
                        _shipmentItem.Store = updatedItem.Store;
                        _shipmentItem.ItemId = updatedItem.ItemId;
                        _shipmentItem.Description = updatedItem.Description;
                        _shipmentItem.Qty = updatedItem.Qty;
                        _shipmentItem.ReceivedCondition = updatedItem.ReceivedCondition;
                        _shipmentItem.CoveredDevice = updatedItem.CoveredDevice;
                        _shipmentItem.ReceivedCondition2 = updatedItem.ReceivedCondition2;
                        _shipmentItem.Disposition = updatedItem.Disposition;
                        bulkShipmentItemSvc.Save(_shipmentItem);
                    }
                }
                //var _shipmentItems = from _si in shipment.ShipmentItems
                //                     where !(from si in shipmentItems
                //                             select si.Id).Contains(_si.Id)
                //                     select _si.Id;

                //foreach (int _shipmentId in _shipmentItems)
                foreach (int _shipmentId in shipment.BulkShipmentItems.Where(it => !shipmentItems.Select(i => i.Id).Contains(it.Id)).Select(it => it.Id).ToList())
                {
                    new CostcoService<BulkShipmentItem>().Delete(new CostcoService<BulkShipmentItem>().GetSingle(i => i.Id == _shipmentId));
                }

                entities.SaveChanges();
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update BulkShipmentItem: ", ex);
            }
            return isSaved;

        }

        public ConditionType GetCondtionTypeByCondition(string condition)
        {
            ConditionType conditionType = null;
            try
            {
                conditionType = entities.ConditionTypes.Where(x => x.Condition == condition).FirstOrDefault();
            }
            catch
            {
                return conditionType;
            }

            return conditionType;
        }

        public List<Document> GetDocsByPDataId(int pdId)
        {

            ForeignTable _forgeignTable = entities.ForeignTables.ToList()
                .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "BulkShipment");

            int processDataTableId = _forgeignTable != null ? _forgeignTable.Id : 0;

            List<DocumentRelation> _docRels = entities.DocumentRelations
                .Where(i => i.ForeignTableID == processDataTableId && i.ForeignID == pdId).ToList();


            List<Document> _docs = new List<Document>();
            foreach (DocumentRelation docrel in _docRels)
            {

                _docs.Add(entities.Documents.FirstOrDefault(i => i.Id == docrel.DocumentID));
            }


            return _docs;
        }

        public int SaveAttachment(Document document)
        {
            try
            {
                DocumentType documentType = new DocumentType();
                String extension = System.IO.Path.GetExtension(document.UploadFileName);

                document.DocumentType = entities.DocumentTypes.FirstOrDefault<DocumentType>(i => i.FileExtension == extension);
                entities.Documents.Add(document);
                int affected = entities.SaveChanges();

                return document.Id;
            }
            catch (Exception)
            {

            }
            return -1;
        }

        public bool SaveAttachmentRelation(DocumentRelation docRelation)
        {
            try
            {
                docRelation.ForeignTableID = entities.ForeignTables.ToList()
                    .FirstOrDefault<ForeignTable>(i => i.ForeignTableName == "BulkShipment").Id;

                docRelation.Document = entities.Documents.ToList().FirstOrDefault<Document>(i => i.Id == docRelation.DocumentID);
                docRelation.ForeignTable = entities.ForeignTables.ToList()
                    .FirstOrDefault<ForeignTable>(i => i.Id == docRelation.ForeignTableID);


                entities.DocumentRelations.Add(docRelation);
                int affected = entities.SaveChanges();

                return affected > 0;
            }
            catch (Exception)
            {

            }
            return false;
        }

        public bool AddProduct(Product product)
        {
            bool isSaved = true;
            try
            {
                var products = entities.Products.Where(x => x.ItemId == product.ItemId).FirstOrDefault();
                if (product.ItemId.ToString().ToUpper() != "UNKNOWN" && products == null)
                {
                    entities.Products.Add(product);
                    entities.SaveChanges();
                    ProductDescription pd = new ProductDescription();
                    pd.ItemId = product.ItemId;
                    pd.Description = product.Description;
                    pd.Timestamp = DateTime.Now;
                    pd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                    new CostcoService<ProductDescription>().Add(pd);
                }
                //else if (product.ItemId.ToString().ToUpper() != "UNKNOWN" && products.CoveredDevice != product.CoveredDevice)
                //{
                //    products.CoveredDevice = product.CoveredDevice;
                //    productService.Save(products);
                //    entities.SaveChanges();
                //}

            }
            catch
            {
                isSaved = false;
            }

            return isSaved;
        }

        public Int32 AddAuditEvent(AuditEvent ae)
        {
            new CostcoService<AuditEvent>().Add(ae);
            return ae.Id;
        }
    }
}
