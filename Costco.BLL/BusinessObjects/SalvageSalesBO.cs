﻿using System;
using System.Collections.Generic;
using System.Linq;
using Costco.DAL.EntityModels;
using System.Data;
using Costco.Common;
using System.Data.Entity.Core.Objects;

namespace Costco.BLL.BusinessObjects
{
    public class SalvageSalesBO
    {
        CostcoEntities entities = null;
        Logger logger = null;
        //CostcoService<AffiliateContact> shipmentSvc = null;
        CostcoService<SalvageSalesOrder> salvageSalesOrderSvc = null;
        CostcoService<SalvageSalesOrderItem> salvageItemSvc = null;

        public SalvageSalesBO() 
        {
            logger = new Logger(this.GetType());
            entities = new CostcoEntities();
            salvageSalesOrderSvc = new CostcoService<SalvageSalesOrder>();
            salvageItemSvc = new CostcoService<SalvageSalesOrderItem>();
        }

        public SalvageSalesOrder GetSalvageSalesOrder(int id)
        {
            SalvageSalesOrder _salesOrder = null;
            try
            {
                _salesOrder = salvageSalesOrderSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _salesOrder;
        }


        public Int32 SaveSalvageSalesOrder(SalvageSalesOrder salvageSalesOrder)
        {
            Int32 id = 0;
            try
            {
                entities.SalvageSalesOrders.Add(salvageSalesOrder);
                entities.SaveChanges();
                id = salvageSalesOrder.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save SalvageSalesOrder: ", ex);
            }
            return id;

        }

        public IQueryable<V_SalvagSalesOrderBought> GetPagedBuyer(int pageIndex, int pageSize, string beginDate, string endDate, int affiliateId, string beginId, string endId, out int totalRecords)
        {
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;
            //DateTime bDate = Convert.ToDateTime(beginDate);
            //DateTime eDate = Convert.ToDateTime(endDate);

            int bId;
            int eId;

            if (!int.TryParse(beginId, out bId))
                bId = 0;
            if (!int.TryParse(endId, out eId))
                eId = int.MaxValue;

            IQueryable<V_SalvagSalesOrderBought> salesOrderList = null;

            //IQueryable<AffiliateShared>



            salesOrderList = entities.V_SalvagSalesOrderBought.Where(x => (x.BuyerAffiliateId == affiliateId || x.SoldToAffiliateId == affiliateId)
                                                                        && x.DateCreated >= bDate
                                                                        && EntityFunctions.TruncateTime(x.DateCreated) <= eDate
                                                                        && x.Id >= bId
                                                                        && x.Id <= eId);


            totalRecords = salesOrderList.Count<V_SalvagSalesOrderBought>();
            return salesOrderList.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public IQueryable<SalvageSalesOrder> GetPaged(int pageIndex, int pageSize, string beginDate, string endDate, int affiliateId, string beginId, string endId, out int totalRecords)
        {
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;
            //DateTime bDate = Convert.ToDateTime(beginDate);
            //DateTime eDate = Convert.ToDateTime(endDate);

            int bId;
            int eId;

            if (!int.TryParse(beginId, out bId))
                bId = 0;
            if (!int.TryParse(endId, out eId))
                eId = int.MaxValue;

            IQueryable<SalvageSalesOrder> salesOrderList = null;

            salesOrderList = entities.SalvageSalesOrders.Include("Affiliate").Where(x => x.AffiliateId == affiliateId 
                                                                        && x.DateCreated >= bDate
                                                                        && EntityFunctions.TruncateTime(x.DateCreated) <= eDate
                                                                        && x.Id >= bId
                                                                        && x.Id <= eId);


            totalRecords = salesOrderList.Count<SalvageSalesOrder>();
            return salesOrderList.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public IQueryable<SalvageSalesOrderItem> GetSSItems(int pageIndex, int pageSize, int salvageSalesOrderId, out int totalRecords)
        {

            IQueryable<SalvageSalesOrderItem> salvageItemList = null;
            salvageItemList = entities.SalvageSalesOrderItems.Where(x => x.SalvageSalesOrderId == salvageSalesOrderId);
            totalRecords = salvageItemList.Count<SalvageSalesOrderItem>();
            return salvageItemList.OrderBy(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public SalvageSalesOrderItem GetSalvageSalesOrderItem(Int64 id)
        {
            SalvageSalesOrderItem _ssItem = null;
            try
            {
                _ssItem = salvageItemSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _ssItem;
        }

        public Int64 AddSalvageSalesOrderItem(SalvageSalesOrderItem ssItem)
        {
            new CostcoService<SalvageSalesOrderItem>().Add(ssItem);
            return ssItem.Id;
        }

        public bool DeleteSalvageSalesOrderItem(Int64 id)
        {
            try
            {
                new CostcoService<SalvageSalesOrderItem>().Delete(GetSalvageSalesOrderItem(id));
            }
            catch (Exception ex)
            {
                logger.Error("Failed to delete Item: " + id + "", ex);
            }
            return true;
        }

        public bool UpdateSalvageSalesOrderItem(SalvageSalesOrderItem shipmentItem)
        {
            new CostcoService<SalvageSalesOrderItem>().Save(shipmentItem);
            return true;
        }

        public bool UpdateSalvageSalesOrder(SalvageSalesOrder salesOrder)
        {
            new CostcoService<SalvageSalesOrder>().Save(salesOrder);
            return true;
        }

        


        public Int32 AddBuyer(Affiliate buyer)
        {
            Int32 id = 0;
            try
            {
                entities.Affiliates.Add(buyer);
                entities.SaveChanges();
                id = (int)buyer.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save Bid: ", ex);
            }
            return id;
        }

        public Int32 GetMissing(Int32 ssoId)
        {
            Int32 missing = 0;
            IList<SalvageSalesOrderItem> recs = salvageItemSvc.GetAll(x => x.SalvageSalesOrderId == ssoId && x.Missing > 0);
            foreach(SalvageSalesOrderItem item in recs){
                missing += (Int32)item.Missing;
            }

            return missing;

        }


    }
}
