﻿using System;
using System.Linq;
using Costco.DAL.EntityModels;
using System.Data;
using Costco.Common;
using System.Data.Entity.Core.Objects;

namespace Costco.BLL.BusinessObjects
{
    public class SalesOrderBO
    {
        CostcoEntities entities = null;
        Logger logger = null;
        //CostcoService<AffiliateContact> shipmentSvc = null;
        CostcoService<SalesOrder> salesOrderSvc = null;
        CostcoService<SalesOrderItem> salesOrderItemSvc = null;
        CostcoService<SalesOrderBulkItem> salesOrderBulkItemSvc = null;
        CostcoService<SalesOrderSSOItem> salesOrderSSOItemSvc = null;
        private ISessionCookieValues _sessionValues = null;

        public SalesOrderBO(ISessionCookieValues sessionValues) 
        {
            logger = new Logger(this.GetType());
            entities = new CostcoEntities();
            salesOrderSvc = new CostcoService<SalesOrder>();
            salesOrderItemSvc = new CostcoService<SalesOrderItem>();
            salesOrderBulkItemSvc = new CostcoService<SalesOrderBulkItem>();
            salesOrderSSOItemSvc = new CostcoService<SalesOrderSSOItem>();
            _sessionValues = sessionValues;
        }

        public SalesOrder GetSalesOrder(int id)
        {
            SalesOrder _salesOrder = null;
            try
            {
                _salesOrder = salesOrderSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _salesOrder;
        }

        public SalesOrderItem GetSalesOrderItem(int id)
        {
            SalesOrderItem _salesOrderItem = null;
            try
            {
                _salesOrderItem = salesOrderItemSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _salesOrderItem;
        }


        public SalesOrderBulkItem GetSalesOrderBulkItem(int id)
        {
            SalesOrderBulkItem _salesOrderBulkItem = null;
            try
            {
                _salesOrderBulkItem = salesOrderBulkItemSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _salesOrderBulkItem;
        }

        public SalesOrderSSOItem GetSalesOrderSSOItem(int id)
        {
            SalesOrderSSOItem _salesOrderSSOItem = null;
            try
            {
                _salesOrderSSOItem = salesOrderSSOItemSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _salesOrderSSOItem;
        }

        public Int32 Save(SalesOrder salesOrder)
        {
            Int32 id = 0;
            try
            {
                entities.SalesOrders.Add(salesOrder);
                entities.SaveChanges();
                id = salesOrder.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save SalesOrder: ", ex);
            }
            return id;

        }

        public bool Update(SalesOrder salesOrder)
        {
            bool isSaved = true;
            try
            {
                salesOrderSvc.Save(salesOrder);
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update SalesOrder: ", ex);
            }
            return isSaved;
        }

        public IQueryable<SalesOrder> GetPaged(int pageIndex, int pageSize, string beginDate, string endDate, int affiliateId, string beginId, string endId, bool incomplete, string auctionType, out int totalRecords)
        {
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;
            //DateTime bDate = Convert.ToDateTime(beginDate);
            //DateTime eDate = Convert.ToDateTime(endDate);

            int bId;
            int eId;

            if (!int.TryParse(beginId, out bId))
                bId = 0;
            if (!int.TryParse(endId, out eId))
                eId = int.MaxValue;

            IQueryable<SalesOrder> salesOrderList = null;

            int typeid = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            if (typeid == 1 || typeid == 7)
            {
                salesOrderList = entities.SalesOrders.Include("Affiliate").Where(x => x.Available == true 
                                                                            &&  x.Created >= bDate 
                                                                            && EntityFunctions.TruncateTime(x.Created) <= eDate 
                                                                            && x.Id >= bId 
                                                                            && x.Id <= eId
                                                                            && (auctionType == "AllOpen" ? x.SoldDate == null : auctionType == "AllClosed" ? x.SoldDate != null : true)
                        );
            }
            else if (typeid == 5 )
            {
                salesOrderList = entities.SalesOrders.Include("Affiliate").Where(x => x.AffiliateId == affiliateId
                                                                            && x.Created >= bDate
                                                                            && EntityFunctions.TruncateTime(x.Created) <= eDate
                                                                            && x.Id >= bId
                                                                            && x.Id <= eId
                                                                            && (incomplete == true ? x.Available == false: true )
                                                                            && (auctionType == "AllOpen" ? x.SoldDate == null : auctionType == "AllClosed" ? x.SoldDate != null : true)
                    );
            }

            totalRecords = salesOrderList.Count<SalesOrder>();
            return salesOrderList.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public IQueryable<SalesOrder> GetPagedEworld(int pageIndex, int pageSize, string beginDate, string endDate, int affiliateId, string beginId, string endId, string invoiceStatus, out int totalRecords)
        {
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;
            //DateTime bDate = Convert.ToDateTime(beginDate);
            //DateTime eDate = Convert.ToDateTime(endDate);

            int bId;
            int eId;

            if (!int.TryParse(beginId, out bId))
                bId = 0;
            if (!int.TryParse(endId, out eId))
                eId = int.MaxValue;

            IQueryable<SalesOrder> salesOrderList = null;


            salesOrderList = entities.SalesOrders.Include("Affiliate").Where(x => x.Available == true
                                                                        && x.Created >= bDate
                                                                        && EntityFunctions.TruncateTime(x.Created) <= eDate
                                                                        && x.Id >= bId
                                                                        && x.Id <= eId
                                                                        && x.SoldDate != null
                                                                        && x.Available == true
                                                                        && (invoiceStatus == "Invoiced" ? x.Invoiced == true : invoiceStatus == "NotInvoiced" ? x.Invoiced == false : true)
                    );

            totalRecords = salesOrderList.Count<SalesOrder>();
            return salesOrderList.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);

        }



        public IQueryable<V_MaxBid> GetBids(string beginDate, string endDate)
        {
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;
            //DateTime bDate = Convert.ToDateTime(beginDate);
            //DateTime eDate = Convert.ToDateTime(endDate);

            IQueryable<V_MaxBid> salesOrderList = null;

            salesOrderList = entities.V_MaxBid.Where(x => x.Created >= bDate && EntityFunctions.TruncateTime(x.Created) <= eDate);

            return salesOrderList;
        }

        public IQueryable<V_MaxAffiliateBid> GetBids(string beginDate, string endDate,int affiliateId, string AuctionType)
        {
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;
            //DateTime bDate = Convert.ToDateTime(beginDate);
            //DateTime eDate = Convert.ToDateTime(endDate);

            IQueryable<V_MaxAffiliateBid> salesOrderList = null;

            salesOrderList = entities.V_MaxAffiliateBid.Where(x => x.Buyer_Id == affiliateId
                                                                && x.Created >= bDate && EntityFunctions.TruncateTime(x.Created) <= eDate
                                                                && (AuctionType != null  ? x.Status == AuctionType : true ));

            return salesOrderList;
        }

        public IQueryable<V_MaxAffiliateBid> GetBids(int pageIndex, int pageSize, int salesOrderId, out int totalRecords)
        {
            IQueryable<V_MaxAffiliateBid> bidList = null;

            bidList = entities.V_MaxAffiliateBid.Where(x => x.SalesOrderId == salesOrderId);

            totalRecords = bidList.Count<V_MaxAffiliateBid>();
            return bidList.OrderByDescending(e => e.Amount).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public IQueryable<SalesOrderItem> GetSalesOrderItems(int pageIndex, int pageSize, int salesOrderid,  out int totalRecords)
        {
            IQueryable<SalesOrderItem> itemList = null;
            itemList = entities.SalesOrderItems.Where(x => x.SalesOrderId == salesOrderid);
            totalRecords = itemList.Count<SalesOrderItem>();
            return itemList.OrderBy(e => e.SalesOrderId).Skip(pageIndex * pageSize).Take(pageSize);
        }

        public IQueryable<V_ShipmentAvailableToSellAll> GetAvailabeShipmentsToSell(int pageIndex, int pageSize, string beginDate, string endDate, int affiliateId, string beginId, string endId, string shipmentType,  out int totalRecords )
        {
            

            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate =  DateTime.MaxValue;

            int bId;
            int eId;

            if (!int.TryParse(beginId,out bId))
                bId = 0;
            if (!int.TryParse(endId,out eId))
                eId = int.MaxValue;



            


            IQueryable<V_ShipmentAvailableToSellAll> shipmentList = null;
            shipmentList = entities.V_ShipmentAvailableToSellAll.Where(x => x.ShipToAffiliateId == affiliateId 
                                                                        && x.ReceivedDate >= bDate 
                                                                        && EntityFunctions.TruncateTime(x.ReceivedDate) <= eDate 
                                                                        && x.Id >= bId 
                                                                        && x.Id <= eId
                                                                        && (shipmentType == "All" || x.Type == shipmentType)
                            // && ((!string.IsNullOrEmpty(beginDate) && !string.IsNullOrEmpty(endDate)) ? (x.ReceivedDate >= bDate && x.ReceivedDate <= eDate) : true)
                            );
            totalRecords = shipmentList.Count<V_ShipmentAvailableToSellAll>();
            return shipmentList.OrderByDescending(e => e.Type).ThenByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);
        }

        public Int32 AddSalesOrderItem(SalesOrderItem salesOrderItem)
        {
            Int32 id = 0;
            try
            {
                entities.SalesOrderItems.Add(salesOrderItem);
                entities.SaveChanges();
                id = salesOrderItem.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save SalesOrderItem: ", ex);
            }
            return id;
        }

        public Int32 AddSalesOrderBulkItem(SalesOrderBulkItem salesOrderBulkItem)
        {
            Int32 id = 0;
            try
            {
                entities.SalesOrderBulkItems.Add(salesOrderBulkItem);
                entities.SaveChanges();
                id = salesOrderBulkItem.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save SalesOrderBulkItem: ", ex);
            }
            return id;
        }

        public Int32 AddSalesOrderSSOItems(SalesOrderSSOItem ssOrderItem)
        {
            Int32 id = 0;
            try
            {
                entities.SalesOrderSSOItems.Add(ssOrderItem);
                entities.SaveChanges();
                id = ssOrderItem.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save SalesOrderSSOItems: ", ex);
            }
            return id;
        }

        public Int32 AddBid(Bid bid)
        {
            Int32 id = 0;
            try
            {
                entities.Bids.Add(bid);
                entities.SaveChanges();
                id = (int)bid.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save Bid: ", ex);
            }
            return id;
        }

        public bool DeleteSalesOrderItem(Int32 salesOrderItemId)
        {
            bool deleted = false;
            try
            {
                SalesOrderItem salesOrderItem = salesOrderItemSvc.GetSingle(x => x.Id == salesOrderItemId);
                salesOrderItemSvc.Delete(salesOrderItem);
                deleted = true;
            }
            catch (Exception ex)
            {
                deleted = false;
                logger.Error("Failed to delete SalesOrderItem: ", ex);
            }
            return deleted;
        }

        public bool DeleteSalesOrderBulkItem(Int32 salesOrderBulkItemId)
        {
            bool deleted = false;
            try
            {
                SalesOrderBulkItem salesOrderItem = salesOrderBulkItemSvc.GetSingle(x => x.Id == salesOrderBulkItemId);
                salesOrderBulkItemSvc.Delete(salesOrderItem);
                deleted = true;
            }
            catch (Exception ex)
            {
                deleted = false;
                logger.Error("Failed to delete SalesOrderItem: ", ex);
            }
            return deleted;
        }

        public bool DeleteSalesOrderSSOItem(Int32 salesOrderSSOItemId)
        {
            bool deleted = false;
            try
            {
                SalesOrderSSOItem salesOrderItem = salesOrderSSOItemSvc.GetSingle(x => x.Id == salesOrderSSOItemId);
                salesOrderSSOItemSvc.Delete(salesOrderItem);
                deleted = true;
            }
            catch (Exception ex)
            {
                deleted = false;
                logger.Error("Failed to delete SalesOrderItem: ", ex);
            }
            return deleted;
        }

        public bool UpdateSalesOrderItem(SalesOrderItem salesOrderItem)
        {
            bool isSaved = true;
            try
            {
                salesOrderItemSvc.Save(salesOrderItem);
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update SalesOrderItem: ", ex);
            }
            return isSaved;
        }

        public bool UpdateSalesOrderBulkItem(SalesOrderBulkItem salesOrderBulkItem)
        {
            bool isSaved = true;
            try
            {
                salesOrderBulkItemSvc.Save(salesOrderBulkItem);
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update SalesOrderBulkItem: ", ex);
            }
            return isSaved;
        }

        public bool UpdateSalesOrderSSOItem(SalesOrderSSOItem salesOrderSSOItem)
        {
            bool isSaved = true;
            try
            {
                salesOrderSSOItemSvc.Save(salesOrderSSOItem);
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update SalesOrderSSOItem: ", ex);
            }
            return isSaved;
        }

        public IQueryable<V_SalesOrderItemAll> GetSalesOrderItemsAll(int pageIndex, int pageSize, int salesOrderid, out int totalRecords)
        {
            IQueryable<V_SalesOrderItemAll> itemList = null;
            itemList = entities.V_SalesOrderItemAll.Where(x => x.SalesOrderId == salesOrderid);
            totalRecords = itemList.Count<V_SalesOrderItemAll>();
            return itemList.OrderByDescending(e => e.Type).ThenBy(e=> e.Id).Skip(pageIndex * pageSize).Take(pageSize);
        }


        public Int32 AddBuyer(Affiliate buyer)
        {
            Int32 id = 0;
            try
            {
                entities.Affiliates.Add(buyer);
                entities.SaveChanges();
                id = (int)buyer.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save Bid: ", ex);
            }
            return id;
        }

        public decimal GetRetailValue(int salesOrderid)
        {
            decimal retailValue = 0;
            //IQueryable<V_SalesOrderItemAll> itemList = null;
            retailValue = entities.V_SalesOrderItemAll
                .Where(x => x.SalesOrderId == salesOrderid)
                .Sum(x => (decimal)x.Price * x.Quantity);
            return retailValue;
            
        }
        public decimal GetSellingPrice(int salesOrderid)
        {
            decimal sellingPrice = 0;
            //IQueryable<V_SalesOrderItemAll> itemList = null;
            sellingPrice = entities.V_SalesOrderItemAll
                .Where(x => x.SalesOrderId == salesOrderid)
                .Sum(x => (decimal)x.SellingPrice * x.Quantity);
            return sellingPrice;

        }

        public decimal? UpdateSoldAmount(int salesOrderId)
        {
            decimal? soldAmount = null;
            soldAmount = entities.p_UpdateSalesOrderSoldAmount(salesOrderId).First();
            return soldAmount;
        }
    }
}
