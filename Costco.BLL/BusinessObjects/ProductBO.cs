﻿using System;
using System.Collections.Generic;
using System.Linq;
using Costco.DAL.EntityModels;
using System.Data;
using Costco.Common;

namespace Costco.BLL.BusinessObjects
{
    public class ProductBO
    {
        CostcoEntities entities = null;
        Logger logger = null;

        CostcoService<Product> productSvc = null;
        CostcoService<ProductPrice> productPriceSvc = null;
        CostcoService<v_MaxProductPrice> maxProductPriceSvc = null;
        CostcoService<ProductDisposition> dispositionSvc = null;
        private ISessionCookieValues _sessionValues = null;

        public ProductBO(ISessionCookieValues sessionValues) 
        {
            logger = new Logger(this.GetType());
            entities = new CostcoEntities();
            productSvc = new CostcoService<Product>();
            productPriceSvc = new CostcoService<ProductPrice>();
            maxProductPriceSvc = new CostcoService<v_MaxProductPrice>();
            dispositionSvc = new CostcoService<ProductDisposition>();
            _sessionValues = sessionValues;
        }

        public IQueryable<v_MaxProductPrice> SearchProducts(int pageIndex, int pageSize, string searchstring, int productTypeId, bool priceNotSet, bool weightNotSet, out int totalRecords)
        {

            IQueryable<v_MaxProductPrice> productList = null;
            productList = entities.v_MaxProductPrice.Where(x =>  (!string.IsNullOrEmpty(searchstring) ?  x.ItemId.Contains(searchstring) || x.Description.Contains(searchstring): true)
                                                           && (productTypeId != 99 ? x.ProductTypeId == productTypeId || productTypeId == null: true)
                                                           && (priceNotSet == true ? x.Price == 0 : true)
                                                           && (weightNotSet == true ? x.Weight == 0 : true));
            totalRecords = productList.Count<v_MaxProductPrice>();
            return productList.OrderByDescending(e => e.Count).Skip(pageIndex * pageSize).Take(pageSize);
        }


        public v_MaxProductPrice GetProductPriceByItemId(string itemId)
        { 
            v_MaxProductPrice  pp = entities.v_MaxProductPrice.FirstOrDefault( x => x.ItemId == itemId);
            return pp;
        }

        public Int32 AddProductPrice(ProductPrice pp)
        {
            try
            {
                new CostcoService<ProductPrice>().Add(pp);
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                return 0;
            }
            return pp.Id;
        }


        public Int32 AddProductDescription(ProductDescription pd)
        {
            try
            {
                new CostcoService<ProductDescription>().Add(pd);
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                return 0;
            }
            return pd.Id;
        }

        public IList<ProductType> GetProductTypes()
        {
            IList<ProductType> unSortedList = entities.ProductTypes.ToList();
            try
            {
                IEnumerable<ProductType> sortedEnum = unSortedList.OrderBy(f => f.Name);
                return sortedEnum.ToList();
            }
            catch (Exception)
            {
                return unSortedList;
            }
        }

        public bool UpdateProduct(Product product)
        {
            bool isSaved = true;
            try
            {
                productSvc.Save(product);
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update product: ", ex);
            }
            return isSaved;
        }

        public bool UpdateProductDisposition(string itemId, string disposition)
        {
            bool isSaved = true;
            try
            {
                ProductDisposition pd = dispositionSvc.GetSingle(x => x.ItemId == itemId);
                if (pd == null && disposition != "Not Set")
                {
                    ProductDisposition newPd = new ProductDisposition();
                    newPd.ItemId = itemId;
                    newPd.Disposition = disposition;
                    newPd.TimeStamp = DateTime.Now;
                    newPd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                    dispositionSvc.Add(newPd);
                }
                else if (disposition == "Not Set")
                {
                    dispositionSvc.Delete(pd);
                }
                else
                {
                    pd.Disposition = disposition;
                    dispositionSvc.Save(pd);
                }


            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update product disposition: ", ex);
            }

            return isSaved;
        }

        public Product GetProduct(string itemId)
        {
            Product product = null;
            try
            {
                product = entities.Products.Where(x => x.ItemId == itemId).FirstOrDefault();
            }
            catch
            {
                return product;
            }

            return product;
        }

        public bool AddProduct(Product product)
        {
            bool isSaved = true;
            try
            {
                var products = entities.Products.Where(x => x.ItemId == product.ItemId).FirstOrDefault();
                if (product.ItemId.ToString().ToUpper() != "UNKNOWN" && products == null)
                {
                    entities.Products.Add(product);
                    entities.SaveChanges();
                    ProductDescription pd = new ProductDescription();
                    pd.ItemId = product.ItemId;
                    pd.Description = product.Description;
                    pd.Timestamp = DateTime.Now;
                    pd.AffiliateContactId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateContactId);
                    new CostcoService<ProductDescription>().Add(pd);
                }
                else if (product.ItemId.ToString().ToUpper() != "UNKNOWN" && product.ItemId.ToString().ToUpper() != "MISC")
                {
                    if (products.CoveredDevice != product.CoveredDevice)
                        products.CoveredDevice = product.CoveredDevice;
                    if (products.Weight == 0 || products.Weight == null)
                        products.Weight = product.Weight;
                    //if (products.Weight != product.Weight && product.Weight > 0)
                    //    products.Weight = product.Weight;
                    productSvc.Save(products);
                    entities.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Error adding description" + ex.Message.ToString());
            }

            return isSaved;
        }
    }
}
