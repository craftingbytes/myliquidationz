﻿using System;
using System.Collections.Generic;
using System.Linq;
using Costco.DAL.EntityModels;
using System.Data;
using Costco.Common;

namespace Costco.BLL.BusinessObjects
{
    public class AffiliateContactBO
    {
        public CostcoEntities entities = null;
        Logger logger = null;
        CostcoService<AffiliateContact> contactSvc = null;
        private ISessionCookieValues _sessionValues = null;

        public AffiliateContactBO(ISessionCookieValues sessionValues)
        {
            logger = new Logger(this.GetType());
            entities = new CostcoEntities();
            contactSvc = new CostcoService<AffiliateContact>();
            _sessionValues = sessionValues;
        }

        public IQueryable<AffiliateContact> GetAffiliateContactsPaged(int pageIndex, int pageSize, out int totalRecords, int affiliateId)
        {
            IQueryable<AffiliateContact> affiliateContactList = null;
            try
            {
                affiliateContactList = contactSvc.CostcoEntities.AffiliateContacts.Where(x => x.AffiliateId == affiliateId);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get entity contacts of " + affiliateId + "", ex);
            }
            totalRecords = affiliateContactList.Count();
            return affiliateContactList.OrderBy(e => e.FirstName).Skip(pageIndex * pageSize).Take(pageSize);

        }

        public List<Message> Add(AffiliateContact _affiliateContactModel)
        {
            List<Message> messages = new List<Message>();
            string unHasedPassword = string.Empty;
            try
            {
                AffiliateContact affiliateContact = contactSvc.GetSingle(a => a.UserName.ToLower() == _affiliateContactModel.UserName.ToLower());
                if (affiliateContact != null)
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.UserNameExists));
                }
                affiliateContact = contactSvc.GetSingle(a => a.Email.ToLower() == _affiliateContactModel.Email.ToLower());
                if (affiliateContact != null  && _affiliateContactModel.Email.ToString().Trim() != "" )
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.EmailExists));
                }
                if (messages.Count > 0)
                    return messages;

                State state = new CostcoService<State>().GetSingle(t => t.Id == _affiliateContactModel.StateId);
                if (state == null)
                {
                    _affiliateContactModel.State = null;
                    _affiliateContactModel.StateId = null;
                }
                else
                    _affiliateContactModel.State = state;

                City city = new CostcoService<City>().GetSingle(t => t.Id == _affiliateContactModel.CityId);
                if (city == null)
                {
                    _affiliateContactModel.City = null;
                    _affiliateContactModel.CityId = null;
                }
                else
                    _affiliateContactModel.City = city;

                if (_affiliateContactModel.SecurityQuestionId != 0)
                {
                    SecurityQuestion securityQuestion = new CostcoService<SecurityQuestion>().GetSingle(t => t.Id == _affiliateContactModel.SecurityQuestionId);
                    _affiliateContactModel.SecurityQuestion = securityQuestion;
                }
                else
                {
                    _affiliateContactModel.SecurityQuestionId = null;
                    _affiliateContactModel.Answer = null;
                    _affiliateContactModel.SecurityQuestion = null;
                }

                unHasedPassword = _affiliateContactModel.Password;
                if (ConfigHelper.ComputePwdHash == "true")
                {
                    _affiliateContactModel.Password = Utility.CreateHash(_affiliateContactModel.Password);
                }

                contactSvc.Add(_affiliateContactModel);
   
                AffiliateContact _aff = this.GetAffiliateContact(_affiliateContactModel.Id);
                _sessionValues.SetSessionValue<int>(Constants.SessionParameters.AffiliateContactId,_aff.Id);

                //Mail to user after successful creation
                //bool success = this.SendUserCredentials(_affiliateContactModel, unHasedPassword);
                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            catch (Exception ex)
            {
                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                logger.Error("Failed to add entity contact: " + _affiliateContactModel.Id + "", ex);
            }
            return messages;
        }

        public AffiliateContact GetAffiliateContact(int id)
        {
            AffiliateContact _affContact = null;
            try
            {
                _affContact = contactSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity contact: " + id + "", ex);
            }
            return _affContact;
        }

        public List<Message> Update(AffiliateContact _affiliateContactModel, string services, bool passwordIsChanged)
        {
            List<Message> messages = new List<Message>();
            try
            {
                AffiliateContact affiliateContact = contactSvc.GetSingle(a => a.UserName.ToLower() == _affiliateContactModel.UserName.ToLower() && a.Id != _affiliateContactModel.Id);
                if (affiliateContact != null)
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.UserNameExists));
                }
                affiliateContact = contactSvc.GetSingle(a => a.Email.ToLower() == _affiliateContactModel.Email.ToLower() && a.Id != _affiliateContactModel.Id);
                if (affiliateContact != null && _affiliateContactModel.Email.ToString().Trim() != "")
                {
                    messages.Add(new Message(MessageType.BusinessError.ToString(), Constants.Messages.EmailExists));
                }

                if (messages.Count > 0)
                    return messages;

                State state = new CostcoService<State>().GetSingle(t => t.Id == _affiliateContactModel.StateId);
                if (state == null)
                {
                    _affiliateContactModel.State = null;
                    _affiliateContactModel.StateId = null;
                }
                else
                    _affiliateContactModel.State = state;

                City city = new CostcoService<City>().GetSingle(t => t.Id == _affiliateContactModel.CityId);
                if (city == null)
                {
                    _affiliateContactModel.City = null;
                    _affiliateContactModel.CityId = null;
                }
                else
                    _affiliateContactModel.City = city;

                if (_affiliateContactModel.SecurityQuestionId != 0)
                {
                    SecurityQuestion securityQuestion = new CostcoService<SecurityQuestion>().GetSingle(t => t.Id == _affiliateContactModel.SecurityQuestionId);
                    _affiliateContactModel.SecurityQuestion = securityQuestion;
                }
                else
                {
                    _affiliateContactModel.SecurityQuestion = null;
                    _affiliateContactModel.SecurityQuestionId = null;
                    _affiliateContactModel.Answer = null;
                }

                if (ConfigHelper.ComputePwdHash == "true")
                {
                    //Check the password is changed                    
                    if (passwordIsChanged)
                    {
                        _affiliateContactModel.Password = Utility.CreateHash(_affiliateContactModel.Password);
                    }
                }

                contactSvc.Save(_affiliateContactModel);


                messages.Add(new Message(MessageType.Success.ToString(), Constants.Messages.RecordSaved));
            }
            catch (Exception ex)
            {

                messages.Add(new Message(MessageType.Exception.ToString(), Constants.Messages.ServerError));
                logger.Error("Failed to update entity contact: " + _affiliateContactModel.Id + "", ex);
            }
            return messages;
        }
    }

}
