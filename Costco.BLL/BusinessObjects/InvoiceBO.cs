﻿using System;
using System.Collections.Generic;
using System.Linq;
using Costco.DAL.EntityModels;
using System.Data;
using Costco.Common;
using System.Data.Entity.Core.Objects;

namespace Costco.BLL.BusinessObjects
{
    public class InvoiceBO
    {
        CostcoEntities entities = null;
        Logger logger = null;

        CostcoService<Invoice> invoiceSvc = null;
        CostcoService<Shipment> shipmentSvc = null;
        private ISessionCookieValues _sessionValues = null;

        public InvoiceBO(ISessionCookieValues sessionValues) 
        {
            logger = new Logger(this.GetType());
            entities = new CostcoEntities();
            invoiceSvc = new CostcoService<Invoice>();
            shipmentSvc = new CostcoService<Shipment>();
            _sessionValues = sessionValues;
        }

        public Invoice GetInvoice(int id)
        {
            Invoice _invoice = null;
            try
            {
                _invoice = invoiceSvc.GetSingle(d => d.Id == id);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get an entity: " + id + "", ex);
            }
            return _invoice;
        }

        public Int32 Save(Invoice invoice)
        {
            Int32 id = 0;
            try
            {
                entities.Invoices.Add(invoice);
                entities.SaveChanges();
                id = invoice.Id;
            }
            catch (Exception ex)
            {
                id = 0;
                logger.Error("Failed to save SalesOrder: ", ex);
            }
            return id;

        }

        public bool UpdateInvoice(Invoice invoice)
        {
            bool isSaved = true;
            try
            {
                invoiceSvc.Save(invoice);
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update invoice: ", ex);
            }
            return isSaved;
        }

        public bool UpdateShipmentWithInvoiceId(Int32 shipmentId, Int32 invoiceId)
        {
            bool isSaved = true;
            try
            {
                Shipment shipment = shipmentSvc.GetSingle(d => d.Id == shipmentId);
                shipment.Invoiceid = invoiceId;
                shipmentSvc.Save(shipment);
            }
            catch (Exception ex)
            {
                isSaved = false;
                logger.Error("Failed to update Shipment: ", ex);
            }
            return isSaved;
        }

        public IQueryable<Shipment> GetShipments(int pageIndex, int pageSize, int Year, string months,  out int totalRecords)
        {

            List<int> monthlist = months.Split(',').Select(n => int.Parse(n)).ToList();

            IQueryable<Shipment> shipmentList = null;
            shipmentList = entities.Shipments.Where(x => x.Complete == true && x.ReceivedDate != null && x.ReceivedDate.Value.Year == Year && monthlist.Contains(x.ReceivedDate.Value.Month) && x.Invoiceid == null);
            totalRecords = shipmentList.Count<Shipment>();
            return shipmentList.OrderBy(x => x.Id).ThenBy(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);
        }

        public IQueryable<V_InvoiceCostcoBill> GetCostcoBills(int pageIndex, int pageSize,  string beginDate, string endDate,  string beginId, string endId, out int totalRecords)
        {
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;

            int bId;
            int eId;

            if (!int.TryParse(beginId, out bId))
                bId = 0;
            if (!int.TryParse(endId, out eId))
                eId = int.MaxValue;
            IQueryable<V_InvoiceCostcoBill> invoiceList = null;
            invoiceList = entities.V_InvoiceCostcoBill.Where(x => x.InvoiceDate >= bDate
                                                                           && EntityFunctions.TruncateTime(x.InvoiceDate) <= eDate
                                                                           && x.Id >= bId
                                                                           && x.Id <= eId
                                                                           );
            totalRecords = invoiceList.Count<V_InvoiceCostcoBill>();
            return invoiceList.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize); 
        }

        public IQueryable<V_InvoiceASRPayable> GetASRPayables(int pageIndex, int pageSize, string beginDate, string endDate, string beginId, string endId,string parentId, out int totalRecords)
        {
            DateTime bDate;  //= Convert.ToDateTime(beginDate);
            DateTime eDate;  //= Convert.ToDateTime(endDate);
            if (!DateTime.TryParse(beginDate, out bDate))
                bDate = DateTime.MinValue;

            if (!DateTime.TryParse(endDate, out eDate))
                eDate = DateTime.MaxValue;

            int bId;
            int eId;
            int pId;

            if (!int.TryParse(beginId, out bId))
                bId = 0;
            if (!int.TryParse(endId, out eId))
                eId = int.MaxValue;

            if (!int.TryParse(parentId, out pId))
                pId = 0;

            IQueryable<V_InvoiceASRPayable> invoiceList = null;
            int affiliateTypeId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateTypeId);
            int affiliateId = _sessionValues.GetSessionIntValue(Constants.SessionParameters.AffiliateId);
            invoiceList = entities.V_InvoiceASRPayable.Where(x => x.InvoiceDate >= bDate
                                                                           && EntityFunctions.TruncateTime(x.InvoiceDate) <= eDate
                                                                           && x.Id >= bId
                                                                           && x.Id <= eId
                                                                           && (affiliateTypeId == 1 || x.AffiliateId == affiliateId)
                                                                           && (pId == 0 || x.ParentInvoiceId == pId)
                                                                           );
            totalRecords = invoiceList.Count<V_InvoiceASRPayable>();
            return invoiceList.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize).Take(pageSize);
        }

        public string AddInvoiceAdjustment(Int32 InvoiceId, string Description, decimal Amount)
        {
            string msg = string.Empty;
            try
            {
                CostcoEntities db = new CostcoEntities();
                InvoiceAdjustment newAdjustment = new InvoiceAdjustment();
                newAdjustment.InvoiceId = InvoiceId;
                newAdjustment.Description = Description;
                newAdjustment.Amount = Amount;
                newAdjustment.AdjustmentDate = DateTime.Now;
                db.InvoiceAdjustments.Add(newAdjustment);
                db.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


    }
}
