﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Costco.BLL.Interfaces;
using System.Web.Mvc;
using Costco.DAL.EntityModels;
using Costco.Common;

namespace Costco.BLL.ViewModels
{
    public class AffiliateViewModel
    {
        public Affiliate Affiliate { get; private set; }
        public SelectList States { get; private set; }
        public SelectList Cities { get; private set; }
        public SelectList AffiliateTypes { get; private set; }
        public SelectList ParentAffiliates { get; private set; }
        public SelectList Recyclers { get; private set; }
        public SelectList Carriers { get; private set; }
        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }
        public AffiliateViewModel( Affiliate affiliate)
        {
            Affiliate = affiliate;
            var affiliateTypeList = new CostcoService<AffiliateType>().GetAll().OrderBy(f => f.Type).ToList();
            affiliateTypeList.Insert(0, new AffiliateType() { Id = 0, Type = "Select" });
            AffiliateTypes = new SelectList(affiliateTypeList, "Id", "Type", affiliate.AffiliateTypeId);

            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            States = new SelectList(stateList, "Id", "Name", affiliate.StateId);

            List<City> cityList = new List<City>();
            Cities = new SelectList(cityList, "Id", "Name", affiliate.CityId);
            if (affiliate.StateId > 0) {
                var data1 = new CostcoService<City>().GetAll(x => x.StateId == affiliate.StateId);
                cityList = data1.OrderBy(f => f.Name).ToList();
            }
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            Cities = new SelectList(cityList, "Id", "Name", affiliate.CityId);

            List<Affiliate> parentList = new List<Affiliate>();
            if (affiliate.AffiliateTypeId == 4)
            {
                var data1 = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 3).ToList();
                parentList = data1.OrderBy(x => x.Name).ToList();
            }
            else if (affiliate.AffiliateTypeId == 7)
            {
                var data1 = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 5).ToList();
                parentList = data1.OrderBy(x => x.Name).ToList();
            }
            parentList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            ParentAffiliates = new SelectList(parentList, "Id", "Name", affiliate.ParentId);

            List<Affiliate> recyclerList = new List<Affiliate>();
            if (affiliate.AffiliateTypeId == 4 || affiliate.AffiliateTypeId == 3)
            {
                var data1 = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 5).ToList();
                recyclerList = data1.OrderBy(x => x.Name).ToList();
            }
            recyclerList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            Recyclers = new SelectList(recyclerList, "Id", "Name", affiliate.DefaultRecycler);

            List<Affiliate> carrierList = new List<Affiliate>();
            if (affiliate.AffiliateTypeId == 4 || affiliate.AffiliateTypeId == 3)
            {
                var data1 = new CostcoService<Affiliate>().GetAll(x => x.AffiliateTypeId == 6).ToList();
                carrierList = data1.OrderBy(x => x.Name).ToList();
            }
            carrierList.Insert(0, new Affiliate() { Id = 0, Name = "Select" });
            Carriers = new SelectList(carrierList, "Id", "Name", affiliate.DefaultCarrier);

        }


    }
}
