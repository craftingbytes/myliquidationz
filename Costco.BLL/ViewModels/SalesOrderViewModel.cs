﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Costco.DAL.EntityModels;
using Costco.BLL.BusinessObjects;
using Costco.Common;
using Costco.BLL.Authorization;
using Costco.BLL.ViewModels;
using Costco.BLL;

namespace Costco.BLL.ViewModels
{
    public class SalesOrderViewModel
    {

        public SalesOrder SalesOrder { get; private set; }
        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }

        public SalesOrderViewModel(SalesOrder salesOrder)
        {
            SalesOrder = salesOrder;
        }
    }
}
