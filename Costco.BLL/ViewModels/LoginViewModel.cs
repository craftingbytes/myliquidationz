﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Costco.Common;

namespace Costco.BLL.ViewModels
{
    public class LoginViewModel
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }

    }
}
