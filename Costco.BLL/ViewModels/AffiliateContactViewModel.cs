﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Costco.BLL.Interfaces;
using System.Web.Mvc;
using Costco.DAL.EntityModels;
using Costco.Common;

namespace Costco.BLL.ViewModels
{
    public class AffiliateContactViewModel
    {
        public AffiliateContact AffiliateContact { get; private set; }
        public SelectList States { get; private set; }
        public SelectList Cities { get; private set; }
        public SelectList SecurityQuestions { get; private set; }
        public SelectList Roles { get; private set; }
        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }

        public AffiliateContactViewModel(AffiliateContact affiliateContact)
        {
            AffiliateContact = affiliateContact;

            var stateList = new CostcoService<State>().GetAll().OrderBy(x => x.Name).ToList();
            stateList.Insert(0, new State() { Id = 0, Name = "Select" });
            States = new SelectList(stateList, "Id", "Name", affiliateContact.StateId);

            List<City> cityList = new List<City>();
            Cities = new SelectList(cityList, "Id", "Name", affiliateContact.CityId);
            if (affiliateContact.StateId > 0)
            {
                var data1 = new CostcoService<City>().GetAll(x => x.StateId == affiliateContact.StateId);
                cityList = data1.OrderBy(f => f.Name).ToList();
            }
            cityList.Insert(0, new City() { Id = 0, Name = "Select" });
            Cities = new SelectList(cityList, "Id", "Name", affiliateContact.CityId);

            var roleList = new CostcoService<Role>().GetAll(x => x.AffiliateTypeId == affiliateContact.Affiliate.AffiliateTypeId).ToList();
            roleList.Insert(0, new Role() { Id = -1, RoleName = "Select" });
            Roles = new SelectList(roleList, "Id", "RoleName", affiliateContact.RoleId);

            var questionList = new CostcoService<SecurityQuestion>().GetAll().OrderBy(x => x.Id).ToList();
            questionList.Insert(0, new SecurityQuestion() { Id = 0, Question = "Select" });
            SecurityQuestions = new SelectList(questionList, "Id", "Question", affiliateContact.SecurityQuestionId);

        }
    }
}
