﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Costco.DAL.EntityModels;
using Costco.BLL.BusinessObjects;
using Costco.Common;
using Costco.BLL.Authorization;
using Costco.BLL.ViewModels;
using Costco.BLL;

namespace Costco.BLL.ViewModels
{
    public class BulkShipmentViewModel
    {
        public BulkShipment BulkShipment { get; private set; }
        public Affiliate ShipToAffiliate { get; set; }
        private List<Message> messages = new List<Message>();
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }

        public BulkShipmentViewModel(BulkShipment bulkShipment)
        {
            BulkShipment = bulkShipment;
        }
    }
}
