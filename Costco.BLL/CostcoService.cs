﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Costco.BLL.ViewModels;
using Costco.BLL.Interfaces;
using Costco.DAL.EntityModels;
using Costco.DAL;
using Costco.DAL.Interfaces;
using System.Data;

namespace Costco.BLL
{
    public class CostcoService<T>: IService<T> where T : class
    {

        private IRepository<T> _costcoRepository;
        CostcoEntities _costcoEntities = null;
    
        public CostcoService()
            : this(new CostcoRepository<T>())
        {
            _costcoEntities = (CostcoEntities)_costcoRepository.DbContext;
        }


        public CostcoService(IRepository<T> repository)
        {
            _costcoRepository = repository;
            _costcoEntities = (CostcoEntities)_costcoRepository.DbContext;
        }


        public T GetSingle(Expression<Func<T, bool>> whereCondition)
        {
            return _costcoRepository.GetSingle(whereCondition);
        }
       
        public void Add(T entity)
        {
            _costcoRepository.Add(entity);
        }
        public void Save(T entity)
        {
            _costcoRepository.Save(entity);
        }
        public void Delete(T entity)
        {
            _costcoRepository.Delete(entity);
        }

        public IList<T> GetAll(Expression<Func<T, bool>> whereCondition)
        {
            return _costcoRepository.GetAll(whereCondition);
        }

        public IList<T> GetAll()
        {
            return _costcoRepository.GetAll();
        }

        public IQueryable<T> GetQueryable(Expression<Func<T, bool>> whereCondition)
        {
            return _costcoRepository.GetQueryable(whereCondition);
        }

        public long Count(Expression<Func<T, bool>> whereCondition)
        {
            throw new NotImplementedException();
        }

        public long Count()
        {
            throw new NotImplementedException();
        }

        public CostcoEntities CostcoEntities
        {
            get { return _costcoEntities; }
            set { _costcoEntities = value; }
        }

      
    }
}
