﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Costco.BLL.Authorization
{
    public class AccessRights
    {
        public AccessRights() { }

        public string Name { get; set; }
        public bool? Read { get; set; }
        public bool? Add { get; set; }
        public bool? Update { get; set; }
        public bool? Delete { get; set; }
    }
}
