﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Costco.Common
{
    public class Message
    {
        private string type;
        private string text;

        public Message()
        {
        }

        public Message(string error, string message)
        {
            this.type = error;
            this.text = message;
        }

        public string Type
        {
            get { return type; }
        }

        public string Text
        {
            get { return text; }
        }
    }

    public enum MessageType
    {
        Success = 1,
        Failure = 2,
        BusinessError = 3,
        Exception = 4
    }
}
