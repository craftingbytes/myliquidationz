﻿using System.Collections.Generic;

namespace Costco.Common
{
    public interface ISessionCookieValues
    {
        Dictionary<string, string> GetSessionValues();
        void SetSessionValues(Dictionary<string, string> values);
        string GetSessionValue(string key);
        int GetSessionIntValue(string key);
        void SetSessionValue<T>(string key, T value);
        void ClearCookie();
    }
}
