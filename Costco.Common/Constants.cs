﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;

namespace Costco.Common
{
    public class Constants
    {
        public enum ActionType : int
        {
            Insert = 1,
            Update = 2,
            Delete = 3,
            Read = 4,
            Login = 5
        }

        public const string AffiliateSessionParameters = "AffiliateSessionParameters";

        public class SessionParameters
        {
            // Const keys for cookie dictionary
            public const string AffiliateId = "AffiliateId";
            public const string AffiliateContactId = "AffiliateContactId";
            public const string AffiliateRoleId = "AffiliateRoleId";
            public const string AffiliateTypeId = "AffiliateTypeId";
            public const string AffiliateContactName = "AffiliateContactName";
            public const string IpAddress = "IpAddress";

            // Non-cookie const keys for Controller.Session dictionary
            public const string SelectedAffiliateId = "SelectedAffiliateId";
            public const string CurrentAffiliateID = "CurrentAffiliateID";
            public const string CurrentBulkShippingID ="CurrentBulkShippingID";
            public const string CurrentSalesOrderID = "CurrentSalesOrderID";
            public const string CurrentShipmentID = "CurrentShipmentID";
            public const string CurrentSalvageSalesOrderId = "CurrentSalvageSalesOrderId";
            public const string CurrentShippingID = "CurrentShippingID";
        }

        public class Messages
        {
            public const string InvalidLoginCredentials = "Invalid Login Credentials, Please try with correct credentials.";
            public const string AffiliateNameExists = "Entity Name already exists. Please try with different Entity Name.";
            public const string RecordSaved = "Record has been saved successfully.";
            public const string RecordNotSaved = "Record could not be saved.";
            public const string UserNameExists = "User Name already exists. Please try with different User Name.";
            public const string EmailExists = "Email already exists. Please try with different Email.";
            public const string InformationExists = "If this information is in our record then we will send you the password";
            public const string ServerError = "Server error occured.";
            public const string TryLater = "Your Request could not be processed now, please try later.";
            public const string RecordExist = "This record already exist";
            public const string UserInactive = "The current user is not active in our record";
            //public static string AccountLocked = 

            public static string AccountLocked
            {
                get { return "" + (ConfigHelper.LoginTryCount + 1) + " unsuccesful attempts to login have caused this address or account to be locked. The account will remain locked for " + ConfigHelper.LoginTryMinute.ToString() + "  minutes"; }
            }
            public static string LoginTry(int? count)
            {
                string msg = "You have made " + (count) + " unsuccessful attempt to login. " + (ConfigHelper.LoginTryCount + 1) + " unsuccessful attempts will cause the address or account to be locked for " + ConfigHelper.LoginTryMinute.ToString() + "  minutes";
                return msg;
            }

            
        }
    }
}
