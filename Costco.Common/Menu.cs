﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace Costco.Common
{
    public class Menu
    {
        public Menu()
        {
            MenuItems = new List<Menu>();
        }

        public string Name { get; set; }
        public string Caption { get; set; }
        public string URL { get; set; }
        public string OnClick { get; set; }
        public bool Show { get; set; }
        public List<Menu> MenuItems { get; set; }

        //private string xmlDocumentPath;

        //public List<Menu> GetMenus(string affiliateType)
        //{
        //    try
        //    {
        //        List<Menu> _menus = new List<Menu>();

        //        xmlDocumentPath = System.Web.HttpContext.Current.Server.MapPath("~/Helpers/Menus.xml");
        //        XmlDocument _xmlDocumnet = new XmlDocument();
        //        _xmlDocumnet.Load(xmlDocumentPath);

        //        XmlElement _rootElement = _xmlDocumnet.DocumentElement;

        //        foreach (XmlNode _node in _rootElement.SelectNodes("//Role[@Name='Public']/Menu"))
        //        {
        //            Menu _menu = new Menu();

        //            if (_node.Attributes["Name"] != null)
        //                _menu.Name = _node.Attributes["Name"].Value;
        //            if (_node.Attributes["Caption"] != null)
        //                _menu.Caption = _node.Attributes["Caption"].Value;
        //            if (_node.Attributes["URL"] != null)
        //                _menu.URL = _node.Attributes["URL"].Value;
        //            if (_node.Attributes["OnClick"] != null)
        //                _menu.OnClick = _node.Attributes["OnClick"].Value;
        //            if (_node.Attributes["Show"] != null)
        //                _menu.Show = Convert.ToBoolean(_node.Attributes["Show"].Value);

        //            GetChildMenus(_node, _menu);
        //            if (_menu.Show)
        //                _menus.Add(_menu);
        //        }
        //        return _menus;
        //    }
        //    catch { throw; }
        //}

        //private void GetChildMenus(XmlNode _parentNode, Menu _parentMenu)
        //{
        //    try
        //    {
        //        foreach (XmlNode _childNode in _parentNode.ChildNodes)
        //        {
        //            Menu _childMenu = new Menu();
        //            if (_childNode.Attributes["Name"] != null)
        //                _childMenu.Name = _childNode.Attributes["Name"].Value;
        //            if (_childNode.Attributes["Caption"] != null)
        //                _childMenu.Caption = _childNode.Attributes["Caption"].Value;
        //            if (_childNode.Attributes["URL"] != null)
        //                _childMenu.URL = _childNode.Attributes["URL"].Value;
        //            if (_childNode.Attributes["OnClick"] != null)
        //                _childMenu.OnClick = _childNode.Attributes["OnClick"].Value;
        //            if (_childNode.Attributes["Show"] != null)
        //                _childMenu.Show = Convert.ToBoolean(_childNode.Attributes["Show"].Value);
        //            GetChildMenus(_childNode, _childMenu);
        //            if (_childMenu.Show)
        //                _parentMenu.MenuItems.Add(_childMenu);
        //        }
        //    }
        //    catch { throw; }
        //}
    }
}
