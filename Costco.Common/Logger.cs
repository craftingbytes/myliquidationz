﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using log4net;
using System.Diagnostics;
using System.Reflection;

namespace Costco.Common
{
    public class Logger
    {
        #region Constants and Fields

        /// <summary>
        /// Determines whether the DEBUG Mode is enabled.
        /// </summary>
        private readonly bool isDebugEnabled;

        /// <summary>
        /// The is error enabled.
        /// </summary>
        private readonly bool isErrorEnabled;

        /// <summary>
        /// Determines whether the FATAL Mode is enabled.
        /// </summary>
        private readonly bool isFatalEnabled;

        /// <summary>
        /// Determines whether the INFO Mode is enabled.
        /// </summary>
        private readonly bool isInfoEnabled;

        /// <summary>
        /// Determines whether the WARN Mode is enabled.
        /// </summary>
        private readonly bool isWarnEnabled;

        /// <summary>
        /// The logger object
        /// </summary>
        private readonly ILog log;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Logger class.
        /// </summary>
        public Logger()
            : this(new StackTrace().GetFrame(1).GetMethod().DeclaringType)
        {
        }

        /// <summary>
        /// Initializes a new instance of the Logger class.
        /// </summary>
        /// <param name="type">
        /// The type of logger.
        /// </param>
        public Logger(Type type)
        {
            this.log = LogManager.GetLogger(type);
            this.isDebugEnabled = this.log.IsDebugEnabled;
            this.isErrorEnabled = this.log.IsErrorEnabled;
            this.isInfoEnabled = this.log.IsInfoEnabled;
            this.isFatalEnabled = this.log.IsFatalEnabled;
            this.isWarnEnabled = this.log.IsWarnEnabled;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Logs the debug message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Debug(string message)
        {
            if (this.isDebugEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Debug(methodBase.DeclaringType.Name + methodBase.Name + " : " + message);
                else
                    this.log.Debug(methodBase.Name + " : " + message);
            }
        }

        /// <summary>
        /// Logs the debug message and the exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        public void Debug(string message, Exception exception)
        {
            if (this.isDebugEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Debug(methodBase.DeclaringType.Name + methodBase.Name + " : " + message, exception);
                else
                    this.log.Debug(methodBase.Name + " : " + message, exception);
            }
        }

        /// <summary>
        /// Logs the error message.
        /// </summary>
        /// <param name="errorMessage">
        /// The error message.
        /// </param>
        public void Error(string message)
        {
            if (this.isErrorEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Error(methodBase.DeclaringType.Name + methodBase.Name + " : " + message);
                else
                    this.log.Error(methodBase.Name + " : " + message);
            }
        }

        /// <summary>
        /// Logs the error message and the exception.
        /// </summary>
        /// <param name="errorMessage">
        /// The error message.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        public void Error(string message, Exception exception)
        {
            if (this.isErrorEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Error(methodBase.DeclaringType.Name + methodBase.Name + " : " + message);
                else
                    this.log.Error(methodBase.Name + " : " + message, exception);
            }
        }

        /// <summary>
        /// Logs the fatal error message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Fatal(string message)
        {
            if (this.isFatalEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Fatal(methodBase.DeclaringType.Name + methodBase.Name + " : " + message);
                else
                    this.log.Fatal(methodBase.Name + " : " + message);
            }
        }

        /// <summary>
        /// Logs the fatal error message and the exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        public void Fatal(string message, Exception exception)
        {
            if (this.isFatalEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Fatal(methodBase.DeclaringType.Name + methodBase.Name + " : " + message, exception);
                else
                    this.log.Fatal(methodBase.Name + " : " + message, exception);
            }
        }

        /// <summary>
        /// Logs the info message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Info(string message)
        {
            if (this.isInfoEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                this.log.Info(message);
                if (methodBase.DeclaringType != null)
                    this.log.Info(methodBase.DeclaringType.Name + methodBase.Name + " : " + message);
                else
                    this.log.Info(methodBase.Name + " : " + message);
            }
        }

        /// <summary>
        /// Logs the info message and the exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        public void Info(string message, Exception exception)
        {
            if (this.isInfoEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Info(methodBase.DeclaringType.Name + methodBase.Name + " : " + message, exception);
                else
                    this.log.Info(methodBase.Name + " : " + message, exception);
            }
        }

        /// <summary>
        /// Logs the warning message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Warn(string message)
        {
            if (this.isWarnEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Warn(methodBase.DeclaringType.Name + methodBase.Name + " : " + message);
                else
                    this.log.Warn(methodBase.Name + " : " + message);
            }
        }

        /// <summary>
        /// Logs the warning message and the exception.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        public void Warn(string message, Exception exception)
        {
            if (this.isWarnEnabled)
            {
                MethodBase methodBase = new StackTrace().GetFrame(1).GetMethod();
                if (methodBase.DeclaringType != null)
                    this.log.Warn(methodBase.DeclaringType.Name + methodBase.Name + " : " + message, exception);
                else
                    this.log.Warn(methodBase.Name + " : " + message, exception);
            }
        }

        #endregion
    }
}
