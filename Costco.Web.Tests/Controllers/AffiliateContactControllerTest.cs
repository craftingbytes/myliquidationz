﻿using Costco.Common;
using Costco.DAL.EntityModels;
using Costco.Web.Controllers;
using Http.TestLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Costco.Web.Tests.Controllers
{
    [TestClass]
    public class AffiliateContactControllerTest
    {
        [TestMethod]
        public void HackInANewUser()
        {
            using (new HttpSimulator("/AffiliateContact", @"C:\ReturnsV1_Code\eworld\EworldCostco\trunk\Costco.Web\").SimulateRequest())
            {
                var mockSession = new Mock<ISessionCookieValues>();
                var controller = new AffiliateContactController(mockSession.Object);
                controller.Index(new AffiliateContact
                {
                    Active = true,
                    Address1 = "501 W Braodway St #515",
                    SecurityQuestionId = 3, // mothers maiden
                    Answer = "Forsythe",
                    CityId = 12946,
                    StateId = 6,
                    Email = "owen@datafit.com",
                    FirstName = "Owen",
                    LastName = "Murphy",
                    Password = Utility.CreateHash("DemoPWD123"),
                    RoleId = 1,
                    UserName = "omurphy",
                });
            }
        }
    }
}
